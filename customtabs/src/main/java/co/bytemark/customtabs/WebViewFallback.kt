package co.bytemark.customtabs

import android.content.Context
import android.content.Intent
import android.net.Uri
import co.bytemark.customtabs.CustomTabsActivityHelper.CustomTabFallback

/**
 * Default [CustomTabsActivityHelper.CustomTabFallback] implementation
 * that uses [WebViewActivity] to display the requested [Uri].
 */
class WebViewFallback : CustomTabFallback {
    override fun openUri(
        context: Context?,
        uri: Uri?
    ) {
        val intent = Intent(context, WebViewActivity::class.java)
        intent.putExtra(WebViewActivity.EXTRA_URL, uri.toString())
        context?.startActivity(intent)
    }
}