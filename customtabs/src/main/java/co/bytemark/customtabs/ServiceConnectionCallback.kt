package co.bytemark.customtabs

import androidx.browser.customtabs.CustomTabsClient

interface ServiceConnectionCallback {
    fun onServiceConnected(client: CustomTabsClient?)

    fun onServiceDisconnected()
}