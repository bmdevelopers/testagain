package co.bytemark.customtabs

import android.content.Context
import android.net.Uri
import androidx.browser.customtabs.CustomTabsClient
import androidx.browser.customtabs.CustomTabsIntent

/**
 * This is a helper class to manage the connection to the Custom Tabs Service.
 */
class CustomTabsActivityHelper : ServiceConnectionCallback {
    private var mClient: CustomTabsClient? = null
    override fun onServiceConnected(client: CustomTabsClient?) {
        mClient = client
        mClient?.warmup(0L)
    }

    override fun onServiceDisconnected() {
        mClient = null
    }

    /**
     * To be used as a fallback to open the Uri when Custom Tabs is not available.
     */
    interface CustomTabFallback {
        fun openUri(context: Context?, uri: Uri?)
    }

    companion object {
        fun openCustomTab(
            context: Context?,
            customTabsIntent: CustomTabsIntent,
            uri: Uri?,
            fallback: CustomTabFallback?
        ) {
            val packageName =
                CustomTabsHelper.getPackageNameToUse(context)

            //If we cant find a package name, it means theres no browser that supports
            //Chrome Custom Tabs installed. So, we fallback to the webview
            if (packageName == null) {
                fallback?.openUri(context, uri)
            } else {
                customTabsIntent.intent.setPackage(packageName)
                customTabsIntent.launchUrl(context!!, uri!!)
            }
        }
    }
}