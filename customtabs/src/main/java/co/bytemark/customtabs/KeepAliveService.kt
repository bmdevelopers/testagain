package co.bytemark.customtabs

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder

/**
 * Empty service to bind to, raising the application's importance.
 */
internal class KeepAliveService : Service() {
    override fun onBind(intent: Intent): IBinder? {
        return binder
    }

    companion object {
        private val binder = Binder()
    }
}