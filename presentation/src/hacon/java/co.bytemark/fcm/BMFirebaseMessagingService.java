package co.bytemark.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.core.content.ContextCompat;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.util.Map;
import java.util.Random;
import co.bytemark.R;
import co.bytemark.appnotification.NotificationsActivity;
import co.bytemark.helpers.AppConstants;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.widgets.util.Util;
import de.hafas.notification.messaging.PushMessageHandler;
import de.hafas.notification.messaging.PushMessageProcessingListener;
import de.hafas.notification.registration.PushRegistrationHandler;
import timber.log.Timber;

/**
 * Receive downstream FCM messages.
 */
public class BMFirebaseMessagingService extends FirebaseMessagingService {

    private static final String ADMIN_CHANNEL_ID = "admin_channel";
    private static final String HAFAS_PUSH_APP_INFO = "appInfo";
    private NotificationManager notificationManager;

    @Override
    public void onNewToken(String s) {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Timber.d("Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        BytemarkSDK.sendRegistrationToServer(refreshedToken);

        if(BytemarkSDK.SDKUtility.isElertEnabled()) {
            BytemarkSDK.SDKUtility.setDeviceTokenSentToElerts(false);
            Util.registerECUINotifications(getApplicationContext());
        }

        if (refreshedToken != null)
            PushRegistrationHandler.getInstance().setRegistrationId(getApplicationContext(), refreshedToken);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // HAFAS push messages are always sent as "data messages".
        Map<String, String> payload = remoteMessage.getData();

        // Check payload for HAFAS identifier.
        if (payload.containsKey(HAFAS_PUSH_APP_INFO)) // && payload.get(HAFAS_PUSH_APP_INFO).equals("<identifier>")
        {
            PushMessageHandler messageHandler = new PushMessageHandler(getApplicationContext(), new PushMessageProcessingListener() {
                @Override
                public void onSuccess() {
                    // Called if the HafasApp processed the message successfully.
                    // A notification was shown to the user.
                }

                @Override
                public void onError() {
                    // Called if an error occurred.
                    // No notification was shown.
                }
            });

            // Message processing involves HAFAS server communication which may take a while.
            // Consider using Firebase Job Dispatcher for message handling (refer to Firebase docs for details).
            messageHandler.onMessage(payload);
        } else if (BytemarkSDK.SDKUtility.isElertEnabled()) {
            if (BytemarkSDK.isLoggedIn()) {
                try {
                    Intent listService = new Intent(getApplicationContext(), Class.forName(AppConstants.ELERTS_MESSAGE_SERVICE_CLASS_NAME));
                    listService.setAction(getResources().getString(Util.getResId(AppConstants.ELERTS_MESSAGE_SERVICE_RESOURCE_NAME, R.string.class)));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startForegroundService(listService);
                    } else {
                        startService(listService);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            // Process push message by yourself
            logTheResult(remoteMessage);

            //You should use an actual ID instead
            int notificationId = new Random().nextInt(60000);

            notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                setupChannels();
            }

            // Check if message contains a data payload & if the Push Notification is for this particular organization.
            if (remoteMessage.getData().size() > 0 && remoteMessage.getData().get("org_uuid")
                    .equals(BytemarkSDK.SDKUtility.getConf().getOrganization().getUuid())) {
                showNotification(remoteMessage, notificationId);
            }

            // Check if message contains a notification payload.
            if (remoteMessage.getNotification() != null) {
                logNotificationPayloadResult(remoteMessage);

                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                                .setSmallIcon(R.mipmap.notification_icon)
                                .setColor(getNotificationIconTintColor())
                                .setContentTitle(remoteMessage.getNotification().getTitle())
                                .setContentText(remoteMessage.getNotification().getBody())
                                .setAutoCancel(true)
                                .setSound(getDefaultSoundUri())
                                .setContentIntent(createPendingIntent());

                notificationManager.notify(notificationId, notificationBuilder.build());
            }
        }
    }

    private int getNotificationIconTintColor() {
        return ContextCompat.getColor(this, R.color.orgHeaderBackgroundColor);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        CharSequence adminChannelName = "Default";
        String adminChannelDescription = "Default notification channel";

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.BLUE);
        adminChannel.enableVibration(true);
        adminChannel.setShowBadge(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

    private void logTheResult(RemoteMessage remoteMessage) {
        Timber.d("RemoteMessage: getFrom " + remoteMessage.getFrom());
        Timber.d("RemoteMessage: getTo " + remoteMessage.getTo());
        Timber.d("RemoteMessage: getData " + remoteMessage.getData());
        Timber.d("RemoteMessage: getCollapseKey " + remoteMessage.getCollapseKey());
        Timber.d("RemoteMessage: getMessageId " + remoteMessage.getMessageId());
        Timber.d("RemoteMessage: getMessageType " + remoteMessage.getMessageType());
        Timber.d("RemoteMessage: getSentTime " + remoteMessage.getSentTime());
        Timber.d("RemoteMessage: getTtl " + remoteMessage.getTtl());
        Timber.d("RemoteMessage: getNotification " + remoteMessage.getNotification());
    }

    private void logNotificationPayloadResult(RemoteMessage remoteMessage) {
        Timber.d("Message Notification toString: " + remoteMessage.getNotification().toString());
        Timber.d("Message Notification getTitle: " + remoteMessage.getNotification().getTitle());
        Timber.d("Message Notification getTitleLocalizationKey: " + remoteMessage.getNotification().getTitleLocalizationKey());
        Timber.d("Message Notification getTitleLocalizationArgs: " + remoteMessage.getNotification().getTitleLocalizationArgs());
        Timber.d("Message Notification getBody: " + remoteMessage.getNotification().getBody());
        Timber.d("Message Notification getBodyLocalizationKey: " + remoteMessage.getNotification().getBodyLocalizationKey());
        Timber.d("Message Notification getBodyLocalizationArgs: " + remoteMessage.getNotification().getBodyLocalizationArgs());
        Timber.d("Message Notification getIcon: " + remoteMessage.getNotification().getIcon());
        Timber.d("Message Notification getSound: " + remoteMessage.getNotification().getSound());
        Timber.d("Message Notification getTag: " + remoteMessage.getNotification().getTag());
        Timber.d("Message Notification getColor: " + remoteMessage.getNotification().getColor());
        Timber.d("Message Notification getClickAction: " + remoteMessage.getNotification().getClickAction());
        Timber.d("Message Notification getLink: " + remoteMessage.getNotification().getLink());
    }

    private PendingIntent createPendingIntent() {
        Intent intent = new Intent(this, NotificationsActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);
        return stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private Uri getDefaultSoundUri() {
        return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    }

    private void showNotification(RemoteMessage remoteMessage, int notificationId) {
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                        .setSmallIcon(R.mipmap.notification_icon)
                        .setColor(getNotificationIconTintColor())
                        .setContentTitle(remoteMessage.getData().get("title"))
                        .setContentText(remoteMessage.getData().get("body"))
                        .setAutoCancel(true)
                        .setSound(getDefaultSoundUri())
                        .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                        .setNumber(1)
                        .setContentIntent(createPendingIntent());

        notificationManager.notify(notificationId, notificationBuilder.build());
    }
}
