#!/usr/bin/env bash

function readJson {
  UNAMESTR=`uname`
  if [[ "$UNAMESTR" == 'Linux' ]]; then
    SED_EXTENDED='-r'
  elif [[ "$UNAMESTR" == 'Darwin' ]]; then
    SED_EXTENDED='-E'
  fi;

  VALUE=`grep -m 1 "\"${2}\"" ${1} | sed ${SED_EXTENDED} 's/^ *//;s/.*: *"//;s/",?//'`

  if [ ! "$VALUE" ]; then
    echo "Error: Cannot find \"${2}\" in ${1}" >&2;
    exit 1;
  else
    echo $VALUE ;
  fi;
}

IDENTIFIER=`readJson conf.json identifier` || exit 1;
REVERSE_DOMAIN=`readJson conf.json reverse_domain` || exit 2;
TYPE=`readJson conf.json type` || exit 2;
ENVIRONMENT=$(echo "${TYPE##*-}" | tr '[:upper:]' '[:lower:]')

PACKAGE_NAME="${REVERSE_DOMAIN}.${IDENTIFIER}.${ENVIRONMENT}"
echo "Package Name: ${PACKAGE_NAME}"

adb shell am start -a android.intent.action.MAIN ${PACKAGE_NAME}/co.bytemark.splash.SplashScreenActivity