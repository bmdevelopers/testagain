package co.bytemark.init_fare_capping

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.helpers.AppConstants

class InitFareCappingActivity : MasterActivity() {
    private var fareMedium: FareMedium? = null
    private var title: String? = null
    override fun getLayoutRes(): Int {
        return R.layout.activity_init_fare_capping
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fareMedium = intent?.extras?.getParcelable(AppConstants.FARE_MEDIA)
        title = intent?.extras?.getString(AppConstants.TITLE)
        setToolbarTitle(title)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, InitFareCappingFragment.newInstance(fareMedium))
                .commit()
    }

    override fun useHamburgerMenu(): Boolean {
        return false
    }
}