package co.bytemark.init_fare_capping

import androidx.lifecycle.MutableLiveData
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.init_fare_capping.GetInitFareCappingUseCase
import co.bytemark.domain.interactor.init_fare_capping.InitFareCappingRequestValues
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.init_fare_capping.InitFareCappingData
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by ranjith on 26/05/20
 */

class InitFareCappingViewModel @Inject constructor(
        private val getFareCappingUseCase: GetInitFareCappingUseCase
) : BaseViewModel() {

    var fareCappingData = MutableLiveData<Result<InitFareCappingData>>()

    fun loadFareCappings(fareMediumId: String) {
        fareCappingData.postValue(Result.Loading(null))
        uiScope.launch {
            val result = getFareCappingUseCase(InitFareCappingRequestValues(fareMediumId))
            fareCappingData.postValue(result)
        }
    }

}