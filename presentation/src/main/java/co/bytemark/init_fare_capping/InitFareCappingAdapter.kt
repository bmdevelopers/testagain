package co.bytemark.init_fare_capping

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.text.color
import androidx.core.text.scale
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.data.util.Utils
import co.bytemark.domain.model.init_fare_capping.InitFareCapping
import co.bytemark.helpers.ConfHelper
import co.bytemark.init_fare_capping.InitFareCappingAdapter.FareCappingViewHolder
import co.bytemark.widgets.util.font
import kotlinx.android.synthetic.main.item_fare_capping.view.*
import java.text.ParseException
import kotlin.math.roundToInt

class InitFareCappingAdapter constructor(private val confHelper: ConfHelper) :
    ListAdapter<InitFareCapping, FareCappingViewHolder>(InitFareCappingDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = FareCappingViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_fare_capping, parent, false)
    )

    override fun onBindViewHolder(holder: FareCappingViewHolder, position: Int) =
        holder.bind(getItem(position))

    inner class FareCappingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(fareCapping: InitFareCapping) {
            with(itemView) {
                textViewName.text = fareCapping.name
                try {
                    Utils.convertToCalendarFormServer(fareCapping.validTo)?.let {
                        textViewExpirationTime.text =
                                confHelper.getFormattedDateAndTime(it.timeInMillis)
                    }
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
                fareCapping.percentageValue?.let {
                    progressBar.progress = (100 - it).roundToInt()
                    if (fareCapping.percentageValue == 0f) {
                        constraintLayout.foreground = ColorDrawable(
                            ContextCompat.getColor(context, R.color.selectionBackgroundColor)
                        )
                        statusTextView.text = formattedFareCappingSpanText(
                            context, context.getString(R.string.fare_capping_ride_free)
                        )
                    } else if (fareCapping.missingValue != null) {
                        val statusText = formattedFareCappingSpanText(
                            context, confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
                                fareCapping.missingValue!!
                            )
                        ).apply {
                            append(" ")
                            color(context.getColor(R.color.orgCollectionPrimaryTextColor)) {
                                append(context.getString(R.string.fare_capping_until_you_ride_free))
                            }
                        }
                        statusTextView.text = statusText
                    }
                }
            }
        }
    }


    fun formattedFareCappingSpanText(context: Context, string: String) =
        SpannableStringBuilder().apply {
            font("sans-serif-medium") {
                scale(1.3f) {
                    color(context.getColor(R.color.orgAccentBackgroundColor)) {
                        append(
                            string
                        )
                    }
                }
            }
        }
}