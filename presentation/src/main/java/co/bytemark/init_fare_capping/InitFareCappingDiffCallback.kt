package co.bytemark.init_fare_capping

import androidx.recyclerview.widget.DiffUtil
import co.bytemark.domain.model.init_fare_capping.InitFareCapping

class InitFareCappingDiffCallback : DiffUtil.ItemCallback<InitFareCapping>() {
    override fun areItemsTheSame(oldItem: InitFareCapping, newItem: InitFareCapping): Boolean {
        return oldItem.potId == newItem.potId
    }

    override fun areContentsTheSame(oldItem: InitFareCapping, newItem: InitFareCapping): Boolean {
        return oldItem == newItem
    }
}