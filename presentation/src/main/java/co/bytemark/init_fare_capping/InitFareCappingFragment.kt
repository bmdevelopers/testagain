package co.bytemark.init_fare_capping

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.InfoMenuBaseFragment
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.init_fare_capping.InitFareCappingData
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.util.createViewModel
import kotlinx.android.synthetic.main.fragment_init_fare_capping.*
import javax.inject.Inject

class InitFareCappingFragment : InfoMenuBaseFragment() {

    lateinit var viewModel: InitFareCappingViewModel

    private lateinit var fareCappingAdapter: InitFareCappingAdapter

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    val fareMedium: FareMedium? = null

    override fun onInject() {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = createViewModel { CustomerMobileApp.appComponent.initFareCappingViewModel }
        if (confHelper.isInfoIconEnabledFor(AppConstants.SCREEN_INIT_FARE_CAPPING)) {
            setHasOptionsMenu(true)
            optionsMenuEnabled = true
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_init_fare_capping, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val fareMedium: FareMedium? = arguments?.getParcelable(ARG_FAREMEDIUM)
        fareMedium?.let { it ->
            textViewFareMediaNickname.text = it.nickname
            textViewId.text = String.format(
                    getString(R.string.fare_medium_id),
                    it.printedCardNumber
            )
            fareCappingAdapter = InitFareCappingAdapter(confHelper)
            recyclerViewFareCapping.adapter = fareCappingAdapter

            // load fare-capping
            viewModel.fareCappingData.observe(this, Observer { result -> loadFareCapping(result) })
            viewModel.loadFareCappings(it.fareMediumId)
        }
    }

    private fun loadFareCapping(result: Result<InitFareCappingData>?) {
        result?.let {
            emptyStateLayout.showContent()
            when (it) {
                is Result.Loading -> {
                    emptyStateLayout.showLoading()
                }
                is Result.Success -> {
                    analyticsPlatformAdapter.fareCappingLoaded(
                            AnalyticsPlatformsContract.Status.SUCCESS,
                            ""
                    )
                    if (it.data?.fareCappingList?.isNotEmpty() == true)
                        fareCappingAdapter.submitList(it.data?.fareCappingList)
                    else {
                        emptyStateLayout.showError(
                                R.drawable.available_passes_empty_state,
                                getString(R.string.fare_capping_unavailable),
                                null,
                                getString(R.string.use_tickets_go_back)
                        ) { activity?.finish() }
                    }
                }
                is Result.Failure -> {
                    val bmError = it.bmError.first()
                    analyticsPlatformAdapter.fareCappingLoaded(
                        AnalyticsPlatformsContract.Status.FAILURE,
                        bmError.message
                    )
                    handleError(bmError)
                }
            }
        }
    }

    override fun connectionErrorDialog(
        positiveTextId: Int,
        enableNegativeAction: Boolean,
        negativeTextId: Int,
        finishOnDismiss: Boolean,
        positiveAction: () -> Unit,
        negativeAction: () -> Unit
    ) {
        emptyStateLayout.showError(
            R.drawable.error_material,
            R.string.change_password_popup_conErrorTitle,
            R.string.change_password_Popup_con_error_body
        )
    }

    override fun showDefaultErrorDialog(
        title: String?,
        errorMsg: String?,
        finishActivity: Boolean
    ) {
        emptyStateLayout.showError(
            R.drawable.error_material,
            R.string.popup_error,
            errorMsg
        )
    }

    override fun showAboutScreenDialog() {
        val title = activity?.intent?.getStringExtra(AppConstants.TITLE) ?: getString(R.string.fare_capping_title)
        showDialog(
                getString(R.string.info_alert_title,title),
                getString(R.string.info_message_init_fare_capping_screen),
                getString(R.string.ok),
        )
    }

    override fun getScreenName() = AppConstants.SCREEN_INIT_FARE_CAPPING

    companion object {
        const val ARG_FAREMEDIUM = "FareMedium"
        fun newInstance(fareMedium: FareMedium?): InitFareCappingFragment {
            return InitFareCappingFragment().apply {
                arguments = Bundle()
                        .apply { putParcelable(ARG_FAREMEDIUM, fareMedium) }
            }
        }
    }
}