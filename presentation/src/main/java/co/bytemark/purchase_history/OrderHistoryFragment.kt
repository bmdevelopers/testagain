package co.bytemark.purchase_history

import androidx.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.Result
import co.bytemark.receipt.ReceiptActivity
import co.bytemark.sdk.model.common.Order
import co.bytemark.widgets.util.connectionErrorDialog
import co.bytemark.widgets.util.createViewModel
import co.bytemark.widgets.util.isOnline
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.fragment_order_history.*
import javax.inject.Inject

/** Created by Santosh on 16/04/19.
 */

class OrderHistoryFragment : BaseMvvmFragment() {

    private lateinit var viewModel: OrderHistoryViewModel

    private lateinit var loadingDialog: MaterialDialog

    private var currentPage = 0

    private var orders: MutableList<Order> = mutableListOf()

    private lateinit var orderHistoryAdapter: OrderHistoryAdapter

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = createViewModel { CustomerMobileApp.appComponent.orderHistoryViewModel }
    }

    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.fragment_order_history, container)!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        orderHistoryAdapter = OrderHistoryAdapter(confHelper, fun(it: Order) {
            startActivity(Intent(activity, ReceiptActivity::class.java).putExtra("Receipt", it))
        }, {
            loadReceipts()
        })
        orderHistoryRecView.adapter = orderHistoryAdapter
        loadReceipts()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initLoadingDialog()
    }

    private fun loadReceipts() {
        if (!context?.isOnline()!!) {
            context?.connectionErrorDialog()
            return
        }
        viewModel.getOrders(PER_PAGE, currentPage + 1, TIME_PURCHASED, DESCENDING)?.observe(this, Observer { result ->
            result?.let {
                when (it) {
                    is Result.Loading -> {
                        loadingDialog.show()
                    }

                    is Result.Success -> {
                        loadingDialog.dismiss()
                        val data = it.data!!

                        var message = ""
                        if (data.totalCount == 0 && data.orders.isEmpty()) {
                            showNoReceiptsAvailableDialog()
                            message = getString(R.string.purchase_history_no_purchases_have_been_made_yet)
                        } else if (data.totalCount > 0 && data.orders.isEmpty() && data.pagination?.next == null) {
                            Snackbar.make(orderHistoryRecView, R.string.purchase_history_popup_no_more_transactions, Snackbar.LENGTH_LONG).show()
                            orderHistoryRecView.visibility = View.VISIBLE
                            orderHistoryAdapter.buttonVisible = false
                            orderHistoryAdapter.notifyDataSetChanged()
                            message = getString(R.string.purchase_history_popup_no_more_transactions)
                        } else if (data.totalCount > 0 && (data.orders.size < PER_PAGE ||
                                        (currentPage * PER_PAGE) + data.orders.size == data.totalCount)
                             && data.pagination?.next == null) {
                            orderHistoryRecView.visibility = View.VISIBLE
                            orders.addAll(data.orders)
                            orderHistoryAdapter.buttonVisible = false
                            orderHistoryAdapter.submitList(orders)
                            orderHistoryAdapter.notifyDataSetChanged()
                        } else {
                            orderHistoryRecView.visibility = View.VISIBLE
                            currentPage = data.pagination!!.page
                            orders.addAll(data.orders)
                            orderHistoryAdapter.buttonVisible = true
                            orderHistoryAdapter.submitList(orders)
                        }
                        analyticsPlatformAdapter.purchaseHistoryScreenDisplayed(currentPage, AnalyticsPlatformsContract.Status.SUCCESS, message)
                    }

                    is Result.Failure -> {
                        loadingDialog.dismiss()
                        val bmError = it.bmError[0]
                        handleError(bmError)
                        analyticsPlatformAdapter.purchaseHistoryScreenDisplayed(currentPage, AnalyticsPlatformsContract.Status.FAILURE, bmError.message)
                    }
                }
            }
        })
    }

    private fun initLoadingDialog() {
        loadingDialog = MaterialDialog.Builder(context!!)
                .content(getString(R.string.popup_getting)
                        .plus(" ").plus((activity as OrderHistoryActivity).title.plus("...")))
                .progress(true, 0)
                .cancelable(false)
                .build()
    }

    private fun showNoReceiptsAvailableDialog() {
        AlertDialog.Builder(context!!)
                .setTitle(R.string.purchase_history_popup_no_receipt)
                .setMessage(R.string.purchase_history_no_purchases_have_been_made_yet)
                .setPositiveButton(R.string.ok) { dialog, _ -> dialog.dismiss() }
                .show()
    }

    override fun showDefaultErrorDialog(title: String?, errorMsg: String?, finishActivity: Boolean) {
        super.showDefaultErrorDialog(title, errorMsg, finishActivity = orders.size == 0)
    }

    companion object {
        const val PER_PAGE = 4
        const val TIME_PURCHASED = "time_purchased"
        const val DESCENDING = "DESC"
    }
}