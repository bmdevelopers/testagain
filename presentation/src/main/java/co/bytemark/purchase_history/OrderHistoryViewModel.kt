package co.bytemark.purchase_history

import androidx.lifecycle.ViewModel
import co.bytemark.domain.interactor.purchase_history.OrderHistoryUseCase
import co.bytemark.domain.interactor.purchase_history.OrderHistoryUseCaseValue
import javax.inject.Inject

/** Created by Santosh on 16/04/19.
 */

class OrderHistoryViewModel @Inject constructor(
        private val orderHistoryUseCase: OrderHistoryUseCase
) : ViewModel() {

    fun getOrders(perPage: Int, pageNo: Int, sortBy: String, order: String) =
            orderHistoryUseCase.getLiveData(OrderHistoryUseCaseValue(perPage, pageNo, sortBy, order))

}