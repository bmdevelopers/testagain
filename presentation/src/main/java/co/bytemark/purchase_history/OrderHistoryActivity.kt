package co.bytemark.purchase_history

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.model.common.Action

/** Created by Santosh on 16/04/19.
 */

class OrderHistoryActivity : MasterActivity() {

    var title: String? = null

    override fun getLayoutRes() = R.layout.activity_purchase_history

    override fun useHamburgerMenu() = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setNavigationViewCheckedItem(Action.SETTINGS)
        title = intent.extras?.getString(AppConstants.TITLE) ?: ""
        setToolbarTitle(title)
    }
}