package co.bytemark.purchase_history

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.Api.ApiUtility
import co.bytemark.sdk.Utils
import co.bytemark.sdk.model.common.Order
import co.bytemark.sdk.model.common.OrderItem
import co.bytemark.shopping_cart.TotalDetails
import co.bytemark.shopping_cart.TotalDetailsItemView
import kotlinx.android.synthetic.main.order_history_footer_item.view.*
import kotlinx.android.synthetic.main.order_history_item.view.*
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

/** Created by Santosh on 19/04/19.
 */

class OrderHistoryAdapter(
        private val confHelper: ConfHelper,
        private val clickListener: (Order) -> Unit,
        private val loadMoreListener: () -> Unit
) : ListAdapter<Order, RecyclerView.ViewHolder>(OrderDiffCallback()) {

    var buttonVisible: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == itemView) {
            LayoutInflater.from(parent.context)
                    .inflate(R.layout.order_history_item, parent, false)
                    .run { OrderHistoryViewHolder(this) }
        } else {
            LayoutInflater.from(parent.context)
                    .inflate(R.layout.order_history_footer_item, parent, false)
                    .run { OrderHistoryFooterViewHolder(this) }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is OrderHistoryViewHolder -> holder.bind(getItem(position))

            is OrderHistoryFooterViewHolder -> holder.bind()
        }
    }

    inner class OrderHistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(order: Order) {
            val date = ApiUtility.getCalendarFromS(order.timePurchased)?.time
            itemView.apply {
                date?.let {
                    dateTimeTextView.text = confHelper.dateDisplayFormat.format(it).plus(" ")
                        .plus(confHelper.timeDisplayFormat.format(it)).plus(" ")
                        .plus(Utils.localTimeZone)
                }

                Timber.tag("TimeZoneLogging").d(Utils.localTimeZone)

                totalTextView.text =
                        confHelper.getConfigurationPurchaseOptionsCurrencySymbol(order.total).toString()

                setOnClickListener { clickListener(order) }

                ticketsView.apply {
                    ticketsView.removeAllViews()
                    order.orderItems?.let { orderItemList ->
                        getConsolidatedTickets(orderItemList).forEach {
                            ticketsView.addView(
                                    TotalDetailsItemView(
                                            context, TotalDetails.Builder()
                                            .setTitle(it.name)
                                            .setQuantity(it.quantity.toString() + "x")
                                            .setOriginDestination(it.OriginDest)
                                            .setTextColor(confHelper.collectionThemePrimaryTextColor)
                                            .build()
                                    )
                            )
                        }
                    }
                }

                if (order.paymentStatus == AppConstants.PAYMENT_STATUS_PENDING) {
                    textViewOrderStatus?.visibility = View.VISIBLE
                } else {
                    textViewOrderStatus?.visibility = View.GONE
                }
            }
        }
    }

    inner class OrderHistoryFooterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind() {
            itemView.apply {
                if (!buttonVisible) buttonLoadMore.visibility = View.GONE
                else buttonLoadMore.visibility = View.VISIBLE
                buttonLoadMore.setOnClickListener { loadMoreListener() }
            }
        }
    }

    class OrderDiffCallback : DiffUtil.ItemCallback<Order>() {
        override fun areItemsTheSame(oldItem: Order, newItem: Order): Boolean {
            return oldItem.uuid == newItem.uuid
        }

        override fun areContentsTheSame(oldItem: Order, newItem: Order): Boolean {
            return oldItem.uuid == newItem.uuid
        }
    }

    private fun getConsolidatedTickets(orderItems: List<OrderItem>): MutableList<ConsolidatedTicket> {
        return HashMap<String, ConsolidatedTicket>().run {
            orderItems.forEach {
                val key = getKeyForOrderItem(it)
                if (it.total >= 0) // to omit refund entries
                    if (containsKey(key)) {
                        val consolidatedTicket = get(key)
                        consolidatedTicket?.quantity = consolidatedTicket?.quantity!!.plus(1)
                        put(key, consolidatedTicket)
                    } else {
                        put(key, ConsolidatedTicket(1, it.translatedLabelName, getOD(it)))
                    }
            }
            mutableListOf<ConsolidatedTicket>().apply { addAll(values) }
        }
    }

    private fun getOD(orderItem: OrderItem): String? {
        return if (getOrigin(orderItem) != null && getDestination(orderItem) != null) {
            getOrigin(orderItem).plus("/").plus(getDestination(orderItem))
        } else null
    }

    private fun getKeyForOrderItem(orderItem: OrderItem): String {
        return orderItem.product?.uuid.plus(orderItem.name).plus(getOrigin(orderItem))
                .plus(getDestination(orderItem))
    }

    private fun getOrigin(orderItem: OrderItem): String? {
        return when {
            orderItem.filterAttributes != null -> {
                val itinerary = orderItem.filterAttributes?.itinerary
                if (confHelper.isDisplayLongNameForOd(orderItem.product?.organizationUuid)) {
                    itinerary?.origin?.longName?.get(Locale.getDefault().language)
                } else {
                    itinerary?.origin?.shortName?.get(Locale.getDefault().language)
                }
            }

            orderItem.attributes != null && orderItem.attributes.size > 0 -> {
                getValueFromAttributes(orderItem, "origin_name")
            }
            else -> {
                null
            }
        }
    }

    private fun getValueFromAttributes(orderItem: OrderItem, key: String): String? {
        orderItem.attributes?.forEach {
            if (it.key == key) {
                return it.value
            }
        }
        return null
    }

    private fun getDestination(orderItem: OrderItem): String? {
        return when {
            orderItem.filterAttributes != null -> {
                val itinerary = orderItem.filterAttributes?.itinerary
                if (confHelper.isDisplayLongNameForOd(orderItem.product?.organizationUuid)) {
                    itinerary?.destination?.longName?.get(Locale.getDefault().language)
                } else {
                    itinerary?.destination?.shortName?.get(Locale.getDefault().language)
                }
            }

            orderItem.attributes != null && orderItem.attributes.size > 0 -> {
                getValueFromAttributes(orderItem, "destination_name")
            }
            else -> {
                null
            }
        }
    }

    override fun submitList(list: MutableList<Order>?) {
        super.submitList(list?.run { ArrayList(list) })
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1) footerView else itemView
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + 1
    }

    companion object {
        const val itemView = 0
        const val footerView = 1
    }

    private data class ConsolidatedTicket(
            var quantity: Int,
            val name: String?,
            val OriginDest: String?
    )
}