package co.bytemark.mvp



interface MvpPresenter<V : MvpView?> {
    /**
     * Set or attach the view to this presenter
     */
    fun attachView(view: V)

    /**
     * Will be called if the view has been destroyed. Typically this method will be invoked from
     * `Activity.detachView()` or `Fragment.onDestroyView()`
     */
    fun detachView(retainInstance: Boolean)
}