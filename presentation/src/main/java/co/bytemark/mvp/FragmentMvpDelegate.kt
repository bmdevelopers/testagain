package co.bytemark.mvp

import android.app.Activity
import android.os.Bundle
import android.view.View

interface FragmentMvpDelegate<V : MvpView?, P : MvpPresenter<V>?> {
    /**
     * Must be called from [Fragment.onCreate]
     *
     * @param saved The bundle
     */
    fun onCreate(saved: Bundle?)

    /**
     * Must be called from [Fragment.onDestroy]
     */
    fun onDestroy()

    /**
     * Must be called from [Fragment.onViewCreated]
     *
     * @param view The inflated view
     * @param savedInstanceState the bundle with the viewstate
     */
    fun onViewCreated(view: View?, savedInstanceState: Bundle?)

    /**
     * Must be called from [Fragment.onDestroyView]
     */
    fun onDestroyView()

    /**
     * Must be called from [Fragment.onPause]
     */
    fun onPause()

    /**
     * Must be called from [Fragment.onResume]
     */
    fun onResume()

    /**
     * Must be called from [Fragment.onStart]
     */
    fun onStart()

    /**
     * Must be called from [Fragment.onStop]
     */
    fun onStop()

    /**
     * Must be called from [Fragment.onActivityCreated]
     *
     * @param savedInstanceState The saved bundle
     */
    fun onActivityCreated(savedInstanceState: Bundle?)

    /**
     * Must be called from [Fragment.onAttach]
     *
     * @param activity The activity the fragment is attached to
     */
    fun onAttach(activity: Activity?)

    /**
     * Must be called from [Fragment.onDetach]
     */
    fun onDetach()

    /**
     * Must be called from [Fragment.onSaveInstanceState]
     */
    fun onSaveInstanceState(outState: Bundle?)
}