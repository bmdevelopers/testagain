package co.bytemark.mvp

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment

abstract class MvpFragment<V : MvpView?, P : MvpPresenter<V>?>(@JvmField override var presenter: P? = null) : Fragment(), BaseMvpDelegateCallback<V, P>, MvpView {

    protected var mvpDelegate: FragmentMvpDelegate<V, P>? = null
    abstract override fun createPresenter(): P

    @JvmName("getDelegate")
    protected fun getMvpDelegate(): FragmentMvpDelegate<V, P> {
        if (mvpDelegate == null) {
            mvpDelegate = FragmentMvpDelegateImpl(this)
        }
        return mvpDelegate as FragmentMvpDelegate<V, P>
    }

    override fun isRetainInstance(): Boolean {
        return retainInstance
    }

    override fun shouldInstanceBeRetained(): Boolean {
        val activity = activity
        val changingConfig = activity != null && activity.isChangingConfigurations
        return retainInstance && changingConfig
    }

    override var mvpView: V
        get() = this as V
        set(mvpView) {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getMvpDelegate().onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        getMvpDelegate().onDestroyView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getMvpDelegate().onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        getMvpDelegate().onDestroy()
    }

    override fun onPause() {
        super.onPause()
        getMvpDelegate().onPause()
    }

    override fun onResume() {
        super.onResume()
        getMvpDelegate().onResume()
    }

    override fun onStart() {
        super.onStart()
        getMvpDelegate().onStart()
    }

    override fun onStop() {
        super.onStop()
        getMvpDelegate().onStop()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getMvpDelegate().onActivityCreated(savedInstanceState)
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        getMvpDelegate().onAttach(activity)
    }

    override fun onDetach() {
        super.onDetach()
        getMvpDelegate().onDetach()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        getMvpDelegate().onSaveInstanceState(outState)
    }
}