package co.bytemark.mvp

import android.app.Activity
import android.os.Bundle
import android.view.View


open class FragmentMvpDelegateImpl<V : MvpView?, P : MvpPresenter<V>?>(delegateCallback: BaseMvpDelegateCallback<V, P>?) : FragmentMvpDelegate<V, P> {
    protected var delegateCallback: BaseMvpDelegateCallback<V, P>? = null
    protected var internalDelegate: MvpInternalDelegate<V, P>? = null
    private var onViewCreatedCalled = false

    init {
        if (delegateCallback == null) {
            throw NullPointerException("MvpDelegateCallback is null!")
        }
        this.delegateCallback = delegateCallback
    }

    protected fun getMvpInternalDelegate(): MvpInternalDelegate<V, P>? {
        if (internalDelegate == null) {
            internalDelegate = MvpInternalDelegate(delegateCallback)
        }
        return internalDelegate
    }
    override fun onCreate(saved: Bundle?) {}

    override fun onDestroy() {}

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        getMvpInternalDelegate()?.createPresenter()
        getMvpInternalDelegate()?.attachView()
        onViewCreatedCalled = true
    }

    override fun onDestroyView() {
        getMvpInternalDelegate()?.detachView()
    }

    override fun onPause() {}

    override fun onResume() {}

    override fun onStart() {
        check(onViewCreatedCalled) { "It seems that you are using " + delegateCallback!!.javaClass.canonicalName + " as headless (UI less) fragment (because onViewCreated() has not been called or maybe delegation misses that part). Having a Presenter without a View (UI) doesn't make sense. Simply use an usual fragment instead of an MvpFragment if you want to use a UI less Fragment" }
    }

    override fun onStop() {}

    override fun onActivityCreated(savedInstanceState: Bundle?) {}

    override fun onAttach(activity: Activity?) {}

    override fun onDetach() {}

    override fun onSaveInstanceState(outState: Bundle?) {}
}