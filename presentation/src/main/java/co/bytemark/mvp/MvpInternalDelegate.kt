package co.bytemark.mvp


open class MvpInternalDelegate<V : MvpView?, P : MvpPresenter<V>?>(delegateCallback: BaseMvpDelegateCallback<V, P>?) {
    protected var delegateCallback: BaseMvpDelegateCallback<V, P>

    /**
     * Called  to create the presenter (if no other one already exisits)
     */
    fun createPresenter() {
        var presenter = delegateCallback.presenter
        if (presenter == null) {
            presenter = delegateCallback.createPresenter()
        }
        if (presenter == null) {
            throw NullPointerException("Presenter is null! Do you return null in createPresenter()?")
        }
        delegateCallback.presenter = presenter
    }

    /**
     * Attaches the view to the presenter
     */
    fun attachView() {
        presenter?.attachView(delegateCallback.mvpView)
    }

    /**
     * Called to detach the view from presenter
     */
    fun detachView() {
        presenter?.detachView(delegateCallback.shouldInstanceBeRetained())
    }

    private val presenter: P
        private get() = delegateCallback.presenter
                ?: throw NullPointerException("Presenter returned from getPresenter() is null")

    init {
        if (delegateCallback == null) {
            throw NullPointerException("MvpDelegateCallback is null!")
        }
        this.delegateCallback = delegateCallback
    }
}