package co.bytemark.mvp

import android.app.Activity


interface BaseMvpDelegateCallback<V : MvpView?, P : MvpPresenter<V>?> {
    /**
     * Creates the presenter instance
     *
     * @return the created presenter instance
     */
    fun createPresenter(): P

    /**
     * Get the presenter. If null is returned, then a internally a new presenter instance gets
     * created
     * by calling [.createPresenter]
     *
     * @return the presenter instance. can be null.
     */
    /**
     * Sets the presenter instance
     *
     * @param presenter The presenter instance
     */
    var presenter: P?

    /**
     * Get the MvpView for the presenter
     *
     * @return The view associated with the presenter
     */
    var mvpView: V
    /**
     * Indicate whether the retain instance feature is enabled by this view or not
     *
     * @return true if the view has  enabled retaining, otherwise false.
     * @see .setRetainInstance
     */
    fun isRetainInstance(): Boolean

    /**
     * Mark this instance as retaining. This means that the feature of a retaining instance is
     * enabled.
     *
     * @param retainingInstance true if retaining instance feature is enabled, otherwise false
     * @see .isRetainInstance
     */
    fun setRetainInstance(retainingInstance: Boolean)

    /**
     * Indicates whether or not the the view will be retained during next screen orientation change.
     * This boolean flag is used for [MvpPresenter.detachView]
     * as parameter. Usually you should take [Activity.isChangingConfigurations] into
     * account. The difference between [.shouldInstanceBeRetained] and [ ][.isRetainInstance] is that [.isRetainInstance] indicates that retain instance
     * feature is enabled or disabled while [.shouldInstanceBeRetained] indicates if the
     * view is going to be destroyed permanently and hence should no more be retained (i.e. Activity
     * is finishing and not just screen orientation changing)
     *
     * @return true if the instance should be retained, otherwise false
     */
    fun shouldInstanceBeRetained(): Boolean
}
