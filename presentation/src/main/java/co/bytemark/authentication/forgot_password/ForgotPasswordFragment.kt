package co.bytemark.authentication.forgot_password

import android.os.Bundle
import android.transition.TransitionManager
import android.util.Pair
import android.view.*
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.authentication.AuthenticationActivity
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.formly.adapter.FormlyAdapter
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.transition.SuperAutoTransition
import co.bytemark.widgets.util.Util
import co.bytemark.widgets.util.getViewModel
import co.bytemark.widgets.util.hide
import co.bytemark.widgets.util.show
import kotlinx.android.synthetic.main.fragment_forgot_password.*
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject

class ForgotPasswordFragment : BaseMvvmFragment() {

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    override fun onInject() = CustomerMobileApp.component.inject(this)

    @Inject
    lateinit var viewModel: ForgotPasswordViewModel

    private val formlyMap = mutableMapOf<String, String>()

    private lateinit var formlyAdapter: FormlyAdapter

    private val subs = CompositeSubscription()

    companion object {
        @JvmStatic
        fun newInstance() = ForgotPasswordFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postponeEnterTransition()
        (activity as AuthenticationActivity).let {
            it.supportActionBar?.title = getString(R.string.forgot_password_titile)
            it.enableOrDisableDrawerHack(true)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_forgot_password, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = getViewModel(CustomerMobileApp.appComponent.forgotPasswordViewModel)

        formlyAdapter = FormlyAdapter(forgotPasswordList, fragmentManager)

        subs.add(formlyAdapter.buttonClicks().subscribe { formly: Formly ->
            onButtonClicked(formly)
        })
        subs.add(formlyAdapter.textChanges().subscribe { textChanges: Pair<Formly, String> ->
            updateFormlyModel(textChanges)
        })

        forgotPasswordCheckmark.setMaterialWidth(Util.dpToPx(2.0).toFloat())
        forgotPasswordList.adapter = formlyAdapter

        viewModel.getFormData()

        with(viewModel) {

            formData.observe(this@ForgotPasswordFragment, Observer {
                it?.let {
                    formlyAdapter.setFormlyList(it)
                    forgotPasswordList.post { startPostponedEnterTransition() }
                }
            })

            errorLiveData.observe(this@ForgotPasswordFragment, Observer {
                it?.let {
                    handleError(it)
                    analyticsPlatformAdapter.forgotPassword(AnalyticsPlatformsContract.Status.FAILURE, it.message)
                }
            })

            isResetPasswordSuccessful.observe(this@ForgotPasswordFragment, Observer {
                if(it) onResetPasswordSuccess()
            })

            isLoading.observe(this@ForgotPasswordFragment, Observer {
                if(it) {
                    showLoading()
                } else {
                    hideLoading()
                }
            })
        }
    }


    private fun onButtonClicked(formly: Formly) {
        hideKeyboard()
        when (formly.key) {
            Formly.RESET_PASSWORD_KEY -> {
                formlyAdapter.enableValidation()
                if (formlyAdapter.isValid()) {
                    if (isOnline()) {
                        resetPassword()
                    } else {
                        connectionErrorDialog()
                    }
                }
            }
        }
    }

    private fun updateFormlyModel(value: Pair<Formly, String>) {
        formlyMap[AppConstants.CLIENT_ID] = confHelper.clientId() ?: "";
        formlyMap[value.first.key] = value.second.trim()
    }

    private fun resetPassword() {
        viewModel.resetPassword(formlyMap)
    }

    private fun showLoading() {
        hideKeyboard()
        formlyAdapter.setLoading(Formly.RESET_PASSWORD_KEY, true)
    }

    private fun hideLoading() {
        formlyAdapter.setLoading(Formly.RESET_PASSWORD_KEY, false)
    }

    private fun onResetPasswordSuccess() {
        analyticsPlatformAdapter.forgotPassword(AnalyticsPlatformsContract.Status.SUCCESS, "")
        val hideContent = SuperAutoTransition()
        hideContent.duration = 300
        hideContent.interpolator = LinearOutSlowInInterpolator()
        hideContent.doOnEnd( Runnable{
            val superAutoTransition = SuperAutoTransition()
            superAutoTransition.duration = 300
            superAutoTransition.doOnEnd (Runnable{
                forgotPasswordCheckmark.isSelected = true
                forgotPasswordCheckmark.postDelayed({
                    if (isAdded) {
                        forgotPasswordButtonOk.visibility = View.VISIBLE
                        forgotPasswordButtonOk.animate().alpha(1f).duration = 300
                        forgotPasswordButtonOk.setOnClickListener(View.OnClickListener {
                            if (activity?.isFinishing == false) {
                                activity?.onBackPressed()
                            }
                        })
                    }
                }, 400)
            })
            TransitionManager.beginDelayedTransition(forgotPasswordRootLayout, superAutoTransition)
            view?.announceForAccessibility(forgotPasswordSuccess.text)
            forgotPasswordSuccess.show()
            forgotPasswordCheckmark.show()
        })
        TransitionManager.beginDelayedTransition(forgotPasswordRootLayout, hideContent)
        forgotPasswordList.hide()
        forgotPasswordListContainer.gravity = Gravity.CENTER
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val item = menu.findItem(R.id.action_notifications)
        item?.isVisible = false
    }

    override fun showAppUpdateDialog() {
        hideLoading()
        super.showAppUpdateDialog()
    }

    override fun setUpColors() {
        forgotPasswordCheckmark.setCheckMarkColor(confHelper.backgroundThemeAccentColor)
    }
}