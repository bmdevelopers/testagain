package co.bytemark.authentication.forgot_password

import androidx.lifecycle.MutableLiveData
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.authentication.ResetPasswordUseCase
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.common.Result
import co.bytemark.helpers.ConfHelper
import kotlinx.coroutines.launch
import javax.inject.Inject

class ForgotPasswordViewModel @Inject constructor(
        private val resetPasswordUseCase: ResetPasswordUseCase,
        private val confHelper: ConfHelper
) : BaseViewModel() {

    var isResetPasswordSuccessful = MutableLiveData<Boolean>().apply {
        value = false
    }

    val formData by lazy {
        MutableLiveData<MutableList<Formly>>()
    }

    fun getFormData() {
        formData.value = confHelper.forgotPasswordForms
    }

    var isLoading = MutableLiveData<Boolean>().apply {
        value = false
    }

    fun resetPassword(params: MutableMap<String, String>) {
        uiScope.launch {
            if(isLoading.value == false) {
                isLoading.value = true
                val resultValues = resetPasswordUseCase.ResetPasswordRequestValues(params)
                when (val result = resetPasswordUseCase(resultValues)) {
                    is Result.Failure -> {
                        errorLiveData.value = result.bmError.first()
                        isLoading.value = false
                    }

                    else -> {
                        isResetPasswordSuccessful.value = true
                        isLoading.value = false
                    }
                }
            }
        }
    }
}