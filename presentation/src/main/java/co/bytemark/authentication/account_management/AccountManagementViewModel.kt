package co.bytemark.authentication.account_management

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Log
import co.bytemark.domain.interactor.authentication.GetUser
import co.bytemark.domain.interactor.authentication.UpdateUser
import co.bytemark.domain.interactor.authentication.UpdateUserRequestValues
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.helpers.ConfHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject


/**
 * Created by Ranjith on 23/04/20.
 */
class AccountManagementViewModel @Inject constructor(
        val confHelper: ConfHelper,
        private val updateUser: UpdateUser,
        private val getUser: GetUser
) : ViewModel() {

    fun getUser() = getUser.getLiveData(null)

    fun updateUser(updateUserRequestValues: UpdateUserRequestValues) =
            updateUser.getLiveData(updateUserRequestValues)

    suspend fun getFormly(): MutableList<Formly> {
        return withContext(Dispatchers.IO) {
            return@withContext confHelper.accountManagementForms
        }
    }
    

}