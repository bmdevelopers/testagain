package co.bytemark.authentication.account_management

import android.os.Bundle
import android.transition.TransitionManager
import android.util.Pair
import android.view.*
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.authentication.AuthenticationActivity
import co.bytemark.authentication.delete_account.DeleteAccountFragment
import co.bytemark.authentication.delete_account.InitDeleteAccountFragment
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.interactor.authentication.UpdateUserRequestValues
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.common.api_response_data.UserProfileData
import co.bytemark.formly.adapter.FormlyAdapter
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.model.common.User
import co.bytemark.widgets.transition.SuperAutoTransition
import co.bytemark.widgets.util.Util
import co.bytemark.widgets.util.replaceFragment
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.fragment_account_management.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.subscriptions.CompositeSubscription
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Ranjith on 23/04/20.
 */
class AccountManagementFragment : BaseMvvmFragment() {

    companion object {
        const val EXTRA_TAG = "ACCOUNT_MANAGEMENT"
        fun newInstance(): AccountManagementFragment {
            return AccountManagementFragment()
        }
    }

    @Inject
    lateinit var accountManagementViewModel: AccountManagementViewModel

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    private val formlyMap = mutableMapOf<String, String>()

    // holding user object to check if the email field is actually changed or not,
    private var user: User? = null

    private var enableEmailVerification: Boolean? = false

    private var emailAddressChanged = false

    private lateinit var formlyAdapter: FormlyAdapter

    private val subs = CompositeSubscription()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        postponeEnterTransition()
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.clear()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_account_management, container, false)
        (activity as AuthenticationActivity).let {
            it.supportActionBar?.title = getString(R.string.settings_your_profile)
            it.enableOrDisableDrawerHack(true)
        }
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        accountManagementList.layoutManager = LinearLayoutManager(context)
        formlyAdapter = FormlyAdapter(accountManagementList, fragmentManager)

        subs.add(
            formlyAdapter.buttonClicks().subscribe { formly: Formly ->
                onButtonClicked(formly)
            })

        subs.add(
            formlyAdapter.textChanges().subscribe { textChange: Pair<Formly, String> ->
                if (enableEmailVerification == true) {
                    emailAddressChanged = textChange.first.key == Formly.EMAIL_KEY
                            && user?.email != textChange.second.trim { it <= ' ' }
                }
                updateFormlyModel(textChange)
            })
        subs.add(
            formlyAdapter.dropdownSelects().subscribe { value: Pair<Formly, String> ->
                updateFormlyModel(value)
            })
        checkmark.setMaterialWidth(Util.dpToPx(2.0).toFloat())

        accountManagementList.adapter = formlyAdapter

        GlobalScope.launch(Dispatchers.Main) {
            loadFormly()
        }

        checkmark.setCheckMarkColor(confHelper.backgroundThemeAccentColor)
    }

    private fun updateFormlyModel(value: Pair<Formly, String>) {
        formlyMap[value.first.key] = value.second.trim()
    }

    private suspend fun loadFormly() {
        val formly = accountManagementViewModel.getFormly()
        formlyAdapter.setFormlyList(formly)
        accountManagementList?.post { startPostponedEnterTransition() }
        accountManagementViewModel.getUser().observe(this, Observer { onGetUserResult(it) })
    }

    private fun onGetUserResult(result: Result<UserProfileData>?) {
        hideLoading()
        result?.let {
            Timber.tag("AccountManagement").d("OnGetUserResult : " + Thread.currentThread().name)
            when (it) {
                is Result.Loading -> {
                    showLoading()
                }
                is Result.Success -> {
                    updateUserDetails(it.data)
                    analyticsPlatformAdapter.profileLoadingCompleted(
                        AnalyticsPlatformsContract.Status.SUCCESS,
                        ""
                    )
                }
                is Result.Failure -> {
                    val error = it.bmError.first()
                    handleError(error)
                    analyticsPlatformAdapter.profileLoadingCompleted(
                        AnalyticsPlatformsContract.Status.FAILURE,
                        error.message
                    )
                }
            }
        }
    }

    private fun onButtonClicked(formly: Formly) {
        hideKeyboard()
        when (formly.key) {
            Formly.UPDATE_KEY -> {
                formlyAdapter.enableValidation()
                if (formlyAdapter.isValid()) {
                    if (isOnline()) {
                        if (emailAddressChanged) {
                            showVerifyEmailDialog()
                        } else {
                            updateUser()
                        }
                    } else {
                        connectionErrorDialog()
                    }
                }
            }
            Formly.DELETE_KEY -> {
                (activity as AuthenticationActivity).replaceFragment(
                    DeleteAccountFragment.newInstance(),
                    R.id.fragment,
                    true
                )
            }

            Formly.INIT_DELETE_KEY -> {
                (activity as AuthenticationActivity).replaceFragment(
                    InitDeleteAccountFragment.newInstance(),
                    R.id.fragment,
                    true
                )
            }
        }
    }

    private fun updateUser() {
        accountManagementViewModel.updateUser(UpdateUserRequestValues(formlyMap))
            .observe(viewLifecycleOwner, Observer { onUpdateUserResult(it) })
    }


    private fun showLoading() {
        hideKeyboard()
        formlyAdapter.setLoading(Formly.UPDATE_KEY, true)
    }

    private fun hideLoading() {
        formlyAdapter.setLoading(Formly.UPDATE_KEY, false)
    }

    override fun onInject() {
        component.inject(this)
    }

    private fun onUpdateUserResult(result: Result<UserProfileData>?) {
        hideLoading()
        result?.let {
            when (it) {
                is Result.Loading -> {
                    showLoading()
                }
                is Result.Success -> {
                    if(confHelper.isElertSdkEnabled()) {
                        Util.userUpdate(activity?.application, it.data?.user)
                    }
                    updateUserSuccessful()
                    analyticsPlatformAdapter.profileUpdated(
                        AnalyticsPlatformsContract.Status.SUCCESS,
                        ""
                    )
                }
                is Result.Failure -> {
                    val error = it.bmError.first();
                    handleError(error)
                    analyticsPlatformAdapter.profileUpdated(
                        AnalyticsPlatformsContract.Status.FAILURE,
                        error.message
                    )
                }
            }
        }
    }

    private fun updateUserSuccessful() {
        val hideContent = SuperAutoTransition()
        hideContent.duration = 300
        hideContent.interpolator = LinearOutSlowInInterpolator()
        hideContent.doOnEnd(Runnable {
            val superAutoTransition = SuperAutoTransition()
            superAutoTransition.duration = 300
            superAutoTransition.doOnEnd(Runnable {
                checkmark.isSelected = true
                checkmark.postDelayed({
                                          if (activity != null) {
                                              if (emailAddressChanged) {
                                                  BytemarkSDK.logout(context)
                                              }
                                              if (!activity!!.isFinishing) {
                                                  activity?.finish()
                                              }
                                          }
                                      }, 400)
            })
            TransitionManager.beginDelayedTransition(rootLayout, superAutoTransition)
            if (view != null) {
                view!!.announceForAccessibility(accountManagementSuccess.text)
            }
            accountManagementSuccess.visibility = View.VISIBLE
            checkmark.visibility = View.VISIBLE
        })
        TransitionManager.beginDelayedTransition(rootLayout, hideContent)
        accountManagementList.visibility = View.GONE
        listContainer.gravity = Gravity.CENTER
    }

    private fun showVerifyEmailDialog() {
        MaterialDialog.Builder(activity!!)
            .title(R.string.email_verification_required)
            .positiveText(android.R.string.ok)
            .negativeText(R.string.popup_cancel)
            .content(R.string.msg_email_verification_profile_change)
            .cancelable(false)
            .onPositive { dialog: MaterialDialog, _: DialogAction? ->
                dialog.dismiss()
                updateUser()
            }
            .onNegative { dialog: MaterialDialog, _: DialogAction? ->
                dialog.dismiss()
            }
            .show()
    }

    private fun updateUserDetails(userProfileData: UserProfileData?) {
        userProfileData?.let {
            it.organizationDetails?.requiresEmailVerification?.let { requiresEmailVerification ->
                enableEmailVerification = requiresEmailVerification
            }
            this.user = userProfileData.user
            user?.let { user ->
                formlyAdapter.setValue(Formly.FIRST_NAME_KEY, user.firstName)
                formlyAdapter.setValue(Formly.LAST_NAME_KEY, user.lastName)
                formlyAdapter.setValue(Formly.EMAIL_KEY, user.email)
                formlyAdapter.setValue(Formly.USERNAME_KEY, user.username)
                formlyAdapter.setValue(Formly.PHONE_KEY, user.mobile)
                formlyAdapter.setValue(Formly.MOBILE_KEY, user.mobile)

                user.addresses?.let { addressList ->
                    for ((addressType, _, addressLine1, addressLine2,
                        postalCode, city, state, country) in addressList) {
                        if (addressType!!.contains(Formly.SHIPPING)) {
                            formlyAdapter.setValue(Formly.ADDRESS_LINE_1, addressLine1 ?: "")
                            formlyAdapter.setValue(Formly.ADDRESS_LINE_2, addressLine2 ?: "")
                            formlyAdapter.setValue(Formly.POSTAL_CODE, postalCode ?: "")
                            formlyAdapter.setValue(Formly.CITY, city ?: "")
                            formlyAdapter.setValue(Formly.STATE, state ?: "")
                            formlyAdapter.setValue(Formly.COUNTRY, country ?: "")
                        }
                    }
                }
            }
        }
    }
}