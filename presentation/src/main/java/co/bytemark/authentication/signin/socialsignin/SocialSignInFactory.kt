package co.bytemark.authentication.signin.socialsignin

import co.bytemark.helpers.AppConstants

object SocialSignInFactory {
    fun build(signInType: String?): SocialSignIn? {
        return when (signInType) {
            AppConstants.LOGIN_TYPE_FACEBOOK -> return FacebookSignIn(AppConstants.LOGIN_TYPE_FACEBOOK)
            AppConstants.LOGIN_TYPE_GOOGLE -> return GoogleSignIn(AppConstants.LOGIN_TYPE_GOOGLE)
            AppConstants.LOGIN_TYPE_APPLE -> return AppleSignIn(AppConstants.LOGIN_TYPE_APPLE)
            else -> null
        }
    }
}