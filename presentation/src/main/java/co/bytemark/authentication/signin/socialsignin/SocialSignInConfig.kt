package co.bytemark.authentication.signin.socialsignin

import androidx.fragment.app.Fragment

data class SocialSignInConfig(
    var activity: Fragment? = null,
    var callback: SocialSignInCallbacks? = null
)