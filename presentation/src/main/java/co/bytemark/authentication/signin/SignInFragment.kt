package co.bytemark.authentication.signin

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.transition.Fade
import android.transition.TransitionManager.beginDelayedTransition
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.CustomerMobileApp.Companion.getConf
import co.bytemark.CustomerMobileApp.Companion.getIsRatingPopUPRequired
import co.bytemark.CustomerMobileApp.Companion.isRatingPopUpRequired
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.authentication.AuthenticationActivity
import co.bytemark.authentication.forgot_password.ForgotPasswordFragment
import co.bytemark.authentication.sign_up.SignUpFragment
import co.bytemark.authentication.signin.binding.SignInBindingAdapter
import co.bytemark.authentication.signin.binding.SignInBindingConfig
import co.bytemark.authentication.signin.socialsignin.SocialSignIn
import co.bytemark.authentication.signin.socialsignin.SocialSignInCallbacks
import co.bytemark.authentication.signin.socialsignin.SocialSignInConfig
import co.bytemark.authentication.signin.socialsignin.SocialSignInFactory
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.databinding.FragmentSignInBinding
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.RatingAndFeedBackHelper
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.model.common.User
import co.bytemark.use_tickets.UseTicketsActivity
import co.bytemark.widgets.transition.MoveTransition
import co.bytemark.widgets.util.*
import kotlinx.android.synthetic.main.fragment_sign_in.*
import javax.inject.Inject

class SignInFragment : BaseMvvmFragment(), SocialSignInCallbacks {

    private lateinit var binding: FragmentSignInBinding
    private lateinit var signInBindingAdapter: SignInBindingAdapter
    private lateinit var viewModel: SignInViewModel
    private var config: SocialSignInConfig? = null
    private var socialSignIn: SocialSignIn? = null
    private var signInType: String = AnalyticsPlatformsContract.SignInType.NATIVE
    private var originScreen: String? = null

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    companion object {
        fun newInstance(origin: String?) = SignInFragment().withArgs {
            putString(AppConstants.EXTRA_ORIGIN, origin)
        }
    }

    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSignInBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(CustomerMobileApp.appComponent.signInViewModel)
        viewModel.logAccessTime()
        if (getIsRatingPopUPRequired() == true) {
            activity?.let { RatingAndFeedBackHelper(it).showRatingPopUP() }
            isRatingPopUpRequired = false
        }
        originScreen = arguments?.getString(
                AppConstants.EXTRA_ORIGIN,
                AnalyticsPlatformsContract.Screen.SETTINGS
        )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        hideKeyboard()
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadNotification()
    }

    override fun onPause() {
        super.onPause()
        viewModel.clearLiveData()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setBindingAdapterData()
        analyticsPlatformAdapter.signInScreenDisplayed()
        setSocialConfig()
        observeLiveData()
        handleViewClick()
        setupAccessibility()
    }

    private fun setBindingAdapterData() {
        signInBindingAdapter = SignInBindingAdapter()
        binding.model = signInBindingAdapter
        binding.config = getSignInBindingConfig()
        binding.lifecycleOwner = viewLifecycleOwner
    }

    @VisibleForTesting
    fun getSignInBindingConfig(): SignInBindingConfig {
        return SignInBindingConfig(
                confHelper.isSignInLogoVisible,
                getConf()?.clients?.customerMobile?.socialSignInList
        )
    }

    private fun setSocialConfig() {
        config = SocialSignInConfig()
        config?.activity = this
        config?.callback = this
    }

    private fun observeLiveData() {
        viewModel.unreadNotificationLiveData.observe(this, Observer {
            (activity as AuthenticationActivity?)?.onUnreadNotificationsCountChanged(it)
        })
        viewModel.resendVerificationLiveData.observe(this, Observer { message ->
            context?.let {
                it.showMaterialDialog(
                        title = R.string.resend_verification_email,
                        body = message,
                        positiveButton = android.R.string.ok,
                        positiveAction = { dialog, _ ->
                            dialog.dismiss()
                            BytemarkSDK.logout(context)
                        }
                )
            }
        })
        viewModel.errorLiveData.observe(this, Observer {
            it?.let {
                it1 -> handleError(it1)
                socialSignIn?.signOut()
            }
        })
        viewModel.signInSuccessLiveData.observe(this, Observer {
            analyticsPlatformAdapter.signIn(
                    originScreen,
                    signInType,
                    AnalyticsPlatformsContract.Status.SUCCESS,
                    ""
            )
            onSuccessSignIn(it)
        })
        viewModel.emailConfirmationLiveData.observe(this, Observer {
            context?.let {
                it.showMaterialDialog(
                        title = R.string.sign_in_cant_login,
                        body = R.string.sign_in_email_verification_required,
                        negativeButton = R.string.resend_verification_email,
                        negativeAction = { _, _ ->
                            viewModel.resendVerificationEmail()
                        },
                        positiveButton = android.R.string.ok,
                        positiveAction = { dialog, _ ->
                            dialog.dismiss()
                            BytemarkSDK.logout(context)
                        }
                )
            }
        })
        viewModel.invalidUserNamePasswordLiveData.observe(this, Observer {
            if(it) {
                showInvalidCredentialDialog()
            }
        })
        viewModel.loadingLiveData.observe(this, Observer {
            if (it) {
                progressViewSignIn.announceForAccessibility(getString(R.string.loading))
            }
            makeAllButtonEnableDisable(it)
            hideKeyboard()
            beginDelayedTransition(signInRootLayout)
            progressViewSignIn.isVisible = it
        })

        signInBindingAdapter.getSignInFields().observe(this, Observer {
            signInType = AnalyticsPlatformsContract.SignInType.NATIVE
            viewModel.getOauthToken(null, it.email, it.password, null, null)
        })
    }

    private fun handleViewClick() {
        signUpTextView.setOnClickListener {
            (context as AppCompatActivity).addFragment(
                    applyAnimationToFragment(SignUpFragment.newInstance()),
                    R.id.fragment,
                    true
            )
        }
        forgotPasswordTextView.setOnClickListener {
            (context as AppCompatActivity).addFragment(
                    applyAnimationToFragment(
                            ForgotPasswordFragment.newInstance()
                    ), R.id.fragment, true
            )
        }
        googleSignInButton.setOnClickListener {
            viewModel.setProgressBarVisibility(true)
            signInType = AnalyticsPlatformsContract.SignInType.GOOGLE
            socialSignIn = SocialSignInFactory.build(AppConstants.LOGIN_TYPE_GOOGLE)
            socialSignIn?.signIn(config)
        }
        facebookSignInButton.setOnClickListener {
            viewModel.setProgressBarVisibility(true)
            signInType = AnalyticsPlatformsContract.SignInType.FACEBOOK
            socialSignIn = SocialSignInFactory.build(AppConstants.LOGIN_TYPE_FACEBOOK)
            socialSignIn?.signIn(config)
        }
        appleSignInButton.setOnClickListener {
            viewModel.setProgressBarVisibility(true)
            signInType = AnalyticsPlatformsContract.SignInType.APPLE
            socialSignIn = SocialSignInFactory.build(AppConstants.LOGIN_TYPE_APPLE)
            socialSignIn?.signIn(config)
        }
    }

    private fun onSuccessSignIn(user: User?) {
        socialSignIn?.signOut()
        user?.let {
            hideKeyboard()
            signInGroup.visibility = View.GONE
            appLogoSignInImageView.visibility = View.GONE
            appleSignInButton.visibility = View.GONE
            googleSignInButton.visibility = View.GONE
            facebookSignInButton.visibility = View.GONE
            signInWelcomeBackTextView.visibility = View.VISIBLE
            signInUserNameTextView.text = it.fullName ?: it.email
            signInRootLayout.announceForAccessibility("${signInWelcomeBackTextView.text}  ${signInUserNameTextView.text}")
            postDelay(400) {
                beginDelayedTransition(signInRootLayout)
                signInUserNameTextView.animate()
                        .translationY(signInUserNameTextView.height.toFloat())
                        .setListener(object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator?) {
                                super.onAnimationEnd(animation)
                                signInUserNameTextView.visibility = View.VISIBLE
                            }
                        })
                progressViewSignIn.visibility = View.INVISIBLE
                signInCheckMarkView.visibility = View.VISIBLE
                signInCheckMarkView.isSelected = true
                postDelay(1000) {
                    if (activity != null && !activity!!.isFinishing) {
                        if (activity!!.isTaskRoot || (activity as AuthenticationActivity)?.deepLinkUrl != null) {
                            val intent = Intent(activity, UseTicketsActivity::class.java)
                            intent.putExtra(AppConstants.DEEPLINK_URL, (activity as AuthenticationActivity)?.deepLinkUrl)
                            startActivity(intent)
                        }

                        if(!activity?.isTaskRoot!! && (activity as AuthenticationActivity)?.deepLinkUrl != null) {
                            activity?.finishAffinity()
                        } else {
                            activity?.setResult(
                                BytemarkSDK.ResponseCode.SUCCESS
                            )
                            activity?.finish()
                        }
                    }
                }
            }
        }
        if(confHelper.isElertSdkEnabled()) {
            Util.regUserAndOrgForECUI(activity?.application!!)
        }
    }

    private fun showInvalidCredentialDialog() {
        analyticsPlatformAdapter.signIn(
                originScreen,
                signInType,
                AnalyticsPlatformsContract.Status.FAILURE,
                getString(R.string.sign_in_popup_wrong_credentials)
        )
        if (isOnline()) {
            showDefaultErrorDialog(
                    getString(R.string.sign_in_cant_login),
                    getString(R.string.sign_in_popup_wrong_credentials)
            )
        } else {
            connectionErrorDialog()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        socialSignIn?.activityResult(requestCode, resultCode, data, config)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onSignInSuccess(
            accessToken: String?,
            email: String?,
            name: String?,
            signInType: String?
    ) {
        viewModel.getOauthToken(name, email, null, signInType, accessToken)
    }

    override fun onSignInCancelled() {
        viewModel.setProgressBarVisibility(false)
    }

    override fun onSignInError(errorCode: String?, errorMessage: String?, signInType: String?) {
        viewModel.setProgressBarVisibility(false)
        if (isOnline()) {
            showDefaultErrorDialog(errorMsg = getString(R.string.something_went_wrong))
        } else {
            connectionErrorDialog()
        }
    }

    private fun setupAccessibility() {
        context?.setCursorForEditTextInAccessibilityMode(
                mutableListOf(
                        emailSignInTextInputEditText,
                        passwordSignInTextInputEditText
                )
        )
    }

    private fun applyAnimationToFragment(fragment: Fragment): Fragment {
        fragment.sharedElementEnterTransition = MoveTransition()
        fragment.enterTransition = Fade()
        fragment.exitTransition = Fade()
        return fragment
    }

    private fun makeAllButtonEnableDisable(isLoading: Boolean) {
        appleSignInButton.isEnabled = !isLoading
        facebookSignInButton.isEnabled = !isLoading
        googleSignInButton.isEnabled = !isLoading
        signInButton.isEnabled = !isLoading
    }
}