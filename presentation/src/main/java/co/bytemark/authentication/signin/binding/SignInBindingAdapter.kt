package co.bytemark.authentication.signin.binding

import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.EditText
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import co.bytemark.BR
import co.bytemark.R
import com.google.android.material.textfield.TextInputLayout

class SignInBindingAdapter : BaseObservable() {

    private val fields = SignInFields()
    private val errors = SignInErrorFields()
    private val buttonClick: MutableLiveData<SignInFields> = MutableLiveData<SignInFields>()
    private var isEmailValid = false
    private var isPasswordValid = false

    val emailFocusChangeListener: View.OnFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
        if (hasFocus.not()) {
            isEmailValid((v as EditText).text.toString())
        }
    }

    @Bindable
    fun isValid(): Boolean {
        return isEmailValid && isPasswordValid
    }


    @Bindable
    fun getAlphaValue(): Float {
        return if (isValid()) 1f else 0.3f
    }

    fun isEmailValid(emailInput: String?): Boolean {
        return emailInput?.let {
            fields.email = emailInput
            notifyPropertyChanged(BR.valid)
            notifyPropertyChanged(BR.alphaValue)
            if (Patterns.EMAIL_ADDRESS.matcher(emailInput.toString()).matches()) {
                errors.email = null
                notifyPropertyChanged(BR.emailError)
                isEmailValid = true
                true
            } else {
                errors.email = R.string.sign_in_invalid_email
                notifyPropertyChanged(BR.emailError)
                isEmailValid = false
                false
            }
        } ?: false
    }

    fun isPasswordValid(passwordInput: String?): Boolean {
        return passwordInput?.let {
            fields.password = passwordInput
            notifyPropertyChanged(BR.valid)
            notifyPropertyChanged(BR.alphaValue)
            if (!TextUtils.isEmpty(passwordInput)) {
                errors.password = null
                notifyPropertyChanged(BR.passwordError)
                isPasswordValid = true
                true
            } else {
                errors.password = R.string.sign_in_password_cannot_be_empty
                notifyPropertyChanged(BR.passwordError)
                isPasswordValid = false
                false
            }
        } ?: false
    }

    fun performSignIn() {
        if (isValid()) {
            buttonClick.value = fields
        }
    }

    fun getSignInFields(): MutableLiveData<SignInFields> {
        return buttonClick
    }

    fun getFields(): SignInFields {
        return fields
    }

    @Bindable
    fun getEmailError(): Int? {
        return errors.email
    }

    @Bindable
    fun getPasswordError(): Int? {
        return errors.password
    }

    fun onEmailTextChange(editable:CharSequence?){
        // As we don't want the invalid email error to appear as soon as user starts typing, I added the onFocus change listener for email,
        // But user can enter password and come to email and change the email to a invalid one. Tried RxView, not working for this case
        // To avoid this issue, validating over text change when password not empty.
        // TODO: 27/05/21 Check if there any alternate soln to handle this elegantly
        if(fields.password?.isNotEmpty() == true){
            isEmailValid(editable.toString())
        }
    }

    fun onPasswordTextChanged(editable: CharSequence?) {
        isPasswordValid(editable.toString())
    }

}

@BindingAdapter("error")
fun setError(textInputLayout: TextInputLayout, error: Int?) {
    val errorMessage = if (error != null) textInputLayout.context.getString(error) else null
    if (textInputLayout.error != errorMessage) {
        textInputLayout.error = errorMessage
    }
}