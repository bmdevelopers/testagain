package co.bytemark.authentication.signin.socialsignin

import android.content.Intent
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.util.isOnline
import com.google.firebase.auth.*

class AppleSignIn(private val signInType: String) : SocialSignIn {

    override fun signIn(socialSignInConfig: SocialSignInConfig?) {
        if (socialSignInConfig?.activity?.context?.isOnline() == true) {
            val auth = FirebaseAuth.getInstance()
            val provider = socialSignInConfig.activity?.resources?.configuration?.locale?.toLanguageTag()?.replace("-", "_")?.let {
                OAuthProvider.newBuilder(AppConstants.FIREBASE_OAUTH_PROVIDER_APPLE)
                    .setScopes(mutableListOf(AppConstants.SOCIAL_LOGIN_SCOPE_NAME, AppConstants.SOCIAL_LOGIN_SCOPE_EMAIL))
                    .addCustomParameter("locale", it)
                    .build()
            } as OAuthProvider

            socialSignInConfig.activity?.activity?.let {
                auth.startActivityForSignInWithProvider(it, provider)
                    .addOnSuccessListener { authResult: AuthResult? ->
                        handleAppleSignInSuccess(authResult, socialSignInConfig)
                    }.addOnFailureListener { e: Exception? ->
                        if (e is FirebaseAuthWebException && e.errorCode == "ERROR_WEB_CONTEXT_CANCELED") {
                            socialSignInConfig.callback?.onSignInCancelled()
                        } else {
                            socialSignInConfig.callback?.onSignInError(null, e?.localizedMessage, signInType)
                        }
                    }
            }
        }else{
            socialSignInConfig?.callback?.onSignInError(null,null, signInType)
        }
    }

    private fun handleAppleSignInSuccess(authResult: AuthResult?, socialSignInConfig: SocialSignInConfig?) {
        val user = authResult?.user
        var name: String? = ""
        user?.providerData?.let {
            if (user.providerData.size > 1)
                name = user.providerData[1].displayName
        }
        val email = user?.email
        var accessToken = ""
        authResult?.credential?.let {
            if (it is OAuthCredential)
                accessToken = it.idToken
        }
        socialSignInConfig?.callback?.onSignInSuccess(accessToken, email, name, signInType)
    }


    override fun activityResult(requestCode: Int, resultCode: Int, data: Intent?, socialSignInConfig: SocialSignInConfig?) {
        // No Activity Result For Apple SignIn
    }

    override fun signOut() {
        FirebaseAuth.getInstance().signOut()
    }


}