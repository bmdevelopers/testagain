package co.bytemark.authentication.signin

import android.os.Build
import androidx.lifecycle.MutableLiveData
import co.bytemark.R
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.authentication.GetOAuthTokenUseCase
import co.bytemark.domain.interactor.authentication.ResendVerificationEmailUseCase
import co.bytemark.domain.interactor.notification.GetNotificationsUseCase
import co.bytemark.domain.model.common.Result
import co.bytemark.sdk.Api.ApiUtility
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.data.identifiers.IdentifiersRepository
import co.bytemark.sdk.data.userAccount.UserAccountRepository
import co.bytemark.sdk.model.common.User
import co.bytemark.sdk.network_impl.BaseNetworkRequest
import kotlinx.coroutines.launch
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.*
import javax.inject.Inject

class SignInViewModel @Inject constructor(
        private val getNotificationsUseCase: GetNotificationsUseCase,
        private val resendVerificationEmailUseCase: ResendVerificationEmailUseCase,
        private val oAuthTokenUseCase: GetOAuthTokenUseCase, private val identifiersRepository: IdentifiersRepository, private val userAccountRepository: UserAccountRepository
) : BaseViewModel() {

    val unreadNotificationLiveData by lazy {
        MutableLiveData<Int>()
    }

    val resendVerificationLiveData by lazy {
        MutableLiveData<Int>()
    }

    val emailConfirmationLiveData by lazy {
        MutableLiveData<Unit>()
    }

    val signInSuccessLiveData by lazy {
        MutableLiveData<User>()
    }

    val invalidUserNamePasswordLiveData by lazy {
        MutableLiveData<Boolean>()
    }

    val loadingLiveData by lazy {
        MutableLiveData<Boolean>()
    }

    fun loadNotification() = uiScope.launch {
        getNotificationsUseCase(Unit).let {
            when (it) {
                is Result.Success -> {
                    unreadNotificationLiveData.value =
                            it.data?.notifications?.count { count -> !count.read } ?: 0
                }
            }
        }
    }

    fun logAccessTime() = uiScope.launch {
        userAccountRepository.insertOrUpdateLastAccessTime(GregorianCalendar())
    }

    fun resendVerificationEmail() = uiScope.launch {
        loadingLiveData.value = true
        resendVerificationEmailUseCase(Unit).let {
            when (it) {
                is Result.Success -> {
                    loadingLiveData.value = false
                    resendVerificationLiveData.value = R.string.msg_verification_email_sent
                }
                is Result.Failure -> {
                    loadingLiveData.value = false
                    resendVerificationLiveData.value =
                            R.string.msg_problem_sending_verification_email
                }
            }
        }
    }

    fun getOauthToken(
            displayName: String?,
            email: String?,
            password: String?,
            signInType: String?,
            accessToken: String?
    ) = uiScope.launch {
        loadingLiveData.value = true
        oAuthTokenUseCase(
                GetOAuthTokenUseCase.SignInRequestValues(
                        buildQueryMap(
                                displayName,
                                email,
                                password,
                                signInType,
                                accessToken
                        )
                )
        ).let {
            when (it) {
                is Result.Success -> {
                    it.data?.oauthToken?.let { it1 -> signIn(it1) }
                }
                is Result.Failure -> {
                    loadingLiveData.value = false
                    if (it.bmError.first().code == BaseNetworkRequest.UNAUTHORISED) {
                        invalidUserNamePasswordLiveData.value = true
                    } else {
                        errorLiveData.value = it.bmError.first()
                    }
                }
                else -> {

                }
            }
        }
    }

    private fun signIn(oauthToken: String) = uiScope.launch {
        val data = oauthToken.let {
            BytemarkSDK.doLogin(it).map { obj ->
                obj.data
            }
        }
        data?.subscribe {
            if (it.organizationDetails?.requiresEmailVerification != null
                    && it.organizationDetails?.requiresEmailVerification == true
                    && it.user?.isEmailVerified == false
            ) {
                loadingLiveData.value = false
                emailConfirmationLiveData.value = Unit
            } else {
                signInSuccessLiveData.value = it.user
            }
        }
    }

    fun setProgressBarVisibility(isLoading: Boolean) {
        loadingLiveData.value = isLoading
    }

    private fun buildQueryMap(
            displayName: String?,
            email: String?,
            password: String?,
            signInType: String?,
            accessToken: String?
    ): MutableMap<String,
            String> {
        val signInQueryMap: MutableMap<String, String> = HashMap()
        addIfNotNullValue(signInQueryMap, "first_name", displayName)
        addIfNotNullValue(signInQueryMap, "email", email)
        addIfNotNullValue(signInQueryMap, "login_type", signInType)
        addIfNotNullValue(signInQueryMap, "password", password)
        addIfNotNullValue(signInQueryMap, "access_token", accessToken)
        return prepareQueryMap(signInQueryMap)
    }

    private fun prepareQueryMap(queryMap: MutableMap<String, String>): MutableMap<String, String> {
        var deviceModel: String? = null
        try {
            deviceModel = URLEncoder.encode(Build.MODEL, "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
        return queryMap.apply {
            put("device_os_version", Build.VERSION.RELEASE)
            put("device_os", "android")
            put("osi", BytemarkSDK.SDKUtility.getDeviceOsi())
            deviceModel?.let { put("device_model", it) }
            deviceModel?.let { put("device_nickname", it) }
            identifiersRepository.getAttribute("aii")?.let { put("aii", it) }
            put("app_version", BytemarkSDK.SDKUtility.getAppVersion())
            put("client_id", BytemarkSDK.SDKUtility.getClientId())
            ApiUtility.getSFromCalendar(Calendar.getInstance())?.let { put("device_time", it) }
        }
    }

    private fun <K, V> addIfNotNullValue(map: MutableMap<K, V>, key: K, value: V?): Map<K, V>? {
        if (value != null) map[key] = value
        return map
    }

    fun clearLiveData() {
        errorLiveData.postValue(null)
        invalidUserNamePasswordLiveData.postValue(false)
    }

}