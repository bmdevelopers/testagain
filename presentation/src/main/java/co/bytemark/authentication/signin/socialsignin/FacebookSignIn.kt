package co.bytemark.authentication.signin.socialsignin

import android.content.Intent
import android.os.Bundle
import co.bytemark.widgets.util.isOnline
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult

class FacebookSignIn(private val signInType: String) : SocialSignIn {

    private var callbackManager: CallbackManager? = null
    private var loginManager: LoginManager? = null

    init {
        callbackManager = CallbackManager.Factory.create()
        loginManager = LoginManager.getInstance()
    }

    override fun signIn(socialSignInConfig: SocialSignInConfig?) {
        if(socialSignInConfig?.activity?.context?.isOnline() == true) {
            val loginPermission = listOf("email")
            loginManager?.logInWithReadPermissions(socialSignInConfig.activity, loginPermission)
            loginManager?.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {

                override fun onSuccess(result: LoginResult?) {
                    result?.let {
                        callGraphAPI(it.accessToken, socialSignInConfig)
                    }
                }

                override fun onCancel() {
                    socialSignInConfig.callback?.onSignInCancelled()
                }

                override fun onError(error: FacebookException?) {
                    if (error is FacebookAuthorizationException) {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            LoginManager.getInstance().logOut()
                        }
                    }
                    socialSignInConfig.callback?.onSignInError(null, error?.localizedMessage, signInType)
                }
            })
        }else{
            socialSignInConfig?.callback?.onSignInError(null,null, signInType)
        }
    }

    private fun callGraphAPI(accessToken: AccessToken, socialSignInConfig: SocialSignInConfig?) {
        val request: GraphRequest = GraphRequest.newMeRequest(accessToken) { _, response ->
            val jsonObject = response?.jsonObject
            val email: String? =
                if (jsonObject?.has("email") == true) jsonObject.getString("email") else null
            socialSignInConfig?.callback?.onSignInSuccess(accessToken.token, email, null, signInType)
        }
        val parameters = Bundle()
        parameters.putString("fields", "email")
        request.parameters = parameters
        request.executeAsync()
    }

    override fun activityResult(requestCode: Int, resultCode: Int, data: Intent?, socialSignInConfig: SocialSignInConfig?) {
        callbackManager?.onActivityResult(requestCode, resultCode, data)
    }

    override fun signOut() {
        GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, GraphRequest.Callback {
            LoginManager.getInstance().logOut()
        }).executeAsync()
    }

}