package co.bytemark.authentication.signin.binding

data class SignInErrorFields(var email: Int? = null, var password: Int? = null)