package co.bytemark.authentication.signin.binding

data class SignInBindingConfig(var isAppIconVisible: Boolean = false, var socialSignInList: MutableList<String>?)