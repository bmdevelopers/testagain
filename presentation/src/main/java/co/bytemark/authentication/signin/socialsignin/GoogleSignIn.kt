package co.bytemark.authentication.signin.socialsignin

import android.content.Intent
import co.bytemark.R
import co.bytemark.widgets.util.isOnline
import com.google.android.gms.auth.api.signin.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task

class GoogleSignIn(private val signInType: String) : SocialSignIn {

    private val googleSignInRequestCode = 9001

    private var gso: GoogleSignInOptions? = null
    private var googleSignInClient: GoogleSignInClient? = null

    override fun signIn(socialSignInConfig: SocialSignInConfig?) {
        if (socialSignInConfig?.activity?.context?.isOnline() == true) {
            gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(socialSignInConfig?.activity?.getString(R.string.server_client_id))
                .requestEmail()
                .build()

            googleSignInClient = socialSignInConfig.activity?.let {
                it.activity?.let { it1 -> GoogleSignIn.getClient(it1, gso!!) }
            }

            socialSignInConfig.activity?.startActivityForResult(googleSignInClient?.signInIntent, googleSignInRequestCode)
        } else {
            socialSignInConfig?.callback?.onSignInError(null, null, signInType)
        }
    }

    override fun activityResult(requestCode: Int, resultCode: Int, data: Intent?, socialSignInConfig: SocialSignInConfig?) {
        if (requestCode == googleSignInRequestCode) {
            val result = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(result, socialSignInConfig)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>, socialSignInConfig: SocialSignInConfig?) {
        try {
            val account: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
            socialSignInConfig?.callback?.onSignInSuccess(account?.idToken, account?.email, null, signInType)
        } catch (e: ApiException) {
            if (e.statusCode == GoogleSignInStatusCodes.SIGN_IN_CANCELLED) {
                socialSignInConfig?.callback?.onSignInCancelled()
            } else {
                socialSignInConfig?.callback?.onSignInError(e.statusCode.toString(), e.message, signInType)
            }
        }
    }

    override fun signOut() {
        googleSignInClient?.revokeAccess()
    }
}