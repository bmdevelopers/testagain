package co.bytemark.authentication.signin.socialsignin

interface SocialSignInCallbacks {

    fun onSignInSuccess(accessToken: String?, email: String?, name: String?, signInType: String?)

    fun onSignInError(errorCode: String?, errorMessage: String?, signInType: String?)

    fun onSignInCancelled()
}