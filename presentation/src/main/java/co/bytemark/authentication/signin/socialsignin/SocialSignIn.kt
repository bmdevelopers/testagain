package co.bytemark.authentication.signin.socialsignin

import android.content.Intent

interface SocialSignIn {

    fun signIn(socialSignInConfig: SocialSignInConfig?)

    fun activityResult(requestCode: Int, resultCode: Int, data: Intent?, socialSignInConfig: SocialSignInConfig?)

    fun signOut()

}