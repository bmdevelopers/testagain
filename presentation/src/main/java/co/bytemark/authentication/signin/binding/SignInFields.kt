package co.bytemark.authentication.signin.binding

data class SignInFields(var email: String? = null, var password: String? =null)