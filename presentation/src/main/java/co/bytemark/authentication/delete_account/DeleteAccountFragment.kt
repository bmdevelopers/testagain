package co.bytemark.authentication.delete_account

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Parcelable
import android.transition.TransitionManager
import android.view.*
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.add_payment_card.PaymentCard
import co.bytemark.authentication.AuthenticationActivity
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.Result
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.AppConstants.PAYMENT_METHODS_REQUEST_CODE
import co.bytemark.payment_methods.PaymentMethodsActivity
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.Helpers.PaymentMethod
import co.bytemark.sdk.model.payment_methods.Card
import co.bytemark.sdk.model.payment_methods.PaymentMethods
import co.bytemark.sdk.model.payment_methods.Wallet
import co.bytemark.widgets.transition.SuperAutoTransition
import co.bytemark.widgets.util.hide
import co.bytemark.widgets.util.invisible
import co.bytemark.widgets.util.show
import com.afollestad.materialdialogs.MaterialDialog
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_account_delete.*
import javax.inject.Inject

open class DeleteAccountFragment : BaseMvvmFragment() {

    companion object {
        @JvmStatic
        fun newInstance() = DeleteAccountFragment()
        const val EXTRA_TAG = "DELETE_ACCOUNT"
    }

    @Inject
    lateinit var viewModel: DeleteAccountViewModel

    @Inject
    lateinit var adapter: TransferFundAdapter

    private var paymentMethods: PaymentMethods? = null
    private var walletWithFundsPresent: Boolean = false
    private var selectedCard: Card? = null

    open val contentMessage by lazy {
        String.format(
            getString(R.string.delete_account_message), getString(R.string.app_name),
            getString(R.string.app_name)
        )
    }

    open val deleteLabel by lazy {
        getString(R.string.deactivate_account)
    }

    open val dialogTitle by lazy {
        getString(
            R.string.deactivate_account_want_to_deactivate
        )
    }

    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        postponeEnterTransition()
        (activity as AuthenticationActivity).let {
            it.supportActionBar?.title = deleteLabel
            it.enableOrDisableDrawerHack(true)
            it.makeAnnouncementForTitle(deleteLabel)
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.clear()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_account_delete, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getPaymentMethods()

        deleteMessageTextView.text = contentMessage
        deleteButton.text = deleteLabel

        deleteButton.setOnClickListener { onDeleteButtonClick() }
        cancelButton.setOnClickListener { onCancelButtonClick() }
        textViewChangePaymentMethod.setOnClickListener { onChangePaymentMethodClick() }

        walletFundTransferRecyclerView.adapter = adapter

        hideKeyboard()
    }

    open fun getPaymentMethods() {
        viewModel.getPaymentMethods().observe(this, Observer { onGettingPaymentMethods(it) })
    }


    private fun onDeleteButtonClick() {
        showDeleteAccountConfirmationDialog()
    }

    private fun onGettingPaymentMethods(result: Result<PaymentMethods>?) {
        hideLoading()
        result?.let {
            when (it) {
                is Result.Loading -> showLoading()
                is Result.Success -> {
                    if (it.data == null) return
                    val paymentMethods = it.data as PaymentMethods
                    this.paymentMethods = paymentMethods
                    if (getWalletWithFunds().isNotEmpty()) {
                        walletWithFundsPresent = true
                        displayTransferFundUI()
                    } else {
                        showAndMoveActionButtonToBottom()
                    }
                }
                is Result.Failure -> {
                    showAndMoveActionButtonToBottom()
                    handleError(it.bmError[0])
                }
            }
        }
    }

    protected fun showAndMoveActionButtonToBottom() {
        val set = ConstraintSet()
        set.clone(constraintLayoutDeactivateAccount)
        set.clear(deleteButton.id, ConstraintSet.TOP)
        set.clear(cancelButton.id, ConstraintSet.TOP)
        set.connect(deleteButton.id, ConstraintSet.BOTTOM, cancelButton.id, ConstraintSet.TOP, 24)
        set.connect(
            cancelButton.id, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 24
        )
        set.applyTo(constraintLayoutDeactivateAccount)
    }

    private fun showDeleteAccountConfirmationDialog() {
        context?.let {
            MaterialDialog.Builder(it).title(dialogTitle).content(
                contentMessage
            ).positiveText(deleteLabel)
                    .positiveColor(ContextCompat.getColor(it, R.color.errorIndicatorColor))
                    .negativeColor(
                        ContextCompat.getColor(it, R.color.orgCollectionPrimaryTextColor)
                    ).onPositive { _, _ -> deleteAccount() }
                    .negativeText(getString(R.string.popup_cancel))
                    .onNegative { dialog, _ -> dialog.cancel() }.build().show()
        }
    }

    private fun deleteAccount() {
        viewModel.getDeleteAccountLiveData(
            if (walletWithFundsPresent && selectedCard != null) selectedCard?.uuid else null
        ).observe(this, Observer { onDeleteAccountResult(it) })
    }

    private fun displayTransferFundUI() {
        groupDeleteAccount.visibility = View.VISIBLE
        textViewMessageSelectPaymentMethod.visibility = View.VISIBLE
        adapter.submitList(getWalletWithFunds())
        adapter.notifyDataSetChanged()
        if (walletWithFundsPresent && paymentMethods?.cards?.isNullOrEmpty() == false) {
            selectedCard = paymentMethods?.cards?.first()
            displayCardPaymentMethod()
        } else {
            textViewChangePaymentMethod.visibility = View.GONE
            textViewPaymentMethodNotFound.visibility = View.VISIBLE
            imageViewPaymentMethodNotFound.visibility = View.VISIBLE
            groupPaymentMethod.visibility = View.GONE
            deleteButton.isEnabled = false
            deleteButton.alpha = 0.3F
        }
    }

    private fun getWalletWithFunds(): ArrayList<Wallet> {
        val walletsWithFunds = ArrayList<Wallet>()
        paymentMethods?.storedWallets?.wallets?.forEach {
            if (it.availableAmount > 0) walletsWithFunds.add(it)
        }
        return walletsWithFunds
    }

    private fun displayCardPaymentMethod() {
        textViewPaymentCardLastFourDigitAndNickName.text =
            selectedCard?.lastFour.plus(" ").plus(selectedCard?.nickname)
        textViewPaymentCardExpiry.text =
            getString(R.string.deactivate_account_card_expiration_text).plus(
                selectedCard?.expirationDate
            )
        Glide.with(imageViewPaymentCardIcon.context).load(
            selectedCard?.typeName?.let { PaymentCard.fromPaymentCardType(it).getTypeImage() })
                .crossFade().into(imageViewPaymentCardIcon)
        textViewChangePaymentMethod.visibility = View.VISIBLE
    }

    private fun onDeleteAccountResult(result: Result<Boolean>?) {
        hideLoading()
        result?.let {
            when (it) {
                is Result.Loading -> showLoading()
                is Result.Success -> {
                    if (it.data!!) {
                        onDeleteAccountSuccess()
                    } else {
                        showDefaultErrorDialog("", "")
                    }
                }
                is Result.Failure -> handleError(it.bmError[0])
            }
        }
    }

    private fun onDeleteAccountSuccess() {
        val hideContent = SuperAutoTransition()
        hideContent.duration = 300
        hideContent.interpolator = LinearOutSlowInInterpolator()
        hideContent.doOnEnd(Runnable {
            val superAutoTransition = SuperAutoTransition()
            superAutoTransition.duration = 300
            superAutoTransition.doOnEnd(Runnable {
                deleteAccountCheckmark.isSelected = true
                deleteAccountCheckmark.postDelayed({
                    if (isAdded) {
                        deleteAccountButtonOk.animate().alpha(1f).duration = 300
                        deleteAccountButtonOk.setOnClickListener(View.OnClickListener {
                            registerReceiver()
                            logOut()
                        })
                    }
                }, 400)
            })
            TransitionManager.beginDelayedTransition(deleteAccountRootLayout, superAutoTransition)
            view?.announceForAccessibility(deleteAccountSuccess.text)
            view?.announceForAccessibility(deleteAccountSuccessMessage.text)
            deleteAccountSuccessContainer.show()
        })
        TransitionManager.beginDelayedTransition(deleteAccountRootLayout, hideContent)
        cardViewDeleteAccountTitle.hide()
        walletFundTransferRecyclerView.hide()
        transferFundTextView.hide()
        cardViewPaymentMethodSpinnerDeleteAccount.hide()
        deleteButton.hide()
        cancelButton.hide()
        loadingProgressLayout.hide()
        textViewChangePaymentMethod.hide()
        groupDeleteAccount.hide()
    }

    private fun onCancelButtonClick() = finishDeleteAccountFragment()

    private fun onChangePaymentMethodClick() {
        startActivityForResult(Intent(context, PaymentMethodsActivity::class.java).apply {
            putExtra(AppConstants.SHOPPING_CART_INTENT, true)
            putExtra(AppConstants.AUTOLOAD, true)
            putExtra(AppConstants.HIDE_ADD_PAYMENT, true)
            putExtra(AppConstants.HIDE_DELETE_PAYMENT, true)
        }, PAYMENT_METHODS_REQUEST_CODE)
    }

    private fun showLoading() {
        loadingProgressLayout?.show()
        deleteButton?.invisible()
        cancelButton?.invisible()
    }

    private fun hideLoading() {
        loadingProgressLayout?.invisible()
        deleteButton?.show()
        cancelButton?.show()
    }

    private fun finishDeleteAccountFragment() {
        if (activity != null) {
            this.fragmentManager?.popBackStack()
        }
    }

    override fun onDeviceLostOrStolenButtonClick() {
        onCommonErrorDialogConfirmationClick()
    }

    override fun onDeviceTimeErrorButtonClick() {
        onCommonErrorDialogConfirmationClick()
    }

    private fun onCommonErrorDialogConfirmationClick() {
        finishDeleteAccountFragment()
    }

    private fun registerReceiver() {
        context?.let {
            LocalBroadcastManager.getInstance(it).registerReceiver(object : BroadcastReceiver() {
                override fun onReceive(context: Context?, intent: Intent?) {
                   activity?.finish()
                }
            }, IntentFilter("LOGOUT"))
        }
    }

    private fun logOut() {
        this.context?.let { BytemarkSDK.logout(it) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val selectedPaymentMethods: List<PaymentMethod>?
        when (requestCode) {
            PAYMENT_METHODS_REQUEST_CODE -> {
                if (data != null) {
                    if (data.getParcelableArrayListExtra<Parcelable>(
                                AppConstants.SELECTED_CARD
                            ) != null) {
                        selectedPaymentMethods =
                            data.getParcelableArrayListExtra(AppConstants.SELECTED_CARD)
                        if (selectedPaymentMethods?.size == 1) {
                            val paymentMethod = selectedPaymentMethods[0]
                            if (paymentMethod is Card) {
                                selectedCard = paymentMethod
                                displayCardPaymentMethod()
                            }
                        }
                    }
                }
            }
        }
    }
}

