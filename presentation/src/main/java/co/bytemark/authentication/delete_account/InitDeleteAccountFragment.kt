package co.bytemark.authentication.delete_account

import android.os.Bundle
import android.view.View
import co.bytemark.R
import kotlinx.android.synthetic.main.fragment_account_delete.*

class InitDeleteAccountFragment : DeleteAccountFragment() {

    companion object {
        @JvmStatic
        fun newInstance() = InitDeleteAccountFragment()
        const val EXTRA_TAG = "INIT_DELETE_ACCOUNT"
    }

    override val contentMessage by lazy {
        getString(R.string.close_account_message)
    }


    override val deleteLabel by lazy {
        getString(R.string.close_account)
    }


    override val dialogTitle by lazy {
       getString(R.string.close_account_dialog_title)
    }

    override fun getPaymentMethods() {
        //skip network call & show button Ui
        showAndMoveActionButtonToBottom()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cardViewDeleteAccountTitle.elevation = 0F
        cardViewDeleteAccountTitle.setBackgroundResource(0)
    }

}