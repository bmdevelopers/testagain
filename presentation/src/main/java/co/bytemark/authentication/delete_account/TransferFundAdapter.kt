package co.bytemark.authentication.delete_account

import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.R
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.model.payment_methods.Wallet
import kotlinx.android.synthetic.main.wallet_transfer_item.view.*
import javax.inject.Inject

/** Created by Santosh on 2019-12-31.
 * Copyright (c) 2019 Bytemark inc. All rights reserved.
 */

class TransferFundAdapter @Inject constructor(
        private val confHelper: ConfHelper
) : ListAdapter<Wallet, TransferFundAdapter.WalletViewHolder>(OrderDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WalletViewHolder =
            LayoutInflater.from(parent.context)
                    .inflate(R.layout.wallet_transfer_item, parent, false)
                    .run { WalletViewHolder(this) }


    override fun onBindViewHolder(viewHolder: WalletViewHolder, position: Int) {
        viewHolder.bind(getItem(position))
    }

    inner class WalletViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(wallet: Wallet) {
            itemView.apply {
                textViewWalletType.text = wallet.walletType?.plus(" ").plus(if (adapterPosition == 0) "" else adapterPosition + 1)
                textViewNickName.text = wallet.nickName
                textViewBalance.text = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(wallet.availableAmount).plus(this.context.getString(R.string.deactivate_account_available_text))
            }
        }
    }

    class OrderDiffCallback : DiffUtil.ItemCallback<Wallet>() {
        override fun areItemsTheSame(oldItem: Wallet, newItem: Wallet): Boolean {
            return oldItem.uuid == newItem.uuid
        }

        override fun areContentsTheSame(oldItem: Wallet, newItem: Wallet): Boolean {
            return oldItem.uuid == newItem.uuid
        }
    }
}