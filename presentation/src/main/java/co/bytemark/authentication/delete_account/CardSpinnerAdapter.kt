package co.bytemark.authentication.delete_account

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import co.bytemark.R
import co.bytemark.add_payment_card.PaymentCard
import co.bytemark.sdk.model.payment_methods.Card
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.card_fund_transfer_item.view.*


/** Created by Santosh on 2019-12-31.
 * Copyright (c) 2019 Bytemark inc. All rights reserved.
 */

class CardSpinnerAdapter constructor(
        private val activity: Activity,
        cards: List<Card>
) : ArrayAdapter<Card>(activity, R.layout.card_fund_transfer_item, cards) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View =
            getCustomView(position, convertView, parent)

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View =
            getCustomView(position, convertView, parent)

    private fun getCustomView(position: Int, convertView: View?, parent: ViewGroup): View {
        val card = getItem(position)

        return activity.layoutInflater
                .inflate(R.layout.card_fund_transfer_item, parent, false).apply {
                    val paymentCard = card?.typeName?.let { PaymentCard.fromPaymentCardType(it) }
                    Glide.with(imageViewCard.context)
                            .load(paymentCard?.getTypeImage())
                            .crossFade()
                            .into(imageViewCard)
                    textViewCardType.text = card?.typeName?.toUpperCase()
                    textViewLastFour.text = card?.lastFour
                }
    }
}