package co.bytemark.authentication.delete_account

import androidx.lifecycle.ViewModel
import co.bytemark.domain.interactor.authentication.DeleteAccountUseCase
import co.bytemark.domain.interactor.authentication.DeleteAccountUseCaseRequestValue
import co.bytemark.domain.interactor.payments.GetPaymentMethodsUseCase
import co.bytemark.domain.interactor.payments.GetPaymentMethodsUseCaseValue
import co.bytemark.helpers.ConfHelper
import javax.inject.Inject

/** Created by Santosh on 2019-12-18.
 * Copyright (c) 2019 Bytemark Inc. All rights reserved.
 */

class DeleteAccountViewModel @Inject constructor(
        private val deleteAccountUseCase: DeleteAccountUseCase,
        private val getPaymentMethodsUseCase: GetPaymentMethodsUseCase,
        private val confHelper: ConfHelper
) : ViewModel() {

    fun getDeleteAccountLiveData(creditCardUuid: String?) =
            deleteAccountUseCase.getLiveData(DeleteAccountUseCaseRequestValue(creditCardUuid))

    fun getPaymentMethods() =
            getPaymentMethodsUseCase.getLiveData(GetPaymentMethodsUseCaseValue(
                    confHelper.organizationUuid))
}
