package co.bytemark.authentication.change_password

import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import androidx.recyclerview.widget.LinearLayoutManager
import android.transition.TransitionManager
import android.util.Pair
import android.view.*
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.authentication.AuthenticationActivity
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.interactor.authentication.ChangePasswordRequestValues
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.common.Result
import co.bytemark.formly.adapter.FormlyAdapter
import co.bytemark.widgets.transition.SuperAutoTransition
import co.bytemark.widgets.util.Util
import kotlinx.android.synthetic.main.fragment_change_password.*
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject

class ChangePasswordFragment : BaseMvvmFragment() {

    companion object {
        const val EXTRA_TAG = "CHANGE_PASSWORD"
        fun newInstance() = ChangePasswordFragment()
    }

    @Inject
    lateinit var changePasswordViewModel: ChangePasswordViewModel

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    private val formlyMap = mutableMapOf<String, String>()

    private lateinit var formlyAdapter: FormlyAdapter

    private val subs = CompositeSubscription()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        postponeEnterTransition()
        (activity as AuthenticationActivity).let {
            it.supportActionBar?.title = getString(R.string.settings_change_password)
            it.enableOrDisableDrawerHack(true)
        }
        setDefaultErrorTitle(getString(R.string.change_password_failed))
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.clear()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_change_password, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changePasswordList?.layoutManager = LinearLayoutManager(context)
        formlyAdapter = FormlyAdapter(changePasswordList, fragmentManager)

        subs.add(formlyAdapter.buttonClicks()?.subscribe { formly: Formly -> onButtonClicked(formly) })
        subs.add(formlyAdapter.textChanges().subscribe { textChange: Pair<Formly, String> -> updateFormlyModel(textChange) })

        checkmark?.setMaterialWidth(Util.dpToPx(2.0).toFloat())
        changePasswordList?.adapter = formlyAdapter

        loadForms()
    }

    private fun loadForms() {
        val formly = changePasswordViewModel.getForms()
        formlyAdapter.setFormlyList(formly)
        changePasswordList?.post { startPostponedEnterTransition() }
    }

    private fun updateFormlyModel(value: Pair<Formly, String>) {
        formlyMap[value.first.key] = value.second.trim()
    }

    private fun onButtonClicked(formly: Formly) {
        hideKeyboard()
        when (formly.key) {
            Formly.CHANGE_PASSWORD_KEY -> {
                formlyAdapter.enableValidation()
                if (formlyAdapter.isValid()) {
                    if (isOnline()) {
                        changePassword()
                    } else {
                        connectionErrorDialog()
                    }
                }
            }
        }
    }

    private fun changePassword() {
        changePasswordViewModel.changePassword(ChangePasswordRequestValues(formlyMap)).observe(this, Observer { onChangePassword(it) })
    }

    private fun onChangePassword(result: Result<Boolean>?) {
        hideLoading()
        result?.let {
            when (it) {
                is Result.Loading -> showLoading()
                is Result.Success -> {
                    onChangePasswordSuccessful()
                    analyticsPlatformAdapter.passwordChanged(AnalyticsPlatformsContract.Status.SUCCESS, "")
                }
                is Result.Failure -> {
                    val error = it.bmError.first()
                    handleError(error)
                    analyticsPlatformAdapter.passwordChanged(AnalyticsPlatformsContract.Status.FAILURE, error.message)
                }
            }
        }
    }

    private fun showLoading() {
        hideKeyboard()
        formlyAdapter.setLoading(Formly.CHANGE_PASSWORD_KEY, true)
    }

    private fun hideLoading() {
        formlyAdapter.setLoading(Formly.CHANGE_PASSWORD_KEY, false)
    }


    override fun showAppUpdateDialog() {
        hideLoading()
        super.showAppUpdateDialog()
    }

    override fun onInject() {
        component.inject(this)
    }

    private fun onChangePasswordSuccessful() {
        hideLoading()
        val hideContent = SuperAutoTransition()
        hideContent.duration = 300
        hideContent.interpolator = LinearOutSlowInInterpolator()
        hideContent.doOnEnd(Runnable {
            val superAutoTransition = SuperAutoTransition()
            superAutoTransition.duration = 300
            superAutoTransition.doOnEnd(Runnable {
                checkmark.isSelected = true
                checkmark?.postDelayed({
                                           if (activity != null && !activity!!.isFinishing) {
                                               activity?.finish()
                                           }
                                       }, 400)
            })
            TransitionManager.beginDelayedTransition(root_layout, superAutoTransition)
            if (view != null) {
                view!!.announceForAccessibility(changePasswordSuccess!!.text)
            }
            changePasswordSuccess?.visibility = View.VISIBLE
            checkmark?.visibility = View.VISIBLE
        })
        TransitionManager.beginDelayedTransition(root_layout, hideContent)
        changePasswordList?.visibility = View.GONE
        changePasswordListContainer?.gravity = Gravity.CENTER
    }

}