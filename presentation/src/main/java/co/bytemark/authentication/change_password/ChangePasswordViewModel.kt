package co.bytemark.authentication.change_password

import androidx.lifecycle.ViewModel
import co.bytemark.domain.interactor.authentication.ChangePasswordRequestValues
import co.bytemark.domain.interactor.authentication.ChangePasswordUseCase
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.helpers.ConfHelper
import javax.inject.Inject

class ChangePasswordViewModel @Inject constructor(
        val confHelper: ConfHelper,
        private val changePasswordUseCase: ChangePasswordUseCase
) : ViewModel() {

    fun changePassword(changePasswordRequestValues: ChangePasswordRequestValues) = changePasswordUseCase.getLiveData(changePasswordRequestValues)

    fun getForms(): MutableList<Formly> {
        return confHelper.changePasswordForms
    }

}