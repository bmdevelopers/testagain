package co.bytemark.authentication

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import co.bytemark.R
import co.bytemark.appnotification.NotificationsActivity
import co.bytemark.authentication.account_management.AccountManagementFragment
import co.bytemark.authentication.change_password.ChangePasswordFragment
import co.bytemark.authentication.delete_account.DeleteAccountFragment
import co.bytemark.authentication.delete_account.InitDeleteAccountFragment
import co.bytemark.authentication.manage_pin.ManagePinFragment
import co.bytemark.authentication.signin.SignInFragment
import co.bytemark.authentication.voucher_code.VoucherCodeFragment
import co.bytemark.base.MasterActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.model.common.Action
import co.bytemark.widgets.util.replaceFragment

class AuthenticationActivity : MasterActivity() {

    companion object {
        const val EXTRA_KEY_DEFAULT_SCREEN = "EXTRA_KEY_DEFAULT_SCREEN"
    }

    var deepLinkUrl: String? = null
    private var unreadNotificationsCount = 0

    override fun getLayoutRes(): Int = R.layout.activity_authentication

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(confHelper.getSettingsMenuScreenTitleFromTheConfig(
                false, false, Action.AUTHENTICATION))
        val originStr = intent?.extras?.getString(AppConstants.EXTRA_ORIGIN)
        deepLinkUrl = intent?.extras?.getString(AppConstants.DEEPLINK_URL)
        val fragment = if (intent.hasExtra(EXTRA_KEY_DEFAULT_SCREEN)) {
            when (intent.getStringExtra(EXTRA_KEY_DEFAULT_SCREEN)) {
                ChangePasswordFragment.EXTRA_TAG -> ChangePasswordFragment.newInstance()
                AccountManagementFragment.EXTRA_TAG -> AccountManagementFragment.newInstance()
                VoucherCodeFragment.EXTRA_TAG -> VoucherCodeFragment.newInstance()
                DeleteAccountFragment.EXTRA_TAG -> DeleteAccountFragment.newInstance()
                InitDeleteAccountFragment.EXTRA_TAG -> InitDeleteAccountFragment.newInstance()
                ManagePinFragment.EXTRA_TAG -> ManagePinFragment.newInstance()
                else -> SignInFragment.newInstance(originStr ) // Load default fragment
            }
        } else {
            SignInFragment.newInstance(originStr) // Load default fragment
        }
        replaceFragment(fragment, R.id.fragment)
    }

    override fun useHamburgerMenu() = true

    override fun useToolbar() = true

    fun enableOrDisableDrawerHack(enable: Boolean) = enableOrDisableDrawer(enable)

    override fun shouldReduceFontSizeToDefaultIfLarge() = true

    fun onUnreadNotificationsCountChanged(@NonNull unreadNotificationsCount: Int) {
        this.unreadNotificationsCount = unreadNotificationsCount
        invalidateOptionsMenu()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            enableOrDisableDrawerHack(false)
            supportActionBar?.title = getString(R.string.sign_in_signin)
            supportFragmentManager.popBackStack()
        } else {
            setResult(BytemarkSDK.ResponseCode.NOT_AUTHENTICATED, Intent())
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (confHelper.isNotificationEnabled) {
            menuInflater.inflate(R.menu.menu_use_tickets, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        if (confHelper.isNotificationEnabled) {
            val notifications: MenuItem = menu.findItem(R.id.action_notifications)
            notifications.setActionView(R.layout.menu_item_notifications_layout)
            val textView = notifications.actionView.findViewById(R.id.tv_notifications_count) as TextView
            textView.backgroundTintList = ColorStateList.valueOf(confHelper.headerThemeAccentColor)
            if (unreadNotificationsCount != 0) {
                val count = unreadNotificationsCount.toString()
                textView.visibility = View.VISIBLE
                val string = resources.getQuantityString(
                    R.plurals.notifications_count,
                    unreadNotificationsCount,
                    unreadNotificationsCount
                )
                textView.contentDescription = string
                textView.text = count
            } else {
                textView.visibility = View.GONE
            }
            val imageView: ImageView = notifications.actionView.findViewById(R.id.iv_notifications) as ImageView
            imageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_bell_filled))
            imageView.imageTintList = ColorStateList.valueOf(confHelper.headerThemePrimaryTextColor)
            imageView.contentDescription = resources.getString(R.string.screen_title_notification)
            notifications.actionView.setOnClickListener { v ->
                //UseTicketsFragment.updateNotificationNavigation();
                startActivity(
                    Intent(
                        this@AuthenticationActivity,
                        NotificationsActivity::class.java
                    )
                )
            }
        }
        return true
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        deepLinkUrl = intent?.extras?.getString(AppConstants.DEEPLINK_URL)
    }
}