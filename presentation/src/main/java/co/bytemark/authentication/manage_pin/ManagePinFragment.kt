package co.bytemark.authentication.manage_pin

import android.app.Activity
import android.os.Bundle
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.authentication.AuthenticationActivity
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.model.common.Action
import co.bytemark.widgets.util.createViewModel
import co.bytemark.widgets.util.postDelay
import com.jakewharton.rxbinding.widget.RxTextView
import kotlinx.android.synthetic.main.fragment_manage_pin.*
import rx.Observable
import rx.subscriptions.CompositeSubscription


class ManagePinFragment : BaseMvvmFragment() {

    private val subscriptions: CompositeSubscription = CompositeSubscription()

    private var pinLength: Int = 0

    private lateinit var viewModel: ManagePinViewModel


    override fun onInject() {
        CustomerMobileApp.appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activity = activity as AuthenticationActivity?
        if (activity != null && activity.supportActionBar != null) {
            activity.supportActionBar?.title =
                (confHelper.getSettingsMenuScreenTitleFromTheConfig(true, true, Action.MANAGE_PIN))
            activity.enableOrDisableDrawerHack(true)
        }
        setHasOptionsMenu(true)
        viewModel = createViewModel { CustomerMobileApp.appComponent.managePinViewModel }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_pin, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // setting max length for pin
        pinLength = confHelper.pinLength
        val pinHint = getString(R.string.new_pin, pinLength)
        pinLayout.hint = pinHint
        pinEditText.contentDescription = pinHint

        val inputFilter = InputFilter.LengthFilter(pinLength)
        pinEditText.filters = arrayOf(inputFilter)
        confirmPinEditText.filters = arrayOf(inputFilter)

        val pinObservable = RxTextView.textChanges(pinEditText).map(CharSequence::toString)

        val confirmPinObservable =
            RxTextView.textChanges(confirmPinEditText).doOnNext { confirmPinLayout.error = "" }
                    .map(CharSequence::toString)

        subscriptions.add(Observable.combineLatest(
            pinObservable, confirmPinObservable, this::isValid
        ).doOnNext {
            saveButton.isEnabled = it
        }.subscribe()
        )

        pinEditText.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->

            if (!hasFocus) {
                val pinStr = pinEditText.text ?: ""
                when {
                    pinStr.isEmpty() -> {
                        pinLayout.error = getString(R.string.msg_new_pin_required)
                    }
                    pinStr.length != pinLength -> {
                        pinLayout.error = getString(R.string.msg_new_pin_invalid, pinLength)
                    }
                    else -> {
                        pinLayout.error = ""
                    }
                }
            } else {
                pinLayout.error = ""
            }
        }

        confirmPinEditText.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val pinStr = confirmPinEditText.text ?: ""
                when {
                    pinStr.isEmpty() -> {
                        confirmPinLayout.error = getString(R.string.msg_confirm_new_pin_required)
                    }
                    pinStr.length != pinLength -> {
                        confirmPinLayout.error =
                            getString(R.string.msg_confirm_new_pin_invalid, pinLength)
                    }
                    else -> {
                        confirmPinLayout.error = ""
                    }
                }
            } else {
                confirmPinLayout.error = ""
            }
        }

        saveButton.setOnClickListener {
            val inputMethodManager =
                context!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(confirmPinEditText.windowToken, 0)
            postDelay(200) {
                if (isOnline()) {
                    viewModel.changePin(
                        pinEditText.text.toString(), confirmPinEditText.text.toString()
                    )
                } else {
                    connectionErrorDialog { }
                }
            }
        }

        initializeObserver()
    }

    fun isValid(pin: String, confirmPin: String): Boolean {
        if (pin.length == pinLength && confirmPin.length == pinLength) {
            return true
        }
        return false
    }

    private fun initializeObserver() {
        observeDisplayLoadingLiveData()
        observeErrorLiveData()
        observeResultLiveData()
    }

    private fun observeResultLiveData() {
        viewModel.resultData.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it) {
                    resultStateLayout.showEmpty(
                        R.drawable.check_material, getString(R.string.success),
                        getString(R.string.msg_pin_change_success), getString(R.string.fare_medium_card_action_done)
                    ) {
                        activity!!.finish()
                    }
                    resultStateLayout.announceForAccessibility(
                        getString(R.string.msg_pin_change_success)
                    )
                } else {
                    showDefaultErrorDialog(
                        getString(R.string.popup_error), getString(R.string.msg_pin_change_failed),
                        false
                    )
                }
            }
        })
    }

    private fun observeDisplayLoadingLiveData() {
        viewModel.displayLoading.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it) {
                    resultStateLayout.showLoading(
                        R.drawable.tickets_material, getString(R.string.loading)
                    )
                    resultStateLayout.announceForAccessibility(
                        getString(R.string.changing_pin_voonly)
                    )
                } else {
                    resultStateLayout.showContent()
                }
            }
        })
    }


    private fun observeErrorLiveData() {
        viewModel.errorData.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.code == BytemarkSDK.ResponseCode.ERROR_CODE_PIN_MISMATCH) {
                    val message = getString(R.string.msg_pin_mismatch)
                    confirmPinLayout.requestFocus()
                    confirmPinLayout.error = message
                    confirmPinLayout.announceForAccessibility(message)
                } else {
                    handleError(it)
                }
            }
        })
    }

    override fun showDefaultErrorDialog(
        title: String?, errorMsg: String?, finishActivity: Boolean
    ) {
        resultStateLayout.showError(
            R.drawable.error_material, title, errorMsg, getString(R.string.back_to_manage_pin)
        ) {
            resultStateLayout.showContent()
        }
        resultStateLayout.announceForAccessibility(errorMsg)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.clear()
    }

    override fun onDestroyView() {
        subscriptions.clear()
        super.onDestroyView()
    }


    companion object {
        const val EXTRA_TAG = "MANAGE_PIN"
        fun newInstance() = ManagePinFragment()
    }
}