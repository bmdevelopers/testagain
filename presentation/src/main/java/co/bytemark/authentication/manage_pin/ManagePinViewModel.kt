package co.bytemark.authentication.manage_pin

import androidx.lifecycle.MutableLiveData
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.authentication.ChangePinRequest
import co.bytemark.domain.interactor.authentication.ChangePinUseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import kotlinx.coroutines.launch
import javax.inject.Inject

class ManagePinViewModel @Inject constructor(
        val changePinUseCase: ChangePinUseCase
) : BaseViewModel() {

    val resultData by lazy {
        MutableLiveData<Boolean>()
    }
    val displayLoading by lazy {
        MutableLiveData<Boolean>()
    }

    val errorData by lazy {
        MutableLiveData<BMError>()
    }

    fun changePin(pin: String, confirmPin: String) =
            uiScope.launch {
                displayLoading.postValue(true)
                val result = changePinUseCase(ChangePinRequest(pin, confirmPin))
                displayLoading.postValue(false)
                when (result) {
                    is Result.Success -> {
                        resultData.postValue(true)
                    }
                    is Result.Failure -> {
                        errorData.postValue(result.bmError.first())
                    }
                }
            }

}