package co.bytemark.authentication;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;


import java.util.List;

import co.bytemark.domain.model.authentication.Formly;
import co.bytemark.mvp.MvpView;

/**
 * Created by Arunkumar on 21/07/17.
 */

public interface BaseFormlyView extends MvpView {
    @MainThread
    void setForms(@NonNull List<Formly> formlyList);

    void showDefaultErrorDialog();

    void showLoading();

    void hideLoading();

    void showErrorMessage(int errorCode, @NonNull String userFacingMessage);

    void closeSession(); // for back-press action in-case of hot-listed device

    void showAppUpdateDialog();
}
