package co.bytemark.authentication.voucher_code;


import javax.inject.Inject;

import co.bytemark.authentication.BaseFormlyPresenter;
import co.bytemark.authentication.BaseFormlyView;
import co.bytemark.domain.interactor.authentication.SubmitVoucherCode;
import co.bytemark.domain.interactor.authentication.SubmitVoucherCodeRequestValues;
import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.helpers.RxUtils;
import co.bytemark.mvp.MvpView;
import co.bytemark.sdk.passcache.PassCacheManager;
import rx.Observable;
import rx.SingleSubscriber;
import timber.log.Timber;

/**
 * Created by Omkar on 9/7/17.
 */
public interface VoucherCode {

    interface View extends MvpView, BaseFormlyView {
        void submitVoucherCodeSuccessFull();
    }

    class Presenter extends BaseFormlyPresenter<View> {
        private final SubmitVoucherCode submitVoucherCode;

        @Inject
        public Presenter(ConfHelper confHelper,
                         RxUtils rxUtils,
                         SubmitVoucherCode submitVoucherCode) {
            super(confHelper, rxUtils);
            this.submitVoucherCode = submitVoucherCode;
        }


        void loadForms() {
            subs.add(Observable.fromCallable(confHelper::getVoucherCodeForms)
                    .filter(forms -> !forms.isEmpty())
                    .compose(rxUtils.applySchedulers())
                    .doOnNext(forms -> {
                        if (isViewAttached()) {
                            getView().setForms(forms);
                        }
                    }).doOnError(Timber::e)
                    .subscribe());
        }

        void submitVoucherCode() {
            if (isViewAttached()) {
                getView().showLoading();
            }
            submitVoucherCode.singleExecute(new SubmitVoucherCodeRequestValues(formlyMap), new SingleSubscriber<BMResponse>() {
                @Override
                public void onSuccess(BMResponse bmResponse) {
                    if (isViewAttached()) {
                        PassCacheManager.get().evictAll();
                        getView().submitVoucherCodeSuccessFull();
                    }
                }

                @Override
                public void onError(Throwable error) {
                    handleError(error);
                }
            });
        }

        void destroy() {
            subs.clear();
        }
    }
}
