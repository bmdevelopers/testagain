package co.bytemark.authentication.voucher_code;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.app.TaskStackBuilder;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.analytics.AnalyticsPlatformsContract;
import co.bytemark.authentication.AuthenticationActivity;
import co.bytemark.base.BaseMvpFragment;
import co.bytemark.domain.model.authentication.Formly;
import co.bytemark.formly.adapter.FormlyAdapter;
import co.bytemark.use_tickets.UseTicketsActivity;
import co.bytemark.widgets.CheckMarkView;
import co.bytemark.widgets.LinearRecyclerView;
import co.bytemark.widgets.transition.SuperAutoTransition;
import rx.subscriptions.CompositeSubscription;

import static android.transition.TransitionManager.beginDelayedTransition;
import static android.view.Gravity.CENTER;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static co.bytemark.domain.model.authentication.Formly.VOUCHER_CODE;
import static co.bytemark.widgets.util.Util.dpToPx;

/**
 * A simple {@link Fragment} subclass.
 */
public class VoucherCodeFragment extends BaseMvpFragment<VoucherCode.View, VoucherCode.Presenter>
        implements VoucherCode.View {

    @Inject
    VoucherCode.Presenter presenter;
    @BindView(R.id.voucher_code_list)
    LinearRecyclerView voucherCodeList;
    @BindView(R.id.root_layout)
    NestedScrollView rootScrollLayout;
    @BindView(R.id.list_container)
    LinearLayout listContainer;
    @BindView(R.id.voucher_code_success)
    TextView voucherCodeSuccess;
    @BindView(R.id.checkmark)
    CheckMarkView checkmark;

    private FormlyAdapter formlyAdapter;

    public static final String EXTRA_TAG = "VOUCHER_CODE";

    private final CompositeSubscription subs = new CompositeSubscription();

    @Inject
    AnalyticsPlatformAdapter analyticsPlatformAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        postponeEnterTransition();
        AuthenticationActivity activity = (AuthenticationActivity) getActivity();
        if (activity != null && activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setTitle(getString(R.string.voucher_code));
            activity.enableOrDisableDrawerHack(true);
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        menu.clear();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        voucherCodeList.setLayoutManager(new LinearLayoutManager(getContext()));
        formlyAdapter = new FormlyAdapter(voucherCodeList, getFragmentManager());
        subs.add(formlyAdapter.buttonClicks().subscribe(this::onButtonClicked));
        subs.add(formlyAdapter.textChanges().subscribe(textChanges -> presenter.updateFormlyModel(textChanges)));
        checkmark.setMaterialWidth(dpToPx(2));
        voucherCodeList.setAdapter(formlyAdapter);
        presenter.loadForms();
    }

    private void onButtonClicked(Formly formly) {
        hideKeyboard();
        switch (formly.getKey()) {
            case VOUCHER_CODE:
                formlyAdapter.enableValidation();
                if (formlyAdapter.isValid()) {
                    if (isOnline()) {
                        presenter.submitVoucherCode();
                    } else {
                        connectionErrorDialog();
                    }
                }
                break;
        }
    }

    @Override
    public void setForms(@NonNull List<Formly> formlyList) {
        analyticsPlatformAdapter.voucherCodeScreenDisplayed();
        formlyAdapter.setFormlyList(formlyList);
        voucherCodeList.post(this::startPostponedEnterTransition);
    }

    @Override
    public void showDefaultErrorDialog() {
        hideLoading();
        new MaterialDialog.Builder(getActivity())
                .title(R.string.voucher_popup_could_not_submit)
                .positiveText(android.R.string.ok)
                .content(R.string.voucher_code_popup_could_not_submit)
                .show();
    }

    @Override
    public void showAppUpdateDialog() {
        hideLoading();
        super.showAppUpdateDialog();
    }

    @Override
    public void submitVoucherCodeSuccessFull() {
        analyticsPlatformAdapter.voucherCodeApplied(AnalyticsPlatformsContract.Status.SUCCESS, "");
        hideLoading();
        final SuperAutoTransition hideContent = new SuperAutoTransition();
        hideContent.setDuration(300);
        hideContent.setInterpolator(new LinearOutSlowInInterpolator());
        hideContent.doOnEnd(() -> {
            final SuperAutoTransition superAutoTransition = new SuperAutoTransition();
            superAutoTransition.setDuration(300);
            superAutoTransition.doOnEnd(() -> {
                checkmark.setSelected(true);
                checkmark.postDelayed(() -> {
                    if (isAdded()) {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            createBackStack(new Intent(getContext(), UseTicketsActivity.class));
                        }
                    }
                }, 400);
            });
            beginDelayedTransition(rootScrollLayout, superAutoTransition);
            if (getView() != null) {
                getView().announceForAccessibility(voucherCodeSuccess.getText());
            }
            voucherCodeSuccess.setVisibility(VISIBLE);
            checkmark.setVisibility(VISIBLE);
        });

        beginDelayedTransition(rootScrollLayout, hideContent);
        voucherCodeList.setVisibility(GONE);
        listContainer.setGravity(CENTER);
    }

    private void createBackStack(Intent intent) {
        TaskStackBuilder builder = TaskStackBuilder.create(getActivity());
        builder.addNextIntentWithParentStack(intent);
        builder.startActivities();
    }

    @Override
    public void showLoading() {
        hideKeyboard();
        formlyAdapter.setLoading(VOUCHER_CODE, true);
    }

    @Override
    public void hideLoading() {
        hideKeyboard();
        formlyAdapter.setLoading(VOUCHER_CODE, false);
    }

    @Override
    public void showErrorMessage(int errorCode, @NonNull String userFacingMessage) {
        analyticsPlatformAdapter.voucherCodeApplied(AnalyticsPlatformsContract.Status.FAILURE, userFacingMessage);
        hideLoading();
        new MaterialDialog.Builder(getActivity())
                .title(R.string.voucher_code_popup_cant_submit)
                .positiveText(android.R.string.ok)
                .content(userFacingMessage)
                .show();
    }

    @Override
    public void closeSession() {
        getActivity().onBackPressed();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_voucher_code;
    }

    @Override
    protected void onInject() {
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    @Override
    protected void onOnline() {

    }

    @Override
    protected void onOffline() {

    }

    @Override
    protected void setAccentThemeColors() {
        rootScrollLayout.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        voucherCodeSuccess.setTextColor(confHelper.getBackgroundThemePrimaryTextColor());
        checkmark.setCheckMarkColor(confHelper.getBackgroundThemeAccentColor());
    }

    @Override
    protected void setBackgroundThemeColors() {

    }

    @Override
    protected void setDataThemeColors() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    @NonNull
    @Override
    public VoucherCode.Presenter createPresenter() {
        return presenter;
    }

    public static VoucherCodeFragment newInstance() {
        return new VoucherCodeFragment();
    }
}

