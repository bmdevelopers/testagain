package co.bytemark.authentication;

import android.util.Pair;


import java.util.HashMap;
import java.util.Map;

import co.bytemark.domain.error.BytemarkException;
import co.bytemark.domain.model.authentication.Formly;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.helpers.RxUtils;
import co.bytemark.mvp.MvpBasePresenter;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.network_impl.BaseNetworkRequest;
import retrofit2.HttpException;
import rx.subscriptions.CompositeSubscription;

import static android.text.TextUtils.isEmpty;

/**
 * Created by Arunkumar on 21/07/17.
 * <p>
 * Presenter abstraction to handle errors.
 */
public abstract class BaseFormlyPresenter<V extends BaseFormlyView> extends MvpBasePresenter<V> {
    protected final ConfHelper confHelper;
    protected final RxUtils rxUtils;
    protected final CompositeSubscription subs = new CompositeSubscription();
    protected final Map<String, String> formlyMap = new HashMap<>();

    protected BaseFormlyPresenter(ConfHelper confHelper, RxUtils rxUtils) {
        this.confHelper = confHelper;
        this.rxUtils = rxUtils;
    }

    protected void handleError(Throwable throwable) {
        if (isViewAttached()) {
            if (throwable instanceof BytemarkSDK.SpecialException) {
                if (((BytemarkSDK.SpecialException) throwable).getErrorCode() == BytemarkSDK.ResponseCode.SDK_VERSION_OUT_OF_DATE) {
                    getView().showAppUpdateDialog();
                    return;
                }
            }
            if (throwable instanceof BytemarkException) {
                handleBytemarkException((BytemarkException) throwable);
            } else if (throwable instanceof HttpException) {

                HttpException httpException = (HttpException) throwable;

                if (httpException.code() == BaseNetworkRequest.UNAUTHORISED) {
                    getView().closeSession(); // Logout is performed inside onError of UseCase.getObservable() method
                } else {
                    final BytemarkException bytemarkException = BytemarkException.fromHttpException(httpException);
                    handleBytemarkException(bytemarkException);
                }

            } else {
                getView().showDefaultErrorDialog();
            }
        }
    }

    protected void handleBytemarkException(BytemarkException bytemarkException) {
        if (isViewAttached()) {
            if (bytemarkException.getStatusCode() == BytemarkSDK.ResponseCode.SDK_VERSION_OUT_OF_DATE) {
                getView().showAppUpdateDialog();
            } else if (bytemarkException != null && !isEmpty(bytemarkException.getUserFacingMessage())) {
                getView().showErrorMessage(bytemarkException.getStatusCode(), bytemarkException.getUserFacingMessage());
            } else {
                getView().showDefaultErrorDialog();
            }
        }
    }

    /**
     * Responsible for continuously updating post body for register request.
     *
     * @param values Text change pair with formly object attached
     */
    public void updateFormlyModel(Pair<Formly, String> values) {
        formlyMap.put(values.first.getKey(), values.second.trim());
    }
}
