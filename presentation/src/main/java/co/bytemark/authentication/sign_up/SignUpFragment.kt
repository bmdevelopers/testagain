package co.bytemark.authentication.sign_up

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.transition.TransitionManager
import android.util.Pair
import android.view.*
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.authentication.AuthenticationActivity
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.common.BMError
import co.bytemark.formly.adapter.FormlyAdapter
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.BytemarkSDK.logout
import co.bytemark.sdk.model.common.User
import co.bytemark.securityquestion.SecurityQuestionActivity
import co.bytemark.use_tickets.UseTicketsActivity
import co.bytemark.widgets.transition.SuperAutoTransition
import co.bytemark.widgets.util.*
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.fragment_sign_up.*
import rx.subscriptions.CompositeSubscription
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SignUpFragment : BaseMvvmFragment() {

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    @Inject
    lateinit var viewModel: SignUpViewModel

    override fun onInject() = CustomerMobileApp.component.inject(this)

    private lateinit var formlyAdapter: FormlyAdapter

    private val subs = CompositeSubscription()

    private var enterTime: Long = 0

    private var formSubmitTime: Long = 0

    private val formlyMap = mutableMapOf<String, Any?>()

    companion object {
        @JvmStatic
        fun newInstance() = SignUpFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postponeEnterTransition()
        enterTime = System.currentTimeMillis()
        val activity = activity as AuthenticationActivity?
        if (activity?.supportActionBar != null) {
            activity.supportActionBar?.title = getString(R.string.sign_up)
            activity.enableOrDisableDrawerHack(true)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_sign_up, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = getViewModel(CustomerMobileApp.appComponent.signUpViewModel)
        viewModel.logAccessTime()

        formlyAdapter = FormlyAdapter(recyclerViewSignUp, fragmentManager)

        subs.add(formlyAdapter.buttonClicks().subscribe { formly: Formly ->
            onAdapterButtonClicked(formly)
        })

        subs.add(formlyAdapter.textChanges().subscribe { textChanges: Pair<Formly, String> ->
            viewModel.updateLoginModel(textChanges)
            updateFormlyModel(textChanges)
        })

        subs.add(formlyAdapter.dropdownSelects().subscribe { values: Pair<Formly, String> ->
            updateFormlyModel(values)
        })

        checkmarkSignUp.setMaterialWidth(Util.dpToPx(2.0).toFloat())
        recyclerViewSignUp.adapter = formlyAdapter

        viewModel.getFormData()

        analyticsPlatformAdapter.signUpScreenDisplayed()

        with(viewModel) {
            formData.observe(this@SignUpFragment, Observer {
                it?.let {
                    formlyAdapter.setFormlyList(it)
                    recyclerViewSignUp.post { startPostponedEnterTransition() }
                }
            })

            errorLiveData.observe(this@SignUpFragment, Observer {
                hideLoadingTransition()
                it?.let {
                    if (it.code == BytemarkSDK.ResponseCode.ACCOUNT_ACTIVATION_REQUIRED) {
                        showErrorDialogOnSignUp(it.message)
                    } else {
                        handleError(it)
                    }
                }
            })

            isEmailVerificationSent.observe(this@SignUpFragment, Observer {
                it?.let {
                    if (it) {
                        showEmailVerificationDialog()
                    }
                }
            })

            userData.observe(this@SignUpFragment, Observer {
                it?.let {
                    onUserSignedIn(it)
                }
            })

            isEmailVerificationSuccessful.observe(this@SignUpFragment, Observer {
                it?.let {
                    handleEmailVerificationResponse(it)
                }
            })
        }
    }

    private fun onUserSignedIn(user: User) {
        if (isAdded) {
            progressBar.invisible()
            val superAutoTransition = SuperAutoTransition()
            superAutoTransition.duration = 300
            superAutoTransition.doOnEnd(Runnable {
                checkmarkSignUp?.isSelected = true
                checkmarkSignUp?.postDelayed(Runnable {
                    if (activity?.isFinishing == false) {
                        if (activity?.isTaskRoot == true) {
                            startActivity(Intent(activity, UseTicketsActivity::class.java))
                        }
                        finishWithResult(BytemarkSDK.ResponseCode.SUCCESS)
                    }
                }, 500)
            })
            authenticationUserName?.text = ""
            rootLayoutSignUp.clearAnimation()
            TransitionManager.beginDelayedTransition(rootLayoutSignUp, superAutoTransition)
            if (!TextUtils.isEmpty(user.fullName)) {
                authenticationUserName?.text = user.fullName
            } else if (!TextUtils.isEmpty(user.email)) {
                authenticationUserName?.text = user.email
            }
            authenticationLoggedIn?.show()
            authenticationUserName?.show()
            checkmarkSignUp?.show()
            view?.announceForAccessibility(authenticationLoggedIn.text.toString() + " " + authenticationUserName.text)
            val formFillDuration =
                TimeUnit.SECONDS.convert(formSubmitTime - enterTime, TimeUnit.MILLISECONDS)
            val accountSignUpDuration = TimeUnit.SECONDS.convert(
                formSubmitTime - BytemarkSDK.SDKUtility.getFirstOpenTimeStamp(),
                TimeUnit.MILLISECONDS
            )
            analyticsPlatformAdapter.signUp(
                accountSignUpDuration,
                formFillDuration,
                AnalyticsPlatformsContract.Status.SUCCESS,
                ""
            )
        } else {
            hideLoadingTransition()
        }

        if(confHelper.isElertSdkEnabled()) {
            Util.regUserAndOrgForECUI(activity?.application!!)
        }
    }

    private fun handleEmailVerificationResponse(status: Boolean) {
        var message = getString(R.string.msg_verification_email_sent)
        if (!status) {
            // unable to send verification email
            message = getString(R.string.msg_problem_sending_verification_email)
        }
        activity?.let {
            MaterialDialog.Builder(it)
                .title(R.string.resend_verification_email)
                .positiveText(android.R.string.ok)
                .content(message)
                .onPositive { dialog: MaterialDialog?, which: DialogAction? ->
                    hideLoadingTransition()
                    logout()
                }
                .show()
        }
    }

    private fun showEmailVerificationDialog() {
        BytemarkSDK.setAwaitingLogout(context)
        activity?.let {
            MaterialDialog.Builder(it)
                .title(R.string.APP_NAME)
                .content(R.string.sign_up_email_verification_required)
                .positiveText(android.R.string.ok)
                .negativeText(R.string.resend_verification_email)
                .cancelable(false)
                .onPositive { dialog: MaterialDialog, which: DialogAction? ->
                    dialog.dismiss()
                    logout()
                }
                .onNegative { dialog: MaterialDialog, which: DialogAction? ->
                    // resend Verification email
                    showLoadingTransition(getString(R.string.msg_request_verification_email))
                    viewModel.resendVerificationEmail()
                    dialog.dismiss()
                }
                .show()
        }
    }

    private fun onAdapterButtonClicked(formly: Formly) {
        if (Formly.SIGN_UP_KEY == formly.key) {
            hideKeyboard()
            formlyAdapter.enableValidation()
            if (formlyAdapter.isValid()) {
                if (!isOnline()) {
                    connectionErrorDialog()
                    return
                }
                if (confHelper.isSecurityQuestionsEnabled) {
                    navigateToSecurityQuestion()
                    return
                }
                showLoadingTransition(getString(R.string.sign_up_creating_account))
                formSubmitTime = System.currentTimeMillis()
                viewModel.signUp(formlyMap)
            }
        }
    }

    private fun navigateToSecurityQuestion() {
        val intent = Intent(this.activity, SecurityQuestionActivity::class.java)
        intent.putExtra(AppConstants.SECURITY_QUESTIONS_SIGN_UP, true)
        intent.putExtra(AppConstants.TITLE, getString(R.string.sign_up))
        startActivityForResult(intent, AppConstants.SECURITY_QUESTIONS_REQUEST_CODE)
    }

    private fun showErrorDialogOnSignUp(errorMsg: String) {
        context?.let {
            MaterialDialog.Builder(it)
                .title(R.string.popup_success)
                .content(errorMsg)
                .cancelable(false)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.dataThemeAccentColor)
                .onPositive { dialog, _ ->
                    dialog.dismiss()
                    activity?.onBackPressed()
                }
                .show()
            val formFillDuration = TimeUnit.MILLISECONDS.convert(
                System.currentTimeMillis() - enterTime,
                TimeUnit.SECONDS
            )
            val accountSignUpDuration = TimeUnit.MILLISECONDS.convert(
                System.currentTimeMillis() - BytemarkSDK.SDKUtility.getFirstOpenTimeStamp(),
                TimeUnit.SECONDS
            )
            analyticsPlatformAdapter.signUp(
                accountSignUpDuration,
                formFillDuration,
                AnalyticsPlatformsContract.Status.FAILURE,
                errorMsg
            )
        }
    }

    private fun showLoadingTransition(message: String?) {
        val hideContent = SuperAutoTransition()
        hideContent.interpolator = LinearOutSlowInInterpolator()
        TransitionManager.beginDelayedTransition(rootLayoutSignUp, hideContent)
        recyclerViewSignUp?.hide()
        listContainerSignUp?.gravity = Gravity.CENTER
        authenticationUserName?.show()
        authenticationUserName?.text = message
        progressBar?.show()
        showActionBar(false)
        Handler().postDelayed({
                                  view?.announceForAccessibility(message)
                              }, 300) //Small Delay For accessibility to work
    }

    private fun hideLoadingTransition() {
        rootLayoutSignUp.clearAnimation()
        Handler().postDelayed({
                                  //Small Delay Require For clearing animation
                                  recyclerViewSignUp?.show()
                                  authenticationUserName?.invisible()
                                  authenticationUserName?.text = ""
                                  progressBar?.invisible()
                                  showActionBar(true)
                              }, 500)
    }

    private fun showActionBar(show: Boolean) {
        if (activity != null && (activity as AuthenticationActivity?)?.supportActionBar != null) {
            if (show) {
                (activity as AuthenticationActivity?)?.supportActionBar?.show()
            } else {
                (activity as AuthenticationActivity?)?.supportActionBar?.hide()
            }
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val item = menu.findItem(R.id.action_notifications)
        item?.isVisible = false
    }

    private fun updateFormlyModel(values: Pair<Formly, String>) {
        formlyMap[values.first.key] = values.second.trim()
    }

    private fun finishWithResult(result: Int) {
        activity?.setResult(result)
        activity?.finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppConstants.SECURITY_QUESTIONS_REQUEST_CODE) {
            handleSecurityResult(resultCode, data)
        }
    }

    private fun handleSecurityResult(resultCode: Int, data: Intent?) {
        if (data != null && data.extras != null) {
            if (resultCode == Activity.RESULT_OK) {
                viewModel.setSecurityQuestions(data.extras?.getParcelableArrayList(AppConstants.SECURITY_QUESTIONS_DATA))
                showLoadingTransition(getString(R.string.sign_up_creating_account))
                viewModel.signUp(formlyMap)
            } else {
                val error: BMError? =
                    data.extras?.getParcelable(AppConstants.SECURITY_QUESTIONS_DATA)
                error?.let { handleError(it) }
            }
        }
    }

    private fun logout() {
        logout(context)
        // take user to Login screen
        if (activity != null && isAdded) {
            fragmentManager?.popBackStack()
        }
    }

    override fun setUpColors() {
        super.setUpColors()
        rootLayoutSignUp.setBackgroundColor(confHelper.backgroundThemeBackgroundColor)
        checkmarkSignUp.setCheckMarkColor(confHelper.backgroundThemeAccentColor)
    }

    override fun showAppUpdateDialog() {
        super.showAppUpdateDialog()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        subs.clear()
        showActionBar(true)
        formlyAdapter.cleanUp()
    }
}