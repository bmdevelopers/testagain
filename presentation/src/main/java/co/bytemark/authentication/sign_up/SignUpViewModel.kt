package co.bytemark.authentication.sign_up

import android.util.Pair
import androidx.lifecycle.MutableLiveData
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.authentication.GetOAuthTokenUseCase
import co.bytemark.domain.interactor.authentication.RegisterNewUserUseCase
import co.bytemark.domain.interactor.authentication.ResendVerificationEmailUseCase
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.common.Result
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.data.userAccount.UserAccountRepository
import co.bytemark.sdk.model.common.User
import co.bytemark.sdk.model.securityquestions.SecurityQuestionInfo
import com.facebook.login.LoginManager
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class SignUpViewModel @Inject constructor(
        private val resendVerificationEmailUseCase: ResendVerificationEmailUseCase,
        private val registerNewUserUseCase: RegisterNewUserUseCase,
        private val getOAuthTokenUseCase: GetOAuthTokenUseCase,
        private val confHelper: ConfHelper, private val userAccountRepository: UserAccountRepository
) : BaseViewModel() {

    private var securityQuestionInfoList: List<SecurityQuestionInfo>? = null
    private val loginParams = TreeMap<String, String?>()

    val formData by lazy {
        MutableLiveData<MutableList<Formly>>()
    }

    var isEmailVerificationSent = MutableLiveData<Boolean>().apply {
        value = false
    }

    val userData by lazy {
        MutableLiveData<User>()
    }

    var isEmailVerificationSuccessful = MutableLiveData<Boolean>().apply {
        value = null
    }

    fun setSecurityQuestions(securityQuestions: List<SecurityQuestionInfo>?) {
        securityQuestionInfoList = securityQuestions
    }

    fun getFormData() {
        formData.value = confHelper.supportedFormlySignUpList
    }

    fun signUp(params: MutableMap<String, Any?>) {
        uiScope.launch {
            if (securityQuestionInfoList != null) {
                params["security_questions"] = securityQuestionInfoList
            }
            val resultValues = registerNewUserUseCase.RegisterNewUserRequestValues(params)
            when (val result = registerNewUserUseCase(resultValues)) {
                is Result.Failure -> {
                    if (result.bmError.first().code == BytemarkSDK.ResponseCode.UNEXPECTED_ERROR) {
                        loginUser()
                    } else {
                        LoginManager.getInstance().logOut()
                        userData.value = null
                        errorLiveData.value = result.bmError.first()
                    }
                }

                else -> {
                    loginUser()
                }
            }
        }
    }

    private fun loginUser() {
        uiScope.launch {
            when (val loginResult =
                    getOAuthTokenUseCase(GetOAuthTokenUseCase.SignInRequestValues(loginParams as MutableMap<String, String>))) {
                is Result.Failure -> {
                    errorLiveData.value = loginResult.bmError.first()
                }

                is Result.Success -> {
                    if (securityQuestionInfoList?.isEmpty() == true) {
                        BytemarkSDK.SDKUtility.setIsUserUpdatedSecurityQuestions(true)
                    }

                    loginResult.data?.oauthToken?.let {
                        BytemarkSDK.doLogin(it).map { obj ->
                            obj.data
                        }
                    }?.subscribe {
                        if (it?.organizationDetails?.requiresEmailVerification != null
                                && it.organizationDetails?.requiresEmailVerification == true
                                && it.user?.isEmailVerified == false
                        ) {
                            isEmailVerificationSent.value = true
                        } else {
                            userData.value = it.user
                        }
                    }
                }
            }
        }
    }

    fun resendVerificationEmail() {
        uiScope.launch {
            when (val result = resendVerificationEmailUseCase(Unit)) {
                is Result.Failure -> {
                    if (result.bmError.first().code == BytemarkSDK.ResponseCode.TIME_INCONSISTENCY_DETECTED ||
                            result.bmError.first().code == BytemarkSDK.ResponseCode.UNEXPECTED_ERROR
                    ) {
                        errorLiveData.value = result.bmError.first()
                    } else {
                        isEmailVerificationSuccessful.value = false
                    }
                }

                is Result.Success -> {
                    isEmailVerificationSuccessful.value = true
                }
            }
        }
    }

    fun updateLoginModel(values: Pair<Formly, String>) {
        when (values.first.key) {
            Formly.USERNAME_KEY, Formly.PASSWORD_KEY, Formly.EMAIL_KEY -> {
                loginParams[values.first.key] = values.second
            }
        }
    }

    fun logAccessTime() {
        userAccountRepository.insertOrUpdateLastAccessTime(GregorianCalendar())
    }
}



