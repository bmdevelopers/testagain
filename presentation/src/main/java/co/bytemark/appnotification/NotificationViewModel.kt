package co.bytemark.appnotification

import android.app.Application
import android.content.Intent
import android.transition.TransitionInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import co.bytemark.MainActivity
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.base.MasterActivity
import co.bytemark.domain.interactor.notification.GetNotificationsUseCase
import co.bytemark.domain.interactor.notification.MarkNotificationAsReadUseCase
import co.bytemark.domain.interactor.notification.NotificationRequestValues
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.common.Navigate
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.notification.Notification
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.webview.WebViewActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class NotificationViewModel @Inject constructor(
        var confHelper: ConfHelper,
        var application: Application,
        var getNotificationsUseCase: GetNotificationsUseCase,
        var markNotificationAsRead: MarkNotificationAsReadUseCase,
        var analyticsPlatformAdapter: AnalyticsPlatformAdapter
) : ViewModel(), LifecycleObserver {

    val navigationLiveData by lazy {
        MutableLiveData<Navigate>()
    }

    val displayLiveData by lazy {
        MutableLiveData<Display>()
    }

    val errorLiveData by lazy {
        MutableLiveData<BMError>()
    }

    val visibilityStateLiveData by lazy {
        MutableLiveData<VisibilityState>()
    }

    val accessibilityLiveData by lazy {
        MutableLiveData<TalkBack>()
    }

    val markNotificationReadLiveData by lazy {
        MutableLiveData<Result<Boolean>>()
    }

    val notificationLiveData by lazy {
        MutableLiveData<MutableList<Notification>>()
    }

    private var navigateFromSignIn = false

    var isOnline = false
        set(value) {
            field = value
            loadNotifications()
        }

    fun loadNotifications() = viewModelScope.launch {
        displayLiveData.value = Display.SwipeRefreshLayout(true)
        val result = getNotificationsUseCase(Unit)
        displayLiveData.value = Display.SwipeRefreshLayout(false)
        when (result) {
            is Result.Success -> {
                result.data?.let {
                    handleNotifications(
                            it.notifications?.toMutableList()
                                    ?: mutableListOf()
                    )
                    Timber.d("load Notification Success")
                }
            }

            is Result.Failure -> {
                displayLiveData.value = Display.SwipeRefreshLayout(false)
                displayLiveData.value = Display.EmptyState.Error(
                        R.drawable.error_material,
                        R.string.change_password_popup_title_error,
                        R.string.something_went_wrong
                )
                errorLiveData.value = result.bmError.first()
                Timber.d("load Notification Failure")
            }
        }
    }

    fun markNotificationAsRead(id: String) = viewModelScope.launch {
        markNotificationReadLiveData.value = markNotificationAsRead(NotificationRequestValues(id))
    }

    fun handleNotifications(notifications: MutableList<Notification>) {

        displayLiveData.value = Display.EmptyState.ShowContent()
        visibilityStateLiveData.value = VisibilityState.LoginLayout(View.INVISIBLE)
        notificationLiveData.value = notifications

        when {
            notifications.isNotEmpty() -> {
                visibilityStateLiveData.value =
                        VisibilityState.SubscribedLayout(if (BytemarkSDK.isLoggedIn()) GONE else VISIBLE)
            }

            isOnline.not() -> {
                displayLiveData.value = Display.EmptyState.ShowContent()
                errorLiveData.value = BMError(BytemarkSDK.ResponseCode.NETWORK_NOT_AVAILABLE)
            }
            else -> {
                handleNoNotification()
            }
        }
    }

    private fun handleNoNotification() {
        visibilityStateLiveData.run {
            value = VisibilityState.LoginLayout(VISIBLE)
            if (BytemarkSDK.isLoggedIn()) {
                value = VisibilityState.SubscribedLayout(GONE)
                value = VisibilityState.SignInButton(GONE)
            } else {
                value = VisibilityState.SubscribedLayout(VISIBLE)
                value = VisibilityState.SignInButton(VISIBLE)
                accessibilityLiveData.value = TalkBack.LoginLayout(R.string.you_are_signed_out)
            }
        }
    }

    fun onSwipeRefresh() = loadNotifications()

    fun onSignInClick() {
        if (confHelper.isAuthenticationNative) {
            navigationLiveData.value = Navigate.FinishActivity()
        } else {
            if (isOnline)
                navigationLiveData.value = Navigate.StartActivityForResult(
                        Intent(application, MainActivity::class.java),
                        MasterActivity.REQUEST_CODE_LOGIN
                )
            else
                displayLiveData.value = Display.SnackBar(
                        R.string.network_connectivity_error_message,
                        Snackbar.LENGTH_LONG
                )
        }
        navigateFromSignIn = true
    }

    fun onConnectionError() {
        displayLiveData.value = Display.EmptyState.Error(
                R.drawable.error_material,
                R.string.network_connectivity_error,
                R.string.network_connectivity_error_message
        )
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onLifeCycleStart() {
        displayLiveData.value =
                Display.EmptyState.Loading(R.drawable.notification_material, R.string.loading)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onLifeCycleResume() {
        if (navigateFromSignIn && BytemarkSDK.isLoggedIn()) {
            loadNotifications()
        }
        navigateFromSignIn = false
    }

    fun onNotificationClick(notification: Notification) {
        markNotificationAsRead(notification.id)
        if (notification.body.isEmpty() && !notification.link.isNullOrEmpty()) {
            navigationLiveData.value =
                    Navigate.StartActivity(Intent(application, WebViewActivity::class.java).apply {
                        putExtra(
                                WebViewActivity.EXTRA_TITLE,
                                application.getString(R.string.screen_title_notification)
                        )
                        putExtra(WebViewActivity.EXTRA_URL, notification.link)
                        putExtra(WebViewActivity.EXTRA_PDF_SUPPORT, true)
                        putExtra(WebViewActivity.EXTRA_NOTIFICATION, notification)
                    })
        } else {
            navigationLiveData.value =
                    Navigate.AddFragment(NotificationDetailFragmentNew.newInstance(notification).apply {
                        sharedElementEnterTransition = TransitionInflater.from(application)
                                .inflateTransition(R.transition.movement_transition)
                        enterTransition = TransitionInflater.from(application)
                                .inflateTransition(android.R.transition.fade)
                    }, R.id.fragment)
        }
    }


    sealed class VisibilityState {
        class SignInButton(val value: Int) : VisibilityState()
        class LoginLayout(val value: Int) : VisibilityState()
        class SubscribedLayout(val value: Int) : VisibilityState()
    }

    sealed class TalkBack {
        class LoginLayout(val contentDescription: Int) : TalkBack()
    }
}