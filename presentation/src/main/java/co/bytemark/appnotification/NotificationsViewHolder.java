package co.bytemark.appnotification;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;

public class NotificationsViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ll_notification_item)
    public LinearLayout linearLayoutNotificationItem;

    @BindView(R.id.notification_title)
    public TextView notificationTitle;

    @BindView(R.id.notification_teaser)
    public TextView notificationTeaser;

    public NotificationsViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
