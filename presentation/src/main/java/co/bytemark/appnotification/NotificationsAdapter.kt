package co.bytemark.appnotification

import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.domain.model.notification.Notification
import co.bytemark.helpers.ConfHelper
import javax.inject.Inject

class NotificationsAdapter(inline val onClick: (Notification, View) -> Unit = { _, _ -> }) : RecyclerView.Adapter<NotificationsViewHolder>() {

    @Inject
    lateinit var confHelper: ConfHelper

    private val notifications = mutableListOf<Notification>()

    init {
        component.inject(this)
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationsViewHolder {
        return NotificationsViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.notification_item, parent, false))
    }

    override fun onBindViewHolder(holder: NotificationsViewHolder, position: Int) {
        confHelper.let {
            with(holder){
                with(notifications[position]){

                    if(read!=null){
                        linearLayoutNotificationItem.setBackgroundColor(if (!read)
                            it.dataThemeBackgroundColor
                        else
                            ColorUtils.setAlphaComponent(it.dataThemeAccentColor, 10))
                    }

                    notificationTitle.text = title
                    notificationTitle.setTextColor(it.dataThemePrimaryTextColor)

                    notificationTeaser.apply {
                        visibility = if(TextUtils.isEmpty(teaser)) View.GONE else View.VISIBLE
                        text = teaser
                        setTextColor(it.dataThemePrimaryTextColor)
                    }

                    itemView.setOnClickListener { _ : View? ->
                        linearLayoutNotificationItem.setBackgroundColor(ColorUtils.setAlphaComponent(it.dataThemeAccentColor, 10))
                        onClick(this,notificationTitle)
                    }
                }
            }
        }
    }

    override fun getItemId(position : Int) = notifications[position].id.hashCode().toLong()

    override fun getItemCount() = notifications.size

    infix fun setNotifications(notificationList : MutableList<Notification>?) {
        notificationList?.let {
            notifications.clear()
            notifications.addAll(notificationList)
            notifyDataSetChanged()
        }
    }
}