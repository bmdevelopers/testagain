package co.bytemark.appnotification

import android.os.Bundle
import android.transition.Slide
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.sdk.model.common.Action
import co.bytemark.widgets.util.replaceFragment

class NotificationsActivity : MasterActivity() {

    override fun getLayoutRes() = R.layout.activity_notification

    override fun useHamburgerMenu() = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setNavigationViewCheckedItem(Action.SETTINGS)
        setToolbarTitle(R.string.screen_title_notification)
        replaceFragment(NotificationFragment().apply { enterTransition = Slide() }, R.id.fragment)
    }

}