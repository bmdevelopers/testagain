package co.bytemark.appnotification

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.databinding.FragmentNotificationDetailBinding
import co.bytemark.domain.model.notification.Notification
import co.bytemark.webview.WebViewActivity
import javax.inject.Inject

class NotificationDetailFragmentNew : BaseMvvmFragment() {

    private lateinit var binding: FragmentNotificationDetailBinding
    private var notification: Notification? = null

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    override fun onInject() {
        CustomerMobileApp.component.inject(this)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentNotificationDetailBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    companion object {
        val NOTIFICATION = "NOTIFICATION"

        fun newInstance(notification: Notification): NotificationDetailFragmentNew {
            val detailFragment = NotificationDetailFragmentNew()
            val args = Bundle()
            args.putParcelable(NOTIFICATION, notification)
            detailFragment.arguments = args
            return detailFragment
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        notification = arguments?.getParcelable(NOTIFICATION)
        if (notification != null) {
            binding.notification = notification
            analyticsPlatformAdapter.notificationDetailsScreenDisplayed(notification?.title, notification?.senderUuid)
            if (notification?.link != null && notification!!.link.trim { it <= ' ' }.isNotEmpty()) {

                binding.notificationDetailMoreInfo?.setOnClickListener(View.OnClickListener { v: View? ->
                    analyticsPlatformAdapter.notificationMoreInfoSelected(notification?.title, notification?.senderUuid)
                    if (isOnline()) {
                        val intent = Intent(context, WebViewActivity::class.java)
                        intent.putExtra(WebViewActivity.EXTRA_URL, notification?.link)
                        intent.putExtra(WebViewActivity.EXTRA_TITLE, getString(R.string.screen_title_notification))
                        intent.putExtra(WebViewActivity.EXTRA_PDF_SUPPORT, true)
                        intent.putExtra(WebViewActivity.EXTRA_NOTIFICATION, true)
                        startActivity(intent)
                    } else {
                        connectionErrorDialog()
                    }
                })
            }
        }
    }
}