package co.bytemark.appnotification

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp.Companion.appComponent
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.common.Navigate
import co.bytemark.widgets.util.getViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.notification_not_logged_in.*
import javax.inject.Inject

class NotificationFragment : BaseMvvmFragment() {

    lateinit var viewModel: NotificationViewModel

    private lateinit var notificationsAdapter: NotificationsAdapter

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    override fun onInject() {
        component.inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View =
            LayoutInflater.from(context).inflate(R.layout.fragment_notification, container, false)

    override fun onOnline() {
        viewModel.isOnline = true
    }

    override fun onOffline() {
        viewModel.isOnline = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel =
                getViewModel(appComponent.notificationViewModel).also { lifecycle.addObserver(it) }

        observerLiveData()

        notificationsAdapter = NotificationsAdapter { notification, _ ->
            viewModel.onNotificationClick(notification)
        }.also {
            notificationRecyclerView.adapter = it
        }

        swipeRefreshLayout.setOnRefreshListener { viewModel.onSwipeRefresh() }
        signInButton.setOnClickListener { viewModel.onSignInClick() }

        analyticsPlatformAdapter.notificationScreenDisplayed()

    }

    private fun observerLiveData() {
        observeDisplayLiveData()
        observeNavigationLiveData()
        observeErrorLiveData()
        observeVisibilityLiveData()
        observeAccessibilityLiveData()
        observeNotificationLiveData()
    }

    private fun observeNavigationLiveData() {
        viewModel.navigationLiveData.observe(this, Observer { result ->
            when (result) {

                is Navigate.FinishActivity -> {
                    activity?.finish()
                }

                is Navigate.StartActivity -> {
                    startActivity(result.intent)
                }

                is Navigate.StartActivityForResult -> {
                    activity?.startActivityForResult(result.intent, result.requestCode)
                }

                is Navigate.AddFragment -> {
                    fragmentManager?.beginTransaction()?.hide(this)
                            ?.add(result.containerId, result.fragment, "")?.addToBackStack("")?.commit()
                }
            }
        })
    }

    private fun observeDisplayLiveData() {
        viewModel.displayLiveData.observe(this, Observer {
            when (it) {
                is Display.SwipeRefreshLayout -> {
                    swipeRefreshLayout.isRefreshing = it.isRefreshing
                }

                is Display.EmptyState.Error -> {
                    it.errorTextContent?.let { it1 ->
                        emptyStateLayout.showError(
                                it.errorImageDrawable,
                                it.errorTextTitle,
                                it1
                        )
                    }
                }

                is Display.EmptyState.ShowContent -> {
                    emptyStateLayout.showContent()
                }

                is Display.EmptyState.Loading -> {
                    emptyStateLayout.showLoading(it.drawable, it.title)
                }

                is Display.SnackBar -> {
                    Snackbar.make(signInButton, it.message, it.length).show()
                }

            }
        })
    }

    private fun observeErrorLiveData() {
        viewModel.errorLiveData.observe(this, Observer {
            it?.let { handleError(it) }
        })
    }

    private fun observeVisibilityLiveData() {
        viewModel.visibilityStateLiveData.observe(this, Observer {
            when (it) {
                is NotificationViewModel.VisibilityState.LoginLayout -> {
                    loginParentLinearLayout.visibility = it.value
                }

                is NotificationViewModel.VisibilityState.SignInButton -> {
                    signInButton.visibility = it.value
                }

                is NotificationViewModel.VisibilityState.SubscribedLayout -> {
                    subscribedLayout.visibility = it.value
                }
            }
        })
    }

    private fun observeAccessibilityLiveData() {
        viewModel.accessibilityLiveData.observe(this, Observer {
            if (it is NotificationViewModel.TalkBack.LoginLayout) {
                loginParentLinearLayout.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
                loginParentLinearLayout.announceForAccessibility(getString(it.contentDescription))
            }
        })
    }

    private fun observeNotificationLiveData() {
        viewModel.notificationLiveData.observe(this, Observer {
            notificationsAdapter setNotifications it
        })
    }

    override fun setUpColors() {
        with(confHelper) {
            swipeRefreshLayout.setColorSchemeColors(
                    accentThemeBacgroundColor,
                    headerThemeAccentColor,
                    backgroundThemeBackgroundColor,
                    dataThemeAccentColor
            )

            swipeRefreshLayout.setBackgroundColor(collectionThemeBackgroundColor)
            subscribedTextView.setTextColor(accentThemeSecondaryTextColor)
            subscribedLayout.setBackgroundColor(accentThemeAccentColor)
            signInButton.setTextColor(backgroundThemeSecondaryTextColor)
            signInButton.backgroundTintList = ColorStateList.valueOf(backgroundThemeAccentColor)
        }
    }

    override fun connectionErrorDialog(
            positiveTextId: Int,
            enableNegativeAction: Boolean,
            negativeTextId: Int,
            finishOnDismiss: Boolean,
            positiveAction: () -> Unit,
            negativeAction: () -> Unit
    ) {
        viewModel.onConnectionError()
    }

}
