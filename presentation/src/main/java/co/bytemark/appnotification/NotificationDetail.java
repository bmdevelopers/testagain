package co.bytemark.appnotification;


import co.bytemark.mvp.MvpBasePresenter;
import co.bytemark.mvp.MvpView;

public interface NotificationDetail {

    /**
     * Interface definition for settings screen
     */
    interface View extends MvpView {

    }

    /**
     * Presenter for {@link View}
     */
    class Presenter extends MvpBasePresenter<View> {
    }
}
