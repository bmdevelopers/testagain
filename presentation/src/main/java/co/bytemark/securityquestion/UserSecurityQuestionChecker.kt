package co.bytemark.securityquestion

import co.bytemark.domain.interactor.securityquestions.UserSecurityQuestionUseCase
import co.bytemark.domain.model.common.Result
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.BytemarkSDK
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class UserSecurityQuestionChecker @Inject constructor(
        private val useCase: UserSecurityQuestionUseCase,
        val confHelper: ConfHelper
) {

    fun checkIfEmpty(listener: SecurityQuestionCheckListener) {
        if (!BytemarkSDK.SDKUtility.getIsUserUpdatedSecurityQuestions() && confHelper.blockAccessWithoutSecurityQuestions()!!) {
            GlobalScope.launch {
                when (val result = useCase.invoke(Unit)) {
                    is Result.Success -> {
                        val data = result.data ?: return@launch

                        if (data.securityQuestionInfoList == null || data.securityQuestionInfoList?.isEmpty() == true) {
                            listener.onEmpty()
                        } else if (!BytemarkSDK.SDKUtility.getIsUserUpdatedSecurityQuestions()) {
                            BytemarkSDK.SDKUtility.setIsUserUpdatedSecurityQuestions(true)
                        }
                    }
                }
            }
        }
    }
}

interface SecurityQuestionCheckListener {
    fun onEmpty()
}