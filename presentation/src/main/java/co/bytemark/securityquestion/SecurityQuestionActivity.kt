package co.bytemark.securityquestion

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.AppConstants.SECURITY_QUESTIONS_SETTING
import co.bytemark.helpers.AppConstants.SECURITY_QUESTIONS_SIGN_UP
import co.bytemark.securityquestion.blockaccess.SecurityQuestionBlockAccessFragment
import co.bytemark.securityquestion.settings.SecurityQuestionSettingFragment
import co.bytemark.securityquestion.signup.SecurityQuestionSignUpFragment
import co.bytemark.use_tickets.BackHandler

class SecurityQuestionActivity : MasterActivity() {

    override fun getLayoutRes() = R.layout.activity_security_question

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setToolbarTitle(intent.getStringExtra(AppConstants.TITLE)
                ?: getString(R.string.sign_up_select_security_question),true)

        enableOrDisableDrawer(true)

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container,
                        when {
                            intent.getBooleanExtra(SECURITY_QUESTIONS_SIGN_UP, false) -> SecurityQuestionSignUpFragment()
                            intent.getBooleanExtra(SECURITY_QUESTIONS_SETTING, false) -> SecurityQuestionSettingFragment()
                            else -> SecurityQuestionBlockAccessFragment()
                        })
                .commit()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        if (fragment is BackHandler) {
            fragment.onBackPressed()
        } else {
            super.onBackPressed()
        }
    }

}
