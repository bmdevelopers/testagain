package co.bytemark.securityquestion

import android.os.Handler
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent.TYPE_VIEW_CLICKED
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.add_payment_card.Helper.TextWatcherHelper
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.hide
import co.bytemark.sdk.model.securityquestions.SecurityQuestionInfo
import co.bytemark.sdk.show
import co.bytemark.widgets.util.addPrefix
import co.bytemark.widgets.util.addSuffix
import co.bytemark.widgets.util.announce
import co.bytemark.widgets.util.setCursorForEditTextInAccessibilityMode
import kotlinx.android.synthetic.main.item_security_question.view.*

class SecurityQuestionAdapter(
    val confHelper: ConfHelper,
    private val questionLimit: Int,
    userSelectedQuestions: MutableList<SecurityQuestionInfo>,
    private var allQuestions: MutableList<SecurityQuestionInfo>
) : RecyclerView.Adapter<SecurityQuestionAdapter.ViewHolder>() {


    var availableQuestionList: List<SecurityQuestionInfo> = arrayListOf()

    val questionMap = mutableMapOf<Int, SecurityQuestionInfo?>()

    val answerMap = mutableMapOf<Int, String>()

    val finalQuestionAnswerSetLiveData = MutableLiveData<ArrayList<SecurityQuestionInfo>>()

    var validationMode = false
    var poistionErrorMap = mutableMapOf<Int, String>()

    var validationMap = mutableMapOf<Int, Pair<Boolean, Boolean>>()

    var firstTime = true

    init {
        var i = 0
        userSelectedQuestions.take(questionLimit).forEach {
            questionMap[i++] = it
            answerMap[i - 1] = ""

        }

        (0 until questionLimit).forEach {
            validationMap[it] = Pair(false, false)
        } //Do not Validate Every Field

        filterQuestions()
    }


    private fun filterQuestions() {
        availableQuestionList = allQuestions - questionMap.values.filterNotNull()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_security_question, parent, false)
        )

    override fun getItemCount() = questionLimit

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var enableSpinnerListener = false

        holder.setIsRecyclable(false)

        with(holder.itemView) {

            fun validateQuestion(position: Int) {
                if (holder.adapterPosition > -1) {
                    validationMap[position] = validationMap[position]!!.copy(first = true)
                    if (spinner.selectedItemPosition == 0) {
                        selectionDownRequiredLabel.text = String.format(
                            (context.getString(R.string.sign_up_security_question_required)),
                            position + 1
                        )
                        if (validationMode) {
                            poistionErrorMap[position] = String.format(
                                (context.getString(R.string.sign_up_security_question_required)),
                                position + 1
                            )
                        } else {
                            announceForAccessibility(
                                String.format(
                                    (context.getString(R.string.sign_up_security_question_required)),
                                    position + 1
                                )
                            )
                        }
                        selectionDownRequiredLabel.show()
                    } else {
                        selectionDownRequiredLabel.hide()
                    }
                }
            }

            fun validateAnswer(position: Int) {
                if (holder.adapterPosition > -1) {
                    validationMap[position] = validationMap[position]!!.copy(second = true)
                    textInputLayout.error =
                        if (TextUtils.isEmpty(editText.text.toString().trim())) {
                            if (validationMode) {
                                poistionErrorMap[position] = (poistionErrorMap[position]
                                    ?: "") + String.format(
                                    (context.getString(R.string.sign_up_answer_required)),
                                    position + 1
                                )
                            } else {
                                announceForAccessibility(
                                    String.format(
                                        (context.getString(R.string.sign_up_answer_required)),
                                        position + 1
                                    )
                                )
                            }
                            String.format(
                                (context.getString(R.string.sign_up_answer_required)),
                                position + 1
                            )

                        } else null

                }

            }

            fun enableAccessibility(enable: Boolean) {
                if (enable) {
                    spinner.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
                    editText.importantForAccessibility =
                        View.IMPORTANT_FOR_ACCESSIBILITY_YES  //Supress Spinner Accessibility to Prevent Error Suppression
                    selectionDownRequiredLabel.importantForAccessibility =
                        View.IMPORTANT_FOR_ACCESSIBILITY_YES  //Supress TextView Accessibility to Prevent Error Suppression

                } else {
                    spinner.importantForAccessibility =
                        View.IMPORTANT_FOR_ACCESSIBILITY_NO  //Supress Spinner Accessibility to Prevent Error Suppression
                    editText.importantForAccessibility =
                        View.IMPORTANT_FOR_ACCESSIBILITY_NO  //Supress EditText Accessibility to Prevent Error Suppression
                    selectionDownRequiredLabel.importantForAccessibility =
                        View.IMPORTANT_FOR_ACCESSIBILITY_NO  //Supress EditText Accessibility to Prevent Error Suppression

                }
            }

            if (firstTime) {
                enableAccessibility(false)
            }

            labelSecurityQuestion.text =
                context.getString(R.string.sign_up_security_question)
                    .addPrefix("${holder.adapterPosition + 1}. ").addSuffix("*")
            labelSecurityQuestion.setTextColor(confHelper.backgroundThemeAccentColor)

            editText.addTextChangedListener(object : TextWatcherHelper() {
                override fun onTextChanged(charSequence: CharSequence?, i: Int, i1: Int, i2: Int) {
                    if (!validationMode && (editText.hasFocus() || validationMap[holder.adapterPosition]!!.second)) {
                        answerMap[holder.adapterPosition] = charSequence.toString()
                        validateAnswer(holder.adapterPosition)
                    }
                }
            })

            editText.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
                if (!hasFocus && !validationMode) { //If focus lost and Validation Mode is Not enabled
                    validateAnswer(holder.adapterPosition)
                } else if (hasFocus && textInputLayout.hint?.endsWith("*") == false) {
                    textInputLayout.hint = textInputLayout.hint.toString().addSuffix("*")
                }
            }

            context?.setCursorForEditTextInAccessibilityMode(mutableListOf(editText))

            editText.setText(answerMap[holder.adapterPosition])
            textInputLayout.hint = context.getString(R.string.sign_up_answer).addSuffix("*")

            var selectionIndex = 0

            val dropDownList = mutableListOf<SecurityQuestionInfo>().apply {
                add(SecurityQuestionInfo(-1, question = context.getString(R.string.select)))
                if (questionMap[holder.adapterPosition]?.id ?: -1 > 0) {
                    add(questionMap[holder.adapterPosition]!!)
                    selectionIndex = 1
                }
                addAll(availableQuestionList)
            }

            spinner.adapter = object : ArrayAdapter<String>(
                holder.itemView.context,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                dropDownList.map { it.question }) {
                override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup) =
                    super.getDropDownView(position, convertView, parent).apply {
                        post {
                            (this.findViewById(android.R.id.text1) as TextView).setSingleLine(
                                false
                            )
                        }
                    }
            }

            spinner.setSelection(selectionIndex)

            spinner.setAccessibilityDelegate(object : View.AccessibilityDelegate() {
                override fun sendAccessibilityEvent(host: View?, eventType: Int) {
                    if (eventType == TYPE_VIEW_CLICKED) {
                        enableSpinnerListener = true
                    }
                    super.sendAccessibilityEvent(host, eventType)
                }
            })

            spinner.setOnTouchListener { _, _ ->
                enableSpinnerListener = true
                false
            }

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    enableSpinnerListener = false
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    spinnerPosition: Int,
                    id: Long
                ) {
                    if (enableSpinnerListener) {
                        enableSpinnerListener = false
                        questionMap[holder.adapterPosition] = dropDownList[spinnerPosition]
                        validateQuestion(holder.adapterPosition)
                        filterQuestions()
                    }
                }

            }

            if (validationMap[holder.adapterPosition]!!.first) {
                validateQuestion(holder.adapterPosition)

                if (textInputLayout.hint?.endsWith("*") == false) {
                    textInputLayout.hint = textInputLayout.hint.toString().addSuffix("*")
                }
            }


            if (validationMap[holder.adapterPosition]!!.second) {
                validateAnswer(holder.adapterPosition)
            }

            if (validationMode) {
                enableAccessibility(false)
                //If Current position has no errors, add empty to it
                if (!poistionErrorMap.contains(holder.adapterPosition)) {
                    poistionErrorMap[holder.adapterPosition] = ""
                }

                //When all view iteration is computed
                if (poistionErrorMap.size == itemCount) {
                    validationMode = false
                    spinner.clearFocus()
                    poistionErrorMap = poistionErrorMap.toSortedMap()
                    if (poistionErrorMap.values.any { it.isNotEmpty() }) { //Announce the errors
                        context.announce(poistionErrorMap.values.filter { it.isNotEmpty() }
                                             .joinToString("\n"))
                    }
                    poistionErrorMap.clear()
                }

                Handler().postDelayed({
                                          enableAccessibility(true)
                                      }, 200)

            }

            if (firstTime) { //Accessibility fix when visible for first time

                if (position == itemCount - 1) {
                    firstTime = false
                }

                Handler().postDelayed({
                                          enableAccessibility(true)
                                      }, 200)
            }
        }


    }

    fun isValid(): Boolean {
        validationMode = true
        poistionErrorMap.clear() //Clear any validation erros
        (0 until questionLimit).forEach {
            validationMap[it] = Pair(true, true)
        } //Validate Every Field
        notifyDataSetChanged() //To Show Errors to the user
        return answerMap.values.all { it.trim().isNotEmpty() }
                && questionMap.values.all { it?.id ?: -1 > -1 }
                && questionMap.size == questionLimit
                && answerMap.size == questionLimit
    }


    fun getFinalSecurityQuestionList(): ArrayList<SecurityQuestionInfo> {
        if (isValid()) {
            val list = arrayListOf<SecurityQuestionInfo>()
            (0 until questionLimit).forEach { key ->
                list.add(questionMap[key]!!.apply { answer = answerMap[key]!! })
            }
            finalQuestionAnswerSetLiveData.value = list
        }
        return arrayListOf()
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}

