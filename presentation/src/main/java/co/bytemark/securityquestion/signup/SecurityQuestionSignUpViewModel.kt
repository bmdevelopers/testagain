package co.bytemark.securityquestion.signup

import co.bytemark.domain.interactor.securityquestions.GetAllSecurityQuestionUseCase
import co.bytemark.domain.model.common.Result
import co.bytemark.helpers.ConfHelper
import co.bytemark.securityquestion.SecurityQuestionAdapter
import co.bytemark.securityquestion.base.SecurityQuestionBaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class SecurityQuestionSignUpViewModel @Inject constructor(
        private val getAllSecurityQuestionUseCase: GetAllSecurityQuestionUseCase,
        private val confHelper: ConfHelper
) : SecurityQuestionBaseViewModel() {

    override fun get() {
        loadingStateLiveData.value = true

        uiScope.launch {
            when (val result = getAllSecurityQuestionUseCase.invoke(Unit)) {

                is Result.Success -> {
                    result.data?.let { data ->
                        data.securityQuestionInfoList?.let { questions ->
                            securityQuestionsAdapterLiveData.value =
                                    SecurityQuestionAdapter(
                                            confHelper,
                                            data.securityQuestionDisplayLimit,
                                            mutableListOf(),
                                            questions
                                    ).apply {
                                        finalQuestionAnswerSetLiveData.observeForever {
                                            finalQuestionAnswerLiveData.value = it
                                        }
                                    }
                        }
                    }
                    loadingStateLiveData.value = false
                }

                is Result.Failure -> {
                    errorLiveData.value = result.bmError.first()
                    loadingStateLiveData.value = false
                }
            }
        }
    }

    override fun submit() {
        securityQuestionsAdapterLiveData.value?.getFinalSecurityQuestionList()
    }
}
