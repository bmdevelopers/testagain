package co.bytemark.securityquestion.signup

import android.app.Activity
import android.content.Intent
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.security_questions.SecurityQuestionsResponse
import co.bytemark.helpers.AppConstants.SECURITY_QUESTIONS_DATA
import co.bytemark.sdk.model.securityquestions.SecurityQuestionInfo
import co.bytemark.securityquestion.base.SecurityQuestionBaseFragment
import co.bytemark.widgets.util.createViewModel

class SecurityQuestionSignUpFragment : SecurityQuestionBaseFragment() {

    companion object {
        fun newInstance() = SecurityQuestionSignUpFragment()
    }

    override fun onOnline() {}
    override fun onOffline() {}

    override fun getSubmitButtonText() = getString(R.string.sign_up)

    override fun observeSuccess(success: SecurityQuestionsResponse) {}

    override fun observeFinalSecurityQuestion(question: ArrayList<SecurityQuestionInfo>) {
        if (question.isNotEmpty()) {

            if (!isOnline()) {
                connectionErrorDialog()
                return
            }

            activity?.setResult(Activity.RESULT_OK, Intent().apply { putExtra(SECURITY_QUESTIONS_DATA, question) })
            activity?.finish()
        }

    }

    override fun observeError(error: BMError) {
        activity?.setResult(Activity.RESULT_CANCELED, Intent().apply { putExtra(SECURITY_QUESTIONS_DATA, error) })
        activity?.finish()
    }

    override fun getViewModel() =
            createViewModel { CustomerMobileApp.appComponent.securityQuestionSignUpViewModel }
}

