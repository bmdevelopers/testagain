package co.bytemark.securityquestion.base

import androidx.lifecycle.Observer
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.security_questions.SecurityQuestionsResponse
import co.bytemark.formly.FormlyUtil.themeButtonBackground
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.model.securityquestions.SecurityQuestionInfo
import co.bytemark.sdk.show
import co.bytemark.securityquestion.SecurityQuestionActivity
import co.bytemark.use_tickets.UseTicketsActivity
import co.bytemark.widgets.util.Util.dpToPx
import co.bytemark.widgets.util.hideViewGroup
import co.bytemark.widgets.util.showViewGroup
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.fragment_security_question.*
import kotlinx.android.synthetic.main.network_connectivity_error_view.*

abstract class SecurityQuestionBaseFragment : BaseMvvmFragment() {

    var securityQuestionFetched = false
    var isSignOutButtonRequired = false

    abstract fun getSubmitButtonText(): String
    abstract fun observeFinalSecurityQuestion(question: ArrayList<SecurityQuestionInfo>)
    abstract fun observeError(error: BMError)
    abstract fun observeSuccess(success: SecurityQuestionsResponse)
    abstract fun getViewModel(): SecurityQuestionBaseViewModel
    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_security_question, container, false)
    }

    override fun onOnline() {

        if (progressBar.visibility == View.VISIBLE) {
            return
        }

        if (!securityQuestionFetched && ll_network_connectivity_error_view.visibility == View.VISIBLE) {
            hideViewGroup(ll_network_connectivity_error_view)
            getViewModel().get()
        }
        super.onOnline()
    }

    override fun onOffline() {
        if (!securityQuestionFetched) {
            hideViewGroup(progressBar, recyclerView, submitButton, signOutButton)
            showNoInternetOverlay()
        }
        super.onOffline()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        checkmark.setMaterialWidth(dpToPx(2.0).toFloat())
        submitButton.text = getSubmitButtonText()
        with(confHelper) {

            backgroundThemeAccentColor.let {
                themeButtonBackground(submitButton.background, it)
                checkmark.setCheckMarkColor(it)
            }

            backgroundThemePrimaryTextColor.let {
                titleTextView.setTextColor(it)
                tv_network_error_1.setTextColor(it)
                tv_network_error_2.setTextColor(it)
                signOutButton.setTextColor(ColorStateList.valueOf(it))
            }

        }

        getViewModel().let { vm ->

            vm.get()

            vm.loadingStateLiveData.observe(this, Observer {
                it?.let {
                    recyclerView.isEnabled = it
                    progressBar.visibility = if (it) View.VISIBLE else View.INVISIBLE
                }
            })

            vm.securityQuestionsAdapterLiveData.observe(this, Observer {
                it?.let {
                    securityQuestionFetched = true
                    showViewGroup(titleTextView, recyclerView, submitButton)
                    if (isSignOutButtonRequired) {
                        signOutButton.show()
                    }
                    recyclerView.adapter = it
                    submitButton.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
                    signOutButton.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
                    titleTextView.announceForAccessibility(getString(R.string.sign_up_select_security_question))
                }
            })


            vm.finalQuestionAnswerLiveData.observe(this, Observer {
                it?.let {
                    observeFinalSecurityQuestion(it)
                }
            })

            vm.errorLiveData.observe(this, Observer {
                it?.let {
                    observeError(it)
                }
            })

            vm.successLiveData.observe(this, Observer {
                it?.let {
                    observeSuccess(it)
                }
            })
        }

        submitButton.setOnClickListener {
            hideKeyboard()
            getViewModel().submit()
        }

        signOutButton.setOnClickListener {
            showSignOutConfirmationDialog()
        }

    }


    private fun showNoInternetOverlay() {
        showViewGroup(ll_network_connectivity_error_view)
        hideViewGroup(recyclerView, submitButton, signOutButton)
        Handler().postDelayed({
            ll_no_network_texts.isFocusableInTouchMode = true
            ll_no_network_texts.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
        }, 100)
    }

    open fun showSignOutConfirmationDialog() {
        MaterialDialog.Builder(requireContext())
                .title(R.string.settings_popup_logout_confirmation_title)
                .content(R.string.settings_popup_logout_confirmation_body)
                .positiveText(R.string.settings_popup_signout)
                .negativeText(R.string.receipt_popup_cancel)
                .onPositive { dialog, which ->
                    BytemarkSDK.logout(requireContext())
                    activity?.finishAffinity()
                    startActivity(Intent(activity, UseTicketsActivity::class.java))
                }
                .onNegative { dialog, which -> dialog.dismiss() }
                .show()
    }


    protected fun showActionBar(show: Boolean) {
        if (activity != null && (activity as SecurityQuestionActivity).supportActionBar != null) {
            if (show) {
                (activity as SecurityQuestionActivity).supportActionBar!!.show()
            } else {
                (activity as SecurityQuestionActivity).supportActionBar!!.hide()
            }
        }
    }
}