package co.bytemark.securityquestion.base

import androidx.lifecycle.MutableLiveData
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.security_questions.SecurityQuestionsResponse
import co.bytemark.sdk.model.securityquestions.SecurityQuestionInfo
import co.bytemark.securityquestion.SecurityQuestionAdapter

abstract class SecurityQuestionBaseViewModel : BaseViewModel() {

    var loadingStateLiveData = MutableLiveData<Boolean>()

    var finalQuestionAnswerLiveData = MutableLiveData<ArrayList<SecurityQuestionInfo>>()

    var securityQuestionsAdapterLiveData = MutableLiveData<SecurityQuestionAdapter>()

    var successLiveData = MutableLiveData<SecurityQuestionsResponse>()

    abstract fun get()
    abstract fun submit()
}