package co.bytemark.securityquestion.settings

import android.content.Context.ACCESSIBILITY_SERVICE
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import android.transition.TransitionManager.beginDelayedTransition
import android.view.View
import android.view.accessibility.AccessibilityManager
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.security_questions.SecurityQuestionsResponse
import co.bytemark.sdk.model.securityquestions.SecurityQuestionInfo
import co.bytemark.sdk.show
import co.bytemark.securityquestion.base.SecurityQuestionBaseFragment
import co.bytemark.widgets.transition.SuperAutoTransition
import co.bytemark.widgets.util.*
import kotlinx.android.synthetic.main.fragment_security_question.*


open class SecurityQuestionSettingFragment : SecurityQuestionBaseFragment() {

    override fun getSubmitButtonText() = getString(R.string.update)

    override fun observeFinalSecurityQuestion(question: ArrayList<SecurityQuestionInfo>) {
        if (question.isNotEmpty()) {

            if (!isOnline()) {
                connectionErrorDialog()
                return
            }


            if (Util.isTalkBackAccessibilityEnabled(context)) {
                (view?.context?.getSystemService(ACCESSIBILITY_SERVICE) as AccessibilityManager).interrupt()
            }

            parentConstraintLayout.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS
            submitButton.invisible()
            progressLayout.show()
        }
    }

    override fun observeSuccess(success: SecurityQuestionsResponse) = showSuccessAnimation()

    override fun observeError(error: BMError) {
        parentConstraintLayout.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        if (isOnline()) {
            handleError(error)
            showViewGroup(titleTextView, recyclerView, submitButton)
        }
    }

    override fun getViewModel() =
            createViewModel { CustomerMobileApp.appComponent.securityQuestionSettingViewModel }

    private fun showSuccessAnimation() {
        if (isSignOutButtonRequired) {
            signOutButton.hide()
        }
        showActionBar(false)
        with(SuperAutoTransition()) {
            duration = 300
            interpolator = LinearOutSlowInInterpolator()
            doOnEnd (Runnable{
                SuperAutoTransition().let {
                    it.duration = 400
                    it.doOnEnd(Runnable {
                        checkmark.isSelected = true
                        checkmark.postDelayed({
                            if (activity?.isFinishing == false) {
                                activity?.finish()
                            }
                        }, 500)
                    })
                    beginDelayedTransition(parentLayout, it)
                    context?.announce(successTextView.text.toString())
                    showViewGroup(checkmark, successTextView)
                }
            })
            beginDelayedTransition(parentLayout, this)
            hideViewGroup(titleTextView, recyclerView, submitButton, progressLayout)
        }
    }

    override fun onDestroy() {
        showActionBar(true)
        super.onDestroy()
    }
}