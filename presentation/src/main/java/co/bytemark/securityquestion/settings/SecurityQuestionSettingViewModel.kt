package co.bytemark.securityquestion.settings

import co.bytemark.domain.interactor.securityquestions.GetAllSecurityQuestionUseCase
import co.bytemark.domain.interactor.securityquestions.UpdateSecurityQuestionUseCase
import co.bytemark.domain.interactor.securityquestions.UserSecurityQuestionUseCase
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.security_questions.SecurityQuestionsResponse
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.model.securityquestions.SecurityQuestionInfo
import co.bytemark.sdk.model.securityquestions.UpdateSecurityQuestion
import co.bytemark.securityquestion.SecurityQuestionAdapter
import co.bytemark.securityquestion.base.SecurityQuestionBaseViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

class SecurityQuestionSettingViewModel @Inject constructor(
        private val userSecurityQuestionUseCase: UserSecurityQuestionUseCase,
        private val getAllSecurityQuestionUseCase: GetAllSecurityQuestionUseCase,
        private val updateSecurityQuestionUseCase: UpdateSecurityQuestionUseCase,
        private val confHelper: ConfHelper
) : SecurityQuestionBaseViewModel() {

    var questionLimit = 3 //default size

    var areQuestionsLinked = false

    override fun get() {
        fetchSecurityQuestions()
    }

    private fun fetchSecurityQuestions() {
        loadingStateLiveData.value = true

        uiScope.launch {
            val getAllQuestions = async { getAllSecurityQuestionUseCase.invoke(Unit) }
            val getUserSecurityQuestion = async { userSecurityQuestionUseCase.invoke(Unit) }

            processAllQuestions(getAllQuestions.await(), getUserSecurityQuestion.await())
        }
    }

    private fun processAllQuestions(
            allQuestionsResult: Result<SecurityQuestionsResponse>,
            userQuestionsResult: Result<SecurityQuestionsResponse>
    ) {
        if (allQuestionsResult is Result.Failure) {
            errorLiveData.value = allQuestionsResult.bmError.first()
            return
        }
        if (userQuestionsResult is Result.Failure) {
            errorLiveData.value = userQuestionsResult.bmError.first()
            return
        }

        //No Errors, Proceed safely
        var allQuestion = mutableListOf<SecurityQuestionInfo>()

        var userQuestion = mutableListOf<SecurityQuestionInfo>()

        if (allQuestionsResult is Result.Success && userQuestionsResult is Result.Success) {
            questionLimit = allQuestionsResult.data?.securityQuestionDisplayLimit!!

            allQuestionsResult.data?.securityQuestionInfoList?.let {
                allQuestion = it
            }

            userQuestionsResult.data?.securityQuestionInfoList?.let {
                userQuestion = it
                areQuestionsLinked = it.isNotEmpty()
            }

            Pair(allQuestion, userQuestion).let {
                if (it.first.isNotEmpty()) {
                    loadingStateLiveData.value = false
                    securityQuestionsAdapterLiveData.value =
                            SecurityQuestionAdapter(confHelper, questionLimit, it.second, it.first).apply {
                                finalQuestionAnswerSetLiveData.observeForever { list ->
                                    if (list?.isNotEmpty() == true) {
                                        this@SecurityQuestionSettingViewModel.finalQuestionAnswerLiveData.value = list
                                        update(UpdateSecurityQuestion(list, areQuestionsLinked))
                                    }
                                }

                            }
                }
            }
        }
    }

    private fun update(update: UpdateSecurityQuestion) {
        uiScope.launch {
            when (val result = updateSecurityQuestionUseCase.invoke(update)) {

                is Result.Success -> {
                    loadingStateLiveData.value = false

                    result.data?.let { successLiveData.value = it }
                    if (!BytemarkSDK.SDKUtility.getIsUserUpdatedSecurityQuestions()) {
                        BytemarkSDK.SDKUtility.setIsUserUpdatedSecurityQuestions(true)
                    }
                }

                is Result.Failure -> {
                    loadingStateLiveData.value = false
                    errorLiveData.value = result.bmError.first()
                }
            }
        }
    }

    override fun submit() {
        securityQuestionsAdapterLiveData.value?.getFinalSecurityQuestionList()
    }
}
