package co.bytemark.securityquestion.blockaccess

import android.os.Bundle
import co.bytemark.base.MasterActivity
import co.bytemark.securityquestion.settings.SecurityQuestionSettingFragment
import co.bytemark.use_tickets.BackHandler


class SecurityQuestionBlockAccessFragment : SecurityQuestionSettingFragment(), BackHandler {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isSignOutButtonRequired = true
    }

    override fun onBackPressed(): Boolean {
        (activity as MasterActivity).showExitDialog()
        return false
    }

}
