package co.bytemark.webview;


import co.bytemark.mvp.MvpView;

/**
 * Created by virajgaikwad on 8/24/16.
 */
public interface WebView extends MvpView {

    void showTitle(String title);
}
