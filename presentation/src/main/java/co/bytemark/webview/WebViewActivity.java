package co.bytemark.webview;

import android.app.ActivityManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.MenuItem;

import java.util.List;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.base.MasterActivity;
import co.bytemark.domain.model.notification.Notification;
import timber.log.Timber;

@SuppressWarnings("unchecked")
public class WebViewActivity extends MasterActivity {
    public static final String EXTRA_TITLE = "title";
    public static final String EXTRA_URL = "url";
    public static final String EXTRA_NOTIFICATION = "notification";
    public static final String EXTRA_HAMBURGER = "toggle";
    public static final String EXTRA_PDF_SUPPORT = "pdfsupport";

    @Inject
    AnalyticsPlatformAdapter analyticsPlatformAdapter;
    private boolean hamburger;
    protected String title;
    private boolean pdfSupportEnabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Call before super to set field value
        hamburger = getIntent().getBooleanExtra(EXTRA_HAMBURGER, false);
        super.onCreate(savedInstanceState);
        CustomerMobileApp.Companion.getComponent().inject(this);

        title = getIntent().getStringExtra(EXTRA_TITLE);
        final String url = getIntent().getStringExtra(EXTRA_URL);
        final Notification notification = getIntent().getParcelableExtra(EXTRA_NOTIFICATION);
        pdfSupportEnabled = getIntent().getBooleanExtra(EXTRA_PDF_SUPPORT, false);

        if (title == null | url == null) {
            Timber.e("Webview closed since no url or tite ");
            finish();
            return;
        }

        Timber.d("Title: %s \nUrl: %s", title, url);

        setToolbarTitle(title);
        setTitle(title);
        replaceContainerWithWebViewFragment(url);

        analyticsPlatformAdapter.webViewPageDisplayed(title);

        if (hamburger) {
            setNavigationViewCheckedItem(url);
        }
    }

    private void replaceContainerWithWebViewFragment(final @NonNull String url) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, getWebViewFragment(url))
                .addToBackStack(null)
                .commit();
    }

    protected Fragment getWebViewFragment(String url) {
        if (pdfSupportEnabled) {
            return PdfSupportWebViewFragment.newInstance(url, title);
        } else {
            return WebViewFragment.newInstance(url, title);
        }
    }

    @Override
    protected boolean useHamburgerMenu() {
        return hamburger;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_webview;
    }

    @Override
    public void onBackPressed() {
        WebViewFragment webViewFragment = (WebViewFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        if (webViewFragment != null && webViewFragment.canGoBack()) {
            webViewFragment.goBack();
        } else {
            ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
            if (taskList.get(0).numActivities == 1 && taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
                super.onBackPressed();
            } else {
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                // NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
