package co.bytemark.webview

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.graphics.Bitmap
import android.net.http.SslError
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.webkit.*
import android.webkit.WebView
import co.bytemark.R
import com.afollestad.materialdialogs.MaterialDialog
import timber.log.Timber

class PdfSupportWebViewFragment : WebViewFragment() {

    private var isRedirectUrl = false
    private var lastPageTitle = ""
    private var urlRetryCount = 0

    companion object {
        @JvmStatic
        fun newInstance(url : String,title : String) = PdfSupportWebViewFragment().apply {
            arguments = Bundle().apply {
                putString(WEBVIEW_URL, url)
                putString(WEBVIEW_TITLE, title)
            }
        }
    }

    override fun getWebViewClient() : WebViewClient {
        return object : WebViewClient() {

            override fun onPageStarted(view: WebView, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                isRedirectUrl = false
                lastPageTitle = view.title!!
                showLoading()
            }

            @SuppressLint("BinaryOperationInTimber")
            override fun onReceivedError(view: WebView?, errorCode: Int, description: String, failingUrl: String) {
                super.onReceivedError(view, errorCode, description, failingUrl)
                hideLoading()
                Timber.i("onReceivedError() called with: errorCode = [$errorCode], description = [$description], failingUrl = [$failingUrl]")
            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean { //Append to google docs link so, pdf can be viewed in webview
                var webUrl = url
                if (webUrl.endsWith(".pdf") && !(webUrl.contains("drive.google.com") || webUrl.contains("docs.google.com"))) {
                    webUrl = GOOGLE_DOCS_VIEWER_URL + webUrl
                    view.stopLoading()
                    view.clearCache(false)
                    view.loadUrl(webUrl)
                    isRedirectUrl = true
                    return true
                }
                return false
            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                hideLoading()
                if (view.title == lastPageTitle && url.contains(GOOGLE_DOCS_VIEWER_URL) && urlRetryCount <= 7) {
                    urlRetryCount++
                    view.loadUrl(url)
                } else {
                    urlRetryCount = 0
                }

            }

            override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
                activity?.let {
                    hideLoading()
                    AlertDialog.Builder(it).apply {
                        setMessage(R.string.login_popup_notification_error_ssl_cert_invalid)
                        setPositiveButton(R.string.continuee) { _, _ -> handler?.proceed() }
                        setNegativeButton(R.string.popup_cancel) { _, _ -> handler?.cancel() }
                    }.create().show()
                }
            }

            override fun onReceivedError(view: android.webkit.WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                super.onReceivedError(view, request, error)
                Timber.e("onReceivedError() called with: view = [ %s ], request = [%s], error = [%s]", view, request, error)
                hideLoading()
            }

            override fun onReceivedHttpError(view: WebView?, request: WebResourceRequest?, errorResponse: WebResourceResponse?) {
                super.onReceivedHttpError(view, request, errorResponse)
                Timber.e("onReceivedHttpError() called with: view = [%s], request = [%s], errorResponse = [%s]", view, request, errorResponse)
                hideLoading()
            }

            override fun onReceivedHttpAuthRequest(view: WebView?, handler: HttpAuthHandler, host: String?, realm: String?) {
                handler.proceed("srvc_upuat_access", "2\$Hear\$Hole\$Note$3")
            }

        }
    }
}
