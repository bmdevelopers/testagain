package co.bytemark.webview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.http.SslError;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.base.BaseMvpFragment;
import co.bytemark.sdk.BytemarkSDK;
import timber.log.Timber;

import static androidx.core.content.ContextCompat.getColor;

@SuppressWarnings("deprecation")
public class WebViewFragment extends BaseMvpFragment<co.bytemark.webview.WebView, WebViewPresenter> implements co.bytemark.webview.WebView {

    protected static final String WEBVIEW_URL = "url";
    protected static final String WEBVIEW_TITLE = "title";
    protected static final String WEBVIEW_ADD_DEFAULT_HEADERS = "addDefaultHeaders";
    protected static final String GOOGLE_DOCS_VIEWER_URL = "https://docs.google.com/gview?embedded=true&url=";
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 200;
    private static final String TRIP_PLANNER_URL = "hafas";

    protected String url;
    protected String title;
    protected Map<String, String> headers = new HashMap<>();

    private AlertDialog alertDialog;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.webView)
    WebView webView;

    public static WebViewFragment newInstance(String url, String title) {
        WebViewFragment fragment = new WebViewFragment();
        Bundle args = new Bundle();
        args.putString(WEBVIEW_URL, url);
        args.putString(WEBVIEW_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public WebViewPresenter createPresenter() {
        return new WebViewPresenter();
    }

    @Override
    protected void onInject() {
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(WEBVIEW_URL);
            title = getArguments().getString(WEBVIEW_TITLE);

            //Append to google docs link so, pdf can be viewed in webview
            if (url.endsWith(".pdf") && !(url.contains("drive.google.com") || url.contains("docs.google.com"))) {
                url = GOOGLE_DOCS_VIEWER_URL + url;
            }

            // Handling headers
            boolean addDefaultHeaders = getArguments().getBoolean(WEBVIEW_ADD_DEFAULT_HEADERS, false);
            if (addDefaultHeaders) {
                headers.put("Authorization", "Bearer " + BytemarkSDK.SDKUtility.getAuthToken());
                headers.put("Accept-Language", Locale.getDefault().toLanguageTag());
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (null != CustomerMobileApp.Companion.getConf().getClients().getCustomerMobile().isWebViewLocationEnabled()
                && CustomerMobileApp.Companion.getConf().getClients().getCustomerMobile().isWebViewLocationEnabled()
                && url.contains(TRIP_PLANNER_URL)) {
            if (!isLocationEnabled()) {
                dialogToEnableLocation();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        alertDialog = null;
    }

    private Boolean isLocationEnabled() {
        LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void dialogToEnableLocation() {
        alertDialog = new AlertDialog.Builder(getActivity())
                .setMessage(R.string.web_view_popup_need_location_permission)
                .setCancelable(false)
                .setNegativeButton(android.R.string.cancel, ((dialog, which) -> dialog.dismiss()))
                .setPositiveButton(R.string.web_view_popup_navigate_to_settings, (dialog, which) -> startSettingsScreen()).show();
    }

    private void startSettingsScreen() {
        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_PERMISSION_REQUEST_CODE);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_web_view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSwipeRefresh();
        initializeWebView();
    }

    private void initSwipeRefresh() {
        swipeRefreshLayout.setColorSchemeColors(getColor(getActivity(), R.color.colorPrimary), getColor(getActivity(), R.color.colorAccent));
    }

    @Override
    protected void onOnline() {

    }

    @Override
    protected void onOffline() {

    }

    @Override
    protected void setAccentThemeColors() {

    }

    @Override
    protected void setBackgroundThemeColors() {

    }

    @Override
    protected void setDataThemeColors() {

    }

    private void initializeWebView() {
        WebSettings settings = webView.getSettings();
        applyWebViewSettings(settings);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
            }
        });

        webView.setWebViewClient(getWebViewClient());
        loadPage();
    }

    public void loadPage() {
        if (webView != null && !url.isEmpty()) {
            showLoading();
            webView.loadUrl(url, headers);
        }
    }

    public void loadPage(String url){
        this.url = url;
        loadPage();
    }

    @SuppressLint("SetJavaScriptEnabled")
    protected void applyWebViewSettings(WebSettings settings) {
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setLoadWithOverviewMode(true);
        settings.setAppCacheEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setDomStorageEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(true);
        settings.setGeolocationEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);

        if (url.contains("hafas")) {
            settings.setAppCacheEnabled(false);
        }
    }

    protected void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setEnabled(true);
    }

    protected void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setEnabled(false);
    }

    @Override
    public void onDestroy() {
        webView.destroy();
        super.onDestroy();
    }

    @Override
    public void showTitle(String title) {
    }

    protected WebViewClient getWebViewClient() {
        return new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return overrideUrlLoading(view, request);
            }

            /**
             * Report an error to the host application. These errors are unrecoverable
             * (i.e. the menu_main resource is unavailable). The errorCode parameter
             * corresponds to one of the ERROR_* constants.
             *
             * @param view        The WebView that is initiating the callback.
             * @param errorCode   The error code corresponding to an ERROR_* value.
             * @param description A String describing the error.
             * @param failingUrl  The url that failed to load.
             * @deprecated Use {@link #onReceivedError(WebView, WebResourceRequest, WebResourceError)
             * onReceivedError(WebView, WebResourceRequest, WebResourceError)} instead.
             */
            @SuppressLint("BinaryOperationInTimber")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                hideLoading();
                super.onReceivedError(view, errorCode, description, failingUrl);
                Timber.i("onReceivedError() called with: errorCode = [" + errorCode + "], description = [" + description + "], failingUrl = [" + failingUrl + "]");
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                hideLoading();
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.login_popup_notification_error_ssl_cert_invalid);
                builder.setPositiveButton(R.string.continuee, (dialog, which) -> handler.proceed());
                builder.setNegativeButton(R.string.popup_cancel, (dialog, which) -> handler.cancel());
                final AlertDialog dialog = builder.create();
                dialog.show();
            }

            /**
             * Report web resource loading error to the host application. These errors usually indicate
             * inability to connect to the server. Note that unlike the deprecated version of the callback,
             * the new version will be called for any resource (iframe, image, etc), not just for the menu_main
             * page. Thus, it is recommended to perform minimum required work in this callback.
             *
             * @param view    The WebView that is initiating the callback.
             * @param request The originating request.
             * @param error   Information about the error occured.
             */
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                hideLoading();
                Timber.e("onReceivedError() called with: view = [ %s ], request = [%s], error = [%s]", view, request, error);
            }

            /**
             * Notify the host application that an HTTP error has been received from the server while
             * loading a resource.  HTTP errors have status codes &gt;= 400.  This callback will be called
             * for any resource (iframe, image, etc), not just for the menu_main page. Thus, it is recommended to
             * perform minimum required work in this callback. Note that the content of the server
             * response may not be provided within the <b>errorResponse</b> parameter.
             *
             * @param view          The WebView that is initiating the callback.
             * @param request       The originating request.
             * @param errorResponse Information about the error occured.
             */
            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                hideLoading();
                Timber.e("onReceivedHttpError() called with: view = [%s], request = [%s], errorResponse = [%s]", view, request, errorResponse);
            }

            @Override
            public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
                handler.proceed("srvc_upuat_access", "2$Hear$Hole$Note$3");
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Timber.i("Page loading competed , URL: %s", url);
                hideLoading();
                onPageLoadCompleted();
            }
        };
    }

    protected void onPageLoadCompleted() { }

    protected boolean overrideUrlLoading(WebView view, WebResourceRequest request){
        return false;
    }

    public boolean canGoBack() {
        if (webView != null) {
            return webView.canGoBack();
        }
        return false;
    }

    public void goBack() {
        webView.goBack();
    }

}


