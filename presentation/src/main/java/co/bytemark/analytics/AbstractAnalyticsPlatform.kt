package co.bytemark.analytics

import android.app.Activity
import android.app.Application
import android.content.Context

/**
 * Created by Santosh on 03/10/17.
 */
abstract class AbstractAnalyticsPlatform : AnalyticsPlatform {
    override fun initializeAnalyticsPlatform(application: Application) {
    }

    override fun startAnalyticsPlatforms(activity: Activity) {
    }

    override fun stopAnalyticsPlatforms(context: Context) {
    }
}