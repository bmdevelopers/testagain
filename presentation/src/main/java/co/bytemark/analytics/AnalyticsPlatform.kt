package co.bytemark.analytics

import android.app.Activity
import android.app.Application
import android.content.Context

/**
 * Created by ranjith on 06/06/20
 */
interface AnalyticsPlatform {
    fun initializeAnalyticsPlatform(application: Application)

    fun startAnalyticsPlatforms(activity: Activity)

    fun stopAnalyticsPlatforms(context: Context)

    fun webViewPageDisplayed(title: String)

    fun tutorialScreenDisplayed(ticketStorageTutorialScreen: String)

    fun tutorialButtonPressed(button: String)

    // Auth Module
    fun signIn(origin: String?, signInType: String, status: String, errorMessage: String?)

    fun signInScreenDisplayed()

    fun signUpScreenDisplayed()

    fun signUp(
        accountSignUpDuration: Long, formFillDuration: Long,
        status: String, errorMessage: String?
    )

    fun forgotPassword(status: String, errorMessage: String?)

    fun signOut(type: Int)


    // Store

    fun storeScreenDisplayed(source: String?)

    fun filterLoadingCompleted(status: String, errorMessage: String?)

    fun filterApplied(groupName: String?, groupUuid: String?, valueName: String?)

    fun productsLoadingCompleted(forceReload: Int, status: String, errorMessage: String?)

    fun addToCart(productName: String?, productUuid: String?, quantity: Int, price: Double)

    fun changeFilter()

    fun clearFilter()

    fun expressCheckout(
        productName: String?,
        productUuid: String?,
        quantity: Int,
        price: Double,
        type: String?
    )

    // Cart
    fun cartScreenDisplayed(source: String?)

    fun viewStoreSelected()

    fun itemRemoved(productName: String?, productUuid: String?, quantity: Int, price: Double)

    // checkout
    fun checkoutScreenDisplayed(source: String?)

    fun defaultPaymentLoading(
        paymentType: String?,
        cardType: String?,
        status: String,
        errorMessage: String?
    )

    fun addPaymentSelected(source: String)

    fun paymentMethodChanged(origin: String?, paymentType: String?)

    fun updateOptionSelected()

    fun promoCodeApplied(screen: String?, promoCode: String?, status: String, errorMessage: String?)

    fun orderPlaced(
        orderQuantity: Int, orderTotal: Double, orderId: String?, orderType: String,
        productUuidList: String?, status: String, errorMessage: String?
    )

    fun productPurchased(uuid: String?, name: String?, price: Double, sum: Double, quantity: Int)

    // Payment
    fun paymentMethodsLoaded(count: Int, forceReload: Int, status: String, errorMessage: String?)

    fun paymentMethodRemoved(type: String?, status: String, errorMessage: String?)

    fun paymentMethodAdded(type: String?, cardType: String?, status: String, errorMessage: String?)

    fun featureScanCardSelected()


    // Wallet and FareMedium
    fun loadMoney(source: String?, amount: Double, status: String, errorMessage: String?)

    fun saveAutoLoad(
        source: String?,
        amount: Double,
        thresholdBalance: Double,
        status: String,
        errorMessage: String?
    )

    fun removeAutoLoad(source: String?, status: String, errorMessage: String?)

    fun createVirtualCard(
        cardType: String?,
        nickname: String?,
        status: String,
        errorMessage: String?
    )

    fun transferVirtualCard(status: String, errorMessage: String?)

    fun existingCardLinked(status: String, errorMessage: String?)

    fun addPassesToFareMediumSelected()

    fun availablePassesLoaded(status: String, errorMessage: String?)

    fun fareCappingLoaded(status: String, errorMessage: String?)

    fun cardHistoryScreenLoaded(status: String, errorMessage: String?)

    fun manageCardsScreenOpened(status: String, errorMessage: String?)

    // More Info
    fun moreInfoScreenDisplayed()

    fun moreInfoOptionSelected(optionName: String?)

    // Profile
    fun profileLoadingCompleted(status: String, errorMessage: String?)

    fun profileUpdated(status: String, errorMessage: String?)

    fun passwordChanged(status: String, errorMessage: String?)

    // Settings
    fun settingsScreenDisplayed()

    fun notificationSettingsLoaded(status: String, errorMessage: String?)


    // VoucherCode
    fun voucherCodeScreenDisplayed()

    fun voucherCodeApplied(status: String, errorMessage: String?)

    // HACON
    fun haconDepartureMenuSelected()

    fun haconTripPlannerMenuSelected()

    fun haconAlarmsMenuSelected()

    // Notification
    fun notificationScreenDisplayed()

    fun notificationDetailsScreenDisplayed(title: String?, uuid: String?)

    fun notificationMoreInfoSelected(title: String?, uuid: String?)

    // Accessibility
    fun voiceOverEnabled()

    fun fontSizeChanged()

    fun languageChanged()

    // UseTickets
    fun useTicketsScreenDisplayed()

    fun cloudPassesLoaded(
        type: String?,
        noOfPasses: Int,
        forceReload: Int,
        status: String,
        errorMessage: String?
    )

    fun devicePassesLoaded(
        type: String?,
        noOfPasses: Int,
        forceReload: Int,
        status: String,
        errorMessage: String?
    )

    fun featureTransferPass(
        source: String,
        storage: String?,
        productName: String?,
        productUuid: String?,
        status: String,
        errorMessage: String?
    )

    fun featureSendTicket(
        productUuid: String?,
        productName: String?,
        status: String,
        errorMessage: String?
    )

    fun featureRepurchase(source: String, productUuid: String?, productName: String?)

    fun featureResendReceipt(
        source: String,
        productUuid: String?,
        status: String,
        errorMessage: String?
    )

    fun featureViewTicketDetails(source: String, productUuid: String?)

    fun activateTicket(
        noOfTickets: Int,
        hasEnablers: Int,
        hasCloudPass: Int,
        status: String,
        errorMessage: String?
    )

    fun displayTicket(noOfTickets: Int, status: String, errorMessage: String?)

    fun ticketStorageScreenDisplayed()

    fun defaultTicketStorageChanged(to: String?)

    fun fareMediumsLoaded(status: String, errorMessage: String?)

    // Purchase History
    fun purchaseHistoryScreenDisplayed(pageNumber: Int, status: String, errorMessage: String?)

    fun receiptScreenDisplayed()

    // Account Photo
    fun accountPhotoScreenDisplayed()

    fun accountPhotoUpdated(status: String, errorMessage: String?)

    // Accessibility
    fun accessibilityState(voiceOverEnabled: Int, fontScale: Double, language: String?)
}