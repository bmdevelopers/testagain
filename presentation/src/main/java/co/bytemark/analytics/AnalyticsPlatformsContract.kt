package co.bytemark.analytics

class AnalyticsPlatformsContract private constructor() {

    object BuildIdentifier {
        const val FIREBASE = "firebase"
    }

    object AnalyticsPlatformEntry {
        const val HEADER_BUTTON = "Header button"
        const val ADD_TO_CART_OVERLAY = "Add To Cart Overlay"
        const val SETTINGS = "Settings"
        const val NATIVE_ACCOUNT = "Native Account"
        const val GOOGLE = "Google"
        const val FACEBOOK = "Facebook"
        const val TICKET_STORAGE_TUTORIAL_SCREEN = "Ticket Storage Tutorial Screen"
        const val GOT_IT = "Got It"
        const val SHOW_ME_LATER = "Show Me Later"
        const val APP_NAME = "APP_NAME"
        const val SOURCE = "Source"
        const val SUCCESS = "Success"
    }

    object Screen {
        const val SETTINGS = "settings"
        const val MENU = "menu"
        const val STORE = "store"
        const val MANAGE_CARD = "fare_medium_manage"
        const val USE_TICKETS = "use_tickets"
        const val PAYMENT = "payment"
        const val CHECKOUT = "checkout"
        const val HACON = "hacon"
        const val SCHEDULES = "schedules"
        const val SHOPPING_CART = "shopping_cart"
        const val REPURCHASE = "repurchase"
        const val EXPRESS_CHECKOUT = "express_checkout"
        const val ORDER = "order"
        const val FARE_MEDIUM = "fare_medium"
        const val NOTIFICATION = "notification"
        const val WALLET = "wallet"
    }

    object OrderType {
        const val PASSES = "passes"
        const val LOAD_WALLET = "load_wallet"
        const val LOAD_FARE_MEDIUM = "load_fare_medium"
    }

    object SignInType {
        const val APPLE = "apple"
        const val FACEBOOK = "facebook"
        const val GOOGLE = "google"
        const val NATIVE = "native"
    }

    object ExpressCheckout {
        const val GOOGLE_PAY = "google_pay"
        const val BUY_NOW = "buy_now"
    }

    object PaymentType {
        const val CARD = "card"
        const val PAYPAL = "paypal"
        const val GOOGLE_PAY = "google_pay"
        const val IDEAL = "ideal"
        const val DOT_PAY = "dot_pay"
        const val PAY_NEAR_ME = "pay_near_me"
        const val WALLET = "wallet"
        const val INCOMM = "incomm"
    }

    object Status {
        const val SUCCESS = "success"
        const val FAILURE = "failure"
    }

    object PassType {
        const val ACTIVE = "active"
        const val AVAILABLE = "available"
        const val HISTORY = "history"
    }

    object TicketStorage {
        const val CLOUD = "cloud"
        const val DEVICE = "device"
    }

    object Events {
        // AUTH MODULE
        const val SIGN_IN = "sign_in"
        const val SIGN_IN_SCREEN_DISPLAYED = "sign_in_screen_displayed"
        const val SIGN_UP_SCREEN_DISPLAYED = "sign_up_screen_displayed"
        const val SIGN_UP = "sign_up"
        const val SIGN_OUT = "sign_out"
        const val FORGOT_PASSWORD = "forgot_password"

        // STORE MODULE
        const val STORE_SCREEN_DISPLAYED = "store_screen_displayed"
        const val FILTERS_LOADING_COMPLETED = "filters_loading_completed"
        const val FILTERS_APPLIED = "filters_selected"
        const val PRODUCT_LOADING_COMPLETED = "products_loading_completed"
        const val ADD_TO_CART = "add_to_cart"
        const val CHANGE_FILTER = "change_filter"
        const val CLEAR_FILTER = "clear_filter"
        const val FEATURE_EXPRESS_CHECKOUT = "feature_express_checkout"

        // CART
        const val CART_SCREEN_DISPLAYED = "shopping_cart_screen_displayed"
        const val VIEW_STORE_SELECTED = "view_store_selected"
        const val ITEM_REMOVED = "cart_item_removed"

        // CHECKOUT
        const val CHECKOUT_SCREEN_DISPLAYED = "checkout_screen_displayed"
        const val DEFAULT_PAYMENT_LOADING_COMPLETED = "default_payment_loading_completed"
        const val ADD_PAYMENT_SELECTED = "add_payment_selected"
        const val PAYMENT_METHOD_CHANGED = "payment_method_changed"
        const val UPDATE_OPTION_SELECTED = "update_option_selected"
        const val PROMO_CODE_APPLIED = "promo_code_applied"
        const val PLACE_ORDER = "place_order"
        const val PRODUCT_PURCHASED = "product_purchased"

        // Payment
        const val PAYMENT_METHODS_LOADED = "payment_methods_loaded"
        const val REMOVE_PAYMENT_METHOD = "remove_payment_method"
        const val NEW_PAYMENT_METHOD_ADDED = "new_payment_method_added"
        const val FEATURE_SCAN_CARD_SELECTED = "feature_scan_card_selected"

        // Wallet and FareMedium
        const val LOAD_MONEY = "load_money"
        const val SAVE_AUTO_LOAD = "save_auto_load"
        const val REMOVE_AUTO_LOAD = "remove_auto_load"
        const val CREATE_VIRTUAL_CARD = "create_virtual_card"
        const val TRANSFER_VIRTUAL_CARD = "feature_transfer_virtual_card"
        const val LINK_EXISTING_CARD = "feature_link_existing_card"
        const val ADD_PASSES_TO_FARE_MEDIUM = "add_passes_to_faremedium_selected"
        const val AVAILABLE_PASSES_LOADED = "available_passes_loaded"
        const val FARE_CAPPING_DISPLAYED = "fare_capping_screen_displayed"
        const val CARD_HISTORY_DISPLAYED = "card_history_screen_displayed"
        const val MANAGE_SCREEN_DISPLAYED = "manage_screen_displayed"

        // settings and more_info
        const val MORE_INFO_SCREEN_DISPLAYED = "more_info_screen_displayed"
        const val MORE_INFO_OPTION_SELECTED = "menu_item_selected"
        const val SETTINGS_SCREEN_DISPLAYED = "settings_screen_displayed"
        const val PROFILE_LOADING_COMPLETED = "profile_loading_completed"
        const val UPDATE_PROFILE = "profile_updated"
        const val CHANGE_PASSWORD = "password_changed"
        const val NOTIFICATION_SETTINGS_SCREEN_DISPLAYED = "notification_settings_loaded"
        const val VOUCHER_CODE_SCREEN_DISPLAYED = "voucher_entry_screen_displayed"
        const val VOUCHER_CODE_APPLIED = "voucher_code_applied"

        // hacon
        const val HACON_DEPARTURE_OPTION_SELECTED = "hacon_departure_menu_selected"
        const val HACON_TRIP_PLANNER_SELECTED = "hacon_trip_planner_menu_selected"
        const val HACON_ALARMS_SELECTED = "hacon_alarms_menu_selected"

        // notifications
        const val NOTIFICATION_SCREEN_DISPLAYED = "notification_list_screen_displayed"
        const val NOTIFICATION_DETAILS_SCREEN_DISPLAYED = "notification_detail_screen_displayed"
        const val NOTIFICATION_MORE_INFO_SELECTED = "notification_detail_more_info_selected"

        // Accessibility
        const val VOICE_OVER_ENABLED = "voice_over_enabled"
        const val FONT_SIZE_CHANGED = "font_size_changed"
        const val LANGUAGE_CHANGED = "language_changed"

        // Account Photo
        const val ACCOUNT_PHOTO_SCREEN_DISPLAYED = "account_photo_screen_displayed"
        const val ACCOUNT_PHOTO_UPDATED = "account_photo_updated"

        // UseTickets
        const val USE_TICKETS_SCREEN_OPENED = "use_tickets_screen_displayed"
        const val CLOUD_PASSES_LOADING_COMPLETED = "cloud_passes_loaded"
        const val DEVICE_PASSES_LOADING_COMPLETED = "device_passes_loaded"
        const val FEATURE_TRANSFER_PASS = "feature_transfer_pass"
        const val FEATURE_SEND_TICKET = "feature_send_ticket"
        const val FEATURE_REPURCHASE = "feature_repurchase"
        const val FEATURE_RESEND_RECEIPT = "feature_resend_receipt"
        const val FEATURE_VIEW_TICKET_DETAILS = "feature_view_ticket_details"
        const val ACTIVATE_TICKET = "activate_pass"
        const val DISPLAY_TICKET = "display_pass"
        const val TICKET_STORAGE_SCREEN_DISPLAYED = "ticket_storage_screen_displayed"
        const val DEFAULT_STORAGE_CHANGED = "default_ticket_storage_changed"
        const val FARE_MEDIUMS_LOADED = "fare_mediums_loaded"

        // Purchase History
        const val PURCHASE_HISTORY_SCREEN_DISPLAYED = "purchase_history_loaded"
        const val RECEIPT_SCREEN_DISPLAYED = "receipt_screen_displayed"

        //Accessibility
        const val ACCESSIBILITY_STATE = "accessibility_state"
    }

    object UserProperty {
        const val LOGIN_STATUS = "login_status"
    }

    object Parameters {
        const val FROM = "from"
        const val TYPE = "type"
        const val STATUS = "status"
        const val ERROR_MESSAGE = "error_message"
        const val SOURCE = "source"

        const val ACCOUNT_SIGN_UP_DURATION = "account_sign_up_duration"
        const val FORM_FILL_DURATION = "form_fill_duration"

        const val HAS_FILTERS = "has_filters"
        const val GROUP_NAME = "group_name"
        const val GROUP_UUID = "group_uuid"
        const val VALUE_NAME = "value_name"
        const val FORCE_RELOADED = "force_reloaded"

        const val PRODUCT_NAME = "product_name"
        const val PRODUCT_UUID = "product_uuid"
        const val QUANTITY = "quantity"
        const val PRICE = "price"
        const val SUM = "sum"
        const val COUNT = "count"

        const val CARD_TYPE = "card_type"
        const val PROMO_CODE = "promo_code"

        const val ORDER_QUANTITY = "quantity"
        const val ORDER_TOTAL = "total"
        const val ORDER_ID = "order_id"
        const val ORDER_TYPE = "order_type"
        const val PRODUCT_UUID_LIST = "product_uuid_list_str"

        const val AMOUNT = "amount"
        const val THRESHOLD_BALANCE = "threshold_balance"
        const val CARD_NICKNAME = "nickname"

        const val OPTION_NAME = "item_name"
        const val TITLE = "notification_title"
        const val UUID = "notification_uuid"
        const val PAGE_NUMBER = "page_number"

        const val STORAGE = "storage"
        const val HAS_ENABLERS = "has_enablers"
        const val HAS_CLOUD_PASS = "has_cloud_pass"

        const val VOICE_OVER_ENABLED = "voice_over_enabled"
        const val FONT_SCALE = "font_scale"
        const val LANGUAGE = "language"
    }
}