package co.bytemark.analytics

import co.bytemark.analytics.AnalyticsPlatformsContract.BuildIdentifier.FIREBASE
import co.bytemark.sdk.model.config.BuildIdentifier
import java.util.*

/**
 * Concrete implementation to get all the analyticsPlatform platforms that should be created
 * from features endpoint.
 */
object AnalyticsPlatformsFactory {
    @JvmStatic
    fun getAnalyticsPlatforms(buildIdentifiers: List<BuildIdentifier>): List<AnalyticsPlatform> {
        val analyticsPlatforms: MutableList<AnalyticsPlatform> = ArrayList()
        buildIdentifiers.forEach { buildIdentifier ->
            createAnalyticsPlatformFromBuildIdentifier(buildIdentifier)?.let {
                analyticsPlatforms.add(it)
            }
        }
        return analyticsPlatforms
    }

    private fun createAnalyticsPlatformFromBuildIdentifier(buildIdentifier: BuildIdentifier): AnalyticsPlatform? =
        when (buildIdentifier.service) {
            FIREBASE ->
                FirebaseAnalyticsAdapter()
            else ->
                null
        }
}