package co.bytemark.analytics

import android.app.Activity
import android.app.Application
import android.content.Context

/**
 * Adapter class to call individual analytics platform.
 */
class AnalyticsPlatformAdapter(
    application: Application,
    private val analyticsPlatformPlatforms: List<AnalyticsPlatform>
) : AbstractAnalyticsPlatform() {

    init {
        this.initializeAnalyticsPlatform(application)
    }

    override fun initializeAnalyticsPlatform(application: Application) =
        analyticsPlatformPlatforms.forEach { it.initializeAnalyticsPlatform(application) }

    override fun startAnalyticsPlatforms(activity: Activity) =
        analyticsPlatformPlatforms.forEach { it.startAnalyticsPlatforms(activity) }

    override fun stopAnalyticsPlatforms(context: Context) =
        analyticsPlatformPlatforms.forEach { it.stopAnalyticsPlatforms(context) }


    override fun webViewPageDisplayed(title: String) =
        analyticsPlatformPlatforms.forEach { it.webViewPageDisplayed(title) }

    override fun tutorialScreenDisplayed(ticketStorageTutorialScreen: String) =
        analyticsPlatformPlatforms.forEach { it.tutorialScreenDisplayed(ticketStorageTutorialScreen) }

    override fun tutorialButtonPressed(button: String) =
        analyticsPlatformPlatforms.forEach { it.tutorialButtonPressed(button) }

    override fun signIn(
        origin: String?,
        signInType: String,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach { it.signIn(origin, signInType, status, errorMessage) }

    override fun signInScreenDisplayed() =
        analyticsPlatformPlatforms.forEach { it.signInScreenDisplayed() }

    override fun signUpScreenDisplayed() =
        analyticsPlatformPlatforms.forEach { it.signUpScreenDisplayed() }

    override fun signUp(
        accountSignUpDuration: Long,
        formFillDuration: Long,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.signUp(
                accountSignUpDuration,
                formFillDuration,
                status,
                errorMessage
            )
        }

    override fun forgotPassword(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.forgotPassword(status, errorMessage) }

    override fun signOut(type: Int) =
        analyticsPlatformPlatforms.forEach { it.signOut(type) }

    override fun storeScreenDisplayed(source: String?) =
        analyticsPlatformPlatforms.forEach { it.storeScreenDisplayed(source) }

    override fun filterLoadingCompleted(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.filterLoadingCompleted(status, errorMessage) }

    override fun filterApplied(groupName: String?, groupUuid: String?, valueName: String?) =
        analyticsPlatformPlatforms.forEach { it.filterApplied(groupName, groupUuid, valueName) }

    override fun productsLoadingCompleted(forceReload: Int, status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach {
            it.productsLoadingCompleted(
                forceReload,
                status,
                errorMessage
            )
        }

    override fun addToCart(
        productName: String?,
        productUuid: String?,
        quantity: Int,
        price: Double
    ) =
        analyticsPlatformPlatforms.forEach {
            it.addToCart(
                productName,
                productUuid,
                quantity,
                price
            )
        }

    override fun changeFilter() =
        analyticsPlatformPlatforms.forEach { it.changeFilter() }

    override fun clearFilter() =
        analyticsPlatformPlatforms.forEach { it.clearFilter() }

    override fun expressCheckout(
        productName: String?,
        productUuid: String?,
        quantity: Int,
        price: Double,
        type: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.expressCheckout(
                productName,
                productUuid,
                quantity,
                price,
                type
            )
        }

    override fun cartScreenDisplayed(source: String?) =
        analyticsPlatformPlatforms.forEach { it.cartScreenDisplayed(source) }

    override fun viewStoreSelected() =
        analyticsPlatformPlatforms.forEach { it.viewStoreSelected() }

    override fun itemRemoved(
        productName: String?,
        productUuid: String?,
        quantity: Int,
        price: Double
    ) =
        analyticsPlatformPlatforms.forEach {
            it.itemRemoved(
                productName,
                productUuid,
                quantity,
                price
            )
        }

    override fun checkoutScreenDisplayed(source: String?) =
        analyticsPlatformPlatforms.forEach { it.checkoutScreenDisplayed(source) }

    override fun defaultPaymentLoading(
        paymentType: String?,
        cardType: String?,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.defaultPaymentLoading(
                paymentType,
                cardType,
                status,
                errorMessage
            )
        }

    override fun addPaymentSelected(source: String) =
        analyticsPlatformPlatforms.forEach { it.addPaymentSelected(source) }

    override fun paymentMethodChanged(origin: String?, paymentType: String?) =
        analyticsPlatformPlatforms.forEach { it.paymentMethodChanged(origin, paymentType) }

    override fun updateOptionSelected() =
        analyticsPlatformPlatforms.forEach { it.updateOptionSelected() }

    override fun promoCodeApplied(
        screen: String?,
        promoCode: String?,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.promoCodeApplied(
                screen,
                promoCode,
                status,
                errorMessage
            )
        }

    override fun orderPlaced(
        orderQuantity: Int,
        orderTotal: Double,
        orderId: String?,
        orderType: String,
        productUuidList: String?,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.orderPlaced(
                orderQuantity,
                orderTotal,
                orderId,
                orderType,
                productUuidList,
                status,
                errorMessage
            )
        }

    override fun productPurchased(
        uuid: String?,
        name: String?,
        price: Double,
        sum: Double,
        quantity: Int
    ) =
        analyticsPlatformPlatforms.forEach { it.productPurchased(uuid, name, price, sum, quantity) }

    override fun paymentMethodsLoaded(
        count: Int,
        forceReload: Int,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.paymentMethodsLoaded(
                count,
                forceReload,
                status,
                errorMessage
            )
        }

    override fun paymentMethodRemoved(type: String?, status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.paymentMethodRemoved(type, status, errorMessage) }

    override fun paymentMethodAdded(
        type: String?,
        cardType: String?,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.paymentMethodAdded(
                type,
                cardType,
                status,
                errorMessage
            )
        }

    override fun featureScanCardSelected() =
        analyticsPlatformPlatforms.forEach { it.featureScanCardSelected() }

    override fun loadMoney(source: String?, amount: Double, status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.loadMoney(source, amount, status, errorMessage) }

    override fun saveAutoLoad(
        source: String?,
        amount: Double,
        thresholdBalance: Double,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.saveAutoLoad(
                source,
                amount,
                thresholdBalance,
                status,
                errorMessage
            )
        }

    override fun removeAutoLoad(source: String?, status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.removeAutoLoad(source, status, errorMessage) }


    override fun createVirtualCard(
        cardType: String?,
        nickname: String?,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.createVirtualCard(
                cardType,
                nickname,
                status,
                errorMessage
            )
        }

    override fun transferVirtualCard(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.transferVirtualCard(status, errorMessage) }

    override fun existingCardLinked(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.existingCardLinked(status, errorMessage) }

    override fun addPassesToFareMediumSelected() =
        analyticsPlatformPlatforms.forEach { it.addPassesToFareMediumSelected() }

    override fun availablePassesLoaded(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.availablePassesLoaded(status, errorMessage) }

    override fun fareCappingLoaded(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.fareCappingLoaded(status, errorMessage) }

    override fun cardHistoryScreenLoaded(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.cardHistoryScreenLoaded(status, errorMessage) }

    override fun manageCardsScreenOpened(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.manageCardsScreenOpened(status, errorMessage) }

    override fun moreInfoScreenDisplayed() =
        analyticsPlatformPlatforms.forEach { it.moreInfoScreenDisplayed() }

    override fun moreInfoOptionSelected(optionName: String?) =
        analyticsPlatformPlatforms.forEach { it.moreInfoOptionSelected(optionName) }

    override fun profileLoadingCompleted(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.profileLoadingCompleted(status, errorMessage) }

    override fun profileUpdated(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.profileUpdated(status, errorMessage) }

    override fun passwordChanged(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.passwordChanged(status, errorMessage) }

    override fun settingsScreenDisplayed() =
        analyticsPlatformPlatforms.forEach { it.settingsScreenDisplayed() }

    override fun notificationSettingsLoaded(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.notificationSettingsLoaded(status, errorMessage) }

    override fun accountPhotoScreenDisplayed() =
        analyticsPlatformPlatforms.forEach { it.accountPhotoScreenDisplayed() }

    override fun accountPhotoUpdated(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.accountPhotoUpdated(status, errorMessage) }

    override fun voucherCodeScreenDisplayed() =
        analyticsPlatformPlatforms.forEach { it.voucherCodeScreenDisplayed() }

    override fun voucherCodeApplied(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.voucherCodeApplied(status, errorMessage) }

    override fun haconDepartureMenuSelected() =
        analyticsPlatformPlatforms.forEach { it.haconDepartureMenuSelected() }

    override fun haconTripPlannerMenuSelected() =
        analyticsPlatformPlatforms.forEach { it.haconTripPlannerMenuSelected() }

    override fun haconAlarmsMenuSelected() =
        analyticsPlatformPlatforms.forEach { it.haconAlarmsMenuSelected() }

    override fun notificationScreenDisplayed() =
        analyticsPlatformPlatforms.forEach { it.notificationScreenDisplayed() }

    override fun notificationDetailsScreenDisplayed(title: String?, uuid: String?) =
        analyticsPlatformPlatforms.forEach { it.notificationDetailsScreenDisplayed(title, uuid) }

    override fun notificationMoreInfoSelected(title: String?, uuid: String?) =
        analyticsPlatformPlatforms.forEach { it.notificationMoreInfoSelected(title, uuid) }

    override fun voiceOverEnabled() =
        analyticsPlatformPlatforms.forEach { it.voiceOverEnabled() }

    override fun fontSizeChanged() =
        analyticsPlatformPlatforms.forEach { it.fontSizeChanged() }

    override fun languageChanged() =
        analyticsPlatformPlatforms.forEach { it.languageChanged() }

    override fun useTicketsScreenDisplayed() =
        analyticsPlatformPlatforms.forEach { it.useTicketsScreenDisplayed() }

    override fun cloudPassesLoaded(
        type: String?,
        noOfPasses: Int,
        forceReload: Int,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.cloudPassesLoaded(
                type,
                noOfPasses,
                forceReload,
                status,
                errorMessage
            )
        }

    override fun devicePassesLoaded(
        type: String?,
        noOfPasses: Int,
        forceReload: Int,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.devicePassesLoaded(
                type,
                noOfPasses,
                forceReload,
                status,
                errorMessage
            )
        }

    override fun featureTransferPass(
        source: String,
        storage: String?,
        productName: String?,
        productUuid: String?,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.featureTransferPass(
                source,
                storage,
                productName,
                productUuid,
                status,
                errorMessage
            )
        }

    override fun featureSendTicket(
        productUuid: String?,
        productName: String?,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.featureSendTicket(
                productUuid,
                productName,
                status,
                errorMessage
            )
        }

    override fun featureRepurchase(
        source: String,
        productUuid: String?,
        productName: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.featureRepurchase(
                source,
                productUuid,
                productName
            )
        }

    override fun featureResendReceipt(
        source: String,
        productUuid: String?,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.featureResendReceipt(
                source,
                productUuid,
                status,
                errorMessage
            )
        }

    override fun featureViewTicketDetails(source: String, productUuid: String?) =
        analyticsPlatformPlatforms.forEach { it.featureViewTicketDetails(source, productUuid) }

    override fun activateTicket(
        noOfTickets: Int,
        hasEnablers: Int,
        hasCloudPass: Int,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.activateTicket(
                noOfTickets,
                hasEnablers,
                hasCloudPass,
                status,
                errorMessage
            )
        }

    override fun displayTicket(noOfTickets: Int, status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.displayTicket(noOfTickets, status, errorMessage) }

    override fun fareMediumsLoaded(status: String, errorMessage: String?) =
        analyticsPlatformPlatforms.forEach { it.fareMediumsLoaded(status, errorMessage) }

    override fun ticketStorageScreenDisplayed() =
        analyticsPlatformPlatforms.forEach { it.ticketStorageScreenDisplayed() }

    override fun defaultTicketStorageChanged(to: String?) =
        analyticsPlatformPlatforms.forEach { it.defaultTicketStorageChanged(to) }

    override fun purchaseHistoryScreenDisplayed(
        pageNumber: Int,
        status: String,
        errorMessage: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.purchaseHistoryScreenDisplayed(
                pageNumber,
                status,
                errorMessage
            )
        }

    override fun receiptScreenDisplayed() =
        analyticsPlatformPlatforms.forEach { it.receiptScreenDisplayed() }

    override fun accessibilityState(
        voiceOverEnabled: Int,
        fontScale: Double,
        language: String?
    ) =
        analyticsPlatformPlatforms.forEach {
            it.accessibilityState(
                voiceOverEnabled,
                fontScale,
                language
            )
        }
}