package co.bytemark.analytics

import android.app.Application
import android.os.Bundle
import co.bytemark.analytics.AnalyticsPlatformsContract.Parameters
import co.bytemark.sdk.BytemarkSDK
import com.google.firebase.analytics.FirebaseAnalytics

private const val FIREBASE_PARAM_CHAR_MAX_LIMIT = 100

internal class FirebaseAnalyticsAdapter : AbstractAnalyticsPlatform() {

    private var firebaseAnalytics: FirebaseAnalytics? = null

    override fun initializeAnalyticsPlatform(application: Application) {
        if (firebaseAnalytics == null) {
            firebaseAnalytics = FirebaseAnalytics.getInstance(application.applicationContext)
            FirebaseAnalytics.getInstance(application.applicationContext)
                .setAnalyticsCollectionEnabled(true)
        }
    }

    override fun webViewPageDisplayed(title: String) {
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.SIGN_IN, Bundle().apply {
            putString(Parameters.TITLE, title)
        })
    }

    override fun tutorialScreenDisplayed(ticketStorageTutorialScreen: String) {
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.SIGN_IN, Bundle().apply {
            putString(Parameters.SOURCE, ticketStorageTutorialScreen)
        })
    }

    override fun tutorialButtonPressed(button: String) {
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.SIGN_IN, Bundle().apply {
            putString(Parameters.OPTION_NAME, button)
        })
    }

    override fun signIn(
        origin: String?,
        signInType: String,
        status: String,
        errorMessage: String?
    ) {
        updateUserProperty()
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.SIGN_IN, Bundle().apply {
            putString(Parameters.FROM, origin)
            putString(Parameters.TYPE, signInType)
            putString(Parameters.STATUS, status)
            errorMessage?.let {
                if (errorMessage.isNotEmpty()) {
                    putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                }
            }
        })
    }

    override fun signInScreenDisplayed() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.SIGN_IN_SCREEN_DISPLAYED,
            Bundle()
        )
    }

    override fun signUpScreenDisplayed() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.SIGN_UP_SCREEN_DISPLAYED,
            Bundle()
        )
    }

    override fun signUp(
        accountSignUpDuration: Long,
        formFillDuration: Long,
        status: String,
        errorMessage: String?
    ) {
        updateUserProperty()
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.SIGN_UP, Bundle().apply {
            putLong(Parameters.ACCOUNT_SIGN_UP_DURATION, accountSignUpDuration)
            putLong(Parameters.FORM_FILL_DURATION, formFillDuration)
            putString(Parameters.STATUS, status)
            errorMessage?.let {
                if (errorMessage.isNotEmpty()) {
                    putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                }
            }
        })
    }

    override fun forgotPassword(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.FORGOT_PASSWORD,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun signOut(type: Int) {
        updateUserProperty()
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.SIGN_OUT, Bundle().apply {
            putInt(Parameters.TYPE, type)
        })
    }

    override fun storeScreenDisplayed(source: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.STORE_SCREEN_DISPLAYED,
            Bundle().apply {
                putString(Parameters.SOURCE, source)
            })
    }

    override fun filterLoadingCompleted(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.FILTERS_LOADING_COMPLETED,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun filterApplied(groupName: String?, groupUuid: String?, valueName: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.FILTERS_APPLIED,
            Bundle().apply {
                putString(Parameters.GROUP_NAME, groupName)
                putString(Parameters.GROUP_UUID, groupUuid)
                putString(Parameters.VALUE_NAME, valueName)
            })
    }

    override fun productsLoadingCompleted(forceReload: Int, status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.PRODUCT_LOADING_COMPLETED,
            Bundle().apply {
                putInt(Parameters.FORCE_RELOADED, forceReload)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun addToCart(
        productName: String?,
        productUuid: String?,
        quantity: Int,
        price: Double
    ) {
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.ADD_TO_CART, Bundle().apply {
            putString(Parameters.PRODUCT_NAME, productName)
            putString(Parameters.PRODUCT_UUID, productUuid)
            putInt(Parameters.QUANTITY, quantity)
            putDouble(Parameters.PRICE, price)
            putDouble(Parameters.SUM, price * quantity)
        })
    }

    override fun changeFilter() {
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.CHANGE_FILTER, Bundle())
    }

    override fun clearFilter() {
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.CLEAR_FILTER, Bundle())
    }

    override fun expressCheckout(
        productName: String?,
        productUuid: String?,
        quantity: Int,
        price: Double,
        type: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.FEATURE_EXPRESS_CHECKOUT,
            Bundle().apply {
                putString(Parameters.PRODUCT_NAME, productName)
                putString(Parameters.PRODUCT_UUID, productUuid)
                putInt(Parameters.QUANTITY, quantity)
                putDouble(Parameters.PRICE, price)
                putDouble(Parameters.SUM, price * quantity)
                putString(Parameters.TYPE, type)
            })
    }

    override fun cartScreenDisplayed(source: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.CART_SCREEN_DISPLAYED,
            Bundle().apply {
                putString(Parameters.SOURCE, source)
            })
    }

    override fun viewStoreSelected() {
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.VIEW_STORE_SELECTED, Bundle())
    }

    override fun itemRemoved(
        productName: String?,
        productUuid: String?,
        quantity: Int,
        price: Double
    ) {
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.ITEM_REMOVED, Bundle().apply {
            putString(Parameters.PRODUCT_NAME, productName)
            putString(Parameters.PRODUCT_UUID, productUuid)
            putInt(Parameters.QUANTITY, quantity)
            putDouble(Parameters.PRICE, price)
            putDouble(Parameters.SUM, price * quantity)
        })
    }

    override fun checkoutScreenDisplayed(source: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.CHECKOUT_SCREEN_DISPLAYED,
            Bundle().apply {
                putString(Parameters.SOURCE, source)
            })
    }

    override fun defaultPaymentLoading(
        paymentType: String?,
        cardType: String?,
        status: String,
        errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.DEFAULT_PAYMENT_LOADING_COMPLETED,
            Bundle().apply {
                putString(Parameters.TYPE, paymentType)
                putString(Parameters.CARD_TYPE, cardType)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun addPaymentSelected(source: String) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.ADD_PAYMENT_SELECTED,
            Bundle().apply {
                putString(Parameters.SOURCE, source)
            })
    }

    override fun paymentMethodChanged(origin: String?, paymentType: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.PAYMENT_METHOD_CHANGED,
            Bundle().apply {
                putString(Parameters.SOURCE, origin)
                putString(Parameters.TYPE, paymentType)
            })
    }

    override fun updateOptionSelected() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.UPDATE_OPTION_SELECTED,
            Bundle()
        )
    }

    override fun promoCodeApplied(
        screen: String?,
        promoCode: String?,
        status: String,
        errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.PROMO_CODE_APPLIED,
            Bundle().apply {
                putString(Parameters.SOURCE, screen)
                putString(Parameters.PROMO_CODE, promoCode)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun orderPlaced(
        orderQuantity: Int, orderTotal: Double, orderId: String?, orderType: String,
        productUuidList: String?, status: String, errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.PLACE_ORDER, Bundle().apply {
            putInt(Parameters.ORDER_QUANTITY, orderQuantity)
            putDouble(Parameters.ORDER_TOTAL, orderTotal)
            putString(Parameters.ORDER_ID, orderId)
            putString(Parameters.ORDER_TYPE, orderType)
            putString(Parameters.PRODUCT_UUID_LIST, productUuidList)
            putString(Parameters.STATUS, status)
            errorMessage?.let {
                if (errorMessage.isNotEmpty()) {
                    putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                }
            }
        })
    }

    override fun productPurchased(
        uuid: String?,
        name: String?,
        price: Double,
        sum: Double,
        quantity: Int
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.PRODUCT_PURCHASED,
            Bundle().apply {
                putString(Parameters.PRODUCT_UUID, uuid)
                putString(Parameters.PRODUCT_NAME, name)
                putDouble(Parameters.PRICE, price)
                putDouble(Parameters.SUM, sum)
                putInt(Parameters.QUANTITY, quantity)
            })
    }

    override fun paymentMethodsLoaded(
        count: Int,
        forceReload: Int,
        status: String,
        errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.PAYMENT_METHODS_LOADED,
            Bundle().apply {
                putInt(Parameters.COUNT, count)
                putInt(Parameters.FORCE_RELOADED, forceReload)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun paymentMethodRemoved(type: String?, status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.REMOVE_PAYMENT_METHOD,
            Bundle().apply {
                putString(Parameters.TYPE, type)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun paymentMethodAdded(
        type: String?,
        cardType: String?,
        status: String,
        errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.NEW_PAYMENT_METHOD_ADDED,
            Bundle().apply {
                putString(Parameters.TYPE, type)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun featureScanCardSelected() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.FEATURE_SCAN_CARD_SELECTED,
            Bundle()
        )
    }

    override fun loadMoney(source: String?, amount: Double, status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.LOAD_MONEY, Bundle().apply {
            putString(Parameters.SOURCE, source)
            putDouble(Parameters.AMOUNT, amount)
            putString(Parameters.STATUS, status)
            errorMessage?.let {
                if (errorMessage.isNotEmpty()) {
                    putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                }
            }
        })
    }

    override fun saveAutoLoad(
        source: String?,
        amount: Double,
        thresholdBalance: Double,
        status: String,
        errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.SAVE_AUTO_LOAD,
            Bundle().apply {
                putString(Parameters.SOURCE, source)
                putDouble(Parameters.AMOUNT, amount)
                putDouble(Parameters.THRESHOLD_BALANCE, thresholdBalance)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun removeAutoLoad(source: String?, status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.REMOVE_AUTO_LOAD,
            Bundle().apply {
                putString(Parameters.SOURCE, source)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun createVirtualCard(
        cardType: String?,
        nickname: String?,
        status: String,
        errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.CREATE_VIRTUAL_CARD,
            Bundle().apply {
                putString(Parameters.CARD_TYPE, cardType)
                putString(Parameters.CARD_NICKNAME, nickname)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun transferVirtualCard(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.TRANSFER_VIRTUAL_CARD,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun existingCardLinked(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.LINK_EXISTING_CARD,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun addPassesToFareMediumSelected() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.ADD_PASSES_TO_FARE_MEDIUM,
            Bundle()
        )
    }

    override fun availablePassesLoaded(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.AVAILABLE_PASSES_LOADED,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun fareCappingLoaded(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.FARE_CAPPING_DISPLAYED,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun cardHistoryScreenLoaded(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.CARD_HISTORY_DISPLAYED,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun manageCardsScreenOpened(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.MANAGE_SCREEN_DISPLAYED,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun moreInfoScreenDisplayed() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.MORE_INFO_SCREEN_DISPLAYED,
            Bundle()
        )
    }

    override fun moreInfoOptionSelected(optionName: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.MORE_INFO_OPTION_SELECTED,
            Bundle().apply {
                putString(Parameters.OPTION_NAME, optionName)
            })
    }

    override fun profileLoadingCompleted(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.PROFILE_LOADING_COMPLETED,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun profileUpdated(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.UPDATE_PROFILE,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun passwordChanged(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.CHANGE_PASSWORD,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun notificationSettingsLoaded(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.NOTIFICATION_SETTINGS_SCREEN_DISPLAYED,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun settingsScreenDisplayed() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.SETTINGS_SCREEN_DISPLAYED,
            Bundle()
        )
    }

    override fun accountPhotoScreenDisplayed() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.ACCOUNT_PHOTO_SCREEN_DISPLAYED,
            Bundle()
        )
    }

    override fun accountPhotoUpdated(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.ACCOUNT_PHOTO_UPDATED,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun voucherCodeScreenDisplayed() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.VOUCHER_CODE_SCREEN_DISPLAYED,
            Bundle()
        )
    }

    override fun voucherCodeApplied(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.VOUCHER_CODE_APPLIED,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun haconDepartureMenuSelected() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.HACON_DEPARTURE_OPTION_SELECTED,
            Bundle()
        )
    }

    override fun haconTripPlannerMenuSelected() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.HACON_TRIP_PLANNER_SELECTED,
            Bundle()
        )
    }

    override fun haconAlarmsMenuSelected() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.HACON_ALARMS_SELECTED,
            Bundle()
        )
    }

    override fun notificationScreenDisplayed() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.NOTIFICATION_SCREEN_DISPLAYED,
            Bundle()
        )
    }

    override fun notificationDetailsScreenDisplayed(title: String?, uuid: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.NOTIFICATION_DETAILS_SCREEN_DISPLAYED,
            Bundle().apply {
                putString(Parameters.TITLE, title)
                putString(Parameters.UUID, uuid)
            })
    }

    override fun notificationMoreInfoSelected(title: String?, uuid: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.NOTIFICATION_MORE_INFO_SELECTED,
            Bundle().apply {
                putString(Parameters.TITLE, title)
                putString(Parameters.UUID, uuid)
            })
    }

    override fun voiceOverEnabled() {
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.VOICE_OVER_ENABLED, Bundle())
    }

    override fun fontSizeChanged() {
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.FONT_SIZE_CHANGED, Bundle())

    }

    override fun languageChanged() {
        firebaseAnalytics?.logEvent(AnalyticsPlatformsContract.Events.LANGUAGE_CHANGED, Bundle())
    }


    override fun useTicketsScreenDisplayed() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.USE_TICKETS_SCREEN_OPENED,
            Bundle()
        )
    }

    override fun cloudPassesLoaded(
        type: String?,
        noOfPasses: Int,
        forceReload: Int,
        status: String,
        errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.CLOUD_PASSES_LOADING_COMPLETED,
            Bundle().apply {
                putString(Parameters.SOURCE, type)
                putInt(Parameters.COUNT, noOfPasses)
                putInt(Parameters.FORCE_RELOADED, forceReload)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun devicePassesLoaded(
        type: String?,
        noOfPasses: Int,
        forceReload: Int,
        status: String,
        errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.DEVICE_PASSES_LOADING_COMPLETED,
            Bundle().apply {
                putString(Parameters.SOURCE, type)
                putInt(Parameters.COUNT, noOfPasses)
                putInt(Parameters.FORCE_RELOADED, forceReload)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }


    override fun featureTransferPass(
        source: String,
        storage: String?,
        productName: String?,
        productUuid: String?,
        status: String,
        errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.FEATURE_TRANSFER_PASS,
            Bundle().apply {
                putString(Parameters.SOURCE, source)
                putString(Parameters.STORAGE, storage)
                putString(Parameters.PRODUCT_NAME, productName)
                putString(Parameters.PRODUCT_UUID, productUuid)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun featureSendTicket(
        productUuid: String?,
        productName: String?,
        status: String,
        errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.FEATURE_SEND_TICKET,
            Bundle().apply {
                putString(Parameters.PRODUCT_UUID, productUuid)
                putString(Parameters.PRODUCT_NAME, productName)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun featureRepurchase(source: String, productUuid: String?, productName: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.FEATURE_REPURCHASE,
            Bundle().apply {
                putString(Parameters.SOURCE, source)
                putString(Parameters.PRODUCT_UUID, productUuid)
                putString(Parameters.PRODUCT_NAME, productName)
            })
    }

    override fun featureResendReceipt(
        source: String,
        productUuid: String?,
        status: String,
        errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.FEATURE_RESEND_RECEIPT,
            Bundle().apply {
                putString(Parameters.SOURCE, source)
                putString(Parameters.PRODUCT_UUID, productUuid)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun featureViewTicketDetails(source: String, productUuid: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.FEATURE_VIEW_TICKET_DETAILS,
            Bundle().apply {
                putString(Parameters.SOURCE, source)
                putString(Parameters.PRODUCT_UUID, productUuid)
            })
    }

    override fun activateTicket(
        noOfTickets: Int,
        hasEnablers: Int,
        hasCloudPass: Int,
        status: String,
        errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.ACTIVATE_TICKET,
            Bundle().apply {
                putInt(Parameters.COUNT, noOfTickets)
                putInt(Parameters.HAS_ENABLERS, hasEnablers)
                putInt(Parameters.HAS_CLOUD_PASS, hasCloudPass)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun displayTicket(noOfTickets: Int, status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.DISPLAY_TICKET,
            Bundle().apply {
                putInt(Parameters.COUNT, noOfTickets)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun fareMediumsLoaded(status: String, errorMessage: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.FARE_MEDIUMS_LOADED,
            Bundle().apply {
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun ticketStorageScreenDisplayed() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.TICKET_STORAGE_SCREEN_DISPLAYED,
            Bundle()
        )
    }

    override fun defaultTicketStorageChanged(to: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.DEFAULT_STORAGE_CHANGED,
            Bundle().apply {
                putString(Parameters.STORAGE, to)
            })
    }

    override fun purchaseHistoryScreenDisplayed(
        pageNumber: Int,
        status: String,
        errorMessage: String?
    ) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.PURCHASE_HISTORY_SCREEN_DISPLAYED,
            Bundle().apply {
                putInt(Parameters.PAGE_NUMBER, pageNumber)
                putString(Parameters.STATUS, status)
                errorMessage?.let {
                    if (errorMessage.isNotEmpty()) {
                        putString(Parameters.ERROR_MESSAGE, trimToLimit(errorMessage))
                    }
                }
            })
    }

    override fun receiptScreenDisplayed() {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.RECEIPT_SCREEN_DISPLAYED,
            Bundle()
        )
    }

    override fun accessibilityState(voiceOverEnabled: Int, fontScale: Double, language: String?) {
        firebaseAnalytics?.logEvent(
            AnalyticsPlatformsContract.Events.ACCESSIBILITY_STATE,
            Bundle().apply {
                putInt(Parameters.VOICE_OVER_ENABLED, voiceOverEnabled)
                putDouble(Parameters.FONT_SCALE, fontScale)
                putString(Parameters.LANGUAGE, language)
            })
    }

    private fun updateUserProperty() {
        firebaseAnalytics?.setUserProperty(
            AnalyticsPlatformsContract.UserProperty.LOGIN_STATUS,
            if (BytemarkSDK.isLoggedIn()) "true" else "false"
        )
    }

    private fun trimToLimit(string: String): String =
        if (string.length > FIREBASE_PARAM_CHAR_MAX_LIMIT) {
            string.substring(0, 100)
        } else {
            string
        }
}