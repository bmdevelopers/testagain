package co.bytemark.buy_tickets;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputFilter;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityManager;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.add_payment_card.Helper.TextWatcherHelper;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.model.product_search.entity.EntityResult;
import co.bytemark.shopping_cart_new.NewShoppingCartActivityFragment;
import timber.log.Timber;

import static android.text.TextUtils.isEmpty;

public class BuyTicketsAdapter extends RecyclerView.Adapter<ProductViewHolder> {

    private final List<EntityResult> entityResults = new ArrayList<>();
    private final Fragment fragment;

    @Inject
    Application application;

    @Inject
    ConfHelper confHelper;

    @Inject
    Gson gson;

    @Inject
    SharedPreferences sharedPreferences;

    @Inject
    AnalyticsPlatformAdapter analyticsPlatformAdapter;

    private String[] productQtys;

    private final Context context;

    public BuyTicketsAdapter(Fragment fragment) {
        CustomerMobileApp.Companion.getComponent().inject(this);
        this.fragment = fragment;
        this.context = fragment.getActivity().getApplicationContext();
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(application).inflate(R.layout.card_view_buy_tickets, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, int position) {
        holder.editTextProductQty.setEnabled(true);

        String backgroundColor = null;
        int defaultBackgroundColor = 0;

        String childOrgUuid = entityResults.get(holder.getAdapterPosition()).getOrganization().getLegacyUuid();

        if (entityResults.get(holder.getAdapterPosition()).getTheme() == null) {
            defaultBackgroundColor = confHelper.getOrganizationHeaderThemeColor(childOrgUuid);
        } else if (entityResults.get(holder.getAdapterPosition()).getTheme().getBackgroundColor() != null) {
            backgroundColor = entityResults.get(holder.getAdapterPosition()).getTheme().getBackgroundColor();
        } else {
            backgroundColor = entityResults.get(holder.getAdapterPosition()).getTheme().getProperties().getBackgroundColor();
        }

        int bgColor = defaultBackgroundColor != 0 ? defaultBackgroundColor : confHelper.parseColor(backgroundColor);

        holder.linearLayoutProduct.setBackgroundColor(confHelper.getDataThemeBackgroundColor());

        String uuid = entityResults.get(holder.getAdapterPosition()).getOrganization().getLegacyUuid().replaceAll("-", "_");

        Drawable drawable = null;
        try {
            drawable = confHelper.getDrawableByName("wide_" + uuid);
        } catch (Exception e) {
            Timber.e("Failure to get drawable id. %s", e.toString());
        }

        if (drawable == null) {
            final String parentOrgUuid = CustomerMobileApp.Companion.getConf().getOrganization().getUuid().replaceAll("-", "_");
            drawable = confHelper.getDrawableByName("wide_" + parentOrgUuid);
        }

        holder.imageViewProductMonochrome.setImageDrawable(drawable);

        holder.imageViewProductMonochrome.setBackgroundTintList(ColorStateList.valueOf(bgColor));

        holder.textViewProductAmount.setText(confHelper.getConfigurationPurchaseOptionsCurrencySymbol(entityResults.get(holder.getAdapterPosition()).getSalePrice()));
        holder.textViewProductAmount.setTextColor(confHelper.getDataThemeAccentColor());

        if (!isEmpty(entityResults.get(holder.getAdapterPosition()).getName())) {
            holder.textViewProductName.setText(entityResults.get(holder.getAdapterPosition()).getName());
        } else {
            holder.textViewProductName.setText(entityResults.get(holder.getAdapterPosition()).getLabelName());
        }
        holder.textViewProductName.setTextColor(confHelper.getDataThemePrimaryTextColor());

        holder.textViewProductShortDescription.setText(entityResults.get(holder.getAdapterPosition()).getShortDescription());
        holder.textViewProductShortDescription.setTextColor(confHelper.getDataThemeAccentColor());

        holder.textViewProductQty.setText(R.string.buy_tickets_qty);
        holder.textViewProductQty.setTextColor(confHelper.getDataThemePrimaryTextColor());

        holder.refPosition = holder.getAdapterPosition();


        if (!isEmpty(holder.editTextProductQty.getText()) && !isEmpty(productQtys[holder.getAdapterPosition()])) {
            final int oldValue = Integer.parseInt(holder.editTextProductQty.getText().toString());
            final int newValue = Integer.parseInt(productQtys[holder.getAdapterPosition()]);
            if (oldValue != newValue) {
                holder.editTextProductQty.setText(productQtys[holder.getAdapterPosition()]);
            }
        } else {
            holder.editTextProductQty.setText(productQtys[holder.getAdapterPosition()]);
        }
        holder.editTextProductQty.setTextColor(confHelper.getDataThemeAccentColor());

        if (fragment instanceof BuyTicketsActivityFragment) {
            holder.linearLayoutMoreInfo.setVisibility(View.VISIBLE);
            holder.textViewMoreInfo.setTextColor(confHelper.getDataThemeAccentColor());
            holder.imageViewInfo.setImageResource(R.drawable.ic_info_filled);
            holder.imageViewInfo.setImageTintList(ColorStateList.valueOf(confHelper.getDataThemeAccentColor()));

            holder.cardViewProductCard.setOnClickListener(v -> ((BuyTicketsActivityFragment) fragment).onCardViewClick(entityResults.get(holder.getAdapterPosition())));
            holder.linearLayoutMoreInfo.setOnClickListener(view -> ((BuyTicketsActivityFragment) fragment).onCardViewClick(entityResults.get(holder.getAdapterPosition())));
        } else
            holder.linearLayoutMoreInfo.setVisibility(View.GONE);

        holder.editTextProductQty.setFilters(new InputFilter[]{new QuantityFilter(1, 10), new InputFilter.LengthFilter(2)});
        holder.editTextProductQty.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
        holder.editTextProductQty.addTextChangedListener(new TextWatcherHelper() {
            @Override
            public void afterTextChanged(Editable editable) {
                productQtys[holder.getAdapterPosition()] = editable.toString();

                if (fragment instanceof NewShoppingCartActivityFragment) {

                    if (!editable.toString().isEmpty()) {

                        entityResults.get(holder.getAdapterPosition()).setQuantity(Integer.parseInt(editable.toString()));

                        ((NewShoppingCartActivityFragment) fragment).updateEntityResultList(entityResults);
                    }

                } else if (fragment instanceof BuyTicketsActivityFragment) {

                    if (!editable.toString().isEmpty()) {

                        entityResults.get(holder.getAdapterPosition()).setQuantity(Integer.parseInt(editable.toString()));

                    }
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                productQtys[holder.getAdapterPosition()] = charSequence.toString();
            }
        });

        holder.viewDivider.setBackgroundColor(confHelper.getDataThemePrimaryTextColor());

        if (fragment instanceof BuyTicketsActivityFragment) {
            holder.buttonProductAddToCart.setText(R.string.buy_tickets_add_to_cart);
            holder.buttonProductAddToCart.setContentDescription(context.getString(R.string.buy_tickets_add_cd_voonly) + " " + entityResults.get(position).getName() + context.getString(R.string.buy_tickets_to_cart_voonly));
            holder.buttonProductAddToCart.setBackgroundTintList(ColorStateList.valueOf(confHelper.getDataThemeAccentColor()));
            holder.buttonProductAddToCart.setTextColor(confHelper.getDataThemeBackgroundColor());
        } else {
            holder.buttonProductAddToCart.setAllCaps(false);
            holder.buttonProductAddToCart.setText(R.string.buy_tickets_delete);
            holder.buttonProductAddToCart.setContentDescription(context.getString(R.string.buy_tickets_remove_cd_voonly) + " " + entityResults.get(position).getName() + context.getString(R.string.buy_tickets_from_cart_voonly));
            holder.buttonProductAddToCart.setBackgroundTintList(ColorStateList.valueOf(confHelper.getIndicatorsError()));
            holder.buttonProductAddToCart.setTextColor(confHelper.getDataThemeBackgroundColor());
        }

        //holder.buttonProductAddToCart.setElevation(20);

        holder.buttonProductAddToCart.setOnClickListener(view -> {
            if (fragment instanceof BuyTicketsActivityFragment) {
                if (!productQtys[holder.getAdapterPosition()].isEmpty()) {
                    hideKeyboard(view);
                    holder.editTextProductQty.clearFocus();
                    final EntityResult entityResult = entityResults.get(holder.getAdapterPosition());
                    entityResult.setQuantity(Integer.parseInt(productQtys[holder.getAdapterPosition()]));
                    ((BuyTicketsActivityFragment) fragment).updateShoppingCart(entityResult, false, null);

                    if (!isEmpty(entityResult.getName())) {
                        analyticsPlatformAdapter.addToCart(entityResult.getName(), entityResult.getUuid(), entityResult.getQuantity(), confHelper.getConfigurationPurchaseOptionsCurrency(entityResult.getSalePrice()));
                    } else {
                        analyticsPlatformAdapter.addToCart(entityResult.getLabelName(), entityResult.getUuid(), entityResult.getQuantity(), confHelper.getConfigurationPurchaseOptionsCurrency(entityResult.getSalePrice()));

                    }
                }
            }
            if (fragment instanceof NewShoppingCartActivityFragment) {
                hideKeyboard(view);
                NewShoppingCartActivityFragment shoppingCartActivityFragment = (NewShoppingCartActivityFragment) fragment;

                final EntityResult entityResult = entityResults.get(holder.getAdapterPosition());
                String name = entityResult.getName();
                if (name.isEmpty()) {
                    name = entityResult.getLabelName();
                }
                analyticsPlatformAdapter.itemRemoved(name, entityResult.getUuid(), entityResult.getQuantity(), confHelper.getConfigurationPurchaseOptionsCurrency(entityResult.getSalePrice()));


                if (holder.getAdapterPosition() > -1) {
                    entityResults.remove(holder.getAdapterPosition());
                }
                ((NewShoppingCartActivityFragment) fragment).updateShoppingCart(entityResults);
            }
        });

        holder.linearLayoutQantityProduct.setContentDescription(context.getString(R.string.buy_tickets_quantity_is_voonly) + productQtys[holder.getAdapterPosition()]);
        AccessibilityManager am = (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
        if (am.isEnabled()) {
            String organizationName = null;
            for (int i = 0; i < CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations().size(); i++) {
                String orgUuid = CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations().get(i).getUuid();
                if (orgUuid.equals(entityResults.get(position).getOrganization().getLegacyUuid())) {
                    organizationName = CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations().get(i).getDisplayName();
                }
            }
            holder.imageViewProductMonochrome.setContentDescription(organizationName + context.getString(R.string.buy_tickets_logo_voonly));
            holder.cardViewProductCard.setContentDescription(holder.textViewProductName.getText().toString() + " " + holder.textViewProductAmount.getText().toString());

        }
    }

    private void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public int getItemCount() {
        return entityResults.size();
    }

    public void resetQuantities() {
        for (int i = 0; i < productQtys.length; i++) {
            productQtys[i] = "1";
        }
    }

    public void updateProductList(List<EntityResult> entityResultBuilder) {
        productQtys = new String[entityResultBuilder.size()];
        entityResults.clear();
        entityResults.addAll(entityResultBuilder);
        for (int i = 0; i < entityResultBuilder.size(); i++) {
            if (entityResultBuilder.get(i).getQuantity() != 0) {
                productQtys[i] = String.valueOf(entityResultBuilder.get(i).getQuantity());
            } else {

                if (fragment instanceof NewShoppingCartActivityFragment) {
                    productQtys[i] = "0";
                    entityResults.get(i).setQuantity(0);
                    ((NewShoppingCartActivityFragment) fragment).updateEntityResultList(entityResults);
                } else {
                    productQtys[i] = "1";
                }

            }
        }
        notifyDataSetChanged();
    }
}
