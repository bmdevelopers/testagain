package co.bytemark.buy_tickets;


import java.util.List;

import co.bytemark.mvp.MvpView;
import co.bytemark.sdk.model.product_search.entity.EntityResult;

interface BuyTicketsView extends MvpView {

    void showNetworkConnectionErrorView();

    void hideNetworkConnectionErrorView();

    void showLoading();

    void hideLoading();

    void setEntityResult(List<EntityResult> entityResultBuilder);

    void showNoAvailableProductsList();

    void showDeviceTimeErrorView();

    void hideDeviceTimeErrorView();

    void showGooglePayReady(boolean isGooglePayReady);

    void callLoadProducts();

    void showAppUpdateDialog();

    void showFilterScreen();
}
