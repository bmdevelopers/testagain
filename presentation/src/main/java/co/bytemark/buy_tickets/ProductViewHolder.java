package co.bytemark.buy_tickets;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;

class ProductViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ll_single_product)
    LinearLayout linearLayoutProduct;

    @BindView(R.id.cv_product_card)
    CardView cardViewProductCard;

    @BindView(R.id.iv_product_monochrome)
    ImageView imageViewProductMonochrome;

    @BindView(R.id.tv_product_amount)
    TextView textViewProductAmount;

    @BindView(R.id.tv_product_name)
    TextView textViewProductName;

    @BindView(R.id.tv_product_short_description)
    TextView textViewProductShortDescription;

    @BindView(R.id.tv_product_qty)
    TextView textViewProductQty;

    @BindView(R.id.et_product_qty)
    EditText editTextProductQty;

    @BindView(R.id.btn_product_add_to_cart)
    Button buttonProductAddToCart;

    @BindView(R.id.v_divider)
    View viewDivider;

    @BindView(R.id.llQantityProduct)
    LinearLayout linearLayoutQantityProduct;

    @BindView(R.id.tap_for_more_info)
    TextView textViewMoreInfo;

    @BindView(R.id.iv_info)
    ImageView imageViewInfo;

    @BindView(R.id.ll_more_information)
    LinearLayout linearLayoutMoreInfo;

    int refPosition;

    ProductViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
