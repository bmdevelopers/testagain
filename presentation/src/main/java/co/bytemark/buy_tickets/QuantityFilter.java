package co.bytemark.buy_tickets;

import android.text.InputFilter;
import android.text.Spanned;

public class QuantityFilter implements InputFilter {
    private final int min;
    private final int max;

    public QuantityFilter(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            final String replacement = source.subSequence(start, end).toString();
            final String newVal = dest.subSequence(0, dstart).toString() + replacement + dest.subSequence(dend, dest.length()).toString();
            final int input = Integer.parseInt(newVal);
            if (isInRange(input))
                return null;
        } catch (NumberFormatException ignored) {
        }
        return "";
    }

    private boolean isInRange(final int input) {
        return input >= min && input <= max;
    }
}
