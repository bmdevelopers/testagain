package co.bytemark.buy_tickets;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.analytics.AnalyticsPlatformsContract;
import co.bytemark.base.MasterActivity;
import co.bytemark.buy_tickets.filters.NewStoreFiltersFragment;
import co.bytemark.buy_tickets.filters.StoreFiltersFragment;
import co.bytemark.domain.model.discount.Discount;
import co.bytemark.domain.model.discount.DiscountCalculationRequest;
import co.bytemark.domain.model.discount.DiscountProduct;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.Helpers.PaymentMethod;
import co.bytemark.sdk.model.product_search.entity.EntityResult;
import co.bytemark.shopping_cart.AcceptPaymentActivity;
import co.bytemark.shopping_cart_new.NewShoppingCartActivity;
import co.bytemark.widgets.util.AddToCartAnimationUtil;
import timber.log.Timber;

import static co.bytemark.analytics.AnalyticsPlatformsContract.AnalyticsPlatformEntry.ADD_TO_CART_OVERLAY;
import static co.bytemark.analytics.AnalyticsPlatformsContract.AnalyticsPlatformEntry.HEADER_BUTTON;
import static co.bytemark.helpers.AppConstants.AUTH_TOKEN_FOR_DEEPLINK;
import static co.bytemark.helpers.AppConstants.DISCOUNT_PERCENT;
import static co.bytemark.helpers.AppConstants.PRODUCT_ID;
import static co.bytemark.helpers.AppConstants.QUANTITY;
import static co.bytemark.helpers.AppConstants.FROM_NATIVE_SCHEDULE;
import static co.bytemark.helpers.AppConstants.SERVICE_LEVEL;
import static co.bytemark.sdk.model.common.Action.STORE;

@SuppressWarnings("deprecation")
public class BuyTicketsActivity extends MasterActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    public String productUUID = "";
    public String productUUIDOfRepurchasePass;
    public String originUUID;
    public String destinationUUID;
    public String originShortName;
    public String destShortName;
    public String fareId;
    public String serviceLevel;
    public String jwtTokenForDeeplink;
    public DiscountCalculationRequest discountCalculationRequest;
    public int quantityFromDeeplink;
    public boolean searchFromDeeplink;
    public boolean isFromNativeSchedules;

    @Inject
    Gson gson;
    @Inject
    ConfHelper confHelper;
    @Inject
    SharedPreferences sharedPreferences;
    @BindView(R.id.iv_animate_dest)
    ImageView imageViewAnimationDestination;
    private boolean isProductAddedToCart;
    private boolean isIntentForReload = false;
    private String fareMediaUuid = null;
    private ImageView imageViewCart;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_buy_tickets;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomerMobileApp.Companion.getComponent().inject(this);

        setNavigationViewCheckedItem(STORE);

        String title = confHelper.getNavigationMenuScreenTitleFromTheConfig(STORE);
        setToolbarTitle(TextUtils.isEmpty(title) ? getString(R.string.screen_title_store) : title);

        isIntentForReload = getIntent().getBooleanExtra("reload", false);
        fareMediaUuid = getIntent().getStringExtra("fareMediumId");

        productUUID = getIntent().getStringExtra(AppConstants.PRODUCT_UUID);
        productUUIDOfRepurchasePass = getIntent().getStringExtra(AppConstants.PRODUCT_UUID_REPURCHASE);
        originUUID = getIntent().getStringExtra("filter_uuid_origin");
        destinationUUID = getIntent().getStringExtra("filter_uuid_dest");
        originShortName = getIntent().getStringExtra("origin_short_name");
        destShortName = getIntent().getStringExtra("dest_short_name");
        isFromNativeSchedules = getIntent().getBooleanExtra(FROM_NATIVE_SCHEDULE, false);
        fareId = getIntent().getStringExtra(AppConstants.FARE_ID);
        if (getIntent().hasExtra(AppConstants.INTENT_FOR_REPURCHASE)) {
            String type = getIntent().getStringExtra(AppConstants.USE_TICKETS_FRAGMENT_TYPE);
            String productName = getIntent().getStringExtra(AppConstants.PRODUCT_NAME);
            analyticsPlatformAdapter.featureRepurchase(type, productUUIDOfRepurchasePass, productName);
        }

        if (productUUID != null && !productUUID.isEmpty() || (productUUIDOfRepurchasePass != null && !productUUIDOfRepurchasePass.isEmpty())) {
            getSupportActionBar().hide();
        }

        String savedFareMediaId = sharedPreferences.getString(AppConstants.FARE_MEDIA_ID, null);
        if (fareMediaUuid != null
                && savedFareMediaId != null
                && !fareMediaUuid.equalsIgnoreCase(savedFareMediaId)) {
            sharedPreferences.edit()
                    .putString(AppConstants.ENTITY_RESULT_LIST, null)
                    .apply();
        }

        sharedPreferences.edit()
                .putString(AppConstants.FARE_MEDIA_ID, fareMediaUuid)
                .apply();

        String origin = AnalyticsPlatformsContract.Screen.MENU;
        if (getIntent() != null && getIntent().hasExtra(AppConstants.EXTRA_ORIGIN)) {
            origin = getIntent().getStringExtra(AppConstants.EXTRA_ORIGIN);
        }
        analyticsPlatformAdapter.storeScreenDisplayed(origin);
        loadStoreFilters();
        handleDeepLink();
    }

    private void handleDeepLink() {
        if (getIntent().getStringExtra(AppConstants.DEEPLINK_URL) != null) {
            Uri deepLink = Uri.parse(getIntent().getStringExtra(AppConstants.DEEPLINK_URL));
            if(deepLink.getPath().equals("/applink/discount")) {
                ArrayList<DiscountProduct> productsList = new ArrayList();
                try {
                    productsList.add(new DiscountProduct(0, 0, 0, deepLink.getQueryParameter(PRODUCT_ID),
                            deepLink.getQueryParameter(QUANTITY) != null ? Integer.parseInt(deepLink.getQueryParameter(QUANTITY)) : 1, deepLink.getQueryParameter(DISCOUNT_PERCENT) != null ? Double.parseDouble(deepLink.getQueryParameter(DISCOUNT_PERCENT)) : 0.0));
                    jwtTokenForDeeplink = deepLink.getQueryParameter(AUTH_TOKEN_FOR_DEEPLINK);
                    discountCalculationRequest = new DiscountCalculationRequest(productsList, new Discount("PRODUCT_DISCOUNT_TYPE", null));
                    productUUID = deepLink.getQueryParameter(PRODUCT_ID);
                    quantityFromDeeplink = deepLink.getQueryParameter(QUANTITY) != null ? Integer.parseInt(deepLink.getQueryParameter(QUANTITY)) : 1;
                } catch (Exception e) {
                    return;
                }
            } else if (deepLink.getPath().equals("/applink/search")) {
                productUUID = deepLink.getQueryParameter(PRODUCT_ID);
                fareId = deepLink.getQueryParameter(AppConstants.FARE_ID);
                searchFromDeeplink = true;
            }
            serviceLevel = deepLink.getQueryParameter(SERVICE_LEVEL);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            handleDeepLink();
        }
    }


    public void loadStoreFilters() {

        if (confHelper.isNewStoreScreen()) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, new NewStoreFiltersFragment())
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, new StoreFiltersFragment())
                    .commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

        /*for (int i = 0; i < textViewsProductAdded.size(); i++) {
            textViewsProductAdded.get(i).setTextColor(confHelper.getBackgroundThemePrimaryTextColor());
        }*/
        //circleBorderImageViewProductAdded.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        //linearLayoutProductAdded.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        // linearLayoutProductAdded.getBackground().setAlpha(215);

        /*if (isProductAddedToCart) {
            linearLayoutProductAdded.setVisibility(View.VISIBLE);
        } else {
            linearLayoutProductAdded.setVisibility(View.GONE);
        }
*/
        /*for (Button button : buttonCheckout) {
            button.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
            button.setTextColor(confHelper.getBackgroundThemeSecondaryTextColor());
        }*/

        invalidateOptionsMenu();
    }


   /* @Override
    public void onBackPressed() {
        if (!(linearLayoutProductAdded.getVisibility() == View.VISIBLE)) {
            super.onBackPressed();
        }
    }*/

    @Override
    protected void onPause() {
        super.onPause();
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_buy_tickets, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem cart = menu.findItem(R.id.action_cart);
        cart.setActionView(R.layout.menu_item_cart_layout);

        TextView textViewCartCount = cart.getActionView().findViewById(R.id.tv_cart_count);
        textViewCartCount.setBackgroundTintList(ColorStateList.valueOf(confHelper.getHeaderThemeAccentColor()));
        textViewCartCount.setTextColor(confHelper.getHeaderThemeSecondaryTextColor());
        textViewCartCount.setVisibility(View.GONE);

        ImageView imageViewCart = cart.getActionView().findViewById(R.id.iv_cart);
        this.imageViewCart = imageViewCart;
        imageViewCart.setImageDrawable(getResources().getDrawable(R.drawable.filled_cart_material));
        imageViewCart.setImageTintList(ColorStateList.valueOf(confHelper.getHeaderThemePrimaryTextColor()));
        imageViewCart.setContentDescription(getResources().getString(R.string.shopping_cart_voonly));

        String entityResultString = sharedPreferences.getString(AppConstants.ENTITY_RESULT_LIST, null);

        List<EntityResult> savedEntityResults = gson.fromJson(entityResultString, new TypeToken<List<EntityResult>>() {
        }.getType());

        String string;

        if (savedEntityResults != null) {

            int quantity = 0;

            for (int i = 0; i < savedEntityResults.size(); i++) {
                quantity += savedEntityResults.get(i).getQuantity();
            }

            if (quantity != 0) {
                String count = String.valueOf(quantity);

                textViewCartCount.setVisibility(View.VISIBLE);
                string = getResources().getQuantityString(R.plurals.shopping_cart_item, quantity, quantity);
                textViewCartCount.setContentDescription(string);
                textViewCartCount.setText(count);
            } else {
                textViewCartCount.setVisibility(View.GONE);
            }

        } else {
            textViewCartCount.setVisibility(View.GONE);
        }

        cart.getActionView().setOnClickListener(view -> {
            final Intent intent = new Intent(BuyTicketsActivity.this, NewShoppingCartActivity.class);
            intent.putExtra(AcceptPaymentActivity.SOURCE, HEADER_BUTTON);
            intent.putExtra(AppConstants.RELOAD, isIntentForReload);
            intent.putExtra("fareMediumId", fareMediaUuid);
            startActivity(intent);
        });

        return true;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (checkForSavedEntityResultsAltered(sharedPreferences)) {
            invalidateOptionsMenu();
            isProductAddedToCart = true;
           /* linearLayoutProductAdded.setVisibility(View.VISIBLE);
            textViewProductAdded.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
            linearLayoutProductAdded.announceForAccessibility(getString(R.string.product_added_to_cart));*/
        } else
            Timber.d("No product has been added to sharedPrefs.");
    }

    /**
     * This is not required now as updateSharedPrefs was moved from onDestroy to onPause. Needs more
     * testing before completely removing this feature.
     *
     * @param sharedPreferences The {@link SharedPreferences} that received the change.
     * @return
     */
    private boolean checkForSavedEntityResultsAltered(SharedPreferences sharedPreferences) {
        String entityResultString = sharedPreferences.getString(AppConstants.ENTITY_RESULT_LIST, null);
        List<EntityResult> savedEntityResult;

        if (entityResultString != null) {
            savedEntityResult = gson.fromJson(entityResultString, new TypeToken<List<EntityResult>>() {
            }.getType());

            return savedEntityResult != null && !savedEntityResult.isEmpty();
        } else
            return false;
    }

    /*@OnClick(R.id.btn_continue_shopping)
    public void onContinue() {
        isProductAddedToCart = false;
        try {
            BuyTicketsActivityFragment fragment = (BuyTicketsActivityFragment)
                    getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
            if (fragment != null)
                fragment.invalidateProductList();
        } catch (Exception e) {
            Timber.e("onCheckout: %s", e.getLocalizedMessage());
        }
        analyticsPlatformAdapter.storeAddProductOverlayPressed(CONTINUE_SHOPPING);
    }*/

    public void onCheckout(PaymentMethod paymentMethods, String origin) {
        isProductAddedToCart = false;
        final Intent intent = new Intent(this, AcceptPaymentActivity.class);
        intent.putExtra(AcceptPaymentActivity.SOURCE, ADD_TO_CART_OVERLAY);
        intent.putExtra(AppConstants.EXTRA_ORIGIN, origin);
        intent.putExtra(AppConstants.RELOAD, isIntentForReload);
        intent.putExtra("fareMediumId", fareMediaUuid);
        intent.putExtra("paymentMethod", paymentMethods);
        if (jwtTokenForDeeplink != null) {
            intent.putExtra(AppConstants.DEEPLINK_DISCOUNT, discountCalculationRequest);
            intent.putExtra(AppConstants.DEEPLINK_JWT_TOKEN, jwtTokenForDeeplink);
        }
        startActivity(intent);
        // if this method gets called because of repurchase action then we need to
        // finish this activity before showing payment screen else on back press
        // in the payment screen will again take you back to the payment screen since
        // productUUIDOfRepurchasePass will not be empty.
        if (!TextUtils.isEmpty(productUUIDOfRepurchasePass)) {
            finish();
        }
        jwtTokenForDeeplink = null;
        discountCalculationRequest = null;
        //analyticsPlatformAdapter.storeAddProductOverlayPressed(START_CHECKOUT);
    }

    protected void startAddToCartAnimation(View animatingView) {
        new AddToCartAnimationUtil()
                .attachActivity(this)
                .setTargetView(animatingView)
                .setMoveDuration(1000)
                .setDestView(imageViewAnimationDestination)
                .startAnimation();
    }
}