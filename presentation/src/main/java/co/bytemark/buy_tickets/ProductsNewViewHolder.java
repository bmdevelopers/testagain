package co.bytemark.buy_tickets;

import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;

class ProductNewViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ll_single_product)
    LinearLayout linearLayoutProduct;

    @BindView(R.id.cv_product_card)
    CardView cardViewProductCard;

    @BindView(R.id.tv_product_amount)
    TextView textViewProductAmount;

    @BindView(R.id.tv_product_name)
    TextView textViewProductName;

    @BindView(R.id.tv_product_short_description)
    TextView textViewProductShortDescription;

    @BindView(R.id.btn_product_add_to_cart)
    Button buttonProductAddToCart;

    @BindView(R.id.textViewLongDescription)
    TextView textViewLongDescription;

    @BindView(R.id.imageViewShowLongDesc)
    ImageView imageViewShowLongDesc;

    @BindView(R.id.buttonPlus)
    ImageButton buttonPlus;

    @BindView(R.id.buttonMinus)
    ImageButton buttonMinus;

    @BindView(R.id.textViewQuantity)
    TextView textViewQuantity;

    @BindView(R.id.buttonGooglePay)
    RelativeLayout relativeLayoutGooglePay;

    @BindView(R.id.buttonBuyNow)
    Button buttonBuyNow;

    @BindView(R.id.iv_product_logo)
    ImageView imageViewProductLogo;

    @BindView(R.id.ll_buy_button_layout)
    LinearLayout linearLayoutBuyButton;

    @BindView(R.id.rl_delete_button_layout)
    ConstraintLayout relativeLayoutDeleteButton;

    @BindView(R.id.btn_delete)
    Button buttonDelete;

    @BindView(R.id.tv_product_origin_destination)
    TextView textViewProductOriginDestination;

    @BindView(R.id.subscribe)
    SwitchCompat subscribe;

    @BindView(R.id.subscribe_text)
    TextView subscribeText;

    @BindView(R.id.licenseNo)
    TextInputEditText licenseNoET;

    @BindView(R.id.licenseNoInputLayout)
    TextInputLayout licenseNoInputLayout;

    @BindView(R.id.spotNo)
    TextInputEditText spotNoET;

    @BindView(R.id.spotNoInputLayout)
    TextInputLayout spotNoInputLayout;


    @BindView(R.id.tv_licence_spot_no)
    TextView licence_spot_no;


    int refPosition;

    ProductNewViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
