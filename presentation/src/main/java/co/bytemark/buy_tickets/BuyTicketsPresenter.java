package co.bytemark.buy_tickets;

import android.os.Parcelable;

import com.google.android.gms.common.api.ApiException;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.domain.interactor.product.GetAcceptedPaymentMethodsUseCase;
import co.bytemark.domain.interactor.product.GetAcceptedPaymentMethodsUseCaseValue;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.mvp.MvpBasePresenter;
import co.bytemark.sdk.BMError;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.PostEntityFilter;
import co.bytemark.sdk.model.common.Data;
import co.bytemark.sdk.model.product_search.entity.EntityResult;
import co.bytemark.sdk.network_impl.BMNetwork;
import co.bytemark.sdk.network_impl.BaseNetworkRequestCallback;
import co.bytemark.sdk.post_body.AppliedFilter;
import co.bytemark.sdk.post_body.PostSearch;
import co.bytemark.sdk.post_body.Search;
import co.bytemark.sdk.post_body.Sort;
import co.bytemark.shopping_cart.GooglePayUtil;
import rx.SingleSubscriber;

@SuppressWarnings({"ConstantConditions", "unchecked"})
public class BuyTicketsPresenter extends MvpBasePresenter<BuyTicketsView> {

    @Inject
    BMNetwork bmNetwork;

    @Inject
    GooglePayUtil googlePayUtil;

    @Inject
    ConfHelper confHelper;

    @Inject
    GetAcceptedPaymentMethodsUseCase getAcceptedPaymentMethodsUseCase;

    PostEntityFilter postEntityFilter;

    private List<String> acceptedPaymentMethods;

    private boolean googlePayReady;

    BuyTicketsPresenter() {
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    void loadProductList(List<? extends Parcelable> appliedFilterList, String fareMediaId, String productUUID) {
        if (isViewAttached())
            getView().showLoading();

        Sort sort = new Sort("list_priority", "asc");
        List<Sort> sortList = new ArrayList<>();
        sortList.add(sort);

        PostSearch postSearch = new PostSearch();
        postSearch.set_return("ENTITY");

        postSearch.setAppliedFilters((ArrayList<AppliedFilter>) appliedFilterList);
        postSearch.setSort(sortList);
        postSearch.setFareMediaId(fareMediaId);
        if (productUUID != null) {
            List<String> uuidList = new ArrayList<>();
            uuidList.add(productUUID);
            Search search = new Search(uuidList);
            List<Search> searchList = new ArrayList<>();
            searchList.add(search);
            postSearch.setSearch(searchList);

        }

        boolean forFareId = false;
        if (appliedFilterList.size() > 0 && ((AppliedFilter) appliedFilterList.get(0)).getKey() != null
                && ((AppliedFilter) appliedFilterList.get(0)).getKey().equals(AppConstants.GTFS_FARE_ID)) {
            forFareId = true;
        }
        postEntity(postSearch, forFareId);
    }

    private void postEntity(PostSearch postSearch, boolean forFareId) {
        cancelRequest();
        postEntityFilter = bmNetwork.postEntityFilter(postSearch, new BaseNetworkRequestCallback() {
            @Override
            public void onNetworkRequestSuccessfullyCompleted() {
            }

            @Override
            public <T> void onNetworkRequestSuccessWithResponse(ArrayList<T> listOf) {
               /* if (listOf == null || listOf.isEmpty()) {
                    if (isViewAttached()) {
                        getView().showNoAvailableProductsList();
                    }
                } else {
                    ArrayList<EntityResult> entityResults = (ArrayList<EntityResult>) listOf;
                    if (isViewAttached()) {
                        getView().hideLoading();
                        getView().hideDeviceTimeErrorView();
                        getView().setEntityResult(entityResults);
                    }
                }*/
            }

            @Override
            public void onNetworkRequestSuccessWithResponse(Object object) {
                Data data = (Data) object;
                ArrayList<EntityResult> entityResults = (ArrayList<EntityResult>) data.entityResults;
                ArrayList<AppliedFilter> appliedFilters = (ArrayList<AppliedFilter>) data.appliedFilters;


                if (entityResults == null || entityResults.isEmpty()) {
                    if (isViewAttached()) {
                        if (forFareId) {
                            getView().showFilterScreen();
                        } else {
                            getView().showNoAvailableProductsList();
                        }
                    }
                } else {
                    if (isViewAttached()) {
                        if (appliedFilters != null && appliedFilters.size() > 0) {
                            entityResults = addFiltersToEntity(entityResults, appliedFilters);
                        }
                        getView().hideLoading();
                        getView().hideDeviceTimeErrorView();
                        getView().setEntityResult(entityResults);
                    }
                }
            }

            @Override
            public void onNetworkRequestSuccessWithUnexpectedError() {
                if (isViewAttached()) {
                    getView().hideLoading();
                }
            }

            @Override
            public void onNetworkRequestSuccessWithError(BMError errors) {
                if (isViewAttached()) {
                    getView().hideLoading();
                }
                switch (errors.getCode()) {
                    case BytemarkSDK.ResponseCode.SDK_VERSION_OUT_OF_DATE:
                        if (isViewAttached()) {
                            getView().showAppUpdateDialog();
                        }
                        break;
                }
            }

            @Override
            public void onNetworkRequestSuccessWithDeviceTimeError() {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showDeviceTimeErrorView();
                }
            }
        });
    }

    private ArrayList<EntityResult> addFiltersToEntity(ArrayList<EntityResult> entityResults, ArrayList<AppliedFilter> appliedFilters) {

        for (EntityResult entityResult : entityResults) {
            entityResult.setAppliedFilters(appliedFilters);
        }

        return entityResults;
    }

    private void cancelRequest() {
        if (postEntityFilter != null)
            postEntityFilter.unSubscribe();
    }

    public void unSubscribe() {
        cancelRequest();
    }

    public void checkGooglePayReady() {
        googlePayUtil.getGooglePayReadyTask().addOnCompleteListener(
                task1 -> {
                    try {
                        boolean result = task1.getResult(ApiException.class);
                        if (isViewAttached()) {
                            googlePayReady = result;
                            getView().showGooglePayReady(result);
                        }
                    } catch (ApiException exception) {
                    }
                });
    }

    public void getAcceptedPaymentMethods() {
        if (BytemarkSDK.isLoggedIn() && (confHelper.getChildOrganizations() == null || confHelper.getChildOrganizations().isEmpty())) {
            GetAcceptedPaymentMethodsUseCaseValue getAcceptedPaymentMethodsUseCaseValue = new GetAcceptedPaymentMethodsUseCaseValue(confHelper.getOrganization().getUuid());
            getAcceptedPaymentMethodsUseCase.singleExecute(getAcceptedPaymentMethodsUseCaseValue,
                    new SingleSubscriber<List<String>>() {
                        @Override
                        public void onSuccess(List<String> strings) {
                            acceptedPaymentMethods = strings;
                            callLoadProductsFromView();
                        }

                        @Override
                        public void onError(Throwable error) {
                            callLoadProductsFromView();
                        }
                    });
        } else {
            callLoadProductsFromView();
        }

    }

    public boolean isGooglePayAccepted() {
        if (acceptedPaymentMethods == null) {
            return false;
        }
        for (String acceptedPaymentMethod : acceptedPaymentMethods) {
            if (acceptedPaymentMethod.equalsIgnoreCase("googlepay")) {
                return true;
            }
        }
        return false;
    }

    public boolean isGooglePayReady() {
        return googlePayReady;
    }

    private void callLoadProductsFromView() {
        if (isViewAttached()) {
            getView().callLoadProducts();
        }
    }
}
