package co.bytemark.buy_tickets.filters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.buy_tickets.filters.viewholders.ValuesViewHolder
import co.bytemark.domain.model.store.filter.Value

class SecondaryFilterAdapter(
		private val filterValuesList: List<Value>,
		private val filterValueClickListener: (Value) -> Unit
) : RecyclerView.Adapter<ValuesViewHolder>() {

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
			ValuesViewHolder(
					LayoutInflater.from(parent.context)
							.inflate(R.layout.new_filters_adapter_item, parent, false),
					filterValueClickListener
			)

	override fun getItemCount(): Int {
		return filterValuesList.size
	}

	override fun onBindViewHolder(holder: ValuesViewHolder,
								  position: Int) {
		holder.bind(filterValuesList[position])
	}

}