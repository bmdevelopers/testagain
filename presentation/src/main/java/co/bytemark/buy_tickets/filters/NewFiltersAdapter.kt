package co.bytemark.buy_tickets.filters


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.util.Pair
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.buy_tickets.filters.viewholders.*
import co.bytemark.domain.model.store.filter.FilterResult
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.post_body.AppliedFilter


/**
 * Created by vally on 05/12/19.
 */
class NewFiltersAdapter(private val confHelper: ConfHelper,
						private val filterFilterResultList: MutableList<FilterResult>,
						private val filterClickListener: (FilterResult, AppliedFilter, Boolean) -> Unit,
						private val clearFilterListener: () -> Unit
) : RecyclerView.Adapter<BaseFilterViewHolder>() {


	private val pairs = java.util.ArrayList<Pair<String, String>>()

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseFilterViewHolder {
		return when (viewType) {
			listView -> FiltersResultListViewHolder(
					inflateView(parent, R.layout.fragment_new_store_filter),
					filterClickListener)
			appliedFilter -> AppliedFilterViewHolder(
					inflateView(parent, R.layout.appliedfilter_row_item),
					clearFilterListener)
			dropDownView -> FiltersResultDropDownViewHolder(
					inflateView(parent, R.layout.store_filter_drop_down),
					pairs,
					confHelper,
					filterClickListener
			)
			else -> ChipsViewHolder(
					inflateView(parent, R.layout.store_filter_chip_item),
					confHelper,
					filterClickListener)
		}

	}

	private fun inflateView(parent: ViewGroup, @LayoutRes layoutId: Int) = LayoutInflater.from(parent.context)
			.inflate(layoutId, parent, false)

	override fun onBindViewHolder(holder: BaseFilterViewHolder, position: Int) {
		holder.bind(filterFilterResultList[position])
	}

	override fun getItemCount(): Int {
		return filterFilterResultList.size
	}

	override fun getItemViewType(position: Int): Int {
		return if (filterFilterResultList[position].type == FilterResult.CATEGORY) {

			if (filterFilterResultList[position].filterType == FilterResult.LIST) {

				if (filterFilterResultList[position].appliedFilters != null && filterFilterResultList.size > 1) {
					appliedFilter
				} else {
					listView
				}
			} else if (filterFilterResultList[position].filterType == FilterResult.DROPDOWN) {
				dropDownView
			} else {
				chips
			}
		} else if (filterFilterResultList[position].filterType == FilterResult.DROPDOWN) {
			dropDownView
		} else {
			chips
		}
	}

	companion object {
		const val listView = 0
		const val dropDownView = 1
		const val appliedFilter = 2
		const val chips = 3
	}
}