package co.bytemark.buy_tickets.filters


import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.buy_tickets.BuyTicketsActivity
import co.bytemark.buy_tickets.BuyTicketsActivityFragment
import co.bytemark.domain.model.store.filter.FilterResult
import co.bytemark.domain.model.store.filter.FilterResult.Companion.CATEGORY
import co.bytemark.domain.model.store.filter.Value
import co.bytemark.helpers.AppConstants.CATEGORY_NAME
import co.bytemark.helpers.AppConstants.GTFS_DESTINATION
import co.bytemark.helpers.AppConstants.GTFS_ORIGIN
import co.bytemark.sdk.post_body.AppliedFilter
import co.bytemark.widgets.util.createViewModel
import co.bytemark.widgets.util.postDelay
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.appliedfilter_row_item.*
import kotlinx.android.synthetic.main.fragment_new_store_filter.*
import kotlinx.android.synthetic.main.second_level_filter_fragment.*
import java.util.*


/**
 * Created by vally on 05/12/19.
 */
class NewStoreFiltersFragment : BaseMvvmFragment() {

	private val BACK_STACK_NAME = "FILTER"

	private lateinit var viewModel: StoreFiltersViewModel

	private lateinit var newFiltersAdapter: NewFiltersAdapter

	val builder = StringBuilder()

	private var appliedFilters: MutableList<AppliedFilter> = ArrayList()

	private var filterResultList: MutableList<co.bytemark.domain.model.store.filter.FilterResult> = ArrayList()

	private var fareMediaId: String? = null
	private var gtfsOrigin: String? = null
	private var gtfsDestination: String? = null
	private var gtfsCategory: String? = null

	override fun onInject() = CustomerMobileApp.component.inject(this)

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
			inflater.inflate(R.layout.second_level_filter_fragment, container, false)


	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		retainInstance = true
		setHasOptionsMenu(true)
		activity?.intent?.let {
			fareMediaId = it.getStringExtra("fareMediumId")
			gtfsOrigin = it.getStringExtra(GTFS_ORIGIN)
			gtfsDestination = it.getStringExtra(GTFS_DESTINATION)
			gtfsCategory = it.getStringExtra(CATEGORY_NAME)
		}

		viewModel = createViewModel { CustomerMobileApp.appComponent.storeFiltersViewModel }
	}

	override fun onOnline() {
		super.onOnline()
		if (gtfsOrigin != null && gtfsDestination != null) {
			appliedFilters.add(AppliedFilter(GTFS_ORIGIN, gtfsOrigin, "", "", "", "", "", ""))
			appliedFilters.add(AppliedFilter(GTFS_DESTINATION, gtfsDestination, "", "", "", "", "", ""))
			navigateToProductsFragmentWithoutBackStack()
		} else if (arguments?.getParcelable<AppliedFilter>("appliedFilters") != null) {
			val applied = arguments?.getParcelable<AppliedFilter>("appliedFilters")
			val result = arguments?.getParcelable<FilterResult>("result")

			tvFilterName.text = getValue(result, applied)?.value

			tvShortDescription.text =
					if (!TextUtils.isEmpty(getValue(result, applied)?.shortDescription))
						getValue(result, applied)?.shortDescription
					else
						""

			if (TextUtils.isEmpty(getValue(result, applied)?.imageUrl)) {
				filterLogo.visibility = View.INVISIBLE
			} else {
				Glide.with(context)
						.load(getValue(result, applied)?.imageUrl)
						.animate(android.R.anim.fade_in)
						.into(filterLogo)
				filterLogo.visibility = View.VISIBLE
			}
			filterLayout.visibility = View.VISIBLE

			postDelay(200) {
				if (applied != null) {
					result?.let {
						filterClickListener(it, applied)
					}
				}
			}

			close.setOnClickListener {
				this.fragmentManager?.popBackStack()
			}
		} else {
			emptyStateLayout.visibility = View.INVISIBLE
			postDelay(200) {
				filterLayout.visibility = View.GONE
				loadFilters()
			}
		}
	}

	override fun onOffline() {
		super.onOffline()
		connectionErrorDialog()
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		registerObservers()
	}

	private fun getValue(filterResult: co.bytemark.domain.model.store.filter.FilterResult?, applied: AppliedFilter?): Value? {
		return filterResult?.values?.firstOrNull { it.uuid == applied!!.filterValue }
	}

	private fun registerObservers() {
		viewModel.displayStatus.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
			when (it) {
				StoreFiltersViewModel.DisplayState.FETCHING_FILTERS -> {
					emptyStateLayout.showLoading(R.drawable.box_material, R.string.loading)
					emptyStateLayout.visibility = View.VISIBLE
				}
				else -> {
					emptyStateLayout.showContent()
				}
			}
		})

		viewModel.errorLiveData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
			handleError(it)
		})

		viewModel.filtersLiveData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
			it?.let {
				setFilterResult(it)
			}
		})
	}

	fun animateRoot() {

	}

	private fun setFilterResult(list: List<co.bytemark.domain.model.store.filter.FilterResult>) {
		filterResultList = list.toMutableList().apply {
			if (arguments?.getParcelable<AppliedFilter>("appliedFilters") != null) {
				removeAt(0)
			}
		}

		if (!isUUIDEmpty()
				|| !isPassRepurchaseUUIDEmpty()
				|| filterResultList.isEmpty()
		) {
			navigateToProductsFragmentWithoutBackStack()
		} else {

			if (filterResultList.any { it.appliedFilters == null }.not()) {
				navigateToProductsFragmentWithBackStack()
			}

			newFiltersAdapter = NewFiltersAdapter(
					confHelper,
					filterResultList,
					::onFilterSelected,
					::clearFilterListener
			)
			recyclerView.adapter = newFiltersAdapter
			emptyStateLayout.showContent()
		}
	}

	private fun onFilterSelected(filterResult: co.bytemark.domain.model.store.filter.FilterResult,
								 appliedFilter: AppliedFilter,
								 hasNext: Boolean) {
		if (!hasNext) {
			filterClickListener(filterResult, appliedFilter)
		} else {
			fragmentManager?.beginTransaction()?.hide(this)
					?.setCustomAnimations(R.anim.zoom_in, R.anim.zoom_in, R.anim.zoom_out, R.anim.zoom_out)
					?.addToBackStack(BACK_STACK_NAME)
					?.replace(R.id.fragmentContainer, newInstance(appliedFilter, filterResult))
					?.commit()
		}
	}


	private fun clearFilterListener() {
		appliedFilters.clear()
		loadFilters()
	}

	private fun loadFilters() {
		if (!isOnline()) {
			showError()
			return
		}
		viewModel.loadFilters(appliedFilters, fareMediaId)
	}


	private fun navigateToProductsFragmentWithoutBackStack() {

		val fragment: BuyTicketsActivityFragment = BuyTicketsActivityFragment
				.newInstance(this.appliedFilters, builder.toString())

		fragmentManager
				?.beginTransaction()
				?.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
				?.hide(this)
				?.add(R.id.fragmentContainer, fragment)
				?.disallowAddToBackStack()?.commit()
	}


	private fun filterClickListener(filterResult: co.bytemark.domain.model.store.filter.FilterResult, appliedFilter: AppliedFilter) {
		setAppliedFilter(filterResult, appliedFilter)
		loadFilters()
	}


	private fun setAppliedFilter(filterResult: co.bytemark.domain.model.store.filter.FilterResult, filter: AppliedFilter) {
		var isAppliedFilterAdded = false

		appliedFilters.filter {
			it.filterUuid == filter.filterUuid
		}.forEach {
			isAppliedFilterAdded = true
			it.filterValue = filter.filterValue
			it.value = filter.value
			it.uuid = filter.uuid
		}

		if (!isAppliedFilterAdded) {
			this.appliedFilters.add(filter)
		}

		if (filterResult.type.equals(CATEGORY, ignoreCase = true)
				&& filterResult.appliedFilters != null && filterResult.appliedFilters?.isNotEmpty()!!) {
			for (i in this.appliedFilters.indices) {
				if (this.appliedFilters[i].filterUuid == filterResult.uuid) {
					this.appliedFilters.subList(i + 1, this.appliedFilters.size).clear()
					return
				}
			}
		}
	}

	override fun showDefaultErrorDialog(title: String?, errorMsg: String?, finishActivity: Boolean) {
		emptyStateLayout.showError(R.drawable.error_material, title, errorMsg, "") {
			if (finishActivity)
				activity?.finish()
		}

	}


	private fun navigateToProductsFragmentWithBackStack() {
		buildFilterValues()
		val fragment = BuyTicketsActivityFragment
				.newInstance(this.appliedFilters, builder.toString())
		fragmentManager
				?.beginTransaction()
				?.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
				?.hide(this)
				?.add(R.id.fragmentContainer, fragment)
				?.addToBackStack(BACK_STACK_NAME)
				?.commit()

	}

	private fun buildFilterValues() {
		builder.setLength(0)
		for (result in filterResultList) {
			result.appliedFilters?.let {
				for (appliedFilter in it) {
					if (!TextUtils.isEmpty(appliedFilter.value)) {
						val value = appliedFilter.value
						if (builder.isEmpty()) {
							builder.append(value)
						} else {
							builder.append(" > ")
							builder.append(value)
						}
					}
				}
			}
		}
	}

	private fun showError() {
		emptyStateLayout.showError(R.drawable.error_material, R.string.network_connectivity_error, R.string.network_connectivity_error_message)

	}

	private fun isUUIDEmpty(): Boolean {
		return TextUtils.isEmpty((activity as BuyTicketsActivity).productUUID) && TextUtils.isEmpty((activity as BuyTicketsActivity).fareId)
	}

	private fun isPassRepurchaseUUIDEmpty(): Boolean {
		return TextUtils.isEmpty((activity as BuyTicketsActivity).productUUIDOfRepurchasePass)
	}

	companion object {

		fun newInstance(appliedFilters: AppliedFilter?, filterResult: co.bytemark.domain.model.store.filter.FilterResult): NewStoreFiltersFragment {
			return NewStoreFiltersFragment().apply {
				arguments = Bundle().apply {
					putParcelable("appliedFilters", appliedFilters)
					putParcelable("result", filterResult)
				}
			}
		}
	}


}