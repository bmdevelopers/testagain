package co.bytemark.buy_tickets.filters.viewholders

import android.text.TextUtils
import android.view.View
import co.bytemark.domain.model.store.filter.FilterResult
import co.bytemark.domain.model.store.filter.Value
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.new_filters_adapter_item.view.*

class AppliedFilterViewHolder(
		itemView: View,
		private val clearFilterListener: () -> Unit
) : BaseFilterViewHolder(itemView) {


	override fun bind(filterResult: FilterResult) {
		itemView.apply {
			tvFilterName.text = filterResult.appliedFilters?.first()?.value ?: ""

			if (TextUtils.isEmpty(getValue(filterResult)?.shortDescription)) {
				tvShortDescription.visibility = View.INVISIBLE
			} else {
				tvShortDescription.text = getValue(filterResult)?.shortDescription
				tvShortDescription.visibility = View.VISIBLE
			}

			if (TextUtils.isEmpty(getValue(filterResult)?.imageUrl)) {
				filterLogo.visibility = View.INVISIBLE
			} else {
				Glide.with(context)
						.load(getValue(filterResult)?.imageUrl)
						.animate(android.R.anim.fade_in)
						.into(filterLogo)
				filterLogo.visibility = View.VISIBLE
			}
			itemView.setOnClickListener {
				clearFilterListener()
			}
		}
	}

	private fun getValue(filterResult: FilterResult): Value? {
		filterResult.values?.let {
			for (value in it) {
				if (value.uuid == filterResult.appliedFilters?.first()?.uuid) {
					return value
				}
			}
		}
		return null
	}
}