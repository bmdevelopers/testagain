package co.bytemark.buy_tickets.filters

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.bytemark.domain.interactor.newStoreFilters.StoreFilterUseCase
import co.bytemark.domain.interactor.newStoreFilters.StoreFilterUseCaseValue
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import co.bytemark.sdk.post_body.AppliedFilter
import co.bytemark.sdk.post_body.PostSearch
import co.bytemark.sdk.post_body.Sort
import kotlinx.coroutines.launch
import javax.inject.Inject

class StoreFiltersViewModel @Inject constructor(
        private val storeFilterUseCase: StoreFilterUseCase
) : ViewModel() {

    val filtersLiveData by lazy {
        MutableLiveData<List<co.bytemark.domain.model.store.filter.FilterResult>>()
    }

    val displayStatus by lazy {
        MutableLiveData<DisplayState>()
    }

    val errorLiveData by lazy {
        MutableLiveData<BMError>()
    }


    fun loadFilters(appliedFilters: List<AppliedFilter>, fareMediaId: String?) = viewModelScope.launch {
        displayStatus.postValue(DisplayState.FETCHING_FILTERS)
        val searchReq = prepareRequest(appliedFilters, fareMediaId)
        when (val result = storeFilterUseCase.invoke(StoreFilterUseCaseValue(searchReq))) {
            is Result.Success -> {
                result.data?.let {
                    filtersLiveData.postValue(it.filterResults)
                }
            }
            is Result.Failure -> {
                errorLiveData.postValue(result.bmError.first())
            }
        }
        displayStatus.postValue(DisplayState.NONE)
    }

    private fun prepareRequest(appliedFilters: List<AppliedFilter>, fareMediaId: String?): PostSearch {
        val sortList = listOf(Sort("list_priority", "asc"))
        return PostSearch("FILTER", appliedFilters, sortList, fareMediaId)
    }


    enum class DisplayState {
        FETCHING_FILTERS,
        NONE
    }

}