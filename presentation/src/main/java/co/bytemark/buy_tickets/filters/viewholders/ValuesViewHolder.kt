package co.bytemark.buy_tickets.filters.viewholders

import android.annotation.SuppressLint
import android.text.TextUtils
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.domain.model.store.filter.Value
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.new_filters_adapter_item.view.*

class ValuesViewHolder(
        itemView: View,
        val clickListener: (Value) -> Unit
) : RecyclerView.ViewHolder(itemView) {

    @SuppressLint("NewApi")
    fun bind(value: Value) {
        itemView.apply {
            if (value.shortDescription.isNullOrEmpty()) {
                tvShortDescription.visibility = View.GONE
            } else {
                tvShortDescription.text = value.shortDescription
                tvShortDescription.visibility = View.VISIBLE
            }
            if (value.value.isNullOrEmpty()) {
                tvFilterName.visibility = View.GONE
            } else {
                tvFilterName.visibility = View.VISIBLE
                tvFilterName.text = value.value
            }
            if (value.imageUrl.isNullOrEmpty()) {
                filterLogo.visibility = View.GONE
                line1.visibility = View.GONE
            } else {
                line1.visibility = View.VISIBLE
                Glide.with(context)
                        .load(value.imageUrl)
                        .animate(android.R.anim.fade_in)
                        .into(filterLogo)
                filterLogo.visibility = View.VISIBLE
            }
            itemView.setOnClickListener {
                clickListener(value)
            }
            tvShortDescription.post(Runnable {
                if ((tvFilterName.lineCount + tvShortDescription.lineCount) > 2) {
                    showLL?.visibility = View.VISIBLE
                    tvFilterName.maxLines = 1
                    when {
                        tvShortDescription.lineCount == 0 -> {
                            tvFilterName.maxLines = 2
                        }
                        tvFilterName.lineCount == 0 -> {
                            tvShortDescription.maxLines = 2
                        }
                        else -> {
                            tvShortDescription.maxLines = 1
                        }
                    }
                } else {
                    showLL?.visibility = View.GONE
                }
            })
            showLL.setOnClickListener {
                if (tvShortDescription.maxLines <= 1 || tvFilterName.maxLines <= 1) {
                    tvShortDescription.isSingleLine = false
                    imageViewShow.rotation = 180f
                    tvFilterName.isSingleLine = false
                } else {
                    tvShortDescription.isSingleLine = true
                    imageViewShow.rotation = 0f
                    tvFilterName.isSingleLine = true
                }
            }

        }
    }
}