package co.bytemark.buy_tickets.filters.viewholders

import android.content.res.ColorStateList
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.util.Pair
import co.bytemark.R
import co.bytemark.domain.model.store.filter.FilterResult
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.post_body.AppliedFilter
import kotlinx.android.synthetic.main.simple_spinner_dropdown_item.view.*
import kotlinx.android.synthetic.main.simple_spinner_item.view.*
import kotlinx.android.synthetic.main.store_filter_item.view.*
import timber.log.Timber

class FiltersResultDropDownViewHolder(
		itemView: View,
		private val pairs: ArrayList<Pair<String, String>>,
		private val confHelper: ConfHelper,
		private val filterClickListener: (FilterResult, AppliedFilter, Boolean) -> Unit
) : BaseFilterViewHolder(itemView) {
	override fun bind(filterResult: FilterResult) {
		itemView.apply {

			title.text = filterResult.displayName?.toUpperCase() ?: ""
			title.setTextColor(confHelper.backgroundThemePrimaryTextColor)

			val values = filterResult.values ?: listOf()
			val displayNames = values.map { it.value ?: "" }.toMutableList()

			if (filterResult.placeholderText != null)
				displayNames.add(filterResult.placeholderText ?: "")
			else if (filterResult.displayName != null)
				displayNames.add(filterResult.displayName ?: "")

			val adapter = object : ArrayAdapter<String>(context, R.layout.simple_spinner_item, displayNames) {
				override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
					val view = super.getView(position, convertView, parent)
					tv_spinner?.setTextColor(confHelper.backgroundThemePrimaryTextColor)
					return view
				}

				override fun getDropDownView(position: Int, convertView: View?,
											 parent: ViewGroup): View {
					val view = super.getDropDownView(position, convertView, parent)
					ctv_spinner?.setTextColor(confHelper.backgroundThemePrimaryTextColor)
					ctv_spinner?.setBackgroundColor(confHelper.backgroundThemeBackgroundColor)
					return view
				}

				override fun getCount(): Int {
					return super.getCount() - 1
				}
			}
			adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
			// store_filter_item
			spinner.adapter = adapter
			spinner.backgroundTintList = ColorStateList.valueOf(confHelper.backgroundThemePrimaryTextColor)

			// Temporarily remove listener as setSelection might trigger onItemSelected.
			spinner.onItemSelectedListener = null
			spinner.setSelection(adapter.count, false)

			filterResult.values?.let { valuesList ->
				filterResult.appliedFilters?.let { appliedList ->
					if (appliedList.isNotEmpty()) {
						val appliedFilter = appliedList.first()
						for (i in valuesList.indices) {
							if (valuesList[i].uuid == appliedFilter.uuid) {
								spinner.setSelection(i, false)
							}
						}
					}
				}
			}

			// Restore selection listener for handling user events
			spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
				override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
					publishOnItemSelected(parent, position, filterResult)
				}

				override fun onNothingSelected(parent: AdapterView<*>) {
					Timber.tag("FilterDropDown").d("onNothingSelected: ")
				}
			}
			clear.visibility = View.GONE
		}
	}

	private fun publishOnItemSelected(parent: AdapterView<*>, position: Int, filterResult: FilterResult) {
		val displayName = parent.getItemAtPosition(position) as String
		filterResult.values?.forEach {
			if (it.value.equals(displayName, ignoreCase = true)) {
				val filterUuid = filterResult.uuid
				val filterValue = it.uuid

				if (!pairs.contains(Pair.create(filterUuid, filterValue))) {
					var isFilterUuid = false
					for (i in pairs.indices) {
						val pair = pairs.get(i)
						if (pair.first == filterUuid) {
							isFilterUuid = true
							pairs.remove(pair)
							pairs.add(i, Pair.create(filterUuid, filterValue))
							pairs.subList(i + 1, pairs.size).clear()
							break
						}
					}

					if (!isFilterUuid) {
						pairs.add(Pair.create(filterUuid, filterValue))
					}

					filterClickListener(filterResult, AppliedFilter("", "", filterResult.uuid, it.uuid, displayName, filterResult.type, filterResult.name, ""), false)
				}
			}
		}
	}
}