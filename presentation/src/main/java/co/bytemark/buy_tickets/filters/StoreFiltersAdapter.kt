package co.bytemark.buy_tickets.filters

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.CheckedTextView
import android.widget.TextView
import androidx.core.util.Pair
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.domain.model.store.filter.FilterResult
import co.bytemark.domain.model.store.filter.FilterResult.Companion.CATEGORY
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.post_body.AppliedFilter
import kotlinx.android.synthetic.main.store_filter_item.view.*
import java.util.*
import kotlin.collections.ArrayList

class StoreFiltersAdapter internal constructor(
        val context: Context,
        var confHelper: ConfHelper,
        private var analyticsAdapter: AnalyticsPlatformAdapter,
        private val selectionCallback: SelectionCallback
) : RecyclerView.Adapter<StoreFiltersAdapter.StoreFiltersViewHolder>() {


    private val filterResults: MutableList<FilterResult> = ArrayList()
    val appliedFilters: MutableList<AppliedFilter> = ArrayList()
    private val pairs: MutableList<Pair<String, String>> = ArrayList()

    private var viewHolderFocusPosition = 0


    inner class StoreFiltersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(filterResult: FilterResult) {
            with(itemView) {
                if (filterResult.displayName != null)
                    title.text = filterResult.displayName?.toUpperCase(Locale.ROOT)
                title.setTextColor(confHelper.backgroundThemePrimaryTextColor)

                val values = filterResult.values
                val displayNames: MutableList<String> =
                        values?.map { it.value ?: "" }?.toMutableList() ?: mutableListOf()

                filterResult.placeholderText?.also {
                    if (it.isNotEmpty())
                        displayNames.add(it)
                }

                val adapter: ArrayAdapter<String> = object : ArrayAdapter<String>(context, R.layout.simple_spinner_item, displayNames) {
                    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                        val view = super.getView(position, convertView, parent)
                        val textView = view.findViewById<TextView>(R.id.tv_spinner)
                        textView.setTextColor(confHelper.backgroundThemePrimaryTextColor)
                        return view
                    }

                    override fun getDropDownView(position: Int, convertView: View?,
                                                 parent: ViewGroup): View {
                        val view = super.getDropDownView(position, convertView, parent)
                        val textView = view.findViewById<CheckedTextView>(R.id.ctv_spinner)
                        textView.setTextColor(confHelper.backgroundThemePrimaryTextColor)
                        textView.setBackgroundColor(confHelper.backgroundThemeBackgroundColor)
                        return view
                    }

                    override fun getCount(): Int {
                        return values?.size ?: 0
                    }
                }
                adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
                spinner.adapter = adapter
                spinner.backgroundTintList = ColorStateList.valueOf(confHelper.backgroundThemePrimaryTextColor)

                // Temporarily remove listener as setSelection might trigger onItemSelected.
                spinner.onItemSelectedListener = null
                filterResult.appliedFilters?.let {
                    if (it.isNotEmpty()) {
                        val appliedFilter = it.first()
                        filterResult.values?.forEachIndexed { index, value ->
                            if (value.uuid == appliedFilter.uuid) {
                                spinner.setSelection(index, false)
                            }
                        }
                    } else {
                        spinner.setSelection(adapter.count, false)
                    }
                } ?: spinner.setSelection(adapter.count, false)

                // Restore selection listener for handling user events
                spinner.onItemSelectedListener = object : OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                        if (adapterPosition == viewHolderFocusPosition) {
                            container.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
                        }
                        publishOnItemSelected(parent, position, filterResult)
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                }
                clear.setOnClickListener { clearAppliedFilters(filterResult) }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreFiltersViewHolder {
        return StoreFiltersViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.store_filter_item, parent, false))
    }

    override fun onBindViewHolder(viewHolder: StoreFiltersViewHolder, position: Int) {
        viewHolder.bind(filterResults[position])
    }

    fun clearAppliedFilters(filterResult: FilterResult) {
        analyticsAdapter.clearFilter()
        val filterUuid = filterResult.uuid
        for (i in pairs.indices) {
            val pair = pairs[i]
            if (pair.first == filterUuid) {
                pairs.subList(i, pairs.size).clear()
                // As we are altering the list here, this break is necessary, otherwise IndexOutOfBound exception will be thrown
                break
            }
        }
        if (filterResult.type.equals(CATEGORY, ignoreCase = true)) {
            for (i in appliedFilters.indices) {
                if (appliedFilters[i].filterUuid == filterResult.uuid) {
                    appliedFilters.subList(i, appliedFilters.size).clear()
                    // As we are altering the list here, this break is necessary, otherwise IndexOutOfBound exception will be thrown
                    break
                }
            }
        } else {
            val iterator = appliedFilters.iterator()
            while (iterator.hasNext()) {
                val appliedFilter = iterator.next()
                if (appliedFilter.filterUuid == filterResult.uuid) {
                    iterator.remove()
                }
            }
        }
        selectionCallback.onFilterSelected(appliedFilters)
    }

    private fun publishOnItemSelected(parent: AdapterView<*>, position: Int, filterResult: FilterResult) {
        val displayName = parent.getItemAtPosition(position) as String
        filterResult.values?.forEach { value ->
            if (value.value.equals(displayName, ignoreCase = true)) {
                val filterUuid = filterResult.uuid
                val filterValue = value.uuid
                if (!pairs.contains(Pair.create(filterUuid, filterValue))) {
                    var isFilterUuid = false
                    for (i in pairs.indices) {
                        val pair = pairs[i]
                        if (pair.first == filterUuid) {
                            isFilterUuid = true
                            pairs.remove(pair)
                            pairs.add(i, Pair.create(filterUuid, filterValue))
                            pairs.subList(i + 1, pairs.size).clear()
                            break
                        }
                    }
                    if (!isFilterUuid) {
                        pairs.add(Pair.create(filterUuid, filterValue))
                    }
                    setAppliedFilter(filterResult, AppliedFilter("", "", filterResult.uuid, value.uuid, displayName, filterResult.type, filterResult.name,""))
                    analyticsAdapter.filterApplied(filterResult.displayName, filterResult.uuid, value.value)
                    selectionCallback.onFilterSelected(appliedFilters)
                }
            }
        }
    }

    fun setAppliedFilter(filterResult: FilterResult, filter: AppliedFilter) {
        var isAppliedFilterAdded = false
        for (i in appliedFilters.indices) {
            if (appliedFilters[i].filterUuid == filter.filterUuid) {
                isAppliedFilterAdded = true
                appliedFilters[i].filterValue = filter.filterValue
                appliedFilters[i].value = filter.value
                appliedFilters[i].uuid = filter.uuid
            }
        }
        if (!isAppliedFilterAdded) {
            appliedFilters.add(filter)
        }

        filterResult.appliedFilters?.let {
            if (filterResult.type.equals(CATEGORY, ignoreCase = true)
                    && it.isNotEmpty()) {
                for (i in appliedFilters.indices) {
                    if (appliedFilters[i].filterUuid == filterResult.uuid) {
                        appliedFilters.subList(i + 1, appliedFilters.size).clear()
                        break
                    }
                }
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return filterResults[position].uuid.hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return filterResults.size
    }

    fun setResults(filterResults: List<FilterResult>) {
        this.filterResults.clear()
        this.filterResults.addAll(filterResults)
        notifyDataSetChanged()
        setNextViewHolderForFocusPosition()
    }

    private fun setNextViewHolderForFocusPosition() {
        for (i in filterResults.indices) {
            val appliedFilters = filterResults[i].appliedFilters
            if (appliedFilters == null || appliedFilters.isEmpty()) {
                if (filterResults[i].type.equals(CATEGORY, ignoreCase = true)) {
                    viewHolderFocusPosition = i
                } else {
                    viewHolderFocusPosition += 1
                    break
                }
            }
        }
    }

    interface SelectionCallback {
        fun onFilterSelected(appliedFilters: MutableList<AppliedFilter>)
    }
}