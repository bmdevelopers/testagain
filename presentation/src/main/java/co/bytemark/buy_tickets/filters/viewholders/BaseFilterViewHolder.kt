package co.bytemark.buy_tickets.filters.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.domain.model.store.filter.FilterResult


abstract class BaseFilterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
	abstract fun bind(filterResult: FilterResult)
}

