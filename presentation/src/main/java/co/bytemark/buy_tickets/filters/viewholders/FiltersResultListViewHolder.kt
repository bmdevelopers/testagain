package co.bytemark.buy_tickets.filters.viewholders

import android.view.View
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import co.bytemark.buy_tickets.filters.SecondaryFilterAdapter
import co.bytemark.domain.model.store.filter.FilterResult
import co.bytemark.sdk.post_body.AppliedFilter
import kotlinx.android.synthetic.main.fragment_new_store_filter.view.*

class FiltersResultListViewHolder(
		itemView: View,
		private val filterClickListener: (FilterResult, AppliedFilter, Boolean) -> Unit
) : BaseFilterViewHolder(itemView) {
	override fun bind(filterResult: FilterResult) {
		itemView.apply {
			emptyStateLayout.showContent()
			filterResult.values?.let { valuesList ->
				val newFiltersAdapter = SecondaryFilterAdapter(valuesList) { value ->
					filterClickListener(filterResult,
							AppliedFilter("", "", filterResult.uuid, value.uuid, filterResult.displayName, filterResult.type, filterResult.name, ""),
							value.isHasNext)
				}
				val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)

				recyclerView?.layoutManager = layoutManager
				recyclerView.adapter = newFiltersAdapter
				newFiltersAdapter.notifyDataSetChanged()
			}
		}
	}
}