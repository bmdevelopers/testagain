package co.bytemark.buy_tickets.filters

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.CustomerMobileApp
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.buy_tickets.BuyTicketsActivity
import co.bytemark.buy_tickets.BuyTicketsActivityFragment
import co.bytemark.buy_tickets.filters.StoreFiltersAdapter.SelectionCallback
import co.bytemark.domain.model.store.filter.FilterResult
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.post_body.AppliedFilter
import co.bytemark.widgets.util.getViewModel
import kotlinx.android.synthetic.main.fragment_store_filter.*
import java.util.*
import javax.inject.Inject

class StoreFiltersFragment : BaseMvvmFragment(), SelectionCallback {

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    lateinit var viewModel: StoreFiltersViewModel

    private lateinit var adapter: StoreFiltersAdapter

    private val builder = StringBuilder()
    private var filterResults: List<FilterResult> = ArrayList()
    private var appliedFilters: MutableList<AppliedFilter> = ArrayList()
    private var fareMediaId: String? = null
    private var gtfsOrigin: String? = null
    private var gtfsDestination: String? = null
    private var gtfsCategory: String? = null
    private var serviceLevel: String? = null
    private var serviceLevelName: String? = null

    private val isUUIDEmpty: Boolean
        get() = TextUtils.isEmpty((activity as BuyTicketsActivity?)!!.productUUID) &&
                TextUtils.isEmpty((activity as BuyTicketsActivity?)!!.fareId)

    private val isPassRepurchaseUUIDEmpty: Boolean
        get() = TextUtils.isEmpty((activity as BuyTicketsActivity?)!!.productUUIDOfRepurchasePass)


    override fun onInject() {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        setHasOptionsMenu(true)
        activity?.intent?.let {
            fareMediaId = it.getStringExtra("fareMediumId")
            gtfsOrigin = it.getStringExtra(AppConstants.GTFS_ORIGIN)
            gtfsDestination = it.getStringExtra(AppConstants.GTFS_DESTINATION)
            gtfsCategory = it.getStringExtra(AppConstants.CATEGORY_NAME)
        }

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View =
            inflater.inflate(R.layout.fragment_store_filter, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = getViewModel(CustomerMobileApp.appComponent.storeFiltersViewModel)
        adapter = StoreFiltersAdapter(activity!!, confHelper, analyticsPlatformAdapter, this)
        recyclerView.adapter = adapter
        recyclerView.preserveFocusAfterLayout = true
        registerObservers()
    }

    private fun registerObservers() {

        viewModel.displayStatus.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            when (it) {
                StoreFiltersViewModel.DisplayState.FETCHING_FILTERS -> {
                    emptyStateLayout.showLoading(R.drawable.box_material, R.string.loading)
                }
                else -> {
                    emptyStateLayout.showContent()
                }
            }

        })

        viewModel.errorLiveData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            handleError(it)
        })

        viewModel.filtersLiveData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            it?.let {
                setFilterResult(it)
            }
        })

    }

	override fun onOnline() {
		if (gtfsOrigin != null && gtfsDestination != null) {
			appliedFilters.add(AppliedFilter(AppConstants.GTFS_ORIGIN, gtfsOrigin, "", "", "", "", "", ""))
			appliedFilters.add(AppliedFilter(AppConstants.GTFS_DESTINATION, gtfsDestination, "", "", "", "", "", ""))
			navigateToProductsFragmentWithoutBackStack()
		} else {
			viewModel.loadFilters(appliedFilters, fareMediaId)
		}
	}

    override fun onOffline() {
        (activity as BuyTicketsActivity?)!!.supportActionBar!!.show()
        showNetworkConnectionErrorView()
    }

    override fun onResume() {
        super.onResume()
        hideKeyboard()
    }


    override fun onFilterSelected(appliedFilters: MutableList<AppliedFilter>) {
        this.appliedFilters = appliedFilters
        viewModel.loadFilters(appliedFilters, fareMediaId)
    }

    override fun connectionErrorDialog(positiveTextId: Int, enableNegativeAction: Boolean, negativeTextId: Int, finishOnDismiss: Boolean, positiveAction: () -> Unit, negativeAction: () -> Unit) {
        emptyStateLayout.showError(R.drawable.error_material, R.string.network_connectivity_error, R.string.network_connectivity_error_message)
    }

    override fun showDefaultErrorDialog(title: String?, errorMsg: String?, finishActivity: Boolean) {
        emptyStateLayout.showError(R.drawable.error_material, title, errorMsg, "Ok") {
            if (finishActivity) {
                activity?.finish()
            }
        }
    }


    fun showNetworkConnectionErrorView() {
        if (!isPassRepurchaseUUIDEmpty) (activity as BuyTicketsActivity?)!!.supportActionBar!!.hide()
        emptyStateLayout.showError(R.drawable.error_material, R.string.network_connectivity_error, R.string.network_connectivity_error_message)
    }

    private fun setFilterResult(filterFilterResults: List<FilterResult>) {
        serviceLevel = (activity as BuyTicketsActivity).serviceLevel
        if (!isUUIDEmpty || !isPassRepurchaseUUIDEmpty) {
            navigateToProductsFragmentWithoutBackStack()
        } else {
            emptyStateLayout!!.showContent()
            if (filterFilterResults.isNotEmpty()) {
                filterResults = filterFilterResults
                adapter.setResults(filterFilterResults)
                var appliedFilters = true
                for (result in filterFilterResults) {
                    if (result.appliedFilters == null) {
                        appliedFilters = false
                    }
                }
                if (appliedFilters) {
                    navigateToProductsFragmentWithBackStack()
                } else
                    if (!serviceLevel.isNullOrEmpty() && !confHelper.getServiceLevelUUID(serviceLevel).isNullOrEmpty()) {
                        serviceLevel = confHelper.getServiceLevelUUID(serviceLevel)
                        findLevel(filterFilterResults)
                    }
            } else {
                filterResults = filterFilterResults
                navigateToProductsFragmentWithoutBackStack()
            }
            analyticsPlatformAdapter.filterLoadingCompleted(AnalyticsPlatformsContract.Status.SUCCESS, "")
        }
    }

    private fun findLevel(filterFilterResults: List<FilterResult>) {
        for (result in filterFilterResults) {
            for (valu in result.values!!) {
                if (valu.uuid == serviceLevel) {
                    adapter.setAppliedFilter( result,AppliedFilter("", "", result.uuid,
                            valu.uuid, serviceLevelName, result.type, result.name,""))
                }
            }
        }

        onFilterSelected(adapter.appliedFilters)
    }

    private fun buildFilterValues() {
        builder.setLength(0)
        for (result in filterResults) {
            result.appliedFilters?.let {
                for (appliedFilter in it) {
                    if (!TextUtils.isEmpty(appliedFilter.value)) {
                        val value = appliedFilter.value
                        if (builder.isEmpty()) {
                            builder.append(value)
                        } else {
                            builder.append(" > ")
                            builder.append(value)
                        }
                    }
                }
            }
        }
    }

    private fun navigateToProductsFragmentWithoutBackStack() {
        val fragment: BuyTicketsActivityFragment = if (gtfsCategory != null && gtfsCategory != "") {
            BuyTicketsActivityFragment
                    .newInstance(appliedFilters, gtfsCategory!!)
        } else {
            BuyTicketsActivityFragment
                    .newInstance(appliedFilters, builder.toString())
        }
        fragmentManager!!.beginTransaction()
                .hide(this)
                .add(R.id.fragmentContainer, fragment)
                .disallowAddToBackStack()
                .commit()
    }

    private fun navigateToProductsFragmentWithBackStack() {
        buildFilterValues()
        val fragment = BuyTicketsActivityFragment
                .newInstance(appliedFilters, builder.toString())
        fragmentManager!!.beginTransaction()
                .hide(this)
                .add(R.id.fragmentContainer, fragment)
                .addToBackStack(BACK_STACK_NAME)
                .commit()
    }

    companion object {
        private const val BACK_STACK_NAME = "FILTER"
    }
}