package co.bytemark.buy_tickets.filters.viewholders

import android.content.res.ColorStateList
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.forEachIndexed
import co.bytemark.R
import co.bytemark.domain.model.store.filter.FilterResult
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.post_body.AppliedFilter
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.store_filter_chip_item.view.*
import java.util.*

class ChipsViewHolder(
		itemView: View,
		val confHelper: ConfHelper,
		private val filterClickListener: (FilterResult, AppliedFilter, Boolean) -> Unit
) : BaseFilterViewHolder(itemView) {
	override fun bind(filterResult: FilterResult) {

		itemView.apply {
			chipGroupView.invalidate()
			chipGroupView.removeAllViews()
			chipGroupView.removeAllViewsInLayout()
			titleTv.text = filterResult.displayName?.toUpperCase(Locale.ROOT)
			titleTv.setTextColor(confHelper.backgroundThemePrimaryTextColor)

			filterResult.values?.forEach { value ->
				Chip(chipGroupView.context).apply {
					text = value.value
					isCheckable = true
					checkedIcon = null
					chipStrokeColor = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
					chipBackgroundColor = ColorStateList.valueOf(resources.getColor(R.color.white))
					setTextColor(ColorStateList.valueOf(confHelper.backgroundThemeAccentColor))
					chipStrokeWidth = 3F
					tag = value.uuid
					setOnClickListener {
						chipBackgroundColor = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
						setTextColor(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.white)))
						filterClickListener(filterResult,
								AppliedFilter("", "", filterResult.uuid, tag as String?, filterResult.displayName, filterResult.type, filterResult.name, "")
								, false)
					}
					filterResult.appliedFilters?.let {
						if (it.isNotEmpty() && value.uuid == it.first().uuid) {
							isChecked = true
							chipBackgroundColor = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
							setTextColor(ColorStateList.valueOf(resources.getColor(R.color.white)))
						}
					}

				}.also { chipGroupView.addView(it) }
			}

			chipGroupView.setOnCheckedChangeListener { chipGroup: ChipGroup, position: Int ->
				chipGroup.forEachIndexed { index, view ->
					with(view as Chip) {
						chipBackgroundColor = ContextCompat.getColorStateList(context, R.color.white)
						setTextColor(ColorStateList.valueOf(confHelper.backgroundThemeAccentColor))
					}
				}
			}
		}
	}
}
