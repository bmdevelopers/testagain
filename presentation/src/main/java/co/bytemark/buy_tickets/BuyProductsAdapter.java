package co.bytemark.buy_tickets;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.SwitchCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.add_payment_card.Helper.TextWatcherHelper;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.analytics.AnalyticsPlatformsContract;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.AccessibilityDelegate;
import co.bytemark.sdk.Helpers.PaymentMethod;
import co.bytemark.sdk.model.config.ChildOrganization;
import co.bytemark.sdk.model.payment_methods.GooglePay;
import co.bytemark.sdk.model.product_search.entity.EntityResult;
import co.bytemark.sdk.model.subscriptions.InputFields;
import co.bytemark.shopping_cart_new.NewShoppingCartActivityFragment;
import co.bytemark.widgets.GlideRotationTransformation;
import timber.log.Timber;

import static android.text.TextUtils.isEmpty;
import static androidx.core.graphics.ColorUtils.setAlphaComponent;
import static co.bytemark.sdk.model.config.RowType.DESTINATION_ATTRIBUTE_NAME;
import static co.bytemark.sdk.model.config.RowType.ORIGIN_ATTRIBUTE_NAME;

public class BuyProductsAdapter extends RecyclerView.Adapter<ProductNewViewHolder> {

    private final List<EntityResult> entityResults = new ArrayList<>();
    private final Fragment fragment;
    private boolean isGooglePayReady;
    private String regularEXP = "^[a-zA-Z0-9]*$";

    @Inject
    Application application;

    @Inject
    ConfHelper confHelper;

    @Inject
    Gson gson;

    @Inject
    SharedPreferences sharedPreferences;

    @Inject
    AnalyticsPlatformAdapter analyticsPlatformAdapter;

    private Map<String, Integer> cartProductQuantityMap;

    private String[] productQtys;

    private final Context context;
    private boolean isFareMediumScreen;

    public BuyProductsAdapter(Fragment fragment, boolean isFareMediumScreen) {
        CustomerMobileApp.Companion.getComponent().inject(this);
        this.fragment = fragment;
        this.context = fragment.getActivity().getApplicationContext();
        this.isFareMediumScreen = isFareMediumScreen;
        cartProductQuantityMap = new HashMap<>();
        getCartQuantityMapping();
    }

    public void getCartQuantityMapping() {
        cartProductQuantityMap.clear();
        String entityResultString = sharedPreferences.getString(AppConstants.ENTITY_RESULT_LIST, null);
        List<EntityResult> savedEntityResults = gson.fromJson(entityResultString, new TypeToken<List<EntityResult>>() {
        }.getType());
        if (savedEntityResults != null && !savedEntityResults.isEmpty()) {
            for (EntityResult item : savedEntityResults) {
                cartProductQuantityMap.put(item.getUuid(), item.getQuantity());
            }
        }
    }

    public void updateCartQuantityMap() {
        getCartQuantityMapping();
        notifyDataSetChanged();
    }

    protected void setGooglePayReady(boolean isGooglePayReady) {
        this.isGooglePayReady = isGooglePayReady;
        notifyDataSetChanged();
    }

    @Override
    public ProductNewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_products, parent, false);
        return new ProductNewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductNewViewHolder holder, int position) {
        holder.textViewProductAmount.setText(confHelper.getConfigurationPurchaseOptionsCurrencySymbol(entityResults.get(holder.getAdapterPosition()).getSalePrice()));

        if (holder.subscribe != null && entityResults.get(position).getSubscription() != null && entityResults.get(position).getSubscription().getSubscription_allowed()) {
            if (entityResults.get(position).getCreateSubscription())
                holder.subscribe.setChecked(true);
            else
                holder.subscribe.setChecked(false);
            holder.subscribe.setVisibility(View.VISIBLE);
            setSwitchCompactColor(holder.subscribe);
            holder.subscribeText.setVisibility(View.VISIBLE);
            if (isFareMediumScreen)
                holder.subscribeText.setText(context.getString(R.string.pass_auto_renewals_auto_renew));
            else
                holder.subscribeText.setText(context.getString(R.string.buy_tickets_popup_subscribe_button));
        } else {
            holder.subscribe.setVisibility(View.GONE);
            holder.subscribeText.setVisibility(View.GONE);
        }


        holder.subscribe.setOnClickListener((View v) -> {

            setSwitchCompactColor(holder.subscribe);
            entityResults.get(position).setCreateSubscription(holder.subscribe.isChecked());
            ((NewShoppingCartActivityFragment) fragment).updateEntityResultList(entityResults);
            updateCardContentDescription(holder);

            // ((NotificationSettingsFragment) (fragment)).updateSubscription(, holder.subscribe.isChecked());
        });

        if (!isEmpty(entityResults.get(holder.getAdapterPosition()).getName())) {
            holder.textViewProductName.setText(entityResults.get(holder.getAdapterPosition()).getName());
        } else {
            holder.textViewProductName.setText(entityResults.get(holder.getAdapterPosition()).getLabelName());
        }

        holder.textViewProductShortDescription.setText(entityResults.get(holder.getAdapterPosition()).getShortDescription());


        holder.refPosition = holder.getAdapterPosition();

        if (!isEmpty(holder.textViewQuantity.getText()) && !isEmpty(productQtys[holder.getAdapterPosition()])) {
            final int oldValue = Integer.parseInt(holder.textViewQuantity.getText().toString());
            final int newValue = Integer.parseInt(productQtys[holder.getAdapterPosition()]);
            if (oldValue != newValue) {
                holder.textViewQuantity.setText(productQtys[holder.getAdapterPosition()]);

            }
        } else {
            holder.textViewQuantity.setText(productQtys[holder.getAdapterPosition()]);

        }

        holder.spotNoET.setText("");
        holder.spotNoET.clearFocus();

        holder.buttonPlus.setOnClickListener(view -> {
            int newValue = Integer.parseInt(holder.textViewQuantity.getText().toString()) + 1;
            EntityResult entityResult = entityResults.get(holder.getAdapterPosition());
            int newItemQuantity = newValue;
            if (cartProductQuantityMap.containsKey(entityResult.getUuid())) {
                newItemQuantity += cartProductQuantityMap.get(entityResult.getUuid());
            }
            if ((newValue <= 20 && entityResult.getMaxQuantityForPurchase() == 0) || newItemQuantity <= entityResult.getMaxQuantityForPurchase()) {
                holder.textViewQuantity.setText(String.valueOf(newValue));
                updateCardContentDescription(holder);
                if (newItemQuantity == entityResult.getMaxQuantityForPurchase()) {
                    holder.buttonPlus.setAlpha(0.3f);
                    holder.buttonPlus.setEnabled(false);
                }
            }
        });

        holder.buttonMinus.setOnClickListener(view -> {
            int newValue = Integer.parseInt(holder.textViewQuantity.getText().toString()) - 1;
            EntityResult entityResult = entityResults.get(holder.getAdapterPosition());
            int newItemQuantity = newValue;
            if (cartProductQuantityMap.containsKey(entityResult.getUuid())) {
                newItemQuantity += cartProductQuantityMap.get(entityResult.getUuid());
            }
            if (newValue >= 1) {
                holder.textViewQuantity.setText(String.valueOf(newValue));
                updateCardContentDescription(holder);
                if (newItemQuantity < entityResult.getMaxQuantityForPurchase()) {
                    holder.buttonPlus.setAlpha(1.0f);
                    holder.buttonPlus.setEnabled(true);
                }
            }
        });

        holder.textViewQuantity.addTextChangedListener(new TextWatcherHelper() {
            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    productQtys[holder.getAdapterPosition()] = editable.toString();

                    if (fragment instanceof NewShoppingCartActivityFragment) {

                        if (!editable.toString().isEmpty()) {

                            entityResults.get(holder.getAdapterPosition()).setQuantity(Integer.parseInt(editable.toString()));

                            ((NewShoppingCartActivityFragment) fragment).updateEntityResultList(entityResults);
                        }

                    } else if (fragment instanceof BuyTicketsActivityFragment) {

                        if (!editable.toString().isEmpty()) {

                            entityResults.get(holder.getAdapterPosition()).setQuantity(Integer.parseInt(editable.toString()));

                        }
                    }
                } catch (IndexOutOfBoundsException e) {
                    Timber.e(e);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                try {
                    productQtys[holder.getAdapterPosition()] = charSequence.toString();
                } catch (IndexOutOfBoundsException e) {
                    Timber.e(e);
                }
            }
        });


        holder.buttonProductAddToCart.setOnClickListener(view -> {
            try {
                holder.buttonProductAddToCart.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
                if (!productQtys[holder.getAdapterPosition()].isEmpty()) {
                    String entityResultString = sharedPreferences.getString(AppConstants.ENTITY_RESULT_LIST, null);
                    List<EntityResult> savedEntityResults = gson.fromJson(entityResultString, new TypeToken<List<EntityResult>>() {
                    }.getType());
                    int currentQuantityinCart = 0;
                    if (savedEntityResults != null) {
                        for (int i = 0; i < savedEntityResults.size(); i++) {
                            currentQuantityinCart += savedEntityResults.get(i).getQuantity();
                        }
                    }
                    if (currentQuantityinCart + Integer.parseInt(productQtys[holder.getAdapterPosition()]) > 20) {
                        showShoppingCartFullErrorDialog();
                    } else {
                        hideKeyboard(view);
                        final EntityResult entityResult = entityResults.get(holder.getAdapterPosition());
                        entityResult.setQuantity(Integer.parseInt(productQtys[holder.getAdapterPosition()]));

                        holder.buttonProductAddToCart.announceForAccessibility(
                                String.format(context.getString(R.string.buy_tickets_add_to_cart_button_tap_vo), entityResult.getQuantity(), entityResult.getName()));

                        if (entityResult.getInputFieldRequired() != null && entityResult.getInputFieldRequired().size() > 0 && ((BuyTicketsActivityFragment) fragment).alertAlreadyShowed(entityResult)) {
                            ((BuyTicketsActivityFragment) fragment).showPopUp(entityResult);
                        } else if (entityResult.getAlertMessage() != null && !entityResult.getAlertMessage().equals("") && !((BuyTicketsActivityFragment) fragment).alertAlreadyShowed(entityResult)) {
                            ((BuyTicketsActivityFragment) fragment).showAlerts(holder.buttonProductAddToCart, entityResult, holder.cardViewProductCard, null);
                        } else if (entityResult.getSubscription() != null && entityResult.getSubscription().getSubscription_allowed() && !((BuyTicketsActivityFragment) fragment).alertAlreadyShowed(entityResult)) {
                            ((BuyTicketsActivityFragment) fragment).showSubscriptions(holder.buttonProductAddToCart, entityResult, holder.cardViewProductCard, null);
                        } else {
                            ((BuyTicketsActivityFragment) fragment).handleTheCart(entityResult, holder.cardViewProductCard);
                        }
                        if (!isEmpty(entityResult.getName())) {
                            analyticsPlatformAdapter.addToCart(entityResult.getName(), entityResult.getUuid(), entityResult.getQuantity(), confHelper.getConfigurationPurchaseOptionsCurrency(entityResult.getSalePrice()));
                        } else {
                            analyticsPlatformAdapter.addToCart(entityResult.getLabelName(), entityResult.getUuid(), entityResult.getQuantity(), confHelper.getConfigurationPurchaseOptionsCurrency(entityResult.getSalePrice()));

                        }
                    }
                }
            } catch (IndexOutOfBoundsException e) {
                Timber.e(e);
            }
        });

        holder.buttonDelete.setOnClickListener(v -> {
            final EntityResult entityResult = entityResults.get(holder.getAdapterPosition());
            String name = entityResult.getName();
            if (name.isEmpty()) {
                name = entityResult.getLabelName();
            }
            analyticsPlatformAdapter.itemRemoved(name, entityResult.getUuid(), entityResult.getQuantity(), confHelper.getConfigurationPurchaseOptionsCurrency(entityResult.getSalePrice()));


            if (holder.getAdapterPosition() > -1) {
                entityResults.remove(holder.getAdapterPosition());
            }
            ((NewShoppingCartActivityFragment) fragment).updateShoppingCart(entityResults);
        });

        handleExtraInfo(holder, position);

        if (isGooglePayReady && confHelper.isGooglePayEnabledOnStoreScreen()) {
            holder.relativeLayoutGooglePay.setVisibility(View.VISIBLE);
            holder.buttonBuyNow.setVisibility(View.GONE);
        } else {
            holder.relativeLayoutGooglePay.setVisibility(View.GONE);
            holder.buttonBuyNow.setVisibility(View.VISIBLE);
        }

        holder.relativeLayoutGooglePay.setOnClickListener(view -> onClickExpressCheckout(holder.relativeLayoutGooglePay, holder, new GooglePay()));

        holder.buttonBuyNow.setOnClickListener(view -> onClickExpressCheckout(holder.buttonBuyNow, holder, null));

        setProductImage(holder, holder.getAdapterPosition());

        AccessibilityManager am = (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
        if (am != null && am.isEnabled() && CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations() != null) {
            String organizationName = null;
            if (CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations() != null
                    && CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations().size() > 0) {
                for (int i = 0; i < CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations().size(); i++) {
                    String orgUuid = CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations().get(i).getUuid();
                    if (orgUuid.equals(entityResults.get(position).getOrganization().getLegacyUuid())) {
                        organizationName = CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations().get(i).getDisplayName();
                    }
                }
            }
            if (organizationName == null &&
                    entityResults.get(position).getOrganization().getLegacyUuid().equals(CustomerMobileApp.Companion.getConf().getOrganization().getUuid())) {
                organizationName = CustomerMobileApp.Companion.getConf().getOrganization().getDisplayName();
            }
            if (!TextUtils.isEmpty(organizationName))
                holder.imageViewProductLogo.setContentDescription(organizationName + " " + context.getString(R.string.buy_tickets_logo_voonly));

            ViewCompat.setAccessibilityDelegate(holder.buttonPlus, new AccessibilityDelegate(context.getString(R.string.buy_tickets_add_button_increase_quantity_by_one_vo_only)));

            ViewCompat.setAccessibilityDelegate(holder.buttonMinus, new AccessibilityDelegate(context.getString(R.string.buy_tickets_remove_button_reduce_quantity_by_one_vo_only)));

            holder.subscribeText.setContentDescription(holder.subscribeText.getText().toString());
            ViewCompat.setAccessibilityDelegate(holder.subscribeText, new AccessibilityDelegate(""));

            updateCardContentDescription(holder);


        }

        if (entityResults.get(position).getInputFieldRequired() != null && entityResults.get(position).getInputFieldRequired().size() > 0) {
            if (entityResults.get(position).getInputFieldRequired().contains(AppConstants.LICENSE_PLATE_NUMBER)) {
                holder.licenseNoET.setVisibility(View.VISIBLE);
                holder.licenseNoInputLayout.setVisibility(View.VISIBLE);
            } else {
                holder.licenseNoET.setVisibility(View.GONE);
                holder.licenseNoInputLayout.setVisibility(View.GONE);
            }

            if (entityResults.get(position).getInputFieldRequired().contains(AppConstants.SPOT_NUMBER)) {
                holder.spotNoET.setVisibility(View.VISIBLE);
                holder.spotNoInputLayout.setVisibility(View.VISIBLE);
            } else {
                holder.spotNoET.setVisibility(View.GONE);
                holder.spotNoInputLayout.setVisibility(View.GONE);
            }
        }

        if (entityResults.get(position).getInputFieldRequired() != null && entityResults.get(holder.getAdapterPosition()).getInputFieldRequired().size() > 0) {
            holder.buttonMinus.setVisibility(View.GONE);
            holder.buttonPlus.setVisibility(View.GONE);
            holder.textViewQuantity.setVisibility(View.GONE);
            enableButtons(holder.licenseNoET, holder.spotNoET, holder.buttonProductAddToCart, holder.relativeLayoutGooglePay, holder.buttonBuyNow);


            holder.spotNoET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (entityResults.get(position).getInputFieldRequired() != null &&
                            entityResults.get(position).getInputFieldRequired().size() > 0) {
                        if (s.toString().matches(regularEXP)) {
                            holder.spotNoInputLayout.setErrorEnabled(false);
                            holder.spotNoInputLayout.setError(null);
                            InputFields inputFields = new InputFields();

                            if (entityResults.get(position).getInputFields() == null) {
                                entityResults.get(position).setInputFields(inputFields);
                            }
                            entityResults.get(position).getInputFields().setSpotNumber(s.toString());
                        } else {
                            holder.spotNoInputLayout.setErrorEnabled(true);
                            holder.spotNoInputLayout.setError("Please enter a valid Spot Number");
                        }

                        enableButtons(holder.licenseNoET, holder.spotNoET, holder.buttonProductAddToCart, holder.relativeLayoutGooglePay, holder.buttonBuyNow);

                    }


                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            holder.licenseNoET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (entityResults.get(position).getInputFieldRequired() != null &&
                            entityResults.get(position).getInputFieldRequired().size() > 0) {

                        if (s.toString().matches(regularEXP)) {

                            holder.licenseNoInputLayout.setErrorEnabled(false);
                            holder.licenseNoInputLayout.setError(null);

                            InputFields inputFields = new InputFields();

                            if (entityResults.get(holder.getAdapterPosition()).getInputFields() == null) {
                                entityResults.get(holder.getAdapterPosition()).setInputFields(inputFields);
                            }
                            entityResults.get(holder.getAdapterPosition()).getInputFields().setLicensePlateNumber(s.toString());
                            ((BuyTicketsActivityFragment) fragment).updateLicenseNo(s.toString());
                        } else {
                            holder.licenseNoInputLayout.setErrorEnabled(true);
                            holder.licenseNoInputLayout.setError("Please enter a valid License number.");
                        }

                        enableButtons(holder.licenseNoET, holder.spotNoET, holder.buttonProductAddToCart, holder.relativeLayoutGooglePay, holder.buttonBuyNow);

                    }

                }

                @Override
                public void afterTextChanged(Editable s) {


                }
            });
        } else {
            holder.buttonMinus.setVisibility(View.VISIBLE);
            holder.buttonPlus.setVisibility(View.VISIBLE);
            holder.textViewQuantity.setVisibility(View.VISIBLE);

        }

        if (fragment instanceof BuyTicketsActivityFragment) {
            holder.linearLayoutBuyButton.setVisibility(View.VISIBLE);
            holder.relativeLayoutDeleteButton.setVisibility(View.GONE);
            holder.buttonProductAddToCart.setContentDescription(context.getString(R.string.buy_tickets_add_cd_voonly) + " " + entityResults.get(position).getName() + " " + context.getString(R.string.buy_tickets_to_cart_voonly));
            holder.textViewProductOriginDestination.setVisibility(View.GONE);
            holder.licence_spot_no.setVisibility(View.GONE);
            if (entityResults.get(position).getInputFieldRequired() != null && entityResults.get(position).getInputFieldRequired().size() > 0) {
                holder.licenseNoET.setText(((BuyTicketsActivityFragment) fragment).getLicenseNo());
                holder.licenseNoET.setSelection(holder.licenseNoET.getText().toString().length());
                holder.spotNoET.setVisibility(View.VISIBLE);
                holder.licenseNoET.setVisibility(View.VISIBLE);

            } else {
                hideLicenseSpot(holder);
            }
        } else {
            holder.linearLayoutBuyButton.setVisibility(View.GONE);
            holder.relativeLayoutDeleteButton.setVisibility(View.VISIBLE);
            holder.buttonDelete.setText(R.string.buy_tickets_delete);
            holder.buttonDelete.setContentDescription(context.getString(R.string.buy_tickets_remove_cd_voonly) + " " + entityResults.get(position).getName() + " " + context.getString(R.string.buy_tickets_from_cart_voonly));

            Boolean displayLongNameForOd = confHelper.isDisplayLongNameForOd(entityResults.get(position).getOrganization().getUuid());
            String origin = entityResults.get(position).getAppliedFilters(ORIGIN_ATTRIBUTE_NAME, displayLongNameForOd);
            String destination = entityResults.get(position).getAppliedFilters(DESTINATION_ATTRIBUTE_NAME, displayLongNameForOd);
            if (origin != null && destination != null) {
                holder.textViewProductOriginDestination.setText(origin + " / " + destination);
                holder.textViewProductOriginDestination.setVisibility(View.VISIBLE);
            } else {
                holder.textViewProductOriginDestination.setVisibility(View.GONE);
            }

            hideLicenseSpot(holder);
            if (entityResults.get(holder.getAdapterPosition()).getInputFields() != null && holder.licence_spot_no != null && entityResults.get(holder.getAdapterPosition()).getInputFields().getLicensePlateNumber() != null && !entityResults.get(holder.getAdapterPosition()).getInputFields().getLicensePlateNumber().equals("") && entityResults.get(holder.getAdapterPosition()).getInputFields().getSpotNumber() != null && !entityResults.get(holder.getAdapterPosition()).getInputFields().getSpotNumber().equals("")) {
                holder.licence_spot_no.setText(entityResults.get(holder.getAdapterPosition()).getInputFields().getLicensePlateNumber() + " / " + entityResults.get(holder.getAdapterPosition()).getInputFields().getSpotNumber());
                holder.licence_spot_no.setVisibility(View.VISIBLE);

            } else {
                holder.licence_spot_no.setVisibility(View.GONE);
            }
        }

        // Max Quantity Check and update
        EntityResult entityResult = entityResults.get(position);

        if (entityResult.getMaxQuantityForPurchase() == 1) {
            holder.buttonPlus.setVisibility(View.GONE);
            holder.buttonMinus.setVisibility(View.GONE);
            holder.textViewQuantity.setVisibility(View.GONE);
        } else {
            holder.buttonPlus.setVisibility(View.VISIBLE);
            holder.buttonMinus.setVisibility(View.VISIBLE);
            holder.textViewQuantity.setVisibility(View.VISIBLE);
        }

        if (cartProductQuantityMap.containsKey(entityResult.getUuid()) &&
                cartProductQuantityMap.get(entityResult.getUuid()) == entityResults.get(position).getMaxQuantityForPurchase()) {
            holder.buttonProductAddToCart.setEnabled(false);
            holder.buttonProductAddToCart.setAlpha(0.3f);
        } else {
            holder.buttonProductAddToCart.setEnabled(true);
            holder.buttonProductAddToCart.setAlpha(1.0f);
        }
    }

    void hideLicenseSpot(ProductNewViewHolder holder) {
        holder.spotNoET.setVisibility(View.GONE);
        holder.licenseNoInputLayout.setVisibility(View.GONE);
        holder.spotNoInputLayout.setVisibility(View.GONE);
        holder.licenseNoET.setVisibility(View.GONE);
    }

    void enableButtons(TextInputEditText licenseNoET, TextInputEditText spotNoET, Button buttonProductAddToCart, RelativeLayout relativeLayoutGooglePay, Button buttonBuyNow) {
        if (!licenseNoET.getText().toString().equals("") && !spotNoET.getText().toString().equals("") && licenseNoET.getText().toString().matches(regularEXP) &&
                spotNoET.getText().toString().matches(regularEXP)) {
            buttonProductAddToCart.setEnabled(true);
            relativeLayoutGooglePay.setEnabled(true);
            buttonBuyNow.setEnabled(true);
            buttonProductAddToCart.setBackgroundTintList(ColorStateList.valueOf(confHelper.getDataThemeAccentColor()));
            buttonBuyNow.setBackgroundTintList(null);


        } else {
            int alphaComponent = setAlphaComponent(confHelper.getAccentThemeBacgroundColor(), 26);
            buttonProductAddToCart.setBackgroundTintList(ColorStateList.valueOf(alphaComponent));
            buttonProductAddToCart.setEnabled(false);
            relativeLayoutGooglePay.setEnabled(false);
            buttonBuyNow.setEnabled(false);
            buttonBuyNow.setBackgroundTintList(ColorStateList.valueOf(alphaComponent));
        }

    }

    void setSwitchCompactColor(SwitchCompat subscribeSwitch) {
        subscribeSwitch.setThumbTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.bt_very_light_gray)));

        if (subscribeSwitch.isChecked()) {
            subscribeSwitch.setTrackTintList(ColorStateList.valueOf(confHelper.getAccentThemeBacgroundColor()));

        } else {
            subscribeSwitch.setTrackTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.warm_grey)));


        }
    }

    private void updateCardContentDescription(ProductNewViewHolder holder) {

        holder.cardViewProductCard.setContentDescription(holder.textViewProductName.getText().toString() + "\n" +
                holder.textViewProductShortDescription.getText().toString() + "\n" +
                holder.textViewProductAmount.getText().toString() + "\n" +
                context.getString(R.string.buy_tickets_number_of_products_selected_vo_only) + " " +
                holder.textViewQuantity.getText().toString() + "" + "\n" +
                holder.subscribeText.getText().toString() + (holder.subscribe.isChecked() ?
                context.getString(R.string.buy_tickets_pass_auto_renewals_enabled) : context.getString(R.string.buy_tickets_pass_auto_renewals_disabled)) + "");

        holder.textViewQuantity.setContentDescription(context.getString(R.string.buy_tickets_quantity_is_voonly) + holder.textViewQuantity.getText().toString());

    }

    private void setProductImage(ProductNewViewHolder holder, int position) {
        int backgroundColor = confHelper.getHeaderThemeBackgroundColor();
        if (entityResults.get(position).getTheme() != null &&
                entityResults.get(position).getTheme().getBackgroundColor() != null) {
            try {
                backgroundColor = confHelper.parseColor(entityResults.get(position).getTheme().getBackgroundColor());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        } else if (confHelper.getChildOrganizations() != null && confHelper.getChildOrganizations().size() > 0) {
            {
                holder.imageViewProductLogo.setBackgroundColor(confHelper.getHeaderThemeBackgroundColor());
                for (ChildOrganization childOrganization : confHelper.getChildOrganizations()) {
                    if (childOrganization.getUuid().equals(entityResults.get(position).getOrganization().getLegacyUuid()))
                        if (!childOrganization.getBranding().getThemes().getHeaderTheme().getBackgroundColor().isEmpty()) {
                            String color = childOrganization.getBranding().getThemes().getHeaderTheme().getBackgroundColor();
                            try {
                                backgroundColor = confHelper.parseColor(color);
                            } catch (IllegalArgumentException e) {
                                e.printStackTrace();
                            }
                        }
                }
            }
        }
        holder.imageViewProductLogo.setBackgroundColor(backgroundColor);

        String uuid = entityResults.get(position).getOrganization().getLegacyUuid().replaceAll("-", "_");

        Drawable drawable = null;
        String drawableName = null;

        try {
            drawable = confHelper.getDrawableByName("wide_" + uuid);
            drawableName = "wide_" + uuid;
        } catch (Exception e) {
            Timber.e("Failure to get drawable id. %s", e.toString());
        }

        if (drawable == null) {
            final String parentOrgUuid = CustomerMobileApp.Companion.getConf().getOrganization().getUuid().replaceAll("-", "_");
            drawableName = "wide_" + parentOrgUuid;
        }

        if (entityResults.get(holder.getAdapterPosition()).getProductImagePath() != null) {
            Glide
                    .with(context)
                    .load(entityResults.get(holder.getAdapterPosition()).getProductImagePath())
                    .animate(android.R.anim.fade_in)
                    .transform(new GlideRotationTransformation(context, -90))
                    .into(holder.imageViewProductLogo);
        } else {
            if (drawableName != null)
                Glide
                        .with(context)
                        .load(context.getResources().getIdentifier(drawableName, "drawable", context.getPackageName()))
                        .animate(android.R.anim.fade_in)
                        .transform(new GlideRotationTransformation(context, -90))
                        .into(holder.imageViewProductLogo);
        }
    }

    private void onClickExpressCheckout(View view, ProductNewViewHolder holder, PaymentMethod paymentMethod) {
        if (holder.getAdapterPosition() >= 0 && !productQtys[holder.getAdapterPosition()].isEmpty()) {
            final EntityResult entityResult = entityResults.get(holder.getAdapterPosition());
            entityResult.setQuantity(Integer.parseInt(productQtys[holder.getAdapterPosition()]));

            view.announceForAccessibility(
                    String.format(context.getString(R.string.buy_tickets_buy_button_tap_vo), entityResult.getQuantity(), entityResult.getName()));

            if (!(paymentMethod instanceof GooglePay) && entityResult.getAlertMessage() != null && !entityResult.getAlertMessage().equals("") && !((BuyTicketsActivityFragment) fragment).alertAlreadyShowed(entityResult)) {
                ((BuyTicketsActivityFragment) fragment).showAlerts(view, entityResult, null, paymentMethod);
            } else if (entityResult.getSubscription() != null && entityResult.getSubscription().getSubscription_allowed() && !((BuyTicketsActivityFragment) fragment).alertAlreadyShowed(entityResult)) {
                ((BuyTicketsActivityFragment) fragment).showSubscriptions(view, entityResult, null, paymentMethod);

            } else {

                ((BuyTicketsActivityFragment) fragment).emptyProductSharedPrefsAndAddNewProducts(entityResult);

                ((BuyTicketsActivityFragment) fragment).goToPaymentScreen(paymentMethod, AnalyticsPlatformsContract.Screen.EXPRESS_CHECKOUT);
            }
            String productName = entityResult.getName();

            if (TextUtils.isEmpty(productName)) {
                productName = entityResult.getLabelName();
            }

            String paymentType = AnalyticsPlatformsContract.ExpressCheckout.BUY_NOW;
            if (paymentMethod instanceof GooglePay) {
                paymentType = AnalyticsPlatformsContract.ExpressCheckout.GOOGLE_PAY;
            }
            analyticsPlatformAdapter.expressCheckout(productName, entityResult.getUuid(), entityResult.getQuantity(),
                    confHelper.getConfigurationPurchaseOptionsCurrency(entityResult.getSalePrice()), paymentType);
        }


    }

    private void handleExtraInfo(ProductNewViewHolder holder, int position) {
        String longDescription = entityResults.get(position).getLongDescription();
        if (longDescription == null || longDescription.isEmpty()) {
            holder.imageViewShowLongDesc.setVisibility(View.INVISIBLE);
            holder.textViewLongDescription.setVisibility(View.GONE);
            holder.imageViewShowLongDesc.setRotation(0);
        } else {
            holder.imageViewShowLongDesc.setVisibility(View.VISIBLE);
            holder.textViewLongDescription.setText(longDescription);
            holder.imageViewShowLongDesc.setContentDescription(context.getString(R.string.buy_tickets_open_description_vo_only));
        }

        holder.imageViewShowLongDesc.setOnClickListener(view -> {
            if (holder.textViewLongDescription.getVisibility() == View.GONE) {
                holder.textViewLongDescription.setVisibility(View.VISIBLE);
                beginTransition(holder.cardViewProductCard);
                holder.imageViewShowLongDesc.setRotation(180);
                holder.imageViewShowLongDesc.setContentDescription(context.getString(R.string.buy_tickets_close_description_vo_only));
            } else {
                holder.textViewLongDescription.setVisibility(View.GONE);
                holder.imageViewShowLongDesc.setRotation(0);
                holder.imageViewShowLongDesc.setContentDescription(context.getString(R.string.buy_tickets_open_description_vo_only));
            }
            beginRotationAnimation(holder.imageViewShowLongDesc);
        });
    }

    private void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public int getItemCount() {
        return entityResults.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void resetQuantities() {
        for (int i = 0; i < productQtys.length; i++) {
            productQtys[i] = "1";
        }
    }

    public void updateProductList(List<EntityResult> entityResultBuilder) {
        productQtys = new String[entityResultBuilder.size()];
        entityResults.clear();
        entityResults.addAll(entityResultBuilder);
        for (int i = 0; i < entityResultBuilder.size(); i++) {
            if (entityResultBuilder.get(i).getQuantity() != 0) {
                productQtys[i] = String.valueOf(entityResultBuilder.get(i).getQuantity());
            } else {

                if (fragment instanceof NewShoppingCartActivityFragment) {
                    productQtys[i] = "0";
                    entityResults.get(i).setQuantity(0);
                    ((NewShoppingCartActivityFragment) fragment).updateEntityResultList(entityResults);
                } else {
                    productQtys[i] = "1";
                }

            }
        }
        notifyDataSetChanged();
    }

    private void beginTransition(ViewGroup rootLayout) {
        TransitionManager.beginDelayedTransition(rootLayout, new AutoTransition().setDuration(200));
    }

    private void beginRotationAnimation(ImageView imageView) {
        RotateAnimation rotateAnimation = new RotateAnimation(180.0f, 0.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setInterpolator(new DecelerateInterpolator());
        rotateAnimation.setRepeatCount(0);
        rotateAnimation.setDuration(200);
        rotateAnimation.setFillAfter(false);
        imageView.startAnimation(rotateAnimation);
    }

    public void showShoppingCartFullErrorDialog() {
        if (fragment != null && fragment.getActivity() != null)
            new MaterialDialog.Builder(fragment.getActivity())
                    .title(R.string.buy_tickets_popup_order_limit_reached)
                    .content(R.string.buy_tickets_popup_order_limit_reached_msg)
                    .positiveText(R.string.ok)
                    .positiveColor(confHelper.getDataThemeAccentColor())
                    .onPositive((dialog, which) -> dialog.dismiss())
                    .show();
    }
}