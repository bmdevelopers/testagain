package co.bytemark.buy_tickets;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.analytics.AnalyticsPlatformsContract;
import co.bytemark.base.BaseMvpFragment;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.product_details.ProductDetailsActivity;
import co.bytemark.schedules.SchedulesActivity;
import co.bytemark.sdk.Helpers.PaymentMethod;
import co.bytemark.sdk.model.product_search.entity.EntityResult;
import co.bytemark.sdk.post_body.AppliedFilter;
import co.bytemark.shopping_cart.AcceptPaymentActivity;
import co.bytemark.shopping_cart.GooglePayUtil;
import co.bytemark.shopping_cart_new.NewShoppingCartActivity;
import co.bytemark.use_tickets.UseTicketsAccessibility;
import co.bytemark.widgets.CircleBorderImageView;
import co.bytemark.widgets.ProgressViewLayout;

import static co.bytemark.analytics.AnalyticsPlatformsContract.AnalyticsPlatformEntry.HEADER_BUTTON;
import static co.bytemark.sdk.model.config.RowType.DESTINATION_ATTRIBUTE_NAME;
import static co.bytemark.sdk.model.config.RowType.ORIGIN_ATTRIBUTE_NAME;

public class BuyTicketsActivityFragment extends BaseMvpFragment<BuyTicketsView, BuyTicketsPresenter>
        implements BuyTicketsView {

    @Inject
    ConfHelper confHelper;

    @Inject
    Gson gson;

    @Inject
    SharedPreferences sharedPreferences;

    @Inject
    AnalyticsPlatformAdapter analyticsPlatformAdapter;

    @Inject
    GooglePayUtil googlePayUtil;

    @BindView(R.id.srl_product_list)
    SwipeRefreshLayout swipeRefreshLayoutProductList;

    @BindView(R.id.productListRecyclerView)
    RecyclerView recyclerViewProductList;

    @BindView(R.id.rl_product_list)
    RelativeLayout relativeLayoutProductList;

    @BindView(R.id.rl_category_selection)
    LinearLayout relativeLayoutCategorySelection;

    @BindView(R.id.tv_category)
    TextView textViewCategory;

    @BindView(R.id.tv_category_name)
    TextView textViewCategoryName;

    @BindView(R.id.ll_network_connectivity_error_view)
    LinearLayout linearLayoutNetworkConnectivityErrorView;

    @BindView(R.id.ll_no_products)
    LinearLayout linearLayoutNoProducts;

    @BindView(R.id.ll_loading_error)
    LinearLayout linearLayoutLoadingError;

    @BindView(R.id.ll_loading_products_list)
    LinearLayout linearLayoutLoadingProductsList;

    @BindView(R.id.ll_no_products_texts)
    LinearLayout linearLayoutNoProductsText;

    @BindView(R.id.ll_loading_error_texts)
    LinearLayout linearLayoutLodingErrorText;

    @BindView(R.id.ll_no_network_texts)
    LinearLayout linearLayoutNoNetworkText;

    @BindViews({R.id.tv_network_error_1, R.id.tv_network_error_2,
            R.id.tv_no_products_1, R.id.tv_no_products_2,
            R.id.tv_loading_products_1, R.id.tv_loading_products_2,
            R.id.tv_loading_error_1, R.id.tv_loading_error_2})
    List<TextView> textViewsLoadingCategories;

    @BindView(R.id.btn_retry)
    Button buttonRetry;

    @BindViews({R.id.btn_update_filters_no_products, R.id.btn_update_filters_loading_products})
    List<Button> button;

    @BindViews({R.id.iv_network_error, R.id.iv_loading_error})
    List<CircleBorderImageView> circleBorderImageView;

    @BindView(R.id.iv_no_products)
    CircleBorderImageView circleBorderImageViewNoProducts;

    @BindView(R.id.pvl_loading_products_list)
    ProgressViewLayout progressViewLayoutLoadingProducts;

    @BindView(R.id.iv_loading_products_list)
    ImageView imageViewLoadingProducts;

    @BindView(R.id.ll_category_selection)
    LinearLayout linearLayoutCategorySelection;

    @BindView(R.id.iv_category)
    ImageView imageViewCategory;

    @BindView(R.id.ll_device_time_error_view)
    LinearLayout linearLayoutDeviceTimeError;


    private BuyProductsAdapter buyTicketsAdapter;

    private List<EntityResult> savedEntityResults = new ArrayList<>();
    private boolean isProductAvailableInSharedPrefs;
    private EntityResult entityResult;
    private boolean isSharedPrefsUpdated;
    private boolean forceReloaded;
    private boolean registerAnalytics = true;

    private String fareMediaId;
    private String productUUIDOfRepurchasePass;

    public static BuyTicketsActivityFragment newInstance(@NonNull List<AppliedFilter> appliedFilters,
                                                         @NonNull String categoryName) {
        final BuyTicketsActivityFragment fragment = new BuyTicketsActivityFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("appliedFilters", (ArrayList<? extends Parcelable>) appliedFilters);
        args.putString("categoryName", categoryName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        //if (isUUIDEmpty() && isRepurchaseUUIDEmpty()) {
        return R.layout.fragment_buy_tickets;
        //}
        //return R.layout.fragment_loading;
    }

    @Override
    protected void setAccentThemeColors() {
        relativeLayoutCategorySelection.setBackgroundColor(confHelper.getAccentThemeAccentColor());
        textViewCategory.setTextColor(confHelper.getAccentThemeSecondaryTextColor());
        textViewCategoryName.setTextColor(confHelper.getAccentThemeSecondaryTextColor());
        imageViewCategory.setImageTintList(ColorStateList.valueOf(confHelper.getAccentThemeSecondaryTextColor()));
    }

    @Override
    protected void setBackgroundThemeColors() {
        if (!isRepurchaseUUIDEmpty()) {
            relativeLayoutProductList.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
            recyclerViewProductList.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        } else {
            relativeLayoutProductList.setBackgroundColor(confHelper.getCollectionThemeBackgroundColor());
            recyclerViewProductList.setBackgroundColor(confHelper.getCollectionThemeBackgroundColor());
        }
        linearLayoutNetworkConnectivityErrorView.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        linearLayoutNoProducts.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        linearLayoutLoadingError.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        linearLayoutLoadingProductsList.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        circleBorderImageViewNoProducts.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));

        for (int i = 0; i < circleBorderImageView.size(); i++) {
            circleBorderImageView.get(i).setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemePrimaryTextColor()));
        }

        for (int i = 0; i < button.size(); i++) {
            button.get(i).setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
            button.get(i).setTextColor(confHelper.getBackgroundThemeBackgroundColor());
        }

        for (int i = 0; i < textViewsLoadingCategories.size(); i++) {
            textViewsLoadingCategories.get(i).setTextColor(confHelper.getBackgroundThemePrimaryTextColor());
        }

        imageViewLoadingProducts.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        progressViewLayoutLoadingProducts.setProgressColor(confHelper.getBackgroundThemeAccentColor(), confHelper.getBackgroundThemeBackgroundColor());
        buttonRetry.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        buttonRetry.setTextColor(confHelper.getBackgroundThemeBackgroundColor());
    }

    @Override
    protected void setDataThemeColors() {
    }

    @NonNull
    @Override
    public BuyTicketsPresenter createPresenter() {
        return new BuyTicketsPresenter();
    }

    @Override
    protected void onInject() {
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        fareMediaId = getActivity().getIntent().getStringExtra("fareMediumId");
        productUUIDOfRepurchasePass = ((BuyTicketsActivity) getActivity()).productUUIDOfRepurchasePass;
    }

    @OnClick(R.id.btn_retry)
    void onClickRetryButton() {
        refreshProductList();
    }

    private void announceForAccessibility() {
        Resources resources = getActivity().getResources();
        linearLayoutNoProductsText.setContentDescription(resources.getString(R.string.buy_tickets_no_available_products) +
                resources.getString(R.string.buy_tickets_no_available_products_message));

        linearLayoutLodingErrorText.setContentDescription(resources.getString(R.string.oops) +
                resources.getString(R.string.error_loading_message));

        linearLayoutNoNetworkText.setContentDescription(resources.getString(R.string.network_connectivity_error) + ". " +
                resources.getString(R.string.network_connectivity_error_message));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        view.announceForAccessibility(getString(R.string.buy_tickets));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        announceForAccessibility();

        setBuyTicketsAdapter();

        if (confHelper.isGooglePayEnabled()) {
            int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getActivity().getApplicationContext());
            if (status == ConnectionResult.SUCCESS) {
                googlePayUtil.makePaymentClient(getActivity());
                presenter.checkGooglePayReady();
            } else {
                showGooglePayReady(false);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (entityResult != null && !isSharedPrefsUpdated) {
            updateSharedPrefs(false, false, null);
        }
        buyTicketsAdapter.updateCartQuantityMap();
    }

    public void updateLicenseNo(String license) {
        sharedPreferences.edit().putString(AppConstants.LICENSE_NUMBER, license).commit();
    }

    public String getLicenseNo() {
        return sharedPreferences.getString(AppConstants.LICENSE_NUMBER, "");
    }

    private void updateSharedPrefs(boolean showAddingDiffOrgProductAlert, boolean invalidate, CardView cardViewProductCard) {
        String entityResultString = sharedPreferences.getString(AppConstants.ENTITY_RESULT_LIST, null);
        boolean isDiffOrg = false;
        if (entityResultString != null) {
            savedEntityResults = gson.fromJson(entityResultString, new TypeToken<List<EntityResult>>() {
            }.getType());

            if (savedEntityResults == null)
                savedEntityResults = new ArrayList<>();

            if (savedEntityResults.size() == 0) {
                savedEntityResults.add(entityResult);
            } else if (savedEntityResults.get(0).getOrganization().getLegacyUuid()
                    .equals(entityResult.getOrganization().getLegacyUuid())) {
                for (int i = 0; i < savedEntityResults.size(); i++) {
                    if (savedEntityResults.get(i).getUuid().equals(entityResult.getUuid()) && areSameFilters(savedEntityResults.get(i), entityResult)) {
                        int totalQty = savedEntityResults.get(i).getQuantity() + entityResult.getQuantity();
                        savedEntityResults.get(i).setQuantity(totalQty);
                        isProductAvailableInSharedPrefs = true;
                        break;
                    } else {
                        isProductAvailableInSharedPrefs = false;
                    }
                }

                if (!isProductAvailableInSharedPrefs) {
                    savedEntityResults.add(entityResult);
                }
            } else {
                if (showAddingDiffOrgProductAlert) {
                    isDiffOrg = true;
                    showAddToCartForDiffOrgErrorDialog(savedEntityResults.get(0).getOrganization().getLegacyUuid(), entityResult, cardViewProductCard);
                } else {
                    emptyProductSharedPrefsAndAddNewProducts(entityResult);
                }
            }
        } else {
            savedEntityResults.add(entityResult);
        }

        String composedSavedEntityResults = gson.toJson(savedEntityResults);

        sharedPreferences.edit()
                .putString(AppConstants.ENTITY_RESULT_LIST, composedSavedEntityResults)
                .apply();

        // cart item updated, update quantity map in adapter
        buyTicketsAdapter.updateCartQuantityMap();

        isSharedPrefsUpdated = true;
        if (invalidate && !isDiffOrg) {
            invalidateProductList();
            if (cardViewProductCard != null)
                startAddToCartAnimation(cardViewProductCard);
        }
    }

    private boolean areSameFilters(EntityResult savedEntity, EntityResult entityResult) {

        if (savedEntity.getAppliedFilters() != null && entityResult.getAppliedFilters() != null) {
            for (int i = 0; i < savedEntity.getAppliedFilters().size(); i++)
                if (savedEntity.getAppliedFilters().get(i).getFilterUuid() != null) {
                    if (!(savedEntity.getAppliedFilters().get(i).getFilterUuid().equals(entityResult.getAppliedFilters().get(i).getFilterUuid()) &&
                            savedEntity.getAppliedFilters().get(i).getFilterValue().equals(entityResult.getAppliedFilters().get(i).getFilterValue()))) {
                        return false;
                    }
                } else if (savedEntity.getAppliedFilters().get(i).getKey() != null) {
                    if (!(savedEntity.getAppliedFilters().get(i).getKey().equals(entityResult.getAppliedFilters().get(i).getKey()) &&
                            savedEntity.getAppliedFilters().get(i).getValue().equals(entityResult.getAppliedFilters().get(i).getValue()))) {
                        return false;
                    }
                }

        }
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.unSubscribe();
    }

    private void setBuyTicketsAdapter() {
        recyclerViewProductList.setHasFixedSize(true);
        recyclerViewProductList.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        buyTicketsAdapter = new BuyProductsAdapter(this, !TextUtils.isEmpty(fareMediaId));
        recyclerViewProductList.setAdapter(buyTicketsAdapter);
    }

    @Override
    protected void onOnline() {
        hideNetworkConnectionErrorView();
        setSwipeRefreshLayoutListener();
        showLoading();
        forceReloaded = false;
        presenter.getAcceptedPaymentMethods();
    }

    @Override
    public void callLoadProducts() {
        presenter.loadProductList(getAppliedFilters(), fareMediaId, ((BuyTicketsActivity) getActivity()).productUUID);
    }

    @Override
    protected void onOffline() {
        showNetworkConnectionErrorView();
    }

    private ArrayList<? extends Parcelable> getAppliedFilters() {
        // First Check if fareId is not null from Hacon and return appliedFiltersdC
        if (((BuyTicketsActivity) getActivity()).fareId != null) {
            ArrayList<AppliedFilter> appliedFilters = new ArrayList<>();
            appliedFilters.add(new AppliedFilter(AppConstants.GTFS_FARE_ID, ((BuyTicketsActivity) getActivity()).fareId, null, null, null, null, null, null));
            return appliedFilters;
        } else if (getArguments() != null) {
            return getArguments().getParcelableArrayList("appliedFilters");
        } else {
            return null;
        }
    }

    private String getCategoryName() {
        if (getArguments() != null) {
            return getArguments().getString("categoryName");
        } else {
            return null;
        }
    }

    private void setSwipeRefreshLayoutListener() {
        swipeRefreshLayoutProductList.setOnRefreshListener(() -> {
            forceReloaded = true;
            callLoadProducts();
        });
    }

    public void invalidateProductList() {
        buyTicketsAdapter.resetQuantities();
        buyTicketsAdapter.notifyDataSetChanged();
    }

    public void refreshProductList() {
        registerAnalytics = false;
        refreshNetworkActivity();
    }

    @Override
    public void showNetworkConnectionErrorView() {
        linearLayoutDeviceTimeError.setVisibility(View.GONE);
        swipeRefreshLayoutProductList.setRefreshing(false);
        linearLayoutNoProducts.setVisibility(View.GONE);
        recyclerViewProductList.setVisibility(View.GONE);
        linearLayoutNetworkConnectivityErrorView.setVisibility(View.VISIBLE);
        linearLayoutLoadingError.setVisibility(View.GONE);
        linearLayoutLoadingProductsList.setVisibility(View.GONE);

        linearLayoutNoNetworkText.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
        linearLayoutNoNetworkText.announceForAccessibility(getString(R.string.network_connectivity_error));
        if (!isRepurchaseUUIDEmpty()) hideActionBar();
    }

    @Override
    public void hideNetworkConnectionErrorView() {
        linearLayoutDeviceTimeError.setVisibility(View.GONE);
        linearLayoutNoProducts.setVisibility(View.GONE);
        recyclerViewProductList.setVisibility(View.GONE);
        linearLayoutNetworkConnectivityErrorView.setVisibility(View.GONE);
        linearLayoutLoadingError.setVisibility(View.GONE);
        linearLayoutLoadingProductsList.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        linearLayoutDeviceTimeError.setVisibility(View.GONE);
        linearLayoutNoProducts.setVisibility(View.GONE);
        recyclerViewProductList.setVisibility(View.GONE);
        linearLayoutNetworkConnectivityErrorView.setVisibility(View.GONE);
        linearLayoutLoadingError.setVisibility(View.GONE);
        if (isOnline()) {
            linearLayoutLoadingProductsList.setVisibility(View.VISIBLE);
        } else {
            showNetworkConnectionErrorView();
        }
    }

    @Override
    public void hideLoading() {
        if (!isOnline()) {
            showNetworkConnectionErrorView();
        } else {
            if (isUUIDEmpty() && isRepurchaseUUIDEmpty()) {
                linearLayoutDeviceTimeError.setVisibility(View.GONE);
                linearLayoutNoProducts.setVisibility(View.GONE);
                recyclerViewProductList.setVisibility(View.VISIBLE);
                linearLayoutNetworkConnectivityErrorView.setVisibility(View.GONE);
                linearLayoutLoadingError.setVisibility(View.GONE);
                linearLayoutLoadingProductsList.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void showNoAvailableProductsList() {
        if (((BuyTicketsActivity) getActivity()).jwtTokenForDeeplink != null && ((BuyTicketsActivity) getActivity()).productUUID != null) {
            showErrorDialogIfNoMatchingProductFound();
        }
        swipeRefreshLayoutProductList.setRefreshing(false);
        linearLayoutNoProducts.setVisibility(View.VISIBLE);
        recyclerViewProductList.setVisibility(View.GONE);
        linearLayoutNetworkConnectivityErrorView.setVisibility(View.GONE);
        linearLayoutLoadingError.setVisibility(View.GONE);
        linearLayoutLoadingProductsList.setVisibility(View.GONE);

        linearLayoutNoProductsText.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
        linearLayoutNoProductsText.announceForAccessibility(getString(R.string.buy_tickets_no_available_products_voonly));

        analyticsPlatformAdapter.productsLoadingCompleted(forceReloaded ? 1 : 0, AnalyticsPlatformsContract.Status.SUCCESS, "No available Products");

    }

    @Override
    public void showDeviceTimeErrorView() {
        linearLayoutDeviceTimeError.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideDeviceTimeErrorView() {
        linearLayoutDeviceTimeError.setVisibility(View.GONE);
    }

    @Override
    public void showGooglePayReady(boolean isGooglePayReady) {
        buyTicketsAdapter.setGooglePayReady(isGooglePayReady && presenter.isGooglePayAccepted());
        buyTicketsAdapter.notifyDataSetChanged();
    }

    @Override
    public void setEntityResult(List<EntityResult> entityResult) {
        String productListSize = String.valueOf(entityResult.size());
        String noOfProductAvailable = getResources().getQuantityString(R.plurals.buy_tickets_product_available,
                Integer.parseInt(productListSize), Integer.parseInt(productListSize));
        textViewCategory.setText(noOfProductAvailable);
        swipeRefreshLayoutProductList.setRefreshing(false);
        buyTicketsAdapter.setGooglePayReady(presenter.isGooglePayReady() && presenter.isGooglePayAccepted());
        buyTicketsAdapter.updateProductList(entityResult);

        if (registerAnalytics) {
            analyticsPlatformAdapter.productsLoadingCompleted(forceReloaded ? 1 : 0, AnalyticsPlatformsContract.Status.SUCCESS, "");
            forceReloaded = false;
        }
        ClearCartAndAddRepurchasePass(entityResult);
        // If Buy pass is selected from HaCon activity then add a single adult ticket to cart
        AddToCartIfPassIsFromHaConOrDeepLink(entityResult);
        updateCategoryName(entityResult);

    }

    private void ClearCartAndAddRepurchasePass(List<EntityResult> entityResultBuilder) {
        if (!TextUtils.isEmpty(productUUIDOfRepurchasePass)) {
            for (EntityResult entityResult : entityResultBuilder) {
                if (isRepurchaseUUIDMatchingWithAnyProduct(entityResult)) {
                    ArrayList appliedFilterList = new ArrayList();
                    appliedFilterList.add(new AppliedFilter("", "", null, ((BuyTicketsActivity) getActivity()).originUUID, null, "origin", ((BuyTicketsActivity) getActivity()).originShortName, ""));
                    appliedFilterList.add(new AppliedFilter("", "", null, ((BuyTicketsActivity) getActivity()).destinationUUID, null, "destination", ((BuyTicketsActivity) getActivity()).destShortName, ""));
                    entityResult.setAppliedFilters(appliedFilterList);
                    emptyProductSharedPrefsAndAddNewProducts(entityResult);
                    setQuantityAndUpdateShoppingCart(entityResult);
                    goToPaymentScreen(null, AnalyticsPlatformsContract.Screen.REPURCHASE);
                }
            }
            ((BuyTicketsActivity) getActivity()).productUUIDOfRepurchasePass = null;
        }
    }

    private void updateCategoryName(List<EntityResult> entityResult) {
        String category = null;
        if (entityResult != null && entityResult.size() > 0 && entityResult.get(0).getAppliedFilters() != null) {
            Boolean displayLongNameForOd = confHelper.isDisplayLongNameForOd(entityResult.get(0).getOrganization().getUuid());
            String origin = entityResult.get(0).getAppliedFilters(ORIGIN_ATTRIBUTE_NAME, displayLongNameForOd);
            String destination = entityResult.get(0).getAppliedFilters(DESTINATION_ATTRIBUTE_NAME, displayLongNameForOd);

            if (origin != null && destination != null) {
                category = origin + " > " + destination;
            }
            if (category != null) {
                relativeLayoutCategorySelection.setVisibility(View.VISIBLE);
                textViewCategoryName.setText(category);
                linearLayoutCategorySelection.setContentDescription(new StringBuilder().append(getString(R.string.buy_tickets_category_voonly))
                        .append(getCategoryName()).append(" ").append(textViewCategory.getText() != null ? textViewCategory.getText().toString() : ""));
            }
        }

        if (category == null && !TextUtils.isEmpty(getCategoryName())) {
            relativeLayoutCategorySelection.setVisibility(View.VISIBLE);
            textViewCategoryName.setText(getCategoryName());
            linearLayoutCategorySelection.setContentDescription(new StringBuilder().append(getString(R.string.buy_tickets_category_voonly)).append(" ")
                    .append(getCategoryName()).append(textViewCategory.getText() != null ? textViewCategory.getText().toString() : ""));
        }
    }

    private void AddToCartIfPassIsFromHaConOrDeepLink(List<EntityResult> entityResultBuilder) {
        if (!isUUIDEmpty()) {
            boolean hasMatchingProduct = false;
            for (EntityResult entityResult : entityResultBuilder) {
                if (isUUIDMatchingWithAnyProduct(entityResult)) {
                    hasMatchingProduct = true;
                    setQuantityAndUpdateShoppingCart(entityResult);
                    //goToPaymentScreen`(null);
                    if (((BuyTicketsActivity) getActivity()).jwtTokenForDeeplink != null) {
                        emptyProductSharedPrefsAndAddNewProducts(entityResult);
                        goToPaymentScreen(null, AnalyticsPlatformsContract.Screen.EXPRESS_CHECKOUT);
                    } else {
                        goToShoppingCartScreen();
                    }
                    ((BuyTicketsActivity) getActivity()).productUUID = null;
                    break;
                }
            }

            if (((BuyTicketsActivity) getActivity()).jwtTokenForDeeplink != null && !hasMatchingProduct) {
                showErrorDialogIfNoMatchingProductFound();
            }
            ((BuyTicketsActivity) getActivity()).productUUID = null;
            hideLoading();
            showActionBar();

        }
    }

    private void showErrorDialogIfNoMatchingProductFound() {
        ((BuyTicketsActivity) getActivity()).productUUID = null;
        new MaterialDialog.Builder(getContext())
                .title(R.string.popup_error)
                .content(getString(R.string.popup_message_on_no_matching_product))
                .positiveText(R.string.ok)
                .cancelable(false)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> {
                    showFilterScreen();
                    dialog.dismiss();
                })
                .show();
    }

    private Boolean isUUIDEmpty() {
        return TextUtils.isEmpty(((BuyTicketsActivity) getActivity()).productUUID);
    }

    private boolean isRepurchaseUUIDEmpty() {
        return TextUtils.isEmpty(((BuyTicketsActivity) getActivity()).productUUIDOfRepurchasePass);
    }

    private Boolean isUUIDMatchingWithAnyProduct(EntityResult entityResult) {
        return entityResult.getUuid().equals(((BuyTicketsActivity) getActivity()).productUUID);
    }

    private Boolean isRepurchaseUUIDMatchingWithAnyProduct(EntityResult entityResult) {
        return entityResult.getUuid().equals(((BuyTicketsActivity) getActivity()).productUUIDOfRepurchasePass);
    }

    private void setQuantityAndUpdateShoppingCart(EntityResult entityResult) {
        if (((BuyTicketsActivity) getActivity()).jwtTokenForDeeplink != null) {
            entityResult.setQuantity(entityResult.getQuantity() + ((BuyTicketsActivity) getActivity()).quantityFromDeeplink);
        } else {
            entityResult.setQuantity(entityResult.getQuantity() + 1);
        }
        updateShoppingCart(entityResult, true, null);
    }

    @OnClick({R.id.rl_category_selection, R.id.iv_category, R.id.ll_category_selection,
            R.id.tv_category, R.id.tv_category_name})
    public void onCategorySelectionViewClicked() {
        if (((AppliedFilter) getAppliedFilters().get(0)).getFilterUuid() != null) {
            FragmentManager fragmentManager = this.getFragmentManager();
            fragmentManager.popBackStack();
            analyticsPlatformAdapter.changeFilter();
        } else {
            navigateToScheduleActivity();

        }
    }

    @OnClick({R.id.btn_update_filters_no_products, R.id.btn_update_filters_loading_products})
    public void onUpdateFilters() {
        if (((BuyTicketsActivity) getActivity()).jwtTokenForDeeplink != null) {
            callLoadProducts();
        } else if (((BuyTicketsActivity) getActivity()).isFromNativeSchedules) {
            navigateToScheduleActivity();
        } else {
            FragmentManager fragmentManager = this.getFragmentManager();
            fragmentManager.popBackStack();
        }
    }

    public void onCardViewClick(EntityResult entityResult) {
        Intent intent = new Intent(getContext(), ProductDetailsActivity.class);
        intent.putExtra(AppConstants.SELECTED_ENTITY_RESULT, entityResult);
        startActivityForResult(intent, AppConstants.PRODUCT_DETAILS_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.PRODUCT_DETAILS_REQUEST_CODE) {
            if (data != null) {
                entityResult = data.getParcelableExtra(AppConstants.ADDED_ENTITY_RESULT);
                isSharedPrefsUpdated = false;
            }
        }
    }

    public void showAddToCartForDiffOrgErrorDialog(String legacyUuid, final EntityResult entityResult, CardView cardViewProductCard) {
        new MaterialDialog.Builder(getContext())
                .title(R.string.buy_tickets_popup_start_new_order_title)
                .content(getString(R.string.buy_tickets_start_new_order_message_1) + "\"" + confHelper.getChildOrganizationName(legacyUuid) + "\"" + getString(R.string.buy_tickets_start_new_order_message_2))
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .negativeText(R.string.popup_cancel)
                .onPositive((dialog, which) -> {
                    emptyProductSharedPrefsAndAddNewProducts(entityResult);
                    invalidateProductList();
                    if (cardViewProductCard != null)

                        startAddToCartAnimation(cardViewProductCard);

                })
                .onNegative((dialog, which) -> dialog.dismiss())
                .show();
    }

    protected void emptyProductSharedPrefsAndAddNewProducts(EntityResult entityResult) {

        savedEntityResults.clear();

        savedEntityResults.add(entityResult);

        String composedSavedEntityResults = gson.toJson(savedEntityResults);

        sharedPreferences.edit()
                .putString(AppConstants.ENTITY_RESULT_LIST, composedSavedEntityResults)
                .apply();

    }

    public void updateShoppingCart(EntityResult entityResult, boolean invalidate, CardView cardViewProductCard) {
//        analyticsPlatformAdapter.addToCartEvent(entityResult.getSalePrice(), confHelper.getConfigurationPurchaseOptionsCurrencyCode(),
//                entityResult.getOrganization().getUuid(), entityResult.getName(), entityResult.getUuid(), entityResult.getQuantity());
        this.entityResult = entityResult;
        updateSharedPrefs(true, invalidate, cardViewProductCard);
    }

    public void goToPaymentScreen(PaymentMethod paymentMethods, String origin) {
        ((BuyTicketsActivity) getActivity()).onCheckout(paymentMethods, origin);
    }

    public void goToShoppingCartScreen() {
        final Intent intent = new Intent(getActivity(), NewShoppingCartActivity.class);
        intent.putExtra(AcceptPaymentActivity.SOURCE, HEADER_BUTTON);

        intent.putExtra(AppConstants.RELOAD, false);
        intent.putExtra("fareMediumId", "");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        if (((BuyTicketsActivity) getActivity()).searchFromDeeplink) {
            intent.putExtra(AppConstants.SHOPPING_CART_FROM_DEEPLINK, true);
        } else {
            intent.putExtra(AppConstants.SHOPPING_CART_FROM_HACON, true);
        }
        startActivity(intent);
    }

    protected void startAddToCartAnimation(View animatingView) {
        ((BuyTicketsActivity) getActivity()).startAddToCartAnimation(animatingView);
    }


    public Boolean alertAlreadyShowed(final EntityResult entityResult) {
        String entityResultString = sharedPreferences.getString(AppConstants.ENTITY_RESULT_LIST, null);

        if (entityResultString != null) {
            savedEntityResults = gson.fromJson(entityResultString, new TypeToken<List<EntityResult>>() {
            }.getType());

            if (savedEntityResults == null) {
                savedEntityResults = new ArrayList<>();
                return false;
            } else {
                for (int i = 0; i < savedEntityResults.size(); i++) {

                    if (savedEntityResults.get(i).getUuid().equals(entityResult.getUuid())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void showAlerts(View view, final EntityResult entityResult, CardView cardViewProductCard, PaymentMethod paymentMethods) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.buy_tickets_popup_product_alert_title)
                .content(entityResult.getAlertMessage())
                .positiveText(R.string.continuee)
                .negativeText(R.string.receipt_popup_cancel)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        if (entityResult.getSubscription() != null && entityResult.getSubscription().getSubscription_allowed()) {
                            showSubscriptions(view, entityResult, cardViewProductCard, paymentMethods);
                        } else {
                            addToCartOrGoToPayment(entityResult, view, cardViewProductCard, paymentMethods);
                        }
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                        dialog.dismiss();
                    }
                }).show();
    }

    public void showPopUp(final EntityResult entityResult) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.buy_tickets_popup_product_alert_title)
                .content(entityResult.getName() + " " + getString(R.string.buy_tickets_parking_product))
                .positiveText(R.string.ok)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        dialog.dismiss();

                    }
                }).show();
    }


    private void addToCartOrGoToPayment(EntityResult entityResult, View view, CardView cardViewProductCard, PaymentMethod paymentMethods) {
        if (view.getId() == R.id.btn_product_add_to_cart) {
            handleTheCart(entityResult, cardViewProductCard);
        } else {
            emptyProductSharedPrefsAndAddNewProducts(entityResult);
            goToPaymentScreen(paymentMethods, AnalyticsPlatformsContract.Screen.STORE);

        }
    }

    public void showSubscriptions(View view, EntityResult entityResult, CardView cardViewProductCard, PaymentMethod paymentMethods) {
        MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(getActivity())
                .positiveColor(confHelper.getDataThemeAccentColor())
                .negativeColor(confHelper.getDataThemeAccentColor())
                .neutralColor(confHelper.getDataThemeAccentColor())
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        entityResult.setCreateSubscription(true);
                        addToCartOrGoToPayment(entityResult, view, cardViewProductCard, paymentMethods);
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                        entityResult.setCreateSubscription(false);
                        addToCartOrGoToPayment(entityResult, view, cardViewProductCard, paymentMethods);
                        dialog.dismiss();

                    }

                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        super.onNeutral(dialog);
                        dialog.dismiss();
                    }
                });
        if (!TextUtils.isEmpty(fareMediaId)) {
            dialogBuilder.title(R.string.pass_auto_renewals_buy_tickets_popup_title)
                    .content(R.string.pass_auto_renewals_buy_tickets_popup_content)
                    .positiveText(getString(R.string.pass_auto_renewals_buy_tickets_popup_button).toUpperCase())
                    .negativeText(getString(R.string.buy_tickets_popup_subscribe_onetime_button).toUpperCase())
                    .neutralText(getString(R.string.popup_cancel).toUpperCase());
        } else {
            dialogBuilder.title(R.string.buy_tickets_popup_subscribe_title)
                    .content(R.string.buy_tickets_popup_subscribe_content)
                    .positiveText(R.string.buy_tickets_popup_subscribe_button)
                    .negativeText(R.string.buy_tickets_popup_subscribe_onetime_button)
                    .neutralText(R.string.popup_cancel);
        }
        MaterialDialog dialog = dialogBuilder.show();
        if (dialog.getContentView() != null) {
            UseTicketsAccessibility.announceAccessibilityWithoutDoubleTapHint(dialog.getContentView());
        }
    }

    public void handleTheCart(EntityResult entityResult, CardView cardViewProductCard) {
        updateShoppingCart(entityResult, true, cardViewProductCard);
    }

    public void navigateToScheduleActivity() {
        Intent intent = new Intent(getContext(), SchedulesActivity.class);
        intent.setFlags(intent.FLAG_ACTIVITY_NEW_TASK | intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void showAppUpdateDialog() {
        showActionBar();
        swipeRefreshLayoutProductList.setRefreshing(false);
        linearLayoutNoProducts.setVisibility(View.VISIBLE);
        recyclerViewProductList.setVisibility(View.GONE);
        linearLayoutNetworkConnectivityErrorView.setVisibility(View.GONE);
        linearLayoutLoadingError.setVisibility(View.GONE);
        linearLayoutLoadingProductsList.setVisibility(View.GONE);
        super.showAppUpdateDialog();
    }

    @Override
    public void showFilterScreen() {
        ((BuyTicketsActivity) getActivity()).fareId = null;
        ((BuyTicketsActivity) getActivity()).loadStoreFilters();
    }

    public void showActionBar() {
        ((BuyTicketsActivity) getActivity()).getSupportActionBar().show();
    }

    public void hideActionBar() {
        ((BuyTicketsActivity) getActivity()).getSupportActionBar().hide();
    }
}