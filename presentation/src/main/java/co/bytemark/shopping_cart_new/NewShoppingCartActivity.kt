package co.bytemark.shopping_cart_new

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.base.ToolbarActivity
import co.bytemark.buy_tickets.BuyTicketsActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.ConfHelper
import co.bytemark.widgets.dynamicmenu.MenuClickManager
import co.bytemark.widgets.util.Util

class NewShoppingCartActivity : ToolbarActivity() {

    private var isFromHacon = false
    private var isFromDeepLnk = false

    override fun getLayoutRes() = R.layout.activity_new_shopping_cart

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(getString(R.string.screen_title_shopping_cart))
        isFromHacon = intent.getBooleanExtra(AppConstants.SHOPPING_CART_FROM_HACON, false)
        isFromDeepLnk = intent.getBooleanExtra(AppConstants.SHOPPING_CART_FROM_DEEPLINK, false)
    }

    override fun onBackPressed() {
        if (isFromHacon) {
            startHaconTripPlannerScreen()
        } else if (isFromDeepLnk) {
            startStoreScreen()
        } else {
            super.onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun startHaconActivity(intent: Intent) {
        // Clear the task stack when going to HaCon screens because we are adding
        // navigation drawer in HaCon screens using which user can navigate to other screen
        // and if he press back after navigating to other screen user should not
        // see the previous activity. App should just close in this case.
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivityForResult(intent, AppConstants.HAFAS_ACTIVITY_REQUEST_CODE)
    }

    private fun startHaconTripPlannerScreen() {
        val intent = Intent()
        try {
            val cls = Class.forName(AppConstants.HACON_NAVIGATION_CLASS_NAME)
            intent.setClass(this@NewShoppingCartActivity, cls)
            val menuClass = Class.forName(AppConstants.HACON_NAVIGATION_MENU_CLASS_NAME)
            // getMethod takes the method name we want to invoke and what type of argument that method name accepts
            // for us method name is "initialize" and argument type is "Context"
            val method = menuClass.getMethod(AppConstants.HACON_NAVIGATION_METHOD_NAME, Integer::class.java, ConfHelper::class.java, MenuClickManager::class.java, AnalyticsPlatformAdapter::class.java)
            // invoke accepts object name on which this method needs to be invoked and context as parameter.
            // we are passing null because we are invoking static method, object name doesen't matter
            intent.putExtra(AppConstants.HACONT_INTENT_MENU_DRAWER, menuClass.cast(method.invoke(null, Util.navigationDrawerWidth(Util.getActionBarHeight(this@NewShoppingCartActivity)), confHelper, this@NewShoppingCartActivity, analyticsPlatformAdapter)) as Parcelable)
            intent.action = AppConstants.ACTION_CONNECTION
            startHaconActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun startStoreScreen() {
        val intent = Intent(this, BuyTicketsActivity::class.java)
        startActivity(intent)
        finish()
    }
}
