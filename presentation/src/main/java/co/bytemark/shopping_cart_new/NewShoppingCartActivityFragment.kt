package co.bytemark.shopping_cart_new

import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.TextUtils
import android.transition.AutoTransition
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.buy_tickets.BuyProductsAdapter
import co.bytemark.buy_tickets.BuyTicketsActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.model.product_search.entity.EntityResult
import co.bytemark.shopping_cart.AcceptPaymentActivity
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.empty_shopping_cart_view.*
import kotlinx.android.synthetic.main.fragment_shopping_cart_new.*
import javax.inject.Inject

class NewShoppingCartActivityFragment : BaseMvvmFragment(), OnSharedPreferenceChangeListener {

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    private var isIntentForReload: Boolean? = null
    private var fareMediaId: String? = null
    private lateinit var buyProductsAdapter: BuyProductsAdapter
    private var savedEntityResults: List<EntityResult>? = null

    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isIntentForReload = activity?.intent?.getBooleanExtra(AppConstants.RELOAD, false)
        fareMediaId = activity?.intent?.getStringExtra(AppConstants.FARE_MEDIUM_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_shopping_cart_new, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setProductListAdapter()
        getProductEntityListFromSharedPrefs()
        makeAuditoryAnnouncement(view)
        updateTotalDetails()
        setUpBackgroundColors()
        buyTicketsEmptyCartButton.setOnClickListener {
            analyticsPlatformAdapter.viewStoreSelected()
            startBuyTicketsActivity()
        }

        checkoutButton.setOnClickListener {
            startAcceptPaymentActivity()
        }
        if (activity!!.intent.hasExtra(AcceptPaymentActivity.SOURCE)) {
            analyticsPlatformAdapter.cartScreenDisplayed(activity!!.intent.getStringExtra(AcceptPaymentActivity.SOURCE))
        }
    }

    override fun onResume() {
        super.onResume()
        sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        super.onPause()
        updateShoppingCart(savedEntityResults)
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
    }

    private fun makeAuditoryAnnouncement(view: View) {
        view.announceForAccessibility(getString(R.string.buy_tickets_button_name))
        savedEntityResults?.let {
            if (it.isEmpty()) {
                emptyCartTextViews.contentDescription = getString(R.string.shopping_cart_is_empty)
            }
        }
    }

    private fun setProductListAdapter() {
        productListRecyclerView.setHasFixedSize(true)
        productListRecyclerView.layoutManager = LinearLayoutManager(activity?.applicationContext)
        buyProductsAdapter = BuyProductsAdapter(this, !TextUtils.isEmpty(fareMediaId))
        productListRecyclerView.adapter = buyProductsAdapter
        productListRecyclerView.descendantFocusability = ViewGroup.FOCUS_BEFORE_DESCENDANTS
    }

    private fun getProductEntityListFromSharedPrefs() {
        val entityResultString: String? = sharedPreferences.getString(AppConstants.ENTITY_RESULT_LIST, null)
        prepareTransition()
        showEmptyCart()
        entityResultString?.let {
            savedEntityResults = gson.fromJson(entityResultString, object : TypeToken<List<EntityResult?>?>() {}.type)
            if (savedEntityResults?.isNotEmpty() == true) {
                shoppingCartEmptyLayout.visibility = View.GONE
                buyProductsAdapter.updateProductList(savedEntityResults)
                totalRelativeLayout.visibility = View.VISIBLE
            } else {
                emptyCartTextViews.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
                emptyCartTextViews.announceForAccessibility(getString(R.string.shopping_cart_is_empty))
            }
        }
    }

    private fun prepareTransition() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) TransitionManager.beginDelayedTransition(rlShoppingCart, AutoTransition().setDuration(200))
    }

    private fun showEmptyCart() {
        totalRelativeLayout.visibility = View.GONE
        shoppingCartEmptyLayout.visibility = View.VISIBLE
        shoppingCartEmptyLayout.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        emptyCartTextViews.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
    }

    private fun updateTotalDetails() {
        var subTotal = 0
        savedEntityResults?.let {
            for (i in it.indices) {
                subTotal += it[i].salePrice * it[i].quantity
            }
            totalValueTextView.text = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(subTotal)
            totalRelativeLayout.contentDescription = StringBuilder().append(getString(R.string.shopping_cart_total_amount_is_voonly))
                    .append("\n").append(confHelper.getConfigurationPurchaseOptionsCurrencySymbol(subTotal)).append(" ")
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        getProductEntityListFromSharedPrefs()
        updateTotalDetails()
    }

    private fun startBuyTicketsActivity() {
        val intent = Intent(activity, BuyTicketsActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.SHOPPING_CART)
        activity?.intent?.extras?.let { intent.putExtras(it) }
        startActivity(intent)
    }

    private fun startAcceptPaymentActivity() {
        if (getNoOfTicketsInCart() > 20) {
            showShoppingCartFullErrorDialog()
        } else {
            val intent = Intent(activity, AcceptPaymentActivity::class.java)
            intent.putExtra(AppConstants.RELOAD, isIntentForReload)
            intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.SHOPPING_CART)
            intent.putExtra(AppConstants.FARE_MEDIUM_ID, fareMediaId)
            startActivity(intent)
        }
    }

    fun updateShoppingCart(entityResults: List<EntityResult>?) {
        savedEntityResults = entityResults
        sharedPreferences.edit()
                .putString(AppConstants.ENTITY_RESULT_LIST, gson.toJson(entityResults))
                .apply()
    }

    private fun setUpBackgroundColors() {
        shoppingCartEmptyLayout.backgroundTintList = ColorStateList.valueOf(confHelper.backgroundThemeBackgroundColor)
        emptyCartImageView.imageTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        checkoutButton.backgroundTintList = ColorStateList.valueOf(confHelper.accentThemeBacgroundColor)
    }

    private fun getNoOfTicketsInCart(): Int {
        return savedEntityResults?.map { it.quantity }?.sum() ?: 0
    }

    fun updateEntityResultList(entityResults: List<EntityResult>) {
        savedEntityResults = entityResults
        updateTotalDetails()
    }

    private fun showShoppingCartFullErrorDialog() {
        activity?.let {
            MaterialDialog.Builder(it)
                    .title(R.string.buy_tickets_popup_order_limit_reached)
                    .content(R.string.buy_tickets_popup_order_limit_reached_msg)
                    .positiveText(R.string.ok)
                    .positiveColor(confHelper.dataThemeAccentColor)
                    .onPositive { dialog: MaterialDialog, dialogAction: DialogAction? -> dialog.dismiss() }
                    .show()
        }
    }
}
