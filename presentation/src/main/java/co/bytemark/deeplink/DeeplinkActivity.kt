package co.bytemark.deeplink

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.buy_tickets.BuyTicketsActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.use_tickets.UseTicketsActivity
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.dynamiclinks.PendingDynamicLinkData
import timber.log.Timber

class DeeplinkActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handleDeepLink()
    }

    private fun handleDeepLink() {
        FirebaseDynamicLinks.getInstance()
            .getDynamicLink(intent)
            .addOnSuccessListener(this) { pendingDynamicLinkData: PendingDynamicLinkData? ->
                var deepLink: Uri? = null
                if (pendingDynamicLinkData != null) {
                    deepLink = pendingDynamicLinkData.link
                }
                if (deepLink != null) {
                    var intent : Intent
                    if (deepLink.path == "/applink/credit") {
                        intent = Intent(this, UseTicketsActivity::class.java)
                        intent.putExtra(
                            AppConstants.EXTRA_ORIGIN,
                            AnalyticsPlatformsContract.Screen.USE_TICKETS
                        )
                    } else {
                        intent = Intent(this, BuyTicketsActivity::class.java)
                    }
                    intent.putExtra(
                        AppConstants.DEEPLINK_URL,
                        deepLink.toString()
                    )
                    startActivity(intent)
                    finish()
                }
            }
            .addOnFailureListener(
                this
            ) { e: Exception -> Timber.w("getDynamicLink:onFailure: $e") }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null) {
            handleDeepLink()
        }
    }
}