package co.bytemark.schedules

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.sdk.model.common.Action

class SchedulesActivity : MasterActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setNavigationViewCheckedItem(Action.NATIVE_SCHEDULE)
    }

    override fun getLayoutRes(): Int = R.layout.activity_schedules
}