package co.bytemark.schedules

import androidx.lifecycle.MutableLiveData
import co.bytemark.domain.model.common.Result
import co.bytemark.CustomerMobileApp
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.interactor.schedules.*
import co.bytemark.gtfs.Agency
import co.bytemark.gtfs.DataObjects.Schedule
import co.bytemark.gtfs.Stop
import co.bytemark.sdk.schedules.ScheduleItem
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class SchedulesViewModel @Inject constructor(
        private val getOriginStops: GetOriginStops,
        private val getDestinationStops: GetDestinationStops,
        private val getSchedules: GetSchedules,
        private val getAgenciesUseCase: GetAgenciesUseCase) :
        BaseViewModel() {

    init {
        CustomerMobileApp.component.inject(this)
    }

    val originListLiveData by lazy { MutableLiveData<List<Stop>>() }

    val stopListLiveData by lazy { MutableLiveData<MutableList<Schedule>>() }

    val agenciesLiveData by lazy { MutableLiveData<List<Agency>>() }

    val destinationsLiveData by lazy { MutableLiveData<List<Stop>>() }

    val scheduleLiveData by lazy { MutableLiveData<List<ScheduleItem>>() }

    fun getOriginList() = uiScope.launch {
        when (val result = getOriginStops(UseCase.EmptyRequestValues())) {
            is Result.Success -> originListLiveData.value = result.data
        }
    }

    fun getStopTimes(origin: String, destination: String, date: Date?) = uiScope.launch {
        when (val result = getSchedules(GTFSRequestValues(origin, destination, date))) {
            is Result.Success -> stopListLiveData.value = result.data
        }
    }

    fun getAgencies() = uiScope.launch {
        when (val result = getAgenciesUseCase(UseCase.EmptyRequestValues())) {
            is Result.Success -> agenciesLiveData.value = result.data
        }
    }

    fun getDestinations(stop: Stop) = uiScope.launch {
        when (val result = getDestinationStops(GTFSRequestValues(stop.identifier))) {
            is Result.Success -> destinationsLiveData.value = result.data
        }
    }

    fun loadScheduleItems(scheduleItem: List<ScheduleItem>) = uiScope.launch {
        scheduleLiveData.value = scheduleItem
    }

}