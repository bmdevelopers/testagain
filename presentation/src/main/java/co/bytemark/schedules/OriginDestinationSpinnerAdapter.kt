package co.bytemark.schedules

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.gtfs.Stop
import co.bytemark.helpers.ConfHelper
import java.util.*
import javax.inject.Inject

class OriginDestinationSpinnerAdapter(activity: Activity, origin: Boolean, stops: List<Stop>?) : BaseAdapter() {

    private val origin: Boolean
    private val activity: Activity
    private val stopList: MutableList<Stop> = ArrayList()

    init {
        component.inject(this)
        this.activity = activity
        stopList.clear()
        stopList.addAll(stops!!)
        this.origin = origin
        stopList.add(Stop())
    }

    @Inject
    lateinit var confHelper: ConfHelper

    override fun getCount(): Int = stopList.size - 1

    override fun getItem(position: Int): Any = stopList[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var convertView: View? = convertView
        val holder: ViewHolder
        if (convertView == null) {
            val inflater = activity.layoutInflater
            convertView = inflater.inflate(R.layout.text_view, parent, false)
            holder = ViewHolder()
            holder.venueLabel = convertView.findViewById<View>(R.id.textViewOriginDestinationItem) as TextView
            holder.view = convertView.findViewById(R.id.view) as View
            convertView.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }
        if (position == count) {
            if (origin) {
                holder.venueLabel?.hint = activity.resources.getString(R.string.use_tickets_origin)
            } else {
                holder.venueLabel?.hint = activity.resources.getString(R.string.use_tickets_destination)
            }
        }
        if (stopList[position] == null || stopList[position].equals("")) {
            if (origin) holder.venueLabel?.text = activity.getString(R.string.use_tickets_origin) else holder.venueLabel?.text = activity.getString(R.string.use_tickets_destination)
        } else {
            holder.venueLabel?.text = stopList[position].stopName
        }
        holder.view?.visibility = View.VISIBLE
        holder.view?.setBackgroundColor(confHelper.collectionThemeBackgroundColor)
        return convertView
    }

    internal class ViewHolder {
        var venueLabel: TextView? = null
        var view: View? = null
    }

}