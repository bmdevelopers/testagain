package co.bytemark.schedules

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import co.bytemark.R
import co.bytemark.gtfs.Agency
import co.bytemark.gtfs.DataObjects.Schedule
import co.bytemark.gtfs.Stop
import co.bytemark.gtfs.StopTime
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.schedules.ScheduleItem
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlinx.android.synthetic.main.schedules_adapter_row.view.*

class ScheduleItemsRecyclerview(private val stopTimes: MutableList<StopTime>?,
                                private val schedules: MutableList<Schedule>?,
                                private val scheduleItems: List<ScheduleItem>?,
                                private val stopOrigin: Stop?,
                                private val stopDestination: Stop?,
                                private val agencies: List<Agency>?
) : RecyclerView.Adapter<ScheduleItemsRecyclerview.ViewHolder>() {

    init {
        if (stopTimes != null) Collections.sort(stopTimes, StopTimeComparator()) else Collections.sort(schedules, ScheduleComparator())
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.schedules_adapter_row, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.itemView) {

            val departureTime: String = getStopTimeZoneTime(schedules?.get(position), stopTimes?.get(position), true)
            val arrivalTime: String = getStopTimeZoneTime(schedules?.get(position), stopTimes?.get(position), false)
            val trainNo: String? = stopTimes?.get(position)?.tripId
                    ?: schedules?.get(position)?.tripId

            val textViewList: MutableList<TextView?> = mutableListOf(textViewTrain, textViewDeparts, textViewArrivals, textViewTrips)
            for (i in textViewList.indices) {
                if (scheduleItems != null && scheduleItems.size - 1 >= i) {
                    when {
                        scheduleItems[i].key.equals("\$trip_short_name", ignoreCase = true) -> {
                            textViewList[i]?.text = trainNo
                        }
                        scheduleItems[i].key.equals("\$trip_departure_time", ignoreCase = true) -> {
                            textViewList[i]?.text = dateFormatter(departureTime)
                        }
                        scheduleItems[i].key.equals("\$trip_arrival_time", ignoreCase = true) -> {
                            textViewList[i]?.text = dateFormatter(arrivalTime)
                        }
                        scheduleItems[i].key.equals("\$trip_time", ignoreCase = true) -> {
                            textViewList[i]?.text = getTripTime(arrivalTime, departureTime)
                        }
                    }
                } else {
                    textViewList[i]?.visibility = View.GONE
                }
            }
        }
    }

    override fun getItemCount(): Int = stopTimes?.size ?: schedules!!.size

    private fun getStopTimeZoneTime(schedule: Schedule?, stopTime: StopTime?, isDeparture: Boolean): String {
        val agencyId = schedule?.agencyId ?: stopTime?.agencyId
        val time: String = if (isDeparture) {
            if (schedule != null) schedule.tripDepartureTime else stopTime!!.departureTime
        } else {
            if (schedule != null) schedule.tripArrivalTime else stopTime!!.arrivalTime
        }
        val agency = getAgency(agencyId) ?: return time
        val agencyZoneTime: DateFormat = SimpleDateFormat("HH:mm:ss", Locale.ROOT)
        agencyZoneTime.timeZone = TimeZone.getTimeZone(agency.agency_timezone)
        val stopZoneTime: DateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        stopZoneTime.timeZone = TimeZone.getTimeZone(if (isDeparture) stopOrigin?.stopTimezone else stopDestination?.stopTimezone)
        return try {
            stopZoneTime.format(agencyZoneTime.parse(time))
        } catch (e: ParseException) {
            e.printStackTrace()
            time
        }
    }

    private fun getAgency(agencyId: String?): Agency? {
        agencies?.let {
            for (agency in agencies) {
                if (agency.agency_id == agencyId) return agency
            }
        }
        return null
    }

    private fun dateFormatter(time: String): String {
        val sdf1 = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        val sdf2 = SimpleDateFormat("h:mm a", Locale.getDefault())
        var date: Date? = Date()
        try {
            date = sdf1.parse(time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return sdf2.format(date)
    }

    private fun getTripTime(arrives: String, departs: String): String {
        var tripTime = ""
        try {
            val sdfArrival = SimpleDateFormat("HH:mm:ss", Locale.ROOT)
            if (getAgency(stopDestination?.agenctId) != null) sdfArrival.timeZone = TimeZone.getTimeZone(stopDestination?.stopTimezone)
            val sdDeparture = SimpleDateFormat("HH:mm:ss", Locale.ROOT)
            if (getAgency(stopDestination?.agenctId) != null) sdDeparture.timeZone = TimeZone.getTimeZone(stopOrigin?.stopTimezone)
            val arrivesDate = Calendar.getInstance()
            val departsDate = Calendar.getInstance()
            arrivesDate.time = sdfArrival.parse(arrives)
            departsDate.time = sdDeparture.parse(departs)
            tripTime = BytemarkSDK.SDKUtility.getElapsedTime(getTimeDifferenceInMillis(arrivesDate.timeInMillis, departsDate.timeInMillis), false)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return tripTime
    }

    private fun getTimeDifferenceInMillis(arrivalTimeInMillis: Long, departuretTmeInMillis: Long): Long {
        val timeDiff = arrivalTimeInMillis - departuretTmeInMillis
        return if (timeDiff > -1) timeDiff else arrivalTimeInMillis + +86400000 - departuretTmeInMillis
        //86400000 is the miiliseconds in a day. add to make it next days time
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    private inner class StopTimeComparator : Comparator<StopTime> {
        override fun compare(lhs: StopTime, rhs: StopTime): Int {
            val parsedTime = parseTime(lhs.departureTime, rhs.departureTime)
            return if (parsedTime.second > parsedTime.first) -1 else if (parsedTime.first == parsedTime.second) 0 else 1
        }
    }

    private inner class ScheduleComparator : Comparator<Schedule> {
        override fun compare(lhs: Schedule, rhs: Schedule): Int {
            val parsedTime = parseTime(lhs.tripDepartureTime, rhs.tripDepartureTime)
            return if (parsedTime.second > parsedTime.first) -1 else if (parsedTime.first == parsedTime.second) 0 else 1
        }
    }

    fun clear() {
        stopTimes?.clear()
        schedules?.clear()
        notifyDataSetChanged()
    }

    private fun parseTime(lhs: String, rhs: String?): Pair<Long, Long> {
        val sdf = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        var lDate = Date()
        var rDate = Date()
        try {
            lDate = sdf.parse(lhs)
            rDate = sdf.parse(rhs)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return Pair(lDate.time, rDate.time)
    }
}