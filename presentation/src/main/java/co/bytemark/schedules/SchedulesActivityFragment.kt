package co.bytemark.schedules

import android.app.Activity
import androidx.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.TextView
import co.bytemark.CustomerMobileApp
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.buy_tickets.BuyTicketsActivity
import co.bytemark.formly.adapterdelegates.DatePickerFragment
import co.bytemark.gtfs.Agency
import co.bytemark.gtfs.DataObjects.Schedule
import co.bytemark.gtfs.Stop
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.schedules.ScheduleItem
import co.bytemark.widgets.util.getViewModel
import kotlinx.android.synthetic.main.fragment_schedules.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class SchedulesActivityFragment : BaseMvvmFragment() {

    companion object {
        const val DATE_FORMAT = "EEEE, MMM. dd, yyyy"
    }

    override fun onInject() {
        component.inject(this)
    }

    private lateinit var scheduleItemsRecyclerview: ScheduleItemsRecyclerview

    private var scheduleItems: List<ScheduleItem>? = ArrayList()

    lateinit var viewModel: SchedulesViewModel

    private var isStopTimesForToday = false
    private lateinit var date: Date
    private lateinit var stop: Stop
    private lateinit var stopOrigin: Stop
    private lateinit var stopDestination: Stop
    private lateinit var stopHolder: Stop
    private var originVenues: List<Stop>? = ArrayList()
    private var destinationVenues: List<Stop>? = ArrayList()
    private var agencies: List<Agency>? = ArrayList()
    private lateinit var originAdapter: OriginDestinationSpinnerAdapter
    private lateinit var destinationAdapter: OriginDestinationSpinnerAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_schedules, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = getViewModel(CustomerMobileApp.appComponent.schedulesViewModel)
        setUpOriginDestinationListeners()
        onDatePickerButtonClickListener()
        viewModel.getAgencies()
        setDate(Date())
        setBuyButtonEnabled(false)
        onBuyClick()
        onSwapImageClick()
        observeLiveData(view.context)
    }

    private fun observeLiveData(context: Context) {
        viewModel.originListLiveData.observe(this, Observer { displayOriginStops(it, context) })
        viewModel.stopListLiveData.observe(this, Observer { displayStopTimes(it) })
        viewModel.agenciesLiveData.observe(this, Observer { setAgencies(it) })
        viewModel.destinationsLiveData.observe(this, Observer { displayDestinationStops(it, context) })
        viewModel.scheduleLiveData.observe(this, Observer { setSchedulesItem(it) })
    }

    private fun setupSchedulesRecyclerView() {
        recyclerView?.layoutManager = LinearLayoutManager(context)
        recyclerView?.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    }

    private fun setAgencies(agencies: List<Agency>?) {
        this.agencies = agencies
    }

    private fun setSchedulesItem(schedulesItem: List<ScheduleItem>?) {
        scheduleItems = schedulesItem
        val textViewList: MutableList<TextView?> = mutableListOf(
            header?.findViewById(R.id.textViewTrain),
            header?.findViewById(R.id.textViewDeparts),
            header?.findViewById(R.id.textViewArrivals),
            header?.findViewById(R.id.textViewTrips)
        )
        for (i in textViewList.indices) {
            if (scheduleItems != null && scheduleItems!!.size - 1 >= i) {
                textViewList[i]?.text = schedulesItem?.get(i)?.name
            } else {
                textViewList[i]?.visibility = View.GONE
            }
        }
        header.setBackgroundColor(confHelper.collectionThemeBackgroundColor)
    }

    private fun displayOriginStops(venues: List<Stop>?, context: Context) {
        originVenues = venues
        originAdapter = OriginDestinationSpinnerAdapter(context as Activity, true, venues)
        spinnerOrigin?.adapter = originAdapter
        if (BytemarkSDK.SDKUtility.getorigin() == "") {
            spinnerOrigin?.setSelection(originAdapter.count)
            val list: MutableList<String?> = mutableListOf()
            list.add("")
            val destinationAdapter: ArrayAdapter<*> = ArrayAdapter<String?>(context, R.layout.spinner_tv, list)
            destinationAdapter.setDropDownViewResource(R.layout.spinner_tv)
            spinnerDestination?.adapter = destinationAdapter
            spinnerDestination?.isEnabled = false
            BytemarkSDK.SDKUtility.setDestination("")
        } else {
            val position = getPosition(venues, BytemarkSDK.SDKUtility.getorigin())
            BytemarkSDK.SDKUtility.setorigin("")
            spinnerOrigin?.setSelection(position)
        }
    }

    private fun displayDestinationStops(venues: List<Stop>?, context: Context) {
        destinationVenues = venues
        destinationAdapter = OriginDestinationSpinnerAdapter(context as Activity, false, destinationVenues)
        spinnerDestination?.adapter = destinationAdapter
        val position: Int
        if (::stopHolder.isInitialized && stopHolder.identifier != null) {
            position = getPosition(destinationVenues, stopHolder.identifier)
            spinnerDestination?.setSelection(position)
        } else if (BytemarkSDK.SDKUtility.getdestination() != "") {
            position = getPosition(destinationVenues, BytemarkSDK.SDKUtility.getdestination())
            BytemarkSDK.SDKUtility.setDestination("")
            spinnerDestination?.setSelection(position)
        } else {
            spinnerDestination?.setSelection(destinationAdapter.count)
        }
    }

    private fun displayStopTimes(stopTimes: MutableList<Schedule>?) {
        scheduleItemsRecyclerview = if (scheduleItems != null) {
            ScheduleItemsRecyclerview(null, stopTimes, scheduleItems, stopOrigin, stopDestination, agencies)
        } else {
            ScheduleItemsRecyclerview(null, stopTimes, null, stopOrigin, stopDestination, agencies)
        }
        recyclerView?.adapter = scheduleItemsRecyclerview
        if (isStopTimesForToday) {
            recyclerView?.layoutManager?.scrollToPosition(getPositionOfCurrentTimeSchedule(stopTimes))
        }
        isStopTimesForToday = false
        if (stopTimes == null || stopTimes.size <= 0) {
            recyclerView?.visibility = View.GONE
            tvNoDataAvailable?.visibility = View.VISIBLE
        } else {
            recyclerView?.visibility = View.VISIBLE
            tvNoDataAvailable?.visibility = View.GONE
        }
    }

    private fun onBuyClick() {
        buttonBuy?.setOnClickListener {
            if (::stopOrigin.isInitialized && ::stopDestination.isInitialized && ::date.isInitialized) {
                with(Intent(activity, BuyTicketsActivity::class.java)) {
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    putExtra(AppConstants.GTFS_ORIGIN, stopOrigin.identifier)
                    putExtra(AppConstants.GTFS_DESTINATION, stopDestination.identifier)
                    putExtra(AppConstants.CATEGORY_NAME, stopOrigin.stopName + " / " + stopDestination.stopName)
                    putExtra(AppConstants.FROM_NATIVE_SCHEDULE, true)
                    activity?.startActivity(this)
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (::stopOrigin.isInitialized && stopOrigin.identifier != null) {
            BytemarkSDK.SDKUtility.setorigin(stopOrigin.identifier)
        }
        if (::stopDestination.isInitialized && stopDestination.identifier != null) {
            BytemarkSDK.SDKUtility.setDestination(stopDestination.identifier)
        }
    }

    override fun onResume() {
        viewModel.getOriginList()
        setupSchedulesRecyclerView()
        viewModel.loadScheduleItems(confHelper.scheduleItems)
        super.onResume()
    }

    private fun setDate(date: Date) {
        this.date = date
        buttonSchedulesDate?.text = makePrettyDate(date)
        if (DateUtils.isToday(date.time)) {
            isStopTimesForToday = true
        }
        if (::stopOrigin.isInitialized && stopOrigin.identifier != null && ::stopDestination.isInitialized && stopDestination.identifier != null) {
            viewModel.getStopTimes(stopOrigin.identifier, stopDestination.identifier, date)
        }
    }

    private fun onDatePickerButtonClickListener() {
        buttonSchedulesDate.setOnClickListener {
            val dialogFragment = DatePickerFragment()
            dialogFragment.setListener(object : DatePickerFragment.OnSetDateListener {
                override fun setDate(date: Date?) {
                    date?.let { setDate(it) }
                }
            })
            fragmentManager?.let { it1 -> dialogFragment.show(it1, "datePicker") }
        }
    }

    private fun makePrettyDate(dateSelected: Date): String {
        val simpleDate = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
        return simpleDate.format(dateSelected)
    }

    private fun setUpOriginDestinationListeners() {
        spinnerOrigin?.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                stopOrigin = parent.getItemAtPosition(position) as Stop
                if (::stopOrigin.isInitialized && !stopOrigin.equals("") && stopOrigin.identifier != null) {
                    spinnerDestination?.isEnabled = true
                    viewModel.getDestinations(stopOrigin)
                }
                if (::scheduleItemsRecyclerview.isInitialized) scheduleItemsRecyclerview.clear()
                view.findViewById<View>(R.id.view).visibility = View.GONE
                tvNoDataAvailable?.visibility = View.GONE
                recyclerView?.visibility = View.VISIBLE
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                parent.setSelection(originAdapter.count)
            }
        }
        spinnerDestination?.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (parent.getItemAtPosition(position) != "") {
                    stop = parent.getItemAtPosition(position) as Stop
                    if (::stop.isInitialized && !stop.equals("")) {
                        stopDestination = parent.getItemAtPosition(position) as Stop
                        if (::stopOrigin.isInitialized && !stopOrigin.equals("") && ::stopDestination.isInitialized && stopDestination.identifier != null) {
                            viewModel.getStopTimes(stopOrigin.identifier, stopDestination.identifier, date)
                            setBuyButtonEnabled(true)
                        }
                    }
                    if (DateUtils.isToday(date.time)) {
                        isStopTimesForToday = true
                    }
                }
                if (view.findViewById<View?>(R.id.view) != null) {
                    view.findViewById<View>(R.id.view).visibility = View.GONE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                parent.setSelection(originAdapter.count)
            }
        }
    }

    private fun onSwapImageClick() {
        imageViewSwap?.setOnClickListener {
            stopHolder = stopOrigin
            val destinationPosition: Int = getPosition(originVenues, stopDestination.identifier)
            originAdapter = OriginDestinationSpinnerAdapter(context as Activity, true, originVenues)
            spinnerOrigin?.adapter = originAdapter
            spinnerOrigin?.setSelection(destinationPosition)
        }
    }

    private fun getPosition(stops: List<Stop>?, identifier: String): Int {
        stops?.let {
            for (i in stops.indices) {
                if (stops[i].identifier != null) {
                    if (stops[i].identifier == identifier) {
                        return i
                    }
                }
            }
        }
        return -1
    }

    private fun getPositionOfCurrentTimeSchedule(schedules: MutableList<Schedule>?): Int {
        schedules?.let {
            for (schedule in schedules) {
                val dateFormat = SimpleDateFormat("hh:mm:ss", Locale.getDefault())
                var arrivalTime: Date? = null
                try {
                    arrivalTime = dateFormat.parse(schedule.tripDepartureTime)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
                if (arrivalTime != null) {
                    val currentTime = Date()
                    val c1 = Calendar.getInstance()
                    c1.time = currentTime
                    val c2 = Calendar.getInstance()
                    c2.time = arrivalTime
                    val t1 = c1[Calendar.HOUR_OF_DAY] * 60 * 60 + c1[Calendar.MINUTE] * 60 + c1[Calendar.SECOND].toLong()
                    val t2 = c2[Calendar.HOUR_OF_DAY] * 60 * 60 + c2[Calendar.MINUTE] * 60 + c2[Calendar.SECOND].toLong()
                    if (t2 > t1) {
                        return schedules.indexOf(schedule)
                    }
                }
            }
        }
        return 0
    }

    private fun setBuyButtonEnabled(isEnabled: Boolean) {
        if (!isEnabled) {
            val alphaComponent = ColorUtils.setAlphaComponent(confHelper.accentThemeBacgroundColor, 26)
            buttonBuy?.backgroundTintList = ColorStateList.valueOf(alphaComponent)
        } else {
            buttonBuy?.backgroundTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        }
        buttonBuy?.isEnabled = isEnabled
    }

}