package co.bytemark.nativeappsupport

import android.os.Bundle
import android.transition.Slide
import co.bytemark.R
import co.bytemark.base.ToolbarActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.util.replaceFragment

class NativeAppSupportActivity : ToolbarActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(intent.getStringExtra(AppConstants.TITLE))
        replaceFragment(NativeAppSupportFragment.newInstance().apply { enterTransition = Slide() }, R.id.container)
    }

    override fun getLayoutRes() = R.layout.activity_native_app_support
}
