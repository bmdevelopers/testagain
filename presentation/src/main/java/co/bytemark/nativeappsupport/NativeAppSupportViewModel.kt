package co.bytemark.nativeappsupport

import androidx.lifecycle.MutableLiveData
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.native_app_support.NativeAppSupportRequestValues
import co.bytemark.domain.interactor.native_app_support.NativeAppSupportUseCase
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.common.Result
import co.bytemark.helpers.ConfHelper
import kotlinx.coroutines.launch
import javax.inject.Inject

class NativeAppSupportViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var confHelper: ConfHelper

    @Inject
    lateinit var nativeAppSupportUseCase: NativeAppSupportUseCase

    init {
        CustomerMobileApp.component.inject(this)
    }

    val nativeAppSupportFormsLiveData by lazy {
        MutableLiveData<MutableList<Formly>>()
    }


    val sentAppSupportMailLiveData by lazy {
        MutableLiveData<Boolean>()
    }

    val displayLiveData by lazy {
        MutableLiveData<Display>()
    }

    fun getForms() {
        nativeAppSupportFormsLiveData.value = confHelper.nativeAppSupportForms
    }

    fun sendAppSupportMail(params: MutableMap<String, String>) = uiScope.launch {
        displayLiveData.value = Display.EmptyState.Loading(R.drawable.notification_material, R.string.loading)
        when (val result = nativeAppSupportUseCase(NativeAppSupportRequestValues(params))) {
            is Result.Success -> {
                displayLiveData.value = Display.EmptyState.ShowContent()
                sentAppSupportMailLiveData.value = true
            }

            is Result.Failure -> {
                displayLiveData.value = Display.EmptyState.ShowContent()
                errorLiveData.value = result.bmError.first()
            }

        }
    }
}