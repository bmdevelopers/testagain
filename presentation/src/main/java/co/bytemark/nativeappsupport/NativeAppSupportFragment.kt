package co.bytemark.nativeappsupport

import androidx.lifecycle.Observer
import android.os.Bundle
import android.os.Handler
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import androidx.recyclerview.widget.LinearLayoutManager
import android.transition.TransitionManager
import android.util.Pair
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.common.Display
import co.bytemark.formly.adapter.FormlyAdapter
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.UserInfoDatabaseManager
import co.bytemark.widgets.transition.SuperAutoTransition
import co.bytemark.widgets.util.Util
import co.bytemark.widgets.util.getViewModel
import kotlinx.android.synthetic.main.fragment_native_app_support.*
import kotlinx.android.synthetic.main.fragment_native_app_support.root_layout
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject

class NativeAppSupportFragment : BaseMvvmFragment() {

    private val formlyMap = mutableMapOf<String, String>()

    private lateinit var formlyAdapter: FormlyAdapter

    private val subs = CompositeSubscription()

    @Inject
    lateinit var nativeAppSupportViewModel: NativeAppSupportViewModel

    companion object {
        fun newInstance() = NativeAppSupportFragment()
    }

    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_native_app_support, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nativeAppSupportViewModel = getViewModel(CustomerMobileApp.appComponent.nativeAppSupportViewModel)
        observerLiveData()

        appSupportList?.layoutManager = LinearLayoutManager(context)
        formlyAdapter = FormlyAdapter(appSupportList, fragmentManager)

        subs.add(formlyAdapter.buttonClicks()?.subscribe { formly: Formly -> onButtonClicked(formly) })
        subs.add(formlyAdapter.textChanges().subscribe { textChange: Pair<Formly, String> -> updateFormlyModel(textChange) })

        checkMarkAppSupport?.setMaterialWidth(Util.dpToPx(2.0).toFloat())
        appSupportList?.adapter = formlyAdapter

        nativeAppSupportViewModel.getForms()
    }

    private fun observerLiveData() {
        observeFormLiveData()
        observeNativeAppSupportMailLiveData()
        observeErrorLiveData()
        observeDisplayLiveData()
    }

    private fun observeFormLiveData() {
        nativeAppSupportViewModel.nativeAppSupportFormsLiveData.observe(this, Observer {
            it?.let {
                formlyAdapter.setFormlyList(it)
                appSupportList?.post { startPostponedEnterTransition() }
                Handler().postDelayed({
                    formlyAdapter.disableEmojiInput()
                    formlyAdapter.setValue(Formly.EMAIL_ADDRESS_KEY, UserInfoDatabaseManager.getInstance(context).userFromDB?.email)
                },1000)
            }
        })
    }

    private fun observeNativeAppSupportMailLiveData() {
        nativeAppSupportViewModel.sentAppSupportMailLiveData.observe(this, Observer {
            it?.let {
                onAppSupportEmailSentSuccessfully()
            }
        })
    }

    private fun observeErrorLiveData() {
        nativeAppSupportViewModel.errorLiveData.observe(this, Observer {
            it?.let { handleError(it) }
        })
    }

    private fun observeDisplayLiveData() {
        nativeAppSupportViewModel.displayLiveData.observe(this, Observer {
            when (it) {
                is Display.EmptyState.ShowContent -> {
                    formlyAdapter.setLoading(Formly.NATIVE_APP_SUPPORT, false)
                }

                is Display.EmptyState.Loading -> {
                    hideKeyboard()
                    formlyAdapter.setLoading(Formly.NATIVE_APP_SUPPORT, true)
                }
            }
        })
    }

    private fun onButtonClicked(formly: Formly) {
        hideKeyboard()
        when (formly.key) {
            Formly.NATIVE_APP_SUPPORT -> {
                formlyAdapter.enableValidation()
                if (formlyAdapter.isValid()) {
                    if (isOnline()) {
                        nativeAppSupportViewModel.sendAppSupportMail(formlyMap)
                    } else {
                        connectionErrorDialog()
                    }
                }
            }
        }
    }

    private fun onAppSupportEmailSentSuccessfully() {
        val hideContent = SuperAutoTransition()
        hideContent.duration = 300
        hideContent.interpolator = LinearOutSlowInInterpolator()
        hideContent.doOnEnd(Runnable {
            val superAutoTransition = SuperAutoTransition()
            superAutoTransition.duration = 300
            superAutoTransition.doOnEnd(Runnable {
                checkMarkAppSupport.isSelected = true
                checkMarkAppSupport?.postDelayed({
                    if (activity != null && activity?.isFinishing == false) {
                        activity?.finish()
                    }
                }, 2000)
            })
            appSupportSuccess.transitionName = arguments?.getString(AppConstants.TITLE)
            TransitionManager.beginDelayedTransition(root_layout, superAutoTransition)
            if (view != null) {
                view?.announceForAccessibility(appSupportSuccess?.text)
            }
            appSupportSuccess?.visibility = View.VISIBLE
            checkMarkAppSupport?.visibility = View.VISIBLE
        })
        TransitionManager.beginDelayedTransition(root_layout, hideContent)
        appSupportList?.visibility = View.GONE
        nativeAppSupportListContainer?.gravity = Gravity.CENTER
    }

    private fun updateFormlyModel(value: Pair<Formly, String>) {
        formlyMap[value.first.key] = value.second.trim()
    }
}
