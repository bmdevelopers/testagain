package co.bytemark.independenttestcases;

import java.util.ArrayList;
import java.util.Calendar;

import co.bytemark.sdk.ActivationRestrictionException;

import static co.bytemark.sdk.PassActivationRestrictionCalculator.isActivationRestricted;

public class HasActivationRestrictionExceptions {

    public static boolean hasActivationRestrictionExceptions(Calendar currentTime,
                                                             ArrayList<ActivationRestrictionException> activationRestrictionExceptions) {
        if (activationRestrictionExceptions != null &&
                !activationRestrictionExceptions.isEmpty()) {

            for (ActivationRestrictionException exception : activationRestrictionExceptions) {
                if (isActivationRestricted(currentTime, exception)) {
                    return true;
                }
            }
        }
        return false;
    }
}
