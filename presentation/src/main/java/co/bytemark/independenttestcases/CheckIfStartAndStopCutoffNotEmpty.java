package co.bytemark.independenttestcases;

import co.bytemark.sdk.ActivationRestrictionV2;

public class CheckIfStartAndStopCutoffNotEmpty {

    public static boolean checkIfStartCutoffNotEmpty(ActivationRestrictionV2 activationRestrictionV2) {
        return activationRestrictionV2.getStartCutoffYear() > -1 ||
                activationRestrictionV2.getStartCutoffMonth() > -1 ||
                activationRestrictionV2.getStartCutoffDay() > -1 ||
                activationRestrictionV2.getStartCutoffDayOfWeek() > -1 ||
                activationRestrictionV2.getStartCutoffHour() > -1 ||
                activationRestrictionV2.getStartCutoffMinute() > -1 ||
                activationRestrictionV2.getStartCutoffSecond() > -1;
    }

    public static boolean checkIfStopCutoffNotEmpty(ActivationRestrictionV2 activationRestrictionV2) {
        return activationRestrictionV2.getStopCutoffYear() > -1 ||
                activationRestrictionV2.getStopCutoffMonth() > -1 ||
                activationRestrictionV2.getStopCutoffDay() > -1 ||
                activationRestrictionV2.getStopCutoffDayOfWeek() > -1 ||
                activationRestrictionV2.getStopCutoffHour() > -1 ||
                activationRestrictionV2.getStopCutoffMinute() > -1 ||
                activationRestrictionV2.getStopCutoffSecond() > -1;
    }
}
