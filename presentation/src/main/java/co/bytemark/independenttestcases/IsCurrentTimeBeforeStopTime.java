package co.bytemark.independenttestcases;

import java.util.Calendar;

import co.bytemark.sdk.ActivationRestrictionV2;

public class IsCurrentTimeBeforeStopTime {

    public static boolean isCurrentTimeBeforeStopCutoff(Calendar cal, ActivationRestrictionV2 activationRestriction) {

        if (activationRestriction.getStopCutoffYear() > -1) {
            if (activationRestriction.getStopCutoffYear() > cal.get(Calendar.YEAR)) {
                return true;
            } else if (activationRestriction.getStopCutoffYear() < cal.get(Calendar.YEAR)) {
                return false;
            }
        }
        if (activationRestriction.getStopCutoffMonth() > -1) {
            if (activationRestriction.getStopCutoffMonth() > (cal.get(Calendar.MONTH) + 1)) {
                return true;
            } else if (activationRestriction.getStopCutoffMonth() < (cal.get(Calendar.MONTH) + 1)) {
                return false;
            }
        }
        if (activationRestriction.getStopCutoffDay() > -1) {
            if (activationRestriction.getStopCutoffDay() > cal.get(Calendar.DAY_OF_MONTH)) {
                return true;
            } else if (activationRestriction.getStopCutoffDay() < cal.get(Calendar.DAY_OF_MONTH)) {
                return false;
            }
        }
        if (activationRestriction.getStopCutoffDayOfWeek() > -1) {
            if (activationRestriction.getStopCutoffDayOfWeek() > cal.get(Calendar.DAY_OF_WEEK)) {
                return true;
            } else if (activationRestriction.getStopCutoffDayOfWeek() < cal.get(Calendar.DAY_OF_WEEK)) {
                return false;
            }
        }
        if (activationRestriction.getStopCutoffHour() > -1) {
            if (activationRestriction.getStopCutoffHour() > cal.get(Calendar.HOUR_OF_DAY)) {
                return true;
            } else if (activationRestriction.getStopCutoffHour() < cal.get(Calendar.HOUR_OF_DAY)) {
                return false;
            }
        }
        if (activationRestriction.getStopCutoffMinute() > -1) {
            if (activationRestriction.getStopCutoffMinute() > cal.get(Calendar.MINUTE)) {
                return true;
            } else if (activationRestriction.getStopCutoffMinute() < cal.get(Calendar.MINUTE)) {
                return false;
            }
        }
        if (activationRestriction.getStopCutoffSecond() > -1) {
            if (activationRestriction.getStopCutoffSecond() > cal.get(Calendar.SECOND)) {
                return true;
            } else if (activationRestriction.getStopCutoffSecond() < cal.get(Calendar.SECOND)) {
                return false;
            }
        }
        return false;
    }
}
