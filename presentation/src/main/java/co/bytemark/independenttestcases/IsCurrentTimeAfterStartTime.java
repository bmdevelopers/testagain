package co.bytemark.independenttestcases;

import java.util.Calendar;

import co.bytemark.sdk.ActivationRestrictionV2;

public class IsCurrentTimeAfterStartTime {

    public static boolean isCurrentTimeAfterStartCutoff(Calendar currentTime,
                                                        ActivationRestrictionV2 activationRestriction) {

        if (activationRestriction.getStartCutoffYear() > -1) {
            if (activationRestriction.getStartCutoffYear() < currentTime.get(Calendar.YEAR)) {
                return true;
            } else if (activationRestriction.getStartCutoffYear() > currentTime.get(Calendar.YEAR)) {
                return false;
            }
        }
        if (activationRestriction.getStartCutoffMonth() > -1) {
            if (activationRestriction.getStartCutoffMonth() < (currentTime.get(Calendar.MONTH) + 1)) {
                return true;
            } else if (activationRestriction.getStartCutoffMonth() > (currentTime.get(Calendar.MONTH) + 1)) {
                return false;
            }
        }
        if (activationRestriction.getStartCutoffDay() > -1) {
            if (activationRestriction.getStartCutoffDay() < currentTime.get(Calendar.DAY_OF_MONTH)) {
                return true;
            } else if (activationRestriction.getStartCutoffDay() > currentTime.get(Calendar.DAY_OF_MONTH)) {
                return false;
            }
        }
        if (activationRestriction.getStartCutoffDayOfWeek() > -1) {
            if (activationRestriction.getStartCutoffDayOfWeek() < currentTime.get(Calendar.DAY_OF_WEEK)) {
                return true;
            } else if (activationRestriction.getStartCutoffDayOfWeek() > currentTime.get(Calendar.DAY_OF_WEEK)) {
                return false;
            }
        }
        if (activationRestriction.getStartCutoffHour() > -1) {
            if (activationRestriction.getStartCutoffHour() < currentTime.get(Calendar.HOUR_OF_DAY)) {
                return true;
            } else if (activationRestriction.getStartCutoffHour() > currentTime.get(Calendar.HOUR_OF_DAY)) {
                return false;
            }
        }
        if (activationRestriction.getStartCutoffMinute() > -1) {
            if (activationRestriction.getStartCutoffMinute() < currentTime.get(Calendar.MINUTE)) {
                return true;
            } else if (activationRestriction.getStartCutoffMinute() > currentTime.get(Calendar.MINUTE)) {
                return false;
            }
        }
        if (activationRestriction.getStartCutoffSecond() > -1) {
            if (activationRestriction.getStartCutoffSecond() < currentTime.get(Calendar.SECOND)) {
                return true;
            } else if (activationRestriction.getStartCutoffSecond() > currentTime.get(Calendar.SECOND)) {
                return false;
            }
        }
        return false;
    }
}
