package co.bytemark.transfer_virtual_card

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity

class TransferVirtualCardActivity : MasterActivity() {

    override fun getLayoutRes() = R.layout.activity_transfer_virtual_card

    override fun useHamburgerMenu() = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(getString(R.string.transfer_existing_virtual_card),true)
    }

    override fun onBackPressed() {
        supportFragmentManager.findFragmentById(R.id.transferVirtualCardFragment)?.let {
            if ((it as TransferVirtualCardFragment).canGoBack()) {
                super.onBackPressed()
            }
        }
    }


}
