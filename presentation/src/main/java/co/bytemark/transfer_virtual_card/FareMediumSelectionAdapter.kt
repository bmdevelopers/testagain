package co.bytemark.transfer_virtual_card

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.fare_medium.UNLIMITED
import co.bytemark.helpers.ConfHelper
import co.bytemark.widgets.util.hide
import kotlinx.android.synthetic.main.fare_medium_selection_item_row_detailed.view.*
import java.lang.StringBuilder

/**
 * Created by ranjith on 05/05/20
 */

class FareMediumSelectionAdapter constructor(
        private val virtualCardList: List<FareMedium>,
        private val confHelper: ConfHelper,
        private val itemType: Int = ITEM_TYPE_MINIMAL,
        private val itemClickListener: (FareMedium) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val showTransferRestrictionViews = confHelper.isTransferRestrictionEnabled()

    companion object {
        const val ITEM_TYPE_MINIMAL = 1
        const val ITEM_TYPE_DETAILED = 2
    }

    inner class DetailedViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                val item = virtualCardList[adapterPosition]
                if (showTransferRestrictionViews && item.remainingTransferCount == 0) {
                    it.context?.let { context ->
                        val message = StringBuffer()
                        message.append(context.getString(R.string.transfer_virtual_card_transfer_restricted_message_1))
                        if (item.nextResetInDays != UNLIMITED) {
                            message.append(context.getString(
                                    R.string.transfer_virtual_card_transfer_restricted_message_2,
                                    context.resources.getQuantityString(
                                            R.plurals.transfer_virtual_card_note_transfer_period,
                                            item.nextResetInDays,
                                            item.nextResetInDays
                                    )
                            ))
                        }
                        Toast.makeText(
                                context,
                                message.toString(),
                                Toast.LENGTH_SHORT).show()
                    }

                } else {
                    itemClickListener(item)
                }
            }
        }

        fun bind(virtualCard: FareMedium) {
            with(itemView) {
                fareMediumName.text = virtualCard.nickname
                fareMediumID.text = "#${virtualCard.printedCardNumber}"
                storedValue.text = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(virtualCard.storedValue)
                if (virtualCard.fares?.isNotEmpty() == true) {
                    virtualCard.fares?.joinToString(postfix = " ") { fare -> fare.name }.also {
                        passesListText.text = it
                    }
                } else {
                    passesListText.text = itemView.context.getString(R.string.msg_no_passes_available)
                }

                if (showTransferRestrictionViews) {
                    // transfer restriction logic
                    // if remaining count is -1, show Unlimited
                    // if next reset days -1 then don't show the next reset message
                    // if remaining count 0, then grey the bg and don't allow them to select
                    if (virtualCard.remainingTransferCount == UNLIMITED) {
                        transferCountMessage.text = context.getString(R.string.transfer_virtual_card_unlimited)
                    } else {
                        val messageBuilder = StringBuilder()
                        if (virtualCard.nextResetInDays >= 0) {
                            messageBuilder.append(context.getString(
                                    R.string.transfer_virtual_card_reset_message,
                                    virtualCard.remainingTransferCount,
                                    context.resources.getQuantityString(
                                            R.plurals.transfer_virtual_card_note_transfer_period,
                                            virtualCard.nextResetInDays,
                                            virtualCard.nextResetInDays
                                    )
                            ))
                        } else {
                            messageBuilder.append(virtualCard.remainingTransferCount)
                        }
                        transferCountMessage.text = messageBuilder.toString()
                    }

                    if (virtualCard.remainingTransferCount == 0) {
                        virtualCardViewDetailed.setCardBackgroundColor(ContextCompat.getColor(context, R.color.gray_theme_disabled))
                    }
                } else {
                    transferCountGroup.hide()
                }
            }
        }
    }

    inner class MinimalViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener { itemClickListener(virtualCardList[adapterPosition]) }
        }

        fun bind(fareMedium: FareMedium) {
            itemView.fareMediumName.text = "${fareMedium.nickname} (${fareMedium.printedCardNumber})"
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            when (viewType) {
                ITEM_TYPE_MINIMAL -> MinimalViewHolder(
                        LayoutInflater
                                .from(parent.context)
                                .inflate(R.layout.fare_medium_selection_item_row_minimal, parent, false))
                else -> DetailedViewHolder(
                        LayoutInflater
                                .from(parent.context)
                                .inflate(R.layout.fare_medium_selection_item_row_detailed, parent, false))
            }

    override fun getItemCount(): Int = virtualCardList.size

    override fun getItemViewType(position: Int) = itemType

    override
    fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when (viewHolder) {
            is MinimalViewHolder -> viewHolder.bind(virtualCardList[position])
            else -> (viewHolder as DetailedViewHolder).bind(virtualCardList[position])
        }
    }
}
