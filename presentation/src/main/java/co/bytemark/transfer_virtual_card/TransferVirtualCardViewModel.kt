package co.bytemark.transfer_virtual_card

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import co.bytemark.domain.interactor.fare_medium.FareMediumRequestValues
import co.bytemark.domain.interactor.fare_medium.GetAllVirtualCardsUseCase
import co.bytemark.domain.interactor.fare_medium.TransferVirtualCardUseCase
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.fare_medium.FareMedium
import javax.inject.Inject

/**
 * Created by ranjith on 05/05/20
 */

class TransferVirtualCardViewModel @Inject constructor(
        private val getAllVirtualCardsUseCase: GetAllVirtualCardsUseCase,
        private val transferVirtualCardUseCase: TransferVirtualCardUseCase
) : ViewModel() {


    fun getVirtualCards(): LiveData<Result<List<FareMedium>>> {
        return getAllVirtualCardsUseCase.getLiveData(null)
    }

    fun transferVirtualCard(fareMediumId: String) =
            transferVirtualCardUseCase.getLiveData(FareMediumRequestValues(fareMediumId))

}