package co.bytemark.transfer_virtual_card

import android.app.Activity
import android.graphics.Color
import android.graphics.Insets
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowInsets
import android.widget.PopupWindow
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.fare_medium.UNLIMITED
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.util.Util.getScreenSize
import co.bytemark.widgets.util.createViewModel
import co.bytemark.widgets.util.hide
import co.bytemark.widgets.util.postDelay
import kotlinx.android.synthetic.main.fare_medium_selection_item_row_detailed.*
import kotlinx.android.synthetic.main.fragment_transfer_virtual_card.*
import kotlinx.android.synthetic.main.virtual_card_selection_dialog.view.*
import javax.inject.Inject


class TransferVirtualCardFragment : BaseMvvmFragment() {

    private var showingPopup: Boolean = false

    private var virtualCardList: List<FareMedium>? = null

    private var selectedVirtualCardUuid: String? = null

    private lateinit var viewModel: TransferVirtualCardViewModel

    private var popupWindow: PopupWindow? = null

    private var maxTransferCount = 0
    private var resetTimePeriod = 0

    private var fareMediumId: String? = null

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    companion object {
        fun newInstance() = TransferVirtualCardFragment()
    }

    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = createViewModel { CustomerMobileApp.appComponent.transferVirtualCardViewModel }
        maxTransferCount = confHelper.getMaxTransferCount()
        resetTimePeriod = confHelper.getTransferCountResetPeriod()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_transfer_virtual_card, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectCardTextView.setOnClickListener { showCardListDialog() }
        selectCardImage.setOnClickListener { showCardListDialog() }
        resultStateLayout.showContent()

        activity?.intent?.getStringExtra(AppConstants.FARE_MEDIA_ID)?.let {
            this.fareMediumId = it
        }

        transferVirtualCardButton.setOnClickListener {
            if (selectedVirtualCardUuid != null) {
                if (isOnline()) {
                    transferVirtualCard()
                } else {
                    connectionErrorDialog()
                }
            } else {
                Toast.makeText(context, getString(R.string.select_card_to_transfer), Toast.LENGTH_SHORT).show()
            }
        }

        loadVirtualCards()
    }

    private fun transferVirtualCard() {
        selectedVirtualCardUuid?.let {
            viewModel.transferVirtualCard(it).observe(this.viewLifecycleOwner, Observer {
                when (it) {
                    is Result.Loading -> {
                        resultStateLayout.showLoading(R.drawable.tickets_material, getString(R.string.loading))
                    }
                    is Result.Success -> {
                        analyticsPlatformAdapter.transferVirtualCard(AnalyticsPlatformsContract.Status.SUCCESS, "")
                        resultStateLayout.showEmpty(
                                R.drawable.check_material,
                                getString(R.string.success),
                                getString(R.string.msg_transfer_card_success),
                                getString(R.string.fare_medium_card_action_done))
                        {
                            sendResult(Activity.RESULT_OK)
                        }
                    }
                    is Result.Failure -> {
                        var errorMessage: String = ""
                        resultStateLayout.showError(
                                R.drawable.error_material,
                                getString(R.string.popup_error),
                                if (!isOnline()) {
                                    errorMessage = getString(R.string.network_connectivity_error_message)
                                    errorMessage
                                } else if (it.bmError.isNotEmpty()) {
                                    errorMessage = it.bmError.first().message
                                    errorMessage
                                } else {
                                    errorMessage = getString(R.string.something_went_wrong)
                                    errorMessage
                                },
                                getString(R.string.back_to_use_screen)
                        ) {
                            sendResult(Activity.RESULT_CANCELED)
                        }
                        analyticsPlatformAdapter.transferVirtualCard(AnalyticsPlatformsContract.Status.FAILURE, errorMessage)
                    }
                }
            })
        }
    }

    private fun showCardListDialog() {
        if (!showingPopup) {
            activity?.let {
                val point = getScreenSize(it)

                selectionLabel.setTextColor(confHelper.accentThemeBacgroundColor)

                popupWindow = PopupWindow(context)
                val popupView = LayoutInflater.from(context).inflate(R.layout.virtual_card_selection_dialog, null, false)
                popupView.virtualCardSelectorView.layoutManager = LinearLayoutManager(context)
                val adapter = FareMediumSelectionAdapter(virtualCardList!!, confHelper, FareMediumSelectionAdapter.ITEM_TYPE_DETAILED) { virtualCard ->
                    onSelectVirtualCard(virtualCard)
                    postDelay(200) {
                        popupWindow!!.dismiss()

                    }
                }
                popupView.virtualCardSelectorView.adapter = adapter
                popupView.minimumWidth = underLine.width
                popupWindow!!.setBackgroundDrawable(ColorDrawable(Color.WHITE))
                popupWindow!!.elevation = 28F
                popupWindow!!.width = underLine.width
                if (virtualCardList!!.size > 3) {
                    popupWindow!!.height = (point.y / 3) * 2
                }
                popupWindow!!.contentView = popupView
                popupWindow!!.isOutsideTouchable = true
                popupWindow!!.setOnDismissListener {
                    showingPopup = false
                }
                popupWindow!!.showAsDropDown(selectionLabel)
                showingPopup = true
            }
        }

    }

    private fun onSelectVirtualCard(virtualCard: FareMedium) {
        this.selectedVirtualCardUuid = virtualCard.uuid
        transferVirtualCardButton.isEnabled = true
        selectCardTextView.text = "${virtualCard.nickname} ${virtualCard.printedCardNumber}"
        cardNicknameTextView.text = virtualCard.nickname
        nicknameLabelTextView.visibility = View.VISIBLE
        selectCardTextView.setTextColor(confHelper.collectionThemePrimaryTextColor)
        cardNicknameTextView.setTextColor(confHelper.collectionThemePrimaryTextColor)
        context?.let {
            selectionLabel.setTextColor(ContextCompat.getColor(it, R.color.gray_theme_label))
        }
    }

    private fun loadVirtualCards() {
        viewModel.getVirtualCards().observe(this.viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> {
                    resultStateLayout.showLoading(R.drawable.box_material, getString(R.string.loading))
                }
                is Result.Success -> {
                    resultStateLayout.showContent()
                    it.data?.let { virtualCardList ->
                        if (virtualCardList.isNotEmpty()) {
                            this.virtualCardList = virtualCardList
                            if(fareMediumId!= null){
                                virtualCardList.forEach { fm ->
                                    if(fm.fareMediumId == fareMediumId && fm.remainingTransferCount != 0){
                                        onSelectVirtualCard(fm)
                                        return@forEach
                                    }
                                }
                            }
                        } else {
                            // this case is not going to happen, since we will check for this case in use screen
                            resultStateLayout.showEmpty(R.drawable.error_material, getString(R.string.transfer_virtual_card_no_cards_available))
                        }
                    }
                }
                is Result.Failure -> {
                    resultStateLayout.showError(
                            R.drawable.error_material,
                            getString(R.string.popup_error),
                            if (!isOnline()) {
                                getString(R.string.network_connectivity_error_message)
                            } else if (it.bmError.isNotEmpty()) {
                                it.bmError.first().message
                            } else {
                                getString(R.string.something_went_wrong)
                            },
                            getString(R.string.back_to_use_screen)
                    ) {
                        sendResult(Activity.RESULT_CANCELED)
                    }
                }
            }
        })
    }

    fun canGoBack() =
            if (showingPopup) {
                popupWindow?.dismiss()
                showingPopup = false
                false
            } else {
                true
            }

    private fun sendResult(resultCode: Int) {
        activity?.setResult(resultCode)
        activity?.finish()
    }

}
