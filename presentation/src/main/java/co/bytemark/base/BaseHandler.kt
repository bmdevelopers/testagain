package co.bytemark.base

import android.view.View

interface BaseHandler<T> {
    fun onClick(view: View, data: T?)
}