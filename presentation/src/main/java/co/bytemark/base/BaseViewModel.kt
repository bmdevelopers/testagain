package co.bytemark.base

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.annotation.StringRes
import co.bytemark.CustomerMobileApp
import co.bytemark.domain.model.common.BMError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import javax.inject.Inject
import javax.inject.Named

open class BaseViewModel : ViewModel() {

    @Inject
    lateinit var application: Application

    @Inject
    @field:Named("ioScope")
    lateinit var ioScope: CoroutineScope

    @Inject
    @field:Named("uiScope")
    lateinit var uiScope: CoroutineScope

    val errorLiveData by lazy {
        MutableLiveData<BMError?>()
    }

    init {
        CustomerMobileApp.component.inject(this)
    }

    public override fun onCleared() {
        ioScope.cancel()
        uiScope.cancel()
    }

    fun getString(@StringRes stringRes : Int) = application.baseContext.getString(stringRes)

}