package co.bytemark.base;

import android.content.res.Configuration;
import android.os.Bundle;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import javax.inject.Inject;

import butterknife.ButterKnife;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import icepick.Icepick;

public abstract class BaseActivity extends AppCompatActivity {

    @Inject
    public AnalyticsPlatformAdapter analyticsPlatformAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (shouldReduceFontSizeToDefaultIfLarge()) {
            reduceFontSizeToDefaultIfLarge();
        } else {
            useSystemFontSizeAsItIs();
        }
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();
        ButterKnife.bind(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        analyticsPlatformAdapter.startAnalyticsPlatforms(this);
    }

    @Override
    public void onStop() {
        analyticsPlatformAdapter.stopAnalyticsPlatforms(this);
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                hideSoftKeyboard();
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void reduceFontSizeToDefaultIfLarge() {
        Configuration configuration = getResources().getConfiguration();
        if (configuration.fontScale > 1.00) {
            configuration.fontScale = (float) 1.00;
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
            wm.getDefaultDisplay().getMetrics(metrics);
            metrics.scaledDensity = configuration.fontScale * metrics.density;
            getBaseContext().getResources().updateConfiguration(configuration, metrics);
        }
    }

    private void useSystemFontSizeAsItIs() {
        Configuration systemConfiguration = new Configuration();
        Settings.System.getConfiguration(getBaseContext().getContentResolver(), systemConfiguration);
        if (systemConfiguration.fontScale != getResources().getConfiguration().fontScale) {
            getBaseContext().getResources().updateConfiguration(systemConfiguration, getResources().getDisplayMetrics());
        }
    }

    protected boolean shouldReduceFontSizeToDefaultIfLarge() {
        return false;
    }
}
