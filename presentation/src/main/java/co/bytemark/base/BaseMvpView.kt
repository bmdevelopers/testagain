package co.bytemark.base

import co.bytemark.mvp.MvpView


interface BaseMvpView : MvpView {
    fun showSessionExpiredError(message: String)
    fun showDeviceLostOrStolenErrorDialog()
    fun showAppUpdateDialog()
    fun showDeviceTimeErrorDialog()
    fun showDefaultErrorDialog(error: String)
    fun connectionErrorDialog()
    fun handleUnAuthorisedUser()
}