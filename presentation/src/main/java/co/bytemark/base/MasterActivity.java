package co.bytemark.base;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.drawerlayout.widget.DrawerLayout.DrawerListener;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.analytics.AnalyticsPlatformsContract;
import co.bytemark.buy_tickets.BuyTicketsActivity;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.manage.ManageCardsActivity;
import co.bytemark.sdk.model.menu.MenuItem;
import co.bytemark.widgets.LinearRecyclerView;
import co.bytemark.widgets.dynamicmenu.MenuClickManager;
import me.grantland.widget.AutofitHelper;
import timber.log.Timber;

import static androidx.core.view.GravityCompat.START;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public abstract class MasterActivity extends BaseActivity
        implements DrawerLayoutCallback, DrawerListener, MenuClickManager {

    public static final int REQUEST_CODE_LOGIN = 1;


    @Inject
    protected ConfHelper confHelper;


    @BindView(R.id.nav_view)
    protected NavigationView navigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.iv_nav_branding)
    ImageView ivNavBranding;
    @BindView(R.id.ll_nav_header)
    LinearLayout llNavHeader;
    @BindView(R.id.navigation_recycler_view)
    LinearRecyclerView navigationList;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    private ActionBarDrawerToggle toggle;

    private NavigationMenuAdapter navigationMenuAdapter;
    private boolean mToolBarNavigationListenerIsRegistered;
    private String title;

    /**
     * Helper method that can be used by child classes to
     * specify that they don't want a {@link Toolbar}
     *
     * @return boolean
     */
    protected boolean useToolbar() {
        return true;
    }

    /**
     * Helper method to allow child classes to opt-out of having the
     * hamburger menu.
     *
     * @return boolean
     */
    protected boolean useHamburgerMenu() {
        return true;
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomerMobileApp.Companion.getComponent().inject(this);

        if (getIntent() != null && getIntent().getExtras() != null) {
            title = getIntent().getExtras().getString(AppConstants.TITLE);
        }

        setContentView(getLayoutRes());
        ButterKnife.bind(this);

        if (useToolbar()) {
            setSupportActionBar(toolbar);
            if (useHamburgerMenu()) { // Show menu
                toggle = new ActionBarDrawerToggle(this,
                        drawer,
                        toolbar,
                        R.string.navigation_drawer_open,
                        R.string.navigation_drawer_close);
                drawer.addDrawerListener(toggle);
                toggle.syncState();
                tintToolbarIcon();
            } else { // Show up indicator
                //noinspection ConstantConditions
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = confHelper.getUpArrowDrawable();
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        } else {
            toolbar.setVisibility(View.GONE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
        drawer.addDrawerListener(this);

        initNavigationDrawer();

        coordinatorLayout.setStatusBarBackgroundColor(confHelper.getStatusBarColor());
        if (title != null && !title.equals("")) {
            setToolbarTitle(title);
            makeAnnouncementForTitle(title);
        }
    }

    protected void enableOrDisableDrawer(boolean enable) {
        //drawer.removeDrawerListener(toggle);
        //drawer.removeDrawerListener(this);
        //toggle = null;
        //toolbar.setNavigationIcon(null);
        if(getSupportActionBar() == null) return;
        if (enable) {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            final Drawable upArrow = confHelper.getUpArrowDrawable();
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            if (!mToolBarNavigationListenerIsRegistered) {
                toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
                mToolBarNavigationListenerIsRegistered = true;
            }
        } else {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            // Remove back button
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            // Show hamburger
            toggle.setDrawerIndicatorEnabled(true);
            // Remove the/any drawer toggle listener
            toggle.setToolbarNavigationClickListener(null);
            mToolBarNavigationListenerIsRegistered = false;
        }
    }

    private void initNavigationDrawer() {
        final String parentOrgUuid = CustomerMobileApp.Companion.getConf().getOrganization().getUuid().replaceAll("-", "_");
        navigationMenuAdapter = new NavigationMenuAdapter(confHelper, this, confHelper.getSupportedNavigationMenuItems(), this, analyticsPlatformAdapter);
        navigationList.setAdapter(navigationMenuAdapter);
        navigationList.setBackgroundColor(confHelper.getNavigationThemeBackgroundColor());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(START)) {
            drawer.closeDrawer(START);
        } else {
            ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
            if (taskList.get(0).numActivities == 1 && taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
                showExitDialog();
            } else {
                super.onBackPressed();
            }
        }
    }

    public void showExitDialog() {
        new MaterialDialog.Builder(MasterActivity.this)
                .title(getString(R.string.popup_exiting_app) + " " + getString(R.string.app_name))
                .content(R.string.popup_exit_app_body)
                .positiveText(R.string.v3_popup_yes)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .negativeText(R.string.popup_cancel)
                .onPositive((dialog, which) -> finishAffinityWithCancelResult())
                .onNegative((dialog, which) -> dialog.dismiss())
                .show();
    }

    private void finishAffinityWithCancelResult() {
        setResult(Activity.RESULT_CANCELED);
        finishAffinity();
    }

    @Override
    public void closeDrawer() {
        drawer.closeDrawer(START);
    }

    private void tintToolbarIcon() {
        final DrawerArrowDrawable drawable = toggle.getDrawerArrowDrawable();
        if (confHelper != null) {
            drawable.setColor(confHelper.getHeaderThemePrimaryTextColor());
        }
    }

    /**
     * Helper to set toolbar title if the activity has toolbar
     *
     * @param title The tile to be set
     */
    protected void setToolbarTitle(@Nullable CharSequence title) {
        title = title == null ? "" : title;
        if (useToolbar()) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(title);
                toolbar.setTitleTextColor(confHelper.getHeaderThemePrimaryTextColor());
                toolbar.setBackgroundColor(confHelper.getHeaderThemeBackgroundColor());
            }
        }
    }

    protected void setToolbarTitle(@Nullable CharSequence title,boolean fitToScreen) {
        setToolbarTitle(title);
        fitToolbarTitle(fitToScreen);
    }

    private void fitToolbarTitle(Boolean fitToScreen) {
        if(fitToScreen){
            if(toolbar.getChildCount() >0 && toolbar.getChildAt(0) instanceof TextView){
                try {
                    AutofitHelper.create((TextView) toolbar.getChildAt(0));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Wrapper around @link setToolbarTitle(CharSequence) to enable setting of title with resources
     *
     * @param stringRes the id of string resource
     */
    protected void setToolbarTitle(@StringRes int stringRes) {
        setToolbarTitle(getString(stringRes));
    }

    public void setContentDescriptionForToolbarTitle(String description) {
        toolbar.getChildAt(0).setContentDescription(description);
    }

    public void makeAnnouncementForTitle(String title) {
        toolbar.setFocusable(true);
        toolbar.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_YES);
        toolbar.announceForAccessibility(title);
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        if (slideOffset < 0.7) {
            toolbar.setVisibility(View.VISIBLE);
        } else {
            toolbar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        if (getCurrentFocus() != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    protected void setNavigationViewCheckedItem(@Nullable String actionScreen) {
        navigationMenuAdapter.setSelectionItem(actionScreen);
    }

    // Menu click manager
    @Nullable
    @Override
    public Activity getAttachedActivity() {
        return this;
    }

    @Override
    public boolean onWebViewItemRequiresConnection() {
        return false;
    }

    @Override
    public void onLogOutInitiated() {

    }

    @Override
    public void onLogInInitiated() {

    }

    @Override
    public void onActionSheetClicked(@org.jetbrains.annotations.Nullable String actionSheetType) {
    }

    @Override
    public boolean onMenuItemClicked(MenuItem item) {
//        drawer.closeDrawer(Gravity.LEFT);
        return false;
    }

    @Override
    public boolean shouldUseTaskStack() {
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.d("onActivityResult() called with: " + "requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");
        if (requestCode == AppConstants.HAFAS_ACTIVITY_REQUEST_CODE) {
            switch (resultCode) {
                case RESULT_OK:
                    Timber.d("Hacon BuyTickets Callback");
                    if (confHelper.isFaremediaApp()) {
                        // For Init app we need to open ManageScreen
                        Intent intent = new Intent(this, ManageCardsActivity.class);
                        intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.HACON);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                    } else {
                        Intent intent = new Intent(this, BuyTicketsActivity.class);
                        // Hacon may send product UUID for keys serviceLevel or product_uuid
                   /* if (data.getExtras() != null && data.getExtras().getString(AppConstants.HACON_SERVICE_LEVEL) != null
                            && !data.getExtras().getString(AppConstants.HACON_SERVICE_LEVEL).contains(AppConstants.HACON_BUY_PASS)) {
                        intent.putExtra(AppConstants.PRODUCT_UUID, data.getExtras().getString(AppConstants.HACON_SERVICE_LEVEL));
                    } else if (data.getExtras() != null && data.getExtras().getString(AppConstants.HACON_PRODUCT_UUID) != null) {
                        intent.putExtra(AppConstants.PRODUCT_UUID, data.getExtras().getString(AppConstants.HACON_PRODUCT_UUID));
                    }*/

                        if (data.getExtras() != null && data.getExtras().getString(AppConstants.HACON_SERVICE_LEVEL) != null
                                && !data.getExtras().getString(AppConstants.HACON_SERVICE_LEVEL).contains(AppConstants.HACON_BUY_PASS)) {

                            if (data.getExtras().getString(AppConstants.HACON_SERVICE_LEVEL).contains(AppConstants.HACON_PRODUCT_UUID)) {
                                intent.putExtra(AppConstants.PRODUCT_UUID, getValueFromJson(AppConstants.HACON_PRODUCT_UUID, data.getExtras().getString(AppConstants.HACON_SERVICE_LEVEL)));
                            } else if (data.getExtras().getString(AppConstants.HACON_SERVICE_LEVEL).contains(AppConstants.HACON_FARE_ID)) {
                                intent.putExtra(AppConstants.FARE_ID, getValueFromJson(AppConstants.HACON_FARE_ID, data.getExtras().getString(AppConstants.HACON_SERVICE_LEVEL)));
                            } else {
                                intent.putExtra(AppConstants.PRODUCT_UUID, data.getExtras().getString(AppConstants.HACON_SERVICE_LEVEL));
                            }
                        } else if (data.getExtras() != null && data.getExtras().getString(AppConstants.HACON_FARE_ID) != null) {
                            intent.putExtra(AppConstants.FARE_ID, data.getExtras().getString(AppConstants.HACON_FARE_ID));
                        } else if (data.getExtras() != null && data.getExtras().getString(AppConstants.HACON_PRODUCT_UUID) != null) {
                            intent.putExtra(AppConstants.PRODUCT_UUID, data.getExtras().getString(AppConstants.HACON_PRODUCT_UUID));
                        }

                        // adding origin extra for Analytics
                        intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.HACON);

                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                    }
                default:
                    finish();
            }
        }
    }

    private String getValueFromJson(String key, String payload) {
        try {
            JSONObject jsonObject = new JSONObject(payload);
            return jsonObject.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
