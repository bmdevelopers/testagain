package co.bytemark.base;

import com.google.android.material.navigation.NavigationView;
import androidx.appcompat.app.AppCompatActivity;

import co.bytemark.helpers.ConfHelper;

public interface NavigationDrawer {

    void setAppCompactActivity(AppCompatActivity appCompactActivity);

    void initNavigationView(NavigationView navigationView);

    void setConfHelper(ConfHelper confHelper);

    void onDestroy();
}
