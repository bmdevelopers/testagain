package co.bytemark.base;

import android.app.ActivityManager;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.lang.ref.WeakReference;

import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.helpers.NavigationUtils;
import co.bytemark.sdk.model.navigationitems.NavigationItems;
import timber.log.Timber;

public final class NavigationDrawerImpl implements NavigationDrawer, NavigationView.OnNavigationItemSelectedListener {

    private WeakReference<AppCompatActivity> activityWeakReference = new WeakReference<>(null);
    private NavigationView navigationView;
    private Menu menu;
    private final NavigationUtils navigationUtils;
    private NavigationItems navigationItems;

    private NavigationDrawerImpl(NavigationUtils navigationUtils) {
        this.navigationUtils = navigationUtils;
    }

    public static NavigationDrawerImpl getInstance(NavigationUtils navigationUtils) {
        return new NavigationDrawerImpl(navigationUtils);
    }

    @Override
    public void setAppCompactActivity(@NonNull AppCompatActivity appCompactActivity) {
        //noinspection ConstantConditions
        if (appCompactActivity != null) {
            activityWeakReference = new WeakReference<>(appCompactActivity);
        } else {
            Timber.e("AppCompatActivity null");
        }
    }

    @Override
    public void initNavigationView(NavigationView navigationView) {
        this.navigationView = navigationView;
        this.menu = navigationView.getMenu();
        this.navigationView.setNavigationItemSelectedListener(this);
        this.navigationView.inflateHeaderView(R.layout.nav_header_master);
    }

    @Override
    public void setConfHelper(ConfHelper confHelper) {
        navigationView.setItemIconTintList(ColorStateList.valueOf(confHelper.getHeaderThemeAccentColor()));
        navigationView.setItemTextColor(ColorStateList.valueOf(confHelper.getHeaderThemeAccentColor()));

        final ImageView header = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.iv_nav_branding);

        final String parentOrgUuid = CustomerMobileApp.Companion.getConf().getOrganization().getUuid().replaceAll("-", "_");
        try {
            header.setImageTintList(null);
            Drawable drawable = confHelper.getDrawableByName("wide_" + parentOrgUuid);
            if (drawable != null) {
                drawable.setColorFilter(confHelper.getHeaderThemePrimaryTextColor(), PorterDuff.Mode.SRC_IN);
            }
            header.setImageDrawable(drawable);
        } catch (Exception e) {
            Timber.e("Failure to get drawable id. %s", e.toString());
        }

        LinearLayout linearLayout = (LinearLayout) navigationView.getHeaderView(0).findViewById(R.id.ll_nav_header);
        linearLayout.setBackgroundColor(confHelper.getHeaderThemeBackgroundColor());

        // navigationItems = confHelper.getSupportedNavigationItems();
        // navigationUtils.buildNavigationMenuItems(navigationItems.getNavigationItems(), menu);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (activityWeakReference.get() == null) return false;

        navigationUtils.launchWithBackStack(item.getIntent());

        DrawerLayoutCallback callback = (DrawerLayoutCallback) activityWeakReference.get();
        callback.closeDrawer();
        activityWeakReference.get().overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

        // Defending against crappy screen pinning feature. KMN
        return !isAppTaskLocked();
    }

    private boolean isAppTaskLocked() {
        final ActivityManager am = (ActivityManager) activityWeakReference.get().getSystemService(Context.ACTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= 23) {
            final int lockMode = am.getLockTaskModeState();
            if (lockMode == ActivityManager.LOCK_TASK_MODE_PINNED) {
                return true;
            }
        } else {
            //noinspection deprecation
            return am.isInLockTaskMode();
        }
        return false;
    }


    @Override
    public void onDestroy() {
        Timber.d("Cleaning up navigation drawer handler");
        if (activityWeakReference != null && activityWeakReference.get() != null) {
            activityWeakReference.clear();
            activityWeakReference = null;
        }
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(null);
            navigationView = null;
        }
    }
}
