package co.bytemark.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.pwittchen.reactivenetwork.library.ReactiveNetwork;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.ButterKnife;
import co.bytemark.R;
import co.bytemark.data.sdk.SDK;
import co.bytemark.domain.model.common.BMError;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.helpers.RxUtils;
import co.bytemark.mvp.MvpFragment;
import co.bytemark.mvp.MvpPresenter;
import co.bytemark.mvp.MvpView;
import co.bytemark.sdk.BytemarkSDK;
import icepick.Icepick;
import rx.Subscription;
import timber.log.Timber;

@SuppressWarnings("unused")
public abstract class BaseMvpFragment<V extends MvpView, P extends MvpPresenter<V>>
        extends MvpFragment<V, P> {

    private Subscription subscription;

    private boolean online = false;

    @Inject
    RxUtils rxUtils;

    @Inject
    protected ConfHelper confHelper;

    public AlertDialog appUpdateDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        onInject();
        appUpdateDialog = new AlertDialog.Builder(getContext()).create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutRes(), container, false);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initNetworkScanning();
        setAccentThemeColors();
        setBackgroundThemeColors();
        setDataThemeColors();
        applyTints();
    }

    @Override
    public void onResume() {
        super.onResume();
//        initInactivityCheck();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unSubscribe();
    }

    private void initNetworkScanning() {
        unSubscribe();

        subscription = ReactiveNetwork.observeNetworkConnectivity(getContext().getApplicationContext())
                .compose(rxUtils.applySchedulers())
                .distinctUntilChanged()
                .subscribe(connectivity -> {
                    if (connectivity.getState().equals(NetworkInfo.State.DISCONNECTED) || connectivity.getState().equals(NetworkInfo.State.UNKNOWN) ||
                            connectivity.getState().equals(NetworkInfo.State.SUSPENDED)) {
                        online = false;
                        onOffline();
                    } else if (connectivity.getState().equals(NetworkInfo.State.CONNECTED)) {
                        online = true;
                        onOnline();
                    }
                });
    }

    private void unSubscribe() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    protected boolean isOnline() {
        return online;
    }

    public void refreshNetworkActivity() {
        if (isOnline()) {
            onOnline();
        } else {
            onOffline();
        }
    }

    private void initInactivityCheck() {
        Calendar lastAccessTime = SDK.getLastAccessTime();
        Calendar currentTime = Calendar.getInstance();

        lastAccessTime.add(Calendar.DAY_OF_MONTH, 7);

        if (!lastAccessTime.getTime().after(currentTime.getTime())) {
            if (BytemarkSDK.isLoggedIn()) {
                BytemarkSDK.logout(getContext());
                showInactivityErrorDialog();
            }
        }
    }

    protected void handleError(BMError error) {
        switch (error.getCode()) {
            case BytemarkSDK.ResponseCode.DEVICE_LOST_OR_STOLEN:
                showDeviceLostOrStolenErrorDialog();
                break;
            case BytemarkSDK.ResponseCode.SESSION_EXPIRED:
                showSessionExpiredError(error.getMessage());
                break;
            case BytemarkSDK.ResponseCode.TIME_INCONSISTENCY_DETECTED:
                showDeviceTimeErrorDialog();
                break;
            default: {
                if (!online) {
                    connectionErrorDialog();
                } else if (!TextUtils.isEmpty(error.getMessage()))
                    showDefaultErrorDialog(error.getMessage());
                else {
                    showDefaultErrorDialog(getString(R.string.something_went_wrong));
                }
            }
        }
    }

    private void applyTints() {
        final View rootView = getView();
        if (rootView == null) {
            Timber.e("Tinting ignored, root view null");
            return;
        }

        final int[] accentTintableViewIds = getTintableViewIdsForAccentColor();
        for (int accentTintableViewId : accentTintableViewIds) {
            performTint(rootView.findViewById(accentTintableViewId));
        }

        final int[] dataTintableIds = getTintableViewIdsForAccentColor();
        for (int dataTintableId : dataTintableIds) {
            performDataTint(rootView.findViewById(dataTintableId));
        }
    }

    private void performDataTint(@Nullable View view) {
        if (view == null) return;
        if (view instanceof TextView) {
            final TextView textView = (TextView) view;
            textView.setTextColor(confHelper.getDataThemePrimaryTextColor());
            textView.setBackgroundTintList(ColorStateList.valueOf(confHelper.getDataThemeBackgroundColor()));
        }
    }

    private void performTint(@Nullable View view) {
        if (view == null) return;
        if (view instanceof TextView) {
            final TextView textView = (TextView) view;
            textView.setTextColor(confHelper.getAccentThemePrimaryTextColor());
            textView.setBackgroundTintList(ColorStateList.valueOf(confHelper.getAccentThemeBacgroundColor()));
        } else if (view instanceof ViewGroup) { // All layout containers
            final ViewGroup viewGroup = (ViewGroup) view;
            viewGroup.setBackgroundColor(confHelper.getAccentThemeBacgroundColor());
        } else {
            Timber.e("Could not tint a view of class type : %s", view.getClass().getSimpleName());
        }
    }

    private void showInactivityErrorDialog() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.popup_error)
                .content(R.string.sign_in_popup__error_message)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();
    }

    public void showDefaultErrorDialog(String message) {
        if (!isOnline()) {
            connectionErrorDialog();
            return;
        }
        new MaterialDialog.Builder(getContext())
                .title(R.string.popup_error)
                .content(message)
                .cancelable(false)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .show();
    }

    public void showInfoDialog(@StringRes int title,@StringRes int message,@StringRes int positiveButtonText){
        showInfoDialog(getString(title),getString(message),getString(positiveButtonText));
    }

    public void showInfoDialog(String title, String message,String positiveButtonText){
        new MaterialDialog.Builder(getActivity())
                .title(title)
                .content(message)
                .positiveText(positiveButtonText)
                .show();
    }

    public void showDeviceTimeErrorDialog() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.popup_error)
                .content(R.string.device_clock_off_message)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> {
                    onDeviceTimeErrorButtonClick();
                    dialog.dismiss();
                })
                .show();
    }

    public void connectionErrorDialog() {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.change_password_popup_conErrorTitle)
                .content(R.string.change_password_Popup_con_error_body)
                .positiveText(R.string.popup_dismiss)
                .show();
    }


    public void onDeviceTimeErrorButtonClick() {

    }

    public void showDeviceLostOrStolenErrorDialog() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.device_lost_stolen_title)
                .content(R.string.device_lost_stolen_body)
                .cancelable(false)
                .positiveText(R.string.ok)
                .onPositive((dialog, which) -> {
                    onDeviceLostOrStolenButtonClick();
                    dialog.dismiss();
                })
                .show();
    }

    public void showSessionExpiredError(String message) {
        new MaterialDialog.Builder(getContext())
                .title(R.string.v3_popup_error)
                .content(message)
                .cancelable(false)
                .positiveText(R.string.v3_popup_ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog1, which) -> {
                    onSessionExpiredButtonClick();
                })
                .show();
    }

    public void onSessionExpiredButtonClick() {
    }


    public void showAppUpdateDialog() {
        BytemarkSDK.logout(getContext());
        if (appUpdateDialog != null && !appUpdateDialog.isShowing()) {
            appUpdateDialog.setTitle(R.string.popup_update_required_title);
            appUpdateDialog.setMessage(getString(R.string.popup_update_required_message));
            appUpdateDialog.setIcon(android.R.drawable.ic_dialog_alert);
            appUpdateDialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.update), (dialog, which) -> {
                Intent goToMarket = new Intent(Intent.ACTION_VIEW)
                        .setData(Uri.parse("market://details?id=" + getContext().getPackageName()));
                getContext().startActivity(goToMarket);
            });
            appUpdateDialog.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.exit), (dialog, which) -> dialog.cancel());
            appUpdateDialog.show();
        }
    }

    //Unauthorised
    public void handleUnAuthorisedUser() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hideKeyboard(); //hide any active keyboard
    }

    public void onDeviceLostOrStolenButtonClick() {
    }

    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getView() != null)
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    protected abstract void onInject();

    protected abstract void onOnline();

    protected abstract void onOffline();

    protected abstract void setAccentThemeColors();

    protected abstract void setBackgroundThemeColors();

    protected abstract void setDataThemeColors();

    // TODO Make this abstract and migrate all sub fragments to use this
    @NonNull
    protected int[] getTintableViewIdsForBackgroundColor() {
        return new int[0];
    }

    // TODO Make this abstract and migrate all sub fragments to use this
    @NonNull
    protected int[] getTintableViewIdsForAccentColor() {
        return new int[0];
    }

    // TODO Make this abstract and migrate all sub fragments to use this
    @NonNull
    protected int[] getTintableViewIdsForDataColor() {
        return new int[0];
    }

    @Override
    public P getPresenter() {
        return super.presenter;
    }

    @Override
    public void setPresenter(P presenter) {
        super.presenter = presenter;
    }
}
