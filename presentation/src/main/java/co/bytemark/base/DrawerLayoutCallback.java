package co.bytemark.base;

interface DrawerLayoutCallback {

    void closeDrawer();
}
