package co.bytemark.base

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import android.view.View
import co.bytemark.R
import co.bytemark.helpers.AppConstants.TITLE
import kotlinx.android.synthetic.main.activity_tabs.*

/** Created by Santosh on 2019-08-12.
 */

/**
 * This is the base class for Activities which has tabs
 */
abstract class TabsActivity : MasterActivity() {

    override fun getLayoutRes(): Int = R.layout.activity_tabs

    abstract fun getFragmentsForTabs(): List<Fragment>

    var tabPagerAdapter: TabsPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getFragmentsForTabs().forEach {
            val tab = tabs.newTab()
            val tabText = it.arguments?.getString(TITLE)
            tab.contentDescription = tabText.plus(" ").plus(getString(R.string.tab))
            tabs.addTab(tab.setText(tabText))
        }
        tabPagerAdapter = TabsPagerAdapter(supportFragmentManager, getFragmentsForTabs())
        swipeToggleViewPager.adapter = tabPagerAdapter
        swipeToggleViewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(swipeToggleViewPager))
    }

    fun hideTabLayout() {
        tabs.visibility = View.GONE
    }

    fun showTabLayout() {
        tabs.visibility = View.VISIBLE
    }

    fun enableSwipe() = swipeToggleViewPager.setSwipeEnabled(true)

    fun disableSwipe() = swipeToggleViewPager.setSwipeEnabled(false)

}