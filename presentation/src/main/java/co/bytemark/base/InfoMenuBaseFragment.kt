package co.bytemark.base

import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import co.bytemark.R
import javax.inject.Inject

abstract class InfoMenuBaseFragment : BaseMvvmFragment() {

  @Inject
  lateinit var sharedPreferences: SharedPreferences

  var optionsMenuEnabled: Boolean = false
  var shouldShowAlertOnResume = false

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    if(optionsMenuEnabled){
      shouldShowAlertOnResume = sharedPreferences.getBoolean(getScreenName(),false).not()
    }
  }

  override fun onResume() {
    super.onResume()
    if(shouldShowAlertOnResume){
      showAboutScreenDialog()
      sharedPreferences.edit().putBoolean(getScreenName(), true).apply()
      shouldShowAlertOnResume = false
    }
  }

  override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    inflater.inflate(R.menu.menu_info,menu)
    super.onCreateOptionsMenu(menu, inflater)
  }

  override fun onPrepareOptionsMenu(menu: Menu) {
    val menuItem = menu.findItem(R.id.info)
    menuItem.setActionView(R.layout.menu_item_page_info_layout)

    val imageView = menuItem.actionView.findViewById<ImageView>(R.id.infoImageView)
    imageView.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_info_filled))
    imageView.imageTintList = ColorStateList.valueOf(confHelper.headerThemePrimaryTextColor)
    imageView.contentDescription = resources.getString(R.string.info_image_description)

    menuItem.actionView.setOnClickListener {
      showAboutScreenDialog()
    }
  }

  abstract fun showAboutScreenDialog()

  abstract fun getScreenName():String

}