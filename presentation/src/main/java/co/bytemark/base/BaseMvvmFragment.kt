package co.bytemark.base

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.NetworkInfo
import android.net.Uri
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import co.bytemark.R
import co.bytemark.domain.model.common.BMError
import co.bytemark.helpers.ConfHelper
import co.bytemark.helpers.RxUtils
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.BytemarkSDK.ResponseCode
import co.bytemark.sdk.network_impl.BaseNetworkRequest
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.github.pwittchen.reactivenetwork.library.Connectivity
import com.github.pwittchen.reactivenetwork.library.ReactiveNetwork
import rx.Subscription
import javax.inject.Inject

/** Created by Santosh on 19/04/19.
 */

abstract class BaseMvvmFragment : Fragment() {

    private var online = false

    @Inject
    lateinit var confHelper: ConfHelper

    @Inject
    lateinit var rxUtils: RxUtils

    private var subscription: Subscription? = null

    private var defaultErrorTitle: String? = null

    var initNetworkScanningInOnStart = true

    abstract fun onInject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onInject()
        defaultErrorTitle = getString(R.string.popup_error)
    }

    override fun onStart() {
        super.onStart()
        if(initNetworkScanningInOnStart) {
            initNetworkScanning()
        }
        setUpColors()
        hideKeyboard()//Hide Any open Keyboard
    }

    protected open fun setUpColors() {}

    protected fun isOnline(): Boolean {
        return online
    }

    private fun initNetworkScanning() {
        unSubscribe()

        subscription = ReactiveNetwork.observeNetworkConnectivity(context!!.applicationContext)
                .compose<Connectivity>(rxUtils.applySchedulers())
                .distinctUntilChanged()
                .subscribe { connectivity ->
                    if (connectivity.state == NetworkInfo.State.DISCONNECTED || connectivity.state == NetworkInfo.State.UNKNOWN ||
                            connectivity.state == NetworkInfo.State.SUSPENDED) {
                        online = false
                        onOffline()
                    } else if (connectivity.state == NetworkInfo.State.CONNECTED) {
                        online = true
                        onOnline()
                    }
                }
    }

    private fun unSubscribe() {
        if (subscription != null && subscription?.isUnsubscribed == false) {
            subscription?.unsubscribe()
        }
    }

    open fun onOnline() {}

    open fun onOffline() {}

    open fun handleError(bmError: BMError) {
        when (bmError.code) {
            ResponseCode.DEVICE_LOST_OR_STOLEN -> showDeviceLostOrStolen()
            ResponseCode.SESSION_EXPIRED -> showSessionExpiredError(bmError.message)
            ResponseCode.TIME_INCONSISTENCY_DETECTED -> showDeviceTimeErrorDialog()
            BaseNetworkRequest.UNAUTHORISED -> closeSession()
            else -> {
                if (!online || bmError.code == ResponseCode.NETWORK_NOT_AVAILABLE) {
                    connectionErrorDialog()
                } else if (bmError.message != null && bmError.message!!.isNotEmpty())
                    showDefaultErrorDialog(errorMsg = bmError.message)
                else {
                    showDefaultErrorDialog(errorMsg = getString(R.string.something_went_wrong))
                }
            }
        }
    }

    open fun closeSession() {
        BytemarkSDK.logout(context)
        activity?.onBackPressed()
    }

    private fun showSessionExpiredError(errorMsg: String) {
        context?.let {
            MaterialDialog.Builder(it)
                    .title(R.string.popup_error)
                    .content(errorMsg)
                    .cancelable(false)
                    .positiveText(R.string.ok)
                    .positiveColor(confHelper.dataThemeAccentColor)
                    .onPositive { dialog, _ ->
                        dialog.dismiss()
                        onSessionExpiredButtonClick()
                        activity?.finish()
                    }
                    .show()
        }
    }

    private fun onSessionExpiredButtonClick() {
    }


    private fun showDeviceLostOrStolen() {
        context?.let {
            BytemarkSDK.logout(it)
            MaterialDialog.Builder(it)
                    .title(R.string.device_lost_stolen_title)
                    .content(R.string.device_lost_stolen_body)
                    .cancelable(false)
                    .positiveText(R.string.ok)
                    .onPositive { dialog, _ ->
                        onDeviceLostOrStolenButtonClick()
                        dialog.dismiss()
                    }
                    .show()
        }
    }

    open infix fun setDefaultErrorTitle(errorTitle: String) {
        defaultErrorTitle = errorTitle
    }

    open fun onDeviceLostOrStolenButtonClick() {}

    open fun showDefaultErrorDialog(title: String? = defaultErrorTitle, errorMsg: String?, finishActivity: Boolean = false) {
        context?.let {
            MaterialDialog.Builder(it)
                    .title(title!!)
                    .content(errorMsg!!)
                    .cancelable(false)
                    .positiveText(R.string.ok)
                    .positiveColor(confHelper.dataThemeAccentColor)
                    .onPositive { dialog, _ ->
                        dialog.dismiss()
                        if (finishActivity) activity?.finish()
                    }
                    .show()
        }
    }

    open fun showDeviceTimeErrorDialog() {
        MaterialDialog.Builder(context!!)
                .title(R.string.popup_error)
                .content(R.string.device_clock_off_message)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.dataThemeAccentColor)
                .onPositive { dialog: MaterialDialog, which: DialogAction? ->
                    onDeviceTimeErrorButtonClick()
                    dialog.dismiss()
                }
                .show()
    }

    open fun showAppUpdateDialog() {
        BytemarkSDK.logout(context)
        showDialog(
                title = getString(R.string.popup_update_required_title),
                content = getString(R.string.popup_update_required_message),
                iconDrawable = ContextCompat.getDrawable(activity!!, android.R.drawable.ic_dialog_alert),
                positiveText = getString(R.string.update),
                negativeText = getString(R.string.exit),
                positiveAction = {
                    val goToMarket = Intent(Intent.ACTION_VIEW).setData(Uri.parse("market://details?id=" + context!!.packageName))
                    activity!!.startActivity(goToMarket)
                },
                negativeAction = {}
        )

    }

    open fun onDeviceTimeErrorButtonClick() {}

    open fun showDialog(
            title: String,
            content: String,
            positiveText: String,
            negativeText: String = "",
            iconDrawable: Drawable? = null,
            finishActivity: Boolean = false,
            positiveAction: () -> Unit = {},
            negativeAction: () -> Unit = {}
    ) {

        activity?.let {
            val builder = MaterialDialog.Builder(it)
                    .title(title)
                    .content(content)
                    .positiveText(positiveText)
                    .positiveColor(confHelper.dataThemeAccentColor)
                    .onPositive { dialog, _ ->
                        dialog.dismiss()
                        positiveAction()
                        if (finishActivity) activity?.finish()
                    }

            iconDrawable?.let { drawable -> builder.icon(drawable) }

            if (negativeText.isNotEmpty()) {
                builder.negativeText(negativeText)
                builder.onNegative { dialog, _ ->
                    negativeAction()
                    dialog.dismiss()
                }
            }

            builder.show()
        }
    }

    open fun connectionErrorDialog(@StringRes positiveTextId: Int = R.string.popup_dismiss,
                                   enableNegativeAction: Boolean = false,
                                   @StringRes negativeTextId: Int = R.string.popup_cancel,
                                   finishOnDismiss: Boolean = false,
                                   positiveAction: () -> Unit = {},
                                   negativeAction: () -> Unit = {}) {

        showDialog(
                title = getString(R.string.change_password_popup_conErrorTitle),
                content = getString(R.string.change_password_Popup_con_error_body),
                positiveText = getString(positiveTextId),
                negativeText = getString(negativeTextId),
                finishActivity = finishOnDismiss,
                positiveAction = positiveAction,
                negativeAction = negativeAction
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        hideKeyboard() //hide any active keyboard
    }

    protected fun hideKeyboard() {
        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (view != null)
            imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    override fun onDestroy() {
        super.onDestroy()
        unSubscribe()
    }

}