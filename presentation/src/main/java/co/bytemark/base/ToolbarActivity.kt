package co.bytemark.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.buy_tickets.BuyTicketsActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.model.menu.MenuItem
import co.bytemark.widgets.dynamicmenu.MenuClickManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_master.*
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject


abstract class ToolbarActivity : BaseActivity(), MenuClickManager {

    @Inject
    lateinit var confHelper: ConfHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
        setContentView(getLayoutRes())
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(confHelper.upArrowDrawable)
        coordinatorLayout?.setStatusBarBackgroundColor(confHelper.statusBarColor)
    }

    @LayoutRes
    protected abstract fun getLayoutRes(): Int

    protected open fun setToolbarTitle(title: String?) {
        supportActionBar?.title = title ?: ""
        if (title?.isNotEmpty() == true) {
            makeAnnouncementForTitle(title)
        }
    }

    open fun makeAnnouncementForTitle(title: String) {
        toolbar.isFocusable = true
        toolbar.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        toolbar.announceForAccessibility(title)
    }

    override fun getAttachedActivity(): Activity? {
        return this
    }

    override fun onWebViewItemRequiresConnection(): Boolean {
        return false
    }

    override fun onLogOutInitiated() {}

    override fun onLogInInitiated() {}

    override fun onMenuItemClicked(item: MenuItem): Boolean {
        return false
    }

    override fun onActionSheetClicked(actionSheetType: String?) {}

    override fun shouldUseTaskStack(): Boolean {
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.d("onActivityResult() called with: requestCode = [$requestCode], resultCode = [$resultCode], data = [$data]")
        if (requestCode == AppConstants.HAFAS_ACTIVITY_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    Timber.d("Hacon BuyTickets Callback")
                    val intent = Intent(this, BuyTicketsActivity::class.java)
                    if (data?.extras?.getString(AppConstants.HACON_SERVICE_LEVEL) != null && data.extras?.getString(AppConstants.HACON_SERVICE_LEVEL)
                            ?.contains(AppConstants.HACON_BUY_PASS) == false
                    ) {
                        when {
                            data.extras?.getString(AppConstants.HACON_SERVICE_LEVEL)?.contains(AppConstants.HACON_PRODUCT_UUID) == true -> {
                                intent.putExtra(
                                    AppConstants.PRODUCT_UUID,
                                    getValueFromJson(
                                        AppConstants.HACON_PRODUCT_UUID,
                                        data.extras?.getString(AppConstants.HACON_SERVICE_LEVEL) as String
                                    )
                                )
                            }
                            data.extras?.getString(AppConstants.HACON_SERVICE_LEVEL)?.contains(AppConstants.HACON_FARE_ID) == true -> {
                                intent.putExtra(
                                    AppConstants.FARE_ID,
                                    getValueFromJson(
                                        AppConstants.HACON_FARE_ID,
                                        data.extras?.getString(AppConstants.HACON_SERVICE_LEVEL) as String
                                    )
                                )
                            }
                            else -> {
                                intent.putExtra(AppConstants.PRODUCT_UUID, data.extras?.getString(AppConstants.HACON_SERVICE_LEVEL) as String)
                            }
                        }
                    } else if (data?.extras?.getString(AppConstants.HACON_FARE_ID) != null) {
                        intent.putExtra(AppConstants.FARE_ID, data.extras?.getString(AppConstants.HACON_FARE_ID) as String)
                    } else if (data?.extras?.getString(AppConstants.HACON_PRODUCT_UUID) != null) {
                        intent.putExtra(AppConstants.PRODUCT_UUID, data.extras?.getString(AppConstants.HACON_PRODUCT_UUID) as String)
                    }
                    startActivity(intent)
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                    finish()
                }
                else -> finish()
            }
        }
    }

    private fun getValueFromJson(key: String, payload: String?): String? {
        return try {
            if (payload == null) return null
            val jsonObject = JSONObject(payload)
            jsonObject.getString(key)
        } catch (e: JSONException) {
            e.printStackTrace()
            null
        }
    }
}
