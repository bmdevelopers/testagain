package co.bytemark.base

import android.app.Activity
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.StateListDrawable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.ColorUtils
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.BytemarkSDK.SDKUtility
import co.bytemark.sdk.model.menu.MenuItem
import co.bytemark.shared.ActionUtil
import co.bytemark.webview.WebViewActivity
import co.bytemark.widgets.dynamicmenu.AbstractMenuItemAdapter
import co.bytemark.widgets.dynamicmenu.MenuClickManager
import co.bytemark.widgets.dynamicmenu.MenuItemDrawableLoader.getDrawableIdRes
import kotlinx.android.synthetic.main.navigation_menu_item_template.view.*

class NavigationMenuAdapter(
    confHelper: ConfHelper?,
    context: Context,
    menuItems: List<MenuItem>,
    menuClickManager: MenuClickManager,
    analyticsPlatformAdapter: AnalyticsPlatformAdapter?
) : AbstractMenuItemAdapter<NavigationMenuAdapter.NavigationMenuItemHolder>(
    confHelper!!,
    context,
    menuItems,
    menuClickManager,
    analyticsPlatformAdapter
) {

    private var selectionItem: String? = null

    override fun onBindViewHolder(holder: NavigationMenuItemHolder, position: Int, menuItem: MenuItem) {
        with(holder.itemView) {
            val headerThemeAccentColor = confHelper.navigationThemePrimaryTextColor
            navigationItemText.text = menuItem.title
            navigationItemText.setTextColor(headerThemeAccentColor)
            this.contentDescription = menuItem.title
            if (shouldSelect(selectionItem, menuItem)) {
                this.setBackgroundColor(ColorUtils.setAlphaComponent(confHelper.navigationThemeAccentColor, 50))
            } else {
                this.background = makeSelector(headerThemeAccentColor)
            }
            navigationItemImage.setImageDrawable(context.getDrawable(getDrawableIdRes(menuItem.selectedImage)))
            navigationItemImage.imageTintList = ColorStateList.valueOf(headerThemeAccentColor)
        }
    }

    /**
     * Pass values of either [or ][co.bytemark.sdk.model.common.Action.screen]
     * Comparison will be made against both to deremine selection.
     *
     * @param selectionItem
     */
    fun setSelectionItem(selectionItem: String?) {
        this.selectionItem = selectionItem
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NavigationMenuItemHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.navigation_menu_item_template, parent, false)
        return NavigationMenuItemHolder(itemView)
    }

    private fun shouldSelect(selectionItem: String?, menuItem: MenuItem): Boolean {
        if (TextUtils.isEmpty(selectionItem)) {
            return false
        }
        if (selectionItem.equals(menuItem.action?.screen, ignoreCase = true)) {
            return true
        } else if (selectionItem.equals(menuItem.action?.hyperlink, ignoreCase = true)) {
            return true
        }
        return false
    }

    override fun handleWebView(activity: Activity?, item: MenuItem) {
        val baseUrl = SDKUtility.getBaseAccountUrl()
        val intent = ActionUtil.getWebViewIntent(activity!!, item,  baseUrl, confHelper.isNativeLogin)
        intent?.putExtra(WebViewActivity.EXTRA_HAMBURGER, true)
        super.launchIntent(activity, intent)
    }

    companion object {
        private fun makeSelector(color: Int): StateListDrawable {
            return StateListDrawable().apply {
                setExitFadeDuration(400)
                alpha = 45
                addState(intArrayOf(android.R.attr.state_pressed), ColorDrawable(color))
                addState(intArrayOf(), ColorDrawable(Color.TRANSPARENT))
            }
        }
    }

    class NavigationMenuItemHolder(itemView: View) : AbstractMenuItemHolder(itemView)

}