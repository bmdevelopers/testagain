package co.bytemark.shared

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import co.bytemark.R
import co.bytemark.acknowledgements.AcknowledgementsActivity
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.appnotification.NotificationsActivity
import co.bytemark.authentication.AuthenticationActivity
import co.bytemark.authentication.account_management.AccountManagementFragment
import co.bytemark.authentication.change_password.ChangePasswordFragment
import co.bytemark.authentication.manage_pin.ManagePinFragment
import co.bytemark.authentication.voucher_code.VoucherCodeFragment
import co.bytemark.buy_tickets.BuyTicketsActivity
import co.bytemark.customtabs.CustomTabsActivityHelper
import co.bytemark.customtabs.CustomTabsHelper
import co.bytemark.customtabs.WebViewFallback
import co.bytemark.fare_capping.FareCappingActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.manage.ManageCardsActivity
import co.bytemark.menu.MenuActivity
import co.bytemark.nativeappsupport.NativeAppSupportActivity
import co.bytemark.notification_settings.NotificationSettingsActivity
import co.bytemark.payment_methods.PaymentMethodsActivity
import co.bytemark.purchase_history.OrderHistoryActivity
import co.bytemark.rewards.RewardsActivity
import co.bytemark.schedules.SchedulesActivity
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.model.common.Action
import co.bytemark.sdk.model.menu.MenuItem
import co.bytemark.securityquestion.SecurityQuestionActivity
import co.bytemark.settings.SettingsActivity
import co.bytemark.static_resource.ImageRendererActivity
import co.bytemark.static_resource.PdfRendererActivity
import co.bytemark.subscriptions.ListSubscriptionsActivity
import co.bytemark.ticket_storage.TicketStorageActivity
import co.bytemark.use_tickets.UseTicketsActivity
import co.bytemark.userphoto.AccountPhotoActivity
import co.bytemark.voucher_redeem.VoucherRedeemActivity
import co.bytemark.webview.WebViewActivity
import co.bytemark.widgets.util.Util

object ActionUtil {
    private const val AUTHORIZATION = "Authorization"

    /**
     * We need this code since platform does not send the urls to screens even though they
     * are web views. The problem arises since  can have display screen as value and
     * it can be a web view.
     *
     * @return true if the screen is supposed to be displayed by a web view.
     */
    @JvmStatic
    fun isWebView(
        action: Action,
        isNative: Boolean
    ): Boolean {
        if (action.type != null && action.type
                .equals(Action.HYPERLINK, ignoreCase = true)
        ) {
            return true
        }
        val screen = action.screen
        return if (TextUtils.isEmpty(screen)) {
            false
        } else screen.equals(
            Action.LINKED_ACCOUNTS,
            ignoreCase = true
        ) ||
                screen.equals(
                    Action.PASSWORD,
                    ignoreCase = true
                ) && !isNative ||
                screen.equals(
                    Action.TERMS_OF_SERVICE,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.ACCOUNT,
                    ignoreCase = true
                ) && !isNative
    }

    /**
     * Denotes whether the action can be launched by directly calling the activity.
     *
     * @return True if can be launched
     */
    @JvmStatic
    fun isActivity(
        action: Action,
        isNative: Boolean
    ): Boolean {
        val screen = action.screen
        if (action.type
                .equals(Action.AUTHENTICATION, ignoreCase = true)
        ) {
            return true
        } else if (TextUtils.isEmpty(screen)) {
            return false
        }
        return screen.equals(
            Action.PAYMENT_METHODS,
            ignoreCase = true
        ) ||
                screen.equals(
                    Action.PURCHASE_HISTORY,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.TICKET_STORAGE,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.TICKET_VOUCHER,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.ACKNOWLEDGMENTS,
                    ignoreCase = true
                ) ||
                screen.equals(Action.STORE, ignoreCase = true) ||
                screen.equals(Action.MANAGE, ignoreCase = true) ||
                screen.equals(Action.USE_TICKETS, ignoreCase = true) ||
                screen.equals(Action.SETTINGS, ignoreCase = true) ||
                screen.equals(
                    Action.NOTIFICATION,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.MORE_INFORMATION,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.NATIVE_AUTHENTICATION,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.PASSWORD,
                    ignoreCase = true
                ) && isNative ||
                screen.equals(
                    Action.ACCOUNT,
                    ignoreCase = true
                ) && isNative ||
                screen.equals(
                    Action.VOUCHER_CODE,
                    ignoreCase = true
                ) ||
                screen.equals(Action.MANAGE_PIN, ignoreCase = true) ||
                screen.equals(
                    Action.ACCOUNT_PHOTO,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.NOTIFICATION_SETTINGS,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.HACON_SCHEDULE,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.HACON_NYCF_TRIP_PLANNER,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.HACON_TRIP_PLANNER,
                    ignoreCase = true
                ) ||
                screen.equals(Action.HACON_ALARM, ignoreCase = true) ||
                screen.equals(
                    Action.HACON_NEXT_DEPARTURE,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.HACON_SYSTEM_MAP,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.HACON_SYSTEM_MAP_PDF,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.NATIVE_SCHEDULE,
                    ignoreCase = true
                ) ||
                screen.equals(Action.HACON_ALARM, ignoreCase = true) ||
                screen.equals(
                    Action.SECURITY_QUESTION,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.SUBSCRIPTION,
                    ignoreCase = true
                ) ||
                screen.equals(
                    Action.NATIVE_APP_SUPPORT,
                    ignoreCase = true
                ) ||
                screen.equals(Action.FARE_CAPPING, ignoreCase = true) ||
                screen.equals(Action.REWARDS, ignoreCase = true)
    }

    /**
     * @param activity Attached activity
     * @param menuItem Action
     * @param isNative
     * @return Intent to launch intent
     */
    @JvmStatic
    fun getActivityIntent(
        activity: Activity,
        menuItem: MenuItem,
        isNative: Boolean
    ): Intent? {
        val action = menuItem.action
        if (action == null || !isActivity(action, isNative)) {
            return null
        }
        val clazz: Class<*>
        val bundle = Bundle()
        bundle.putString(AppConstants.TITLE, menuItem.title)
        if (action.type
                .equals(Action.AUTHENTICATION, ignoreCase = true)
        ) {
            clazz = AuthenticationActivity::class.java
            val intent = Intent(activity, clazz)
            intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.MENU)
            intent.putExtras(bundle)
            return intent
        }
        when (action.screen) {
            Action.SETTINGS -> clazz = SettingsActivity::class.java
            Action.STORE -> clazz = BuyTicketsActivity::class.java
            Action.MANAGE -> clazz = ManageCardsActivity::class.java
            Action.USE_TICKETS -> clazz =
                UseTicketsActivity::class.java
            Action.PAYMENT_METHODS -> clazz =
                PaymentMethodsActivity::class.java
            Action.PURCHASE_HISTORY -> clazz =
                OrderHistoryActivity::class.java
            Action.TICKET_STORAGE -> clazz =
                TicketStorageActivity::class.java
            Action.TICKET_VOUCHER -> clazz =
                VoucherRedeemActivity::class.java
            Action.ACKNOWLEDGMENTS -> clazz =
                AcknowledgementsActivity::class.java
            Action.MORE_INFORMATION -> clazz = MenuActivity::class.java
            Action.NOTIFICATION -> clazz =
                NotificationsActivity::class.java
            Action.NATIVE_AUTHENTICATION -> clazz =
                AuthenticationActivity::class.java
            Action.ACCOUNT_PHOTO -> clazz =
                AccountPhotoActivity::class.java
            Action.PASSWORD -> {
                clazz = AuthenticationActivity::class.java
                bundle.putString(
                    AuthenticationActivity.EXTRA_KEY_DEFAULT_SCREEN,
                    ChangePasswordFragment.EXTRA_TAG
                )
            }
            Action.MANAGE_PIN -> {
                clazz = AuthenticationActivity::class.java
                bundle.putString(
                    AuthenticationActivity.EXTRA_KEY_DEFAULT_SCREEN,
                    ManagePinFragment.EXTRA_TAG
                )
            }
            Action.ACCOUNT -> {
                clazz = AuthenticationActivity::class.java
                bundle.putString(
                    AuthenticationActivity.EXTRA_KEY_DEFAULT_SCREEN,
                    AccountManagementFragment.EXTRA_TAG
                )
            }
            Action.VOUCHER_CODE -> {
                clazz = AuthenticationActivity::class.java
                bundle.putString(
                    AuthenticationActivity.EXTRA_KEY_DEFAULT_SCREEN,
                    VoucherCodeFragment.EXTRA_TAG
                )
            }
            Action.NOTIFICATION_SETTINGS -> clazz =
                NotificationSettingsActivity::class.java
            Action.HACON_SCHEDULE -> {
                clazz = UseTicketsActivity::class.java
                bundle.putString(AppConstants.TITLE, AppConstants.HACON_SCHEDULE)
            }
            Action.HACON_NYCF_TRIP_PLANNER -> {
                clazz = UseTicketsActivity::class.java
                bundle.putString(AppConstants.TITLE, AppConstants.HACON_TRIP)
            }
            Action.HACON_TRIP_PLANNER -> {
                clazz = UseTicketsActivity::class.java
                bundle.putString(AppConstants.TITLE, AppConstants.HACON_TRIP)
            }
            Action.HACON_ALARM -> {
                clazz = UseTicketsActivity::class.java
                bundle.putString(AppConstants.TITLE, AppConstants.HACON_ALARMS)
            }
            Action.NATIVE_SCHEDULE -> clazz =
                SchedulesActivity::class.java
            Action.HACON_SYSTEM_MAP_PDF -> {
                clazz = UseTicketsActivity::class.java
                bundle.putString(AppConstants.TITLE, AppConstants.HACON_SYSTEM_MAP_PDF)
            }
            Action.HACON_SYSTEM_MAP -> {
                clazz = UseTicketsActivity::class.java
                bundle.putString(AppConstants.TITLE, AppConstants.HACON_SYSTEM_MAP)
            }
            Action.SUBSCRIPTION -> {
                clazz = ListSubscriptionsActivity::class.java
                bundle.putString(AppConstants.TITLE, menuItem.title)
            }
            Action.SECURITY_QUESTION -> {
                clazz = SecurityQuestionActivity::class.java
                bundle.putString(AppConstants.TITLE, menuItem.title)
                bundle.putBoolean(AppConstants.SECURITY_QUESTIONS_SETTING, true)
            }
            Action.NATIVE_APP_SUPPORT -> clazz =
                NativeAppSupportActivity::class.java
            Action.FARE_CAPPING -> {
                clazz = FareCappingActivity::class.java
                bundle.putString(AppConstants.TITLE, menuItem.title)
            }
            Action.REWARDS -> {
                clazz = RewardsActivity::class.java
                bundle.putString(AppConstants.TITLE, menuItem.title)
            }
            else -> return null
        }
        val intent = Intent(activity, clazz)
        intent.putExtras(bundle)
        intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.MENU)
        intent.putExtra(AppConstants.ACTION, menuItem.action!!.screen)
        return intent
    }

    @JvmStatic
    fun getStaticResourcesIntent(
        context: Context,
        menuItem: MenuItem,
        localeCode: String
    ): Intent? {
        val action = menuItem.action
        if (action == null || !action.isStaticResource) {
            return null
        }
        val link = action.link
        var resourceName = link?.substring(link.lastIndexOf("/") + 1)
        val extension = resourceName?.substring(resourceName.lastIndexOf(".") + 1)
        resourceName = resourceName?.substring(0, resourceName.lastIndexOf("."))?.toLowerCase()
        val fileName = resourceName + "_" + localeCode + "." + extension
        if (action.isPDF) {
            val intent = Intent(context, PdfRendererActivity::class.java)
            intent.putExtra(WebViewActivity.EXTRA_TITLE, menuItem.title)
            intent.putExtra(
                PdfRendererActivity.EXTRA_FILENAME,
                resourceName + "_" + localeCode + "." + extension
            )
            return intent
        } else if (action.isHTML) {
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtra(WebViewActivity.EXTRA_TITLE, menuItem.title)
            intent.putExtra(
                WebViewActivity.EXTRA_URL,
                "file:///android_asset/$fileName"
            )
            return intent
        } else if (action.isPNG || action.isJPEG) {
            val intent = Intent(context, ImageRendererActivity::class.java)
            intent.putExtra(WebViewActivity.EXTRA_TITLE, menuItem.title)
            intent.putExtra(
                PdfRendererActivity.EXTRA_FILENAME,
                resourceName + "_" + localeCode.replace("-", "_").toLowerCase()
            )
            return intent
        }
        return null
    }

    @JvmStatic
    fun getWebViewIntent(
        context: Context, menuItem: MenuItem, baseUrl: String, isNative: Boolean
    ): Intent? {
        val action = menuItem.action
        if (action == null || !isWebView(action, isNative)) {
            return null
        }
        // First parse if the type is hyperlink
        if (action.type
                .equals(Action.HYPERLINK, ignoreCase = true)
        ) {
            val customTabsIntent = CustomTabsIntent.Builder()
                .setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .enableUrlBarHiding()
                .setShowTitle(true)
                .build()
            CustomTabsHelper.addKeepAliveExtra(
                context,
                customTabsIntent.intent
            )
            CustomTabsActivityHelper
                .openCustomTab(
                    context, customTabsIntent,
                    Uri.parse(action.hyperlink),
                    WebViewFallback()
                )
            return null
        }
        // If the action is not hyperlink type then parse manually.
        val actionUrl: String
        actionUrl = when (action.screen) {
            Action.ACCOUNT -> Action.ACCOUNT_ACTION_URL
            Action.PASSWORD -> Action.PASSWORD_ACTION_URL
            Action.NOTIFICATION_SETTINGS -> Action.NOTIFICATION_SETTINGS_ACTION_URL
            Action.LINKED_ACCOUNTS -> Action.LINKED_ACCOUNTS_ACTION_URL
            Action.TERMS_OF_SERVICE -> Action.TERMS_OF_SERVICE_ACTION_URL
            else -> return null
        }
        val fullUrl = baseUrl + actionUrl
        val customTabsIntent = CustomTabsIntent.Builder()
            .setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary))
            .enableUrlBarHiding()
            .setShowTitle(true)
            .build()
        customTabsIntent.intent.putExtra(
            AUTHORIZATION,
            authorizationHeader
        )
        CustomTabsHelper.addKeepAliveExtra(context, customTabsIntent.intent)
        CustomTabsActivityHelper
            .openCustomTab(
                context, customTabsIntent,
                Uri.parse(fullUrl),
                WebViewFallback()
            )
        return null
    }

    private val authorizationHeader: String
        get() {
            if (BytemarkSDK.SDKUtility.getAuthToken() != null) {
                return " Bearer " + BytemarkSDK.SDKUtility.getAuthToken()
            } else if (BytemarkSDK.SDKUtility.getClientId() != null) {
                return " Client client_id=" + BytemarkSDK.SDKUtility.getClientId()
            }
            return ""
        }
}