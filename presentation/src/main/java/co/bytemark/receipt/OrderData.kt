package co.bytemark.receipt

import co.bytemark.sdk.model.common.Payment
import co.bytemark.widgets.util.ConsolidatedTicket

data class OrderData(
    val type: Int,
    var dateTime: String? = null,
    val payments: MutableList<Payment> = mutableListOf(),
    val orderItemList: MutableList<ConsolidatedTicket> = mutableListOf(),
    var discount: Int = 0,
    var subTotal: Int = 0,
    var total: Int = 0,
    var paymentStatus: String?
)