package co.bytemark.receipt

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.R
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.model.common.Payment
import kotlinx.android.synthetic.main.receipt_card_item.view.*

class CardsPaidWithAdapter(val confHelper: ConfHelper, private var payments: MutableList<Payment>) : RecyclerView.Adapter<CardsPaidWithAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.receipt_card_item, parent, false))

    override fun getItemCount() = payments.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.apply {
            cardTypeUsed.text = payments[position].cardType.replace("_", " ")
            cardTotal.text = String.format("%s %s", confHelper.getConfigurationPurchaseOptionsCurrencySymbol(payments[position].amount), confHelper.configurationPurchaseOptionsCurrencyCode)
            if (payments[position].cardLastFour != null && payments[position].cardLastFour?.isNotEmpty() == true) {
                stars.visibility = View.VISIBLE
                receiptLastFourOfCard.visibility = View.VISIBLE
                receiptLastFourOfCard.text = payments[position].cardLastFour
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}