package co.bytemark.receipt

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.Display
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.AppConstants.ORDER_TYPE_REFUND
import co.bytemark.helpers.AppConstants.ORDER_TYPE_SALE
import co.bytemark.incomm.SelectRetailerActivity
import co.bytemark.sdk.model.common.Order
import co.bytemark.sdk.model.common.OrderItem
import co.bytemark.widgets.util.*
import kotlinx.android.synthetic.main.fragment_receipt.*
import java.util.*
import javax.inject.Inject

class ReceiptFragment : BaseMvvmFragment() {

    private var order: Order? = null

    private lateinit var viewModel: ReceiptViewModel

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) =
        inflater.inflate(R.layout.fragment_receipt, container, false)!!

    companion object {
        fun newInstance(order: Order) = ReceiptFragment().withArgs {
            putParcelable("Receipt", order)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = createViewModel { CustomerMobileApp.appComponent.receiptViewModel }
        if (arguments?.getParcelable<Order>("Receipt") != null) {
            order = arguments?.getParcelable("Receipt")
            order?.let {

                it.fareMediumReceipt?.let {fareMediumReceipt ->
                    if(!fareMediumReceipt.cardNumber.isNullOrEmpty()){
                        fareMediumBorderView.show()
                        fareMediumCardNumberTextView.show()
                        fareMediumCardNumberTextView.text = getString(R.string.receipt_order_fare_medium_card_number_format, fareMediumReceipt.cardNumber)
                    }
                    if(!fareMediumReceipt.nickName.isNullOrEmpty()){
                        fareMediumBorderView.show()
                        fareMediumCardNameTextView.show()
                        fareMediumCardNameTextView.text = getString(R.string.receipt_order_fare_medium_card_nick_name,fareMediumReceipt.nickName)
                    }
                }

                val receiptReference = it.referenceNumber
                    ?.replace(".(?!$)".toRegex(), "$0 ")
                    ?.replace("-", "dash")
                    ?.replace("0", "zero")

                orderNumberTextView.contentDescription =
                    String.format(
                        getString(R.string.purchase_history_order_number_is),
                        receiptReference
                    )
                orderNumberTextView.text = String.format(
                    getString(
                        R.string.receipt_order_number_format,
                        it.referenceNumber
                    )
                )

                orderReceiptRecycler.layoutManager = LinearLayoutManager(context)
                orderReceiptRecycler.adapter =
                    OrderReceiptAdapter(mapToOrderData(it), confHelper, it.paymentType)
            }

            if (order?.paymentType == AppConstants.PAYMENT_TYPE_INCOMM.toUpperCase(Locale.getDefault()) &&
                order?.paymentStatus == AppConstants.PAYMENT_STATUS_PENDING
            ) {
                receiptCompleteTransactionButton.visibility = View.VISIBLE
            } else {
                receiptCompleteTransactionButton.visibility = View.GONE
            }
        }

        receiptCompleteTransactionButton.setOnClickListener {
            order?.uuid?.let { it1 -> viewModel.getBarcodeDetails(it1) }
            barcodeDetailProgressBar.announceForAccessibility(getString(R.string.loading))
        }
        analyticsPlatformAdapter.receiptScreenDisplayed()
        observeLiveData()
    }

    private fun observeLiveData() {
        viewModel.barcodeDetailsLiveData.observe(this, androidx.lifecycle.Observer {
            val intent = Intent(activity, SelectRetailerActivity::class.java)
            intent.putExtra(AppConstants.INTENT_INCOMM_BARCODE_DETAIL, it)
            intent.putExtra(AppConstants.INTENT_INCOMM_ORDER_AMOUNT, order?.total)
            intent.putExtra(AppConstants.INTENT_INCOMM_ORDER_UUID, order?.uuid)
            if (order?.orderItems?.isNotEmpty() == true && order?.orderItems?.get(0)?.product?.typeName == AppConstants.ORDER_TYPE_LOAD_STORE_WALLET) {
                intent.putExtra(AppConstants.INTENT_INCOMM_HISTORY_WALLET_LOAD, true)
            }
            startActivity(intent)
        })
        viewModel.errorLiveData.observe(this, androidx.lifecycle.Observer {
            it?.let { it1 -> handleError(it1) }
        })

        viewModel.displayLiveData.observe(this, androidx.lifecycle.Observer {
            when (it) {
                is Display.ProgressBar -> {
                    if (it.visibility == View.VISIBLE) {
                        barcodeDetailProgressBar.show()
                        receiptCompleteTransactionButton.invisible()
                    } else if (it.visibility == View.GONE) {
                        receiptCompleteTransactionButton.show()
                        barcodeDetailProgressBar.hide()
                    }
                }
            }
        })
    }

    private fun mapToOrderData(order: Order): List<OrderData> {
        val saleData = OrderData(ORDER_TYPE_SALE, paymentStatus = order.paymentStatus)
        val refundData = OrderData(ORDER_TYPE_REFUND, paymentStatus = order.paymentStatus)
        val mapper = ConsolidatedTicketMapper(confHelper)

        saleData.discount = order.totalOrderDiscount
        saleData.subTotal = order.originalTotal
        saleData.total = order.total

        val saleList = mutableListOf<OrderItem>()
        val refundList = mutableListOf<OrderItem>()

        // Looping through order item to find sale and refund data
        order.orderItems?.forEach {
            if (it.name == "Refund") {
                refundList.add(it)
            } else {
                saleList.add(it)
            }
        }

        saleData.orderItemList.addAll(mapper.getConsolidatedTickets(saleList))
        refundData.orderItemList.addAll(mapper.getConsolidatedTickets(refundList, true))

        order.payments?.forEach {
            if (it.amount < 0) {
                // consider as refund
                refundData.payments.add(it)
            } else {
                // consider as sale
                saleData.payments.add(it)
            }
        }

        // setting date time
        saleData.dateTime = order.orderItems?.first()?.timePurchased.toString()
        refundData.dateTime = order.orderItems?.last()?.timePurchased.toString()

        val orderDataList = mutableListOf<OrderData>()

        orderDataList.add(saleData)
        if (refundData.orderItemList.isNotEmpty()) {
            refundData.total = order.refund
            orderDataList.add(refundData)
        }
        return orderDataList
    }
}