package co.bytemark.receipt

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import co.bytemark.R
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.model.common.Payment
import kotlinx.android.synthetic.main.receipt_card_item.view.*
import java.util.*


class PaymentDetailsItemView(
    context: Context, payment: Payment?, confHelper: ConfHelper,
    accessibilityAnnouncementBuilder: StringBuilder,
    type: Int, paymentType: String?
) : LinearLayout(context) {
    init {

        val view = View.inflate(context, R.layout.receipt_card_item, this)
        with(view) {
            if (payment != null) {
                cardTypeUsed.text = payment.cardType.replace("_", " ").toUpperCase()
                cardTotal.text =
                    confHelper.getConfigurationPurchaseOptionsCurrencySymbol(payment.amount)
                accessibilityAnnouncementBuilder
                    .append("\n${cardTotal.text}")
                    .append(
                        "\n${
                        if (type == AppConstants.ORDER_TYPE_SALE)
                            context.getString(R.string.receipt_paid_with_voonly)
                        else
                            context.getString(R.string.receipt_refunded_to_voonly)
                        }\n"
                    )
                if (!payment.cardLastFour.isNullOrEmpty()) {
                    stars.visibility = View.VISIBLE
                    receiptLastFourOfCard.visibility = View.VISIBLE
                    receiptLastFourOfCard.text = payment.cardLastFour
                    accessibilityAnnouncementBuilder
                        .append("${cardTypeUsed.text}")
                        .append(context.getString(R.string.receipt_ending_with_voonly))
                        .append("\n${payment.cardLastFour?.replace(".(?!$)".toRegex(), "$0 ")}\n")
                }
                else {
                    if (paymentType != AppConstants.PAYMENT_TYPE_INCOMM.toUpperCase(Locale.getDefault())) {
                        accessibilityAnnouncementBuilder
                            .append("\n")
                            .append(cardTypeUsed.text.toString())
                    }
                }
            }
            else if (paymentType != null && paymentType == AppConstants.PAYMENT_TYPE_VOUCHER_CODE) {
                val voucherStr = context.getText(R.string.voucher_code_str)
                cardTypeUsed.text = voucherStr
                accessibilityAnnouncementBuilder
                    .append("\n$voucherStr\n")
            }
            if (paymentType != null && paymentType == AppConstants.PAYMENT_TYPE_INCOMM.toUpperCase(
                    Locale.getDefault()
                )
            ) {
                val cash = context.getString(R.string.receipt_payment_method_cash)
                cardTypeUsed.text = cash
                accessibilityAnnouncementBuilder
                    .append("\n$cash\n")
            }
        }

    }

}
