package co.bytemark.receipt

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.sdk.model.common.Order


class ReceiptActivity : MasterActivity() {

    override fun getLayoutRes() = R.layout.activity_receipt

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val order = intent.getParcelableExtra<Order>("Receipt")
        if (order != null) {
            setToolbarTitle(getString(R.string.purchase_history_receipt))

            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.receipt_container, ReceiptFragment.newInstance(order))
                    .commit()
        }
    }

    override fun useHamburgerMenu() = false

}
