package co.bytemark.receipt

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.R
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.model.common.Itinerary
import co.bytemark.sdk.model.common.OrderItem
import co.bytemark.sdk.model.common.Pass
import co.bytemark.sdk.model.config.RowType
import kotlinx.android.synthetic.main.receipt_ticket_item.view.*
import java.lang.StringBuilder
import java.util.*

class ReceiptPassAdapter(val confHelper: ConfHelper, private var orderItems: MutableList<OrderItem>):  RecyclerView.Adapter<ReceiptPassAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.receipt_ticket_item, parent, false))

    override fun getItemCount() = orderItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.apply {
            orderItems[position].product?.let {
                ticketTitle.text = it.labelName
                ticketSubType.text = StringBuilder().append(" (").append(it.subTypeName).append(")").toString()
                ticketPrice.text = StringBuilder().append(confHelper.getConfigurationPurchaseOptionsCurrencySymbol(it.salePrice)).append(" ").append(confHelper.configurationPurchaseOptionsCurrencyCode).toString()

                if (orderItems[position].passes != null && orderItems[position].passes.size > 0) {
                    val displayLongNameForOd = confHelper.isDisplayLongNameForOd(it.organizationUuid)
                    val originDestination = getOriginDestination(displayLongNameForOd, orderItems[position].passes[0])
                    ticketOriginDestination.visibility = View.GONE
                    if (originDestination.isNotEmpty()) {
                        ticketOriginDestination.text = originDestination
                        ticketOriginDestination.visibility = View.VISIBLE
                    }
                    ticketLicenceSpot.visibility = View.GONE

                    if (getLicenseAndSpot(orderItems[position]).isNotEmpty()) {
                        ticketLicenceSpot.text = getLicenseAndSpot(orderItems[position])
                        ticketLicenceSpot.visibility = View.VISIBLE
                    }
                }

                if (orderItems[position].discount > 0) {
                    linearLayoutDiscount.visibility = View.VISIBLE
                    textViewDiscount.text = StringBuilder("-").append(confHelper.getConfigurationPurchaseOptionsCurrencySymbol(orderItems[position].discount)).toString()
                }

            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    private fun getLicenseAndSpot(orderItem: OrderItem): String {
        try {
            if (orderItem.passes[0].inputFields != null && orderItem.passes[0].inputFields?.licensePlateNumber?.isNotEmpty()!! &&
                    orderItem.passes[0].inputFields?.spotNumber?.isNotEmpty()!!) {
                return orderItem.passes[0].inputFields?.licensePlateNumber + " / " +
                        orderItem.passes[0].inputFields?.spotNumber
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""
    }

    private fun getOriginDestination(displayLongNameForOd: Boolean?, pass: Pass): String {
        var origin: String? = null
        var destination: String? = null
        pass.filterAttributes?.let {
            val itinerary: Itinerary? = pass.filterAttributes?.itinerary
            itinerary?.origin?.let {
                origin = displayLongNameForOd.let { itinerary.origin.longName!![Locale.getDefault().language] }
                        ?: itinerary.origin.shortName!![Locale.getDefault().language]
            }

            itinerary?.destination?.let {
                destination = displayLongNameForOd.let { itinerary.destination.longName!![Locale.getDefault().language] }
                        ?: itinerary.destination.shortName!![Locale.getDefault().language]
            }
        } ?: kotlin.run {
            origin = pass.getAttribute(RowType.ORIGIN_NAME)
            destination = pass.getAttribute(RowType.DESTINATION_NAME)
            if (origin == null) {
                origin = pass.getAttribute(RowType.ORIGIN_CODE)

            }
            if (destination == null)
                destination = pass.getAttribute(RowType.DESTINATION_CODE)
        }

        return if (origin != null && destination != null) {
            "$origin > $destination"
        } else ""

    }
}