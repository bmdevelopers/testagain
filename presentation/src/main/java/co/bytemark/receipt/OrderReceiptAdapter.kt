package co.bytemark.receipt

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.Api.ApiUtility
import co.bytemark.sdk.Utils
import co.bytemark.shopping_cart.TotalDetails
import co.bytemark.shopping_cart.TotalDetailsItemView
import kotlinx.android.synthetic.main.order_receipt_item_row.view.*
import java.util.*

class OrderReceiptAdapter(
    private val orderReceiptList: List<OrderData>,
    private val confHelper: ConfHelper,
    private val paymentType: String?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class OrderReceiptViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(orderData: OrderData, confHelper: ConfHelper, paymentType: String?) {

            val date = ApiUtility.getCalendarFromS(orderData.dateTime)?.time

            with(itemView) {
                date?.let {
                    dateTimeTextView.text = confHelper.dateDisplayFormat.format(it).plus(" ")
                        .plus(confHelper.timeDisplayFormat.format(it)).plus(" ")
                        .plus(Utils.localTimeZone)
                }

                // preparing accessibility announcement
                val accessibilityAnnouncementBuilder = StringBuilder()
                    .append(
                        "${
                        if (orderData.type == AppConstants.ORDER_TYPE_SALE)
                            context.getString(R.string.receipt_ordered_on_voonly)
                        else
                            context.getString(R.string.receipt_refund_on_voonly)
                        }\n"
                    )
                    .append(dateTimeTextView.text.toString())
                    .append("\n\n ${context.getString(R.string.receipt_payment_details_voonly)} \n")

                orderData.payments.forEach {
                    paymentListLayout.addView(
                        PaymentDetailsItemView(
                            context,
                            it,
                            confHelper,
                            accessibilityAnnouncementBuilder,
                            orderData.type,
                            paymentType
                        )
                    )
                }

                if (orderData.payments.isEmpty() && orderData.type == AppConstants.ORDER_TYPE_SALE && paymentType == AppConstants.PAYMENT_TYPE_VOUCHER_CODE) {
                    paymentListLayout.addView(
                        PaymentDetailsItemView(
                            context,
                            null,
                            confHelper,
                            accessibilityAnnouncementBuilder,
                            orderData.type,
                            paymentType
                        )
                    )
                }

                if (orderData.payments.isEmpty() && paymentType == AppConstants.PAYMENT_TYPE_INCOMM.toUpperCase(
                        Locale.ROOT
                    )
                ) {
                    paymentListLayout.addView(
                        PaymentDetailsItemView(
                            context,
                            null,
                            confHelper,
                            accessibilityAnnouncementBuilder,
                            orderData.type,
                            paymentType
                        )
                    )
                }

                accessibilityAnnouncementBuilder.append("\n\n ${context.getString(R.string.receipt_items_voonly)} \n")
                orderData.orderItemList.forEach {
                    orderItemListLayout.addView(
                        TotalDetailsItemView(
                            context, TotalDetails.Builder()
                                .setTitle(it.name)
                                .setPrice(
                                    confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
                                        it.price
                                    )
                                )
                                .setQuantity(it.quantity.toString() + "x")
                                .setOriginDestination(it.OriginDest)
                                .setTextColor(confHelper.collectionThemePrimaryTextColor)
                                .build()
                        )
                    )

                    accessibilityAnnouncementBuilder
                        .append("\n ${it.name}")
                        .append("\n ${it.OriginDest?.replace("/", " to ") ?: ""}")
                        .append("\n ${context.getString(R.string.quantity_voonly)}")
                        .append(it.quantity)
                        .append(
                            "\n ${
                            if (orderData.type == AppConstants.ORDER_TYPE_SALE)
                                context.getString(R.string.receipt_amount_voonly)
                            else
                                context.getString(R.string.receipt_refunded_amount_voonly)
                            }"
                        )
                        .append("\n")
                        .append(confHelper.getConfigurationPurchaseOptionsCurrencySymbol(it.price))
                }

                when {
                    (paymentType == AppConstants.PAYMENT_TYPE_INCOMM.toUpperCase(Locale.getDefault()) &&
                            orderData.paymentStatus.equals(AppConstants.PAYMENT_STATUS_PENDING)) -> {
                        receiptTypeTextView.text =
                            context.getText(R.string.purchase_history_unpaid_status)
                        receiptTypeTextView.background =
                            context.getDrawable(R.drawable.receipt_type_unpaid)
                        subtotalTextview.text =
                            confHelper.getConfigurationPurchaseOptionsCurrencySymbol(orderData.subTotal)
                        accessibilityAnnouncementBuilder
                            .append("\n ${context.getString(R.string.receipt_order_status_is_voonly)}")
                            .append("${receiptTypeTextView.text}")
                            .append("\n ${context.getString(R.string.receipt_subtotal_amount_is_voonly)}")
                            .append("\n ${subtotalTextview.text}")
                    }
                    orderData.type == AppConstants.ORDER_TYPE_SALE -> {
                        receiptTypeTextView.text = context.getText(R.string.receipt_type_sale)
                        receiptTypeTextView.background =
                            context.getDrawable(R.drawable.receipt_type_sale)
                        subtotalTextview.text =
                            confHelper.getConfigurationPurchaseOptionsCurrencySymbol(orderData.subTotal)
                        accessibilityAnnouncementBuilder
                            .append("\n ${context.getString(R.string.receipt_subtotal_amount_is_voonly)}")
                            .append("\n ${subtotalTextview.text}")
                    }
                    else -> {
                        receiptTypeTextView.text = context.getText(R.string.receipt_type_refund)
                        receiptTypeTextView.background =
                            context.getDrawable(R.drawable.receipt_type_refund_bg)
                        subtotalTextview.visibility = View.GONE
                        subtotalLabel.visibility = View.GONE
                        separatorTotal.visibility = View.GONE
                    }
                }

                if (orderData.discount > 0) {
                    discount.text =
                        confHelper.getConfigurationPurchaseOptionsCurrencySymbol(-orderData.discount)
                    accessibilityAnnouncementBuilder
                        .append("\n ${context.getString(R.string.receipt_discount_voonly)}")
                        .append("\n ${discount.text}\n")
                } else {
                    discount.visibility = View.GONE
                    discountLabel.visibility = View.GONE
                }

                totalTextView.text =
                    confHelper.getConfigurationPurchaseOptionsCurrencySymbol(orderData.total)
                accessibilityAnnouncementBuilder
                    .append("\n ${context.getString(R.string.purchase_history_total_is_voonly)}")
                    .append(" ${totalTextView.text}")

                itemView.contentDescription = accessibilityAnnouncementBuilder.toString()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.order_receipt_item_row, parent, false)
        return OrderReceiptViewHolder(view)
    }

    override fun getItemCount() = orderReceiptList.size

    override fun onBindViewHolder(holer: RecyclerView.ViewHolder, position: Int) {
        (holer as OrderReceiptViewHolder).bind(orderReceiptList[position], confHelper, paymentType)
    }
}
