package co.bytemark.receipt

import android.view.View
import androidx.lifecycle.MutableLiveData
import co.bytemark.CustomerMobileApp
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.payments.IncommBarcodeDetailUseCase
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.incomm.IncommBarcodeDetail
import kotlinx.coroutines.launch
import javax.inject.Inject

class ReceiptViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var incommBarcodeDetailUseCase: IncommBarcodeDetailUseCase

    val displayLiveData by lazy { MutableLiveData<Display>() }

    val barcodeDetailsLiveData by lazy { MutableLiveData<IncommBarcodeDetail>() }

    init {
        CustomerMobileApp.component.inject(this)
    }

    fun getBarcodeDetails(orderUuid: String) = uiScope.launch {
        displayLiveData.value = Display.ProgressBar(View.VISIBLE)
        when (val result = incommBarcodeDetailUseCase(orderUuid)) {
            is Result.Success -> {
                barcodeDetailsLiveData.value = result.data
            }
            is Result.Failure -> {
                errorLiveData.value = result.bmError.first()
                displayLiveData.value = Display.ProgressBar(View.GONE)
            }
        }
    }
}