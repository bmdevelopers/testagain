package co.bytemark.widgets

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import androidx.annotation.ColorInt
import androidx.appcompat.widget.AppCompatImageView
import co.bytemark.widgets.util.Util
import kotlin.math.min

class CircleBorderImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AppCompatImageView(context, attrs, defStyle) {
    private var strokePaint: Paint = Paint()

    @ColorInt
    var strokeColor = Color.BLACK
    var strokeWidth = Util.dpToPx(2.0)

    init {
        strokePaint.flags = Paint.ANTI_ALIAS_FLAG
        strokePaint.color = strokeColor
        strokePaint.style = Paint.Style.STROKE
        strokePaint.strokeWidth = strokeWidth.toFloat()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        strokePaint.color = currentTintedColor
        var radius = min(width, height) / 2
        radius -= strokeWidth
        val cx = width / 2
        val cy = height / 2
        canvas.drawCircle(cx.toFloat(), cy.toFloat(), radius.toFloat(), strokePaint)
    }

    private val currentTintedColor: Int
        get() {
            strokeColor = (if (imageTintList == null) {
                return strokeColor
            } else {
                imageTintList?.defaultColor
            }) ?: Color.BLACK
            return strokeColor
        }

    fun setBorderStrokeWidth(sizeInPx: Int) {
        strokePaint.strokeWidth = sizeInPx.toFloat()
        strokeWidth = sizeInPx
        invalidate()
    }
}