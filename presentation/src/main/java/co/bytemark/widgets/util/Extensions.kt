package co.bytemark.widgets.util

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.SpannableStringBuilder
import android.text.style.TypefaceSpan
import android.util.TypedValue
import android.view.View
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityManager
import android.view.accessibility.AccessibilityNodeInfo
import androidx.annotation.StringRes
import androidx.core.text.inSpans
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import co.bytemark.R
import co.bytemark.sdk.hide
import co.bytemark.sdk.show
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.textfield.TextInputEditText


// https://android.jlelse.eu/navigation-drawer-styling-under-material-design-f0767882e692
fun Context.navigationDrawerWidth(actionBarHeight: Int): Int {
    return actionBarHeight * 5
}

fun Context.getActionBarHeight(): Int {
    // Calculate ActionBar height
    return calculateActionBarHeight(this)
}

fun Context.calculateActionBarHeight(context: Context): Int {
    var actionBarHeight = 56
    val tv = TypedValue()
    if (context.theme.resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
        actionBarHeight =
            TypedValue.complexToDimensionPixelSize(tv.data, context.resources.displayMetrics)
    }
    return actionBarHeight
}

inline fun <reified T : ViewModel> Fragment.createViewModel(crossinline factory: () -> T): T =
    T::class.java.let { clazz ->
        ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                if (modelClass == clazz) {
                    @Suppress("UNCHECKED_CAST")
                    return factory() as T
                }
                throw IllegalArgumentException("Unexpected argument: $modelClass")
            }
        }).get(clazz)
    }

inline fun <reified T : ViewModel> FragmentActivity.createViewModel(crossinline factory: () -> T): T =
    T::class.java.let { clazz ->
        ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                if (modelClass == clazz) {
                    @Suppress("UNCHECKED_CAST")
                    return factory() as T
                }
                throw IllegalArgumentException("Unexpected argument: $modelClass")
            }
        }).get(clazz)
    }


inline fun <reified T : ViewModel> Fragment.getViewModel(clz: T): T = T::class.java.let { clazz ->
    this.createViewModel { clz }
}

fun Context.isOnline() = (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
    .activeNetworkInfo?.isConnected == true

fun Context.connectionErrorDialog() {
    MaterialDialog.Builder(this)
        .title(R.string.change_password_popup_conErrorTitle)
        .content(R.string.change_password_Popup_con_error_body)
        .positiveText(R.string.popup_dismiss)
        .show()
}

fun Context?.showMaterialDialog(
    @StringRes title: Int,
    @StringRes body: Int,
    @StringRes positiveButton: Int,
    @StringRes negativeButton: Int = 0,
    positiveAction: ((materialDialog: MaterialDialog, whichDialog: DialogAction) -> Unit)? = null,
    negativeAction: ((materialDialog: MaterialDialog, whichDialog: DialogAction) -> Unit)? = null
) {

    this?.let {
        MaterialDialog.Builder(it)
            .title(title)
            .content(body)
            .positiveText(positiveButton)
            .onPositive { dialog, which -> positiveAction?.invoke(dialog, which) }
            .negativeText(negativeButton)
            .onNegative { dialog, which -> negativeAction?.invoke(dialog, which) }
            .show()
    }
}


fun View?.show() {
    this?.let {
        it.visibility = View.VISIBLE
    }
}

fun View?.hide() {
    this?.let {
        it.visibility = View.GONE
    }
}

fun View?.invisible() {
    this?.let {
        it.visibility = View.INVISIBLE
    }
}

fun String.addSuffix(str: String) = this + str

fun String.addPrefix(str: String) = str + this

fun Context.announce(text: String) {
    val manager = getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager
    if (manager.isEnabled) {
        val e = AccessibilityEvent.obtain()
        e.eventType = AccessibilityEvent.TYPE_ANNOUNCEMENT
        e.className = javaClass.name
        e.packageName = packageName
        e.text.add(text)
        manager.sendAccessibilityEvent(e)
    }
}

fun hideViewGroup(vararg views: View) = views.forEach { it.hide() }

fun showViewGroup(vararg views: View) = views.forEach { it.show() }

fun String.getDigitsOnly(): String {
    return this.replace("[^0-9]".toRegex(), "")
}

inline fun <T : Fragment> T.withArgs(argsBuilder: Bundle.() -> Unit): T =
    this.apply {
        arguments = Bundle().apply(argsBuilder)
    }

fun Fragment.postDelay(delay: Long, call: () -> Unit) {
    view?.postDelayed(call, delay)
}

fun postDelay(delay: Long, call: () -> Unit) {
    Handler(Looper.getMainLooper()).postDelayed(call, delay)
}

fun Context?.setCursorForEditTextInAccessibilityMode(
    textInputEditTextList:
    MutableList<TextInputEditText>
) {
    textInputEditTextList.forEach { editText ->
        editText.setOnClickListener {
            if (Util.isTalkBackAccessibilityEnabled(this))
                editText.text?.length?.let { index ->
                    editText.setSelection(index)
                }
        }
    }
}

inline fun SpannableStringBuilder.font(
    fontFamily: String,
    builderAction: SpannableStringBuilder.() -> Unit
) = inSpans(TypefaceSpan(fontFamily), builderAction = builderAction)


fun View.requestAccessibilityFocus() {
    requestFocus()
    sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
    performAccessibilityAction(AccessibilityNodeInfo.ACTION_ACCESSIBILITY_FOCUS, null)
}

fun Context.isValidContext() = this !is Activity || (!this.isDestroyed && !this.isFinishing)