package co.bytemark.widgets.util

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.TimeInterpolator
import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.LinearInterpolator
import android.widget.FrameLayout
import android.widget.ImageView
import java.lang.ref.WeakReference
import kotlin.math.max

class AddToCartAnimationUtil {
    private var targetView: View? = null
    private var destView: View? = null
    private var originX = 0f
    private var originY = 0f
    private var destX = 0f
    private var destY = 0f
    private var moveDuration = DEFAULT_DURATION
    private val disappearDuration = DEFAULT_DURATION_DISAPPEAR
    private var context: WeakReference<Activity?>? = null
    private var bitmap: Bitmap? = null
    private var imageView: ImageView? = null
    fun attachActivity(activity: Activity?): AddToCartAnimationUtil {
        context = WeakReference(activity)
        return this
    }

    fun setTargetView(view: View?): AddToCartAnimationUtil {
        targetView = view
        targetView?.width?.toFloat()?.let { targetView?.height?.toFloat()?.let { it1 ->
            setOriginRect(it,
                it1
            )
        } }
        return this
    }

    private fun setOriginRect(x: Float, y: Float): AddToCartAnimationUtil {
        originX = x
        originY = y
        return this
    }

    private fun setDestRect(x: Float, y: Float): AddToCartAnimationUtil {
        destX = x
        destY = y
        return this
    }

    fun setDestView(view: View?): AddToCartAnimationUtil {
        destView = view
        destView?.width?.toFloat()?.let { setDestRect(it, destView?.width?.toFloat()!!) }
        return this
    }

    fun setMoveDuration(duration: Int): AddToCartAnimationUtil {
        moveDuration = duration
        return this
    }

    private fun prepare(): Boolean {
        if (context?.get() != null) {
            val decoreView = context?.get()?.window?.decorView as ViewGroup
            bitmap = targetView?.width?.let { targetView?.height?.let { it1 ->
                drawViewToBitmap(targetView, it,
                    it1
                )
            } }
            if (imageView == null) imageView = ImageView(context?.get())
            imageView?.setImageBitmap(bitmap)
            val src = IntArray(2)
            targetView?.getLocationOnScreen(src)
            val params =
                targetView?.width?.let { targetView?.height?.let { it1 ->
                    FrameLayout.LayoutParams(it,
                        it1
                    )
                } }
            params?.setMargins(src[0], src[1], 0, 0)
            if (imageView?.parent == null) decoreView.addView(imageView, params)
        }
        return true
    }

    fun startAnimation() {
        if (prepare()) {
            animator.start()
        }
    }

    private val animator: AnimatorSet
        get() {
            val endRadius = max(destX, destY) / 2
            val startRadius = max(originX, originY)
            val animator: Animator = ObjectAnimator.ofFloat(
                imageView,
                "drawableRadius",
                startRadius,
                endRadius * 1.05f,
                endRadius * 0.9f,
                endRadius
            )
            animator.interpolator = AccelerateInterpolator()
            val scaleFactor = 0.25f
            val scaleAnimatorY: Animator = ObjectAnimator.ofFloat(
                imageView,
                View.SCALE_Y,
                1f,
                1f,
                scaleFactor,
                scaleFactor
            )
            val scaleAnimatorX: Animator = ObjectAnimator.ofFloat(
                imageView,
                View.SCALE_X,
                1f,
                1f,
                scaleFactor,
                scaleFactor
            )
            val animatorCircleSet = AnimatorSet()
            animatorCircleSet.duration = DEFAULT_DURATION.toLong()
            animatorCircleSet.playTogether(scaleAnimatorX, scaleAnimatorY, animator)
            animatorCircleSet.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {}
                override fun onAnimationEnd(animation: Animator) {
                    val src = IntArray(2)
                    val dest = IntArray(2)
                    imageView?.getLocationOnScreen(src)
                    destView?.getLocationOnScreen(dest)
                    val y = imageView?.y
                    val x = imageView?.x
                    val translatorX: Animator =
                        x?.let {
                            ObjectAnimator.ofFloat(
                                imageView,
                                View.X,
                                it,
                                x + dest[0] - (src[0] + (originX * scaleFactor - 2 * endRadius * scaleFactor) / 2) + (0.5f * destX - scaleFactor * endRadius)
                            )
                        }!!
                    translatorX.interpolator = TimeInterpolator { input: Float ->
                        (-Math.pow(
                            input - 1.toDouble(),
                            2.0
                        ) + 1f).toFloat()
                    }
                    val translatorY: Animator =
                        y?.let {
                            ObjectAnimator.ofFloat(
                                imageView,
                                View.Y,
                                it,
                                y + dest[1] - (src[1] + (originY * scaleFactor - 2 * endRadius * scaleFactor) / 2) + (0.5f * destY - scaleFactor * endRadius)
                            )
                        }!!
                    translatorY.interpolator = LinearInterpolator()
                    val animatorMoveSet = AnimatorSet()
                    animatorMoveSet.playTogether(translatorX, translatorY)
                    animatorMoveSet.duration = moveDuration.toLong()
                    val animatorDisappearSet = AnimatorSet()
                    val disappearAnimatorY: Animator =
                        ObjectAnimator.ofFloat(
                            imageView,
                            View.SCALE_Y,
                            scaleFactor,
                            0f
                        )
                    val disappearAnimatorX: Animator =
                        ObjectAnimator.ofFloat(
                            imageView,
                            View.SCALE_X,
                            scaleFactor,
                            0f
                        )
                    animatorDisappearSet.duration = disappearDuration.toLong()
                    animatorDisappearSet.playTogether(disappearAnimatorX, disappearAnimatorY)
                    val total = AnimatorSet()
                    total.playSequentially(animatorMoveSet, animatorDisappearSet)
                    total.addListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {}
                        override fun onAnimationEnd(animation: Animator) {
                            reset()
                        }

                        override fun onAnimationCancel(animation: Animator) {}
                        override fun onAnimationRepeat(animation: Animator) {}
                    })
                    total.start()
                }

                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
            })
            return animatorCircleSet
        }

    private fun drawViewToBitmap(
        view: View?,
        width: Int,
        height: Int
    ): Bitmap {
        val drawable: Drawable = BitmapDrawable()
        val dest = Bitmap.createBitmap(
            width,
            height,
            Bitmap.Config.ARGB_8888
        )
        val c = Canvas(dest)
        drawable.bounds = Rect(0, 0, width, height)
        drawable.draw(c)
        view?.draw(c)
        return dest
    }

    private fun reset() {
        bitmap?.recycle()
        bitmap = null
        if (imageView?.parent != null) (imageView?.parent as ViewGroup).removeView(imageView)
        imageView = null
        targetView?.visibility = View.VISIBLE
    }

    companion object {
        private const val DEFAULT_DURATION = 500
        private const val DEFAULT_DURATION_DISAPPEAR = 200
    }
}