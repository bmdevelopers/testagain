package co.bytemark.widgets.util

import android.accessibilityservice.AccessibilityServiceInfo
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.res.Resources
import android.graphics.Insets
import android.graphics.Point
import android.os.Build
import android.provider.Settings
import android.provider.Settings.SettingNotFoundException
import android.text.Html
import android.text.Spanned
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.WindowInsets
import android.view.accessibility.AccessibilityManager
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.model.common.User

/**
 * Created by Arunkumar on 20/07/16.
 */
object Util {
    @JvmStatic
    fun dpToPx(dp: Double): Int {
        val displayMetrics =
            Resources.getSystem().displayMetrics
        return (dp * displayMetrics.density + 0.5).toInt()
    }

    /**
     * Map a value within a given range to another range.
     *
     * @param value    the value to map
     * @param fromLow  the low end of the range the value is within
     * @param fromHigh the high end of the range the value is within
     * @param toLow    the low end of the range to map to
     * @param toHigh   the high end of the range to map to
     * @return the mapped value
     */
    @JvmStatic
    fun mapValueFromRangeToRange(
        value: Double,
        fromLow: Double,
        fromHigh: Double,
        toLow: Double,
        toHigh: Double
    ): Double {
        val fromRangeSize = fromHigh - fromLow
        val toRangeSize = toHigh - toLow
        val valueScale = (value - fromLow) / fromRangeSize
        return toLow + valueScale * toRangeSize
    }

    @JvmStatic
    fun <V : View?> registerForLayout(
        view: V,
        cb: LayoutDoneListener<V>
    ) {
        val viewTreeObserver = view!!.viewTreeObserver
        if (viewTreeObserver.isAlive) {
            viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    cb.onLayoutFinished(view)
                }
            })
        }
    }

    fun fromHtml(source: String?): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(source)
        }
    }

    @JvmStatic
    fun navigationDrawerWidth(actionBarHeight: Int): Int {
        return actionBarHeight * 5
    }

    @JvmStatic
    fun getActionBarHeight(context: Context): Int {
        return calculateActionBarHeight(context)
    }

    private fun calculateActionBarHeight(context: Context): Int {
        var actionBarHeight = 56
        val tv = TypedValue()
        if (context.theme.resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(
                tv.data,
                context.resources.displayMetrics
            )
        }
        return actionBarHeight
    }

    fun getSystemBrightness(activity: Activity?): Float {
        return if (activity != null) {
            try {
                Settings.System.getFloat(
                    activity.contentResolver,
                    Settings.System.SCREEN_BRIGHTNESS
                )
            } catch (e: SettingNotFoundException) {
                0.5f
            }
        } else 0.5f
    }

    @JvmStatic
    fun isTalkBackAccessibilityEnabled(context: Context?): Boolean {
        val manager =
            context?.getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager
        if (manager.isEnabled) {
            val serviceInfoList =
                manager.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_SPOKEN)
            return serviceInfoList.size > 0
        }
        return false
    }

    interface LayoutDoneListener<V : View?> {
        fun onLayoutFinished(view: V)
    }

    @JvmStatic
    fun sendTokenToECUI(
        context: Context?,
        refreshedToken: String?
    ) {
        try {
            val clsUtil =
                Class.forName(AppConstants.ELERTS_UTIL_CLASS_NAME)
            val utilMethodName = AppConstants.ELERTS_SEND_DEVICE_TOKEN_METHOD_NAME
            val utilMethod = clsUtil.getMethod(
                utilMethodName,
                Context::class.java,
                String::class.java
            )
            utilMethod.invoke(
                null,
                context,
                refreshedToken
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun registerUserAndOrg(
        application: Application?,
        email: String?,
        firstName: String?,
        lastName: String?,
        mobile: String?,
        uuid: String?,
        isOnlyUserUpdate: Boolean
    ) {
        try {
            invokeECUIUtilMethod(AppConstants.ELERTS_CLEAR_ALERTS_METHOD_NAME, application?.applicationContext, null)
            val clsUtil =
                Class.forName(AppConstants.ELERTS_UTIL_CLASS_NAME)
            val utilMethodName = AppConstants.ELERTS_REG_METHOD_NAME
            val utilMethod = clsUtil.getMethod(
                utilMethodName,
                Application::class.java,
                String::class.java,
                String::class.java,
                String::class.java,
                String::class.java,
                String::class.java,
                Boolean::class.java
            )
            utilMethod.invoke(null, application, email, firstName, lastName, mobile, uuid, isOnlyUserUpdate)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    fun regUserAndOrgForECUI(application: Application) {
        var user: User? = null
        if (!BytemarkSDK.SDKUtility.isClientAndOrgRegisteredForElerts() && BytemarkSDK.isLoggedIn()) {
            try {
                user = BytemarkSDK.SDKUtility.getUserInfo()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (user != null) {
                registerUserAndOrg(
                    application,
                    user.email,
                    user.firstName,
                    user.lastName,
                    user.mobile,
                    user.uuid,
                    false
                )
            }
        }
    }

    @JvmStatic
    fun registerECUINotifications(context: Context?) {
        if (!BytemarkSDK.SDKUtility.isDeviceTokenSentToElerts()) {
            sendTokenToECUI(context, BytemarkSDK.SDKUtility.getDeviceToken())
        } else {
            invokeECUIUtilMethod(AppConstants.ELERTS_API_HELPER_METHOD_NAME, context, null)
        }
    }

    @JvmStatic
    fun getResId(resName: String, c: Class<*>): Int {
        return try {
            val idField = c.getDeclaredField(resName)
            idField.getInt(idField)
        } catch (e: Exception) {
            e.printStackTrace()
            -1
        }
    }

    @JvmStatic
    fun invokeECUIUtilMethod(methodName: String, context: Context?, activity: Activity?) {
        try {
            val clsUtil =
                Class.forName(AppConstants.ELERTS_UTIL_CLASS_NAME)
            when (methodName) {
                AppConstants.ELERTS_API_HELPER_METHOD_NAME -> {
                    val helperMethod =
                        clsUtil.getMethod(methodName, Context::class.java)
                    helperMethod.invoke(null, context)
                }
                AppConstants.ELERTS_SHOW_CALL_PROMPT_METHOD_NAME -> {
                    val callMethod =
                        clsUtil.getMethod(methodName, Activity::class.java)
                    callMethod.invoke(null, activity)
                }
                AppConstants.ELERTS_CLEAR_ALERTS_METHOD_NAME -> {
                    val clearAlertMethod =
                        clsUtil.getMethod(methodName, Context::class.java)
                    clearAlertMethod.invoke(null, context)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    fun getClass(className: String): Class<*>? {
        return try {
            when (className) {
                AppConstants.ELERTS_MESSAGE_CLASS_NAME -> {
                    val clsSdk =
                        Class.forName(AppConstants.ELERTS_MESSAGE_CLASS_NAME)
                    val field =
                        clsSdk.getField(AppConstants.ELERTS_MESSAGE_CLASS_FIELD_NAME)
                    field[null] as Class<*>
                }
                AppConstants.ELERTS_REPORT_CLASS_NAME -> {
                    Class.forName(AppConstants.ELERTS_REPORT_CLASS_NAME)
                }
                else -> null
            }
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }
    @JvmStatic
    fun userUpdate(application: Application?, user: User?) {
        user?.let {
            registerUserAndOrg(application, it.email, it.firstName, it.lastName, it.mobile, it.uuid, true)
        }
    }

    @JvmStatic
    fun getOrgs(application: Application, activity: Activity?, launchReport: Boolean) {
        try {
            val clsUtil =
                Class.forName(AppConstants.ELERTS_UTIL_CLASS_NAME)
            val utilMethodName = AppConstants.ELERTS_GET_ORG_METHOD_NAME
            val utilMethod = clsUtil.getMethod(
                utilMethodName,
                Application::class.java,
                Activity::class.java,
                Boolean::class.java
            )
            utilMethod.invoke(null, application, activity, launchReport)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    fun getScreenSize( activity: Activity): Point {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val windowMetrics = activity.windowManager.currentWindowMetrics
            val insets: Insets = windowMetrics.windowInsets
                    .getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
            Point(windowMetrics.bounds.width() - insets.left - insets.right,
                    windowMetrics.bounds.height() - insets.top - insets.bottom)
        } else {
            val displayMetrics = DisplayMetrics()
            activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
            Point(displayMetrics.widthPixels, displayMetrics.heightPixels)
        }
    }
}