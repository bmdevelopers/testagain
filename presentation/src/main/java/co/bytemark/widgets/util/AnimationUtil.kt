package co.bytemark.widgets.util

import android.animation.PropertyValuesHolder
import android.view.View
import androidx.annotation.FloatRange
import androidx.cardview.widget.CardView

object AnimationUtil {
    @JvmStatic
    fun calcPropertyHolders(
        viewToAnimate: View,
        translationY: Float,
        scale: Float,
        elevation: Float
    ): Array<PropertyValuesHolder?> {
        viewToAnimate.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        val isCard = viewToAnimate is CardView
        val properties: Array<PropertyValuesHolder?>
        properties = if (isCard) {
            arrayOfNulls(5)
        } else {
            arrayOfNulls(4)
        }
        properties[0] =
            PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, translationY)
        properties[1] = PropertyValuesHolder.ofFloat(View.TRANSLATION_X, 0f)
        properties[2] = PropertyValuesHolder.ofFloat(View.SCALE_X, scale)
        properties[3] = PropertyValuesHolder.ofFloat(View.SCALE_Y, scale)
        if (isCard) properties[4] = PropertyValuesHolder.ofFloat("cardElevation", elevation)
        return properties
    }

    @JvmStatic
    fun calcXPropertyHolders(
        viewToAnimate: View,
        translationX: Float,
        elevation: Float
    ): Array<PropertyValuesHolder?> {
        val isCard = viewToAnimate is CardView
        val properties: Array<PropertyValuesHolder?>
        properties = if (isCard) {
            arrayOfNulls(3)
        } else {
            arrayOfNulls(2)
        }
        properties[0] =
            PropertyValuesHolder.ofFloat(View.TRANSLATION_X, translationX)
        properties[1] = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0f)
        if (isCard) {
            properties[2] = PropertyValuesHolder.ofFloat("cardElevation", elevation)
        }
        return properties
    }

    @JvmStatic
    fun calcXPropertyHolders(
        viewToAnimate: View,
        translationX: Float
    ): Array<PropertyValuesHolder?> {
        viewToAnimate.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        val properties = arrayOfNulls<PropertyValuesHolder>(1)
        properties[0] =
            PropertyValuesHolder.ofFloat(View.TRANSLATION_X, translationX)
        return properties
    }

    @JvmStatic
    fun calcPropertyHolderYandAlpha(
        translationY: Float,
        @FloatRange(from = 0.0, to = 255.0) alpha: Float
    ): Array<PropertyValuesHolder?> {
        val properties = arrayOfNulls<PropertyValuesHolder>(2)
        properties[0] =
            PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, translationY)
        properties[1] = PropertyValuesHolder.ofFloat(View.ALPHA, alpha)
        return properties
    }
}