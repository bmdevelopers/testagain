package co.bytemark.widgets.util

import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.model.common.OrderItem
import java.util.*

/**
 * Created by ranjith on 29/05/20
 */

data class ConsolidatedTicket(var quantity: Int, val name: String?, val OriginDest: String?, var price: Int)

class ConsolidatedTicketMapper(val confHelper: ConfHelper) {

    fun getConsolidatedTickets(orderItems: List<OrderItem>, isRefund: Boolean = false): MutableList<ConsolidatedTicket> {
        return HashMap<String, ConsolidatedTicket>().run {
            orderItems.forEach {
                val key = if (isRefund) it.uuid else getKeyForOrderItem(it)
                if (containsKey(key)) {
                    val consolidatedTicket = get(key)
                    consolidatedTicket?.quantity = consolidatedTicket?.quantity!!.plus(1)
                    consolidatedTicket.price = consolidatedTicket.price + it.total
                    put(key, consolidatedTicket)
                } else {
                    put(
                            key, ConsolidatedTicket(
                            1,
                            getName(it, isRefund),
                            getOD(if (isRefund) it.creditedOrderItem else it),
                            it.total
                    )
                    )
                }
            }
            mutableListOf<ConsolidatedTicket>().apply { addAll(values) }
        }
    }

    private fun getName(orderItem: OrderItem, isRefund: Boolean): String? {
        return if (isRefund) {
            orderItem.creditedOrderItem?.passes?.firstOrNull()?.labelName ?: orderItem.translatedLabelName
        } else {
            orderItem.translatedLabelName
        }
    }

    private fun getOD(orderItem: OrderItem?): String? {
        return if (getOrigin(orderItem) != null && getDestination(orderItem) != null) {
            getOrigin(orderItem).plus("/").plus(getDestination(orderItem))
        } else null
    }

    private fun getKeyForOrderItem(orderItem: OrderItem): String {
        return orderItem.product?.uuid.plus(orderItem.name).plus(getOrigin(orderItem)).plus(getDestination(orderItem))
    }

    private fun getOrigin(orderItem: OrderItem?): String? {
        return when {
            orderItem?.filterAttributes != null -> {
                val itinerary = orderItem.filterAttributes?.itinerary
                if (confHelper.isDisplayLongNameForOd(orderItem.product?.organizationUuid)) {
                    itinerary?.origin?.longName?.get(Locale.getDefault().language)
                } else {
                    itinerary?.origin?.shortName?.get(Locale.getDefault().language)
                }
            }

            orderItem?.attributes != null && orderItem.attributes.size > 0 -> {
                getValueFromAttributes(orderItem, "origin_name")
            }
            else -> {
                null
            }
        }
    }

    private fun getValueFromAttributes(orderItem: OrderItem?, key: String): String? {
        orderItem?.attributes?.forEach {
            if (it.key == key) {
                return it.value
            }
        }
        return null
    }

    private fun getDestination(orderItem: OrderItem?): String? {
        return when {
            orderItem?.filterAttributes != null -> {
                val itinerary = orderItem.filterAttributes?.itinerary
                if (confHelper.isDisplayLongNameForOd(orderItem.product?.organizationUuid)) {
                    itinerary?.destination?.longName?.get(Locale.getDefault().language)
                } else {
                    itinerary?.destination?.shortName?.get(Locale.getDefault().language)
                }
            }

            orderItem?.attributes != null && orderItem.attributes.size > 0 -> {
                getValueFromAttributes(orderItem, "destination_name")
            }
            else -> {
                null
            }
        }
    }
}

