package co.bytemark.widgets.util

import android.transition.Transition

class EndTransitionAdapter(private val endRunnable: Runnable) :
    Transition.TransitionListener {
    override fun onTransitionStart(transition: Transition) {}
    override fun onTransitionEnd(transition: Transition) {
        endRunnable.run()
    }
    override fun onTransitionCancel(transition: Transition) {}
    override fun onTransitionPause(transition: Transition) {}
    override fun onTransitionResume(transition: Transition) {}
}