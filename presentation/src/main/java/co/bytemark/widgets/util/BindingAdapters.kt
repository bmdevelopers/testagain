package co.bytemark.widgets.util


import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.jakewharton.rxbinding.view.RxView
import com.jakewharton.rxbinding.widget.RxTextView
import kotlinx.android.synthetic.main.send_ticket_to_user_dialog.*
import rx.android.schedulers.AndroidSchedulers
import java.lang.UnsupportedOperationException
import java.util.concurrent.TimeUnit

@BindingAdapter("visible")
fun View.setVisible(show: Boolean?) {
    show?.let {
        visibility = if (it) View.VISIBLE else View.INVISIBLE
    }
}

@BindingAdapter("setVisibleOrGone")
fun View.setVisibleOrGone(show: Boolean?) {
    show?.let {
        visibility = if (it) View.VISIBLE else View.GONE
    }
}

@BindingAdapter("imageResource")
fun setImageResource(imageView: ImageView, resource: Int) {
    imageView.setImageResource(resource)
}

@BindingAdapter("requestFocus")
fun requestFocus(editText: View, requestFocus: Boolean) {
    if (requestFocus) {
        editText.isFocusableInTouchMode = true
        editText.requestFocus()
    }
}

@BindingAdapter("onFocusChange")
fun onFocusChange(editText: View, listener: View.OnFocusChangeListener) {
    editText.onFocusChangeListener = listener
}

@BindingAdapter("onClick")
fun onClick(view: View, action: (()->Unit)?){
    view.setOnClickListener {
        action?.invoke()
    }
}