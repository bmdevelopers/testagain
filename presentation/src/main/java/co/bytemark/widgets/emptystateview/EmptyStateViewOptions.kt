package co.bytemark.widgets.emptystateview

import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.IntegerRes
import androidx.annotation.LayoutRes

class EmptyStateViewOptions {

    @LayoutRes
    var containerRes = 0

    @IntegerRes
    var progressRes = 0

    @DrawableRes
    var imageRes = 0

    @DrawableRes
    var circleImageRes = 0
    var title: String? = null
    var description: String? = null
    var buttonText: String? = null
    var buttonOnClickListener: View.OnClickListener? = null

    fun containerRes(@LayoutRes containerRes: Int): EmptyStateViewOptions {
        this.containerRes = containerRes
        return this
    }

    fun progressRes(@IntegerRes progressRes: Int): EmptyStateViewOptions {
        this.progressRes = progressRes
        return this
    }

    fun imageRes(@DrawableRes imageRes: Int): EmptyStateViewOptions {
        this.imageRes = imageRes
        return this
    }

    fun circleImageRes(@DrawableRes circleBorderImageRes: Int): EmptyStateViewOptions {
        circleImageRes = circleBorderImageRes
        return this
    }

    fun title(title: String?): EmptyStateViewOptions {
        this.title = title
        return this
    }

    fun description(description: String?): EmptyStateViewOptions {
        this.description = description
        return this
    }

    fun buttonText(buttonText: String?): EmptyStateViewOptions {
        this.buttonText = buttonText
        return this
    }

    fun buttonOnClickListener(buttonOnClickListener: View.OnClickListener?): EmptyStateViewOptions {
        this.buttonOnClickListener = buttonOnClickListener
        return this
    }
}