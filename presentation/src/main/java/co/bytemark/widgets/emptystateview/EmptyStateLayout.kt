package co.bytemark.widgets.emptystateview

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import co.bytemark.R
import co.bytemark.widgets.ProgressViewLayout
import co.bytemark.widgets.emptystateview.State.*
import co.bytemark.widgets.util.invisible


class EmptyStateLayout @JvmOverloads constructor(
		context: Context,
		attrs: AttributeSet? = null,
		defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

	private val TAG_EMPTY_STATE = "EmptyStateActivity.TAG"

	var inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    private var state = CONTENT

	private var layoutParams: LayoutParams? = null

	private var contentViews = mutableListOf<View>()

  private var emptyStateLayout: FrameLayout? = null
  private var progressLayout: ProgressViewLayout? = null
  private var progressImageView: ImageView? = null
  private var circularImageView: ImageView? = null
  private var titleTextView: TextView? = null
  private var contentTextView: TextView? = null
  private var positiveActionButton: Button? = null
  private var negativeActionButton: TextView? = null


  init {

    inflater.inflate(R.layout.progress_frame_layout_loading_view, this, false)?.let {
      emptyStateLayout = it.findViewById(R.id.frame_layout_loading)
      emptyStateLayout?.tag = TAG_EMPTY_STATE

      // Setup ProgressBar
      progressLayout = it.findViewById(R.id.progress_view_layout_loading)
      progressImageView = it.findViewById(R.id.image_view_loading)
      circularImageView = it.findViewById(R.id.circle_border_empty)
      titleTextView = it.findViewById(R.id.title_loading)
      contentTextView = it.findViewById(R.id.content_loading)
      positiveActionButton = it.findViewById(R.id.button_loading)
      negativeActionButton = it.findViewById(R.id.btnNegativeAction)

      layoutParams = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
              ViewGroup.LayoutParams.MATCH_PARENT)
      addView(emptyStateLayout, layoutParams)
    }

    val typedArray = context.obtainStyledAttributes(attrs, R.styleable.EmptyStateLayout)
    val themeValue = typedArray.getInt(
            R.styleable.EmptyStateLayout_eslTheme, Theme.BACKGROUND.ordinal
    )
    setTheme(Theme.values()[themeValue])
    typedArray.recycle()

    setContentVisibility(true, emptyList())
  }

  private fun setTheme(theme: Theme) {
    emptyStateLayout?.setBackgroundColor(getColor(theme.backgroundColor))
    progressLayout?.setProgressColor(getColor(theme.accentColor), getColor(theme.backgroundColor))
    progressImageView?.imageTintList = ColorStateList.valueOf(getColor(theme.accentColor))
    circularImageView?.imageTintList = ColorStateList.valueOf(getColor(theme.accentColor))
    titleTextView?.setTextColor(getColor(theme.primaryTextColor))
    contentTextView?.setTextColor(getColor(theme.primaryTextColor))
    positiveActionButton?.let {
      it.setTextColor(getColor(theme.secondaryTextColor))
      it.backgroundTintList = ColorStateList.valueOf(getColor(theme.accentColor))
    }
    negativeActionButton?.let {
      it.setTextColor(theme.accentColor)
    }
  }

  private fun getColor(@ColorRes colorRes: Int) =
          ContextCompat.getColor(context, colorRes)

  override fun addView(child: View, index: Int, params: ViewGroup.LayoutParams?) {
    super.addView(child, index, params)
    if (child.tag == null || child.tag != TAG_EMPTY_STATE) {
      contentViews.add(child)
    }
  }

  // ContentState functions

  fun showContent() {
    switchState(CONTENT)
  }

  fun showContent(list: List<Int>){
    switchState(CONTENT, skipIds = list)
  }

  // LoadingState functions

  @JvmOverloads
  fun showLoading(emptyImageDrawable: Int = 0, emptyTextTitle: String? = null, emptyTextContent: String? = null) {
    switchState(LOADING, getDrawable(emptyImageDrawable), emptyTextTitle, emptyTextContent)
  }

  fun showLoading(emptyImageDrawable: Int, emptyTextTitle: Int) {
    switchState(LOADING, getDrawable(emptyImageDrawable), getString(emptyTextTitle))
  }


  // EmptyState Functions

  @JvmOverloads
  fun showEmpty(emptyImageDrawable: Int = 0, emptyTextTitle: Int = 0, emptyTextContent: Int = 0) {
    switchState(EMPTY, getDrawable(emptyImageDrawable), getString(emptyTextTitle), getString(emptyTextContent))
  }

  fun showEmpty(emptyImageDrawable: Int, emptyTextTitle: String?, emptyTextContent: String? = null, emptyBtnText: String? = null, clickListener: ((v: View) -> Unit)? = null) {
    switchState(EMPTY, getDrawable(emptyImageDrawable), emptyTextTitle, emptyTextContent, emptyBtnText, clickListener)
  }


  // ErrorState functions

  fun showError(errorImageDrawable: Int, errorTextTitle: Int, errorTextContent: String? = null) {
    switchState(ERROR, getDrawable(errorImageDrawable), getString(errorTextTitle), errorTextContent)
  }

  fun showError(errorImageDrawable: Int, errorTextTitle: String?, errorTextContent: String?, errorButtonText: String?, onClickListener: ((v: View) -> Unit)?) {
    switchState(ERROR, getDrawable(errorImageDrawable), errorTextTitle, errorTextContent, errorButtonText, onClickListener)
  }

  @JvmOverloads
  fun showError(errorImageDrawable: Int, errorTextTitle: Int, errorTextContent: Int, errorButtonText: Int = 0, onClickListener: ((v: View) -> Unit)? = null) {
    switchState(ERROR, getDrawable(errorImageDrawable), getString(errorTextTitle), getString(errorTextContent), getString(errorButtonText), onClickListener)
  }

  fun show(state: State = EMPTY,
           drawable: Int,
           title: String,
           message: String,
           positiveButtonText: String? = null,
           positiveActionListener: ((v: View) -> Unit)? = null,
           negativeButtonText: String? = null,
           negativeActionListener: ((v: View) -> Unit)? = null
  ) {
    switchState(
            state,
            getDrawable(drawable),
            title,
            message,
            positiveButtonText,
            positiveActionListener,
            negativeButtonText,
            negativeActionListener
    )
  }

  private fun switchState(
          state: State,
          drawable: Drawable? = null,
          titleText: String? = null,
          messageText: String? = null,
          positiveButtonText: String? = null,
          positiveActionListener: ((v: View) -> Unit)? = null,
          negativeButtonText: String? = null,
          negativeActionListener: ((v: View) -> Unit)? = null,
          skipIds: List<Int> = emptyList()
  ) {
    this.state = state
    when (state) {
      CONTENT -> setContentVisibility(true, skipIds)
      LOADING -> {
        showEmptyStateView()
        progressLayout?.visibility = View.VISIBLE
        circularImageView?.visibility = View.INVISIBLE
        progressImageView?.setImageDrawable(drawable)
        titleTextView?.text = titleText
        if (!TextUtils.isEmpty(messageText)) {
          contentTextView?.visibility = View.VISIBLE
          contentTextView?.text = messageText
        } else {
          contentTextView?.visibility = View.INVISIBLE
        }
        if (!TextUtils.isEmpty(positiveButtonText)) {
          positiveActionButton?.visibility = View.VISIBLE
          positiveActionButton?.text = positiveButtonText
          positiveActionListener?.let { positiveActionButton?.setOnClickListener(it) }
        } else {
          positiveActionButton?.visibility = View.INVISIBLE
        }
        if (!TextUtils.isEmpty(negativeButtonText)) {
          negativeActionButton?.visibility = View.VISIBLE
          negativeActionButton?.text = negativeButtonText
          negativeActionListener?.let { negativeActionButton?.setOnClickListener(it) }
        } else {
          negativeActionButton?.visibility = View.GONE
        }
      }
      EMPTY, ERROR -> {
        showEmptyStateView()
        progressLayout?.visibility = View.INVISIBLE
        circularImageView?.visibility = View.VISIBLE
        circularImageView?.setImageDrawable(drawable)
        titleTextView?.text = titleText
        if (!TextUtils.isEmpty(messageText)) {
          contentTextView?.visibility = View.VISIBLE
          contentTextView?.text = messageText
        } else {
          contentTextView?.visibility = View.INVISIBLE
        }
        if (!TextUtils.isEmpty(positiveButtonText)) {
          positiveActionButton?.visibility = View.VISIBLE
          positiveActionButton?.text = positiveButtonText
          positiveActionListener?.let { positiveActionButton?.setOnClickListener(it) }
        } else {
          positiveActionButton?.invisible()
        }
        if (!TextUtils.isEmpty(negativeButtonText)) {
          negativeActionButton?.visibility = View.VISIBLE
          negativeActionButton?.text = negativeButtonText
          negativeActionListener?.let { negativeActionButton?.setOnClickListener(it) }
        } else {
          negativeActionButton?.visibility = View.GONE
        }
      }
    }
  }

  private fun setContentVisibility(visible: Boolean, skipIds: List<Int>?) {
    if (skipIds != null) {
      contentViews.filter {
        !skipIds.contains(it.id)
      }.forEach {
        it.isVisible = visible
      }
      if (visible) {
        hideEmptyStateView()
      }
    }
  }

  private fun hideEmptyStateView() {
    emptyStateLayout?.visibility = View.GONE
  }

  private fun showEmptyStateView() {
    emptyStateLayout?.visibility = View.VISIBLE
    setContentVisibility(false, emptyList())
  }

  private fun getDrawable(@DrawableRes resId: Int) =
          if (resId == 0)
            null
          else
            ContextCompat.getDrawable(context, resId)


  private fun getString(@StringRes resId: Int) =
          if (resId == 0)
            ""
          else
            context.getString(resId)

}