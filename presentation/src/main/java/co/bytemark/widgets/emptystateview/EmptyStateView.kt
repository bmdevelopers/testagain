package co.bytemark.widgets.emptystateview

import android.content.Context
import android.content.res.ColorStateList
import android.os.Build
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.AnimRes
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import co.bytemark.R
import co.bytemark.widgets.CircleBorderImageView
import co.bytemark.widgets.ProgressViewLayout

class EmptyStateView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    lateinit var linearLayout: LinearLayout
    lateinit var progressViewLayout: ProgressViewLayout
    lateinit var imageView: ImageView
    lateinit var circleBorderImageView: CircleBorderImageView
    lateinit var textViewTitle: TextView
    lateinit var textViewDescription: TextView
    lateinit var button: Button

    private var view: View? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.empty_state_view, this, false)?.let {
            linearLayout = it.findViewById(R.id.esv_container)
            progressViewLayout = it.findViewById(R.id.esv_progress)
            imageView = it.findViewById(R.id.esv_image)
            circleBorderImageView = it.findViewById(R.id.esv_circle_border_image)
            textViewTitle = it.findViewById(R.id.esv_title)
            textViewDescription = it.findViewById(R.id.esv_description)
            button = it.findViewById(R.id.esv_button)
        }

        val typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.EmptyStateView, defStyleAttr, 0)
        val themeValue = typedArray.getInt(
                R.styleable.EmptyStateLayout_eslTheme, Theme.BACKGROUND.ordinal
        )
        setTheme(Theme.values()[themeValue])
        typedArray.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        check(childCount <= 1) { "EmptyStateView can have only one child." }
        if (isInEditMode) return
        orientation = VERTICAL
        view = getChildAt(0)
    }

    private fun setTheme(theme: Theme) {
        linearLayout.setBackgroundColor(getColor(theme.backgroundColor))
        progressViewLayout.setProgressColor(getColor(theme.accentColor), getColor(theme.backgroundColor))
        imageView.imageTintList = ColorStateList.valueOf(getColor(theme.accentColor))
        circleBorderImageView.imageTintList = ColorStateList.valueOf(getColor(theme.accentColor))
        textViewTitle.setTextColor(getColor(theme.primaryTextColor))
        textViewDescription.setTextColor(getColor(theme.primaryTextColor))
        button.let {
            it.setTextColor(getColor(theme.secondaryTextColor))
            it.backgroundTintList = ColorStateList.valueOf(getColor(theme.accentColor))
        }
    }

    fun showContent() {
        linearLayout.clearAnimation()
        view!!.clearAnimation()
        if (linearLayout.visibility == View.VISIBLE) {
            linearLayout.visibility = View.GONE
            view!!.visibility = View.VISIBLE
        }
    }

    private fun showEmptyStateView(emptyStateView: EmptyStateViewOptions) {
        linearLayout.clearAnimation()
        view!!.clearAnimation()
        if (linearLayout.visibility == View.GONE) {
            view!!.visibility = View.GONE
            linearLayout.visibility = View.VISIBLE
            setVisibility(emptyStateView)
        } else {
            setVisibility(emptyStateView)
        }
    }

    private fun getColor(@ColorRes colorRes: Int): Int {
        return if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            context.resources.getColor(colorRes, null)
        } else {
            context.resources.getColor(colorRes)
        }
    }

    private fun setVisibility(emptyStateViewOptions: EmptyStateViewOptions) {
        if (emptyStateViewOptions.imageRes != 0) {
            progressViewLayout.visibility = View.VISIBLE
            circleBorderImageView.visibility = View.GONE
            imageView.visibility = View.VISIBLE
            imageView.setImageResource(emptyStateViewOptions.imageRes)
        } else {
            progressViewLayout.visibility = View.GONE
            imageView.visibility = View.GONE
        }
        if (emptyStateViewOptions.circleImageRes != 0) {
            circleBorderImageView.visibility = View.VISIBLE
            circleBorderImageView.setImageResource(emptyStateViewOptions.circleImageRes)
            progressViewLayout.visibility = View.GONE
        } else {
            circleBorderImageView.visibility = View.GONE
        }
        if (!TextUtils.isEmpty(emptyStateViewOptions.title)) {
            textViewTitle.visibility = View.VISIBLE
            textViewTitle.text = emptyStateViewOptions.title
        } else {
            textViewTitle.visibility = View.GONE
        }
        if (!TextUtils.isEmpty(emptyStateViewOptions.description)) {
            textViewDescription.visibility = View.VISIBLE
            textViewDescription.text = emptyStateViewOptions.description
        } else {
            textViewDescription.visibility = View.INVISIBLE
        }
        if (!TextUtils.isEmpty(emptyStateViewOptions.buttonText) &&
                emptyStateViewOptions.buttonOnClickListener != null) {
            button.visibility = View.VISIBLE
            button.text = emptyStateViewOptions.buttonText
            button.setOnClickListener(emptyStateViewOptions.buttonOnClickListener)
        } else {
            button.visibility = View.INVISIBLE
        }
    }

    fun showLoading(@DrawableRes imageRes: Int,
                    @StringRes titleRes: Int,
                    @StringRes descriptionRes: Int) {
        announceForAccessibility(getString(titleRes) + " " + getString(descriptionRes))
        showEmptyStateView(EmptyStateViewOptions()
                .imageRes(imageRes)
                .title(getString(titleRes))
                .description(getString(descriptionRes)))
    }

    fun showLoading(@DrawableRes imageRes: Int,
                    @StringRes titleRes: Int) {
        announceForAccessibility(getString(titleRes))
        showEmptyStateView(EmptyStateViewOptions()
                .imageRes(imageRes)
                .title(getString(titleRes)))
    }

    fun showEmpty(@DrawableRes circleImageRes: Int,
                  @StringRes titleRes: Int,
                  @StringRes descriptionRes: Int,
                  @StringRes buttonTextRes: Int,
                  clickListener: OnClickListener?) {
        announceForAccessibility(getString(titleRes) + " " + getString(descriptionRes))
        showEmptyStateView(EmptyStateViewOptions()
                .circleImageRes(circleImageRes)
                .title(getString(titleRes))
                .description(getString(descriptionRes))
                .buttonText(getString(buttonTextRes))
                .buttonOnClickListener(clickListener))
    }

    fun showEmpty(@DrawableRes circleImageRes: Int,
                  @StringRes titleRes: Int,
                  @StringRes descriptionRes: Int) {
        announceForAccessibility(getString(titleRes) + " " + getString(descriptionRes))
        showEmptyStateView(EmptyStateViewOptions()
                .circleImageRes(circleImageRes)
                .title(getString(titleRes))
                .description(getString(descriptionRes)))
    }

    fun showEmpty(@DrawableRes circleImageRes: Int,
                  @StringRes titleRes: Int) {
        announceForAccessibility(getString(titleRes))
        showEmptyStateView(EmptyStateViewOptions()
                .circleImageRes(circleImageRes)
                .title(getString(titleRes)))
    }

    fun showError(@DrawableRes circleImageRes: Int,
                  @StringRes titleRes: Int,
                  @StringRes descriptionRes: Int) {
        announceForAccessibility(getString(titleRes) + " " + getString(descriptionRes))
        showEmptyStateView(EmptyStateViewOptions()
                .circleImageRes(circleImageRes)
                .title(getString(titleRes))
                .description(getString(descriptionRes)))
    }

    fun showError(@DrawableRes circleImageRes: Int,
                  @StringRes titleRes: Int,
                  descriptionRes: String) {
        announceForAccessibility(getString(titleRes) + " " + descriptionRes)
        showEmptyStateView(EmptyStateViewOptions()
                .circleImageRes(circleImageRes)
                .title(getString(titleRes))
                .description(descriptionRes))
    }

    fun showError(@DrawableRes circleImageRes: Int,
                  @StringRes titleRes: Int,
                  @StringRes descriptionRes: Int,
                  @StringRes buttonTextRes: Int,
                  clickListener: OnClickListener?) {
        announceForAccessibility(getString(titleRes) + " " + getString(descriptionRes))
        showEmptyStateView(EmptyStateViewOptions()
                .circleImageRes(circleImageRes)
                .title(getString(titleRes))
                .description(getString(descriptionRes))
                .buttonText(getString(buttonTextRes))
                .buttonOnClickListener(clickListener))
    }

    private fun getString(@StringRes resId: Int): String {
        return context.getString(resId)
    }

    private fun anim(@AnimRes resId: Int): Animation? {
        return AnimationUtils.loadAnimation(context, resId)
    }

    fun showErrorState(@DrawableRes circleImageRes: Int, title: String?, description: String?) {
        showEmptyStateView(EmptyStateViewOptions()
                .circleImageRes(circleImageRes)
                .title(title)
                .description(description))
    }
}