package co.bytemark.widgets.emptystateview

import co.bytemark.R

enum class Theme(val backgroundColor: Int, val accentColor: Int, val primaryTextColor: Int, val secondaryTextColor: Int) {
    ACCENT(
            R.color.orgAccentBackgroundColor,
            R.color.orgAccentAccentColor,
            R.color.orgAccentPrimaryTextColor,
            R.color.orgAccentSecondaryTextColor
    ),
    BACKGROUND(
            R.color.orgBackgroundBackgroundColor,
            R.color.orgBackgroundAccentColor,
            R.color.orgBackgroundPrimaryTextColor,
            R.color.orgBackgroundSecondaryTextColor
    ),
    COLLECTION(
            R.color.orgDataBackgroundColor,
            R.color.orgDataAccentColor,
            R.color.orgDataPrimaryTextColor,
            R.color.orgDataSecondaryTextColor
    ),
    DATA(
            R.color.orgCollectionBackgroundColor,
            R.color.orgCollectionAccentColor,
            R.color.orgCollectionPrimaryTextColor,
            R.color.orgCollectionSecondaryTextColor
    )
}

enum class State {
    CONTENT, LOADING, EMPTY, ERROR
}