package co.bytemark.widgets

import android.animation.ValueAnimator
import android.animation.ValueAnimator.AnimatorUpdateListener
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.animation.LinearInterpolator
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import co.bytemark.R
import co.bytemark.widgets.util.ArcUtils
import co.bytemark.widgets.util.Util
import kotlin.math.min

class ProgressViewLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle), AnimatorUpdateListener {
    private var startAngle = 0
    private val sweepAngle = 360

    @ColorInt
    var strokeColor = Color.BLACK

    @ColorInt
    var trailColor = Color.TRANSPARENT
    var strokeWidth = Util.dpToPx(2.0)
    private var progressPaint: Paint = Paint()
    private var shader: Shader? = null
    private val center = PointF()
    private var radius = 0f
    private var rotationMatrix: Matrix? = null
    private var arcAnimator: ValueAnimator? = null

    init {
        val typedArray =
            context.obtainStyledAttributes(attrs, R.styleable.ProgressViewLayout, 0, 0)
        strokeColor =
            typedArray.getColor(R.styleable.ProgressViewLayout_strokeColor, strokeColor)
        trailColor =
            typedArray.getColor(R.styleable.ProgressViewLayout_trialColor, trailColor)
        progressPaint.flags = Paint.ANTI_ALIAS_FLAG
        progressPaint.color = strokeColor
        progressPaint.style = Paint.Style.STROKE
        progressPaint.strokeWidth = strokeWidth.toFloat()
        arcAnimator = ValueAnimator.ofInt(0, 360)
        arcAnimator?.duration = 1000
        arcAnimator?.interpolator = LinearInterpolator()
        arcAnimator?.repeatCount = ValueAnimator.INFINITE
        arcAnimator?.addUpdateListener(this)
        rotationMatrix = Matrix()
        typedArray.recycle()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        center.x = w / 2.toFloat()
        center.y = h / 2.toFloat()
        radius = min(w, h) / 2.toFloat()
        radius -= strokeWidth.toFloat()
        shader = SweepGradient(
            center.x, center.y, intArrayOf(
                trailColor,
                strokeColor,
                strokeColor
            ), null
        )
        rotationMatrix = Matrix()
        rotationMatrix?.postRotate(270f, center.x, center.y)
        shader?.setLocalMatrix(rotationMatrix)
        progressPaint.shader = shader
    }

    override fun dispatchDraw(canvas: Canvas) {
        ArcUtils.drawArc(
            canvas,
            center,
            radius,
            startAngle.toFloat(),
            sweepAngle.toFloat(),
            progressPaint
        )
        super.dispatchDraw(canvas)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        arcAnimator?.start()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        arcAnimator?.cancel()
    }

    override fun onAnimationUpdate(valueAnimator: ValueAnimator) {
        startAngle = valueAnimator.animatedValue as Int
        if (shader != null) {
            rotationMatrix?.setRotate(startAngle.toFloat(), center.x, center.y)
            shader?.setLocalMatrix(rotationMatrix)
        }
        invalidate()
    }

    fun setProgressColor(@ColorInt color: Int, @ColorInt colorEnd: Int) {
        trailColor = colorEnd
        strokeColor = color
        progressPaint.shader = shader
        invalidate()
        requestLayout()
    }
}