package co.bytemark.widgets.stackview.interpolators;

/**
 * Created by Arunkumar on 18/04/17.
 */
public class SpringInterpolator implements android.view.animation.Interpolator {
    private double amp = 1;
    private double frequency = 10;

    public SpringInterpolator() {

    }

    public SpringInterpolator(double amplitude, double frequency) {
        amp = amplitude;
        this.frequency = frequency;
    }

    public float getInterpolation(float time) {
        return (float) (-1 * Math.pow(Math.E, -time / amp) *
                Math.cos(frequency * time) + 1);
    }
}