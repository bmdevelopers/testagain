package co.bytemark.widgets.stackview;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;

import java.util.LinkedList;
import java.util.List;

import co.bytemark.widgets.stackview.annotations.Section;
import timber.log.Timber;

/**
 * Created by Arunkumar on 22/07/16.
 */
public abstract class StackAdapter<T, VH extends ViewHolder> extends ObservableBaseStackAdapter<VH> {
    private final Context context;
    private final LayoutInflater inflater;

    protected final List<T> firstData;
    protected final List<T> secondData;
    protected final List<T> thirdData;
    protected final List<T> fourthData;

    protected StackSelectionListener<T> stackSelectionListener = new StackSelectionListener<T>() {
        @Override
        public void onStackItemSelected(T item, @Section int section, int position) {
            // no-op
        }

        @Override
        public void onStackItemUnSelected(T item, @Section int section, int position) {
            // no-op
        }
    };


    protected StackAdapter(Context context) {
        this.context = context.getApplicationContext();
        inflater = LayoutInflater.from(context);
        firstData = new LinkedList<>();
        secondData = new LinkedList<>();
        thirdData = new LinkedList<>();
        fourthData = new LinkedList<>();
    }

    protected LayoutInflater getLayoutInflater() {
        return inflater;
    }

    public Context getContext() {
        return context;
    }


    public void addFirstList(@NonNull List<T> firstList) {
        firstData.addAll(firstList);
        notifyFirstListReset();
    }

    public void addSecondList(@NonNull List<T> secondList) {
        secondData.addAll(secondList);
        notifySecondListReset();
    }

    public void addThirdList(@NonNull List<T> thirdList) {
        thirdData.addAll(thirdList);
        notifyThirdListReset();
    }

    public void addFourthList(@NonNull List<T> fourthList) {
        fourthData.addAll(fourthList);
        notifyFourthListReset();
    }

    public void setSelectionListener(@Nullable StackSelectionListener<T> selectionListener) {
        if (selectionListener != null) {
            stackSelectionListener = selectionListener;
        }
    }

    @Override
    public void onBindFirstSectionViewHolder(VH view, int position) {
        T item = getFirstItem(position);
        bindFirstSectionView(item, position, view);
    }

    @Override
    protected void onBindSecondSectionViewHolder(VH view, int position) {
        T item = getSecondItem(position);
        bindSecondSectionView(item, position, view);
    }

    @Override
    protected void onBindThirdSectionViewHolder(VH view, int position) {
        T item = getThirdItem(position);
        bindThirdSectionView(item, position, view);
    }

    @Override
    protected void onBindFourthSectionViewHolder(VH view, int position) {
        T item = getFourthItem(position);
        bindFourthSectionView(item, position, view);
    }

    protected abstract void bindFirstSectionView(T data, int position, VH holder);

    protected abstract void bindSecondSectionView(T data, int position, VH holder);

    protected abstract void bindThirdSectionView(T data, int position, VH holder);

    protected abstract void bindFourthSectionView(T data, int position, VH holder);

    @Override
    public int getFirstSectionCount() {
        return firstData.size();
    }

    @Override
    public int getSecondSectionCount() {
        return secondData.size();
    }

    @Override
    public int getThirdSectionCount() {
        return thirdData.size();
    }

    @Override
    public int getFourthSectionCount() {
        return fourthData.size();
    }

    private T getFirstItem(int position) {
        return firstData.get(position);
    }

    private T getSecondItem(int position) {
        return secondData.get(position);
    }

    private T getThirdItem(int position) {
        return thirdData.get(position);
    }

    private T getFourthItem(int position) {
        return fourthData.get(position);
    }

    public void clear() {
        firstData.clear();
        thirdData.clear();
        secondData.clear();
        fourthData.clear();
        notifyDataSetChanged();
    }

    public boolean isEmpty() {
        return firstData.isEmpty() & secondData.isEmpty() & thirdData.isEmpty() && fourthData.isEmpty();
    }

    @Nullable
    public T getItemAt(@Section int section, int currSelection) {
        try {
            switch (section) {
                case StackViewLayout.FIRST:
                    return firstData.get(currSelection);
                case StackViewLayout.SECOND:
                    return secondData.get(currSelection);
                case StackViewLayout.THIRD:
                    return thirdData.get(currSelection);
                case StackViewLayout.FOURTH:
                    return fourthData.get(currSelection);
            }

        } catch (Exception ignored) {
            Timber.e(ignored.toString());
        }
        return null;
    }

    /**
     * Moves a {@code item} from one section to another and refreshes the stack.
     *
     * @param fromSection From section
     * @param fromIndex   Location of the item
     * @param toSection   Destination section.
     */
    public void moveItem(@Section int fromSection, int fromIndex, @Section int toSection) {
        List<T> fromList = new LinkedList<>();
        List<T> toList = new LinkedList<>();

        switch (fromSection) {
            case StackViewLayout.FIRST:
                fromList = firstData;
                break;
            case StackViewLayout.SECOND:
                fromList = secondData;
                break;
            case StackViewLayout.THIRD:
                fromList = thirdData;
                break;
            case StackViewLayout.FOURTH:
                fromList = fourthData;
                break;
        }

        toList.add(fromList.remove(fromIndex));

        switch (toSection) {
            case StackViewLayout.FIRST:
                addFirstList(toList);
                break;
            case StackViewLayout.SECOND:
                addSecondList(toList);
                break;
            case StackViewLayout.THIRD:
                addThirdList(toList);
                break;
            case StackViewLayout.FOURTH:
                addFourthList(toList);
                break;
        }

        notifyDataSetChanged();
    }

    public void replaceItem(@NonNull T itemToReplace, int index, @Section int section) {
        List<T> targetList = new LinkedList<>();
        switch (section) {
            case StackViewLayout.FIRST:
                targetList = firstData;
                break;
            case StackViewLayout.SECOND:
                targetList = secondData;
                break;
            case StackViewLayout.THIRD:
                targetList = thirdData;
                break;
            case StackViewLayout.FOURTH:
                targetList = fourthData;
                break;
        }
        targetList.set(index, itemToReplace);
        notifyDataSetChanged();
    }

    public List<T> getData(@Section int section) {
        switch (section) {
            case StackViewLayout.FIRST:
                return firstData;
            case StackViewLayout.SECOND:
                return secondData;
            case StackViewLayout.THIRD:
                return thirdData;
            case StackViewLayout.FOURTH:
                return fourthData;
            default:
                return firstData;
        }
    }

    @Override
    public void onStackItemSelected(@Section int section, int position) {
        T item = getItemAt(section, position);
        stackSelectionListener.onStackItemSelected(item, section, position);
    }

    @Override
    public void onStackItemUnSelected(@Section int section, int position) {
        T item = getItemAt(section, position);
        stackSelectionListener.onStackItemUnSelected(item, section, position);
    }

    public interface StackSelectionListener<T> {
        void onStackItemSelected(@Nullable T item, @Section int section, int position);

        void onStackItemUnSelected(@Nullable T item, @Section int section, int position);
    }
}