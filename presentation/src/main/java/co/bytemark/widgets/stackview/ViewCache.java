package co.bytemark.widgets.stackview;

import androidx.annotation.Nullable;
import android.view.View;

import java.util.LinkedList;

import timber.log.Timber;

/**
 * Created by Arunkumar on 01/09/16.
 * A cache implementation that holds {@link android.view.View} instances in memory. It is backed by
 * a LinkedList
 */
@Deprecated
// Need to adapt view cache to handle different pass view type since now each pass item
// is dynamic
public class ViewCache {
    final LinkedList<View> cache = new LinkedList<>();

    private final int maxCacheSize = 50;

    private ViewCache() {

    }

    public static ViewCache newInstance() {
        return new ViewCache();
    }

    public void putInCache(@Nullable final View view) {
        if (cache.size() < maxCacheSize && view != null) {
            view.setOnClickListener(null);
            cache.push(view);
        }
    }

    @Nullable
    public View getView() {
        if (cache.isEmpty()) {
            return null;
        }
        return cache.pop();
    }

    /**
     * Drops all views that are cached. Call this when you no longer need the cache.
     * NOTE: Missing call to this method will cause memory leak.
     */
    public void dropCache() {
        for (View view : cache) {
            // Needed for faster garbage collection.
            view = null;
        }
        cache.clear();
        Timber.d("Cache dropped");
    }

    public boolean hasViews() {
        return !cache.isEmpty();
    }
}
