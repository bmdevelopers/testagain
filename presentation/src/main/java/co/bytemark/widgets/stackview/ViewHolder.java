package co.bytemark.widgets.stackview;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import android.view.View;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.helpers.ConfHelper;

/**
 * Created by Arunkumar on 08/09/16.
 * View holder class to hold the actual View object and providing customizations around it.
 * Common operations for all type of views will be added here and you can extend this class to implement
 * screen specific operations.
 * It is a good practice to call {@link #release()} whenever you determine the holder is not necessary
 * and need not be recycled.
 */
public abstract class ViewHolder {
    public View rootView;

    @Inject
    public ConfHelper confHelper;

    public ViewHolder(@NonNull View rootView) {
        CustomerMobileApp.Companion.getComponent().inject(this);
        this.rootView = rootView;
    }

    @CallSuper
    public void release() {
        rootView = null;
    }

    public abstract void onPartiallyVisible();

    public abstract void onFullyVisible();
}
