package co.bytemark.widgets.stackview.annotations;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import co.bytemark.widgets.stackview.StackViewLayout;

/**
 * Created by Arunkumar on 19/08/16.
 */
@Retention(RetentionPolicy.SOURCE)
@IntDef({StackViewLayout.FIRST, StackViewLayout.THIRD, StackViewLayout.SECOND, StackViewLayout.FOURTH})
public @interface Section {
}
