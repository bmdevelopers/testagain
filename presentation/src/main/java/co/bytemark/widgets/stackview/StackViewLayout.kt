package co.bytemark.widgets.stackview

import android.animation.*
import android.content.Context
import android.database.Observable
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.widget.RelativeLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.annotation.FloatRange
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.cardview.widget.CardView
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import co.bytemark.R
import co.bytemark.widgets.stackview.annotations.Section
import co.bytemark.widgets.stackview.interpolators.SpringInterpolator
import co.bytemark.widgets.util.AnimationUtil
import co.bytemark.widgets.util.Util
import co.bytemark.widgets.util.Util.LayoutDoneListener
import timber.log.Timber
import java.util.*

/**
 * Created by Arunkumar on 20/07/16.
 */
class StackViewLayout constructor(context: Context) : RelativeLayout(context) {
    // Minimum scale of the top most item in the stack. The scale of the items will be
    // progressively interpolated from this value to 1.
    @FloatRange(from = 0.0, to = 1.0)
    private var MINIMUM_SCALE_FACTOR = 0.975f

    // Scale factor for bottom stack view when a card is selected
    @FloatRange(from = 0.0, to = 1.0)
    private var MINIMUM_SCALE_FACTOR_FOCUSED = 0.975f
    private val firstItems =
        LinkedList<ViewHolder>()
    private val secondItems =
        LinkedList<ViewHolder>()
    private val thirdItems =
        LinkedList<ViewHolder>()
    private val fourthItems =
        LinkedList<ViewHolder>()
    private var firstHeader: TextView
    private var secondHeader: TextView
    private var thirdHeader: TextView
    private var fourthHeader: TextView
    private var showFirstHeader = true
    private val showSecondHeader = true
    private val showThirdHeader = true
    private val showFourthHeader = true
    private val disableFirstSectionAnimation = false

    // View which appears at the bottom of the selected item. Usually the one that contains the
    // hold to activate and remove actions
    private var supplementaryView: View

    // Adapter interaction fields
    private val observer = ViewDataObserver()
    private var adapter: ObservableBaseStackAdapter<ViewHolder>? =
        null

    // Cache to reuse inflated views
    private var viewCache: ViewCache? = null

    @get:Section
    @Section
    var selectedSection = 0
        private set
    private var selectedItemIndex = 0
    private var isSelectedFlag = false
    private var selectedItemElevation = 0f
    private var selectedItemView: View? = null

    // Flag to help in correctly broadcasting callback to clients.
    private var selectedStateFlag = false

    // Dummy stackSelectionObserver
    private var stackSelectionObserver: StackSelectionObserver = object : StackSelectionObserver {
        override fun onStackItemSelected(
            @Section sectionType: Int,
            position: Int
        ) {
        }

        override fun onStackItemUnSelected(
            @Section sectionType: Int,
            position: Int
        ) {
        }
    }
    private val multiSelectEnabled = false
    private val multiSelectCallback: MultiSelectCallback =
        object : MultiSelectCallback {
            override fun onMultiAnimationEnded(
                section: Int,
                index: Int,
                absoluteIndex: Int,
                singleItem: Boolean
            ) {
            }
        }

    // Click stackSelectionObserver that acts on all items in the stack. Both top and bottom.
    private val clickListener = ViewClickListener()
    private val selectedAnimators: AnimatorSet? = null
    private var stackAnimators: AnimatorSet? = null
    private val stackGapNormalPx = STACK_GAP_NORMAL
    private var stackGapSelectedPx = STACK_GAP_SELECTED
    private var choreographer: AnimatorSet? = null

    init {
        val inflater = LayoutInflater.from(context)
        isSelectedFlag = false
        selectedStateFlag = false
        firstHeader = inflater.inflate(
            R.layout.section_header,
            this,
            false
        ) as TextView
        secondHeader = inflater.inflate(
            R.layout.section_header,
            this,
            false
        ) as TextView
        thirdHeader = inflater.inflate(
            R.layout.section_header,
            this,
            false
        ) as TextView
        fourthHeader = inflater.inflate(
            R.layout.section_header,
            this,
            false
        ) as TextView
        addView(thirdHeader)
        addView(firstHeader)
        addView(secondHeader)
        addView(fourthHeader)

        // Add the selected action view
        supplementaryView =
            inflater.inflate(R.layout.default_focused_action_layout, this, false)
        supplementaryView?.alpha = 0f
        addView(supplementaryView)
        postResetStack()
        viewCache = ViewCache.newInstance()
    }

    private fun initItems() {
        for (viewHolder in firstItems) {
            removeView(viewHolder.rootView)
            viewHolder.release()
        }
        for (viewHolder in secondItems) {
            removeView(viewHolder.rootView)
            viewHolder.release()
        }
        for (viewHolder in thirdItems) {
            removeView(viewHolder.rootView)
            viewHolder.release()
        }
        for (viewHolder in fourthItems) {
            removeView(viewHolder.rootView)
            viewHolder.release()
        }
        firstItems.clear()
        secondItems.clear()
        thirdItems.clear()
        fourthItems.clear()

        // Inflate and add all the top items
        adapter?.firstSectionCount?.let {
            for (i in 0 until it) {
                val viewHolder =
                    createViewHolderFromAdapter(FIRST)
                adapter?.bindFirstSectionView(viewHolder, i)
                viewHolder?.let { holder -> firstItems.add(holder) }
            }
        }

        // Inflate and add all the middle items
        adapter?.secondSectionCount?.let {
            for (i in 0 until it) {
                val viewHolder =
                    createViewHolderFromAdapter(SECOND)
                adapter?.bindSeconSectionView(viewHolder, i)
                viewHolder?.let { holder -> secondItems.add(holder) }
            }
        }

        // Inflate and add all the bottom items
        adapter?.thirdSectionCount?.let {
            for (i in 0 until it) {
                val viewHolder =
                    createViewHolderFromAdapter(THIRD)
                adapter?.bindThirdSection(viewHolder, i)
                viewHolder?.let { holder -> thirdItems.add(holder) }
            }
        }

        // Inflate and add all the bottom items
        adapter?.fourthSectionCount?.let {
            for (i in 0 until it) {
                val viewHolder =
                    createViewHolderFromAdapter(FOURTH)
                adapter?.bindFourthSection(viewHolder, i)
                viewHolder?.let { holder -> fourthItems.add(holder) }
            }
        }
    }

    /**
     * Used to intelligently refesh the stack state. If the stack is selected, then selected animations are
     * triggered if not reset stack animations are called. Hence state is preserved and only
     * necessary items are redrawn again.
     */
    private fun refreshStack() {
        if (isSelectedFlag) {
            selectedItemView?.let { setSelectedItem(it) }
        } else {
            resetStack()
        }
    }

    /**
     * Resets all the items and their headers to their initial states. The items will automatically
     * animate to their new position from their existing position. So you can call this any time and
     * does not matter if there is a selected item or not.
     *
     * @param endRunnable Runnable to execute after animations
     */
    private fun resetStack(endRunnable: Runnable? = null) {
        isSelectedFlag = false
        cancelAnimations()
        restoreSelectedItemElevation()
        var yPos = 0 // The variable to keep track of Y position of items
        val animators: MutableList<Animator> =
            LinkedList()
        animators.add(
            ObjectAnimator.ofPropertyValuesHolder(
                firstHeader,
                *AnimationUtil.calcPropertyHolderYandAlpha(yPos.toFloat(), 1f)
            )
        )
        yPos += firstHeaderHeight
        yPos = addStackAnimatorsToViews(yPos, animators, firstItems)
        animators.add(
            ObjectAnimator.ofPropertyValuesHolder(
                secondHeader,
                *AnimationUtil.calcPropertyHolderYandAlpha(yPos.toFloat(), 1f)
            )
        )
        yPos += secondHeaderHeight
        yPos = addStackAnimatorsToViews(yPos, animators, secondItems)
        animators.add(
            ObjectAnimator.ofPropertyValuesHolder(
                thirdHeader,
                *AnimationUtil.calcPropertyHolderYandAlpha(yPos.toFloat(), 1f)
            )
        )
        yPos += thirdHeaderHeight
        yPos = addStackAnimatorsToViews(yPos, animators, thirdItems)
        animators.add(
            ObjectAnimator.ofPropertyValuesHolder(
                fourthHeader,
                *AnimationUtil.calcPropertyHolderYandAlpha(yPos.toFloat(), 1f)
            )
        )
        yPos += fourthHeaderHeight
        // Supplementary view
        animators.add(
            ObjectAnimator.ofPropertyValuesHolder(
                supplementaryView,
                *AnimationUtil.calcPropertyHolderYandAlpha(yPos.toFloat(), 0f)
            )
        )
        addStackAnimatorsToViews(yPos, animators, fourthItems)

        // Accessibility
        signalViewShownWhenNormal()
        stackAnimators = AnimatorSet()
        stackAnimators?.duration = 400
        stackAnimators?.interpolator = LinearOutSlowInInterpolator()
        stackAnimators?.playTogether(animators)
        stackAnimators?.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                requestLayout()
                restoreScrolling()
            }

            override fun onAnimationEnd(animation: Animator) {
                restoreLayers()
                if (selectedStateFlag) {
                    stackSelectionObserver.onStackItemUnSelected(
                        selectedSection,
                        selectedItemIndex
                    )
                    selectedStateFlag = false
                }
                endRunnable?.run()
            }
        })
        stackAnimators?.start()
    }

    /**
     * Selects a view based on configuration.
     *
     * @param viewToSelect
     */
    private fun setSelectedItem(viewToSelect: View) {
        if (multiSelectEnabled) {
            if (isAnimationRunning) {
                cancelAnimations()
            }
            doMultiSelectAnimation(viewToSelect)
        }
    }

    /**
     * Choreographs all the views so that they can be put in a position where it is easy for client
     * to do any multi select animation.
     *
     *
     * The second act is called "The Turn". **The magician takes the ordinary something and makes it
     * do something extraordinary**. Now you're looking for the secret... but you won't find it,
     * because of course you're not really looking. You don't really want to know. You want to be fooled.
     * **But you wouldn't clap yet.**
     * [co.bytemark.use_tickets.MultiSelectFragment.onCreateAnimation]
     *
     * @param view View that was clicked.
     */
    private fun doMultiSelectAnimation(view: View) {
        val linearOutSlowInInterpolator = LinearOutSlowInInterpolator()
        isSelectedFlag = true
        selectedItemView = view
        selectedItemElevation = selectedItemView?.elevation ?: 0f

        // Durations
        val slideAwayDuration = 100
        val selectedItemTransitionDuration = 100
        val initialStackToTopDuration = 200
        var yPos = 0

        /* Step 1: Take all the views above the selected view and stack them upwards.
         *
         * Do this by iterating and creating a list which will contain only the top stack.
         */
        val initialStackAnimators: MutableList<Animator> =
            ArrayList()
        // Hide all the headers first
        initialStackAnimators.add(
            ObjectAnimator.ofPropertyValuesHolder(
                firstHeader,
                *AnimationUtil.calcPropertyHolderYandAlpha(firstHeader.translationY, 0f)
            )
        )
        initialStackAnimators.add(
            ObjectAnimator.ofPropertyValuesHolder(
                secondHeader,
                *AnimationUtil.calcPropertyHolderYandAlpha(secondHeader.translationY, 0f)
            )
        )
        initialStackAnimators.add(
            ObjectAnimator.ofPropertyValuesHolder(
                thirdHeader,
                *AnimationUtil.calcPropertyHolderYandAlpha(thirdHeader.translationY, 0f)
            )
        )
        initialStackAnimators.add(
            ObjectAnimator.ofPropertyValuesHolder(
                fourthHeader,
                *AnimationUtil.calcPropertyHolderYandAlpha(fourthHeader.translationY, 0f)
            )
        )
        var foundView = false
        val topStack =
            LinkedList<View>()
        val bottomStack =
            LinkedList<View>()
        for (i in firstItems.indices) {          // This crappy 4 loop ladder will give
            val item =
                firstItems[i].rootView // us the top stack before the view user clicked.
            if (!foundView) {
                topStack.add(item)
            } else {
                bottomStack.add(item)
            }
            if (item == selectedItemView) {
                selectedSection = FIRST
                selectedItemIndex = i
                foundView = true
            }
        }
        for (i in secondItems.indices) {
            val item = secondItems[i].rootView
            if (!foundView) {
                topStack.add(item)
            } else {
                bottomStack.add(item)
            }
            if (item == selectedItemView) {
                selectedSection = SECOND
                selectedItemIndex = i
                foundView = true
            }
        }
        for (i in thirdItems.indices) {
            val item = thirdItems[i].rootView
            if (!foundView) {
                topStack.add(item)
            } else {
                bottomStack.add(item)
            }
            if (item == selectedItemView) {
                selectedSection = THIRD
                selectedItemIndex = i
                foundView = true
            }
        }
        for (i in fourthItems.indices) {
            val item = fourthItems[i].rootView
            if (!foundView) {
                topStack.add(item)
            } else {
                bottomStack.add(item)
            }
            if (item == selectedItemView) {
                selectedSection = FOURTH
                selectedItemIndex = i
                foundView = true
            }
        }
        // I could probably do the animation calculation by reusing loops above. But I am concentrating
        // on my sanity than performance now. We are not launching a missile anyway.
        for (i in topStack.indices) {
            val item = topStack[i]
            val scale = getScaleForPosition(i, topStack)
            val elevation = getElevationForPosition(i, topStack)
            initialStackAnimators.add(
                ObjectAnimator.ofPropertyValuesHolder(
                    item,
                    *AnimationUtil.calcPropertyHolders(item, yPos.toFloat(), scale, elevation)
                )
            )
            yPos += STACK_GAP_SELECTED
        }
        // Step 1 ends -----------------------------------------------------------------------------


        // Step 2: Take all the view below the selected view and slide them downwards to fade.
        val yAway = height + height / 2
        for (i in bottomStack.indices) {
            val item = bottomStack[i]
            val scale = getScaleForPosition(i, topStack)
            val elevation = getElevationForPosition(i, topStack)
            initialStackAnimators.add(
                ObjectAnimator.ofPropertyValuesHolder(
                    item,
                    *AnimationUtil.calcPropertyHolders(item, yAway.toFloat(), scale, elevation)
                )
            )
        }
        // Step 2 ends -----------------------------------------------------------------------------


        // Now take all the views on the upper stack and slide away to right.
        val slideRightAnimators: MutableList<Animator> =
            ArrayList()
        var xPos = width
        val size = topStack.size
        val dpGap = Util.dpToPx(60.0)
        var i = size
        while (--i >= 0) {
            val item = topStack[i]
            if (item != selectedItemView) {
                val slideAway = ObjectAnimator.ofPropertyValuesHolder(
                    item,
                    *AnimationUtil.calcXPropertyHolders(item, xPos.toFloat())
                )
                slideAway.duration = slideAwayDuration.toLong()
                slideAway.startDelay = 15 * i.toLong()
                slideAway.interpolator = AccelerateInterpolator()
                slideRightAnimators.add(slideAway)
                xPos += dpGap
            }
        }
        // Move the touched item to top and scale it.
        // First item in the stack stays on the left and others will be center aligned.
        val selectedItemX: Int
        val predictedWidth = (selectedItemView?.width?.times(0.85))?.toInt() ?: 0
        selectedItemX = when (topStack.size) {
            1 -> {
                0
            }
            adapter?.count -> {
                width - predictedWidth - Util.dpToPx(16.0) * 2
            }
            else -> {
                width / 2 - predictedWidth / 2 - Util.dpToPx(16.0)
            }
        }
        val selectedItemTransition = AnimatorSet()
        val slideAwayTransition = AnimatorSet()
        selectedItemView?.let {
            val selectedViewUpAnimation: ValueAnimator = ObjectAnimator.ofPropertyValuesHolder(
                it,
                *AnimationUtil.calcXPropertyHolders(
                    it,
                    selectedItemX.toFloat(),
                    it.elevation
                )
            )
            val selectedViewSizeAnimation = getWidthAnimator(
                it,
                predictedWidth + Util.dpToPx(16.0)
            )
            // We need a animator set to simultaneously do scale and translate.
            selectedItemTransition.duration = selectedItemTransitionDuration.toLong()
            selectedItemTransition.interpolator = SPRING_INTERPOLATOR
            selectedItemTransition.playTogether(selectedViewUpAnimation, selectedViewSizeAnimation)
            slideAwayTransition.playTogether(slideRightAnimators)
        }

        // Stack all items to the top
        val stage1 = AnimatorSet()
        stage1.interpolator = linearOutSlowInInterpolator
        stage1.duration = initialStackToTopDuration.toLong()
        stage1.playTogether(initialStackAnimators)

        // Move selected item to top and move all others to the side
        val stage2 = AnimatorSet()
        stage2.interpolator = linearOutSlowInInterpolator
        stage2.playSequentially(slideAwayTransition, selectedItemTransition)
        val absoluteIndex = topStack.size - 1

        // NOTE: If there is only item in the stack, then we don't need any animations. Just give
        // callback directly.
        if (adapter?.count == 1) {
            postDelayed(
                { multiSelectAnimationFinished(absoluteIndex, true) },
                100
            )
            return
        }

        // The difficult part is choreographing everything. This one does that.
        choreographer = AnimatorSet()
        choreographer?.playSequentially(stage1, stage2)
        choreographer?.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                requestLayout()
                Util.registerForLayout(
                    this@StackViewLayout,
                    object : LayoutDoneListener<View> {
                        override fun onLayoutFinished(view: View) {
                            { scrollParentToTop() }
                        }
                    }
                )
            }

            override fun onAnimationEnd(animation: Animator) {
                multiSelectAnimationFinished(absoluteIndex, false)
            }
        })
        choreographer?.start()
    }

    private fun multiSelectAnimationFinished(
        absoluteIndex: Int,
        singleItem: Boolean
    ) {
        if (!selectedStateFlag) {
            multiSelectCallback.onMultiAnimationEnded(
                selectedSection,
                selectedItemIndex,
                absoluteIndex,
                singleItem
            )
            selectedStateFlag = true
        }
        postDelayed({
            selectedItemView?.layoutParams = LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            selectedItemView?.requestLayout()
        }, 500)
    }

    private fun addStackAnimatorsToViews(
        position: Int,
        animators: MutableList<Animator>,
        viewHolderList: LinkedList<ViewHolder>
    ): Int {
        var yPos = position
        for (i in viewHolderList.indices) {
            val item = viewHolderList[i].rootView
            val scale = getScaleForPosition(i, viewHolderList)
            val elevation = getElevationForPosition(i, viewHolderList)
            animators.add(
                ObjectAnimator.ofPropertyValuesHolder(
                    item,
                    *AnimationUtil.calcPropertyHolders(item, yPos.toFloat(), scale, elevation)
                )
            )
            yPos += stackGapNormalPx
            viewHolderList[i].onPartiallyVisible()
        }
        if (!viewHolderList.isEmpty()) {
            yPos += viewHolderList.last.rootView.height - stackGapNormalPx
        }
        return yPos
    }
    /**
     * Ensures views are measured before calculating stack animation parameters. When adding views
     * manually, always call this instead of [.resetStack]
     *
     * @param runnable Runnable to execute after resetting.
     */
    /**
     * Ensures views are measured before calculating stack animation parameters. When adding views
     * manually, always call this instead of [.resetStack]
     */
    private fun postResetStack(runnable: Runnable? = null) {
        requestLayout()
        Util.registerForLayout(
            this,
            object : LayoutDoneListener<View> {
                override fun onLayoutFinished(view: View) {
                    resetStack(runnable)
                }
            }
        )
    }

    private fun restoreSelectedItemElevation() {
        if (selectedItemView is CardView) {
            (selectedItemView as CardView).cardElevation = selectedItemElevation
        }
    }

    fun getSelectedItemIndex(): Int {
        return if (isSelectedFlag) {
            selectedItemIndex
        } else NO_SELECTION
    }

    /**
     * Cancels all running animations. Call this when fresh animations are needed.
     */
    private fun cancelAnimations() {
        if (selectedAnimators != null && selectedAnimators.isRunning) {
            selectedAnimators.removeAllListeners()
            selectedAnimators.cancel()
        }
        if (stackAnimators != null && stackAnimators?.isRunning == true) {
            stackAnimators?.removeAllListeners()
            stackAnimators?.cancel()
        }
        if (choreographer != null && choreographer?.isRunning == true) {
            choreographer?.removeAllListeners()
            choreographer?.cancel()
        }
    }

    private val isAnimationRunning: Boolean
        get() = ((selectedAnimators != null && selectedAnimators.isRunning
                || stackAnimators != null && stackAnimators?.isRunning == true)
                || choreographer != null && choreographer?.isRunning == true)

    /**
     * Animates the given view to the given width in px.
     *
     * @param viewToAnimate View to animate
     * @param widthPx       Destination width
     * @return [ValueAnimator]
     */
    private fun getWidthAnimator(viewToAnimate: View, widthPx: Int): ValueAnimator {
        val anim = ValueAnimator.ofInt(viewToAnimate.width, widthPx)
        anim.interpolator = FastOutSlowInInterpolator()
        viewToAnimate.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                viewToAnimate.setLayerType(View.LAYER_TYPE_NONE, null)
            }
        })
        anim.addUpdateListener { animation: ValueAnimator ->
            viewToAnimate.layoutParams.width = animation.animatedValue as Int
            viewToAnimate.requestLayout()
        }
        return anim
    }

    private val supplementaryViewHeight: Int
        get() = supplementaryView.height

    /**
     * Removes hardware layering from all the views.
     */
    private fun restoreLayers() {
        for (viewHolder in firstItems) {
            viewHolder.rootView.setLayerType(View.LAYER_TYPE_NONE, null)
        }
        for (viewHolder in secondItems) {
            viewHolder.rootView.setLayerType(View.LAYER_TYPE_NONE, null)
        }
        for (viewHolder in thirdItems) {
            viewHolder.rootView.setLayerType(View.LAYER_TYPE_NONE, null)
        }
        for (viewHolder in fourthItems) {
            viewHolder.rootView.setLayerType(View.LAYER_TYPE_NONE, null)
        }
    }

    /**
     * Commands signal to view holders that it is fully shown.
     */
    private fun signalViewShownWhenNormal() {
        if (!firstItems.isEmpty()) {
            ((firstItems as LinkedList<*>).last as ViewHolder).onFullyVisible()
        }
        if (!secondItems.isEmpty()) {
            ((secondItems as LinkedList<*>).last as ViewHolder).onFullyVisible()
        }
        if (!thirdItems.isEmpty()) {
            ((thirdItems as LinkedList<*>).last as ViewHolder).onFullyVisible()
        }
        if (!fourthItems.isEmpty()) {
            ((fourthItems as LinkedList<*>).last as ViewHolder).onFullyVisible()
        }
    }

    /**
     * Inflates a view and adds it to the hierarchy and sets click listeners
     *
     * @param section Section to create view for
     * @return The view that was added.
     */
    private fun createViewHolderFromAdapter(@Section section: Int): ViewHolder? {
        val viewHolder = adapter?.createView(section, this)
        if (!(section == FIRST && disableFirstSectionAnimation)) {
            viewHolder?.rootView?.setOnClickListener(clickListener)
        }
        addView(viewHolder?.rootView)
        return viewHolder
    }

    override fun onLayout(
        changed: Boolean,
        l: Int,
        t: Int,
        r: Int,
        b: Int
    ) {
        super.onLayout(changed, l, t, r, b)
    }

    /**
     * The height of stack layout is the sum of all the stacked tickets height + section headers if any.
     *
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val firstListPresent = !firstItems.isEmpty()
        val secondListPresent = !secondItems.isEmpty()
        val thirdListPresent = !thirdItems.isEmpty()
        val fourthListPresent = !fourthItems.isEmpty()
        val heightPadding = paddingTop + paddingBottom
        if (!isSelectedFlag) {
            var firsListHeight = 0
            var secondListHeight = 0
            var thirdListHeight = 0
            var fourthListHeight = 0
            val firstHeaderHeight = firstHeaderHeight
            val secondHeaderHeight = secondHeaderHeight
            val thirdHeaderHeight = thirdHeaderHeight
            val fourthHeaderHeight = fourthHeaderHeight
            if (firstListPresent) {
                firsListHeight =
                    firstItems.last.rootView.height + (firstItems.size - 1) * stackGapNormalPx
            }
            if (secondListPresent) {
                secondListHeight =
                    secondItems.last.rootView.height + (secondItems.size - 1) * stackGapNormalPx
            }
            if (thirdListPresent) {
                thirdListHeight =
                    thirdItems.last.rootView.height + (thirdItems.size - 1) * stackGapNormalPx
            }
            if (fourthListPresent) {
                fourthListHeight =
                    fourthItems.last.rootView.height + (fourthItems.size - 1) * stackGapNormalPx
            }
            val totalHeight = (firstHeaderHeight
                    + firsListHeight
                    + secondHeaderHeight
                    + secondListHeight
                    + thirdHeaderHeight
                    + thirdListHeight
                    + fourthHeaderHeight
                    + fourthListHeight
                    + heightPadding)
            setMeasuredDimension(measuredWidth, totalHeight)
        } else {
            val selectedItemHeight = selectedItemView?.height ?: 0
            val shrunkItemsHeight: Int
            var lastItemInBottomStackHeight = 0
            val supplementaryViewHeight = supplementaryViewHeight
            if (firstItems.size > 1) {
                lastItemInBottomStackHeight =
                    firstItems[firstItems.size - 2].rootView.height
            }
            if (secondItems.size > 1) {
                lastItemInBottomStackHeight =
                    secondItems[secondItems.size - 2].rootView.height
            }
            if (thirdItems.size > 1) {
                lastItemInBottomStackHeight =
                    thirdItems[thirdItems.size - 2].rootView.height
            }
            if (fourthItems.size > 1) {
                lastItemInBottomStackHeight =
                    fourthItems[fourthItems.size - 2].rootView.height
            }
            val totalShrunkItems =
                firstItems.size + secondItems.size + thirdItems.size + fourthItems.size - 2
            shrunkItemsHeight =
                totalShrunkItems * stackGapSelectedPx + lastItemInBottomStackHeight

            // Timber.d("shrunk %d , height %d, last %d", totalShrunkItems, shrunkItemsHeight, lastItemInBottomStackHeight);
            val totalHeight = (selectedItemHeight
                    + supplementaryViewHeight
                    + shrunkItemsHeight
                    + heightPadding)
            setMeasuredDimension(measuredWidth, totalHeight)
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        viewCache?.dropCache()
        cancelAnimations()
    }

    private val fourthHeaderHeight: Int
        get() {
            if (!showFourthHeader) {
                return 0
            }
            return (if (fourthItems.isEmpty()) {
                fourthHeader.visibility = View.INVISIBLE
                0
            } else {
                fourthHeader.visibility = View.VISIBLE
                fourthHeader.height
            })
        }

    /**
     * Checks if we should show the header by checking the size of [.secondItems]. If we can
     * then returns the actual height, if not hides the header and returns 0
     *
     * @return True height if middle stack is not empty
     */
    private val thirdHeaderHeight: Int
        get() {
            if (!showThirdHeader) {
                return 0
            }
            return (if (thirdItems.isEmpty()) {
                thirdHeader.visibility = View.INVISIBLE
                0
            } else {
                thirdHeader.visibility = View.VISIBLE
                thirdHeader.height
            })
        }

    /**
     * Checks if we should show the header by checking the size of [.secondItems]. If we can
     * then returns the actual height, if not hides the header and returns 0
     *
     * @return True height if middle stack is not empty
     */
    private val secondHeaderHeight: Int
        get() {
            if (!showSecondHeader) {
                return 0
            }
            return (if (secondItems.isEmpty()) {
                secondHeader.visibility = View.INVISIBLE
                0
            } else {
                secondHeader.visibility = View.VISIBLE
                secondHeader.height
            })
        }

    /**
     * Checks if we should show the header by checking the size of [.firstItems]. If we can
     * then returns the actual height, if not hides the header and returns 0
     *
     * @return True height if top stack is not empty
     */
    private val firstHeaderHeight: Int
        get() {
            if (!showFirstHeader) {
                return 0
            }
            return (if (firstItems.isEmpty()) {
                firstHeader.visibility = View.INVISIBLE
                0
            } else {
                firstHeader.visibility = View.VISIBLE
                firstHeader.height
            })
        }

    /**
     * Set the data source adapter for stack layout. Use the adapter instance to modify content of
     * this view
     *
     * @param adapter Adapter instance
     */
    fun setAdapter(adapter: ObservableBaseStackAdapter<*>) {
        this.adapter = adapter as ObservableBaseStackAdapter<ViewHolder>
        this.adapter?.registerObserver(observer)
        stackSelectionObserver = adapter.selectionObserver
        initItems()
        postResetStack()
    }

    /**
     * Resets the stack to normal mode. i.e un selected mode.
     *
     * @param endRunnable Runnable to execute after animations
     */
    fun requestResetStack(endRunnable: Runnable?) {
        postResetStack(endRunnable)
    }

    private fun reInitializeFourth() {
        adapter?.let {
            for (i in 0 until it.fourthSectionCount) {
                it.bindFourthSection(fourthItems[i], i)
            }
        }
        postResetStack()
    }

    /**
     * This means the we have to re render the entire Bottom stack. So we will re use the views that
     * are already inflated and inflate new ones if we need them.
     */
    private fun reInitializeThird() {
        adapter?.let {
            for (i in 0 until it.thirdSectionCount) {
                it.bindThirdSection(thirdItems[i], i)
            }
        }
        postResetStack()
    }

    /**
     * This means the we have to re render the entire Middle stack. So we will re use the views that
     * are already inflated and inflate new ones if we need them.
     */
    private fun reInitializeSecond() {
        adapter?.let {
            for (i in 0 until it.secondSectionCount) {
                it.bindSeconSectionView(secondItems[i], i)
            }
        }
        postResetStack()
    }

    /**
     * This means the we have to re render the entire Top stack. So we will re use the views that
     * are already inflated and inflate new ones if we need them.
     */
    private fun reInitializeFirst() {
        adapter?.let {
            for (i in 0 until it.firstSectionCount) {
                it.bindFirstSectionView(firstItems[i], i)
            }
        }
        postResetStack()
    }

    /**
     * Ideally whenever we get fresh data, we could remove all the views we have and blindly create new
     * ones and call [StackAdapter.bindFirstSectionView] or any sections. This is fine for
     * first time, but is terribly inefficient if we already have some views being shown.
     *
     *
     * So instead of creating new views all the time, we will reuse existing views and just update the content.
     * To facilitate this, this method will compare old size, new size and fill in missing gaps.
     *
     *
     * Example:
     * If we already have 4 items, and new data size is 5, then will just create 1 view and reuse the
     * 4 old views.
     */
    private fun reuseOrCreateViewsForSection(@Section section: Int) {
        val existingCount: Int
        val newCount: Int
        val diff: Int
        when (section) {
            FIRST -> {
                existingCount = firstItems.size
                newCount = adapter?.firstSectionCount ?: 0
                diff = existingCount - newCount
                if (diff != 0) {
                    if (diff > 0) {
                        // Remove extra views
                        shrinkViewList(firstItems, newCount)
                    } else {
                        expandViewList(FIRST, firstItems, newCount)
                    }
                }
            }
            SECOND -> {
                existingCount = secondItems.size
                newCount = adapter?.secondSectionCount ?: 0
                diff = existingCount - newCount
                if (diff != 0) {
                    if (diff > 0) {
                        // Remove extra views
                        shrinkViewList(secondItems, newCount)
                    } else {
                        expandViewList(SECOND, secondItems, newCount)
                    }
                }
            }
            THIRD -> {
                existingCount = thirdItems.size
                newCount = adapter?.thirdSectionCount ?: 0
                diff = existingCount - newCount
                if (diff != 0) {
                    if (diff > 0) {
                        // Remove extra views
                        shrinkViewList(thirdItems, newCount)
                    } else {
                        expandViewList(THIRD, thirdItems, newCount)
                    }
                }
            }
            FOURTH -> {
                existingCount = fourthItems.size
                newCount = adapter?.fourthSectionCount ?: 0
                diff = existingCount - newCount
                if (diff != 0) {
                    if (diff > 0) {
                        // Remove extra views
                        shrinkViewList(fourthItems, newCount)
                    } else {
                        expandViewList(FOURTH, fourthItems, newCount)
                    }
                }
            }
        }
    }

    /**
     * Fills in empty places with new view objects bound from adapter
     *
     * @param section  section
     * @param viewList list to expand for
     * @param newSize  size of new list
     */
    private fun expandViewList(
        @Section section: Int,
        viewList: MutableList<ViewHolder>,
        newSize: Int
    ) {
        var offset = 5 /* No of section headers + supplementary view */
        when (section) {
            FIRST -> offset = 5
            SECOND -> offset += firstItems.size
            THIRD -> offset += firstItems.size + secondItems.size
            FOURTH -> offset += firstItems.size + secondItems.size + thirdItems.size
            else -> offset = 0
        }
        for (i in viewList.size - 1 until newSize - 1) {
            val viewHolder =
                adapter?.createView(section, this)
            if (!(section == FIRST && disableFirstSectionAnimation)) {
                viewHolder?.rootView?.setOnClickListener(clickListener)
            }
            addView(viewHolder?.rootView, offset)
            viewHolder?.let { viewList.add(it) }
        }
        Timber.v("New size %d", viewList.size)
    }

    /**
     * Shrinks the view list to specified list and removes the extra views from hierarchy
     *
     * @param viewHolders
     * @param newSize
     */
    private fun shrinkViewList(
        viewHolders: LinkedList<ViewHolder>,
        newSize: Int
    ) {
        val listSize = viewHolders.size
        for (index in newSize until listSize) {
            val view = viewHolders[index].rootView
            removeView(view)
        }
        viewHolders.subList(newSize, listSize).clear()
        Timber.v("New size %d", viewHolders.size)
    }

    /**
     * Used to calculate the scale of the item in the stack. This is needed so that realistic stack
     * effect is achieved by keeping a minimum scale specified by [] and gradually increasing
     * it to 1 for the last item in the list.
     *
     * @param itemIndex Position of item in list
     * @param list      The list against which the scale is to be calculated
     * @return The scaled value
     */
    @FloatRange(from = 0.0, to = 1.0)
    private fun getScaleForPosition(
        itemIndex: Int,
        list: List<*>
    ): Float {
        return getScaleForPosition(itemIndex, list.size)
    }

    /**
     * Similar to [.getScaleForPosition] just that you are specifying list size
     * manually
     */
    @FloatRange(from = 0.0, to = 1.0)
    private fun getScaleForPosition(itemIndex: Int, maxSize: Int): Float {
        return Util.mapValueFromRangeToRange(
            itemIndex.toDouble(),
            0.0,
            maxSize.toDouble(),
            MINIMUM_SCALE_FACTOR.toDouble(),
            1.0
        ).toFloat()
    }

    private fun getElevationForPosition(
        itemIndex: Int,
        list: List<*>
    ): Float {
        return getElevationForPosition(itemIndex, list.size)
    }

    private fun getElevationForPosition(itemIndex: Int, maxSize: Int): Float {
        return Util.mapValueFromRangeToRange(
            itemIndex.toDouble(),
            0.0,
            maxSize.toDouble(),
            MINIMUM_ELEVATION.toDouble(),
            MAXIMUM_ELEVATION.toDouble()
        ).toFloat()
    }

    fun setMinimumScaleFactor(
        @FloatRange(
            from = 0.0,
            to = 1.0
        ) minimumScaleFactor: Float
    ) {
        MINIMUM_SCALE_FACTOR = minimumScaleFactor
        refreshStack()
    }

    fun setMinimumScaleFactorSelected(
        @FloatRange(
            from = 0.0,
            to = 1.0
        ) minimumScaleFactor: Float
    ) {
        MINIMUM_SCALE_FACTOR_FOCUSED = minimumScaleFactor
        refreshStack()
    }

    fun setStackGapSelectedPx(stackGapSelectedPx: Int) {
        this.stackGapSelectedPx = stackGapSelectedPx
    }

    fun showTopHeader(showTopHeader: Boolean) {
        showFirstHeader = showTopHeader
        refreshStack()
    }

    /**
     * If the parent is a scrollview, then scrolls it to the top.
     * Note: will not work the there is a layout pass in progress. So ensure you are calling this
     * after layout is done.
     */
    private fun scrollParentToTop() {
        val parentView = parent
        if (parentView is ScrollView) {
            val scrollView = parentView
            scrollView.smoothScrollTo(0, 0)
            // ANDROID-1360 wants scrolling
            scrollView.setOnTouchListener { view: View?, motionEvent: MotionEvent? -> true }
        }
    }

    /**
     * Restores scrolling on the parent scroll view.
     */
    private fun restoreScrolling() {
        // Commented for ANDROID-1360
        val parentView = parent
        if (parentView is ScrollView) {
            parentView.setOnTouchListener(null)
        }
    }

    fun setSecondHeaderText(@StringRes res: Int) {
        secondHeader.setText(res)
    }

    /**
     * This is the layout that will be shown when a item in the stack is selected.
     *
     * @param supplementaryLayout
     * @return The inflated view. Note: The inflated layout will not be measured yet.
     */
    fun setSupplementaryViewLayout(@LayoutRes supplementaryLayout: Int): View? {
        removeView(supplementaryView)
        supplementaryView =
            LayoutInflater.from(context).inflate(supplementaryLayout, this, false)
        supplementaryView.alpha = 0f
        addView(supplementaryView, firstItems.size + 1)
        Util.registerForLayout(
            supplementaryView,
            object : LayoutDoneListener<View> {
                override fun onLayoutFinished(view: View) {
                    if (isSelectedFlag) {
                        selectedItemView?.let { setSelectedItem(it) }
                    } else {
                        resetStack()
                    }
                }
            }
        )
        return supplementaryView
    }

    private inner class ViewClickListener : OnClickListener {
        override fun onClick(view: View) {
            if (isSelectedFlag) {
                resetStack()
            } else {
                setSelectedItem(view)
            }
        }
    }

    /**
     * Observer to receive events from attached adapter.
     */
    abstract class AdapterDataObserver {
        abstract fun onChanged()
        abstract fun onTopSectionReset()
        abstract fun onMiddleSectionReset()
        abstract fun onBottomSectionReset()
        abstract fun setSelection(
            @Section sectionType: Int,
            index: Int
        )

        abstract fun setSelectionObserver(stackSelectionObserver: StackSelectionObserver)
        abstract fun onFourthSectionReset()
    }

    class AdapterDataObservable :
        Observable<AdapterDataObserver?>() {
        fun notifyChanged() {
            for (i in mObservers.indices.reversed()) {
                mObservers[i]?.onChanged()
            }
        }

        fun notifyTopListReset() {
            for (i in mObservers.indices.reversed()) {
                mObservers[i]?.onTopSectionReset()
            }
        }

        fun notifyMiddleListReset() {
            for (i in mObservers.indices.reversed()) {
                mObservers[i]?.onMiddleSectionReset()
            }
        }

        fun notifyBottomListReset() {
            for (i in mObservers.indices.reversed()) {
                mObservers[i]?.onBottomSectionReset()
            }
        }

        fun setSelection(
            @Section sectionType: Int,
            index: Int
        ) {
            for (i in mObservers.indices.reversed()) {
                mObservers[i]?.setSelection(sectionType, index)
            }
        }

        fun setSelectionObserver(stackSelectionObserver: StackSelectionObserver) {
            for (i in mObservers.indices.reversed()) {
                mObservers[i]?.setSelectionObserver(stackSelectionObserver)
            }
        }

        fun notifyFourthListReset() {
            for (i in mObservers.indices.reversed()) {
                mObservers[i]?.onFourthSectionReset()
            }
        }
    }

    private inner class ViewDataObserver :
        AdapterDataObserver() {
        override fun onChanged() {
            reuseOrCreateViewsForSection(FIRST)
            reuseOrCreateViewsForSection(SECOND)
            reuseOrCreateViewsForSection(THIRD)
            reuseOrCreateViewsForSection(FOURTH)
            reInitializeFirst()
            reInitializeSecond()
            reInitializeThird()
            reInitializeFourth()
            postResetStack()
            selectedStateFlag = false
        }

        override fun onTopSectionReset() {
            reuseOrCreateViewsForSection(FIRST)
            reInitializeFirst()
        }

        override fun onMiddleSectionReset() {
            reuseOrCreateViewsForSection(SECOND)
            reInitializeSecond()
        }

        override fun onBottomSectionReset() {
            reuseOrCreateViewsForSection(THIRD)
            reInitializeThird()
        }

        override fun onFourthSectionReset() {
            reuseOrCreateViewsForSection(FOURTH)
            reInitializeFourth()
        }

        override fun setSelection(
            @Section sectionType: Int,
            index: Int
        ) {
            postResetStack(Runnable {
                when (sectionType) {
                    FIRST -> setSelectedItem(firstItems[index].rootView)
                    SECOND -> setSelectedItem(secondItems[index].rootView)
                    THIRD -> setSelectedItem(thirdItems[index].rootView)
                    FOURTH -> setSelectedItem(fourthItems[index].rootView)
                }
            })
        }

        override fun setSelectionObserver(stackSelectionObserver: StackSelectionObserver) {
            this@StackViewLayout.stackSelectionObserver = stackSelectionObserver
        }
    }

    interface MultiSelectCallback {
        fun onMultiAnimationEnded(
            @Section section: Int,
            index: Int,
            absoluteIndex: Int,
            singleItem: Boolean
        )
    }

    interface StackSelectionObserver {
        fun onStackItemSelected(
            @Section sectionType: Int,
            position: Int
        )

        fun onStackItemUnSelected(
            @Section sectionType: Int,
            position: Int
        )
    }

    companion object {
        const val FIRST = 0
        const val SECOND = 1
        const val THIRD = 2
        const val FOURTH = 3
        const val NO_SELECTION = -1

        // Denotes the size of gap between two tickets in pixels when normal.
        private val STACK_GAP_NORMAL = Util.dpToPx(61.0)

        // Denotes the size of gap between two tickets in pixels when stacked at the bottom.
        private val STACK_GAP_SELECTED = Util.dpToPx(12.0)

        // The top element will have mim elevation and last item will have max elevation. All items in
        // middle have linearly interpolated values.
        private val MINIMUM_ELEVATION =
            Util.dpToPx(4.0).toFloat()
        private val MAXIMUM_ELEVATION =
            Util.dpToPx(12.0).toFloat()
        private val SPRING_INTERPOLATOR = SpringInterpolator(0.2, 4.0)
    }
}