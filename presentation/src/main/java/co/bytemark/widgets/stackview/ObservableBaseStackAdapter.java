package co.bytemark.widgets.stackview;

import androidx.annotation.NonNull;
import android.view.ViewGroup;

import co.bytemark.widgets.stackview.annotations.Section;
import timber.log.Timber;

/**
 * Created by Arunkumar on 22/07/16.
 */
public abstract class ObservableBaseStackAdapter<VH extends ViewHolder> implements StackViewLayout.StackSelectionObserver {

    private final StackViewLayout.AdapterDataObservable observable = new StackViewLayout.AdapterDataObservable();

    ObservableBaseStackAdapter() {
        observable.setSelectionObserver(this);
    }

    VH createView(@Section int section, ViewGroup parent) {
        return onCreateViewHolder(section, parent);
    }

    protected abstract VH onCreateViewHolder(@Section int section, ViewGroup parent);

    void bindFirstSectionView(VH view, int position) {
        onBindFirstSectionViewHolder(view, position);
    }

    protected abstract void onBindFirstSectionViewHolder(VH view, int position);

    void bindSeconSectionView(VH view, int position) {
        onBindSecondSectionViewHolder(view, position);
    }

    protected abstract void onBindSecondSectionViewHolder(VH view, int position);

    void bindThirdSection(VH view, int position) {
        onBindThirdSectionViewHolder(view, position);
    }

    protected abstract void onBindThirdSectionViewHolder(VH view, int position);

    void bindFourthSection(VH view, int position) {
        onBindFourthSectionViewHolder(view, position);
    }

    protected abstract void onBindFourthSectionViewHolder(VH view, int position);

    protected final void notifyDataSetChanged() {
        observable.notifyChanged();
    }

    protected final void notifyFirstListReset() {
        observable.notifyTopListReset();
    }

    protected final void notifySecondListReset() {
        observable.notifyMiddleListReset();
    }

    final void notifyThirdListReset() {
        observable.notifyBottomListReset();
    }

    final void notifyFourthListReset() {
        observable.notifyFourthListReset();
    }

    public abstract int getFirstSectionCount();

    public abstract int getSecondSectionCount();

    public abstract int getThirdSectionCount();

    public abstract int getFourthSectionCount();

    public int getCount() {
        return getFirstSectionCount()
                + getThirdSectionCount()
                + getSecondSectionCount()
                + getFourthSectionCount();
    }

    final void registerObserver(@NonNull StackViewLayout.AdapterDataObserver observer) {
        observable.registerObserver(observer);
    }

    public void cleanUp() {
        Timber.d("Cleaned up adapter");
        observable.unregisterAll();
    }

    @NonNull
    final StackViewLayout.StackSelectionObserver getSelectionObserver() {
        return this;
    }

    public final void unRegisterObserver(@NonNull StackViewLayout.AdapterDataObserver observer) {
        observable.unregisterObserver(observer);
    }

    protected void setSelection(@Section final int section, final int index) {
        observable.setSelection(section, index);
    }


}
