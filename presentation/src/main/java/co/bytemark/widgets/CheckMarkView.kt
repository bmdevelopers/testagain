package co.bytemark.widgets

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import co.bytemark.R
import co.bytemark.stylekit.BMWLIcons

class CheckMarkView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : View(context, attrs, defStyle) {

    private var targetFrame: RectF? = null
    private var checkLength = 0f
    private var materialWidth = 0f
    private var materialColor = 0
    private var anim: ValueAnimator? = null

    fun setMaterialWidth(materialWidth: Float) {
        this.materialWidth = materialWidth
    }

    init {
        val a = context.obtainStyledAttributes(
            attrs,
            R.styleable.CheckMarkView,
            defStyle,
            0
        )
        materialWidth = a.getDimension(R.styleable.CheckMarkView_materialWidth, 0f)
        materialColor = a.getColor(
            R.styleable.CheckMarkView_materialColor,
            ContextCompat.getColor(context, R.color.colorPrimary)
        )
        a.recycle()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        targetFrame = RectF(0F, 0F, width.toFloat(), height.toFloat())
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        BMWLIcons.drawCheckmarkMaterial(
            canvas,
            targetFrame,
            BMWLIcons.ResizingBehavior.AspectFit,
            materialColor,
            checkLength,
            materialWidth
        )
    }

    fun setCheckMarkColor(@ColorInt color: Int) {
        materialColor = color
        invalidate()
    }

    override fun setSelected(selected: Boolean) {
        super.setSelected(selected)
        if (anim != null) {
            anim?.cancel()
        }
        if (selected) {
            anim = ValueAnimator.ofFloat(0f, MAX_CHECK_LENGTH.toFloat())
            anim?.addUpdateListener { animation: ValueAnimator ->
                checkLength = animation.animatedValue as Float
                invalidate()
            }
        } else {
            anim = ValueAnimator.ofFloat(MAX_CHECK_LENGTH.toFloat(), 0f)
            anim?.addUpdateListener { animation: ValueAnimator ->
                checkLength = animation.animatedValue as Float
                invalidate()
            }
        }
        anim?.interpolator = AccelerateDecelerateInterpolator()
        anim?.duration = 200
        anim?.start()
    }

    companion object {
        private const val MAX_CHECK_LENGTH = 50
    }
}