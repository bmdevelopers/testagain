package co.bytemark.widgets

import android.content.Context
import android.graphics.drawable.StateListDrawable
import androidx.core.content.ContextCompat
import android.view.Gravity
import android.widget.RadioButton
import android.widget.RadioGroup
import co.bytemark.R
import co.bytemark.widgets.util.Util.dpToPx

class BmRadioButton(context: Context) : RadioButton(context) {

    init {
        layoutParams = RadioGroup.LayoutParams(dpToPx(48.0), dpToPx(36.0)).apply {
            setMargins(dpToPx(2.0), dpToPx(2.0), dpToPx(2.0), dpToPx(2.0))
        }
        gravity = Gravity.CENTER
        buttonDrawable = StateListDrawable()
        background = ContextCompat.getDrawable(context, R.drawable.selector_background_radio_button_amount)
        setTextColor(ContextCompat.getColorStateList(context, R.color.selector_text_color_radio_button_amount))
    }

}