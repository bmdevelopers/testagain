package co.bytemark.widgets

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager

class LinearDividerRecyclerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearRecyclerView(context, attrs, defStyle) {

    init {
        val linearLayoutManager =
            LinearLayoutManager(context)
        val dividerItemDecoration =
            DividerItemDecoration(
                context,
                linearLayoutManager.orientation
            )
        addItemDecoration(dividerItemDecoration)
        layoutManager = linearLayoutManager
    }
}