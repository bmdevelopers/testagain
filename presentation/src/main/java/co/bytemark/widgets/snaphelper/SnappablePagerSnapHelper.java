package co.bytemark.widgets.snaphelper;

import android.graphics.PointF;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import android.util.DisplayMetrics;
import android.view.View;

import rx.Observable;
import rx.Subscription;
import rx.subjects.PublishSubject;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

/**
 * Extension to PagerSnapHelper which provides ability to snap to any position on request and
 * get notified of the said snap.
 */
public class SnappablePagerSnapHelper extends SnapHelper {
    private RecyclerView recyclerView;

    private static final int MAX_SCROLL_ON_FLING_DURATION = 100; // ms
    private static final float MILLISECONDS_PER_INCH = 100f;

    private int currentSnapPosition = -1;

    // Orientation helpers are lazily created per LayoutManager.
    @Nullable
    private OrientationHelper verticalHelper;
    @Nullable
    private OrientationHelper horizontalHelper;

    /**
     * Snap subject to broadcast snap events. The stream is of type int which the position of the snap.
     */
    private final PublishSubject<Integer> snapsSubject;
    private final Subscription subscription;

    public SnappablePagerSnapHelper(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        snapsSubject = PublishSubject.create();

        subscription = snapsSubject
                .filter(integer -> integer != NO_POSITION)
                .distinctUntilChanged()
                .doOnNext(integer -> currentSnapPosition = integer)
                .subscribe();

        attachToRecyclerView(recyclerView);
    }


    @Override
    public void attachToRecyclerView(@Nullable RecyclerView recyclerView) throws IllegalStateException {
        super.attachToRecyclerView(recyclerView);
        if (recyclerView == null) {
            this.recyclerView = null;
            subscription.unsubscribe();
        }
    }

    @Nullable
    @Override
    public int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager layoutManager,
                                              @NonNull View targetView) {
        int[] out = new int[2];
        if (layoutManager.canScrollHorizontally()) {
            out[0] = distanceToCenter(layoutManager, targetView, getHorizontalHelper(layoutManager));
        } else {
            out[0] = 0;
        }

        if (layoutManager.canScrollVertically()) {
            out[1] = distanceToCenter(layoutManager, targetView, getVerticalHelper(layoutManager));
        } else {
            out[1] = 0;
        }
        return out;
    }

    @Nullable
    @Override
    public View findSnapView(RecyclerView.LayoutManager layoutManager) {
        View snapView = null;
        if (layoutManager.canScrollVertically()) {
            snapView = findCenterView(layoutManager, getVerticalHelper(layoutManager));
        } else if (layoutManager.canScrollHorizontally()) {
            snapView = findCenterView(layoutManager, getHorizontalHelper(layoutManager));
        }
        return snapView;
    }

    @Override
    public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX,
                                      int velocityY) {
        final int itemCount = layoutManager.getItemCount();
        if (itemCount == 0) {
            return NO_POSITION;
        }

        View mStartMostChildView = null;
        if (layoutManager.canScrollVertically()) {
            mStartMostChildView = findStartView(layoutManager, getVerticalHelper(layoutManager));
        } else if (layoutManager.canScrollHorizontally()) {
            mStartMostChildView = findStartView(layoutManager, getHorizontalHelper(layoutManager));
        }

        if (mStartMostChildView == null) {
            return NO_POSITION;
        }
        final int centerPosition = layoutManager.getPosition(mStartMostChildView);
        if (centerPosition == NO_POSITION) {
            return NO_POSITION;
        }

        final boolean forwardDirection;
        if (layoutManager.canScrollHorizontally()) {
            forwardDirection = velocityX > 0;
        } else {
            forwardDirection = velocityY > 0;
        }
        boolean reverseLayout = false;
        if ((layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
            RecyclerView.SmoothScroller.ScrollVectorProvider vectorProvider = (RecyclerView.SmoothScroller.ScrollVectorProvider) layoutManager;
            PointF vectorForEnd = vectorProvider.computeScrollVectorForPosition(itemCount - 1);
            if (vectorForEnd != null) {
                reverseLayout = vectorForEnd.x < 0 || vectorForEnd.y < 0;
            }
        }
        final int snapPosition = reverseLayout
                ? (forwardDirection ? centerPosition - 1 : centerPosition)
                : (forwardDirection ? centerPosition + 1 : centerPosition);
        snapsSubject.onNext(snapPosition);
        return snapPosition;
    }

    @Override
    protected LinearSmoothScroller createSnapScroller(RecyclerView.LayoutManager layoutManager) {
        return createSnapScroller(layoutManager, null);
    }

    /**
     * Custom extension to LinearSmoothScroller which has a callback when there is a target found
     * but there is no scrolling necessary meaning we are already at the position we are meant to
     * scroll towards.
     * <p>
     * You specify what action to be executed when this occurs via {@param noScrollingRunnable}. If
     * you are not interested in handling this case, then give null.
     *
     * @param layoutManager       Layout manager of RecyclerView.
     * @param noScrollingRunnable Runnable to execute when there is a scroll event requested for a
     *                            position but the Scroller determined that we are already at that
     *                            position.
     * @return Linear smooth scroller.
     */
    private LinearSmoothScroller createSnapScroller(RecyclerView.LayoutManager layoutManager, final Runnable noScrollingRunnable) {
        if (!(layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
            return null;
        }
        return new LinearSmoothScroller(recyclerView.getContext()) {
            @Override
            protected void onTargetFound(View targetView, RecyclerView.State state, Action action) {
                int[] snapDistances = calculateDistanceToFinalSnap(recyclerView.getLayoutManager(),
                        targetView);
                final int dx = snapDistances[0];
                final int dy = snapDistances[1];
                final int time = calculateTimeForDeceleration(Math.max(Math.abs(dx), Math.abs(dy)));
                if (time > 0) {
                    action.update(dx, dy, time, mDecelerateInterpolator);
                } else {
                    execute(noScrollingRunnable);
                }
            }

            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
            }

            @Override
            protected int calculateTimeForScrolling(int dx) {
                return Math.min(MAX_SCROLL_ON_FLING_DURATION, super.calculateTimeForScrolling(dx));
            }
        };
    }

    private int distanceToCenter(@NonNull RecyclerView.LayoutManager layoutManager, @NonNull View targetView, OrientationHelper helper) {
        final int childCenter = helper.getDecoratedStart(targetView)
                + (helper.getDecoratedMeasurement(targetView) / 2);
        final int containerCenter;
        if (layoutManager.getClipToPadding()) {
            containerCenter = helper.getStartAfterPadding() + helper.getTotalSpace() / 2;
        } else {
            containerCenter = helper.getEnd() / 2;
        }
        return childCenter - containerCenter;
    }

    /**
     * Return the child view that is currently closest to the center of this parent.
     *
     * @param layoutManager The {@link RecyclerView.LayoutManager} associated with the attached
     *                      {@link RecyclerView}.
     * @param helper        The relevant {@link OrientationHelper} for the attached {@link RecyclerView}.
     * @return the child view that is currently closest to the center of this parent.
     */
    @Nullable
    private View findCenterView(RecyclerView.LayoutManager layoutManager, OrientationHelper helper) {
        int childCount = layoutManager.getChildCount();
        if (childCount == 0) {
            return null;
        }

        View closestChild = null;
        final int center;
        if (layoutManager.getClipToPadding()) {
            center = helper.getStartAfterPadding() + helper.getTotalSpace() / 2;
        } else {
            center = helper.getEnd() / 2;
        }
        int absClosest = Integer.MAX_VALUE;

        for (int i = 0; i < childCount; i++) {
            final View child = layoutManager.getChildAt(i);
            int childCenter = helper.getDecoratedStart(child)
                    + (helper.getDecoratedMeasurement(child) / 2);
            int absDistance = Math.abs(childCenter - center);

            if (absDistance < absClosest) {
                absClosest = absDistance;
                closestChild = child;
            }
        }
        return closestChild;
    }

    /**
     * Return the child view that is currently closest to the start of this parent.
     *
     * @param layoutManager The {@link RecyclerView.LayoutManager} associated with the attached
     *                      {@link RecyclerView}.
     * @param helper        The relevant {@link OrientationHelper} for the attached {@link RecyclerView}.
     * @return the child view that is currently closest to the start of this parent.
     */
    @Nullable
    private View findStartView(RecyclerView.LayoutManager layoutManager, OrientationHelper helper) {
        int childCount = layoutManager.getChildCount();
        if (childCount == 0) {
            return null;
        }

        View closestChild = null;
        int startest = Integer.MAX_VALUE;

        for (int i = 0; i < childCount; i++) {
            final View child = layoutManager.getChildAt(i);
            int childStart = helper.getDecoratedStart(child);

            if (childStart < startest) {
                startest = childStart;
                closestChild = child;
            }
        }
        return closestChild;
    }

    private int getSnappedPosition() {
        final RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            return ((LinearLayoutManager) layoutManager).findFirstCompletelyVisibleItemPosition();
        }
        return NO_POSITION;
    }


    @NonNull
    private OrientationHelper getVerticalHelper(@NonNull RecyclerView.LayoutManager layoutManager) {
        if (verticalHelper == null || verticalHelper.layoutManager != layoutManager) {
            verticalHelper = OrientationHelper.createVerticalHelper(layoutManager);
        }
        return verticalHelper;
    }

    @NonNull
    private OrientationHelper getHorizontalHelper(
            @NonNull RecyclerView.LayoutManager layoutManager) {
        if (horizontalHelper == null || horizontalHelper.layoutManager != layoutManager) {
            horizontalHelper = OrientationHelper.createHorizontalHelper(layoutManager);
        }
        return horizontalHelper;
    }

    public void snapTo(int position, Runnable endRunnable) {
        if (recyclerView == null) {
            return;
        }

        // NOTE: When there is only one item, recycler view does not scroll. Handle this edge case.
        if (position == 0 && recyclerView.getAdapter().getItemCount() == 1) {
            recyclerView.post(() -> {
                snapsSubject.onNext(position);
                execute(endRunnable);
            });
            return;
        }

        // Handle for all other cases.

        // We attach a scroll listener so that we execute endRunnable, only after the scroll is
        // completed.
        final RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    recyclerView.removeOnScrollListener(this);
                    snapsSubject.onNext(position);
                    execute(endRunnable);
                }
            }
        };

        // If the scroller is not able to scroll, then we remove the listener and then just execute
        // the runnable.
        final Runnable scrollFailedRunnable = () -> {
            recyclerView.removeOnScrollListener(scrollListener);
            snapsSubject.onNext(position);
            execute(endRunnable);
        };

        final LinearSmoothScroller smoothScroller = createSnapScroller(recyclerView.getLayoutManager(), scrollFailedRunnable);
        smoothScroller.setTargetPosition(position);

        recyclerView.addOnScrollListener(scrollListener);

        recyclerView.getLayoutManager().startSmoothScroll(smoothScroller);
    }

    public Observable<Integer> snaps() {
        return snapsSubject.asObservable();
    }

    public int getCurrentSnapPosition() {
        return currentSnapPosition;
    }

    private void execute(@Nullable Runnable endRunnable) {
        if (endRunnable != null) {
            endRunnable.run();
        }
    }
}
