package co.bytemark.widgets

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import androidx.annotation.IntDef
import androidx.annotation.StringRes
import androidx.appcompat.widget.AppCompatTextView
import co.bytemark.R

class LoadingTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
): AppCompatTextView(context, attrs, defStyle) {
    private var loadingCounter = 0
    private var animHandler: Handler? = null
    private lateinit var loadingString: String
    private val animateRunnable = Runnable {
        loadingCounter %= MAX_COUNT
        val builder = StringBuilder(loadingString)
        for (i in 0..loadingCounter) {
            builder.append('.')
        }
        text = builder.toString()
        loadingCounter++
        beginAnimation()
    }

    @IntDef(
        LOADING,
        LOADED,
        PAUSED
    )
    annotation class LoadingState

    @LoadingState
    private var state = 0

    init {
        animHandler = Handler(Looper.getMainLooper())
        state = PAUSED
        loadingString = getContext().getString(R.string.loading).replace("…", "")
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (state == PAUSED) {
            beginAnimation()
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        if (state != LOADED) {
            setState(PAUSED, null)
        }
    }

    private fun beginAnimation() {
        animHandler?.postDelayed(animateRunnable, 200)
    }

    private fun setLoading() {
        setState(LOADING, null)
    }

    fun setLoadedText(charSequence: CharSequence?) {
        if (charSequence == null) {
            setLoading()
        } else {
            setState(LOADED, charSequence)
        }
    }

    fun setLoadedText(@StringRes stringRes: Int) {
        setState(LOADED, context.getString(stringRes))
    }

    private fun setState(
        @LoadingState state: Int,
        charSequence: CharSequence?
    ) {
        animHandler?.removeCallbacksAndMessages(null)
        this.state = state
        when (state) {
            LOADED -> text = charSequence
            LOADING -> beginAnimation()
            PAUSED -> {}
        }
    }

    companion object {
        private const val LOADING = 0
        private const val LOADED = 1
        private const val PAUSED = 2
        private const val MAX_COUNT = 3
    }
}