package co.bytemark.widgets.swiperefreshlayout

import android.content.Context
import android.util.AttributeSet
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.helpers.ConfHelper
import javax.inject.Inject

class BmSwipeRefreshLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
): SwipeRefreshLayout(context, attrs) {
    @Inject
    lateinit var confHelper: ConfHelper

    init {
        component.inject(this)
        setColorSchemeColors(
            confHelper.accentThemeBacgroundColor,
            confHelper.accentThemeAccentColor
        )
    }
}