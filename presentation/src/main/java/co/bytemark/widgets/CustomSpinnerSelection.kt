package co.bytemark.widgets

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatSpinner

class CustomSpinnerSelection @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AppCompatSpinner(context, attrs, defStyle) {
    private var mToggleFlag = true

    override fun getSelectedItemPosition(): Int {
        return if (!mToggleFlag) {
            0 // get us to the first element
        } else super.getSelectedItemPosition()
    }

    override fun performClick(): Boolean {
        mToggleFlag = false
        val result = super.performClick()
        mToggleFlag = true
        return result
    }
}