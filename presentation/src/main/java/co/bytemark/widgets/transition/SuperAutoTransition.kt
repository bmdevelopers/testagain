package co.bytemark.widgets.transition

import android.transition.AutoTransition
import co.bytemark.widgets.util.EndTransitionAdapter

class SuperAutoTransition : AutoTransition() {
    fun doOnEnd(runnable: Runnable) {
        addListener(EndTransitionAdapter(runnable))
    }
}