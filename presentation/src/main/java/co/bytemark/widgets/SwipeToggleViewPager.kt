package co.bytemark.widgets

import android.content.Context
import androidx.viewpager.widget.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent



/** Created by Santosh on 2019-08-30.
 */

class SwipeToggleViewPager(context: Context, attributeSet: AttributeSet)
    : ViewPager(context, attributeSet) {

    private var swipeEnabled : Boolean = true

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return swipeEnabled && super.onTouchEvent(event)
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return swipeEnabled && super.onInterceptTouchEvent(event)
    }

    fun setSwipeEnabled(enabled: Boolean) {
        this.swipeEnabled = enabled
    }

    fun isSwipeEnabled(): Boolean {
        return swipeEnabled
    }
}