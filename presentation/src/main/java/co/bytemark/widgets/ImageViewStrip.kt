package co.bytemark.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import co.bytemark.widgets.util.Util

class ImageViewStrip(
    context: Context?,
    attrs: AttributeSet?
) : LinearLayout(context, attrs) {
    fun setImageResources(imageResources: List<Int>) {
        removeAllViews()
        for (imageRes in imageResources) {
            val imageView = ImageView(context)
            imageView.setImageResource(imageRes)
            imageView.adjustViewBounds = true
            val layoutParams = LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            layoutParams.setMargins(
                Util.dpToPx(2.0),
                0,
                Util.dpToPx(2.0),
                0
            )
            layoutParams.gravity = Gravity.CENTER_VERTICAL
            imageView.layoutParams = layoutParams
            addView(imageView)
        }
        invalidate()
    }

    init {
        orientation = HORIZONTAL
    }
}