package co.bytemark.widgets.dynamicmenu

import android.text.TextUtils
import androidx.annotation.DrawableRes
import co.bytemark.R
import co.bytemark.sdk.model.menu.MenuItem

object MenuItemDrawableLoader {
    @JvmStatic
    @DrawableRes
    fun getDrawableIdRes(menuItem: String?): Int {
        if (menuItem == null || TextUtils.isEmpty(menuItem)) return -1
        return when (menuItem) {
            MenuItem.GEAR_FILLED -> R.drawable.ic_gear_filled
            MenuItem.MULTIPLE_TICKETS_FILLED -> R.drawable.ic_multiple_tickets_filled
            MenuItem.ADD_TO_CART_FILLED -> R.drawable.ic_add_to_cart_filled
            MenuItem.MAP_POINTS_FILLED -> R.drawable.ic_map_points_filled
            MenuItem.PROFILE_FILLED -> R.drawable.ic_profile_filled
            MenuItem.UNLOCKED_FILLED -> R.drawable.ic_unlock_filled
            MenuItem.PAYMENT_CARD_FILLED -> R.drawable.ic_payment_card_filled
            MenuItem.NOTIFICATIONS_FILLED -> R.drawable.ic_notification_setting_filled
            MenuItem.CLOUD_FILLED -> R.drawable.ic_cloud_filled
            MenuItem.CART_FILLED -> R.drawable.ic_cart_filled
            MenuItem.CONNECTIONS_FILLED -> R.drawable.ic_connections_filled
            MenuItem.EXIT_FILLED -> R.drawable.ic_exit_filled
            MenuItem.INFO_FILLED -> R.drawable.ic_info_filled
            MenuItem.DOCUMENT_FILLED -> R.drawable.ic_document_filled
            MenuItem.AWARD_FILLED -> R.drawable.ic_award_filled
            MenuItem.ENTER_FILLED -> R.drawable.ic_enter_filled
            MenuItem.HOME -> R.drawable.ic_home_filled
            MenuItem.HELP -> R.drawable.ic_help_filled
            MenuItem.FACEBOOK -> R.drawable.ic_facebook_filled
            MenuItem.TWITTER -> R.drawable.ic_twitter_filled
            MenuItem.YOUTUBE -> R.drawable.ic_youtube_filled
            MenuItem.FACEBOOK_COLOR -> R.drawable.ic_facebook_color
            MenuItem.TWITTER_COLOR -> R.drawable.ic_twitter_color
            MenuItem.YOUTUBE_COLOR -> R.drawable.youtube_color
            MenuItem.PINTEREST_COLOR -> R.drawable.ic_pinterest_color
            MenuItem.PHONE -> R.drawable.ic_phone_filled
            MenuItem.ASSISTANT -> R.drawable.ic_assistant_filled
            MenuItem.MAIL -> R.drawable.ic_email_filled
            MenuItem.AT -> R.drawable.ic_at_filled
            MenuItem.TERMS -> R.drawable.ic_terms_filled
            MenuItem.PRIVACY -> R.drawable.ic_shield_key_filled
            MenuItem.MORE_DOTS -> R.drawable.more_dots_material
            MenuItem.CAMERA -> R.drawable.ic_camera_filled
            MenuItem.TIMETABLE -> R.drawable.timetable_material
            MenuItem.LOST_AND_FOUND -> R.drawable.ic_lost_and_found_filled
            MenuItem.ACCOUNT_PHOTO -> R.drawable.ic_account_photo_filled
            MenuItem.CLOCK -> R.drawable.ic_clock_filled
            MenuItem.FLICKR_COLOR -> R.drawable.ic_flickr_color
            MenuItem.INSTAGRAM_COLOR -> R.drawable.ic_instagram_color
            MenuItem.FOURSQUARE_COLOR -> R.drawable.foursquare_color
            MenuItem.BUS -> R.drawable.ic_bus_filled
            MenuItem.PARKING -> R.drawable.ic_parking_filled
            MenuItem.FEEDBACK -> R.drawable.ic_feedback_filled
            MenuItem.PUSH_NOTIFICATION -> R.drawable.ic_push_notification_filled
            MenuItem.VOUCHER_CODE -> R.drawable.ic_voucher_code_filled
            MenuItem.SUBSCRIPTION_CODE -> R.drawable.ic_subscription_ticket_filled
            MenuItem.ALARMS_FILLED -> R.drawable.ic_alarm_filled
            MenuItem.BELL_FILLED -> R.drawable.ic_bell_filled
            MenuItem.WARNING_CODE -> R.drawable.ic_warning_filled
            MenuItem.PIN_EMPTY -> R.drawable.ic_pin_empty
            MenuItem.PIN_FILLED -> R.drawable.ic_pin_filled
            MenuItem.SECURITY_QUESTIONS_EMPTY -> R.drawable.ic_security
            MenuItem.SECURITY_QUESTIONS_FILLED -> R.drawable.ic_shield_question_filled
            MenuItem.CHECK_BALANCE_EMPTY -> R.drawable.ic_payment_empty
            MenuItem.CHECK_BALANCE_FILLED -> R.drawable.ic_payment_filled
            MenuItem.REPORT_PROBLEM_EMPTY -> R.drawable.ic_report_problem_filled
            MenuItem.TICKET_VOUCHER -> R.drawable.ic_voucher_code_filled
            MenuItem.SUPPORT -> R.drawable.ic_support_filled
            MenuItem.REPORT_TRANSIT_SECURITY_ISSUE -> R.drawable.ic_report_transit_security_issue_filled
            MenuItem.SURVEY_FILLED, MenuItem.SURVEY_EMPTY -> R.drawable.ic_survey_filled
            MenuItem.FARE_CAPPING_FILLED -> R.drawable.ic_farecapping_filled
            else -> -1
        }
    }
}