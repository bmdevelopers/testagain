package co.bytemark.widgets.dynamicmenu

import android.app.Activity
import co.bytemark.sdk.model.menu.MenuItem

/**
 * Interface to handle to/fro communication from adapter to activity without any memory leaks.
 */
interface MenuClickManager {
    /**
     * To retrieve current Activity the adapter is serving.
     *
     * @return Attached activity
     */
    fun getAttachedActivity(): Activity?

    /**
     * Adapter will call this when an click action that requires network connectivity is clicked
     * when being offline.
     *
     * @return True if you want to consume the event
     */
    fun onWebViewItemRequiresConnection(): Boolean

    /**
     * Called when user clicks sign out
     */
    fun onLogOutInitiated()

    /**
     * Called when user clicked sign in button.
     */
    fun onLogInInitiated()

    /**
     * True if the intent we are going to launch should have a different task stack
     *
     * @return
     */
    fun shouldUseTaskStack(): Boolean

    /**
     * Method to allow overriding of inbuilt click handling
     *
     * @param item Menu Item that was clicked
     * @return True if you consumed the event
     */
    fun onMenuItemClicked(item: MenuItem): Boolean

    fun onActionSheetClicked(actionSheetType: String?)
}