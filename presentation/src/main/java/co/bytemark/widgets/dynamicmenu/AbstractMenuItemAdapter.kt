package co.bytemark.widgets.dynamicmenu

import android.app.Activity
import android.app.TaskStackBuilder
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Parcelable
import android.text.TextUtils
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import co.bytemark.CustomerMobileApp.Companion.getConf
import co.bytemark.MainActivity
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.base.MasterActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.ConfHelper
import co.bytemark.helpers.EmailIntentBuilder
import co.bytemark.menu.MenuActivity
import co.bytemark.sdk.Api.ApiUtility
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.BytemarkSDK.SDKUtility
import co.bytemark.sdk.model.common.Action
import co.bytemark.sdk.model.menu.MenuItem
import co.bytemark.shared.ActionUtil
import co.bytemark.webview.WebViewActivity
import co.bytemark.widgets.dynamicmenu.AbstractMenuItemAdapter.AbstractMenuItemHolder
import co.bytemark.widgets.util.Util
import timber.log.Timber

/**
 * Adapter to render the menu items.
 * Currently the adapter is immutable. You can't modify the list once you set it.
 */

abstract class AbstractMenuItemAdapter<T : AbstractMenuItemHolder?>(
    var confHelper: ConfHelper, context: Context, menuItems: List<MenuItem>,
    menuClickManager: MenuClickManager, analyticsPlatformAdapter: AnalyticsPlatformAdapter?
) : RecyclerView.Adapter<T>() {

    private val context: Context = context.applicationContext
    private val menuItems: MutableList<MenuItem> = ArrayList()
    private val menuClickManager: MenuClickManager?
    private val analyticsPlatformAdapter: AnalyticsPlatformAdapter?
    private val nativeLogin: Boolean

    private fun setMenuItems(menuItems: List<MenuItem>?) {
        this.menuItems.clear()
        if (menuItems != null) {
            this.menuItems.addAll(menuItems)
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: T, position: Int) {
        holder?.itemView?.setOnClickListener {
            val currentPosition = holder.adapterPosition
            if (currentPosition != RecyclerView.NO_POSITION) {
                val item = menuItems[currentPosition]
                val action = item.action
                if (isValid(action)) return@setOnClickListener
                if (menuClickManager?.onMenuItemClicked(item) == true) return@setOnClickListener
                if (handledSignItems(item)) return@setOnClickListener
                if (item.action != null) {
                    logTheEventForMenuClick(item)
                }
                when {
                    ActionUtil.isActivity(action!!, nativeLogin) -> {
                        launchIntent(
                            activity,
                            ActionUtil.getActivityIntent(activity!!, item, nativeLogin)
                        )
                    }
                    ActionUtil.isWebView(action!!, nativeLogin) -> {
                        handleWebView(activity, item)
                    }
                    action?.isEmail() -> action.email?.let { it1 -> composeEmail(it1) }
                    action?.isPhone() -> action.phone?.let { it1 -> dialPhoneNumber(it1) }
                    action?.isMenuGroup -> {
                        val intent = Intent(activity, MenuActivity::class.java)
                        intent.putExtra(EXTRA_KEY_ACTION, action)
                        intent.putExtra(AppConstants.TITLE, menuItems[position].title)
                        launchIntent(activity, intent)
                    }
                    action?.isDeepLink -> action.androidStore?.let { it1 ->
                        openApp(context,
                            it1
                        )
                    }
                    action?.isStaticResource -> handleStaticResource(activity, item)

                    action?.isActionSheet() -> {
                        action.actionSheet?.let { menuClickManager?.onActionSheetClicked(action.actionSheet) }
                    }
                }
            }
        }
        onBindViewHolder(holder, position, menuItems[position])
    }

    private fun openApp(context: Context?, appPackageName: String) {
        if (context == null) {
            return
        }
        val pm = context.packageManager
        val intent = pm.getLaunchIntentForPackage(appPackageName)
        if (intent != null) {
            context.startActivity(intent)
        } else {
            try {
                val i = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName"))
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(i)
            } catch (anfe: ActivityNotFoundException) {
                Timber.e(anfe.message)
            }
        }
    }

    abstract fun onBindViewHolder(holder: T, position: Int, menuItem: MenuItem)

    override fun getItemCount(): Int = menuItems.size

    open fun handleWebView(activity: Activity?, item: MenuItem) {
        if (!ApiUtility.internetIsAvailable(context) && menuClickManager?.onWebViewItemRequiresConnection() == true) {
            return
        }
        val baseUrl = SDKUtility.getBaseAccountUrl()
        val intent = ActionUtil.getWebViewIntent(activity!!, item, baseUrl, confHelper.isNativeLogin)

        // Specific customizations
        if (intent != null) {
            if (MenuItem.TRIP_PLANNER.equals(item.title, ignoreCase = true)
                || MenuItem.TIMETABLES.equals(item.title, ignoreCase = true)
                || MenuItem.SCHEDULE.equals(item.title, ignoreCase = true)
                || MenuItem.DART_SITE.equals(item.title, ignoreCase = true)
            ) {
                intent.putExtra(WebViewActivity.EXTRA_HAMBURGER, true)
            }
        }
        launchIntent(activity, intent)
    }

    private fun handleStaticResource(activity: Activity?, item: MenuItem) {
        val intent = confHelper.supportedLocaleLanguageTag?.let {
            ActionUtil.getStaticResourcesIntent(activity!!, item,
                it
            )
        }
        launchIntent(activity, intent)
    }

    private fun handledSignItems(item: MenuItem): Boolean {
        if (activity == null) {
            return false
        }
        if (item.action?.type.equals(Action.AUTHENTICATION, ignoreCase = true)) {
            when {
                BytemarkSDK.isLoggedIn() -> {
                    Timber.d("Sign out clicked")
                    menuClickManager?.onLogOutInitiated()
                }
                confHelper.isNativeLogin -> {
                    Timber.d("Sign in clicked")
                    val action = Action()
                    action.screen = "native_authentication"
                    launchIntent(activity, ActionUtil.getActivityIntent(activity!!, item, confHelper.isNativeLogin))
                    menuClickManager?.onLogInInitiated()
                }
                ApiUtility.internetIsAvailable(context) -> {
                    activity?.startActivityForResult(Intent(activity, MainActivity::class.java), MasterActivity.REQUEST_CODE_LOGIN)
                    menuClickManager?.onLogInInitiated()
                }
                else -> {
                    menuClickManager?.onWebViewItemRequiresConnection()
                }
            }
            return true
        }
        return false
    }

    fun launchIntent(activity: Activity?, intent: Intent?) {
        if (intent != null && activity != null) {
            if (getConf()?.clients?.customerMobile?.isHaconSdkEnabled != null
                && getConf()?.clients?.customerMobile?.isHaconSdkEnabled == true
            ) {
                if (isIntentTitleNotNull(intent) && isOneOfHaconScreens(intent)) {
                    setupIntentAndStartActivityForHaCon(intent, activity)
                } else {
                    startActivity(intent, activity)
                }
            } else {
                startActivity(intent, activity)
            }
        }
    }

    private fun isIntentTitleNotNull(intent: Intent): Boolean =
        intent.extras?.getString("title") != null

    private fun isOneOfHaconScreens(intent: Intent): Boolean {
        return (intent.extras?.getString("title").equals(AppConstants.HACON_TRIP, ignoreCase = true)
                || intent.extras?.getString("title").equals(AppConstants.HACON_SCHEDULE, ignoreCase = true)
                || intent.extras?.getString("title").equals(AppConstants.HACON_ALARMS, ignoreCase = true)
                || intent.extras?.getString("title").equals(AppConstants.HACON_TRIP_PLANNER, ignoreCase = true)
                || intent.extras?.getString("title").equals(AppConstants.HACON_NEXT_DEPARTURE, ignoreCase = true)
                || intent.extras?.getString("title").equals(AppConstants.HACON_SYSTEM_MAP, ignoreCase = true)
                || intent.extras?.getString("title").equals(AppConstants.HACON_SYSTEM_MAP_PDF, ignoreCase = true)
                || intent.extras?.getString("title").equals(AppConstants.HACON_NEXT_BUS, ignoreCase = true)
                || intent.extras?.getString("title").equals(AppConstants.HACON_NEXT_DART_BUS, ignoreCase = true))
    }

    private fun setupIntentAndStartActivityForHaCon(intent: Intent, activity: Activity?) {
        try {
            val cls = Class.forName("co.bytemark.hacon.HaconActivity")
            intent.setClass(activity!!, cls)
            val menuClass = Class.forName("co.bytemark.hacon.MenuProvider")
            val methodName = "getInstance"
            // getMethod takes the method name we want to invoke and what type of argument that method name accepts
            // for us method name is "initialize" and argument type is "Context"
            val type = arrayOf(
                Int::class.javaObjectType, ConfHelper::class.java, MenuClickManager::class.java,
                AnalyticsPlatformAdapter::class.java
            )

            val method = menuClass.getMethod(methodName, *type)
            // invoke accepts object name on which this method needs to be invoked and context as parameter.
            // we are passing null because we are invoking static method, object name doesen't matter
            intent.putExtra(
                "de.hafas.app.menu.drawer",
                menuClass.cast(
                    method.invoke(
                        null,
                        Util.navigationDrawerWidth(Util.getActionBarHeight(context)),
                        confHelper,
                        menuClickManager,
                        analyticsPlatformAdapter
                    )
                ) as Parcelable?
            )
            if (null != intent.extras?.getString("title")) {
                var isHaconScreen = true
                when (intent.extras?.getString("title")) {
                    AppConstants.HACON_TRIP -> intent.action = AppConstants.ACTION_CONNECTION
                    AppConstants.HACON_SCHEDULE, AppConstants.HACON_NEXT_BUS, AppConstants.HACON_NEXT_DART_BUS, AppConstants.HACON_NEXT_DEPARTURE -> intent.action =
                        AppConstants.ACTION_STATIONBOARD
                    AppConstants.HACON_TRIP_PLANNER -> intent.action = AppConstants.ACTION_CONNECTION
                    AppConstants.HACON_ALARMS -> intent.action = AppConstants.ACTION_ALARM
                    AppConstants.HACON_SYSTEM_MAP_PDF -> {
                        intent.action = AppConstants.ACTION_SHOW_NETWORKMAPS
                        intent.putExtra("title", AppConstants.HACON_SYSTEM_MAP_PDF)
                    }
                    AppConstants.HACON_SYSTEM_MAP -> {
                        intent.action = AppConstants.ACTION_SHOW_MOBILITYMAP
                        intent.putExtra("title", AppConstants.HACON_SYSTEM_MAP)
                    }
                    else -> {
                        isHaconScreen = false
                        startActivity(intent, activity)
                    }
                }
                if (isHaconScreen) {
                    var title = confHelper.getNavigationMenuScreenTitleFromTheConfig(intent.extras?.getString(AppConstants.ACTION))
                    if (title == "") {
                        title = confHelper.getSettingsMenuScreenTitleFromTheConfig(
                            BytemarkSDK.isLoggedIn(),
                            true,
                            intent.extras?.getString(AppConstants.ACTION)
                        )
                    }
                    startHaconActivity(intent, title)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun startHaconActivity(intent: Intent, title: String) {
        // Clear the task stack when going to HaCon screens because we are adding
        // navigation drawer in HaCon screens using which user can navigate to other screen
        // and if he press back after navigating to other screen user should not
        // see the previous activity. App should just close in this case.
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra("title", title)
        activity?.startActivityForResult(intent, AppConstants.HAFAS_ACTIVITY_REQUEST_CODE)
    }

    private fun startActivity(intent: Intent, activity: Activity?) {
        if (menuClickManager?.shouldUseTaskStack() == true) {
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
            val builder = TaskStackBuilder.create(activity)
            builder.addNextIntentWithParentStack(intent)
            builder.startActivities()
            activity?.finish()
        } else {
            activity?.startActivity(intent)
        }
    }

    private fun composeEmail(address: String) {
        if (TextUtils.isEmpty(address)) {
            return
        }
        EmailIntentBuilder(context).to(address).build().start()
    }

    private fun dialPhoneNumber(phoneNumber: String) {
        if (TextUtils.isEmpty(phoneNumber)) {
            return
        }
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phoneNumber")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        if (intent.resolveActivity(context.packageManager) != null) {
            context.startActivity(intent)
        }
    }

    protected val activity: Activity?
        get() = menuClickManager?.getAttachedActivity()

    private fun isValid(action: Action?): Boolean {
        return action == null || activity == null
    }

    abstract class AbstractMenuItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            ButterKnife.bind(this, itemView)
        }
    }

    private fun logTheEventForMenuClick(menuItem: MenuItem) {
        try {
            analyticsPlatformAdapter?.let {
                if (menuItem.action != null && menuItem.action?.screen != null) {
                    when (val screen = menuItem.title) {
                        AppConstants.HACON_SCHEDULE, AppConstants.HACON_NEXT_DEPARTURE -> analyticsPlatformAdapter.haconDepartureMenuSelected()
                        Action.HACON_ALARM, AppConstants.HACON_ALARMS -> analyticsPlatformAdapter.haconAlarmsMenuSelected()
                        Action.HACON_NYCF_TRIP_PLANNER, AppConstants.HACON_TRIP_PLANNER -> analyticsPlatformAdapter.haconTripPlannerMenuSelected()
                        else -> analyticsPlatformAdapter.moreInfoOptionSelected(screen)
                    }
                } else if (menuItem.title != null) {
                    analyticsPlatformAdapter.moreInfoOptionSelected(menuItem.title)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        const val EXTRA_KEY_ACTION = "EXTRA_KEY_ACTION"
    }

    init {
        this.menuClickManager = menuClickManager
        this.analyticsPlatformAdapter = analyticsPlatformAdapter
        nativeLogin = confHelper.isNativeLogin
        setMenuItems(menuItems)
    }
}