package co.bytemark.widgets.dynamicmenu

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import co.bytemark.R
import co.bytemark.sdk.model.menu.MenuGroup
import co.bytemark.sdk.model.menu.MenuItem
import co.bytemark.widgets.LinearDividerRecyclerView
import co.bytemark.widgets.dynamicmenu.AbstractMenuGroupAdapter.AbstractMenuGroupHolder
import co.bytemark.widgets.dynamicmenu.AbstractMenuItemAdapter.AbstractMenuItemHolder
import java.util.*

abstract class AbstractMenuGroupAdapter<T : AbstractMenuGroupHolder?, Y : AbstractMenuItemHolder?>
constructor(
    context: Context,
    private val menuClickManager: MenuClickManager
) : RecyclerView.Adapter<T>() {

    private val context: Context = context.applicationContext

    private val menuGroupList: MutableList<MenuGroup> = ArrayList()

    fun setMenuGroups(menuGroupList: List<MenuGroup>) {
        this.menuGroupList.clear()
        this.menuGroupList.addAll(menuGroupList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return menuGroupList.size
    }

    protected val activity: Activity?
        get() = menuClickManager.getAttachedActivity()

    override fun onBindViewHolder(holder: T, position: Int) {
        val item = menuGroupList[position]
        holder?.menuItemList?.let {
            it.adapter = createMenuItemAdapter(context, menuClickManager, item.menuItems)
        }
        onBindViewHolder(holder, position, item)
    }

    protected abstract fun onBindViewHolder(holder: T, position: Int, menuGroup: MenuGroup)

    protected abstract fun createMenuItemAdapter(
        context: Context,
        menuClickManager: MenuClickManager,
        menuItems: MutableList<MenuItem>
    ): AbstractMenuItemAdapter<Y>

    abstract class AbstractMenuGroupHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        /**
         * Optional menu list item. This is the only common thing in all menu group items. Will be
         * populated if its specified in layout.
         *
         * NOTE: The id should be the same as below
         */
        @JvmField
        @BindView(R.id.dynamicMenuItemRecyclerView)
        var menuItemList: LinearDividerRecyclerView? = null

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}