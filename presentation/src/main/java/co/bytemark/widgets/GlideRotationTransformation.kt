package co.bytemark.widgets

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation


class GlideRotationTransformation(
    context: Context?,
    rotationAngle: Float
) : BitmapTransformation(context) {
    private var rotationAngle = 0f
    override fun transform(
        pool: BitmapPool,
        toTransform: Bitmap,
        outWidth: Int,
        outHeight: Int
    ): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(rotationAngle)
        return Bitmap.createBitmap(
            toTransform,
            0,
            0,
            toTransform.width,
            toTransform.height,
            matrix,
            true
        )
    }

    override fun getId(): String {
        return "rotate$rotationAngle"
    }

    init {
        this.rotationAngle = rotationAngle
    }
}