package co.bytemark.settings.menu

import android.content.Context
import android.content.res.ColorStateList
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.model.menu.MenuGroup
import co.bytemark.sdk.model.menu.MenuItem
import co.bytemark.settings.menu.SettingsMenuAdapter.SettingMenuGroupHolder
import co.bytemark.settings.menu.SettingsMenuAdapter.SettingsMenuItemHolder
import co.bytemark.widgets.dynamicmenu.AbstractMenuGroupAdapter
import co.bytemark.widgets.dynamicmenu.AbstractMenuItemAdapter
import co.bytemark.widgets.dynamicmenu.AbstractMenuItemAdapter.AbstractMenuItemHolder
import co.bytemark.widgets.dynamicmenu.MenuClickManager
import co.bytemark.widgets.dynamicmenu.MenuItemDrawableLoader
import co.bytemark.widgets.util.hide
import co.bytemark.widgets.util.show
import kotlinx.android.synthetic.main.settings_menu_group_item_template.view.*
import kotlinx.android.synthetic.main.settings_menu_item_template.view.*
import javax.inject.Inject

class SettingsMenuAdapter(context: Context, menuClickManager: MenuClickManager) :
    AbstractMenuGroupAdapter<SettingMenuGroupHolder, SettingsMenuItemHolder>(context, menuClickManager) {

    @Inject
    lateinit var confHelper: ConfHelper

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    override fun onBindViewHolder(holder: SettingMenuGroupHolder, position: Int, menuGroup: MenuGroup) {
        with(holder.itemView) {
            menuGroupHeader.text = menuGroup.header
            this.contentDescription = menuGroup.header
        }
    }

    public override fun createMenuItemAdapter(
        context: Context,
        menuClickManager: MenuClickManager,
        menuItems: MutableList<MenuItem>
    ): AbstractMenuItemAdapter<SettingsMenuItemHolder> {

        return object : AbstractMenuItemAdapter<SettingsMenuItemHolder>(confHelper, context, menuItems, menuClickManager, analyticsPlatformAdapter) {

            override fun onBindViewHolder(holder: SettingsMenuItemHolder, position: Int, menuItem: MenuItem) {
                with(holder.itemView) {
                    this.contentDescription = menuItem.title
                    if (!TextUtils.isEmpty(menuItem.subtitle)) {
                        settingsMenuItemSingleTitle.visibility = View.GONE
                        settingsMenuItemDoubleTitle.show()
                        settingsMenuItemDoubleSubTitle.show()
                        settingsMenuItemDoubleTitle.text = menuItem.title
                        settingsMenuItemDoubleSubTitle.text = menuItem.subtitle
                    } else {
                        settingsMenuItemDoubleTitle.hide()
                        settingsMenuItemDoubleSubTitle.hide()
                        settingsMenuItemSingleTitle.visibility = View.VISIBLE
                        settingsMenuItemSingleTitle.text = menuItem.title
                    }
                    if (!TextUtils.isEmpty(menuItem.selectedImage)) {
                        try {
                            settingsMenuItemIcon.setImageResource(MenuItemDrawableLoader.getDrawableIdRes(menuItem.selectedImage))
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        // avoid setting tint on colored icons
                        if (!menuItem.selectedImage.contains("color")) {
                            settingsMenuItemIcon.imageTintList = ColorStateList.valueOf(confHelper.dataThemePrimaryTextColor)
                        }
                    } else {
                        settingsMenuItemIcon.visibility = View.GONE
                    }
                }
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingsMenuItemHolder =
                SettingsMenuItemHolder(LayoutInflater.from(parent.context).inflate(R.layout.settings_menu_item_template, parent, false))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingMenuGroupHolder =
        SettingMenuGroupHolder(LayoutInflater.from(parent.context).inflate(R.layout.settings_menu_group_item_template, parent, false))

    class SettingMenuGroupHolder(itemView: View) : AbstractMenuGroupHolder(itemView)

    class SettingsMenuItemHolder(itemView: View) : AbstractMenuItemHolder(itemView)

    init {
        component.inject(this)
    }
}