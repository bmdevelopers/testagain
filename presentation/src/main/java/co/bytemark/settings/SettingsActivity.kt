package co.bytemark.settings

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.model.common.Action

class SettingsActivity : MasterActivity() {

    override fun getLayoutRes(): Int = R.layout.activity_settings

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setNavigationViewCheckedItem(Action.SETTINGS)
        setToolbarTitle(confHelper.getNavigationMenuScreenTitleFromTheConfig(Action.SETTINGS))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AppConstants.ELERTS_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val errorMessage =
                    data.extras?.getString("elert_error_message", null)
                val errorCode = data.extras?.getInt("elert_error_code")
                var fragment = supportFragmentManager.findFragmentById(R.id.fragment) as SettingsFragment
                fragment.showElertsDialog(errorMessage == null, errorMessage, errorCode)
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }
}