package co.bytemark.settings

import androidx.lifecycle.MutableLiveData
import co.bytemark.base.BaseViewModel
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.BMError
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.model.menu.MenuGroup
import co.bytemark.sdk.network_impl.BMNetwork
import co.bytemark.sdk.network_impl.BaseNetworkRequestCallback
import co.bytemark.securityquestion.SecurityQuestionCheckListener
import co.bytemark.securityquestion.UserSecurityQuestionChecker
import java.util.*
import javax.inject.Inject

class SettingsViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var configHelper: ConfHelper

    @Inject
    lateinit var userSecurityQuestionChecker: UserSecurityQuestionChecker

    @Inject
    lateinit var bmNetwork: BMNetwork

    private var isSecurityQuestionsDisplayed = false

    val settingListLiveData by lazy { MutableLiveData<List<MenuGroup>>() }
    val enforceSecurityQuestionsLiveData by lazy { MutableLiveData<Unit>() }

    fun checkUserInfo() {
        bmNetwork.getUser(object : BaseNetworkRequestCallback {
            override fun onNetworkRequestSuccessfullyCompleted() {}
            override fun <T> onNetworkRequestSuccessWithResponse(listOf: ArrayList<T>) {}
            override fun onNetworkRequestSuccessWithResponse(`object`: Any) {}
            override fun onNetworkRequestSuccessWithUnexpectedError() {}
            override fun onNetworkRequestSuccessWithError(errors: BMError) {
                errorLiveData.value = co.bytemark.domain.model.common.BMError(errors.code, errors.message)
            }

            override fun onNetworkRequestSuccessWithDeviceTimeError() {
                errorLiveData.value = co.bytemark.domain.model.common.BMError(BytemarkSDK.ResponseCode.DEVICE_LOST_OR_STOLEN, null)
            }
        })
    }

    fun cleanUp() {
        bmNetwork.cancelGetUser()
    }

    fun checkIfSecurityQuestionsAreAnswered() {
        userSecurityQuestionChecker.checkIfEmpty(object : SecurityQuestionCheckListener {
            override fun onEmpty() {
                if (!isSecurityQuestionsDisplayed) {
                    isSecurityQuestionsDisplayed = true
                    enforceSecurityQuestionsLiveData.value = Unit
                }
            }
        })
    }

    fun loadMenuGroups() {
        val settingList = configHelper.supportedSettingsList
        if (BytemarkSDK.isLoggedIn()) {
            settingListLiveData.value = settingList.signedInMenuGroups
        } else {
            settingListLiveData.value = settingList.signedOutMenuGroups
        }
    }
}