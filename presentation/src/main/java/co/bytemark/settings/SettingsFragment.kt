package co.bytemark.settings

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import co.bytemark.BuildConfig
import co.bytemark.CustomerMobileApp
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.elerts.ElertsUIComponent
import co.bytemark.sdk.Api.ApiUtility
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.model.common.Action
import co.bytemark.sdk.model.config.Domain
import co.bytemark.sdk.model.menu.MenuItem
import co.bytemark.sdk.session.ACTION_LOGOUT_API_CALL_COMPLETED
import co.bytemark.securityquestion.SecurityQuestionActivity
import co.bytemark.settings.menu.SettingsMenuAdapter
import co.bytemark.widgets.dynamicmenu.MenuClickManager
import co.bytemark.widgets.util.createViewModel
import co.bytemark.widgets.util.getViewModel
import co.bytemark.widgets.util.showMaterialDialog
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.appliedfilter_row_item.*
import kotlinx.android.synthetic.main.fragment_settings_dynamic.*
import java.util.*
import javax.inject.Inject

class SettingsFragment : BaseMvvmFragment(), MenuClickManager {

    private lateinit var viewModel: SettingsViewModel

    private var elertsBottomSheetDialog: BottomSheetDialog? = null

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    private var start: Calendar? = null

    private lateinit var settingMenuGroupAdapter: SettingsMenuAdapter

    private lateinit var logoutProgressIndicator: MaterialDialog

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_settings_dynamic, container, false)

    override fun onInject() {
        component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = createViewModel { CustomerMobileApp.appComponent.settingsViewModel }
        observeLiveData()
        analyticsPlatformAdapter.settingsScreenDisplayed()
        initMenuList()
        val domain: Domain? = confHelper.domain
        if (domain?.region != null && domain.region?.contentEquals("EU") == true) {
            brandingNameSettingsMenu.setText(R.string.settings_bytetoken)
        } else {
            brandingNameSettingsMenu.setText(R.string.settings_bytemark)
        }
        appNameSettingsMenu.text =
            String.format(getString(R.string.settings_mobile_app), getString(R.string.app_name))
        appVersionSettingsMenu.text = String.format("V %s", BuildConfig.VERSION_NAME)
        appVersionSettingsMenu.contentDescription =
            String.format(getString(R.string.settings_app_version_is), BuildConfig.VERSION_NAME)
        if (confHelper.isElertSdkEnabled()) {
            setupElertBottomSheet()
        }
    }

    fun getViewModel(): SettingsViewModel {
        return getViewModel(CustomerMobileApp.appComponent.settingsViewModel)
    }

    private fun observeLiveData() {
        viewModel.settingListLiveData.observe(this, androidx.lifecycle.Observer {
            settingMenuGroupAdapter.setMenuGroups(it)
        })
        viewModel.errorLiveData.observe(this, androidx.lifecycle.Observer {
            it?.let {
                it1 ->
                if (it.code == BytemarkSDK.ResponseCode.DEVICE_LOST_OR_STOLEN)
                    viewModel.loadMenuGroups()
                handleError(it1)
            }
        })
        viewModel.enforceSecurityQuestionsLiveData.observe(this, androidx.lifecycle.Observer {
            startActivity(Intent(activity, SecurityQuestionActivity::class.java))
        })
    }

    override fun onResume() {
        super.onResume()
        if (BytemarkSDK.isLoggedIn() && ApiUtility.internetIsAvailable(activity)) {
            viewModel.checkUserInfo()
            viewModel.checkIfSecurityQuestionsAreAnswered()
        }
        viewModel.loadMenuGroups()
    }

    override fun onDestroy() {
        viewModel.cleanUp()
        super.onDestroy()
    }

    private fun initMenuList() {
        context?.let {
            settingMenuGroupAdapter = SettingsMenuAdapter(it, this)
            settingsMenuGroupRecyclerView.adapter = settingMenuGroupAdapter
        }
    }

    override fun getAttachedActivity(): Activity? = activity

    override fun shouldUseTaskStack(): Boolean = false

    override fun onWebViewItemRequiresConnection(): Boolean {
        connectionErrorDialog()
        return true
    }

    override fun onLogOutInitiated() {
        initProgressIndicator()
        context?.let {
            it.showMaterialDialog(
                title = R.string.settings_popup_logout_confirmation_title,
                body = R.string.settings_popup_logout_confirmation_body,
                positiveButton = R.string.settings_popup_signout,
                negativeButton = R.string.receipt_popup_cancel,
                positiveAction = { dialog, _ ->
                    dialog.dismiss()
                    registerReceiver()
                    logoutProgressIndicator.show()
                    val user = BytemarkSDK.SDKUtility.getUserInfo()
                    BytemarkSDK.logout(activity?.applicationContext)
                    if (user != null) {
                        analyticsPlatformAdapter.signOut(1)
                    }
                },
                negativeAction = { materialDialog, _ -> materialDialog.dismiss() }
            )
        }
    }

    private fun registerReceiver() {
        context?.let {
            LocalBroadcastManager.getInstance(it).registerReceiver(object : BroadcastReceiver() {
                override fun onReceive(context: Context?, intent: Intent?) {
                    logoutProgressIndicator?.dismiss()
                    viewModel.loadMenuGroups()
                }
            }, IntentFilter(ACTION_LOGOUT_API_CALL_COMPLETED))
        }
    }

    private fun initProgressIndicator() {
        context?.let {
            logoutProgressIndicator = MaterialDialog.Builder(it)
                    .content(it.getString(R.string.loading))
                    .progress(true, 0)
                    .cancelable(false)
                    .build()
        }
    }

    override fun onLogInInitiated() {
        start = Calendar.getInstance()
    }

    override fun onMenuItemClicked(item: MenuItem): Boolean {
        if (!isOnline() && item.action?.screen != null
            && !item.action?.screen.equals(Action.ACKNOWLEDGMENTS, ignoreCase = true)
            && !item.action?.screen.equals(Action.ACCOUNT_PHOTO, ignoreCase = true)
            && !item.action?.screen.equals(Action.NOTIFICATION, ignoreCase = true)
        ) {
            connectionErrorDialog()
            return true
        }
        return false
    }

    override fun onActionSheetClicked(actionSheetType: String?) {
        if (actionSheetType?.equals(Action.ELERTS_ACTION_SHEET, ignoreCase = true) == true) {
            showElertsBottomSheet()
        }
    }

    private fun showElertsBottomSheet() {
        activity?.let {
            elertsBottomSheetDialog?.show()
        }
    }

    private fun setupElertBottomSheet() {
        activity?.let {
            val elertsUIComponent = ElertsUIComponent()
            elertsBottomSheetDialog = BottomSheetDialog(it)
            elertsUIComponent.setupElertsBottomSheet(
                elertsBottomSheetDialog!!,
                it,
                this
            )
        }
    }

    fun showElertsDialog(
        isSuccess: Boolean,
        errorMessage: String?,
        errorCode: Int?
    ) {
        MaterialDialog.Builder(activity!!)
            .title(if (isSuccess) R.string.report_success_pop_up_title else R.string.report_failure_pop_up_title)
            .content(
                (if (isSuccess) getString(R.string.elert_report_success) else if (errorCode == 0) getString(
                    R.string.report_connection_error_message
                ) else errorMessage)!!
            )
            .positiveText(R.string.ok)
            .positiveColor(confHelper.dataThemeAccentColor)
            .onPositive { dialog: MaterialDialog, which: DialogAction? ->
                dialog.dismiss()
            }
            .show()
    }
}