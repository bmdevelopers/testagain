package co.bytemark.incomm.retailer_details

import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.app.TaskStackBuilder
import androidx.core.view.doOnLayout
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.incomm.Incomm
import co.bytemark.domain.model.incomm.IncommBarcodeDetail
import co.bytemark.helpers.AppConstants
import co.bytemark.manage.ManageCardsActivity
import co.bytemark.payment_methods.PaymentMethodsActivity
import co.bytemark.use_tickets.UseTicketsActivity
import co.bytemark.widgets.util.Util
import co.bytemark.widgets.util.show
import co.bytemark.widgets.util.withArgs
import com.bumptech.glide.Glide
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.google.zxing.oned.Code128Writer
import kotlinx.android.synthetic.main.fragment_retailer_detail.*

class RetailerDetailsFragment : BaseMvvmFragment() {

    companion object {
        fun newInstance(
            incomm: Incomm?,
            incommBarcodeDetail: IncommBarcodeDetail?,
            amount: Int,
            isIntentForWalletLoad: Boolean,
            isIntentFromHistoryWalletLoad: Boolean,
            orderUuid: String?,
            isIntentForLoadFareMedium: Boolean = false
        ) =
            RetailerDetailsFragment().withArgs {
                putParcelable(AppConstants.INTENT_INCOMM_STORE, incomm)
                putInt(AppConstants.INTENT_INCOMM_ORDER_AMOUNT, amount)
                putParcelable(AppConstants.INTENT_INCOMM_BARCODE_DETAIL, incommBarcodeDetail)
                putBoolean(AppConstants.INTENT_INCOMM_WALLET_LOAD, isIntentForWalletLoad)
                putBoolean(
                    AppConstants.INTENT_INCOMM_HISTORY_WALLET_LOAD,
                    isIntentFromHistoryWalletLoad
                )
                putBoolean(AppConstants.INTENT_INCOMM_FAREMEDIUM_LOAD, isIntentForLoadFareMedium)
                putString(AppConstants.INTENT_INCOMM_ORDER_UUID, orderUuid)
            }
    }

    override fun onInject() {
        CustomerMobileApp.component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_retailer_detail, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val incomm = arguments?.getParcelable<Incomm>(AppConstants.INTENT_INCOMM_STORE)
        val barcodeDetail =
            arguments?.getParcelable<IncommBarcodeDetail>(AppConstants.INTENT_INCOMM_BARCODE_DETAIL)
        val amount = arguments?.getInt(AppConstants.INTENT_INCOMM_ORDER_AMOUNT)
        val isIntentForWalletLoad = arguments?.getBoolean(AppConstants.INTENT_INCOMM_WALLET_LOAD)
        val isIntentFromHistoryWalletLoad =
            arguments?.getBoolean(AppConstants.INTENT_INCOMM_HISTORY_WALLET_LOAD)
        val isIntentForFaremediumLoad =
            arguments?.getBoolean(AppConstants.INTENT_INCOMM_FAREMEDIUM_LOAD)

        generateBarcode(barcodeDetail)

        if (isIntentForWalletLoad == true || isIntentFromHistoryWalletLoad == true) {
            retailerDetailTicketTextView.text = getString(
                R.string
                    .incomm_message_after_wallet_transaction_complete
            )
            if (isIntentForFaremediumLoad == false) {
                goToPaymentButton.show()
            } else {
                goToManageScreenButton.show()
            }
        } else {
            retailerDetailTicketTextView.text =
                getString(R.string.incomm_message_after_ticket_transaction_complete)
            showGoToButton(goToTicketButton)
        }
        paymentToOrgTextView.text = barcodeDetail?.orgName
        retailerDetailAddressTextView.text = incomm?.address?.formattedAddress
        retailerDetailsSubtitleTextView.text =
            String.format(getString(R.string.incomm_scan_barcode_at), incomm?.merchant?.name)
        paymentAmountTextView.text =
            amount?.let { confHelper.getConfigurationPurchaseOptionsCurrencySymbol(it) }

        enlargeBarcodeTextView.setOnClickListener {
            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }

        cancelFullScreenButton.setOnClickListener {
            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }

        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            (activity as RetailerDetailsActivity).let {
                it.supportActionBar?.hide()
            }
        } else {
            (activity as RetailerDetailsActivity).let {
                it.supportActionBar?.show()
            }
        }

        goToPaymentButton.setOnClickListener {
            if (isIntentForWalletLoad == true) {
                activity?.setResult(AppConstants.INCOMM_WALLET_LOAD_REQUEST_CODE, Intent())
                activity?.finish()
            } else if (isIntentFromHistoryWalletLoad == true) {
                context?.let {
                    val builder = TaskStackBuilder.create(it)
                    builder.addNextIntentWithParentStack(
                        Intent(
                            it,
                            PaymentMethodsActivity::class.java
                        )
                    )
                    builder.startActivities()
                }
            }
        }

        goToTicketButton.setOnClickListener {
            context?.let {
                val builder = TaskStackBuilder.create(it)
                val intent = Intent(it, UseTicketsActivity::class.java)
                intent.putExtra(AppConstants.INTENT_INCOMM_USE_TICKET, true)
                builder.addNextIntentWithParentStack(intent)
                builder.startActivities()
            }
        }

        goToManageScreenButton.setOnClickListener {
            context?.let {
                val builder = TaskStackBuilder.create(it)
                val intent = Intent(it, ManageCardsActivity::class.java)
                //intent.putExtra(AppConstants.INTENT_INCOMM_USE_TICKET, true)
                builder.addNextIntentWithParentStack(intent)
                builder.startActivities()
            }
        }
    }

    private fun showGoToButton(goToButton: Button) {
        if (confHelper.isFaremediaApp()) {
            goToManageScreenButton.show()
        } else {
            goToButton.show()
        }
    }

    private fun generateBarcode(incommBarcodeDetail: IncommBarcodeDetail?) {
        barcodeImageView.doOnLayout {
            try {
                val width = it.width
                val height = it.height
                val bitMatrix: BitMatrix = Code128Writer().encode(
                    incommBarcodeDetail?.accountNumber, BarcodeFormat.CODE_128,
                    width, height
                )
                val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                for (i in 0 until width) {
                    for (j in 0 until height) {
                        bitmap.setPixel(i, j, if (bitMatrix.get(i, j)) Color.BLACK else Color.WHITE)
                    }
                }
                Glide.with(context).load(incommBarcodeDetail?.iconUrl).into(barcodeIconImageView)
                barcodeNumberTextView.text = incommBarcodeDetail?.accountNumber
                barcodeImageView.setImageBitmap(bitmap)
            } catch (e: WriterException) {
                e.printStackTrace()
            }
        }
    }

    private fun setUpBrightness(brightness: Float) {
        if (isAdded && activity != null) {
            val layout = activity?.window?.attributes
            layout?.screenBrightness = brightness
            activity?.window?.attributes = layout
        }
    }

    override fun onResume() {
        super.onResume()
        setUpBrightness(1f)
    }

    override fun onPause() {
        super.onPause()
        setUpBrightness(Util.getSystemBrightness(activity))
    }

}