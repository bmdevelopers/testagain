package co.bytemark.incomm.retailer_details

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.ToolbarActivity
import co.bytemark.domain.model.incomm.Incomm
import co.bytemark.domain.model.incomm.IncommBarcodeDetail
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.util.replaceFragment

class RetailerDetailsActivity : ToolbarActivity() {

    override fun getLayoutRes() = R.layout.activity_retailer_details

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(getString(R.string.incomm_toolbar_title_store_details))
        val barcodeDetail =
            intent?.getParcelableExtra<IncommBarcodeDetail>(AppConstants.INTENT_INCOMM_BARCODE_DETAIL)
        val incomm = intent?.getParcelableExtra<Incomm>(AppConstants.INTENT_INCOMM_STORE)
        val amount = intent.getIntExtra(AppConstants.INTENT_INCOMM_ORDER_AMOUNT, 0)
        val orderUUid = intent.getStringExtra(AppConstants.INTENT_INCOMM_ORDER_UUID)
        val isIntentForWalletLoad = intent.getBooleanExtra(
            AppConstants
                .INTENT_INCOMM_WALLET_LOAD, false
        )
        val isIntentFromHistoryWalletLoad = intent.getBooleanExtra(
            AppConstants
                .INTENT_INCOMM_HISTORY_WALLET_LOAD, false
        )
        val isIntentForFaremediumLoad =
            intent.getBooleanExtra(AppConstants.INTENT_INCOMM_FAREMEDIUM_LOAD, false)

        replaceFragment(
            RetailerDetailsFragment.newInstance(
                incomm,
                barcodeDetail,
                amount,
                isIntentForWalletLoad,
                isIntentFromHistoryWalletLoad,
                orderUUid,
                isIntentForFaremediumLoad
            ),
            R.id.fragment
        )
    }

    override fun onBackPressed() {
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        } else {
            super.onBackPressed()
        }
    }
}