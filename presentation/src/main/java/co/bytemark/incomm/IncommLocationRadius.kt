package co.bytemark.incomm

data class IncommLocationRadius(val key :Double,val value:String){

    override fun toString(): String {
        return value
    }
}