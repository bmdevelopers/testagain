package co.bytemark.incomm

import android.Manifest
import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import android.widget.ArrayAdapter
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.doOnLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.TransitionManager
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.add_payment_card.Helper.TextWatcherHelper
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.incomm.IncommBarcodeDetail
import co.bytemark.helpers.AppConstants
import co.bytemark.incomm.retailer_details.RetailerDetailsActivity
import co.bytemark.widgets.util.Util.isTalkBackAccessibilityEnabled
import co.bytemark.widgets.util.createViewModel
import co.bytemark.widgets.util.hide
import co.bytemark.widgets.util.show
import co.bytemark.widgets.util.withArgs
import kotlinx.android.synthetic.main.fragment_select_retailer.*
import timber.log.Timber

class SelectRetailerFragment : BaseMvvmFragment() {

    companion object {
        private const val PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 100
        private const val REQUEST_CHECK_SETTINGS = 100
        fun newInstance(
            incommBarcodeDetail: IncommBarcodeDetail?, amount: Int,
            isIntentForWalletLoad: Boolean,
            isIntentFromHistoryWalletLoad: Boolean,
            orderUuid: String?,
            isIntentForFaremediumLoad: Boolean = false
        ) =
            SelectRetailerFragment().withArgs {
                putBoolean(AppConstants.INTENT_INCOMM_WALLET_LOAD, isIntentForWalletLoad)
                putParcelable(AppConstants.INTENT_INCOMM_BARCODE_DETAIL, incommBarcodeDetail)
                putInt(AppConstants.INTENT_INCOMM_ORDER_AMOUNT, amount)
                putBoolean(
                    AppConstants.INTENT_INCOMM_HISTORY_WALLET_LOAD,
                    isIntentFromHistoryWalletLoad
                )
                putString(AppConstants.INTENT_INCOMM_ORDER_UUID, orderUuid)
                putBoolean(AppConstants.INTENT_INCOMM_FAREMEDIUM_LOAD, isIntentForFaremediumLoad)
            }
    }

    override fun onInject() {
        CustomerMobileApp.component.inject(this)
    }

    private lateinit var viewModel: SelectRetailerViewModel

    private var paymentLocationsAdapter = IncommStoreAdapter()

    private var lastTypedAddress = ""

    private var orderUuid: String? = ""

    private var spinnerData = ArrayList<IncommLocationRadius>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_select_retailer, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = createViewModel { CustomerMobileApp.appComponent.selectRetailerViewModel }
        lifecycle.addObserver(viewModel)
        retainInstance = true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val barcodeDetail =
            arguments?.getParcelable<IncommBarcodeDetail>(AppConstants.INTENT_INCOMM_BARCODE_DETAIL)
        val amount = arguments?.getInt(AppConstants.INTENT_INCOMM_ORDER_AMOUNT)
        val isIntentForWalletLoad = arguments?.getBoolean(AppConstants.INTENT_INCOMM_WALLET_LOAD)
        val isIntentFromHistoryWalletLoad =
            arguments?.getBoolean(AppConstants.INTENT_INCOMM_HISTORY_WALLET_LOAD)
        orderUuid = arguments?.getString(AppConstants.INTENT_INCOMM_ORDER_UUID)
        val isIntentForFaremediumLoad =
            arguments?.getBoolean(AppConstants.INTENT_INCOMM_FAREMEDIUM_LOAD)

        observeLiveData()
        clearEditTextError()
        setRadiusSpinner()
        paymentRecyclerSetup(
            barcodeDetail,
            amount,
            isIntentForWalletLoad,
            isIntentFromHistoryWalletLoad,
            isIntentForFaremediumLoad
        )
        viewModel.getDefaultZipCode()

        searchRetailerButton.setOnClickListener {
            hideKeyboard()
            orderUuid?.let { it1 ->
                viewModel.getRetailerList(
                    (distanceSpinner?.selectedItem as IncommLocationRadius).key,
                    zipCodeTextInputLayout.editText?.text.toString().trim(),
                    it1
                )
            }
        }

        locationImageView.setOnClickListener {
            viewModel.lastTypedAddress.value = zipCodeTextInputLayout.editText?.text.toString()
            if (checkPermissions())
                viewModel.startLocation(false)
            else
                askPermission()
        }

        cancelImageView.setOnClickListener {
            viewModel.stopLocationUpdate(true)
            toggleLocationButton(false)
        }
    }

    private fun paymentRecyclerSetup(
        incommBarcodeDetail: IncommBarcodeDetail?, amount: Int?,
        isIntentForWalletLoad: Boolean?,
        isIntentFromHistoryWalletLoad: Boolean?,
        isIntentForFaremediumLoad: Boolean?
    ) {
        retailersListRecyclerView.layoutManager = LinearLayoutManager(context)
        retailersListRecyclerView.adapter = paymentLocationsAdapter

        paymentLocationsAdapter.onDirectionClick = {
            val url =
                "https://www.google.com/maps/search/?api=1&query=${it?.latitude},${it?.longitude}"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(intent)
        }

        paymentLocationsAdapter.onItemClick = {
            val intent = Intent(activity, RetailerDetailsActivity::class.java)
            intent.putExtra(AppConstants.INTENT_INCOMM_BARCODE_DETAIL, incommBarcodeDetail)
            intent.putExtra(AppConstants.INTENT_INCOMM_STORE, it)
            intent.putExtra(AppConstants.INTENT_INCOMM_ORDER_AMOUNT, amount)
            intent.putExtra(AppConstants.INTENT_INCOMM_WALLET_LOAD, isIntentForWalletLoad)
            intent.putExtra(
                AppConstants.INTENT_INCOMM_HISTORY_WALLET_LOAD,
                isIntentFromHistoryWalletLoad
            )
            intent.putExtra(AppConstants.INTENT_INCOMM_FAREMEDIUM_LOAD, isIntentForFaremediumLoad)
            if (isIntentForWalletLoad == true) {
                startActivityForResult(intent, AppConstants.INCOMM_WALLET_LOAD_REQUEST_CODE)
            } else {
                startActivity(intent)
            }
        }
    }

    private fun setRadiusSpinner() {
        val radiusStringArray = resources.getStringArray(R.array.radius)
        spinnerData.add(IncommLocationRadius(0.25, radiusStringArray[0]))
        spinnerData.add(IncommLocationRadius(0.5, radiusStringArray[1]))
        spinnerData.add(IncommLocationRadius(0.75, radiusStringArray[2]))
        spinnerData.add(IncommLocationRadius(1.0, radiusStringArray[3]))
        spinnerData.add(IncommLocationRadius(2.0, radiusStringArray[4]))
        spinnerData.add(IncommLocationRadius(3.0, radiusStringArray[5]))
        spinnerData.add(IncommLocationRadius(4.0, radiusStringArray[6]))
        spinnerData.add(IncommLocationRadius(5.0, radiusStringArray[7]))
        spinnerData.add(IncommLocationRadius(7.0, radiusStringArray[8]))
        spinnerData.add(IncommLocationRadius(10.0, radiusStringArray[9]))
        spinnerData.add(IncommLocationRadius(15.0, radiusStringArray[10]))
        spinnerData.add(IncommLocationRadius(20.0, radiusStringArray[11]))
        spinnerData.add(IncommLocationRadius(25.0, radiusStringArray[12]))

        val adapter: ArrayAdapter<IncommLocationRadius>? = context?.let {
            ArrayAdapter<IncommLocationRadius>(
                it, android.R.layout
                    .simple_spinner_dropdown_item, spinnerData
            )
        }

        distanceSpinner?.adapter = adapter

        distanceSpinner.doOnLayout {
            distanceSpinner.dropDownWidth = it.width
            distanceSpinner.setPadding(0, it.paddingTop, 0, it.paddingBottom)
        }
    }

    private fun clearEditTextError() {
        zipCodeTextInputLayout.editText?.addTextChangedListener(object : TextWatcherHelper() {
            override fun afterTextChanged(s: Editable?) {
                super.afterTextChanged(s)
                viewModel.getAddress(s.toString())
            }
        })
        zipCodeEditTextLayout.setOnClickListener {
            if (isTalkBackAccessibilityEnabled(it.context))
                zipCodeEditTextLayout.text?.length?.let { index ->
                    zipCodeEditTextLayout.setSelection(
                        index
                    )
                }
        }
    }

    private fun observeLiveData() {
        viewModel.defaultZipCode.observe(this, Observer {
            zipCodeTextInputLayout.editText?.setText(it)
            distanceSpinner?.setSelection(7)
            orderUuid?.let { it1 ->
                viewModel.getRetailerList(
                    (distanceSpinner?.selectedItem as IncommLocationRadius).key,
                    zipCodeTextInputLayout.editText?.text.toString().trim(),
                    it1
                )
            }
        })
        viewModel.startLocationLiveData.observe(this, Observer {
            LocationUtil.startLocationUpdates(this, REQUEST_CHECK_SETTINGS, it)
        })
        viewModel.openSettingLiveData.observe(this, Observer {
            if (it == true)
                LocationUtil.openSettings(this)
        })
        viewModel.stopLocationUpdateLiveData.observe(this, Observer {
            if (it == true)
                LocationUtil.stopLocationUpdates()
        })
        viewModel.addressErrorLiveData.observe(this, Observer {
            if (it != null) {
                zipCodeTextInputLayout.error = null
                zipCodeTextInputLayout.error = getString(it)
            } else {
                zipCodeTextInputLayout.error = null
            }
        })
        viewModel.locationAvailabilityLiveData.observe(this, Observer {
            toggleLocationButton(it)
        })
        viewModel.errorLiveData.observe(this, Observer {
            it?.let { it1 -> handleError(it1) }
        })
        viewModel.retailerStoreListLiveData.observe(this, Observer {
            it?.let {
                if (it.isNotEmpty()) {
                    retailersListRecyclerView.show()
                    paymentLocationsAdapter.setPaymentLocation(it)
                    retailersListRecyclerView.announceForAccessibility(
                        String.format(
                            getString(
                                R.string
                                    .incomm_store_result_accessibility
                            ), it.size
                        )
                    )
                }
            }
        })
        viewModel.displayLiveData.observe(this, Observer {
            retailersListRecyclerView.hide()
            when (it) {
                is Display.EmptyState.Error -> {
                    it.errorTextContent?.let { it1 ->
                        emptyStateLayoutSelectRetailer.showError(
                            it.errorImageDrawable,
                            it.errorTextTitle,
                            it1
                        )
                    }
                }
                is Display.EmptyState.ShowContent -> {
                    retailersListRecyclerView.show()
                    emptyStateLayoutSelectRetailer.showContent()
                }
                is Display.EmptyState.ShowNoData -> {
                    emptyStateLayoutSelectRetailer.showEmpty(it.drawable, it.descriptionText)
                }
                is Display.EmptyState.Loading -> {
                    emptyStateLayoutSelectRetailer.showLoading(it.drawable, it.title)
                }
            }
        })
        viewModel.checkPermissionLiveData.observe(this, Observer {
            if (it == true)
                viewModel.isPermissionHave = checkPermissions()
        })
        viewModel.isValidAddressLiveData.observe(this, Observer {
            if (it) {
                searchRetailerButton.isEnabled = it
                searchRetailerButton.alpha = 1.0f
            } else {
                searchRetailerButton.isEnabled = it
                searchRetailerButton.alpha = 0.6f
            }
        })
        viewModel.lastTypedAddress.observe(this, Observer {
            lastTypedAddress = it
        })
        viewModel.accessibilityLiveData.observe(this, Observer {
            emptyStateLayoutSelectRetailer.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
            if (it.first != null && it.second != null) {
                emptyStateLayoutSelectRetailer.announceForAccessibility(
                    "${it.first?.let { it1 -> getString(it1) }}  ${
                        it
                            .second?.let { it1 -> getString(it1) }
                    }"
                )
            } else {
                emptyStateLayoutSelectRetailer.announceForAccessibility(
                    "${
                        it.first?.let { it1 ->
                            getString(
                                it1
                            )
                        }
                    }"
                )
            }
        })
    }

    private fun askPermission() {
        if (context?.let {
                ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            } != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSION_REQUEST_ACCESS_FINE_LOCATION
            )
            return
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CHECK_SETTINGS -> when (resultCode) {
                RESULT_OK -> {
                    viewModel.startLocation(false)
                }
                RESULT_CANCELED -> {
                    viewModel.mRequestingLocationUpdates = false
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_ACCESS_FINE_LOCATION) {
            for (permission in permissions) {
                if (activity?.let { shouldShowRequestPermissionRationale(permission) } == true) {
                    Timber.tag("denied").e(permission)
                } else {
                    if (context?.let {
                            ActivityCompat.checkSelfPermission(it, permission)
                        } == PackageManager.PERMISSION_GRANTED) {
                        viewModel.startLocation(false)
                    } else {
                        viewModel.openSettingDialog(true)
                    }
                }
            }
        }
    }

    private fun checkPermissions(): Boolean {
        val permissionState = context?.let {
            ActivityCompat.checkSelfPermission(
                it,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun toggleLocationButton(isLocationShowing: Boolean?) {
        TransitionManager.beginDelayedTransition(constraintLayoutSelectRetailer)
        if (isLocationShowing == true) {
            cancelImageView.show()
            locationImageView.hide()
            zipCodeEditTextLayout.setText(getString(R.string.incomm_current_location))
            zipCodeTextInputLayout.isHintEnabled = false
            zipCodeEditTextLayout.isFocusableInTouchMode = false
            zipCodeEditTextLayout.isEnabled = false
            zipCodeTextInputLayout.error = null
            zipCodeTextInputLayout.isErrorEnabled = false
        } else {
            cancelImageView.hide()
            locationImageView.show()
            zipCodeEditTextLayout.setText(lastTypedAddress)
            zipCodeTextInputLayout.isHintEnabled = true
            zipCodeEditTextLayout.isFocusableInTouchMode = true
            zipCodeEditTextLayout.isEnabled = true
            viewModel.mRequestingLocationUpdates = false
        }
    }
}