package co.bytemark.incomm

import android.text.TextUtils
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.payments.GetRetailersWithAddressUseCase
import co.bytemark.domain.interactor.payments.GetRetailersWithAddressUseCaseValue
import co.bytemark.domain.interactor.payments.GetRetailersWithLocationUseCase
import co.bytemark.domain.interactor.payments.GetRetailersWithLocationUseCaseValue
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.incomm.Incomm
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.BytemarkSDK
import com.google.android.gms.location.LocationAvailability
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import kotlinx.coroutines.launch
import java.util.regex.Pattern
import javax.inject.Inject

class SelectRetailerViewModel @Inject constructor() : BaseViewModel(), LifecycleObserver {

    @Inject
    lateinit var getRetailersWithLocationUseCase: GetRetailersWithLocationUseCase

    @Inject
    lateinit var getRetailersWithAddressUseCase: GetRetailersWithAddressUseCase

    @Inject
    lateinit var configHelper: ConfHelper

    init {
        component.inject(this)
    }

    private var mLocationCallback: LocationCallback? = null

    var mRequestingLocationUpdates: Boolean = false

    var isPermissionHave = false

    private var isLocationCall = false

    private var defaultCountryCode: String? = null

    private var latitude: Double = 0.0

    private var longitude: Double = 0.0

    val startLocationLiveData by lazy { MutableLiveData<LocationCallback>() }

    val openSettingLiveData by lazy { MutableLiveData<Boolean>() }

    val stopLocationUpdateLiveData by lazy { MutableLiveData<Boolean>() }

    val addressErrorLiveData by lazy { MutableLiveData<Int>() }

    val locationAvailabilityLiveData by lazy { MutableLiveData<Boolean>() }

    val displayLiveData by lazy { MutableLiveData<Display>() }

    val retailerStoreListLiveData by lazy { MutableLiveData<List<Incomm>>() }

    var locationCoordinatesCallback: ((coordinates: Pair<Double, Double>) -> Unit)? = null

    val checkPermissionLiveData by lazy { MutableLiveData<Boolean>() }

    val defaultZipCode by lazy { MutableLiveData<String>() }

    val isValidAddressLiveData by lazy { MutableLiveData<Boolean>() }

    val lastTypedAddress by lazy { MutableLiveData<String>() }

    val accessibilityLiveData by lazy {
        MutableLiveData<Pair<Int?, Int?>>()
    }

    fun getDefaultZipCode() = uiScope.launch {
        defaultCountryCode = configHelper.countryCode
        val zipCode = configHelper.zipCode
        defaultZipCode.value = zipCode
    }

    fun startLocation(isRequestingPermission: Boolean) {
        if (mRequestingLocationUpdates == isRequestingPermission && isPermissionHave) {
            mLocationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    super.onLocationResult(locationResult)
                    latitude = locationResult.locations.first().latitude
                    longitude = locationResult.locations.first().longitude
                    locationCoordinatesCallback?.let {
                        it(
                            Pair(latitude, longitude)
                        )
                    }
                    mRequestingLocationUpdates = true
                }

                override fun onLocationAvailability(locationAvailability: LocationAvailability?) {
                    super.onLocationAvailability(locationAvailability)
                    mRequestingLocationUpdates = locationAvailability?.isLocationAvailable != false
                    locationAvailabilityLiveData.value = mRequestingLocationUpdates
                }
            }
            startLocationLiveData.value = mLocationCallback
        }
    }

    fun openSettingDialog(value: Boolean) {
        openSettingLiveData.value = value
    }

    fun stopLocationUpdate(value: Boolean) {
        stopLocationUpdateLiveData.value = value
    }

    fun getAddress(locationInputText: String?) {
        if (locationInputText?.isEmpty() == false) {
            if (isValidPattern("[0-9]+\\s*[a-zA-Z]+", locationInputText)) {
                isValidAddressLiveData.value = false
                addressErrorLiveData.value = R.string.incomm_zip_should_only_5_digit_numeric
            } else if (TextUtils.isDigitsOnly(locationInputText)) {
                if (isValidPattern("^(\\d{5})\$", locationInputText)) {
                    isValidAddressLiveData.value = true
                    addressErrorLiveData.value = null
                } else {
                    isValidAddressLiveData.value = false
                    addressErrorLiveData.value = R.string.incomm_zip_should_only_5_digit
                }
            } else if (isValidPattern("^[a-zA-Z\\s]+\\,[a-zA-Z\\s]+\$", locationInputText)) {
                addressErrorLiveData.value = null
                isValidAddressLiveData.value = true
            } else {
                isValidAddressLiveData.value = false
                addressErrorLiveData.value = R.string.incomm_enter_as_city_state
            }
        } else {
            isValidAddressLiveData.value = false
            addressErrorLiveData.value = R.string.incomm_location_cannot_empty
        }
        if (mRequestingLocationUpdates) {
            isValidAddressLiveData.value = true
        }
    }

    fun getRetailerList(radius: Double, address: String?, orderUuid: String) {
        if (mRequestingLocationUpdates) {
            displayLiveData.value = Display.EmptyState.Loading(
                R.drawable.ic_retailer, R.string
                    .incomm_loading_button_accessibility
            )
            accessibilityLiveData.value = Pair(R.string.incomm_loading_button_accessibility, null)
            if (latitude != 0.0 && longitude != 0.0) {
                getRetailerListWithLocation(latitude, longitude, radius, orderUuid)
            } else {
                isLocationCall = false
                locationCoordinatesCallback = {
                    if (!isLocationCall) {
                        getRetailerListWithLocation(it.first, it.second, radius, orderUuid)
                        isLocationCall = true
                    }
                }
            }
        } else {
            getRetailerListWithAddress("$defaultCountryCode $address", radius,orderUuid)
        }
    }

    private fun getRetailerListWithAddress(address: String, radius: Double, orderUuid: String) = uiScope.launch {
        displayLiveData.value = Display.EmptyState.Loading(
            R.drawable.ic_retailer, R.string
                .incomm_loading_button_accessibility
        )
        accessibilityLiveData.value = Pair(R.string.incomm_loading_button_accessibility, null)
        when (val result = getRetailersWithAddressUseCase(
            GetRetailersWithAddressUseCaseValue(address, radius, orderUuid)
        )) {
            is Result.Success -> {
                setRetailerData(result.data?.incommStoreList)
            }
            is Result.Failure -> {
                setRetailerError(result.bmError.first())
            }
        }
    }

    private fun getRetailerListWithLocation(latitude: Double, longitude: Double, radius: Double, orderUuid: String) =
        uiScope.launch {
            when (val result = getRetailersWithLocationUseCase(
                GetRetailersWithLocationUseCaseValue(latitude, longitude, radius, orderUuid)
            )) {
                is Result.Success -> {
                    setRetailerData(result.data?.incommStoreList)
                }
                is Result.Failure -> {
                    setRetailerError(result.bmError.first())
                }
            }
        }

    private fun setRetailerData(storeList: List<Incomm>?) {
        if (storeList?.isNotEmpty() == true) {
            displayLiveData.value = Display.EmptyState.ShowContent()
            retailerStoreListLiveData.value = storeList
        } else {
            displayLiveData.value = Display.EmptyState.ShowNoData(R.drawable.ic_retailer, R.string.incomm_no_result_found)
            accessibilityLiveData.value = Pair(R.string.incomm_no_result_found, null)
        }
    }

    private fun setRetailerError(bmError: BMError) {
        if (bmError.code == BytemarkSDK.ResponseCode.NETWORK_NOT_AVAILABLE) {
            displayLiveData.value =
                Display.EmptyState.Error(
                    R.drawable.error_material,
                    R.string.change_password_popup_conErrorTitle,
                    R.string.change_password_Popup_con_error_body
                )
            accessibilityLiveData.value =
                Pair(R.string.change_password_popup_conErrorTitle, R.string.change_password_Popup_con_error_body)
        } else {
            displayLiveData.value =
                Display.EmptyState.Error(R.drawable.error_material, R.string.popup_error, R.string.something_went_wrong)
            accessibilityLiveData.value = Pair(R.string.popup_error, R.string.something_went_wrong)
        }
    }

    private fun isValidPattern(regex: String, value: String): Boolean {
        return Pattern.compile(regex).matcher(value).matches()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun onLifeCyclePause() {
        stopLocationUpdate(true)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onLifeCycleResume() {
        startLocation(true)
        checkPermissionLiveData.value = true
    }
}