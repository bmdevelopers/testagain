package co.bytemark.incomm

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.domain.model.incomm.Coordinates
import co.bytemark.domain.model.incomm.Incomm
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_incomm_retailer.view.*

class IncommStoreAdapter : RecyclerView
.Adapter<IncommStoreAdapter.PaymentLocationsViewHolder>() {

    var onItemClick: ((incommRetailer: Incomm) -> Unit)? = null
    var onDirectionClick: ((direction: Coordinates?) -> Unit)? = null

    private val storeList = mutableListOf<Incomm>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): PaymentLocationsViewHolder =
        PaymentLocationsViewHolder(
            LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_incomm_retailer, viewGroup, false)
        )

    override fun getItemCount(): Int = storeList.size

    override fun onBindViewHolder(viewHolder: PaymentLocationsViewHolder, position: Int) {
        with(viewHolder.itemView) {
            val store = storeList[position]
            textViewRetailerName.text = store.merchant.name
            textViewRetailerAddress.text = store.address?.formattedAddress
            textViewDistanceFromLocation.text =
                String.format(context.getString(R.string.incomm_store_distance_prefix_mi), store.distance)
            Glide.with(this.context).load(store.merchant.iconUrl)
                .placeholder(ContextCompat.getDrawable(this.context, R.drawable.ic_no_retailer_logo))
                .into(imageViewRetailerIcon)
            textViewRetailerDirection.setOnClickListener {
                onDirectionClick?.invoke(store.coordinates)
            }
            this.setOnClickListener {
                onItemClick?.invoke(store)
            }
            contentDescription = "${textViewRetailerName.text}  ${textViewRetailerAddress.text} " +
                    "${textViewDistanceFromLocation.text}  ${textViewRetailerDirection.text}"
        }
    }

    infix fun setPaymentLocation(stores: List<Incomm>?) {
        stores?.let {
            storeList.clear()
            storeList.addAll(stores)
            notifyDataSetChanged()
        }
    }

    inner class PaymentLocationsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}

