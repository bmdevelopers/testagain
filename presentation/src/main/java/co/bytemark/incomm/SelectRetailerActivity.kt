package co.bytemark.incomm

import android.content.Intent
import android.os.Bundle
import androidx.core.app.TaskStackBuilder
import co.bytemark.R
import co.bytemark.base.ToolbarActivity
import co.bytemark.domain.model.incomm.IncommBarcodeDetail
import co.bytemark.helpers.AppConstants
import co.bytemark.payment_methods.PaymentMethodsActivity
import co.bytemark.use_tickets.UseTicketsActivity
import co.bytemark.widgets.util.replaceFragment

class SelectRetailerActivity : ToolbarActivity() {

    private var isIntentForWalletLoad = false
    private var isIntentFromHistoryWalletLoad = false
    private var isIntentForFaremediumLoad = false

    override fun getLayoutRes() = R.layout.activity_select_retailer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(getString(R.string.incomm_toolbar_title_select_store))
        val barcodeDetail =
            intent.getParcelableExtra<IncommBarcodeDetail>(AppConstants.INTENT_INCOMM_BARCODE_DETAIL)
        val amount = intent.getIntExtra(AppConstants.INTENT_INCOMM_ORDER_AMOUNT, 0)
        val orderUUid = intent.getStringExtra(AppConstants.INTENT_INCOMM_ORDER_UUID)
        isIntentForWalletLoad = intent.getBooleanExtra(
            AppConstants
                .INTENT_INCOMM_WALLET_LOAD, false
        )
        isIntentFromHistoryWalletLoad = intent.getBooleanExtra(
            AppConstants
                .INTENT_INCOMM_HISTORY_WALLET_LOAD, false
        )
        isIntentForFaremediumLoad= intent.getBooleanExtra(
            AppConstants
                .INTENT_INCOMM_FAREMEDIUM_LOAD, false
        )
        replaceFragment(
            SelectRetailerFragment.newInstance(
                barcodeDetail,
                amount,
                isIntentForWalletLoad,
                isIntentFromHistoryWalletLoad,
                orderUUid,
                isIntentForFaremediumLoad
            ), R.id.fragment
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == AppConstants.INCOMM_WALLET_LOAD_REQUEST_CODE) {
            finish()
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            super.onBackPressed()
        } else {
            when {
                isIntentForWalletLoad -> {
                    finish()
                }
                isIntentFromHistoryWalletLoad -> {
                    val builder = TaskStackBuilder.create(this)
                    builder.addNextIntentWithParentStack(Intent(this, PaymentMethodsActivity::class.java))
                    builder.startActivities()
                }
                else -> {
                    val intent = Intent(
                        this,
                        UseTicketsActivity::class.java
                    )
                    intent.putExtra(AppConstants.INTENT_INCOMM_USE_TICKET, true)
                    val builder = TaskStackBuilder.create(this)
                    builder.addNextIntentWithParentStack(intent)
                    builder.startActivities()
                }
            }

        }
    }

}