package co.bytemark.incomm

import android.annotation.SuppressLint
import android.content.Intent
import android.content.IntentSender
import android.net.Uri
import android.os.Looper
import android.provider.Settings
import androidx.fragment.app.Fragment
import co.bytemark.BuildConfig
import co.bytemark.R
import co.bytemark.widgets.util.showMaterialDialog
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import timber.log.Timber

object LocationUtil {

    private const val updateIntervalInMilliseconds: Long = 60000
    private const val fastestUpdateIntervalInMilliseconds: Long = 60000

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var settingsClient: SettingsClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationSettingsRequest: LocationSettingsRequest
    private var locationCallback: LocationCallback? = null

    fun init(locationCallback: LocationCallback?) {
        this.locationCallback = locationCallback

        locationRequest = LocationRequest()
        locationRequest.interval = updateIntervalInMilliseconds
        locationRequest.fastestInterval = fastestUpdateIntervalInMilliseconds
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(locationRequest)
        locationSettingsRequest = builder.build()
    }

    @SuppressLint("MissingPermission")
    fun startLocationUpdates(
        fragment: Fragment?,
        requestCode: Int,
        locationCallback: LocationCallback?
    ) {
        stopLocationUpdates()
        fragment?.activity?.let {
            init(locationCallback)
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(it)
            settingsClient = LocationServices.getSettingsClient(it)

            settingsClient.checkLocationSettings(locationSettingsRequest)
                ?.addOnSuccessListener(it) {
                    // Callback if location is enabled and now locationCallback will receive updates in every one minute
                    fusedLocationClient.requestLocationUpdates(
                        locationRequest,
                        this.locationCallback,
                        Looper.myLooper()
                    )
                }
                ?.addOnFailureListener(it) { e ->
                    when ((e as ApiException).statusCode) {
                        // Callback if location is not enabled and now ask for enable the location
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                            try {
                                val rae = e as ResolvableApiException
                                fragment.startIntentSenderForResult(
                                    rae.resolution.intentSender,
                                    requestCode,
                                    null,
                                    0,
                                    0,
                                    0,
                                    null
                                )
                            } catch (sie: IntentSender.SendIntentException) {
                                Timber.tag("Location").i("PendingIntent unable to execute request.")
                            }
                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            val errorMessage = "Location settings are inadequate, and cannot be " +
                                    "fixed here. Fix in Settings."
                            Timber.tag("Location").e(errorMessage)
                        }
                    }
                }
        }
    }

    fun openSettings(fragment: Fragment?) {
        fragment?.context?.let {
            it.showMaterialDialog(
                title = R.string.dialog_title_location_service_disabled,
                body = R.string.dialog_message_location_service_disabled,
                positiveButton = R.string.location_service_setting_dialog_positive_title,
                negativeButton = R.string.location_service_setting_dialog_negative_title,
                positiveAction = { _, _ ->
                    fragment.startActivity(
                        Intent(
                            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.parse("package:" + BuildConfig.APPLICATION_ID)
                        )
                    )
                },
                negativeAction = { materialDialog, _ -> materialDialog.dismiss() }
            )
        }

    }

    fun stopLocationUpdates() {
        if (locationCallback != null) {
            fusedLocationClient.removeLocationUpdates(locationCallback).addOnFailureListener {
                Timber.tag("Location")
                        .e("Location updates failed with exception ${it.localizedMessage}")
            }.addOnSuccessListener {
                Timber.tag("Location").e("Location updates Stopped Success")
            }
            Timber.tag("Location").e("Location updates stopped")
        }

    }
}