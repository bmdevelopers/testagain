package co.bytemark.use_tickets

import android.widget.LinearLayout
import android.widget.TextView

data class EmptyStates(val activeTicketsText: TextView? = null,
                       val linearLayoutLogin: LinearLayout? = null,
                       val activeTicketsLayout: LinearLayout? = null,
                       val linearLayoutNoTickets: LinearLayout? = null,
                       val linearLayoutNoVirtualFareMedium: LinearLayout? = null,
                       val linearLayoutNetworkDown: LinearLayout? = null,
                       val loadingUseTickets: LinearLayout? = null,
                       val linearLayoutNoLoggedInText: LinearLayout? = null,
                       val linearLayoutNoTicketsText: LinearLayout? = null,
                       val linearLayoutNoNetworkText: LinearLayout? = null)