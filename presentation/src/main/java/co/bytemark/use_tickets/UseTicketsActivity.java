package co.bytemark.use_tickets;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.facebook.CallbackManager;
import com.google.android.material.tabs.TabLayout;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import co.bytemark.CustomerMobileApp;
import co.bytemark.MainActivity;
import co.bytemark.R;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.analytics.AnalyticsPlatformsContract;
import co.bytemark.appnotification.NotificationsActivity;
import co.bytemark.authentication.AuthenticationActivity;
import co.bytemark.base.MasterActivity;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.helpers.RatingAndFeedBackHelper;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.Pass;
import co.bytemark.use_tickets.adapter.UseTicketsViewPagerAdapter;
import co.bytemark.widgets.util.Util;

import static co.bytemark.sdk.model.common.Action.USE_TICKETS;


public class UseTicketsActivity extends MasterActivity implements UseTicketsAvailableFragment.NavigateAwayReason, UseTicketsActiveFragment.NavigateAwayReason,
        UseTicketsUIComponent.NotificationsListener {

    @Inject
    AnalyticsPlatformAdapter analyticsPlatformAdapter;

    @Inject
    ConfHelper confHelper;

    @BindView(R.id.fragment)
    FrameLayout fragment;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.elertsBottomSheet)
    ImageButton bottomSheetBtn;

    private int unreadNotificationsCount;
    private CallbackManager callbackManager;

    public ViewPager useTicketsViewPager;
    private UseTicketsViewPagerAdapter useTicketsViewPagerAdapter;
    private TabLayout useTicketsTabs;
    private String TAG = getClass().getSimpleName();

    private boolean shouldShowDeviceStolenDialog = true;
    private boolean shouldShowAppUpdateDialog = true;
    public Uri deepLink = null;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_use_tickets_new;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getIntent().getStringExtra(AppConstants.DEEPLINK_URL) != null) {
            deepLink = Uri.parse(getIntent().getStringExtra(AppConstants.DEEPLINK_URL));
        }
        CustomerMobileApp.Companion.getComponent().inject(this);
        if (!BytemarkSDK.isLoggedIn()) {
            launchLoginScreen();
        } else {
            setNavigationViewCheckedItem(USE_TICKETS);
            setToolbarTitle(confHelper.getNavigationMenuScreenTitleFromTheConfig(USE_TICKETS));

            if (CustomerMobileApp.Companion.getIsRatingPopUPRequired()) {
                new RatingAndFeedBackHelper(this).showRatingPopUP();
                CustomerMobileApp.Companion.setRatingPopUpRequired(false);
            }

//            final UseTicketsFragment fragment = new UseTicketsFragment();
//            getSupportFragmentManager().beginTransaction()
//                    .replace(R.id.fragment, fragment)
//                    .commit();
        }
//        if (!BuildConfig.DATABERRIES_KEY.equalsIgnoreCase("null")) {
//            DataBerries.requestLocationPermission(this);
//        }
        if (confHelper.getOrganizationFareMediumConfigs() != null && !confHelper.getOrganizationFareMediumConfigs().isEmpty()) {
            fragment.setVisibility(View.VISIBLE);
            tabLayout.setVisibility(View.GONE);
            final FareMediumFragment fragment = new FareMediumFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment, fragment)
                    .commit();
        } else {
            setupViewPager(getIntent().getBooleanExtra(AppConstants.INTENT_INCOMM_USE_TICKET, false));
            if(confHelper.isElertSdkEnabled()) {
                Util.regUserAndOrgForECUI(getApplication());
                if(confHelper.isElertSdkEnabledOnTicketsScreen()) {
                    bottomSheetBtn.setVisibility(View.VISIBLE);
                }
                DrawableCompat.setTint(
                        DrawableCompat.wrap(bottomSheetBtn.getDrawable()),
                        ContextCompat.getColor(this, R.color.orgBackgroundAccentColor)
                );
                bottomSheetBtn.setOnClickListener(v -> {
                    showElertsBottomSheet();
                });
            }
        }
        analyticsPlatformAdapter.useTicketsScreenDisplayed();
    }

    private void setupViewPager(boolean isFromIncomm) {
        useTicketsViewPager = findViewById(R.id.viewPager);
        useTicketsViewPager.setVisibility(View.VISIBLE);
        useTicketsTabs = findViewById(R.id.tabs);

        useTicketsViewPagerAdapter = new UseTicketsViewPagerAdapter(getSupportFragmentManager(), this, isFromIncomm);
        useTicketsViewPager.setOffscreenPageLimit(2);
        useTicketsViewPager.setAdapter(useTicketsViewPagerAdapter);
        // Setting up to show Available tickets tab upon opening Use Tickets screen
        // TODO update so it will select tabs based on availability of Active or Available passes
        useTicketsViewPager.setCurrentItem(1);
        useTicketsTabs.setupWithViewPager(useTicketsViewPager);

        // Set up ViewPager colors
        //useTicketsTabs.setTabTextColors(ColorStateList.valueOf(confHelper.getHeaderThemePrimaryTextColor()));
        //useTicketsTabs.setBackgroundColor(confHelper.getHeaderThemeBackgroundColor());
    }


    private void launchLoginScreen() {
        if (confHelper.isAuthenticationNative()) {
            Intent intent = new Intent(this, AuthenticationActivity.class);
            intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.USE_TICKETS);
            if(deepLink != null) {
                intent.putExtra(AppConstants.DEEPLINK_URL, deepLink.toString());
            }
            startActivity(intent);
        } else {
            startActivityForResult(new Intent(this, MainActivity.class), MasterActivity.REQUEST_CODE_LOGIN);
        }
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    }


//    @Override
//    public void onUnreadNotificationsCountChanged(@NonNull int unreadNotificationsCount) {
//        this.unreadNotificationsCount = unreadNotificationsCount;
//        invalidateOptionsMenu();
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (confHelper.isNotificationEnabled()) {
            getMenuInflater().inflate(R.menu.menu_use_tickets, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (confHelper.isNotificationEnabled()) {
            final MenuItem notifications = menu.findItem(R.id.action_notifications);
            notifications.setActionView(R.layout.menu_item_notifications_layout);

            final TextView textView = notifications.getActionView().findViewById(R.id.tv_notifications_count);
            textView.setBackgroundTintList(ColorStateList.valueOf(confHelper.getHeaderThemeAccentColor()));
            textView.setTextColor(confHelper.getHeaderThemeSecondaryTextColor());

            if (unreadNotificationsCount != 0) {
                String count = String.valueOf(unreadNotificationsCount);
                textView.setVisibility(View.VISIBLE);
                String string = getResources().getQuantityString(R.plurals.notifications_count, unreadNotificationsCount, unreadNotificationsCount);
                textView.setContentDescription(string);
                textView.setText(count);
            } else {
                textView.setVisibility(View.GONE);
            }

            final ImageView imageView = notifications.getActionView().findViewById(R.id.iv_notifications);
            imageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_bell_filled));
            imageView.setImageTintList(ColorStateList.valueOf(confHelper.getHeaderThemePrimaryTextColor()));
            imageView.setContentDescription(getResources().getString(R.string.screen_title_notification));

            notifications.getActionView().setOnClickListener(v -> {
                //UseTicketsFragment.updateNotificationNavigation();
                startActivity(new Intent(UseTicketsActivity.this, NotificationsActivity.class));
            });
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        final Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (fragment instanceof BackHandler) {
            if (((BackHandler) fragment).onBackPressed()) {
                super.onBackPressed();
            } else {
                getSupportFragmentManager().popBackStack();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void setNavigateAwayReason(int navigateAwayReason, String fragment) {
        try {
            if (fragment.equalsIgnoreCase(AppConstants.USE_TICKETS_AVAILABLE_FRAGMENT_TYPE)) {
                UseTicketsAvailableFragment useTicketsAvailableFragment = (UseTicketsAvailableFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + useTicketsViewPager.getCurrentItem());
                if (useTicketsAvailableFragment != null) {
                    useTicketsAvailableFragment.setNavigateAwayReason(navigateAwayReason);
                }

                UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 1));
                if (useTicketsActiveFragment != null) {
                    useTicketsActiveFragment.setNavigateAwayReason(navigateAwayReason);
                }
            } else if (fragment.equalsIgnoreCase(AppConstants.USE_TICKETS_ACTIVE_FRAGMENT_TYPE)) {
                UseTicketsAvailableFragment useTicketsAvailableFragment = (UseTicketsAvailableFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() + 1));
                if (useTicketsAvailableFragment != null) {
                    useTicketsAvailableFragment.setNavigateAwayReason(navigateAwayReason);
                }

                UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem()));
                if (useTicketsActiveFragment != null) {
                    useTicketsActiveFragment.setNavigateAwayReason(navigateAwayReason);
                }
            } else {
                UseTicketsAvailableFragment useTicketsAvailableFragment = (UseTicketsAvailableFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 1));
                if (useTicketsAvailableFragment != null) {
                    useTicketsAvailableFragment.setNavigateAwayReason(navigateAwayReason);
                }

                UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 2));
                if (useTicketsActiveFragment != null) {
                    useTicketsActiveFragment.setNavigateAwayReason(navigateAwayReason);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void moveToActiveTabsIfActivePassesAreAvailable(boolean isActivePassesAvailable) {
        if (isActivePassesAvailable && (useTicketsViewPager.getCurrentItem() != 0)) {
            useTicketsViewPager.setCurrentItem(0);
        } else if (!isActivePassesAvailable) {
            useTicketsViewPager.setCurrentItem(1);
        }
    }

    public void CloudPassesLoaded() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem()));
        if (fragment == null) return;
        if (fragment instanceof TicketHistoryFragment) {
            UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 2));
            if (useTicketsActiveFragment != null) {
                useTicketsActiveFragment.setSwipeRefreshingToFalse();
            }
        } else if (fragment instanceof UseTicketsActiveFragment) {
            UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) fragment;
            useTicketsActiveFragment.setSwipeRefreshingToFalse();
        } else if (fragment instanceof UseTicketsAvailableFragment) {
            UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 1));
            if (useTicketsActiveFragment != null) {
                useTicketsActiveFragment.setSwipeRefreshingToFalse();
            }
        }
    }

    private void showElertsBottomSheet() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem()));
        if (fragment == null) return;
        if (fragment instanceof TicketHistoryFragment) {
            ((TicketHistoryFragment) fragment).showElertsBottomSheet();
        } else if (fragment instanceof UseTicketsAvailableFragment) {
            ((UseTicketsAvailableFragment) fragment).showElertsBottomSheet();
        } else {
            ((UseTicketsActiveFragment) fragment).showElertsBottomSheet();
        }
    }

    public void clearEnablerAndSelectedTicketsList() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem()));
        if (fragment == null) return;
        if (fragment instanceof UseTicketsAvailableFragment) {
            ((UseTicketsAvailableFragment) fragment).clearEnablerAndSelectedTicketsList();
        } else if (fragment instanceof UseTicketsActiveFragment) {
            UseTicketsAvailableFragment useTicketsAvailableFragment = (UseTicketsAvailableFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() + 1));
            if (useTicketsAvailableFragment != null)
                useTicketsAvailableFragment.clearEnablerAndSelectedTicketsList();
        } else {
            UseTicketsAvailableFragment useTicketsAvailableFragment = (UseTicketsAvailableFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 1));
            if (useTicketsAvailableFragment != null)
                useTicketsAvailableFragment.clearEnablerAndSelectedTicketsList();
        }
    }


    //Todo: Remove try/catch and instead use instanceOf. This refactoring is pretty risky at this time
    // Will take a seperate refactoring ticket for this.
    public void setActivePasses(@NonNull List<Pass> activePasses) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem()));
        if (fragment == null) return;
        if (fragment instanceof TicketHistoryFragment) {
            UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 2));
            if (useTicketsActiveFragment != null) {
                useTicketsActiveFragment.setActivePasses(activePasses);
            }
        } else if (fragment instanceof UseTicketsActiveFragment) {
            UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) fragment;
            useTicketsActiveFragment.setActivePasses(activePasses);
        } else if (fragment instanceof UseTicketsAvailableFragment) {
            UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 1));
            if (useTicketsActiveFragment != null) {
                useTicketsActiveFragment.setActivePasses(activePasses);
            }
        }
    }

    public void setUnlimitedPasses(@NonNull Set<Pass> unlimitedPasses) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem()));
        if (fragment == null) return;
        if (fragment instanceof TicketHistoryFragment) {
            UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 2));
            if (useTicketsActiveFragment != null) {
                useTicketsActiveFragment.setUnlimitedPasses(unlimitedPasses);
            }
        } else if (fragment instanceof UseTicketsActiveFragment) {
            UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) fragment;
            useTicketsActiveFragment.setUnlimitedPasses(unlimitedPasses);
        } else if (fragment instanceof UseTicketsAvailableFragment) {
            UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 1));
            if (useTicketsActiveFragment != null) {
                useTicketsActiveFragment.setUnlimitedPasses(unlimitedPasses);
            }
        }
    }

    // We need to notify in active fragment as well to load unlimited passes.
    public void notifyRefreshToActiveFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem()));
        if (fragment == null) return;
        if (fragment instanceof TicketHistoryFragment) {
            UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 2));
            if (useTicketsActiveFragment != null) {
                useTicketsActiveFragment.setUnlimitedCallbackCalled(false);
            }
        } else if (fragment instanceof UseTicketsActiveFragment) {
            UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) fragment;
            useTicketsActiveFragment.setUnlimitedCallbackCalled(false);
        } else if (fragment instanceof UseTicketsAvailableFragment) {
            UseTicketsActiveFragment useTicketsActiveFragment = (UseTicketsActiveFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 1));
            if (useTicketsActiveFragment != null) {
                useTicketsActiveFragment.setUnlimitedCallbackCalled(false);
            }
        }
    }

    public void setCallbackManager(CallbackManager callbackManager) {
        this.callbackManager = callbackManager;
    }

    public void setDeviceStolenFlag(boolean flag) {
        shouldShowDeviceStolenDialog = flag;
    }

    public boolean getDeviceStolenFlag() {
        return shouldShowDeviceStolenDialog;
    }

    public boolean isShouldShowAppUpdateDialog() {
        return shouldShowAppUpdateDialog;
    }

    public void setShouldShowAppUpdateDialog(boolean shouldShowAppUpdateDialog) {
        this.shouldShowAppUpdateDialog = shouldShowAppUpdateDialog;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.ELERTS_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String errorMessage = data.getExtras().getString("elert_error_message", null);
                int errorCode =  data.getExtras().getInt("elert_error_code");
                Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem()));
                if (fragment == null) return;
                if (fragment instanceof TicketHistoryFragment) {
                    ((TicketHistoryFragment) fragment).showElertsDialog(errorMessage == null, errorMessage, errorCode);
                    ((TicketHistoryFragment) fragment).putErrorDialogOnHold();
                    UseTicketsAvailableFragment useTicketsAvailableFragment = (UseTicketsAvailableFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 1));
                    if (useTicketsAvailableFragment != null) {
                        useTicketsAvailableFragment.putHoldonCloudErrorDialog();
                    }
                } else if (fragment instanceof UseTicketsAvailableFragment) {
                    ((UseTicketsAvailableFragment) fragment).showElertsDialog(errorMessage == null, errorMessage, errorCode);
                    ((UseTicketsAvailableFragment) fragment).putHoldonCloudErrorDialog();
                } else {
                    ((UseTicketsActiveFragment) fragment).showElertsDialog(errorMessage == null, errorMessage, errorCode);
                    UseTicketsAvailableFragment useTicketsAvailableFragment = (UseTicketsAvailableFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() + 1));
                    if (useTicketsAvailableFragment != null) {
                        useTicketsAvailableFragment.putHoldonCloudErrorDialog();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void showCloudPassErrorDialog() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem()));
        if (fragment == null) return;
        if (fragment instanceof TicketHistoryFragment) {
            UseTicketsAvailableFragment useTicketsAvailableFragment = (UseTicketsAvailableFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() - 1));
            if (useTicketsAvailableFragment != null) {
                useTicketsAvailableFragment.showCloudPassesErrorDialog();
            }
        } else if (fragment instanceof UseTicketsActiveFragment) {
            UseTicketsAvailableFragment useTicketsAvailableFragment = (UseTicketsAvailableFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + (useTicketsViewPager.getCurrentItem() + 1));
            if (useTicketsAvailableFragment != null) {
                useTicketsAvailableFragment.showCloudPassesErrorDialog();
            }
        }
    }

    @Override
    public void onUnreadNotificationsCountChanged(int unreadNotificationsCount) {
        this.unreadNotificationsCount = unreadNotificationsCount;
        invalidateOptionsMenu();
    }
}
