package co.bytemark.use_tickets;

import android.view.animation.Animation;

/**
 * Created by Arunkumar on 28/04/17.
 */
class AnimationListenerAdapter implements Animation.AnimationListener {
    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
