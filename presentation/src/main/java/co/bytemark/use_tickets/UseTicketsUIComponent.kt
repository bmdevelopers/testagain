package co.bytemark.use_tickets

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.accessibility.AccessibilityEvent.TYPE_VIEW_FOCUSED
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.BounceInterpolator
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.buy_tickets.BuyTicketsActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.Pass
import co.bytemark.sdk.passcache.PassCacheManager.CACHE_INVALIDATION_TIMEOUT_MS
import co.bytemark.use_tickets.UseTickets.Presenter.Cloud
import co.bytemark.use_tickets.adapter.UseTicketsAdapterNew
import co.bytemark.use_tickets.send_ticket_dialog.SendTicketToUserDialog
import co.bytemark.widgets.swiperefreshlayout.BmSwipeRefreshLayout
import co.bytemark.widgets.util.Util
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.bottomsheet.BottomSheetDialog
import timber.log.Timber
import java.lang.System.currentTimeMillis


class UseTicketsUIComponent {
    private var passType: Int = Cloud
    private lateinit var moveToCloud: TextView
    private lateinit var repurchase: TextView
    private lateinit var viewTicketDetails: TextView
    private lateinit var resendReceipt: TextView
    private lateinit var sendTicket: TextView

    // No reason
    private val NONE = -1

    // User navigated to v3 screen
    private val V3 = 0

    // User requested sign in
    private val SIGN_IN = 1

    // Buy tickets
    private val BUY_TICKETS = 2

    // Buy tickets
    private val NOTIFICATIONS = 3

    private val OFFLINE = 4

    public var passToRepurchase: Pass? = null

    fun initialisePresenter(presenter: UseTickets.Presenter) {
        presenter.initialize()
    }

    fun downloadGTFS(presenter: UseTickets.Presenter) {
        presenter.downloadGTFS()
    }

    fun initialiseAdapterAndAttachToRecycler(
        layoutManager: LinearLayoutManager,
        ticketsRecyclerView: RecyclerView,
        useTicketsAdapterNew: UseTicketsAdapterNew,
        passType: PassType,
        adapterClickListener: UseTicketsAdapterNew.ClickListener
    ) {
        ticketsRecyclerView.layoutManager = layoutManager
        useTicketsAdapterNew.setClickListener(adapterClickListener)
        useTicketsAdapterNew.setPassType(passType)
        ticketsRecyclerView.adapter = useTicketsAdapterNew
    }

    fun onPassClick(
        confHelper: ConfHelper,
        recentPasses: MutableList<Pass>,
        actionButton: Button,
        pass: Pass,
        selectedOverlay: LinearLayout,
        noOfPasses: Int,
        activity: Activity,
        presenter: UseTickets.Presenter
    ) {

        if (!pass.isUnavailable) {
            if (confHelper.isMultiPassesActivationEnabled(pass.issuer.userUuid)) {
                if (!recentPasses.contains(pass)) {
                    recentPasses.add(pass)
                } else {
                    recentPasses
                        .remove(pass)
                }
                selectUnselectPass(pass, selectedOverlay)
                if (actionButton.visibility == VISIBLE) {
                    if (recentPasses
                            .isEmpty()
                    ) {
                        val anim = AnimationUtils.loadAnimation(activity, R.anim.fade_out)
                        anim.setAnimationListener(object : Animation.AnimationListener {
                            override fun onAnimationStart(animation: Animation) {

                            }

                            override fun onAnimationEnd(animation: Animation) {
                                actionButton.visibility = GONE
                            }

                            override fun onAnimationRepeat(animation: Animation) {

                            }
                        })
                        actionButton.startAnimation(anim)
                    } else {
                        actionButton.text =
                            "${actionButton.context.getString(R.string.use_tickets_display)} ($recentPasses.size)"
                    }
                } else {
                    if (noOfPasses > 1) {
                        actionButton.visibility = VISIBLE
                        val anim = AnimationUtils.loadAnimation(activity, R.anim.bounce)
                        anim.interpolator = BounceInterpolator()
                        actionButton.startAnimation(anim)
                        actionButton.text =
                            "${actionButton.context.getString(R.string.use_tickets_display)} ($recentPasses.size)"
                    } else {
                        removeSelectionAndDisplayTickets(
                            actionButton,
                            presenter,
                            selectedOverlay,
                            recentPasses
                        )
                    }
                }
            } else {
                displaySingleTicket(pass, actionButton, presenter)
            }
        }
    }

    private fun selectUnselectPass(pass: Pass, selectedOverlay: LinearLayout) {
        pass.selected = !pass.selected
        if (pass.selected) {
            selectedOverlay.visibility = View.VISIBLE
        } else {
            selectedOverlay.visibility = GONE
        }
    }

    private fun displaySingleTicket(
        pass: Pass,
        actionButton: Button,
        presenter: UseTickets.Presenter
    ) {
        if (pass.selected) {
            pass.selected = !pass.selected
        }
        actionButton.visibility = GONE
        presenter.activateTickets(pass)
    }

    fun removeSelectionAndDisplayTickets(
        actionButton: Button,
        presenter: UseTickets.Presenter,
        selectedOverlay: LinearLayout,
        recentPasses: MutableList<Pass>
    ) {
        presenter.activateTickets(recentPasses)
        actionButton.visibility = GONE
        for (pass in recentPasses) {
            if (pass.selected) {
                selectedOverlay.visibility = GONE
            }
        }
        recentPasses.clear()
    }

    fun onBuyTicketsButtonClick(
        analyticsPlatformAdapter: AnalyticsPlatformAdapter,
        activity: UseTicketsActivity
    ) {
        activity.startActivity(Intent(activity, BuyTicketsActivity::class.java)
            .apply {
                putExtra(
                    AppConstants.EXTRA_ORIGIN,
                    AnalyticsPlatformsContract.Screen.USE_TICKETS
                )
            })
    }

    fun populatePassesInAdapter(
        passes: List<Pass>,
        useTicketsAdapterNew: UseTicketsAdapterNew
    ) {
        useTicketsAdapterNew.setPasses(passes)
        useTicketsAdapterNew.notifyDataSetChanged()
    }

    fun fetchPasses(
        passType: PassType,
        status: String,
        presenter: UseTickets.Presenter,
        activity: UseTicketsActivity,
        emptyStates: EmptyStates
    ) {
        if (BytemarkSDK.isLoggedIn()) {
            //swipeRefreshLayout.setEnabled(true);
            presenter.loadContent(passType, status, "100", "")
        } else {
            //swipeRefreshLayout.setEnabled(false);
            showLoginView(emptyStates, activity)
        }
    }

    fun showLoginView(
        emptyStates: EmptyStates,
        activity: UseTicketsActivity
    ) {
        emptyStates.activeTicketsText?.visibility = GONE
        emptyStates.linearLayoutLogin?.visibility = VISIBLE
        emptyStates.linearLayoutNoTickets?.visibility = INVISIBLE
        emptyStates.linearLayoutNoVirtualFareMedium?.visibility = INVISIBLE
        emptyStates.linearLayoutNetworkDown?.visibility = INVISIBLE
        emptyStates.loadingUseTickets?.visibility = INVISIBLE

        emptyStates.linearLayoutNoLoggedInText?.sendAccessibilityEvent(TYPE_VIEW_FOCUSED)
        emptyStates.linearLayoutNoLoggedInText?.announceForAccessibility(activity.getString(R.string.you_are_signed_out))
    }

    fun onOffline(
        useTicketsAdapterNew: UseTicketsAdapterNew,
        presenter: UseTickets.Presenter,
        passType: PassType,
        status: String,
        emptyStates: EmptyStates,
        activity: UseTicketsActivity
    ) {
        if (BytemarkSDK.isLoggedIn()) {
            //swipeRefreshLayout.setEnabled(true);
            if (useTicketsAdapterNew.isEmpty) {
                showNetworkConnectionErrorView(emptyStates, activity)
            }
            // Handle the case where user initially comes to screen with offline mode.
            // We should load content in that case.
            presenter.loadContent(passType, status, "100", "")
        } else {
            showLoginView(emptyStates, activity)
        }
    }

    fun onResume(
        lastPauseMs: Long,
        navigateAwayReason: Int,
        presenter: UseTickets.Presenter,
        swipeRefreshLayout: BmSwipeRefreshLayout,
        fetchPasses: () -> Unit
    ) {
        if (navigateAwayReason != AppConstants.NAVIGATION_TICKET_DETAILS && currentTimeMillis() - lastPauseMs >= CACHE_INVALIDATION_TIMEOUT_MS) {
            fetchPasses()
        } else {
            when (navigateAwayReason) {
                V3 -> {
                    fetchPasses()
                }
                BUY_TICKETS -> {
                    //fetchPasses()
                }
                SIGN_IN, NOTIFICATIONS -> if (BytemarkSDK.isLoggedIn()) {
                    Timber.d("User signed in newly")
                    //fetchPasses()
                }
                NONE -> Timber.i("Resuming normally")
            }
        }
        presenter.loadNotifications()
    }

    fun showNetworkConnectionErrorView(
        emptyStates: EmptyStates,
        activity: UseTicketsActivity
    ) {
        emptyStates.linearLayoutNetworkDown?.visibility = VISIBLE
        emptyStates.linearLayoutNoTickets?.visibility = INVISIBLE
        emptyStates.linearLayoutNoVirtualFareMedium?.visibility = INVISIBLE
        emptyStates.loadingUseTickets?.visibility = INVISIBLE
        emptyStates.linearLayoutLogin?.visibility = INVISIBLE
        emptyStates.linearLayoutNoNetworkText?.sendAccessibilityEvent(TYPE_VIEW_FOCUSED)
        emptyStates.linearLayoutNoNetworkText?.announceForAccessibility(activity.getString(R.string.network_connectivity_error))
    }

    fun hideNetworkConnectionErrorView(linearLayoutNetworkDown: LinearLayout) {
        linearLayoutNetworkDown.visibility = INVISIBLE
    }

    fun showDeviceTimeErrorDialog(
        activity: UseTicketsActivity, confHelper: ConfHelper,
        emptyStates: EmptyStates
    ) {
        MaterialDialog.Builder(activity)
            .title(R.string.popup_error)
            .content(R.string.device_time_error_message)
            .positiveText(R.string.ok)
            .positiveColor(confHelper.dataThemeAccentColor)
            .cancelable(false)
            .onPositive { _, _ -> showLoginView(emptyStates, activity) }
            .show()
    }

    fun showSessionExpiredErrorDialog(
        activity: UseTicketsActivity, confHelper: ConfHelper,
        message: String,
        emptyStates: EmptyStates
    ) {
        MaterialDialog.Builder(activity)
            .title(R.string.popup_error)
            .content(message)
            .cancelable(false)
            .positiveText(R.string.ok)
            .positiveColor(confHelper.dataThemeAccentColor)
            .onPositive { dialog, _ ->
                dialog.dismiss()
                showLoginView(emptyStates, activity)
            }
            .show()
    }

    fun disableSwipeRefresh(swipeRefreshLayout: BmSwipeRefreshLayout) {
        swipeRefreshLayout.isEnabled = false
        swipeRefreshLayout.isRefreshing = false
    }

    fun enableSwiperefresh(swipeRefreshLayout: BmSwipeRefreshLayout) {
        swipeRefreshLayout.isEnabled = true
        swipeRefreshLayout.isRefreshing = true
    }

    fun clearPassesListAndHideButton(passes: MutableList<Pass>, button: Button) {
        if (passes.isNotEmpty()) {
            passes.clear()
        }
        button.visibility = GONE
    }

    fun setSwipeRefreshLayoutColorSchemes(
        swipeRefreshLayout: BmSwipeRefreshLayout,
        confHelper: ConfHelper
    ) {
        swipeRefreshLayout.setColorSchemeColors(
            confHelper.accentThemeBacgroundColor,
            confHelper.headerThemeAccentColor,
            confHelper.backgroundThemeBackgroundColor,
            confHelper.dataThemeAccentColor
        )
    }

    fun showLoading(emptyStates: EmptyStates) {
        emptyStates.loadingUseTickets?.visibility = VISIBLE
        emptyStates.linearLayoutNetworkDown?.visibility = INVISIBLE
        emptyStates.linearLayoutNoTickets?.visibility = INVISIBLE
        emptyStates.linearLayoutNoVirtualFareMedium?.visibility = INVISIBLE
        emptyStates.linearLayoutLogin?.visibility = INVISIBLE
    }

    fun hideLoading(emptyStates: EmptyStates) {
        emptyStates.activeTicketsText?.visibility = VISIBLE
        emptyStates.linearLayoutLogin?.visibility = INVISIBLE
        emptyStates.loadingUseTickets?.visibility = INVISIBLE
    }

    fun showNoTicketsView(emptyStates: EmptyStates) {
        emptyStates.linearLayoutNoTickets?.visibility = VISIBLE
        emptyStates.activeTicketsLayout?.visibility = GONE
        emptyStates.linearLayoutNoVirtualFareMedium?.visibility = INVISIBLE
        emptyStates.linearLayoutNetworkDown?.visibility = INVISIBLE
        emptyStates.loadingUseTickets?.visibility = INVISIBLE
        emptyStates.linearLayoutLogin?.visibility = INVISIBLE
        emptyStates.linearLayoutNoTicketsText?.sendAccessibilityEvent(TYPE_VIEW_FOCUSED)
    }

    fun showDeviceLostOrStolenErrorDialog(
        context: Context,
        onDeviceLostOrStolenButtonClick: () -> Unit
    ) {
        if ((context as UseTicketsActivity).deviceStolenFlag) {
            (context as UseTicketsActivity).deviceStolenFlag = false
            MaterialDialog.Builder(context)
                .title(R.string.device_lost_stolen_title)
                .content(R.string.device_lost_stolen_body)
                .cancelable(false)
                .positiveText(R.string.ok)
                .onPositive { dialog, which ->
                    (context as UseTicketsActivity).deviceStolenFlag = true
                    onDeviceLostOrStolenButtonClick()
                    dialog.dismiss()
                }
                .show()
        }
    }

    fun showAppUpdateDialog(context: Context, fragmentName: String, fragment: Fragment) {
        if ((context as UseTicketsActivity).isShouldShowAppUpdateDialog) {
            var useTicketFragment: Fragment = fragment
            BytemarkSDK.logout(context)
            (context as UseTicketsActivity).isShouldShowAppUpdateDialog = false
            val alertDialog = AlertDialog.Builder(context)
            alertDialog.setTitle(R.string.popup_update_required_title)
            alertDialog.setMessage(R.string.popup_update_required_message)
            alertDialog.setIcon(android.R.drawable.ic_dialog_alert)
            alertDialog.setPositiveButton(R.string.update) { _, _ ->
                (context as UseTicketsActivity).isShouldShowAppUpdateDialog = true
                if (fragmentName.equals(AppConstants.USE_TICKETS_AVAILABLE_FRAGMENT_TYPE, true)) {
                    (useTicketFragment as UseTicketsAvailableFragment).showLoginView()
                } else if (fragmentName.equals(
                        AppConstants.USE_TICKETS_ACTIVE_FRAGMENT_TYPE,
                        true
                    )
                ) {
                    (useTicketFragment as UseTicketsActiveFragment).showLoginView()
                }
                val goToMarket = Intent(Intent.ACTION_VIEW)
                    .setData(Uri.parse("market://details?id=" + context.packageName))
                context.startActivity(goToMarket)
            }
            alertDialog.setNegativeButton(R.string.exit) { dialog, which ->
                (context as UseTicketsActivity).isShouldShowAppUpdateDialog = true
                dialog.cancel()
                if (fragmentName.equals(AppConstants.USE_TICKETS_AVAILABLE_FRAGMENT_TYPE, true)) {
                    (useTicketFragment as UseTicketsAvailableFragment).showLoginView()
                } else if (fragmentName.equals(
                        AppConstants.USE_TICKETS_ACTIVE_FRAGMENT_TYPE,
                        true
                    )
                ) {
                    (useTicketFragment as UseTicketsActiveFragment).showLoginView()
                }
            }
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
    }

    internal interface NotificationsListener {
        fun onUnreadNotificationsCountChanged(unreadNotificationsCount: Int)
    }

    fun assignPassToRepurchase(pass: Pass) {
        passToRepurchase = pass
    }

    fun setupBottomSheet(
        moreOptionsBottomSheetDialog: BottomSheetDialog,
        activity: Activity,
        fragment: Fragment,
        fragmentType: String,
        enableTransferPassInUseTicket: Boolean
    ) {
        val bottomSheetView =
            LayoutInflater.from(activity).inflate(R.layout.more_options_bottom_sheet, null)
        repurchase = bottomSheetView.findViewById<TextView>(R.id.repurchase)
        moveToCloud = bottomSheetView.findViewById<TextView>(R.id.move_to_cloud)
        sendTicket = bottomSheetView.findViewById<TextView>(R.id.send_ticket)

        // setting up repurchase
        repurchase = bottomSheetView.findViewById<TextView>(R.id.repurchase)
        setupRepurchaseClickListener(
            repurchase,
            moreOptionsBottomSheetDialog,
            activity,
            fragmentType
        )

        // setting up transfer pass
        if (fragmentType == "available") {
            setupSendTicketListener(sendTicket, moreOptionsBottomSheetDialog, activity)
        } else {
            // send tickets is not for "active tickets" fragment
            hideSendTicketLabel()
        }

        if (enableTransferPassInUseTicket) {
            if (fragmentType == AppConstants.USE_TICKETS_AVAILABLE_FRAGMENT_TYPE) {
                setupMoveToCloudClickListener(
                    moveToCloud,
                    moreOptionsBottomSheetDialog,
                    activity,
                    fragment as UseTicketsAvailableFragment
                )
            } else if (fragmentType == AppConstants.USE_TICKETS_ACTIVE_FRAGMENT_TYPE) {
                setupMoveToCloudClickListener(
                    moveToCloud,
                    moreOptionsBottomSheetDialog,
                    activity,
                    fragment as UseTicketsActiveFragment
                )
            }
        } else {
            moveToCloud.visibility = GONE
        }

        resendReceipt = bottomSheetView.findViewById(R.id.resend_receipt)
        setupResendReceiptClickListener(
            resendReceipt,
            moreOptionsBottomSheetDialog,
            fragment,
            activity
        )

        viewTicketDetails = bottomSheetView.findViewById(R.id.view_ticket_details)
        setupViewTicketDetailsListener(
            viewTicketDetails,
            moreOptionsBottomSheetDialog,
            activity,
            fragmentType
        )

        moreOptionsBottomSheetDialog.setContentView(bottomSheetView)
    }

    fun showSendTicketLabel() {
        sendTicket.visibility = VISIBLE
    }

    fun hideSendTicketLabel() {
        sendTicket.visibility = GONE
    }

    fun hideMoveToCloudLabel() {
        moveToCloud.visibility = GONE
    }

    fun showMoveToCloudLabel() {
        moveToCloud.visibility = VISIBLE
    }

    fun hideRepurchaseLabel() {
        repurchase.visibility = GONE
    }

    fun showRepurchaseLabel() {
        repurchase.visibility = VISIBLE
    }

    fun showViewTicketDetailsLabel() {
        viewTicketDetails.visibility = VISIBLE
    }

    fun hideResendReceipt() {
        resendReceipt.visibility = GONE
    }

    fun showResendReceipt() {
        resendReceipt.visibility = VISIBLE
    }

    private fun isUnlimitedActivePassExpired(pass: Pass?): Boolean {
        pass?.let {
            return (pass.isUnlimitedPassActivated && TextUtils.isEmpty(pass!!.lockedToDevice))
        }
        return false
    }

    fun setMoveToCloudText(text: String) {
        moveToCloud.text = text
    }

    fun setPassType(passType: Int) {
        this.passType = passType
    }

    private fun setupRepurchaseClickListener(
        repurchase: TextView,
        moreOptionsBottomSheetDialog: BottomSheetDialog,
        context: Context,
        fragmentType: String
    ) {
        repurchase.setOnClickListener { v ->
            moreOptionsBottomSheetDialog.hide()
            startBuyTicketsActivity(context, fragmentType)
        }
    }

    private fun setupViewTicketDetailsListener(
        viewTicketDetails: TextView,
        moreOptionsBottomSheetDialog: BottomSheetDialog,
        context: Context,
        fragmentType: String
    ) {
        viewTicketDetails.setOnClickListener {
            (context as UseTicketsActivity).setNavigateAwayReason(
                AppConstants.NAVIGATION_TICKET_DETAILS,
                fragmentType
            )
            moreOptionsBottomSheetDialog.dismiss()
            starTicketDetailsActivity(context, fragmentType)
        }
    }

    private fun starTicketDetailsActivity(context: Context, fragmentType: String) {
        passToRepurchase?.let {
            val i = Intent(context, TicketDetailsActivity::class.java)
            i.putExtra("pass", it)
            i.putExtra(AppConstants.USE_TICKETS_FRAGMENT_TYPE, fragmentType)
            context.startActivity(i)
        }
    }

    private fun startBuyTicketsActivity(context: Context, fragmentType: String) {

        passToRepurchase?.let {
            context.startActivity(createBuyTicketsIntentWithExtrasAdded(context, fragmentType))
        }
    }

    private fun createBuyTicketsIntentWithExtrasAdded(
        context: Context,
        fragmentType: String
    ): Intent {
        val i = Intent(context, BuyTicketsActivity::class.java)
        i.putExtra("product_uuid_repurchase", passToRepurchase?.productUuid)
        i.putExtra("filter_uuid_origin", passToRepurchase?.getFilterUUID("origin"))
        i.putExtra("filter_uuid_dest", passToRepurchase?.getFilterUUID("destination"))
        i.putExtra("origin_short_name", passToRepurchase?.getFilterAttribute("origin", false))
        i.putExtra("dest_short_name", passToRepurchase?.getFilterAttribute("destination", false))
        i.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.REPURCHASE)
        i.putExtra(AppConstants.USE_TICKETS_FRAGMENT_TYPE, fragmentType)
        i.putExtra(AppConstants.INTENT_FOR_REPURCHASE, true)
        i.putExtra(AppConstants.PRODUCT_NAME, passToRepurchase?.productLabelName)
        return i
    }

    private fun setupMoveToCloudClickListener(
        moveToCloud: TextView,
        moreOptionsBottomSheetDialog: BottomSheetDialog,
        activity: Activity,
        fragment: Fragment
    ) {
        moveToCloud.setOnClickListener { v ->
            moreOptionsBottomSheetDialog.hide()
            movePassesToCloud(activity, fragment)
        }
    }

    private fun setupResendReceiptClickListener(
        resendReceipt: TextView,
        moreOptionsBottomSheetDialog: BottomSheetDialog,
        fragmentType: Fragment,
        activity: Activity
    ) {
        resendReceipt.setOnClickListener {
            moreOptionsBottomSheetDialog.dismiss()
            resendReceipt(fragmentType, activity)
        }
    }

    private fun movePassesToCloud(
        activity: Activity,
        fragment: Fragment
    ) {
        passToRepurchase?.let {
            UseTickets.Presenter().transferPass(activity, passToRepurchase!!, passType, fragment)
        }
    }

    private fun resendReceipt(fragment: Fragment, activity: Activity) {
        resendReceipt?.let {
            passToRepurchase?.orderItem?.orderUuid?.let { orderUUID ->
                if (fragment is TicketHistoryFragment)
                    fragment.resendReceipt(orderUUID)
                else
                    UseTickets.Presenter().resendReceipt(activity, orderUUID, fragment)
            }
        }
    }

    private fun setupSendTicketListener(
        sendTicket: TextView,
        moreOptionsBottomSheetDialog: BottomSheetDialog,
        activity: Activity
    ) {
        sendTicket.setOnClickListener {
            moreOptionsBottomSheetDialog.dismiss()
            showSendTicketDialog(activity);
        }
    }

    private fun showSendTicketDialog(activity: Activity) {
        passToRepurchase?.let {
            val dialogFragment = SendTicketToUserDialog.newInstance(it.uuid, it.productLabelName)
            val fragmentManager = (activity as AppCompatActivity).supportFragmentManager
            dialogFragment.show(fragmentManager, "SendTicket")
        }
    }
}