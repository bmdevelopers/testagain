package co.bytemark.use_tickets

import android.content.res.ColorStateList
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import co.bytemark.helpers.ConfHelper
import co.bytemark.widgets.CircleBorderImageView
import co.bytemark.widgets.ProgressViewLayout

class ActiveAndAvailableUIThemes(val confHelper: ConfHelper,
                                 val buttonBuyTickets: Button?,
                                 val buttonBuyTicketsNoTickets: Button?,
                                 val buttonNoFareMedium: Button?,
                                 val textRefreshTickets: TextView?,
                                 val buttonSignIn: Button?,
                                 val buttonRetry: Button?,
                                 val buttonGotIt: Button?,
                                 val buttonShowMeLater: Button?,
                                 var imageViewNoTickets: CircleBorderImageView?,
                                 var imageViewLoggedOut: CircleBorderImageView?,
                                 var imageViewLoadingTickets: ImageView?,
                                 var imageViewNetworkError: CircleBorderImageView?,
                                 var imageViewNoFareMedium: CircleBorderImageView?,
                                 var progressViewLayout: ProgressViewLayout?,
                                 var loadingUseTickets: LinearLayout?,
                                 var linearLayoutNoTickets: LinearLayout?,
                                 var linearLayoutNoVirtualFareMedium: LinearLayout?,
                                 var linearLayoutNetworkDown: LinearLayout?,
                                 var linearLayoutTicketStorage: LinearLayout?) {

    data class Builder(var confHelper: ConfHelper? = null,
                       var buttonBuyTickets: Button? = null,
                       var buttonBuyTicketsNoTickets: Button? = null,
                       var buttonNoFareMedium: Button? = null,
                       var textRefreshTickets: TextView? = null,
                       var buttonSignIn: Button? = null,
                       var buttonRetry: Button? = null,
                       var buttonGotIt: Button? = null,
                       var buttonShowMeLater: Button? = null,
                       var imageViewNoTickets: CircleBorderImageView? = null,
                       var imageViewLoggedOut: CircleBorderImageView? = null,
                       var imageViewLoadingTickets: ImageView? = null,
                       var imageViewNetworkError: CircleBorderImageView? = null,
                       var imageViewNoFareMedium: CircleBorderImageView? = null,
                       var progressViewLayout: ProgressViewLayout? = null,
                       var loadingUseTickets: LinearLayout? = null,
                       var linearLayoutNoTickets: LinearLayout? = null,
                       var linearLayoutNoVirtualFareMedium: LinearLayout? = null,
                       var linearLayoutNetworkDown: LinearLayout? = null,
                       var linearLayoutTicketStorage: LinearLayout? = null) {

        fun addConfHelper(confHelper: ConfHelper)  = apply {
                this.confHelper = confHelper
            }

        fun buyTicketsButton(buttonBuyTickets: Button) = apply {
                this.buttonBuyTickets = buttonBuyTickets
            }

        fun buyTicketsNoTicketsButton(buttonBuyTicketsNoTickets: Button) = apply {
                this.buttonBuyTicketsNoTickets = buttonBuyTicketsNoTickets
            }

        fun noFareMediumButton(buttonNoFareMedium: Button) = apply {
                this.buttonNoFareMedium = buttonNoFareMedium
            }

        fun refreshTicketsButton(buttonRefreshTickets: TextView) = apply {
                this.textRefreshTickets = buttonRefreshTickets
            }

        fun signInButton(buttonSignIn: Button) = apply {
                this.buttonSignIn = buttonSignIn
            }

        fun retryButton(buttonRetry: Button) = apply {
                this.buttonRetry = buttonRetry
            }

        fun gotItButton(buttonGotIt: Button) = apply {
                this.buttonGotIt = buttonGotIt
            }

        fun showMeLaterButton(buttonShowMeLater: Button) = apply {
                this.buttonShowMeLater = buttonShowMeLater
            }

        fun noTicketsImageView(imageViewNoTickets: CircleBorderImageView) = apply {
            this.imageViewNoTickets = imageViewNoTickets
        }

        fun loggedOutImageView(imageViewLoggedOut: CircleBorderImageView) = apply {
            this.imageViewLoggedOut = imageViewLoggedOut
        }

        fun loadingTicketsImageView(imageViewLoadingTickets: ImageView) = apply {
            this.imageViewLoadingTickets = imageViewLoadingTickets
        }

        fun networkErrorImageView(imageViewNetworkError: CircleBorderImageView) = apply {
            this.imageViewNetworkError = imageViewNetworkError
        }

        fun noFareMediumImageView(imageViewNoFareMedium: CircleBorderImageView) = apply {
            this.imageViewNoFareMedium = imageViewNoFareMedium
        }

        fun layoutProgressView(progressViewLayout: ProgressViewLayout) = apply {
            this.progressViewLayout = progressViewLayout
        }

        fun useticketsLoading(loadingUseTickets: LinearLayout) = apply {
            this.loadingUseTickets = loadingUseTickets
        }

        fun noTicketsLayout(linearLayoutNoTickets: LinearLayout) = apply {
            this.linearLayoutNoTickets = linearLayoutNoTickets
        }

        fun noVirtualFareMediumLayout(linearLayoutNoVirtualFareMedium: LinearLayout) = apply {
            this.linearLayoutNoVirtualFareMedium = linearLayoutNoVirtualFareMedium
        }

        fun networkDownLayout(linearLayoutNetworkDown: LinearLayout) = apply {
            this.linearLayoutNetworkDown = linearLayoutNetworkDown
        }

        fun ticketStorageLayout(linearLayoutTicketStorage: LinearLayout) = apply {
            this.linearLayoutTicketStorage = linearLayoutTicketStorage
        }

        fun build() = ActiveAndAvailableUIThemes(confHelper!!,
                    buttonBuyTickets,
                    buttonBuyTicketsNoTickets,
                    buttonNoFareMedium,
                    textRefreshTickets,
                    buttonSignIn,
                    buttonRetry,
                    buttonGotIt,
                    buttonShowMeLater,
                    imageViewNoTickets,
                    imageViewLoggedOut,
                    imageViewLoadingTickets,
                    imageViewNetworkError,
                    imageViewNoFareMedium,
                    progressViewLayout,
                    loadingUseTickets,
                    linearLayoutNoTickets,
                    linearLayoutNoVirtualFareMedium,
                    linearLayoutNetworkDown,
                    linearLayoutTicketStorage)
    }

    fun setAccentThemeColors() {
        buttonBuyTickets?.setTextColor(confHelper.backgroundThemeSecondaryTextColor)
        //buttonBuyTickets.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        buttonBuyTicketsNoTickets?.setTextColor(confHelper.backgroundThemeSecondaryTextColor)
        buttonBuyTicketsNoTickets?.backgroundTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        buttonNoFareMedium?.setTextColor(confHelper.backgroundThemeSecondaryTextColor)
        buttonNoFareMedium?.backgroundTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        //textRefreshTickets.setTextColor(confHelper.pri);
        textRefreshTickets?.backgroundTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        buttonSignIn?.setTextColor(confHelper.backgroundThemeSecondaryTextColor)
        buttonSignIn?.backgroundTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        buttonRetry?.setTextColor(confHelper.backgroundThemeSecondaryTextColor)
        buttonRetry?.backgroundTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        buttonGotIt?.setTextColor(confHelper.backgroundThemeSecondaryTextColor)
        buttonGotIt?.backgroundTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        buttonShowMeLater?.setTextColor(confHelper.backgroundThemeAccentColor)
    }

    fun setBackgroundThemeColors() {
        imageViewNoTickets?.imageTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        imageViewLoggedOut?.imageTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        imageViewLoadingTickets?.imageTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        imageViewNetworkError?.imageTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        imageViewNoFareMedium?.imageTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        progressViewLayout?.setProgressColor(confHelper.backgroundThemeAccentColor, confHelper.backgroundThemeBackgroundColor)

        loadingUseTickets?.setBackgroundColor(confHelper.backgroundThemeBackgroundColor)
        linearLayoutNoTickets?.setBackgroundColor(confHelper.backgroundThemeBackgroundColor)
        linearLayoutNoVirtualFareMedium?.setBackgroundColor(confHelper.backgroundThemeBackgroundColor)
        linearLayoutNetworkDown?.setBackgroundColor(confHelper.backgroundThemeBackgroundColor)
        linearLayoutTicketStorage?.setBackgroundColor(confHelper.backgroundThemeBackgroundColor)
    }
}
