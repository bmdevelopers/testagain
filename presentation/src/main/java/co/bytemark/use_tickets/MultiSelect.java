package co.bytemark.use_tickets;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.helpers.RxEventBus;
import co.bytemark.mvp.MvpBasePresenter;
import co.bytemark.mvp.MvpView;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.GetRecentActivePasses.GetRecentActivePassesCallback;
import co.bytemark.sdk.Pass;
import co.bytemark.sdk.PassesActivationCalculator;
import co.bytemark.sdk.network_impl.BMNetwork;
import co.bytemark.use_tickets.UseTicketsEvents.ForceReload;
import co.bytemark.use_tickets.UseTicketsEvents.QueueReload;
import co.bytemark.use_tickets.UseTicketsEvents.VptDownloaded;
import rx.subscriptions.CompositeSubscription;

public interface MultiSelect {

    interface View extends MvpView {

        Context getActivityContext();

        void setRecentActivePasses(List<Pass> passes);

        void updatePass(@NonNull Pass pass);
    }

    class Presenter extends MvpBasePresenter<View> implements GetRecentActivePassesCallback {

        private final BMNetwork bmNetwork;

        private final CompositeSubscription subs = new CompositeSubscription();

        @Inject
        RxEventBus eventBus;

        @Inject
        AnalyticsPlatformAdapter analyticsPlatformAdapter;

        String str;

        @Inject
        public Presenter(BMNetwork bmNetwork, RxEventBus eventBus) {
            this.bmNetwork = bmNetwork;
            this.eventBus = eventBus;

            subs.add(eventBus
                    .observeEvents(VptDownloaded.class)
                    .subscribe(vptDownloaded -> onPassVPTUpdated(vptDownloaded.pass)));
        }

        /**
         * Checks if the current focused pass can be enabled for selection.
         *
         * @param focusedPass The current pass in focus.
         * @return If the focused pass can be enabled for selection.
         */
        boolean hasRequiredEnablers(@NonNull Pass focusedPass, List<Pass> selectedPasses) {
            selectedPasses.remove(focusedPass);
            return PassesActivationCalculator.hasRequiredEnablers(focusedPass, selectedPasses);
        }

        void displayTickets(Activity activity, List<Pass> passes) {
            int noOfTickets = passes.size();
            boolean hasEnablers = false;
            boolean hasCloudPass = false;
            for (Pass pass : passes) {
                if (!pass.getProductEnablers().isEmpty()) {
                    hasEnablers = true;
                }
                if (pass.getLockedToDevice().isEmpty()) {
                    hasCloudPass = true;
                }
            }

            // Trigger activate in SDK
            BytemarkSDK.SDKUtility.displayTickets(activity, new ArrayList<>(passes));
            queueUseTicketsReload();
        }

        /**
         * Queues a reload on {@link UseTicketsFragment} so that when the user navigates to the page,
         * content is loaded freshly.
         */
        void queueUseTicketsReload() {
            eventBus.postEvent(new QueueReload());
        }

        /**
         * Force reload use tickets.
         */
        void forceReloadUseTickets() {
            eventBus.postEvent(new ForceReload());
        }

        boolean isAllTicketsUsing(List<Pass> selectedPasses) {
            for (Pass pass : selectedPasses) {
                if (!pass.getStatus().equalsIgnoreCase("USING")) {
                    return false;
                }
            }
            return true;
        }

        void loadRecentActivePasses() {
            if (isViewAttached()) {
                bmNetwork.getRecentActivePasses(getView().getActivityContext(), this);
            }
        }

        @Override
        public void setRecentActivePasses(List<Pass> passes) {
            if (isViewAttached()) {
                getView().setRecentActivePasses(passes);
            }
        }

        public void cleanUp() {
            subs.clear();
        }

        private void onPassVPTUpdated(@NonNull Pass pass) {
            if (isViewAttached()) {
                getView().updatePass(pass);
            }
        }
    }

}
