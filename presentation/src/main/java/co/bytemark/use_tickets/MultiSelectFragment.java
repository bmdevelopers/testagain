package co.bytemark.use_tickets;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.util.Pair;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.Button;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.base.BaseMvpFragment;
import co.bytemark.sdk.Pass;
import co.bytemark.userphoto.AccountPhotoActivity;
import co.bytemark.widgets.CircleBorderImageView;
import co.bytemark.widgets.snaphelper.SnappablePagerSnapHelper;
import co.bytemark.widgets.util.Util;
import icepick.State;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

import static android.content.res.ColorStateList.valueOf;
import static androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL;
import static android.view.Gravity.CENTER;
import static android.view.Gravity.END;
import static android.view.Gravity.TOP;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.view.accessibility.AccessibilityEvent.CONTENT_CHANGE_TYPE_TEXT;
import static co.bytemark.domain.model.userphoto.UserPhoto.MISSING;
import static co.bytemark.domain.model.userphoto.UserPhoto.REJECTED;
import static co.bytemark.sdk.Pass.PHOTO_REQUIRED;

public class MultiSelectFragment extends BaseMvpFragment<MultiSelect.View, MultiSelect.Presenter> implements MultiSelect.View, BackHandler {
    private static final String EXTRA_PASSES = "EXTRA_PASSES";
    private static final String EXTRA_PASS_INDEX = "EXTRA_PASS_INDEX";
    private static final String EXTRA_PHOTO_STATUS = "EXTRA_PHOTO_STATUS";

    public static final String ACTION_QUEUE_RELOAD_USE_TICKETS = "ACTION_QUEUE_RELOAD_USE_TICKETS";

    @BindView(R.id.pass_overlay_image_view)
    ImageView passOverlayImageView;
    @BindView(R.id.pass_recycler_view)
    RecyclerView passRecyclerView;
    @BindView(R.id.multi_select_root)
    NestedScrollView multiSelectRoot;
    @BindView(R.id.hold_to_activate_view)
    CircleBorderImageView supplementaryIcon;
    @BindView(R.id.focus_description)
    TextView supplementaryDescription;
    @BindView(R.id.supplementary_layout)
    RelativeLayout supplementaryLayout;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.btn_upload_photo)
    Button buttonPhoto;

    Bitmap overlayBitmap;
    @State
    int passIndex;
    @State
    String photoStatus;

    private MultiSelectPassAdapter multiSelectPassAdapter;
    private SnappablePagerSnapHelper snapHelper;

    private final CompositeSubscription subs = new CompositeSubscription();
    private final List<Pass> recentActivePasses = new ArrayList<>();

    @Inject
    MultiSelect.Presenter presenter;

    public MultiSelectFragment() {
    }

    @Override
    public MultiSelect.Presenter createPresenter() {
        return presenter;
    }

    public static MultiSelectFragment newInstance(ArrayList<Pass> passes, int absoluteIndex, String userPhotoStatus) {
        final MultiSelectFragment fragment = new MultiSelectFragment();
        final Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(EXTRA_PASSES, passes);
        bundle.putInt(EXTRA_PASS_INDEX, absoluteIndex);
        bundle.putString(EXTRA_PHOTO_STATUS, userPhotoStatus);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void setOverlayBitmap(Bitmap overlayBitmap) {
        this.overlayBitmap = overlayBitmap;
    }

    @Override
    protected void onInject() {
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            passIndex = getArguments().getInt(EXTRA_PASS_INDEX);
            List<Pass> passes = getArguments().getParcelableArrayList(EXTRA_PASSES);
            if (passes == null) {
                passes = new ArrayList<>(); // Fail safe
            }
            photoStatus = getArguments().getString(EXTRA_PHOTO_STATUS, MISSING);
            multiSelectPassAdapter = new MultiSelectPassAdapter(passes);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupTransitionOverlay();
        setupRecyclerView();
        setupActivateTickets();
        presenter.loadRecentActivePasses();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_multi_select;
    }

    public int getPassesCount() {
        if (multiSelectPassAdapter != null) {
            return multiSelectPassAdapter.getItemCount();
        }
        return 0;
    }


    private void setupTransitionOverlay() {
        passOverlayImageView.setImageBitmap(overlayBitmap);
        final LayoutParams params = (LayoutParams) passOverlayImageView.getLayoutParams();
        if (passIndex == multiSelectPassAdapter.getItemCount() - 1) {
            params.gravity = END | TOP;
        } else if (passIndex > 0) {
            params.gravity = CENTER | TOP;
        }
    }

    private void setupRecyclerView() {
        passRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), HORIZONTAL, false));

        snapHelper = new SnappablePagerSnapHelper(passRecyclerView);
        subs.add(snapHelper.snaps().subscribe(this::onPassSnapped));
        multiSelectPassAdapter.setSnapLookup(() -> snapHelper.getCurrentSnapPosition());

        passRecyclerView.setAdapter(multiSelectPassAdapter);

        subs.add(multiSelectPassAdapter.passClicks().subscribe(this::onPassClicked));

        subs.add(multiSelectPassAdapter.selectionChanges().subscribe(this::onPassSelectionChanged));

        subs.add(multiSelectPassAdapter.snapRequests().subscribe(integer -> snapHelper.snapTo(integer, null)));
    }

    private void setupActivateTickets() {
        subs.add(RxView.clicks(supplementaryIcon)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(a -> onActivateClicked()));
    }

    @Override
    protected void onOnline() {

    }

    @Override
    protected void onOffline() {

    }

    @Override
    protected void setAccentThemeColors() {
        buttonPhoto.setTextColor(confHelper.getBackgroundThemeSecondaryTextColor());
        buttonPhoto.setBackgroundTintList(valueOf(confHelper.getBackgroundThemeAccentColor()));
    }

    @Override
    protected void setBackgroundThemeColors() {
        multiSelectRoot.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        supplementaryIcon.setImageResource(R.drawable.circle_drawable);
        supplementaryIcon.setBorderStrokeWidth(Util.dpToPx(1));
        supplementaryIcon.setImageTintList(valueOf(confHelper.getBackgroundThemeAccentColor()));
        supplementaryDescription.setTextColor(confHelper.getBackgroundThemeAccentColor());
        fab.setBackgroundTintList(valueOf(confHelper.getIndicatorsSuccess()));
        fab.setImageTintList(valueOf(Color.WHITE));
    }

    @Override
    protected void setDataThemeColors() {

    }


    private void onPassClicked(int position) {
        if (isAdded()) {
            Timber.d("Position clicked %d", position);
            if (multiSelectPassAdapter.getSelectedPasses().isEmpty()) {
                goBack();
            }
        }
    }

    /**
     * Called when a pass is snapped during scrolling. When that happens, we should update the pass
     * enabler state and supplementary for that pass.
     * <p>
     * Since two other passes are also partially visible on the sides, we update the enabler state
     * alone for that pass.
     *
     * @param position Position of the item that was snapped.
     */
    private void onPassSnapped(int position) {
        if (!isAdded()) {
            return;
        }
        Timber.d("onSnapped %d", position);
        passIndex = position;

        // For left and right passes, just update the pass overlay and not the supplementary view.
        updatePassEnablerState(position - 1, false);
        updatePassEnablerState(position + 1, false);

        // Update the pass user is currently seeing.
        updatePassEnablerState(position, true);
    }

    /**
     * Called when user makes any changes to selections in adapter. This means that already selected
     * passes have to be invalidated. So we get all the selected indices and do a check if their
     * selection is valid and then update their disabled state.
     * <p>
     * We don't have much UI to update other than checking enabler state and supplementary view.
     * Since we already have that logic in {@link #onPassSnapped(int)}, we just forward the call to
     * there and force an update.
     *
     * @param selection A pair containing selected status and the position of change.
     */
    private void onPassSelectionChanged(Pair<Boolean, Integer> selection) {
        Timber.d("Selection changed: %s", selection.toString());

        if (!isAdded()) {
            return;
        }

        // If this was an unselection we need to invalidate already selected tickets.
        if (!selection.first) {
            // Get all currently selected indices and force an update.
            for (int i : multiSelectPassAdapter.getSelectedIndices()) {
                updatePassEnablerState(i, false);
            }
        }

        // Finally update ones the user is currently seeing.
        final int snappedIndex = snapHelper.getCurrentSnapPosition();
        if (snappedIndex != -1) {
            onPassSnapped(snappedIndex);
        }

        // Accessibility crap.
        final String labelName = multiSelectPassAdapter.getPass(selection.second).getLabelName(Locale.getDefault().getLanguage());
        if (selection.first) {
            passRecyclerView.announceForAccessibility(getResources().getString(R.string.use_tickets_pass_selected_voonly, labelName));
        } else {
            passRecyclerView.announceForAccessibility(getResources().getString(R.string.use_tickets_pass_unselected_voonly, labelName));
        }
    }

    /**
     * When user clicks the activate button, we assume the enablers have passed and there is no
     * foul scenario. So we ask the presenter to activate the tickets.
     */
    private void onActivateClicked() {
        final List<Pass> selectedPasses = multiSelectPassAdapter.getSelectedPasses();
        activateTickets(selectedPasses);
    }

    /**
     * Responsible for talking to presenter and checking if the current position is enabled.
     * <p>
     * If it's not, we disable the pass and update UI via {@link #multiSelectPassAdapter}
     * <p>
     * Optionally you can specify if you want to update the supplementary view below the pass
     * as well.
     * <p>
     * Note: Only set {@param updateSupplementaryView} to true if you are giving the focused pass
     * position in {@param passPosition}, because you don't want to wrongly update the supplementary
     * view for a pass that is not currently focused.
     *
     * @param passPosition            Pass position to check for enablers.
     * @param updateSupplementaryView Whether supplementary view under the pass should be updated as
     *                                well.
     */
    private void updatePassEnablerState(int passPosition, boolean updateSupplementaryView) {
        if (!isAdded()) {
            return;
        }
        try {
            final Pass focusedPass = multiSelectPassAdapter.getPass(passPosition);
            // Check pass enabler group.
            final List<Pass> selectedPasses = multiSelectPassAdapter.getSelectedPasses();

            final boolean hasRequiredEnablers = presenter.hasRequiredEnablers(focusedPass, selectedPasses);

            if (!hasRequiredEnablers) {
                multiSelectPassAdapter.setDisabled(passPosition);
            } else {
                multiSelectPassAdapter.clearDisabled(passPosition);
            }
            if (updateSupplementaryView) {
                invalidateSupplementaryView(passPosition, !hasRequiredEnablers);
            }
        } catch (IndexOutOfBoundsException ignored) {

        }
    }

    /**
     * Given a {@link Pass} will update the supplementary view below it based on availability status.
     * <p>
     * A check is made against the no of passes that are already selected to adapt the message
     * accordingly.
     *
     * @param position        Position to update for
     * @param missingEnablers Whether the pass is missing enablers.
     */
    private void invalidateSupplementaryView(int position, boolean missingEnablers) {
        if (!isAdded()) {
            return;
        }

        supplementaryLayout.setVisibility(VISIBLE);

        final Pass focusedPass = multiSelectPassAdapter.getPass(position);

        if (focusedPass.isUnavailable() || missingEnablers) {
            supplementaryIcon.setVisibility(GONE);

            // Special case: photo status
            final boolean photoMissing = photoStatus.equalsIgnoreCase(MISSING) || photoStatus.equalsIgnoreCase(REJECTED);
            buttonPhoto.setVisibility(focusedPass.getLockedReason() == PHOTO_REQUIRED && photoMissing ? VISIBLE : GONE);

            // Default is unavailable
            int stringRes = R.string.use_tickets_supplementary_desc_unavailable;
            if (focusedPass.getLockedDescriptionRes() != -1) {
                stringRes = focusedPass.getLockedDescriptionRes();
            } else if (missingEnablers) {
                stringRes = R.string.use_tickets_supplementary_desc_enablers_missing;
            }
            supplementaryDescription.setText(stringRes);
        } else {
            // Hide photo button
            buttonPhoto.setVisibility(GONE);

            final int size = multiSelectPassAdapter.getSelectedPasses().size();
            final boolean allUsing = presenter.isAllTicketsUsing(multiSelectPassAdapter.getSelectedPasses());
            final boolean isTicketSelected = multiSelectPassAdapter.isSelected(position);

            if (!isTicketSelected) {
                supplementaryIcon.setVisibility(GONE);
                switch (focusedPass.getStatus()) {
                    case "USING":
                        supplementaryDescription.setText(R.string.use_tickets_supplementary_ask_to_select_display);
                        break;
                    case "USABLE":
                        supplementaryDescription.setText(R.string.use_tickets_supplementary_ask_to_select_activate);
                        break;
                }
            } else {
                supplementaryIcon.setVisibility(VISIBLE);
                if (allUsing) {
                    final String quantityString = getResources().getQuantityString(R.plurals.use_ticket_supplementary_desc_multiple_display, size, size);
                    supplementaryDescription.setText(quantityString);
                    supplementaryIcon.setContentDescription(quantityString);
                } else {
                    final String quantityString = getResources().getQuantityString(R.plurals.use_ticket_supplementary_desc_multiple_activation, size, size);
                    supplementaryDescription.setText(quantityString);
                    supplementaryIcon.setContentDescription(quantityString);
                }
            }
        }
        supplementaryDescription.sendAccessibilityEvent(CONTENT_CHANGE_TYPE_TEXT);
    }

    private void goBack() {
        // Clear manually
        subs.clear();
        // Try to go back.
        // Since this is called from a handler, it is unaware of the current lifecycle state.
        try {
            getFragmentManager().popBackStack();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        multiSelectPassAdapter.cleanUp();
        presenter.cleanUp();
        subs.clear();
        snapHelper.attachToRecyclerView(null);
        clearBitmap();
        super.onDestroy();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        final AnimationSet set = new AnimationSet(false);
        final Animation animation = super.onCreateAnimation(transit, enter, nextAnim);
        if (animation != null) {
            set.addAnimation(animation);
        }
        set.setAnimationListener(new AnimationListenerAdapter() {
            /**
             * Because making something disappear isn't enough; you have to bring it back.
             * <b>That's why every magic trick has a third act, the hardest part,
             * the part we call "The Prestige".</b>
             */
            @Override
            public void onAnimationEnd(Animation animation) {
                passRecyclerView.post(() -> {
                    multiSelectPassAdapter.setReferenceWidth(passOverlayImageView.getWidth());
                    passRecyclerView.scrollToPosition(passIndex);
                    passRecyclerView.post(() ->
                            // Snap to user selected pass
                            snapHelper.snapTo(passIndex, () -> {

                                if (!isAdded()) {
                                    return;
                                }

                                // Update supplementary view to default selection.
                                invalidateSupplementaryView(passIndex, false);

                                // Update selection for the pass the user just clicked.
                                final boolean focusedPassEnabled = !multiSelectPassAdapter.getPass(passIndex).isUnavailable();
                                multiSelectPassAdapter.setSelectionAndNotify(passIndex, focusedPassEnabled);

                                // Show the recycler view.
                                passRecyclerView.setVisibility(VISIBLE);
                                passOverlayImageView.setVisibility(GONE);

                                // Announce to user
                                if (multiSelectPassAdapter.getItemCount() > 1 && multiSelectPassAdapter.getItemCount() != passIndex + 1) {
                                    passRecyclerView.announceForAccessibility(getString(R.string.use_tickets_scroll_right_guide_voonly));
                                } else if (multiSelectPassAdapter.getItemCount() > 1) {
                                    passRecyclerView.announceForAccessibility(getString(R.string.use_tickets_scroll_left_guide_voonly));
                                }

                                passRecyclerView.requestFocus();

                                // Dispose magic
                                clearBitmap();
                            }));
                });
            }
        });
        return set;
    }

    private void clearBitmap() {
        passOverlayImageView.setImageBitmap(null);
    }

    @Override
    public Context getActivityContext() {
        return getActivity();
    }

    @OnClick(R.id.fab)
    public void onFABClick() {
        activateTickets(recentActivePasses);
    }

    @OnClick(R.id.btn_upload_photo)
    public void onPhotoUploadClick() {
        startActivity(new Intent(getActivity(), AccountPhotoActivity.class));
        presenter.queueUseTicketsReload();
    }

    @Override
    public void setRecentActivePasses(List<Pass> passes) {
        if (!passes.isEmpty()) {
            recentActivePasses.clear();
            recentActivePasses.addAll(passes);
            fab.show();
        } else {
            fab.hide();
        }
    }

    @Override
    public void updatePass(@NonNull Pass pass) {
        // Update pass UI
        multiSelectPassAdapter.updatePass(pass);
        // If the user is seeing the pass, force a enabler check.
        final int snappedIndex = snapHelper.getCurrentSnapPosition();
        if (snappedIndex != -1) {
            if (pass.getUuid().equalsIgnoreCase(multiSelectPassAdapter.getPass(snappedIndex).getUuid())) {
                onPassSnapped(passIndex);
            }
        }
    }

    /**
     * Activates list of passes given.
     *
     * @param selectedPasses Passes to activate.
     */
    private void activateTickets(List<Pass> selectedPasses) {
        if (!selectedPasses.isEmpty()) {
            Timber.d("Activating tickets : %d", selectedPasses.size());
            // Display the tickets
            presenter.displayTickets(getActivity(), selectedPasses);
            // Close this fragment.
            goBack();
        }
    }

    @Override
    public boolean onBackPressed() {
        if (multiSelectPassAdapter.getItemCount() == 1) {
            return true;
        }
        return false;
    }
}
