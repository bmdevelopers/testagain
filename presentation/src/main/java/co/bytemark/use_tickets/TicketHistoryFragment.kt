package co.bytemark.use_tickets

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import android.widget.LinearLayout
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import co.bytemark.elerts.ElertsUIComponent
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.Pass
import co.bytemark.use_tickets.UseTicketsAccessibility.Companion.announceForAccessibility
import co.bytemark.use_tickets.UseTicketsAccessibility.Companion.announceForAccessibilityEmptyStates
import co.bytemark.use_tickets.adapter.UseTicketsAdapterNew
import co.bytemark.widgets.util.createViewModel
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_ticket_history.*
import kotlinx.android.synthetic.main.loading_use_tickets_view.*
import kotlinx.android.synthetic.main.network_connectivity_error_view.*
import kotlinx.android.synthetic.main.no_use_tickets_view.*
import kotlinx.android.synthetic.main.use_tickets_not_logged_in.*
import javax.inject.Inject


class TicketHistoryFragment : BaseMvvmFragment(), UseTicketsAdapterNew.ClickListener {

    private lateinit var viewModel: TicketHistoryViewModel
    private lateinit var useTicketsAdapter: UseTicketsAdapterNew
    private var nextPageUrl: String? = null
    private var historyPasses: MutableList<Pass> = mutableListOf()
    private val useTicketsUIComponent = UseTicketsUIComponent()
    private val elertsUIComponent = ElertsUIComponent()
    private var moreOptionsBottomSheetDialog: BottomSheetDialog? = null
    private var resendReceiptLoadingDialog: MaterialDialog? = null
    lateinit var elertsBottomSheetDialog: BottomSheetDialog

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter;
    private var showErrorDialogs = true
    private val bmError: BMError? = null

    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = createViewModel { CustomerMobileApp.appComponent.ticketHistoryViewModel }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.fragment_ticket_history, container, false)!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpBottomSheet()
        useTicketsAdapter = UseTicketsAdapterNew(activity)
        useTicketsAdapter.setPassType(PassType.EXPIRED)
        useTicketsAdapter.setClickListener(this)
        historyTicketsRecyclerView.adapter = useTicketsAdapter
        setSwipeRefreshLayoutColorSchemes()
        initImageTintColors()
        swipeRefreshLayout.setOnRefreshListener {
            historyTicketsRecyclerView.visibility = View.GONE
            clearDataAndLoadTickets()
        }
        loadMoreButton.setOnClickListener { loadHistoryTickets() }

        btn_refresh_tickets.setOnClickListener {
            ll_no_tickets.visibility = View.GONE
            clearDataAndLoadTickets()
        }

        announceForAccessibility(ll_no_tickets_text, getString(R.string.use_tickets_history_you_have_no_expired_tickets))
        announceForAccessibilityEmptyStates(activity!!,
                ll_no_network_texts,
                loginLinearLayout)

    }

    private fun setSwipeRefreshLayoutColorSchemes() {
        useTicketsUIComponent.setSwipeRefreshLayoutColorSchemes(swipeRefreshLayout, confHelper)
    }

    private fun loadHistoryTickets() {
        if (!isOnline()) {
            showNetworkConnectionErrorView()
            return
        }

        if (!BytemarkSDK.isLoggedIn()) {
            showLoginView()
            return
        }

        viewModel.getTicketHistory(PER_PAGE, STATUS, nextPageUrl)?.observe(this, Observer { result ->
            useTicketsAdapter.showLoadMoreButton(false)
            result?.let {
                when (it) {
                    is Result.Loading -> {
                        showLoading()
                    }

                    is Result.Failure -> {
                        hideLoading()
                        val bmError = it.bmError[0]
                        if (showErrorDialogs)
                            handleError()
                    }

                    is Result.Success -> {
                        hideLoading()
                        val data = it.data ?: return@let

                        if (data.totalCount == 0 && data.passes?.isEmpty()!!) {
                            analyticsPlatformAdapter.cloudPassesLoaded(AnalyticsPlatformsContract.PassType.HISTORY, 0
                                    , 0, AnalyticsPlatformsContract.Status.SUCCESS,
                                    getString(R.string.use_tickets_history_you_have_no_expired_tickets))
                            showNoTicketsView()
                        } else {
                            if (data.pagination != null && !data.pagination?.next.isNullOrEmpty()) {
                                nextPageUrl = data.pagination?.next
                                useTicketsAdapter.showLoadMoreButton(true)
                            }
                            analyticsPlatformAdapter.cloudPassesLoaded(AnalyticsPlatformsContract.PassType.HISTORY, data.passes?.size
                                    ?: 0, 0, AnalyticsPlatformsContract.Status.SUCCESS, "")
                            historyTicketsRecyclerView.visibility = View.VISIBLE
                            data.passes?.let { it1 -> historyPasses.addAll(it1) }
                            useTicketsAdapter.setPasses(historyPasses)
                            useTicketsAdapter.notifyDataSetChanged()
                        }
                    }
                }
            }

        })
    }

    fun showLoading() {
        historyTicketsRecyclerView.isClickable = false
        ll_no_tickets.visibility = View.GONE
        ll_loading_use_tickets.visibility = View.VISIBLE
        loginParentLinearLayout.visibility = View.GONE
        ll_no_tickets_text.visibility = View.GONE
        swipeRefreshLayout.isEnabled = false
        swipeRefreshLayout.isRefreshing = false
        announceForAccessibility(ll_loading_use_tickets, getString(R.string.loading))
    }

    private fun handleError() {
        bmError?.let { handleError(it) }
        analyticsPlatformAdapter.cloudPassesLoaded(AnalyticsPlatformsContract.PassType.HISTORY, 0
            , 0, AnalyticsPlatformsContract.Status.FAILURE,
            bmError?.message)
    }

    private fun showNoTicketsView() {
        ll_no_tickets.visibility = View.VISIBLE
        ll_no_tickets_text.visibility = View.VISIBLE
        tv_no_tickets_2.visibility = View.GONE
        tv_no_tickets_1.visibility = View.VISIBLE
        tv_no_tickets_1.text = getString(R.string.use_tickets_history_you_have_no_expired_tickets)
        ll_network_connectivity_error_view.visibility = View.INVISIBLE
        ll_loading_use_tickets.visibility = View.INVISIBLE
        loginParentLinearLayout.visibility = View.INVISIBLE
        btn_refresh_tickets.visibility = View.VISIBLE
        btn_buy_tickets_no_tickets.visibility = View.GONE
        ll_no_tickets_text.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
    }

    fun hideLoading() {
        historyTicketsRecyclerView.isClickable = true
        loginParentLinearLayout.visibility = View.INVISIBLE
        ll_loading_use_tickets.visibility = View.INVISIBLE
        swipeRefreshLayout.isEnabled = true
    }

    fun showNetworkConnectionErrorView() {
        ll_network_connectivity_error_view.visibility = View.VISIBLE
        ll_no_tickets.visibility = View.INVISIBLE
        ll_loading_use_tickets.visibility = View.INVISIBLE
        loginParentLinearLayout.visibility = View.INVISIBLE
        ll_no_network_texts.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
        ll_no_network_texts.announceForAccessibility(getString(R.string.network_connectivity_error))
    }

    companion object {
        const val PER_PAGE = 2
        const val STATUS = "EXPIRED"
    }

    override fun onOnline() {
        ll_network_connectivity_error_view.visibility = View.GONE
        if (historyPasses.isEmpty()) {
            historyTicketsRecyclerView.visibility = View.GONE
            clearDataAndLoadTickets()
        }
        super.onOnline()
    }

    override fun onOffline() {
        ll_loading_use_tickets.visibility = View.GONE
        historyTicketsRecyclerView.visibility = View.GONE
        clearData()
        showNetworkConnectionErrorView()
        super.onOffline()
    }

    private fun clearData() {
        historyPasses = ArrayList()
        nextPageUrl = null
    }

    override fun onClick(pass: Pass?, selectedOverlay: LinearLayout?, noOfPasses: Int) {
    }

    override fun onLoadMoreClick() {
        loadHistoryTickets()
    }

    override fun onMoreDotsClick(pass: Pass?) {
        if (pass?.isUnavailable == true) return
        useTicketsUIComponent.showViewTicketDetailsLabel()
        useTicketsUIComponent.hideMoveToCloudLabel()
        useTicketsUIComponent.assignPassToRepurchase(pass!!)

        if (pass.orderItem?.orderUuid?.isNotEmpty() == true)
            useTicketsUIComponent.showResendReceipt()
        else
            useTicketsUIComponent.hideResendReceipt()

        if (pass.isProductRepurchaseEnabled)
            useTicketsUIComponent.showRepurchaseLabel()
        else
            useTicketsUIComponent.hideRepurchaseLabel()
        moreOptionsBottomSheetDialog?.show()
    }

    private fun setUpBottomSheet() {
        moreOptionsBottomSheetDialog = activity?.let { BottomSheetDialog(it) }
        moreOptionsBottomSheetDialog?.let {
            useTicketsUIComponent.setupBottomSheet(moreOptionsBottomSheetDialog!!,
                    activity!!, this,
                    AppConstants.USE_TICKETS_HISTORY_FRAGMENT_TYPE,
                    confHelper.isEnableTransferPassInUseTicket)
        }

        if (confHelper.isElertSdkEnabled()) {
            elertsBottomSheetDialog =
                BottomSheetDialog(activity!!)
            elertsUIComponent.setupElertsBottomSheet(
                elertsBottomSheetDialog,
                activity!!,
                this
            )
        }
    }

    fun showElertsBottomSheet() {
        if(getActivity() != null)
            elertsBottomSheetDialog.show()
    }

    fun resendReceipt(orderUUID: String) {
        if (activity == null) return
        viewModel.resendReceipt(orderUUID)?.observe(this, Observer { result ->
            result?.let {
                when (it) {
                    is Result.Loading -> {
                        showResendReceiptDialog()
                    }

                    is Result.Failure -> {
                        hideResendReceiptDialog()
                        val error = it.bmError.first()
                        handleError(error)
                        analyticsPlatformAdapter.featureResendReceipt(AnalyticsPlatformsContract.PassType.HISTORY, orderUUID,
                                AnalyticsPlatformsContract.Status.FAILURE, error.message)
                    }

                    is Result.Success -> {
                        hideResendReceiptDialog()
                        if (it.data?.success!!) {
                            showResendReceiptResultDialog()
                            analyticsPlatformAdapter.featureResendReceipt(AnalyticsPlatformsContract.PassType.HISTORY, orderUUID,
                                    AnalyticsPlatformsContract.Status.SUCCESS, "")
                        }
                    }
                }
            }
        })
    }

    private fun showResendReceiptDialog() {
        resendReceiptLoadingDialog = MaterialDialog.Builder(activity!!)
                .content(R.string.use_tickets_resending_receipt_text)
                .progress(true, 0)
                .cancelable(false).show()
    }

    private fun hideResendReceiptDialog() {
        resendReceiptLoadingDialog?.let { resendReceiptLoadingDialog!!.hide() }
    }

    private fun showResendReceiptResultDialog() {
        MaterialDialog.Builder(activity!!)
                .title(R.string.popup_success)
                .content(getString(R.string.sign_in_change_password_success))
                .positiveText(R.string.ok)
                .positiveColor(confHelper.dataThemeAccentColor)
                .onPositive { dialog: MaterialDialog, dialogAction: DialogAction? -> dialog.dismiss() }
                .show()
    }

    private fun clearDataAndLoadTickets() {
        clearData()
        loadHistoryTickets()
    }

    private fun initImageTintColors() {
        iv_loading_tickets.imageTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        iv_network_error.imageTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        use_tickets_image_view.imageTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        pvl.setProgressColor(confHelper.backgroundThemeAccentColor, confHelper.backgroundThemeBackgroundColor)
    }

    private fun showLoginView() {
        swipeRefreshLayout.isEnabled = false
        loginParentLinearLayout.visibility = View.VISIBLE
        ll_network_connectivity_error_view.visibility = View.INVISIBLE
        loginLinearLayout.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
        loginLinearLayout.announceForAccessibility(getString(R.string.you_are_signed_out))
    }


    fun showElertsDialog(
        isSuccess: Boolean,
        errorMessage: String?,
        errorCode: Int
    ) {
        MaterialDialog.Builder(activity!!)
            .title(if (isSuccess) R.string.report_success_pop_up_title else R.string.report_failure_pop_up_title)
            .content(
                (if (isSuccess) getString(R.string.elert_report_success) else if (errorCode == 0) getString(
                    R.string.report_connection_error_message
                ) else errorMessage)!!
            )
            .positiveText(R.string.ok)
            .positiveColor(confHelper.dataThemeAccentColor)
            .onPositive { dialog: MaterialDialog, which: DialogAction? ->
                dialog.dismiss()
                if(!isOnline())
                    (activity as UseTicketsActivity).showCloudPassErrorDialog()
            }

            .show()
    }

    fun putErrorDialogOnHold() {
        showErrorDialogs = false
    }
}
