package co.bytemark.use_tickets;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.ArraySet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringDef;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.analytics.AnalyticsPlatformsContract;
import co.bytemark.domain.error.BytemarkException;
import co.bytemark.domain.interactor.credit_pass.CreditPassUseCase;
import co.bytemark.domain.interactor.discount.DiscountCalculationUseCase;
import co.bytemark.domain.interactor.fare_medium.FareMediumRequestValues;
import co.bytemark.domain.interactor.fare_medium.GetFareMedia;
import co.bytemark.domain.interactor.fare_medium.GetFareMediumContents;
import co.bytemark.domain.interactor.notification.GetUnreadNotificationsCount;
import co.bytemark.domain.interactor.passes.TransferPassUseCaseV1;
import co.bytemark.domain.interactor.passes.TransferPassUseCaseValue;
import co.bytemark.domain.interactor.resend_receipt.ResendReceiptUseCase;
import co.bytemark.domain.interactor.resend_receipt.ResendReceiptUseCaseValue;
import co.bytemark.domain.interactor.userphoto.GetUserPhotoStatus;
import co.bytemark.domain.model.common.Data;
import co.bytemark.domain.model.common.Result;
import co.bytemark.domain.model.credit_pass.CreditPassRequest;
import co.bytemark.domain.model.discount.DiscountCalculationRequest;
import co.bytemark.domain.model.discount.DiscountData;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents;
import co.bytemark.domain.model.userphoto.UserPhoto;
import co.bytemark.gtfs.GtfsUpdater_new;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.helpers.RxEventBus;
import co.bytemark.helpers.RxUtils;
import co.bytemark.mvp.MvpBasePresenter;
import co.bytemark.mvp.MvpView;
import co.bytemark.sdk.Api.ApiUtility;
import co.bytemark.sdk.BMError;
import co.bytemark.sdk.BMErrors;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.Event;
import co.bytemark.sdk.GetActivePasses;
import co.bytemark.sdk.GetAvailablePasses;
import co.bytemark.sdk.GetPasses;
import co.bytemark.sdk.GetRecentActivePasses;
import co.bytemark.sdk.Pass;
import co.bytemark.sdk.PassCacheDatabaseManager;
import co.bytemark.sdk.PassEvent;
import co.bytemark.sdk.Passes;
import co.bytemark.sdk.PrivateKey;
import co.bytemark.sdk.model.config.BarcodeValidation;
import co.bytemark.sdk.network_impl.BMNetwork;
import co.bytemark.sdk.network_impl.BaseNetworkRequestCallback;
import co.bytemark.sdk.passcache.PassCacheManager;
import co.bytemark.securityquestion.UserSecurityQuestionChecker;
import co.bytemark.use_tickets.UseTicketsEvents.VptDownloaded;
import rx.Observable;
import rx.SingleSubscriber;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

import static co.bytemark.sdk.BytemarkSDK.ResponseCode.DEVICE_LOST_OR_STOLEN;
import static co.bytemark.sdk.BytemarkSDK.ResponseCode.NETWORK_NOT_AVAILABLE;
import static co.bytemark.sdk.BytemarkSDK.ResponseCode.SESSION_EXPIRED;
import static co.bytemark.sdk.BytemarkSDK.ResponseCode.TIME_INCONSISTENCY_DETECTED;
import static co.bytemark.sdk.BytemarkSDK.SDKUtility.getAppInstallationId;
import static co.bytemark.sdk.Pass.PHOTO_REQUIRED;
import static co.bytemark.sdk.Pass.TIME_BASED_RESTRICTION;
import static co.bytemark.sdk.Pass.V3_INVALID;
import static co.bytemark.sdk.Pass.V3_NOT_CONFIGURED;
import static co.bytemark.sdk.Pass.VPT_DOWNLOAD_PENDING;
import static co.bytemark.sdk.PassCacheDatabaseManager.ACTION_VPT_DOWNLOAD_COMPLETE;
import static co.bytemark.sdk.PassCacheDatabaseManager.EXTRA_KEY_PASS;
import static co.bytemark.sdk.PassCacheDatabaseManager.EXTRA_KEY_PASS_UUID;
import static co.bytemark.sdk.PassesActivationCalculator.hasUpdatedV3;
import static co.bytemark.sdk.network_impl.BaseNetworkRequest.UNAUTHORISED;

@SuppressWarnings("all")
public interface UseTickets {
    /**
     * Created by Arunkumar on 08/08/16.
     */
    interface View extends MvpView {
        void onLoadingStarted();

        void showLoading();

        void hideLoading();

        void setSwipeRefreshingFalse();

        void hideRefreshIndicatorIfCloudLoaded();

        void setFareMediums(@NonNull List<FareMedium> fareMedia);

        void setAvailablePasses(@NonNull List<Pass> availablePasses);

        void setDeviceAvailablePasses(@NonNull List<Pass> availablePasses);

        void setActivePasses(@NonNull List<Pass> activePasses);

        void setUnlimitedPasses(@NonNull Set<Pass> activePasses);

        void showNoTicketsView();

        void showNoVirtualFareMediumView();

        void showNetworkConnectionErrorView();

        void hideNetworkConnectionErrorView();

        void showDeviceTimeErrorDialog();

        void showDeviceLostOrStolenError();

        Context getActivityContext();

        void showCloudPassesErrorDialog();

        void defaultError(@NonNull String message);

        void showSessionExpiredErrorDialog(String message);

        void loadPassesOnExpiration();

        void cloudPassesLoaded();

        void localPassesLoaded(int size, List<Pass> passes);

        void onPassVPTDownloaded(@NonNull Pass updatedPass);

        @Nullable
        Pass getPassFromAdapterWithUuid(String passUUID);

        void setUnreadNotificationsCount(@NonNull int unreadNotificationsCount);

        void updateFareMedium(FareMedium fareMedium);

        void queueReload();

        void setRecentActivePasses(List<Pass> passes);

        void closeSession();

        void showAppUpdateDialog();

        void onRefresh();

        void enforceSecurityQuestions();

        void showResendReceiptResultDialog();

        void showResendReceiptDialog();

        void hideResendReceiptDialog();

        void showCreditPassDialog(boolean isSuccess, String message);

        void setSwipeRefreshingState();
    }

    class Presenter extends MvpBasePresenter<View> implements GetPasses.GetPassesCallback, GetAvailablePasses.GetPassesCallback,
            GetRecentActivePasses.GetRecentActivePassesCallback, GetActivePasses.GetPassesCallback {
        public static final int Device = 0;
        public static final int Cloud = 1;
        private static final String CLOUD = "CLOUD";
        private static final String DEVICE = "DEVICE";
        private static final String LOCKED = "LOCKED";
        private static final String ACTIVE = "ACTIVE";
        private static final String NONACTIVE = "NONACTIVE";
        private final List<String> uuidList = new ArrayList<>();
        /**
         * RxJava subscriptions holder to hold reference to cancel all active subscriptions.
         * Should be invoked, when the View determines the active presenter calls are not necessary.
         * Like {@link Fragment#onPause()} event
         * where we usually make fresh calls again.
         */
        private final CompositeSubscription subscriptions = new CompositeSubscription();
        /**
         * RxJava filter that allows a cloud pass only when the below conditions are met.
         * 1. If the cloud pass' locked to device id does not match this device's id.
         * 2. If the pass is not already present in offline passes
         * 3. Pass is not null
         */
        private final Func1<Pass, Boolean> CLOUD_PASS_FILTER = pass -> {
            if ((pass != null && (!getAppInstallationId().equals(pass.getLockedToDeviceAppInstallationId())))) {
                if (uuidList.contains(pass.getUuid())) {
                    if (isViewAttached()) {
                        getView().onRefresh();
                    }
                    return false;
                } else {
                    return true;
                }
            }

            return false;

        };
        /**
         * Receiver to get notified about VPT download.
         */
        private final VPTDownloadReceiver vptDownloadReceiver = new VPTDownloadReceiver();
        @Inject
        ResendReceiptUseCase resendReceiptUseCase;
        @Inject
        TransferPassUseCaseV1 transferPassUseCaseV1;
        @Inject
        RxUtils rxUtils;
        @Inject
        BMNetwork bmNetwork;
        @Inject
        ConfHelper confHelper;
        @Inject
        GetUnreadNotificationsCount getUnreadNotificationsCount;
        @Inject
        GetFareMedia getFareMedia;
        @Inject
        GetFareMediumContents getFareMediumContents;
        @Inject
        GetUserPhotoStatus getUserPhotoStatus;
        @Inject
        RxEventBus eventBus;
        @Inject
        UserSecurityQuestionChecker userSecurityQuestionChecker;
        @Inject
        SharedPreferences sharedPreferences;
        @Inject
        AnalyticsPlatformAdapter analyticsPlatformAdapter;
        boolean showTransferVirtualCard;
        private boolean isSecurityQuestionsDisplayed;
        private Timer timer;
        private boolean isCloudPassCallFirstTime = true;
        private boolean isFromIncomm = false;
        private int passStorageSetting = 0;
        private String userPhotoStatus = UserPhoto.MISSING;
        boolean forceNetworkFareMediumReload = false;

        @Inject
        CreditPassUseCase creditPassUseCase;

        @Inject
        Presenter() {
            CustomerMobileApp.Companion.getComponent().inject(this);
        }

        private static PrivateKey getPrivateKeyForActivation(List<PrivateKey> privateKeys) {
            Date currentDate = Calendar.getInstance().getTime();
            PrivateKey privateKeyToUse = null;
            for (PrivateKey privateKey : privateKeys) {
                Calendar calendarValidFrom = ApiUtility.getCalendarFromS(privateKey.getValidFrom());
                calendarValidFrom.add(Calendar.MINUTE, -10);
                Date validFrom = calendarValidFrom.getTime();
                Date validTo = ApiUtility.getCalendarFromS(privateKey.getValidTo()).getTime();
                if (currentDate.after(validFrom) && currentDate.before(validTo)) {
                    privateKeyToUse = privateKey;
                }
            }
            return privateKeyToUse;
        }

        /**
         * Any initialization code goes here.
         */
        void initialize() {
            if (isViewAttached()) {
                final IntentFilter intentFilter = new IntentFilter(ACTION_VPT_DOWNLOAD_COMPLETE);
                LocalBroadcastManager.getInstance(getView().getActivityContext()).registerReceiver(vptDownloadReceiver, intentFilter);
            }
        }

        /**
         * Clean up presenter here.
         *
         * @param activity
         */
        void cleanUp(Activity activity) {
            if (activity != null) {
                LocalBroadcastManager.getInstance(activity).unregisterReceiver(vptDownloadReceiver);
            }
            subscriptions.clear();
            uuidList.clear();

            if (timer != null) {
                timer.cancel();
            }
        }

        /**
         * RxJava transformer that takes a observable of type list and categorizes them based on their
         * type and returns a map. This can be used in both local and cloud passes
         *
         * @return Map observable
         */
        private Observable.Transformer<List<Pass>, HashMap<String, List<Pass>>> passCategoryTransformer(@PassType final String passType) {
            return observable -> observable.map(new Func1<List<Pass>, HashMap<String, List<Pass>>>() {
                @Override
                public HashMap<String, List<Pass>> call(List<Pass> passes) {
                    final HashMap<String, List<Pass>> passMap = new HashMap<>();
                    final List<Pass> activePasses = new ArrayList<>();
                    final List<Pass> nonActivePasses = new ArrayList<>();
                    final List<Pass> lockedPasses = new ArrayList<>();

                    for (final Pass pass : passes) {
                        final boolean dataError = pass.isDataError();
                        // Check V3 download only when HTML v3 is being used.
                        final boolean vptDownloaded = vptDownloaded(pass);
                        // Is the pass activateable at current time?
                        final boolean passAcivateable = pass.isPassActivationRestricted();

                        boolean v3Invalid = !hasUpdatedV3(Calendar.getInstance(), pass);

                        boolean lockedToOtherDevice = isPassLockedToOtherDevice(pass);

                        final boolean photoInvalid = !isPhotoRequirementsMet(pass);

                        Timber.tag("TAG").d("PASS ACTIVATED STATUS: " + pass.isUnlimitedPassActivated());

                        switch (pass.getStatus()) {
                            case "USABLE":
                                if (v3Invalid) {
                                    pass.setLockedReason(V3_INVALID);
                                    lockedPasses.add(pass);
                                } else if (lockedToOtherDevice) {
                                    pass.setLockedReason(Pass.LOCKED_TO_OTHER_DEVICE);
                                    lockedPasses.add(pass);
                                } else if (dataError) {
                                    pass.setLockedReason(V3_NOT_CONFIGURED);
                                    lockedPasses.add(pass);
                                } else if (!vptDownloaded) {
                                    pass.setLockedReason(VPT_DOWNLOAD_PENDING);
                                    lockedPasses.add(pass);
                                } else if (passAcivateable) {
                                    pass.setLockedReason(TIME_BASED_RESTRICTION);
                                    lockedPasses.add(pass);
                                } else if (photoInvalid) {
                                    pass.setLockedReason(PHOTO_REQUIRED);
                                    lockedPasses.add(pass);
                                } else if (pass.isUnlimitedPassActivated()) {
                                    activePasses.add(pass);
                                } else {
                                    nonActivePasses.add(pass);
                                }
                                break;
                            case "USING":
                                if (v3Invalid) {
                                    pass.setLockedReason(V3_INVALID);
                                    lockedPasses.add(pass);
                                } else if (lockedToOtherDevice) {
                                    pass.setLockedReason(Pass.LOCKED_TO_OTHER_DEVICE);
                                    lockedPasses.add(pass);
                                } else if (dataError) {
                                    pass.setLockedReason(V3_NOT_CONFIGURED);
                                    lockedPasses.add(pass);
                                } else if (!vptDownloaded) {
                                    pass.setLockedReason(VPT_DOWNLOAD_PENDING);
                                    lockedPasses.add(pass);
                                } else if (passAcivateable) {
                                    pass.setLockedReason(TIME_BASED_RESTRICTION);
                                    lockedPasses.add(pass);
                                } else if (photoInvalid) {
                                    pass.setLockedReason(PHOTO_REQUIRED);
                                    lockedPasses.add(pass);
                                } else {
                                    Timber.tag("TAG").d("ACTIVE PASSES added: " + pass.getLabelName());
                                    activePasses.add(pass);
                                }
                                break;
                            case "USES_GONE":
                                break;
                            case "EXPIRED":
                                break;
                        }
                    }

                    passMap.put(ACTIVE, activePasses);
                    passMap.put(NONACTIVE, nonActivePasses);
                    passMap.put(LOCKED, lockedPasses);
                    return passMap;
                }

                private boolean vptDownloaded(final Pass pass) {
                    if (!pass.hasV3Templates()) {
                        return true;
                    }
                    try {
                        for (PassEvent passEvent : pass.getPassEvents()) {
                            Event event = passEvent.getEvent();
                            if (!event.getV3Templates().isEmpty()) {
                                final Context context = getView().getActivityContext();
                                final String uid = PassCacheDatabaseManager
                                        .getInstance(context)
                                        .getV3template(event.getUuid()).get(0).getUuid();

                                return !PassCacheDatabaseManager
                                        .getInstance(context)
                                        .getVisualPassTemplate(uid)
                                        .isEmpty();
                            }
                        }
                    } catch (Exception ignore) {
                        Timber.e(ignore.toString());
                    }
                    return false;
                }
            });
        }

        private boolean isPassLockedToOtherDevice(Pass pass) {
            return pass.getLockedToDeviceAppInstallationId().length() > 0 && !getAppInstallationId()
                    .equals(pass.getLockedToDeviceAppInstallationId());
        }

        public String getUserPhotoStatus() {
            return userPhotoStatus;
        }


        /**
         * Mapper function responsible for merging cloud locked passes with the ones we locked manually.
         * Also applies reasoning to the cloud locked passes since they won't have it.
         *
         * @param cloudLockedPasses The cloud locked passes to merge with
         * @return
         */
        @NonNull
        private Func1<HashMap<String, List<Pass>>, HashMap<String, List<Pass>>> lockedPassMergeMapper(final ArrayList<Pass> cloudLockedPasses) {
            return passMap -> {
                // Apply locked to other device reasoning
                for (final Pass pass : cloudLockedPasses) {
                    pass.setLockedReason(Pass.LOCKED_TO_OTHER_DEVICE);
                }
                // Merge to categorized list
                final List<Pass> processedLockedPass = passMap.get(LOCKED);
                processedLockedPass.addAll(cloudLockedPasses);
                passMap.put(LOCKED, processedLockedPass);
                // Send back
                return passMap;
            };
        }

        /**
         * Clears all cached passes on memory. This means next {@link BMNetwork#getPasses} will
         * return fresh data.
         */
        void clearPassCache() {
            PassCacheManager.get().evictAll();
        }

        boolean isUsingQrCode() {
            BarcodeValidation barcodeValidation = confHelper.getOrganization().getPassConfiguration().getBarcodeValidation();
            if (barcodeValidation == null) {
                return false;
            } else if (barcodeValidation.get_2dBarcodeType().equalsIgnoreCase(BarcodeValidation.NONE)) {
                return false;
            }
            return true;
        }

        boolean isPayloadTypeUIC() {
            try {
                String payloadType = confHelper.getOrganization().getPassConfiguration().getBarcodeValidation().getPayloadType();
                return payloadType.equalsIgnoreCase(BarcodeValidation.KAPSCH_PAYLOAD);
            } catch (Exception e) {
                return false;
            }
        }

        boolean isPayloadTypeINIT() {
            try {
                String payloadType = confHelper.getOrganization().getPassConfiguration().getBarcodeValidation().getPayloadType();
                return payloadType.equalsIgnoreCase(BarcodeValidation.INIT_PAYLOAD);
            } catch (Exception e) {
                return false;
            }
        }

        public boolean isRequiringPrivateKey() {
            return isPayloadTypeINIT() || isPayloadTypeUIC();
        }

        /**
         * Begins loading fare medium and passes.
         */
        void loadContent() {
            if (isViewAttached()) {
                getView().onLoadingStarted();
                // commenting this because we were cancelling the request before firing the new one. But according
                // to new designs which will have tabs this will be a problem.
                //cancelRequests();

                loadPrivateKeys();

                loadPasses();

                loadNotifications();

            } else {
                Timber.d("Skipped");
            }
        }

        void setIncommConfigForAvailable(boolean isFromIncomm) {
            this.isFromIncomm = isFromIncomm;
            this.passStorageSetting = sharedPreferences.getInt(AppConstants.DEFAULT_DEVICE_STORAGE, 0);
        }

        void loadContent(Enum passType, String status, String perPage, String pageNo) {
            if (isViewAttached()) {
                isCloudPassCallFirstTime = true;
                getView().onLoadingStarted();
                //cancelRequests();

                loadPrivateKeys();

                loadPasses(passType, status, perPage, pageNo);

                loadNotifications();

            } else {
                Timber.d("Skipped");
            }
        }

        void downloadGTFS() {
            try {
                if (confHelper.getGTFSURL() != null && !confHelper.getGTFSURL().equals(""))
                    GtfsUpdater_new.getInstance((getView().getActivityContext()), confHelper.getGTFSURL()).update();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        private void loadPrivateKeys() {
            if (isViewAttached())
                if (isRequiringPrivateKey()) {
                    if (isViewAttached()) {
                        getView().showLoading();
                    }
                    bmNetwork.getPrivateKeys(getView().getActivityContext(), new BaseNetworkRequestCallback() {
                        @Override
                        public void onNetworkRequestSuccessfullyCompleted() {
                        }

                        @Override
                        public <T> void onNetworkRequestSuccessWithResponse(ArrayList<T> listOf) {

                        }

                        @Override
                        public void onNetworkRequestSuccessWithResponse(Object object) {
                            checkFlagAndLoadFaremedium();
                        }

                        @Override
                        public void onNetworkRequestSuccessWithUnexpectedError() {

                        }

                        @Override
                        public void onNetworkRequestSuccessWithError(BMError errors) {

                        }

                        @Override
                        public void onNetworkRequestSuccessWithDeviceTimeError() {

                        }
                    });
                } else {
                    checkFlagAndLoadFaremedium();
                }
        }

        private void loadPasses() {
            if (confHelper.getOrganizationPassRowConfigs() != null && !confHelper.getOrganizationPassRowConfigs().isEmpty()) {
                if (confHelper.isPhotoValidationEnabled()) {
                    getUserPhotoStatus.getObservable()
                            .doOnError(throwable -> doGetPasses())
                            .subscribe(new Action1<String>() {
                                @Override
                                public void call(String status) {
                                    Timber.d("User photo status: %s", status);
                                    if (!TextUtils.isEmpty(status)) {
                                        userPhotoStatus = status;
                                    }
                                    doGetPasses();
                                }
                            }, Timber::e);
                } else {
                    doGetPasses();
                }
            }
        }

        private void getPasses(Enum passType, String status, String perPage, String pageNo) {
            if (passType == co.bytemark.use_tickets.PassType.AVAILABLE) {
                doGetAvailablePasses(status, perPage, pageNo);
            } else { // NEED TO HANDLE EXPIRED PASSES AS WELL
                doGetActivePasses(status, perPage);
            }
        }

        private void loadPasses(Enum passType, String status, String perPage, String pageNo) {
            if (confHelper.getOrganizationPassRowConfigs() != null && !confHelper.getOrganizationPassRowConfigs().isEmpty()) {
                if (confHelper.isPhotoValidationEnabled()) {
                    getUserPhotoStatus.getObservable()
                            .doOnError(throwable -> getPasses(passType, status, perPage, pageNo))
                            .subscribe(new Action1<String>() {
                                @Override
                                public void call(String status) {
                                    Timber.d("User photo status: %s", status);
                                    if (!TextUtils.isEmpty(status)) {
                                        userPhotoStatus = status;
                                    }
                                    getPasses(passType, status, perPage, pageNo);
                                }
                            }, Timber::e);
                } else {
                    getPasses(passType, status, perPage, pageNo);
                }
            }
        }

        private void doGetPasses() {
            if (isViewAttached()) {
                bmNetwork.getPasses(getView().getActivityContext(), this);
            }
        }

        private void doGetAvailablePasses(String status, String perPage, String pageNo) {
            if (isViewAttached()) {
                bmNetwork.getAvailablePasses(getView().getActivityContext(), this, status, perPage, pageNo);
            }
        }

        private void doGetActivePasses(String status, String perPage) {
            if (isViewAttached()) {
                bmNetwork.getActivePasses(getView().getActivityContext(), this, status, perPage);
            }
        }

        Observable<ArrayList<Pass>> getDiskPassObservable() {
            return getPassLocalManager()
                    .getOfflinePassObservable()
                    .toList()
                    .map(passes -> (ArrayList<Pass>) passes);
        }

        PassCacheDatabaseManager getPassLocalManager() {
            return PassCacheDatabaseManager.getInstance(getView().getActivityContext());
        }

        void loadNotifications() {
            if (!confHelper.isNotificationEnabled()) return;
            getUnreadNotificationsCount.execute(new Subscriber<Integer>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable throwable) {
                    Timber.e("Get Unread Notifications: " + throwable.getMessage());
                }

                @Override
                public void onNext(Integer integer) {
                    if (isViewAttached()) {
                        getView().setUnreadNotificationsCount(integer);
                    }
                }
            });
        }

        void checkFlagAndLoadFaremedium() {
            if (forceNetworkFareMediumReload) {
                loadFareMediums(false);
                forceNetworkFareMediumReload = false;
            } else {
                loadFareMediums(true);
            }
        }

        void loadFareMediums(boolean loadFromLocalStore) {
            if (confHelper.getOrganizationFareMediumConfigs() != null && !confHelper.getOrganizationFareMediumConfigs().isEmpty()) {
                getFareMedia.singleExecute(new FareMediumRequestValues("", loadFromLocalStore), new SingleSubscriber<List<FareMedium>>() {
                    @Override
                    public void onSuccess(List<FareMedium> fareMedia) {
                        if (isViewAttached()) {
                            if (!fareMedia.isEmpty()) {
                                List<FareMedium> virtualFareMedia = new ArrayList<>();
                                for (FareMedium fareMedium : fareMedia) {
                                    if (fareMedium.getType() == 220) {
                                        if (fareMedium.getActiveFareMedium()) {
                                            virtualFareMedia.add(fareMedium);
                                        } else {
                                            // we wanted to show "Transfer VC" option only if there is
                                            // atleast one inactive virtual card is available
                                            showTransferVirtualCard = true;
                                        }
                                    }
                                }

                                if (!virtualFareMedia.isEmpty()) {
                                    getView().setFareMediums(virtualFareMedia);
                                } else {
                                    // show the No virtual card view only if it is remote Response, and no active faremediums in that
                                    if (!loadFromLocalStore)
                                        getView().showNoVirtualFareMediumView();
                                }
                                loadFareMediumContents(fareMedia, loadFromLocalStore);
                                // hide tha loading if the response is for local data
                                if (!loadFromLocalStore) {
                                    getView().hideLoading();
                                    getView().setSwipeRefreshingFalse();
                                }
                            } else {
                                // FareMedium list is empty, So if it is remote response, then show the empty view,
                                // if it is local, then show the loader till we get the remote response
                                if (!loadFromLocalStore) {
                                    getView().hideLoading();
                                    getView().setSwipeRefreshingFalse();
                                    getView().showNoVirtualFareMediumView();
                                } else {
                                    if (isViewAttached()) {
                                        getView().showLoading();
                                    }
                                }
                            }
                            // if this response is for local data, then trigger the remote data fetch
                            if (loadFromLocalStore) {
                                loadFareMediums(false);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof BytemarkException) {
                            if (isViewAttached()) {
                                getView().hideLoading();
                                getView().setSwipeRefreshingFalse();
                                getView().defaultError(((BytemarkException) e).getUserFacingMessage());
                                getView().showNoVirtualFareMediumView();
                            }
                        }
                        // if we encounter nay error from local fetch, try the remote fetch
                        if (loadFromLocalStore) {
                            loadFareMediums(false);
                        }
                    }
                });
            }
        }

        private void loadFareMediumContents(List<FareMedium> fareMedia, boolean loadFromLocalStore) {
            for (FareMedium fareMedium : fareMedia) {
                getFareMediumContents.singleExecute(new FareMediumRequestValues(fareMedium.getUuid(), loadFromLocalStore), new SingleSubscriber<FareMediumContents>() {
                    @Override
                    public void onSuccess(FareMediumContents fareMediumContents) {
                        fareMedium.setFareMediumContents(fareMediumContents);
                        if (isViewAttached()) {
                            getView().updateFareMedium(fareMedium);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof BytemarkException) {
                            if (isViewAttached()) {
                                getView().hideLoading();
                                getView().defaultError(((BytemarkException) e).getUserFacingMessage());
                            }
                        }
                    }
                });
            }
        }

        public void checkIfSecurityQuestionsAreAnswered() {
            userSecurityQuestionChecker.checkIfEmpty(() -> {
                if (getView() != null && !isSecurityQuestionsDisplayed) {
                    isSecurityQuestionsDisplayed = true;
                    getView().enforceSecurityQuestions();
                }
            });
        }

        /**
         * Cancels the current active request, and un subscribes from any RxJava subscriptions if active
         */
        void cancelRequests() {
            Timber.d("cancelRequests");
            bmNetwork.cancelGetPasses();
            subscriptions.clear();
            getUnreadNotificationsCount.unsubscribe();
            getFareMedia.unsubscribe();
            getFareMediumContents.unsubscribe();
        }

        void cancelAvailableRequest() {
            Timber.d("cancelRequests");
            bmNetwork.cancelGetAvailablePasses();
            subscriptions.clear();
            getUnreadNotificationsCount.unsubscribe();
            getFareMedia.unsubscribe();
            getFareMediumContents.unsubscribe();
        }

        void cancelActiveRequest() {
            Timber.d("cancelRequests");
            bmNetwork.cancelGetActivePasses();
            subscriptions.clear();
            getUnreadNotificationsCount.unsubscribe();
            getFareMedia.unsubscribe();
            getFareMediumContents.unsubscribe();
        }

        boolean isAvailableCallRunning() {
            return bmNetwork.isAvailableRequestRunning();
        }

        boolean isActiveCallRunning() {
            return bmNetwork.isActiveRequestRunning();
        }

        /**
         * Processes the cloud passes received from server and notifies the view, if attached. Does the
         * following.
         * 1. Filters the list in background thread
         *
         * @param passes Raw cloud passes
         */
        private void handleCloudPasses(final Passes passes) {
            final List<Pass> cloudPasses = passes.getPasses();
            final Subscription cloud = Observable.from(cloudPasses)
                    .distinct()
                    .compose(rxUtils.applySchedulers())
                    .filter(CLOUD_PASS_FILTER)
                    .toList()
                    .compose(passCategoryTransformer(CLOUD))
                    .map(lockedPassMergeMapper(passes.getLockedPasses()))
                    .compose(rxUtils.applySchedulers())
                    .toSingle()
                    .doOnSuccess(passMap -> {
                        if (isViewAttached()) {
                            //getView().hideLoading();

                            Set<Pass> unlimitedPasses = new ArraySet<>();

                            List<Pass> availablePasses = new ArrayList<>();
                            List<Pass> activePasses = new ArrayList<>();
                            activePasses.addAll(passMap.get(ACTIVE));
                            availablePasses.addAll(passMap.get(NONACTIVE));
                            availablePasses.addAll(passMap.get(LOCKED));
                            getView().setAvailablePasses(availablePasses);
                            for (Pass pass : passMap.get(ACTIVE)) {
                                if (pass.isUnlimitedPassActivated()) {
                                    unlimitedPasses.add(pass);
                                }
                            }
                            if (unlimitedPasses.size() > 0) {
                                getView().setUnlimitedPasses(unlimitedPasses);
                            }
                            getView().cloudPassesLoaded();
                            getView().setSwipeRefreshingState();
//                            getView().hideLoading();
                        }
                    }).doOnError(throwable -> {
                        Timber.e(throwable);

                        if (isViewAttached()) {
                            getView().hideLoading();
                            getView().cloudPassesLoaded();
                            getView().setSwipeRefreshingState();
                        }
                    })
                    .subscribe();
            subscriptions.add(cloud);
        }

        private void handleLocalPasses(final ArrayList<Pass> localPasses) {
            final Subscription local = Observable.from(localPasses)
                    .distinct()
                    .toList()
                    .compose(passCategoryTransformer(DEVICE))
                    .compose(rxUtils.applySchedulers())
                    .toSingle()
                    .doOnSuccess(passMap -> {
                        if (isViewAttached()) {
                            //getView().hideLoading();
                            //getView().hideRefreshIndicatorIfCloudLoaded();

                            final List<Pass> active = passMap.get(ACTIVE);
                            final List<Pass> nonActive = passMap.get(NONACTIVE);
                            final List<Pass> locked = passMap.get(LOCKED);

                            List<Pass> availablePasses = new ArrayList<>();
                            List<Pass> unlimitedPasses = new ArrayList<>();
                            availablePasses.addAll(passMap.get(NONACTIVE));
                            availablePasses.addAll(passMap.get(LOCKED));
                            getView().setDeviceAvailablePasses(availablePasses);

                            getView().setActivePasses(passMap.get(ACTIVE));


                            final List<Pass> passes = new ArrayList<Pass>();
                            passes.addAll(active);
                            passes.addAll(nonActive);
                            passes.addAll(locked);

                            getView().localPassesLoaded(active.size() + nonActive.size() + locked.size(), passes);
                        }
                    }).doOnError(throwable -> {
                        Timber.e(throwable);

                        if (isViewAttached()) {
                            getView().hideLoading();
                            getView().hideRefreshIndicatorIfCloudLoaded();
                            getView().localPassesLoaded(0, null);
                        }
                    })
                    .subscribe();
            subscriptions.add(local);
        }

        void activateTickets(@NonNull final Pass pass) {
            if (isViewAttached()) {
                BytemarkSDK.SDKUtility.displayTickets(getView().getActivityContext(),
                        new ArrayList<Pass>() {{
                            add(pass);
                        }});
            }
        }

        void activateTickets(@NonNull final List<Pass> passes) {
            if (isViewAttached()) {
                BytemarkSDK.SDKUtility.displayTickets(getView().getActivityContext(),
                        new ArrayList<Pass>() {{
                            addAll(passes);
                        }});
            }
        }

        private void scheduleExpirationTimer(List<Pass> passes) {
            if (timer != null) {
                timer.cancel();
                timer.purge();
            }
            timer = new Timer();

            for (final Pass pass : passes) {
                if (pass.getEarliestPassUseEnd() != null)
                    timer.schedule(new LoadPassesTask(), pass.getEarliestPassUseEnd().getTime());//
            }
        }

        @Override
        public void onCloudPasses(Passes cloudPasses) {
            if (!isFromIncomm || (isFromIncomm && passStorageSetting == 1)) {
                Timber.d("onCloudPasses: passes: %s", cloudPasses.getPasses().size());
                scheduleExpirationTimer(cloudPasses.getPasses());
                handleCloudPasses(cloudPasses);
            } else if (isFromIncomm && isCloudPassCallFirstTime && passStorageSetting == 0) {
                loadContent(co.bytemark.use_tickets.PassType.AVAILABLE, "usable", "10", "1");
                isCloudPassCallFirstTime = false;
                isFromIncomm = false;
            }
        }

        @Override
        public void onLocalPasses(ArrayList<Pass> localPasses) {
            Timber.d("onLocalPasses: passes: %s", localPasses.size());

            scheduleExpirationTimer(localPasses);
            handleLocalPasses(localPasses);
            if (isViewAttached()) {
                bmNetwork.getRecentActivePasses(getView().getActivityContext(), this);
            }

            uuidList.clear();
            if (localPasses != null && localPasses.size() > 0) {
                for (Pass pass : localPasses) {
                    uuidList.add(pass.getUuid());
                }
            }
        }

        @Override
        public void onError(@Nullable BMErrors errors) {
            if (errors != null && !errors.getErrors().isEmpty()) {
                if (isViewAttached()) {
                    getView().setSwipeRefreshingFalse();
                    getView().hideLoading();
                }
                for (BMError error : errors.getErrors()) {
                    final int code = error.getCode();
                    switch (code) {
                        case TIME_INCONSISTENCY_DETECTED:
                            if (isViewAttached()) {
                                getView().showDeviceTimeErrorDialog();
                                getView().setRecentActivePasses(new ArrayList<>());
                            }
                            break;
                        case DEVICE_LOST_OR_STOLEN:
                            if (isViewAttached()) {
                                getView().showDeviceLostOrStolenError();
                                BytemarkSDK.logout(getView().getActivityContext());
                                getView().setRecentActivePasses(new ArrayList<>());
                            }
                            break;
                        case NETWORK_NOT_AVAILABLE:
                            if (isViewAttached()) {
                                getView().showCloudPassesErrorDialog();
                            }
                            break;
                        case SESSION_EXPIRED:
                            if (isViewAttached()) {
                                getView().showSessionExpiredErrorDialog(error.getMessage());
                                BytemarkSDK.logout(getView().getActivityContext());
                                getView().setRecentActivePasses(new ArrayList<>());
                            }
                            break;
                        case UNAUTHORISED:
                            if (isViewAttached()) {
                                BytemarkSDK.logout(getView().getActivityContext());
                                getView().setRecentActivePasses(new ArrayList<>());
                                getView().closeSession();
                            }
                            break;
                        case BytemarkSDK.ResponseCode.SDK_VERSION_OUT_OF_DATE:
                            if (isViewAttached()) {
                                //BytemarkSDK.logout(getView().getActivityContext());
                                getView().showAppUpdateDialog();
                            }
                            break;
                    }
                }
            }
        }

        public void removePass(String uuid, boolean isDevicePass) {
            if (isDevicePass) {
                // remove from local cache and db
                PassCacheDatabaseManager.getInstance(getView().getActivityContext()).deletePass(uuid);
                PassCacheManager.get().removeLocalPass(uuid);
            } else {
                // remove from local cache
                PassCacheManager.get().removeCloudPass(uuid);
            }
        }

        /**
         * A device pass will have up to date information from DB whereas for cloud it will in the last
         * performed API call (Cloud pass don't get stored in DB). However we have reference to Pass
         * object in {@link co.bytemark.use_tickets.adapter.UseTicketsAdapter}.
         * <p>
         * So this method queries the adapter for latest data if the pass is device type and uses the DB pass
         * object if its device type.
         *
         * @return Updated pass object.
         */
        @Nullable
        private Pass extractPassFromIntent(@NonNull Intent intent) {
            Pass pass = intent.getParcelableExtra(EXTRA_KEY_PASS);
            final String passUUID = intent.getStringExtra(EXTRA_KEY_PASS_UUID);

            final boolean isCloudPass = pass == null;

            if (passUUID == null) return null;

            if (isCloudPass) {
                pass = getView().getPassFromAdapterWithUuid(passUUID);
            }
            return pass;
        }

        @Override
        public void setRecentActivePasses(List<Pass> passes) {
            if (isViewAttached()) {
                getView().setRecentActivePasses(passes);
            }
        }

        private boolean isPhotoRequirementsMet(@NonNull Pass pass) {
            return confHelper.isPhotoValidationEnabled() ? pass.getAllowedPhotoStatuses().isEmpty()
                    ? true : pass.getAllowedPhotoStatuses().contains(userPhotoStatus) : true;
        }

        public void resendReceipt(@Nullable Activity activity, String orderUUID, Fragment fragment) {
            if (activity == null) return;

            LifecycleOwner view = null;
            if (isViewAttached()) {
                view = (LifecycleOwner) getView();
            }
            if (fragment != null) {
                view = fragment;
            }

            resendReceiptUseCase.getLiveData(new ResendReceiptUseCaseValue(orderUUID)).observe(view, result -> {
                if (result instanceof Result.Loading) {
                    if (fragment != null) {
                        if (fragment instanceof UseTicketsAvailableFragment) {
                            ((UseTicketsAvailableFragment) fragment).showResendReceiptDialog();
                        } else if (fragment instanceof UseTicketsActiveFragment) {
                            ((UseTicketsActiveFragment) fragment).showResendReceiptDialog();
                        }
                    }
                }

                if (result instanceof Result.Success) {
                    if (fragment != null && ((Result.Success<Data>) result).getData().getSuccess()) {
                        String screen = AnalyticsPlatformsContract.PassType.ACTIVE;
                        if (fragment instanceof UseTicketsAvailableFragment) {
                            ((UseTicketsAvailableFragment) fragment).hideResendReceiptDialog();
                            ((UseTicketsAvailableFragment) fragment).showResendReceiptResultDialog();
                            screen = AnalyticsPlatformsContract.PassType.AVAILABLE;
                        } else if (fragment instanceof UseTicketsActiveFragment) {
                            ((UseTicketsActiveFragment) fragment).hideResendReceiptDialog();
                            ((UseTicketsActiveFragment) fragment).showResendReceiptResultDialog();
                            screen = AnalyticsPlatformsContract.PassType.ACTIVE;
                        }
                        analyticsPlatformAdapter.featureResendReceipt(screen, orderUUID, AnalyticsPlatformsContract.Status.SUCCESS, "");
                    }
                }

                if (result instanceof Result.Failure) {
                    String message = ((Result.Failure<Data>) result).getBmError().get(0).getMessage();
                    if (fragment != null && message != null) {
                        String screen = AnalyticsPlatformsContract.PassType.ACTIVE;
                        if (fragment instanceof UseTicketsAvailableFragment) {
                            ((UseTicketsAvailableFragment) fragment).hideResendReceiptDialog();
                            ((UseTicketsAvailableFragment) fragment)
                                    .showDefaultErrorDialog(message);
                            screen = AnalyticsPlatformsContract.PassType.AVAILABLE;
                        } else if (fragment instanceof UseTicketsActiveFragment) {
                            ((UseTicketsActiveFragment) fragment).hideResendReceiptDialog();
                            ((UseTicketsActiveFragment) fragment)
                                    .showDefaultErrorDialog(message);
                            screen = AnalyticsPlatformsContract.PassType.ACTIVE;
                        }
                        analyticsPlatformAdapter.featureResendReceipt(screen, orderUUID, AnalyticsPlatformsContract.Status.FAILURE, message);
                    }
                }
            });
        }

        public void transferPass(@Nullable Activity activity, @NonNull Pass targetPass, int target, Fragment fragment) {
            if (activity != null) {

                final String destination = target == Device ?
                        TransferPassUseCaseV1.DEVICE : TransferPassUseCaseV1.CLOUD;

                LifecycleOwner view = null;
                if (isViewAttached()) {
                    view = (LifecycleOwner) getView();
                }
                if (fragment != null) {
                    view = fragment;
                }

                transferPassUseCaseV1.getLiveData(new TransferPassUseCaseValue(targetPass.getUuid(), destination))
                        .observe(view, result -> {

                            if (result instanceof Result.Loading) {
                                if (fragment != null) {
                                    if (fragment instanceof UseTicketsAvailableFragment) {
                                        ((UseTicketsAvailableFragment) fragment).showTransferPassDialog();
                                    } else if (fragment instanceof UseTicketsActiveFragment) {
                                        ((UseTicketsActiveFragment) fragment).showTransferPassDialog();
                                    }
                                }
                            }

                            if (result instanceof Result.Success) {
                                if (fragment != null) {
                                    if (fragment instanceof UseTicketsAvailableFragment) {
                                        ((UseTicketsAvailableFragment) fragment).hideTransferPassDialog();
                                    } else if (fragment instanceof UseTicketsActiveFragment) {
                                        ((UseTicketsActiveFragment) fragment).hideTransferPassDialog();
                                    }
                                }
                                analyticsPlatformAdapter.featureTransferPass(AnalyticsPlatformsContract.Screen.USE_TICKETS, destination, targetPass.getProductLabelName(), targetPass.getProductUuid(),
                                        AnalyticsPlatformsContract.Status.SUCCESS, "");
                            }

                            if (result instanceof Result.Failure) {
                                co.bytemark.domain.model.common.BMError error = ((Result.Failure<Passes>) result).getBmError().get(0);
                                if (fragment != null) {
                                    if (fragment instanceof UseTicketsAvailableFragment) {
                                        ((UseTicketsAvailableFragment) fragment).hideTransferPassDialog();
                                        ((UseTicketsAvailableFragment) fragment).showDefaultErrorDialog(error.getMessage());
                                    } else if (fragment instanceof UseTicketsActiveFragment) {
                                        ((UseTicketsActiveFragment) fragment).hideTransferPassDialog();
                                        ((UseTicketsActiveFragment) fragment).showDefaultErrorDialog(error.getMessage());
                                    }
                                }
                                analyticsPlatformAdapter.featureTransferPass(AnalyticsPlatformsContract.Screen.USE_TICKETS, destination, targetPass.getProductLabelName(), targetPass.getProductUuid(),
                                        AnalyticsPlatformsContract.Status.FAILURE, error.getMessage());
                            }
                        });
            }
        }

        @StringDef({CLOUD, DEVICE})
        @Retention(RetentionPolicy.SOURCE)
        @interface PassType {
        }

        /**
         * Responsible for listening to VPT download status.
         */
        private final class VPTDownloadReceiver extends BroadcastReceiver {

            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase(ACTION_VPT_DOWNLOAD_COMPLETE)) {
                    clearPassCache();
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (isViewAttached()) {
                                final Pass pass = extractPassFromIntent(intent);

                                if (pass != null) {
                                    Pass.applyLockedReasonForDataAndActivationError(pass);

                                    final boolean photoInValid = !isPhotoRequirementsMet(pass);
                                    if (photoInValid) {
                                        pass.setLockedReason(PHOTO_REQUIRED);
                                    }
                                    getView().onPassVPTDownloaded(pass);
                                    eventBus.postEvent(new VptDownloaded(pass));
                                }
                            } else {
                                Timber.e("Skipped screen update due to error in retrieving pass");
                            }
                        }
                    }, 700);
                }
            }
        }

        private final class LoadPassesTask extends TimerTask {
            @Override
            public void run() {
                PassCacheManager.get().evictAll();
                if (isViewAttached()) {
                    getView().loadPassesOnExpiration();
                }
            }
        }
        void creditPass(CreditPassRequest creditPassRequest, String deeplinkJwtToken) {
            LifecycleOwner view = null;
            if (isViewAttached()) {
                view = (LifecycleOwner) getView();
            }
            creditPassUseCase.getLiveData(new CreditPassUseCase.CreditPassUseCaseValue(creditPassRequest, deeplinkJwtToken))
                    .observe(view, result -> {
                        if (result instanceof Result.Loading) {
                            if (isViewAttached()) {
                                getView().showLoading();
                            }
                        }

                        if (result instanceof Result.Success) {
                            if (isViewAttached()) {
                                getView().hideLoading();
                                getView().showCreditPassDialog(true, "");
                            }
                        }

                        if (result instanceof Result.Failure) {
                            co.bytemark.domain.model.common.BMError error = ((Result.Failure<Data>) result).getBmError().get(0);
                            if (isViewAttached()) {
                                getView().hideLoading();
                                getView().showCreditPassDialog(false, error.getMessage());
                            }
                        }
                    });
        }

    }
}
