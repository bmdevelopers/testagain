package co.bytemark.use_tickets.passview;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.sdk.Pass;
import co.bytemark.sdk.Theme;
import co.bytemark.sdk.model.config.RowType;

import static android.graphics.Color.parseColor;
import static androidx.core.graphics.ColorUtils.setAlphaComponent;

/**
 * Created by Omkar on 1/3/18.
 */
public class OriginDestinationItemRowHolder extends BaseItemRowHolder {

    @BindView(R.id.left_frame)
    LinearLayout leftFrame;
    @BindView(R.id.meta_pass_item_header_left)
    TextView metaPassItemHeaderLeft;
    @BindView(R.id.meta_pass_item_value_left)
    TextView metaPassItemValueLeft;

    @BindView(R.id.image_view)
    ImageView imageView;

    @BindView(R.id.right_frame)
    LinearLayout rightFrame;
    @BindView(R.id.meta_pass_item_header_right)
    TextView metaPassItemHeaderRight;
    @BindView(R.id.meta_pass_item_value_right)
    TextView metaPassItemValueRight;
    @BindView(R.id.origin_destination_item_pass_root)
    LinearLayout originDestinationItemPassRoot;


    @IntDef({LEFT, RIGHT})
    @Retention(RetentionPolicy.SOURCE)
    @interface Side {
    }

    private static final int LEFT = 0;
    private static final int RIGHT = 1;

    private final RowType originDestinationItemRowType;

    OriginDestinationItemRowHolder(@NonNull RowType originDestinationItemRowType, @NonNull ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.origin_destination_item_pass_view, parent, false));
        ButterKnife.bind(this, rootView);
        this.originDestinationItemRowType = originDestinationItemRowType;
    }

    @Override
    public void bindPass(@NonNull Pass pass, boolean unavailable) {
        themeItems(pass);
        bindPassItem(LEFT, pass);
        bindPassItem(RIGHT, pass);
    }

    private void themeItems(@Nullable Pass pass) {
        if (pass != null && pass.getTheme() != null) {
            final Theme theme = pass.getTheme();
            int themePrimaryTextColor = parseColor(theme.getPrimaryTextColor());
            int opacity80PrimaryTextColor = setAlphaComponent(themePrimaryTextColor, 204);
            metaPassItemHeaderLeft.setTextColor(opacity80PrimaryTextColor);
            metaPassItemHeaderRight.setTextColor(opacity80PrimaryTextColor);
            metaPassItemValueLeft.setTextColor(themePrimaryTextColor);
            metaPassItemValueRight.setTextColor(themePrimaryTextColor);
        } else {
            int headerThemePrimaryTextColor = confHelper.getHeaderThemePrimaryTextColor();
            int opacity80PrimaryTextColor = setAlphaComponent(headerThemePrimaryTextColor, 204);
            metaPassItemHeaderLeft.setTextColor(opacity80PrimaryTextColor);
            metaPassItemHeaderRight.setTextColor(opacity80PrimaryTextColor);
            metaPassItemValueLeft.setTextColor(headerThemePrimaryTextColor);
            metaPassItemValueRight.setTextColor(headerThemePrimaryTextColor);
        }
    }

    private void bindPassItem(@Side int side, Pass pass) {
        final String item = side == LEFT ? originDestinationItemRowType.getLeftItem() : originDestinationItemRowType.getRightItem();
        Boolean displayLongNameForOd = confHelper.isDisplayLongNameForOd(pass.getIssuer().getUserUuid());
        switch (item) {
            case RowType.ORIGIN_NAME:
                String origin = "";
                if (pass.getFilterAttributes() != null && !pass.getFilterAttributes().equals("")) {
                    origin = pass.getFilterAttribute(RowType.ORIGIN_ATTRIBUTE_NAME, displayLongNameForOd);
                } else {
                    origin = pass.getAttribute(RowType.ORIGIN_NAME);
                    if(origin==null){
                        origin=pass.getAttribute(RowType.ORIGIN_CODE);
                    }
                }
                switch (side) {
                    case LEFT:
                        setLeftItemValue(R.string.use_tickets_origin, origin);
                        break;
                    case RIGHT:
                        setRightItemValue(R.string.use_tickets_origin, origin);
                        break;
                }
                break;
            case RowType.DESTINATION_NAME:
                String destination = "";
                if (pass.getFilterAttributes() != null && !pass.getFilterAttributes().equals("")) {
                    destination = pass.getFilterAttribute(RowType.DESTINATION_ATTRIBUTE_NAME, displayLongNameForOd);
                } else {
                    destination = pass.getAttribute(RowType.DESTINATION_NAME);
                    if(destination==null){
                        destination=pass.getAttribute(RowType.DESTINATION_CODE);
                    }
                }
                switch (side) {
                    case LEFT:
                        setLeftItemValue(R.string.use_tickets_destination, destination);
                        break;
                    case RIGHT:
                        setRightItemValue(R.string.use_tickets_destination, destination);
                        break;
                }
                break;
            case RowType.ORIGIN_ZONE:
                switch (side) {
                    case LEFT:
                        setLeftItemValue(R.string.use_tickets_origin_zone, pass.getAttribute(originDestinationItemRowType.getLeftItem()));
                        break;
                    case RIGHT:
                        setRightItemValue(R.string.use_tickets_origin_zone, pass.getAttribute(originDestinationItemRowType.getRightItem()));
                        break;
                }
                break;
            case RowType.DESTINATION_ZONE:
                switch (side) {
                    case LEFT:
                        setLeftItemValue(R.string.use_tickets_destination_zone, pass.getAttribute(originDestinationItemRowType.getLeftItem()));
                        break;
                    case RIGHT:
                        setRightItemValue(R.string.use_tickets_destination_zone, pass.getAttribute(originDestinationItemRowType.getRightItem()));
                        break;
                }
                break;
        }
    }

    private void setLeftItemValue(int headerText, String subText) {
        metaPassItemHeaderLeft.setText(headerText);
        metaPassItemValueLeft.setText(subText);
    }

    private void setRightItemValue(int headerText, String subText) {
        metaPassItemHeaderRight.setText(headerText);
        metaPassItemValueRight.setText(subText);
    }

    @Override
    public void bindFareMedium(FareMedium fareMedium) {

    }
}
