package co.bytemark.use_tickets.passview;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.sdk.Pass;
import co.bytemark.sdk.model.config.RowType;

/**
 * Created by Arunkumar on 08/09/16.
 */
class SimpleItemRowHolder extends BaseItemRowHolder {
    private final RowType simpleItemRowType;
    @BindView(R.id.text_pass_item)
    TextView textPassItem;
    @BindView(R.id.simple_item_pass_root)
    LinearLayout simpleItemRoot;


    SimpleItemRowHolder(@NonNull RowType simpleItemRowType, @NonNull ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_row_item_pass_view, parent, false));
        ButterKnife.bind(this, rootView);

        this.simpleItemRowType = simpleItemRowType;

        if (simpleItemRowType.getItem() != null) {
            textPassItem.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void bindPass(@NonNull Pass pass, boolean unavailable) {
        applyTheme();
    }

    private void applyTheme() {
        int headerThemePrimaryTextColor = confHelper.getHeaderThemePrimaryTextColor();
        textPassItem.setTextColor(headerThemePrimaryTextColor);
    }

    @Override
    public void bindFareMedium(FareMedium fareMedium) {
        switch (simpleItemRowType.getItem()) {
            case RowType.DISPLAY_NAME:
                textPassItem.setTextColor(confHelper.getAccentThemePrimaryTextColor());
                textPassItem.setText(fareMedium.getNickname());
                break;
        }
    }
}
