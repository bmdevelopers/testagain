package co.bytemark.use_tickets.passview;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents;
import co.bytemark.sdk.Pass;
import co.bytemark.sdk.Theme;
import co.bytemark.sdk.model.config.ChildOrganization;
import co.bytemark.sdk.model.config.RowType;
import co.bytemark.widgets.LoadingTextView;

import static android.graphics.Color.parseColor;
import static androidx.core.graphics.ColorUtils.setAlphaComponent;
import static co.bytemark.sdk.PassesActivationCalculator.getExpirationTimeForPasses;
import static java.lang.String.valueOf;

/**
 * Created by Arunkumar on 08/09/16.
 * A pass view row having two items on left and right.
 */
class DoubleItemRowHolder extends BaseItemRowHolder {
    @IntDef({LEFT, RIGHT})
    @Retention(RetentionPolicy.SOURCE)
    @interface Side {
    }

    @BindView(R.id.image_pass_item_left)
    ImageView imagePassItemLeft;
    @BindView(R.id.text_pass_item_left)
    TextView textPassItemLeft;
    @BindView(R.id.meta_pass_item_header_left)
    TextView metaPassItemHeaderLeft;
    @BindView(R.id.meta_pass_item_value_left)
    LoadingTextView metaPassItemValueLeft;
    @BindView(R.id.meta_pass_item_sub_value_left)
    LoadingTextView metaPassItemSubValueLeft;
    @BindView(R.id.meta_pass_item_left)
    LinearLayout metaPassItemLeft;
    @BindView(R.id.image_pass_item_right)
    ImageView imagePassItemRight;
    @BindView(R.id.text_pass_item_right)
    TextView textPassItemRight;
    @BindView(R.id.meta_pass_item_header_right)
    TextView metaPassItemHeaderRight;
    @BindView(R.id.meta_pass_item_value_right)
    LoadingTextView metaPassItemValueRight;
    @BindView(R.id.meta_pass_item_sub_value_right)
    LoadingTextView metaPassItemSubValueRight;
    @BindView(R.id.meta_pass_item_right)
    LinearLayout metaPassItemRight;
    @BindView(R.id.double_item_pass_root)
    LinearLayout doubleItemPassRoot;
    @BindView(R.id.right_frame)
    FrameLayout rightFrame;
    @BindView(R.id.left_frame)
    FrameLayout leftFrame;

    private static final int LEFT = 0;
    private static final int RIGHT = 1;

    private final RowType doubleItemRowType;

    DoubleItemRowHolder(@NonNull RowType doubleItemRowType, @NonNull ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.double_item_pass_view, parent, false));
        ButterKnife.bind(this, rootView);

        this.doubleItemRowType = doubleItemRowType;

        handleItemVisibility(doubleItemRowType.getLeftItem(), LEFT);
        handleItemVisibility(doubleItemRowType.getRightItem(), RIGHT);
    }

    /**
     * Method to determine which view will be visible. Initially all views are invisible and as and when
     * we encounter the appropriate row value, we make this visible.
     *
     * @param item What does the item represent?
     * @param side Which side the item represent?
     */
    private void handleItemVisibility(@Nullable String item, @Side int side) {
        if (item != null)
            switch (item) {
                case RowType.AGENCY_IMAGE:
                    switch (side) {
                        case LEFT:
                            imagePassItemLeft.setVisibility(View.VISIBLE);
                            break;
                        case RIGHT:
                            imagePassItemRight.setVisibility(View.VISIBLE);
                            break;
                    }
                    break;
                case RowType.PASS_DISPLAY_NAME:
                    switch (side) {
                        case LEFT:
                            textPassItemLeft.setVisibility(View.VISIBLE);
                            break;
                        case RIGHT:
                            textPassItemRight.setVisibility(View.VISIBLE);
                            break;
                    }
                    break;
                case RowType.REMAINING_USES:
                case RowType.ABSOLUTE_PASS_DELETION_DATE_TIME:
                case RowType.RELATIVE_PASS_DELETION_DATE_TIME:
                case RowType.PASS_STATUS:
                case RowType.FARE_PRODUCTS:
                    switch (side) {
                        case LEFT:
                            metaPassItemLeft.setVisibility(View.VISIBLE);
                            break;
                        case RIGHT:
                            metaPassItemRight.setVisibility(View.VISIBLE);
                            break;
                    }
                    break;
                case RowType.PRODUCTS_AVAILABLE:
                    switch (side) {
                        case LEFT:
                            metaPassItemLeft.setVisibility(View.VISIBLE);
                            break;
                        case RIGHT:
                            metaPassItemRight.setVisibility(View.VISIBLE);
                            break;
                    }
                    break;
                case RowType.SERIAL_NUMBER:
                    switch (side) {
                        case LEFT:
                            metaPassItemLeft.setVisibility(View.VISIBLE);
                            break;
                        case RIGHT:
                            metaPassItemRight.setVisibility(View.VISIBLE);
                            break;
                    }
                    break;
                case RowType.STORED_VALUE:
                    switch (side) {
                        case LEFT:
                            metaPassItemLeft.setVisibility(View.VISIBLE);
                            break;
                        case RIGHT:
                            metaPassItemRight.setVisibility(View.VISIBLE);
                            break;
                    }
                    break;
            }
    }

    @Override
    public void bindPass(@NonNull Pass pass, boolean unavailable) {
        themeItems(pass);
        bindPassItem(LEFT, pass, unavailable);
        bindPassItem(RIGHT, pass, unavailable);
    }

    @Override
    public void bindFareMedium(FareMedium fareMedium) {
        themeItems(null);
        bindFareItem(LEFT, fareMedium);
        bindFareItem(RIGHT, fareMedium);
    }

    private void themeItems(@Nullable Pass pass) {
        if (pass != null && pass.getTheme() != null) {
            final Theme theme = pass.getTheme();
            setTheme(parseColor(theme.getPrimaryTextColor()));
        } else {
            try {
                List<ChildOrganization> childOrgs = confHelper.getChildOrganizations();
                ChildOrganization childOrg = null;
                if(childOrgs != null) {
                    for (ChildOrganization org : childOrgs) {
                        if (org.getUuid().equalsIgnoreCase(pass.getIssuer().getUserUuid())) {
                            childOrg = org;
                        }
                    }
                }
                if (childOrg != null) {
                    String color = childOrg.getBranding().getThemes().getHeaderTheme().getPrimaryColor();
                    setTheme(parseColor(color));
                } else {
                    setTheme(confHelper.getHeaderThemePrimaryTextColor());
                }
            } catch (Exception e) {
                setTheme(confHelper.getHeaderThemePrimaryTextColor());
            }
        }
    }

    private void setTheme(Integer textColor) {
        int opaqueTextColor = setAlphaComponent(textColor, 204);
        textPassItemLeft.setTextColor(textColor);
        textPassItemRight.setTextColor(textColor);
        metaPassItemHeaderLeft.setTextColor(opaqueTextColor);
        metaPassItemHeaderRight.setTextColor(opaqueTextColor);
        metaPassItemValueLeft.setTextColor(textColor);
        metaPassItemSubValueLeft.setTextColor(textColor);
        metaPassItemValueRight.setTextColor(textColor);
        metaPassItemSubValueRight.setTextColor(textColor);
    }

    private void bindPassItem(@Side int side, Pass pass, boolean unavailable) {
        final String item = side == LEFT ? doubleItemRowType.getLeftItem() : doubleItemRowType.getRightItem();
        switch (item) {
            case RowType.AGENCY_IMAGE:
                if (pass.getIssuer() != null) {
                    if (pass.getIssuer().getUserUuid() != null) {
                        final String passIssuerUuid = pass.getIssuer().getUserUuid().replaceAll("-", "_");
                        final int drawabledRes = confHelper.getDrawableRes("wide_" + passIssuerUuid);
                        Glide.with(imagePassItemLeft.getContext()).load(drawabledRes).into(imagePassItemLeft);
                        Glide.with(imagePassItemRight.getContext()).load(drawabledRes).into(imagePassItemRight);
                        if(confHelper.getChildOrganizations() != null) {
                            for (int i = 0; i < confHelper.getChildOrganizations().size(); i++) {
                                final String orgUuid = confHelper.getChildOrganizations().get(i).getUuid().replaceAll("-", "_");
                                if (orgUuid.equalsIgnoreCase(passIssuerUuid)) {
                                    final String organizationName = confHelper.getChildOrganizations().get(i).getDisplayName();
                                    imagePassItemLeft.setContentDescription(organizationName);
                                }
                            }
                        }
                    }
                }
                break;
            case RowType.PASS_DISPLAY_NAME:
                switch (side) {
                    case LEFT:
                        textPassItemLeft.setText(pass.getLabelName(Locale.getDefault().getLanguage()));
                        break;
                    case RIGHT:
                        textPassItemRight.setText(pass.getLabelName(Locale.getDefault().getLanguage()));
                        break;
                }
                break;
            case RowType.REMAINING_USES:
                switch (side) {
                    case LEFT:
                        if (pass.getAllowedUses() == 0) {
                            setLeftItemValue(R.string.use_tickets_remaining_uses, rootView.getContext().getString(R.string.use_tickets_unlimited));
                        } else {
                            setLeftItemValue(R.string.use_tickets_remaining_uses, valueOf(pass.getAllowedUses() - pass.getUsesCount()));
                        }
                        break;
                    case RIGHT:
                        if (pass.getAllowedUses() == 0) {
                            setRightItemValue(R.string.use_tickets_remaining_uses, rootView.getContext().getString(R.string.use_tickets_unlimited));
                        } else {
                            setRightItemValue(R.string.use_tickets_remaining_uses, valueOf(pass.getAllowedUses() - pass.getUsesCount()));
                        }
                        break;
                }
                break;
            case RowType.ABSOLUTE_PASS_DELETION_DATE_TIME:
                switch (side) {
                    case LEFT:
                        if (getAbsolutePassExpirationText(pass, confHelper.getDateDisplayFormat()).equals("")) {
                            setLeftItemValue(R.string.use_tickets_expiration, rootView.getContext().getString(R.string.na));
                        } else {
                            setLeftItemValue(R.string.use_tickets_expiration, getAbsolutePassExpirationText(pass, confHelper.getDateDisplayFormat()) + " " +
                                    getAbsolutePassExpirationText(pass, confHelper.getTimeDisplayFormat()));
                        }
                        break;
                    case RIGHT:
                        if (getAbsolutePassExpirationText(pass, confHelper.getDateDisplayFormat()).equals("")) {
                            setRightItemValue(R.string.use_tickets_expiration, rootView.getContext().getString(R.string.na));
                        } else {
                            setRightItemValue(R.string.use_tickets_expiration, getAbsolutePassExpirationText(pass, confHelper.getDateDisplayFormat()) + " " +
                                    getAbsolutePassExpirationText(pass, confHelper.getTimeDisplayFormat()));
                        }
                        break;
                }
                break;
            case RowType.RELATIVE_PASS_DELETION_DATE_TIME:
                switch (side) {
                    case LEFT:
                        if (getRelativePassDeletionDate(pass, confHelper.getDateDisplayFormat()).equals("")) {
                            setLeftItemValue(R.string.buy_tickets_delete, rootView.getContext().getString(R.string.na));
                        } else {
                            setLeftItemValue(R.string.buy_tickets_delete, getRelativePassDeletionDate(pass, confHelper.getDateDisplayFormat()) + " " +
                                    getRelativePassDeletionDate(pass, confHelper.getTimeDisplayFormat()));
                        }
                        break;
                    case RIGHT:
                        if (getRelativePassDeletionDate(pass, confHelper.getDateDisplayFormat()).equals("")) {
                            setRightItemValue(R.string.buy_tickets_delete, rootView.getContext().getString(R.string.na));
                        } else {
                            setRightItemValue(R.string.buy_tickets_delete, getRelativePassDeletionDate(pass, confHelper.getDateDisplayFormat()) + " " +
                                    getRelativePassDeletionDate(pass, confHelper.getTimeDisplayFormat()));
                        }
                        break;
                }
                break;
            case RowType.PASS_STATUS:
                switch (side) {
                    case LEFT:
                        if (unavailable)
                            setLeftItemValue(R.string.use_ticket_status, R.string.use_tickets_unavailable);
                        else {
                            final String passStatus = pass.getPassStatusToDisplay();
                            if (passStatus.equalsIgnoreCase("Using")) {
                                setLeftItemValue(R.string.use_ticket_status, R.string.use_tickets_using);
                            } else if (passStatus.equalsIgnoreCase("Usable")) {
                                setLeftItemValue(R.string.use_ticket_status, R.string.use_tickets_usable);
                            }
                        }
                        break;
                    case RIGHT:
                        if (unavailable)
                            setRightItemValue(R.string.use_ticket_status, R.string.use_tickets_unavailable);
                        else {
                            final String passStatus = pass.getPassStatusToDisplay();
                            if (passStatus.equalsIgnoreCase("Using")) {
                                setRightItemValue(R.string.use_ticket_status, R.string.use_tickets_using);
                            } else if (passStatus.equalsIgnoreCase("Usable")) {
                                setRightItemValue(R.string.use_ticket_status, R.string.use_tickets_usable);
                            }
                        }
                        break;
                }
                break;
        }
    }

    private void bindFareItem(int side, FareMedium fareMedium) {
        final String item = side == LEFT ? doubleItemRowType.getLeftItem() : doubleItemRowType.getRightItem();
        if (item != null) {
            final FareMediumContents fareMediumContents = fareMedium.getFareMediumContents();
            switch (item) {
                case RowType.STORED_VALUE:
                    if (fareMediumContents != null) {
                        final String storedValue = fareMediumContents.getStoredValue();
                        final String value = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(Integer.parseInt(storedValue));
                        setValue(side, R.string.use_tickets_stored_value, value);
                    } else {
                        setValue(side, R.string.use_tickets_stored_value, null);
                    }
                    break;
                case RowType.PRODUCTS_AVAILABLE:
                    if (fareMediumContents != null) {
                        if (fareMediumContents.getFares().size() > 2) {
                            setValue(side, R.string.fare_medium_products_available, fareMediumContents.getFares().get(0).getName(), fareMediumContents.getFares().size() - 1 + " more products");
                        } else {
                            setValue(side, R.string.fare_medium_products_available, fareMediumContents.getFares().get(0).getName());
                        }
                    } else {
                        setValue(side, R.string.fare_medium_products_available, null);
                    }
                    break;
                case RowType.SERIAL_NUMBER:
                    setValue(side, R.string.fare_medium_serial_number, fareMedium.getBarcodePayload());
                    break;
                case RowType.FARE_PRODUCTS:
                    if (fareMediumContents != null) {
                        setValue(side, fareMediumContents.getFares().get(0).getName(), fareMediumContents.getFares().size() - 1 + " more products");
                    }
                    break;
            }
        }
    }

    private void setValue(@Side int side, @StringRes int headerRes, String text, String subText) {
        switch (side) {
            case LEFT:
                setLeftItemValue(headerRes, text, subText);
                break;
            case RIGHT:
                setRightItemValue(headerRes, text, subText);
        }
    }

    private void setValue(@Side int side, String header, String subText) {
        switch (side) {
            case LEFT:
                setLeftItemValue(header, subText);
                break;
            case RIGHT:
                setRightItemValue(header, subText);
        }
    }

    private void setValue(@Side int side, @StringRes int headerRes, String subText) {
        switch (side) {
            case LEFT:
                setLeftItemValue(headerRes, subText);
                break;
            case RIGHT:
                setRightItemValue(headerRes, subText);
        }
    }

    private void setRightItemValue(@StringRes int headerText, @StringRes int subText) {
        metaPassItemHeaderRight.setText(headerText);
        metaPassItemValueRight.setLoadedText(subText);
        metaPassItemSubValueRight.setVisibility(View.GONE);
    }

    private void setRightItemValue(@StringRes int headerText, String subText) {
        metaPassItemHeaderRight.setText(headerText);
        metaPassItemValueRight.setLoadedText(subText);
        metaPassItemSubValueRight.setVisibility(View.GONE);
    }

    private void setRightItemValue(@StringRes int headerText, String subText1, String subText2) {
        metaPassItemHeaderRight.setText(headerText);
        metaPassItemValueRight.setLoadedText(subText1);
        metaPassItemSubValueRight.setLoadedText(subText2);
    }

    private void setLeftItemValue(@StringRes int headerText, @StringRes int subText) {
        metaPassItemHeaderLeft.setText(headerText);
        metaPassItemSubValueLeft.setVisibility(View.GONE);
        metaPassItemValueLeft.setLoadedText(subText);
    }

    private void setLeftItemValue(@StringRes int headerText, String subText) {
        metaPassItemHeaderLeft.setText(headerText);
        metaPassItemSubValueLeft.setVisibility(View.GONE);
        metaPassItemValueLeft.setLoadedText(subText);
    }

    private void setLeftItemValue(@StringRes int headerText, String subText1, String subText2) {
        metaPassItemHeaderLeft.setText(headerText);
        metaPassItemValueLeft.setLoadedText(subText1);
        metaPassItemSubValueLeft.setLoadedText(subText2);
    }

    private void setRightItemValue(String headerText, String subText) {
        metaPassItemHeaderRight.setText(headerText);
        metaPassItemValueRight.setLoadedText(subText);
        metaPassItemSubValueRight.setVisibility(View.GONE);
    }

    private void setRightItemValue(String headerText, String subText1, String subText2) {
        metaPassItemHeaderRight.setText(headerText);
        metaPassItemValueRight.setLoadedText(subText1);
        metaPassItemSubValueRight.setText(subText2);
    }

    private void setLeftItemValue(String headerText, String subText) {
        metaPassItemHeaderLeft.setText(headerText);
        metaPassItemValueLeft.setLoadedText(subText);
        metaPassItemSubValueLeft.setVisibility(View.GONE);
    }

    private void setLeftItemValue(String headerText, String subText1, String subText2) {
        metaPassItemHeaderLeft.setText(headerText);
        metaPassItemValueLeft.setLoadedText(subText1);
        metaPassItemSubValueLeft.setLoadedText(subText2);
    }

    @NonNull
    private String getAbsolutePassExpirationText(Pass pass, SimpleDateFormat simpleDateFormat) {
        try {
            final Date expirationDate = pass.getExpiration().getTime();
            return simpleDateFormat.format(expirationDate);
        } catch (Exception e) {
            return "";
        }
    }

    private String getRelativePassDeletionDate(Pass pass, SimpleDateFormat simpleDateFormat) {
        try {
            List<Pass> passes = new ArrayList<>();
            passes.add(pass);
            Date date = getExpirationTimeForPasses(passes).getTime();
            return simpleDateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}