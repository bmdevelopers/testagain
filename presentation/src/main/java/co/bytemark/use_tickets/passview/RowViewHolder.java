package co.bytemark.use_tickets.passview;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.ColorUtils;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.Pass;
import co.bytemark.sdk.model.config.ChildOrganization;
import co.bytemark.sdk.model.config.RowType;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static co.bytemark.sdk.Pass.LOCKED_TO_OTHER_DEVICE;
import static co.bytemark.sdk.Pass.NONE;
import static co.bytemark.sdk.Pass.PHOTO_REQUIRED;
import static co.bytemark.sdk.Pass.TIME_BASED_RESTRICTION;
import static co.bytemark.sdk.Pass.V3_INVALID;
import static co.bytemark.sdk.Pass.V3_NOT_CONFIGURED;
import static co.bytemark.sdk.Pass.VPT_DOWNLOAD_PENDING;

/**
 * Created by Arunkumar on 08/09/16.
 */
public class RowViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.root_layout)
    public LinearLayout rootLayout;
    @BindView(R.id.ll_pass_view_overlay)
    LinearLayout linearLayoutPassViewOverlay;
    @BindView(R.id.unavailable_icon)
    public ImageView lockIcon;
    @BindView(R.id.card)
    public CardView card;
    @BindView(R.id.unavailable_text)
    TextView lockText;

    public final LinkedList<BaseItemRowHolder> rows = new LinkedList<>();

    private CheckmarkItemRowHolder checkmarkItemRowHolder;

    @Inject
    public ConfHelper confHelper;

    RowViewHolder(@NonNull ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.pass_view, parent, false));
        CustomerMobileApp.Companion.getComponent().inject(this);
        ButterKnife.bind(this, card);
        //rootView.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_NO);
//        rootView.setAccessibilityDelegate(new View.AccessibilityDelegate() {
//            @Override
//            public void onInitializeAccessibilityNodeInfo(View host, AccessibilityNodeInfo info) {
//                super.onInitializeAccessibilityNodeInfo(host, info);
//                info.addAction(new AccessibilityAction(
//                        ACTION_CLICK.getId(), rootView.getResources().getString(R.string.select)
//                ));
//            }
//        });
    }

    void addOriginDestinationItemRow(@Nullable RowType originDestinationItemRowType) {
        if (originDestinationItemRowType != null) {
            final BaseItemRowHolder baseItemRowHolder = new OriginDestinationItemRowHolder(originDestinationItemRowType, card);
            rows.add(baseItemRowHolder);
            rootLayout.addView(baseItemRowHolder.rootView);
        }
    }

    void addDoubleItemRow(@Nullable RowType doubleItemRowType) {
        if (doubleItemRowType != null) {
            final BaseItemRowHolder baseItemRowHolder = new DoubleItemRowHolder(doubleItemRowType, card);
            rows.add(baseItemRowHolder);
            rootLayout.addView(baseItemRowHolder.rootView);
        }
    }

    void addSingleItemRow(@Nullable RowType singleItemRowType) {
        if (singleItemRowType != null) {
            final BaseItemRowHolder baseItemRowHolder = new SingleItemRowHolder(singleItemRowType, card);
            rows.add(baseItemRowHolder);
            rootLayout.addView(baseItemRowHolder.rootView);
        }
    }

    void addSimpleItemRow(@Nullable RowType simpleItemRowType) {
        if (simpleItemRowType != null) {
            final BaseItemRowHolder baseItemRowHolder = new SimpleItemRowHolder(simpleItemRowType, card);
            rows.add(baseItemRowHolder);
            rootLayout.addView(baseItemRowHolder.rootView);
        }
    }

    void addImageRowType(@Nullable RowType imageRowType) {
        if (imageRowType != null) {
            final BaseItemRowHolder baseItemRowHolder = new ImageViewRowHolder(imageRowType, card);
            rows.add(baseItemRowHolder);
            rootLayout.addView(baseItemRowHolder.rootView);
        }
    }

    void addCheckmarkRow() {
        checkmarkItemRowHolder = new CheckmarkItemRowHolder(card);
        rows.add(checkmarkItemRowHolder);
        rootLayout.addView(checkmarkItemRowHolder.rootView);
    }

    /**
     * Public method to allow this view holder object to change the view properties it contains from
     * the provided Pass object. Any property that can be handled by itself are done and then bind
     * pass is called on each item row. The item row should handle the data extraction on its own.
     *
     * @param pass The current pass
     */
    public void bindPass(@NonNull Pass pass, boolean unavailable) {
        bindPass(pass, unavailable, false);
    }

    /**
     * Public method to allow this view holder object to change the view properties it contains from
     * the provided Pass object. Any property that can be handled by itself are done and then bind
     * pass is called on each item row. The item row should handle the data extraction on its own.
     * <p>
     * If a special parameter {@param enablerRestriction} is given, then forcefully unavailable
     * status is shown as overlay.
     *
     * @param pass The current pass
     */
    public void bindPass(@NonNull Pass pass, boolean unavailable, boolean enablerRestriction) {
        // First theme things that are not part of the row.
        try {
            ArrayList<ChildOrganization> childOrgs = (ArrayList<ChildOrganization>) confHelper.getChildOrganizations();
            ChildOrganization childOrg = null;
            if(childOrgs != null) {
                for (ChildOrganization org : childOrgs) {
                    if (java.util.Objects.equals(org.getUuid(), pass.getIssuer().getUserUuid())) {
                        childOrg = org;
                    }
                }
            }
            if (childOrg != null) {
                String color = childOrg.getBranding().getThemes().getHeaderTheme().getBackgroundColor();
                card.setCardBackgroundColor(Color.parseColor(color));
            } else {
                card.setCardBackgroundColor(Color.parseColor(pass.getTheme().getBackgroundColor()));
            }
        } catch (Exception e) {
            card.setCardBackgroundColor(confHelper.getHeaderThemeBackgroundColor());

        }

        if (unavailable || enablerRestriction) {
            linearLayoutPassViewOverlay.setBackground(new ColorDrawable(ColorUtils.setAlphaComponent(Color.BLACK, 150)));
            lockIcon.setVisibility(VISIBLE);
            lockText.setVisibility(VISIBLE);

            if (unavailable) {
                bindUnavailableOverlay(pass);
            } else {
                bindEnablerRestriction(pass);
            }
        } else {
            linearLayoutPassViewOverlay.setBackground(null);
            lockIcon.setVisibility(GONE);
            lockText.setVisibility(GONE);
        }

        // Dispatch command to rows, so that they can theme things that they represent
        for (final BaseItemRowHolder baseItemRowHolder : rows) {
            baseItemRowHolder.bindPass(pass, unavailable);
        }
    }


    /**
     * Call this if you need to bind fare medium instead of a pass.
     *
     * @param fareMedium fare medium to bind
     */
    public void bindFareMedium(@NonNull final FareMedium fareMedium) {
        card.setCardBackgroundColor(confHelper.getAccentThemeBacgroundColor());
        for (final BaseItemRowHolder baseItemRowHolder : rows) {
            baseItemRowHolder.bindFareMedium(fareMedium);
        }
    }

    /**
     * Themes the unavailable overlay icon and text based on the pass selection reason.
     *
     * @param pass Current pass
     */
    private void bindEnablerRestriction(Pass pass) {
        try {
            final int color = Color.WHITE;
            lockIcon.setImageTintList(ColorStateList.valueOf(color));
            lockText.setTextColor(color);
        } catch (Exception e) {
            lockIcon.setImageTintList(ColorStateList.valueOf(confHelper.getAccentThemePrimaryTextColor()));
            lockText.setTextColor(confHelper.getAccentThemePrimaryTextColor());
        }
        lockIcon.setImageResource(R.drawable.add_ticket_material);
        lockText.setText(R.string.use_ticket_add_on_ticket);
    }

    /**
     * Themes the unavailable overlay icon and text based on the pass unavailability reason.
     *
     * @param pass Current pass
     */
    private void bindUnavailableOverlay(@NonNull Pass pass) {
        try {
            final int color = Color.WHITE;
            lockIcon.setImageTintList(ColorStateList.valueOf(color));
            lockText.setTextColor(color);
        } catch (Exception e) {
            lockIcon.setImageTintList(ColorStateList.valueOf(confHelper.getAccentThemePrimaryTextColor()));
            lockText.setTextColor(confHelper.getAccentThemePrimaryTextColor());
        }
        switch (pass.getLockedReason()) {
            case V3_INVALID:
                lockIcon.setImageResource(R.drawable.outdated_ticket_material);
                lockText.setText(R.string.use_tickets_redownload_required);
                break;
            case VPT_DOWNLOAD_PENDING:
                lockIcon.setImageResource(R.drawable.download_pass_material);
                lockText.setText(R.string.loading);
                break;
            case V3_NOT_CONFIGURED:
                lockIcon.setImageResource(R.drawable.locked_material);
                lockText.setText(R.string.use_tickets_data_error);
                break;
            case LOCKED_TO_OTHER_DEVICE:
                lockIcon.setImageResource(R.drawable.locked_material);
                lockText.setText(R.string.use_tickets_saved_elsewhere);
                break;
            case TIME_BASED_RESTRICTION:
                lockIcon.setImageResource(R.drawable.time_restricted_material);
                lockText.setText(R.string.use_tickets_time_restriction);
                break;
            case PHOTO_REQUIRED:
                lockIcon.setImageResource(R.drawable.photo_restricted_material);
                lockText.setText(R.string.account_photo_required);
                break;
            case NONE:
                lockIcon.setImageDrawable(null);
                lockText.setText("");
                break;
        }
    }

    public void setSelected(boolean selected) {
        if (checkmarkItemRowHolder != null) {
            checkmarkItemRowHolder.setSelected(selected);
        }
    }

    public void toggleSelection() {
        setSelected(!isSelected());
    }

    public boolean isSelected() {
        return checkmarkItemRowHolder.isSelected();
    }


}
