package co.bytemark.use_tickets.passview;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.model.config.PassViewRowConfiguration;
import co.bytemark.sdk.model.config.RowType;

/**
 * Created by Arunkumar on 06/09/16.
 */
public class PassViewFactory {
    private final Context context;

    @Inject
    ConfHelper confHelper;

    public PassViewFactory(@NonNull Context context) {
        this.context = context.getApplicationContext();
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    @NonNull
    public RowViewHolder createPassViewHolderConfigs(@NonNull ViewGroup parent, final boolean selectable) {
        final RowViewHolder viewHolder = new RowViewHolder(parent);
        if(confHelper.getOrganizationPassRowConfigs() != null) {
            for (PassViewRowConfiguration config : confHelper.getOrganizationPassRowConfigs()) {
                if (config != null && config.getDoubleItemRowType() != null) {
                    viewHolder.addDoubleItemRow(config.getDoubleItemRowType());
                }
                if (config != null && config.getSingleItemRowType() != null) {
                    viewHolder.addSingleItemRow(config.getSingleItemRowType());
                }
                if (config != null && config.getOriginDestinationItemRowType() != null) {
                    viewHolder.addOriginDestinationItemRow(config.getOriginDestinationItemRowType());
                }
            }
        }
        if (selectable) {
            viewHolder.addCheckmarkRow();
        }
        return viewHolder;
    }

    // Added separately to allow for any future customizations
    @NonNull
    public RowViewHolder createFareMediumHolderConfigs(@NonNull ViewGroup parent) {
        final RowViewHolder viewHolder = new RowViewHolder(parent);

        final RowType qrCode = new RowType();
        qrCode.setItem(RowType.IMAGE);
        viewHolder.addImageRowType(qrCode);

        for (PassViewRowConfiguration config : confHelper.getOrganizationFareMediumConfigs()) {
            if (config != null && config.getDoubleItemRowType() != null) {
                viewHolder.addDoubleItemRow(config.getDoubleItemRowType());
            }
            if (config != null && config.getSingleItemRowType() != null) {
                viewHolder.addSingleItemRow(config.getSingleItemRowType());
            }
        }
        return viewHolder;
    }
}
