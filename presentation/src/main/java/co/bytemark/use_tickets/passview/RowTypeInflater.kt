package co.bytemark.use_tickets.passview

import android.graphics.Typeface
import android.os.Build
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import co.bytemark.R
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.Api.ApiUtility.getCalendarFromS
import co.bytemark.sdk.Pass
import co.bytemark.sdk.PassesActivationCalculator.getExpiration
import co.bytemark.sdk.model.config.RowType
import co.bytemark.use_tickets.PassType
import java.util.*


class RowTypeInflater {

    fun decideAndInflate(rowName: String,
                         textPassImage: ImageView,
                         textPassValue: TextView,
                         textPassItem: TextView,
                         licenseSpotTV: TextView,
                         pass: Pass,
                         singleItemRowType: RowType,
                         confHelper: ConfHelper,
                         rootView: View, passType: Enum<PassType>?) {
        when (rowName) {

            "pass_display_name" -> {
                textPassImage.visibility = View.GONE
                pass.setFrenchParentLabelName()
                textPassValue.text = pass.getLabelName(Locale.getDefault().language);
                if (getLicenceAndSpot(pass).equals("")) {
                    licenseSpotTV.visibility = View.GONE
                } else {
                    licenseSpotTV.text = getLicenceAndSpot(pass)
                    licenseSpotTV.visibility = View.VISIBLE
                }
                textPassValue.textSize = 20f
                pass.accessibilityText.append(singleItemRowType.item?.replace("_", "") + "\n" + textPassValue.text.toString() + "\n")
            }

            "remaining_uses" -> {
                licenseSpotTV.visibility = View.GONE
                textPassImage.setImageDrawable(confHelper.getDrawableById(R.drawable.ic_remaining_uses))
                textPassItem.text = rootView.context.getString(R.string.use_tickets_remaining_uses) + " : "
                if (pass.allowedUses == 0) {
                    textPassValue.text = rootView.context.getString(R.string.use_tickets_unlimited)
                } else {
                    textPassValue.text = pass.remainingUses.toString()
                }
                pass.accessibilityText.append(textPassValue.context.getString(R.string.use_tickets_remaining_uses_count) + textPassValue.text.toString() + "\n")
            }

            "expiration" -> {
                licenseSpotTV.visibility = View.GONE
                if(passType != null && passType == PassType.EXPIRED) {
                    if(pass.expirationDate != null) pass.setExpirationDate(getCalendarFromS(pass.expirationDate))
                    if(pass.filterAttributes == null) pass.setFilterAttributesObj()
                }

                if (pass.expiration != null) {
                    textPassImage.setImageDrawable(confHelper.getDrawableById(R.drawable.ic_expiration))
                    textPassItem.text = rootView.context.getString(R.string.use_tickets_expiration) + " : "
                    if (pass.status.equals(textPassItem.context.getString(R.string.use_tickets_using), ignoreCase = true)) {
                        val passes = ArrayList<Pass>()
                        passes.add(pass)
                        val relativeExpiration: String
                        if (getExpiration(pass) != null) {
                            relativeExpiration = getDate(getExpiration(pass)!!.time, confHelper) + " " + getTime(getExpiration(pass)!!.time, confHelper)
                            textPassValue.text = relativeExpiration
                        } else {
                            textPassValue.text = rootView.context.getString(R.string.na)
                        }
                    } else {
                        val passes = ArrayList<Pass>()
                        passes.add(pass)
                        val absoluteExpiration = getExpiration(pass)?.time?.let { getDate(it, confHelper) + " " + getTime(it, confHelper) }
                        textPassValue.text = absoluteExpiration
                    }
                } else {
                    textPassImage.setImageDrawable(confHelper.getDrawableById(R.drawable.ic_expiration))
                    textPassItem.text = rootView.context.getString(R.string.use_tickets_expiration) + " : "
                    textPassValue.text = rootView.context.getString(R.string.na)
                }
                pass.accessibilityText.append(rootView.context.getString(R.string.use_tickets_expiration) + " : " + textPassValue.text.toString() + "\n")
            }

            "itinerary_short" -> {
                licenseSpotTV.visibility = View.GONE
                val originName = getOrigin(pass, false)
                val destinationName = getDestination(pass, false)
                if (isODNull(originName, destinationName)) {
                    textPassValue.visibility = View.GONE
                } else {
                    textPassValue.visibility = View.VISIBLE
                    if (singleItemRowType.rowNumber == 0) {
                        textPassImage.visibility = View.GONE
                        setODColor(textPassValue, R.color.black_overlay)
                        textPassValue.setTypeface(null, Typeface.NORMAL)
                        setTextAttributes(textPassValue)
                    } else {
                        textPassImage.setImageDrawable(confHelper.getDrawableById(R.drawable.ic_origindestination_image))
                        textPassItem.text = "Stops : "
                        setODColor(textPassValue, android.R.color.black)
                        textPassValue.textSize = 14f
                        textPassValue.setPadding(16, 0, 0, 0)
                        // we need to wait until textPassValue layout happens to get the linecount
                        textPassValue.post {
                            if (textPassValue.lineCount > 1) {
                                textPassItem.setPadding(0, 0, 0, 52)
                            } else {
                                textPassItem.setPadding(0, 0, 0, 0)
                            }
                        }
                    }
                    textPassValue.text = "$originName / $destinationName"
                    setAccessibilityTextForOD(pass, textPassValue, originName, destinationName)
                }
            }

            "zones" -> {
                licenseSpotTV.visibility = View.GONE
                val originZone = getOriginZone(pass)
                val destinationZone = getDestinationZone(pass)
                if (isODNull(originZone, destinationZone)) {
                    textPassValue.visibility = View.GONE
                } else {
                    textPassValue.visibility = View.VISIBLE
                    if (singleItemRowType.rowNumber == 0) {
                        textPassImage.visibility = View.GONE
                        setODColor(textPassValue, R.color.black_overlay)
                        textPassValue.setTypeface(null, Typeface.NORMAL)
                        setTextAttributes(textPassValue)
                    } else {
                        textPassItem.text = "Zones : "
                        textPassImage.setImageDrawable(confHelper.getDrawableById(R.drawable.ic_zones_image))
                        setODColor(textPassValue, android.R.color.black)
                        textPassValue.textSize = 16f
                        textPassValue.setPadding(16, 0, 0, 0)
                    }
                    textPassValue.text = "$originZone / $destinationZone"
                    setAccessibilityTextForOD(pass, textPassValue, originZone, destinationZone)
                }
            }

            "dummy" -> {
                licenseSpotTV.visibility = View.GONE
                textPassImage.visibility = View.GONE
                textPassValue.visibility = View.INVISIBLE
                textPassValue.textSize = 14f
                textPassValue.setPadding(0, 0, 0, 0)
            }
        }
    }

    private fun getDate(date: Date, confHelper: ConfHelper): String {
        return confHelper.dateDisplayFormat.format(date)
    }

    private fun getTime(date: Date, confHelper: ConfHelper): String {
        return confHelper.timeDisplayFormat.format(date)
    }

    private fun setODColor(textPassValue: TextView, color: Int) {
        if (isGreaterOrEqualToMarshmallow()) {
            textPassValue.setTextColor(textPassValue.context.getColor(color))
        }
    }

    private fun getOrigin(pass: Pass, longName: Boolean): String? {
        return pass.getFilterAttribute(RowType.ORIGIN_ATTRIBUTE_NAME, longName)
    }

    private fun getDestination(pass: Pass, longName: Boolean): String? {
        return pass.getFilterAttribute(RowType.DESTINATION_ATTRIBUTE_NAME, longName)
    }

    private fun getOriginZone(pass: Pass): String? {
        return pass?.getAttribute("origin_zone")
    }

    private fun getDestinationZone(pass: Pass): String? {
        return pass?.getAttribute("destination_zone")
    }

    private fun isGreaterOrEqualToMarshmallow(): Boolean {
        return android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
    }

    private fun isODNull(origin: String?, destination: String?): Boolean {
        // OR condition because it does not make sense to have null/destination
        // or origin/null. Hence if any one of them is null return true.
        return TextUtils.isEmpty(origin) || TextUtils.isEmpty(destination)
    }

    private fun setTextAttributes(textPassValue: TextView) {
        textPassValue.textSize = 18f
        textPassValue.setTypeface(null, Typeface.NORMAL)
        textPassValue.setPadding(0, 0, 0, 36)
    }

    private fun setAccessibilityTextForOD(pass: Pass, textPassValue: TextView, originName: String?, destinationName: String?) {
        textPassValue.contentDescription = "$originName to $destinationName\n"
        pass.accessibilityText.append("$originName to $destinationName\n")
    }

    private fun getLicenceAndSpot(pass: Pass): String {
        if (pass.getInputFields() != null && !pass.inputFields.licensePlateNumber.isNullOrEmpty() &&
                !pass.inputFields.spotNumber.isNullOrEmpty()) {
            return (pass.inputFields.licensePlateNumber + " / " +
                    pass.inputFields.spotNumber)
        }
        return "";

    }
}