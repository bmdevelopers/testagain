package co.bytemark.use_tickets.passview;

import androidx.annotation.NonNull;
import android.view.View;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.Pass;

/**
 * Created by Arunkumar on 08/09/16.
 */
public abstract class BaseItemRowHolder {
    public final View rootView;

    @Inject
    ConfHelper confHelper;

    BaseItemRowHolder(@NonNull View rootView) {
        CustomerMobileApp.Companion.getComponent().inject(this);
        this.rootView = rootView;
    }

    /**
     * View data should be bound by calling this method with the appropriate pass object as parameter.
     *
     * @param pass        Pass from which data should be extracted
     * @param unavailable
     */
    public abstract void bindPass(@NonNull Pass pass, boolean unavailable);

    public abstract void bindFareMedium(FareMedium fareMedium);

    void disableAccessibility() {
        rootView.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS);
    }

    void enableAccessibility() {
        rootView.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_AUTO);
    }

    void restrictFocus() {
        rootView.setFocusable(false);
        rootView.setFocusableInTouchMode(false);
    }

    void allowFocus() {
        rootView.setFocusable(true);
        rootView.setFocusableInTouchMode(true);
    }

}
