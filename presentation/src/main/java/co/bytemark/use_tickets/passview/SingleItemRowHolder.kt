package co.bytemark.use_tickets.passview

import android.graphics.Bitmap
import android.graphics.Typeface
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import butterknife.BindView
import butterknife.ButterKnife
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.domain.model.fare_medium.FARE_MEDIUM_BLOCK_STATE
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.RxEventBus
import co.bytemark.helpers.glide.QRCodeImage
import co.bytemark.sdk.Api.ApiUtility
import co.bytemark.sdk.Pass
import co.bytemark.sdk.QrBitmapFactory
import co.bytemark.sdk.Utils
import co.bytemark.sdk.data.identifiers.IdentifiersRepository
import co.bytemark.sdk.data.userAccount.UserAccountRepository
import co.bytemark.sdk.model.common.BarcodeValidationV2
import co.bytemark.sdk.model.config.BarcodeValidation
import co.bytemark.sdk.model.config.RowType
import co.bytemark.sdk.payload_encryption.InitPayloadEncryption
import co.bytemark.use_tickets.PassType
import co.bytemark.use_tickets.UseTicketsEvents.InitQrCodeReady
import co.bytemark.widgets.LoadingTextView
import co.bytemark.widgets.util.hide
import co.bytemark.widgets.util.isOnline
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import rx.Observable
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class SingleItemRowHolder(
        singleItemRowType: RowType,
        parent: ViewGroup
) : BaseItemRowHolder(
        LayoutInflater.from(parent.context).inflate(
                R.layout.single_item_pass_view,
                parent,
                false
        )
) {

    private val singleItemRowType: RowType

    @JvmField
    @BindView(R.id.text_pass_item)
    var textPassItem: TextView? = null

    @JvmField
    @BindView(R.id.text_pass_value)
    var textPassValue: TextView? = null

    @JvmField
    @BindView(R.id.text_pass_image)
    var textPassImage: ImageView? = null

    @JvmField
    @BindView(R.id.single_item_pass_root)
    var singleItemPassRoot: FrameLayout? = null

    @JvmField
    @BindView(R.id.meta_pass_item_left)
    var metaPassItemLeft: LinearLayout? = null

    @JvmField
    @BindView(R.id.meta_pass_item_header_left)
    var metaPassItemHeaderLeft: TextView? = null

    @JvmField
    @BindView(R.id.meta_pass_item_value_left)
    var metaPassItemValueLeft: LoadingTextView? = null

    @JvmField
    @BindView(R.id.meta_pass_item_sub_value_left)
    var metaPassItemSubValueLeft: LoadingTextView? = null

    @JvmField
    @BindView(R.id.image)
    var image: ImageView? = null

    @JvmField
    @BindView(R.id.progressBar)
    var progressBar: ProgressBar? = null

    @JvmField
    @BindView(R.id.bar_Code_layout)
    var barCodeLayout: FrameLayout? = null

    @JvmField
    @BindView(R.id.meta_pass_item_nickname)
    var textPassNickname: TextView? = null

    @JvmField
    @BindView(R.id.blockedBannerText)
    var blockedBannerText: TextView? = null

    @JvmField
    @Inject
    var eventBus: RxEventBus? = null

    @JvmField
    @BindView(R.id.licenceAndSpotTV)
    var licenseSpotTV: TextView? = null
    private val parent: ViewGroup
    private var fareMedium: FareMedium? = null
    private val rowTypeInflater: RowTypeInflater
    private var passType: Enum<PassType>? = null

    @Inject
    lateinit var identifiersRepository: IdentifiersRepository

    @Inject
    lateinit var userAccountRepository: UserAccountRepository

    private fun initDependencies() {
        component.inject(this)
    }

    override fun bindPass(pass: Pass, unavailable: Boolean) {
        applyTheme()
        when (singleItemRowType.rowNumber) {
            -1, 0 -> {
                textPassValue?.setPadding(0, 0, 0, 8)
                textPassValue?.textSize = 20f
                textPassItem?.visibility = View.GONE
                textPassImage?.visibility = View.GONE
                if (singleItemRowType.rowNumber == 0) {
                    textPassValue?.let {
                        it.setTypeface(null, Typeface.NORMAL)
                        it.setTextColor(ContextCompat.getColor(it.context, R.color.black_overlay))
                    }
                }
            }
            1, 2, 3 -> {
                textPassValue?.textSize = 14f
                textPassItem?.visibility = View.VISIBLE
                textPassImage?.visibility = View.VISIBLE
                textPassValue?.setTypeface(null, Typeface.NORMAL)
                val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                params.setMargins(8, 8, 0, 8)
                textPassImage?.layoutParams = params
            }
        }
        rowTypeInflater.decideAndInflate(
                singleItemRowType.item!!, textPassImage!!, textPassValue!!,
                textPassItem!!, licenseSpotTV!!, pass, singleItemRowType, confHelper, rootView, passType
        )
    }

    private fun applyTheme() {
        val headerThemePrimaryTextColor = confHelper.headerThemePrimaryTextColor
        val dataThemePrimaryTextColor = confHelper.dataThemePrimaryTextColor
        val opacity80PrimaryTextColor = ColorUtils.setAlphaComponent(headerThemePrimaryTextColor, 204)
        // textPassItem.setTextColor(headerThemePrimaryTextColor);
        textPassNickname?.setTextColor(dataThemePrimaryTextColor)
        metaPassItemHeaderLeft?.setTextColor(dataThemePrimaryTextColor)
        metaPassItemValueLeft?.setTextColor(dataThemePrimaryTextColor)
        metaPassItemSubValueLeft?.setTextColor(dataThemePrimaryTextColor)
    }

    private fun hideFareMediumViews() {
        metaPassItemHeaderLeft?.visibility = View.GONE
        metaPassItemValueLeft?.visibility = View.GONE
        metaPassItemSubValueLeft?.visibility = View.GONE
    }

    fun setPassType(passType: Enum<PassType>?) {
        this.passType = passType
    }

    override fun bindFareMedium(fareMedium: FareMedium) {
        val fareMediumContents = fareMedium.fareMediumContents
        applyTheme()
        when (singleItemRowType.item) {
            RowType.IMAGE -> {
                metaPassItemLeft?.visibility = View.VISIBLE
                barCodeLayout?.visibility = View.VISIBLE
                textPassNickname?.visibility = View.GONE
                hideFareMediumViews()
                image?.contentDescription = image?.context?.getString(R.string.fare_medium_img_content_desc_voonly)
                progressBar?.visibility = View.VISIBLE
                if (fareMedium.state == FARE_MEDIUM_BLOCK_STATE) {
                    blockedBannerText?.visibility = View.VISIBLE
                    image?.alpha = 0.3f
                } else {
                    blockedBannerText?.visibility = View.GONE
                    image?.alpha = 1.0f
                }
                this.fareMedium = fareMedium
                generateQRCode(fareMedium)
            }
            RowType.DISPLAY_NAME -> {
                textPassImage?.visibility = View.GONE
                barCodeLayout?.visibility = View.GONE
                hideFareMediumViews()
                metaPassItemLeft?.visibility = View.VISIBLE
                textPassNickname?.visibility = View.VISIBLE
                textPassNickname?.setPadding(0, 0, 0, 24)
                textPassNickname?.text = fareMedium.nickname
            }
            RowType.PRODUCTS_AVAILABLE ->
                if (fareMediumContents != null && fareMediumContents.fares.isNotEmpty()) {
                    if (fareMediumContents.fares.size > 2) {
                        setValue(R.string.fare_medium_products_available, fareMediumContents.fares[fareMediumContents.fares.size - 1].name, (fareMediumContents.fares.size - 1).toString() + " " + metaPassItemLeft?.context?.getString(R.string.fare_medium_more_products))
                    } else {
                        if (fareMediumContents.fares.size == 2) {
                            setValue(R.string.fare_medium_products_available, fareMediumContents.fares[0].name, fareMediumContents.fares[1].name)
                        } else {
                            setValue(R.string.fare_medium_products_available, fareMediumContents.fares[0].name)
                        }
                    }
                } else {
                    setValue(R.string.fare_medium_products_available, R.string.buy_tickets_no_available_products)
                }
            RowType.STORED_VALUE ->
                if (fareMediumContents?.storedValue != null) {
                    val value = fareMediumContents.storedValue?.toInt()?.let { confHelper.getConfigurationPurchaseOptionsCurrencySymbol(it) }
                    metaPassItemValueLeft?.textSize = 22f
                    setValue(R.string.use_tickets_stored_value, value)
                } else {
                    setValue(R.string.use_tickets_stored_value, null)
                }
            RowType.LAST_UPDATED -> {
                val date = confHelper.getFormattedDateAndTime(fareMedium.lastDownloadTime ?: 0L)
                if (date != null) {
                    val fareMediaLastUpdateLabel =
                        confHelper.conf?.clients?.customerMobile?.fareMediaLastUpdateLabel
                    if (fareMediaLastUpdateLabel != null) {
                        val locale = Locale.getDefault().toLanguageTag()
                        val label =
                            when {
                                fareMediaLastUpdateLabel.containsKey(locale) -> fareMediaLastUpdateLabel[locale]
                                fareMediaLastUpdateLabel.containsKey(AppConstants.DEFAULT_LOCALE_CODE) -> fareMediaLastUpdateLabel[AppConstants.DEFAULT_LOCALE_CODE]
                                else -> parent.context.getString(R.string.fare_medium_last_update)
                            }
                        setValue(label!!, date.replace(",", " "))
                    } else {
                        // Remove comma from date between date and time i.e (05/14/19 2:43 PM instead of 05/14/19, 2:43 PM)
                        setValue(R.string.fare_medium_last_update, date.replace(",", " "))
                    }
                }
            }
            RowType.TRANSFER_TIME -> {
                if (metaPassItemHeaderLeft?.context?.isOnline()!!) {
                    if (fareMediumContents != null) {
                        val transferTime = fareMediumContents.transferTime
                        if (!TextUtils.isEmpty(transferTime)) {
                            val date = ApiUtility.getCalendarFromS(transferTime)?.time
                            if (date != null) {
                                setValue(
                                    R.string.fare_medium_transfer_time,
                                    metaPassItemValueLeft?.context?.getString(
                                        R.string.fare_medium_transfer_time_text
                                    )
                                        .plus(" ")
                                        .plus(confHelper.timeDisplayFormat.format(date))
                                )
                            }
                        } else {
                            setValue(
                                    R.string.fare_medium_transfer_time,
                                    ""
                            )
                        }
                    }
                } else {
                    setValue(
                            R.string.fare_medium_transfer_time,
                            R.string.fare_medium_transfer_time_offline_msg
                    )
                }
            }
        }
    }

    private fun setValue(@StringRes headerRes: Int, text: String, subText: String) {
        textPassNickname?.visibility = View.GONE
        metaPassItemLeft?.visibility = View.VISIBLE
        metaPassItemHeaderLeft?.setText(headerRes)
        metaPassItemValueLeft?.setLoadedText(text)
        metaPassItemSubValueLeft?.setLoadedText(subText)
    }

    private fun setValue(label: String, text: String?) {
        textPassNickname?.visibility = View.GONE
        metaPassItemLeft?.visibility = View.VISIBLE
        metaPassItemHeaderLeft?.setText(label)
        metaPassItemValueLeft?.setLoadedText(text)
        metaPassItemSubValueLeft?.visibility = View.GONE
    }

    private fun setValue(@StringRes headerRes: Int, text: String?) {
        textPassNickname?.visibility = View.GONE
        metaPassItemLeft?.visibility = View.VISIBLE
        metaPassItemHeaderLeft?.setText(headerRes)
        metaPassItemValueLeft?.setLoadedText(text)
        metaPassItemSubValueLeft?.visibility = View.GONE
    }

    private fun setValue(@StringRes headerRes: Int, text: Int) {
        textPassNickname?.visibility = View.GONE
        metaPassItemLeft?.visibility = View.VISIBLE
        metaPassItemHeaderLeft?.setText(headerRes)
        metaPassItemValueLeft?.setLoadedText(text)
        metaPassItemSubValueLeft?.visibility = View.GONE
    }

    private fun generateQRCode(fareMedium: FareMedium) {
        if (fareMedium.hasValidBarcodeValidationV2()) {
            setupQRCode(fareMedium.barcodePayloadV2!!) // For Transit Application
        } else if (isPayloadTypeINIT()) {
            setupQRCode(fareMedium) // For Init Application
        } else {
            progressBar.hide()
        }
    }

    private fun isPayloadTypeINIT(): Boolean {
        return try {
            val payloadType = confHelper.organization?.passConfiguration?.barcodeValidation?.payloadType
            payloadType.equals(BarcodeValidation.INIT_PAYLOAD, ignoreCase = true)
        } catch (e: java.lang.Exception) {
            false
        }
    }

    private fun setupQRCode(fareMedium: FareMedium) {
        try {
            val encryption = InitPayloadEncryption()
            Observable.fromCallable { encryption.getEncryptedPayload(image?.context!!, fareMedium.barcodePayload!!, confHelper.qrCodeRefreshRate, identifiersRepository) }
                    .retry(3)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : Observer<ByteArray> {
                        override fun onCompleted() {}
                        override fun onError(e: Throwable) {
                            e.printStackTrace()
                            progressBar.hide()
                        }

                        override fun onNext(bytes: ByteArray) {
                            Glide.with(image?.context)
                                    .load(QRCodeImage(bytes, confHelper.barcodeValidation))
                                    .crossFade()
                                    .skipMemoryCache(true)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .listener(object : RequestListener<QRCodeImage?, GlideDrawable?> {
                                        override fun onException(e: Exception, model: QRCodeImage?, target: Target<GlideDrawable?>, isFirstResource: Boolean): Boolean {
                                            progressBar.hide()
                                            return false
                                        }

                                        override fun onResourceReady(resource: GlideDrawable?, model: QRCodeImage?, target: Target<GlideDrawable?>, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                                            eventBus?.postEvent(InitQrCodeReady())
                                            progressBar.hide()
                                            return false
                                        }
                                    })
                                    .into(image)
                        }
                    })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setupQRCode(barcodeValidationV2: BarcodeValidationV2) {
        image?.post {
            val width = image!!.width - Utils.dpToPx(2.0)
            val height = image!!.height - Utils.dpToPx(2.0)
            with(barcodeValidationV2) {
                Observable.fromCallable { QrBitmapFactory.createQR(payloadData, width, height, mediaScheme, mediaType) }
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(object : Observer<Bitmap> {
                            override fun onCompleted() {}
                            override fun onError(e: Throwable) {
                                e.printStackTrace()
                                progressBar.hide()
                            }

                            override fun onNext(bitmap: Bitmap) {
                                progressBar.hide()
                                eventBus?.postEvent(InitQrCodeReady())
                                image?.setImageBitmap(bitmap)
                            }
                        })
            }
        }
    }

    companion object {
        @JvmField
        var accessibilityText = StringBuilder()
    }

    init {
        ButterKnife.bind(this, rootView)
        this.parent = parent
        this.singleItemRowType = singleItemRowType
        if (singleItemRowType.item != null) {
            textPassItem?.visibility = View.VISIBLE
        }
        initDependencies()
        rowTypeInflater = RowTypeInflater()
    }
}