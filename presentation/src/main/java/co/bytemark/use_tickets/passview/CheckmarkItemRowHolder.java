package co.bytemark.use_tickets.passview;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.sdk.Pass;
import co.bytemark.sdk.Theme;
import co.bytemark.widgets.CheckMarkView;

import static android.graphics.Color.parseColor;

/**
 * Created by Arunkumar on 08/09/16.
 */
class CheckmarkItemRowHolder extends BaseItemRowHolder {
    @BindView(R.id.checkmark)
    CheckMarkView checkmark;

    CheckmarkItemRowHolder(@NonNull ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.checkmark_row_item_layout, parent, false));
        ButterKnife.bind(this, rootView);
    }

    @Override
    public void bindPass(@NonNull Pass pass, boolean unavailable) {
        if (pass.getTheme() != null) {
            final Theme theme = pass.getTheme();
            checkmark.setCheckMarkColor(parseColor(theme.getPrimaryTextColor()));
        } else {
            checkmark.setCheckMarkColor(confHelper.getHeaderThemePrimaryTextColor());
        }
    }

    @Override
    public void bindFareMedium(FareMedium fareMedium) {

    }

    public void setSelected(final boolean selected) {
        checkmark.setSelected(selected);
    }

    public boolean isSelected() {
        return checkmark.isSelected();
    }
}
