package co.bytemark.use_tickets.passview;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.helpers.RxEventBus;
import co.bytemark.helpers.glide.QRCodeImage;
import co.bytemark.sdk.Pass;
import co.bytemark.sdk.data.identifiers.IdentifiersRepository;
import co.bytemark.sdk.model.config.RowType;
import co.bytemark.sdk.payload_encryption.InitPayloadEncryption;
import co.bytemark.use_tickets.UseTicketsEvents;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ImageViewRowHolder extends BaseItemRowHolder {

    private final RowType imageRowType;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.image_item_pass_root)
    LinearLayout imageItemPassRoot;

    @Inject
    RxEventBus eventBus;

    @Inject
    IdentifiersRepository identifiersRepository;

    ImageViewRowHolder(@NonNull RowType imageRowType, @NonNull ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.image_item_pass_view, parent, false));
        ButterKnife.bind(this, rootView);
        this.imageRowType = imageRowType;
        initDependencies();
    }

    private void initDependencies() {
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    @Override
    public void bindPass(@NonNull Pass pass, boolean unavailable) {

    }

    @Override
    public void bindFareMedium(final FareMedium fareMedium) {
        switch (imageRowType.getItem()) {
            case RowType.IMAGE:
                image.setContentDescription(image.getContext().getString(R.string.fare_medium_img_content_desc_voonly));
                progressBar.setVisibility(View.VISIBLE);
                setupQRCode(fareMedium);
                break;
        }
    }

    private void setupQRCode(final FareMedium fareMedium) {
        try {
            InitPayloadEncryption encryption = new InitPayloadEncryption();
            Observable.fromCallable(() -> encryption.getEncryptedPayload(image.getContext(), fareMedium.getBarcodePayload(), confHelper.getQrCodeRefreshRate(), identifiersRepository))
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<byte[]>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(byte[] bytes) {
                            Glide.with(image.getContext())
                                    .load(new QRCodeImage(bytes, confHelper.getBarcodeValidation()))
                                    .crossFade()
                                    .skipMemoryCache(true)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .listener(new RequestListener<QRCodeImage, GlideDrawable>() {
                                        @Override
                                        public boolean onException(Exception e, QRCodeImage model, Target<GlideDrawable> target, boolean isFirstResource) {
                                            progressBar.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(GlideDrawable resource, QRCodeImage model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            eventBus.postEvent(new UseTicketsEvents.InitQrCodeReady());
                                            progressBar.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(image);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
