package co.bytemark.use_tickets

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.Pass


class TicketDetailsActivity : MasterActivity() {

    override fun getLayoutRes() = R.layout.activity_ticket_details

    override fun useHamburgerMenu() = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(getString(R.string.ticket_details))
        val pass: Pass? = intent.getParcelableExtra("pass")
        val fragmentType: String? = intent.getStringExtra(AppConstants.USE_TICKETS_FRAGMENT_TYPE)
        if (pass != null) {
            supportFragmentManager.beginTransaction().replace(R.id.ticket_detail_container, TicketDetailsFragment.newInstance(pass, fragmentType!!)).commit()
        }
    }
}
