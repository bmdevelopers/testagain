package co.bytemark.use_tickets.viewholder

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.Pass
import co.bytemark.sdk.Pass.*
import co.bytemark.sdk.model.config.RowType
import co.bytemark.use_tickets.PassType
import co.bytemark.use_tickets.UseTicketsAccessibility
import co.bytemark.use_tickets.adapter.UseTicketsAdapterNew
import co.bytemark.use_tickets.passview.SingleItemRowHolder
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class SingleItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private var card: CardView = itemView.findViewById(R.id.card)
    private var passLayout: LinearLayout = itemView.findViewById(R.id.pass_layout)
    var frameLayout: FrameLayout = itemView.findViewById(R.id.available_ll)
    private var singleProduct: LinearLayout = itemView.findViewById(R.id.single_product)
    private var passType: TextView = itemView.findViewById(R.id.pass_type)
    var productImage: ImageView = itemView.findViewById(R.id.iv_product_logo)
    private var selectedOverlay: LinearLayout = itemView.findViewById(R.id.selection_overlay)
    private var unavailablePassOverlay: LinearLayout = itemView.findViewById(R.id.unavailable_pass_view_overlay)
    private var lockIcon: ImageView = itemView.findViewById(R.id.unavailable_icon)
    private var lockText: TextView = itemView.findViewById(R.id.unavailable_text)
    private var lockReasonText: TextView = itemView.findViewById(R.id.unavailable_reason_text)
    var moreDots: ImageView = itemView.findViewById(R.id.more_dots)

    private lateinit var clickListener: UseTicketsAdapterNew.ClickListener
    private var passes: List<Pass> = ArrayList()

    private lateinit var passTypeEnum: Enum<PassType>

    var previousSelectedOverlay: LinearLayout? = null

    @Inject
    lateinit var confHelper: ConfHelper

    init {
        CustomerMobileApp.component.inject(this)


        card.setOnClickListener {
            clickListener.onClick(this.passes[position], selectedOverlay, passes.size)
        }
    }

    fun setClickListener(clickListener: UseTicketsAdapterNew.ClickListener) {
        this.clickListener = clickListener
    }

    fun setPasses(passes: List<Pass>) {
        Timber.tag("TAG").d("PASSES: " + passes.size)
        this.passes = passes
    }

    fun setPassType(passType: Enum<PassType>) {
        this.passTypeEnum = passType
    }

    fun bind(pass: Pass) {
        selectUnselectPassOverlay(pass)
        passType.text = pass.labelName
        UseTicketsAccessibility.announceForAccessibility(card, pass.accessibilityText.toString())
        //pass.accessibilityText.setLength(0)
        if (pass.isUnavailable) {
            bindUnavailableOverlay(pass)
            //blurCardView(true)
        } else { // Need this else part or else the available tickets will show saved elsewhere when you scroll
            //to bottom of the recycler view and scroll to beginning again
            //blurCardView(false)
            unavailablePassOverlay.visibility = View.GONE
            lockIcon.visibility = View.GONE
            lockText.visibility = View.GONE
            lockReasonText.visibility = View.GONE
        }
        setupMoreDotsClickListener(pass)
    }

    private fun setupMoreDotsClickListener(pass: Pass) {
        moreDots.setOnClickListener {
            clickListener.onMoreDotsClick(pass)
        }
    }

    private fun blurCardView(shouldBlur: Boolean) {
        if (shouldBlur) {
            card.alpha = 0.4F
        } else {
            card.alpha = 1F
        }
    }

    private fun selectUnselectPassOverlay(pass: Pass) {
        if (pass.selected) {
            //UseTicketsAccessibility.announceForAccessibility(selectedOverlay, card.context.getString(R.string.use_tickets_selected_ticket_voonly) + pass.labelName + card.context.getString(R.string.use_tickets_double_tap_to_activate))
            selectedOverlay.visibility = View.VISIBLE
        } else {
            selectedOverlay.visibility = View.GONE
        }
    }

    fun addSingleItemRow(singleItemRowType: RowType?, position: Int) {
        if (singleItemRowType != null && passes.isNotEmpty() && position != -1) {
            val baseItemRowHolder = SingleItemRowHolder(singleItemRowType, card)
            baseItemRowHolder.setPassType(passTypeEnum)
            baseItemRowHolder.bindPass(passes[position], false)
            singleProduct.addView(baseItemRowHolder.rootView)
        }
    }

    fun removeRows() {
        singleProduct.removeAllViews()
    }

    /**
     * Themes the unavailable overlay icon and text based on the pass unavailability reason.
     *
     * @param pass Current pass
     */
    private fun bindUnavailableOverlay(pass: Pass) {
        unavailablePassOverlay.visibility = View.VISIBLE
        lockIcon.visibility = View.VISIBLE
        lockText.visibility = View.VISIBLE
        lockReasonText.visibility = View.VISIBLE
        unavailablePassOverlay.background = ColorDrawable(ColorUtils.setAlphaComponent(Color.BLACK, 150))
        try {
            val color = Color.WHITE
            lockIcon.imageTintList = ColorStateList.valueOf(color)
            lockText.setTextColor(color)
            lockReasonText.setTextColor(color)
        } catch (e: Exception) {
            lockIcon.imageTintList = ColorStateList.valueOf(confHelper.accentThemePrimaryTextColor)
            lockText.setTextColor(confHelper.accentThemePrimaryTextColor)
            lockReasonText.setTextColor(confHelper.accentThemePrimaryTextColor)
        }

        when (pass.lockedReason) {
            V3_INVALID -> {
                lockIcon.setImageResource(R.drawable.outdated_ticket_material)
                lockText.setText(R.string.use_tickets_redownload_required)
                lockReasonText.setText(co.bytemark.sdk.R.string.unavailable_please_invalid_v3)
            }
            VPT_DOWNLOAD_PENDING -> {
                lockIcon.setImageResource(R.drawable.download_pass_material)
                lockText.setText(R.string.loading)
                lockReasonText.setText(co.bytemark.sdk.R.string.unavailable_ticket_data_not_downloaded)
            }
            V3_NOT_CONFIGURED -> {
                lockIcon.setImageResource(R.drawable.warning_material)
                lockText.setText(R.string.use_tickets_data_error)
                lockReasonText.setText(co.bytemark.sdk.R.string.unavailable_please_contact_customer_support)
            }
            LOCKED_TO_OTHER_DEVICE -> {
                lockIcon.setImageResource(R.drawable.locked_material)
                lockText.setText(R.string.use_tickets_saved_elsewhere)
                lockReasonText.setText(co.bytemark.sdk.R.string.unavailable_saved_to_another_device)
            }
            TIME_BASED_RESTRICTION -> {
                lockIcon.setImageResource(R.drawable.time_restricted_material)
                lockText.setText(R.string.use_tickets_time_restriction)
                lockReasonText.setText(co.bytemark.sdk.R.string.unavailable_time_based_restriction)
            }
            PHOTO_REQUIRED -> {
                lockIcon.setImageResource(R.drawable.photo_restricted_material)
                lockText.setText(R.string.account_photo_required)
                lockReasonText.setText(co.bytemark.sdk.R.string.unavailable_account_photo_required)
            }
            NONE -> {
                lockIcon.setImageDrawable(null)
                lockText.text = ""
                lockReasonText.text = ""
            }
        }
    }
}