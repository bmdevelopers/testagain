package co.bytemark.use_tickets.viewholder

import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.model.config.RowType
import co.bytemark.use_tickets.passview.SingleItemRowHolder
import java.util.*
import javax.inject.Inject

class FareMediumViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var blockedMessageBanner: LinearLayout = itemView.findViewById(R.id.cardBlockedMessageBanner)
    private var card: CardView = itemView.findViewById(R.id.card)
    private var singleProduct: LinearLayout = itemView.findViewById(R.id.single_product)
    var productImage: ImageView = itemView.findViewById(R.id.iv_product_logo)
    private var passes: List<FareMedium> = ArrayList()

    @Inject
    lateinit var confHelper: ConfHelper

    init {
        CustomerMobileApp.component.inject(this)
        singleProduct.setBackgroundColor(confHelper.dataThemeBackgroundColor)
    }

    fun setPasses(passes: List<FareMedium>) {
        this.passes = passes
    }

    fun addSingleItemRow(singleItemRowType: RowType?, position: Int) {
        if (singleItemRowType != null && passes.isNotEmpty() && position != -1) {
            val baseItemRowHolder = SingleItemRowHolder(singleItemRowType, card)
            baseItemRowHolder.bindFareMedium(passes[position])
            singleProduct.addView(baseItemRowHolder.rootView)
        }
    }

    fun removeRows() {
        singleProduct.removeAllViews()
    }
}