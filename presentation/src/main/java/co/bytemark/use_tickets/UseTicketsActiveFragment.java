package co.bytemark.use_tickets;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import co.bytemark.CustomerMobileApp;
import co.bytemark.MainActivity;
import co.bytemark.R;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.analytics.AnalyticsPlatformsContract;
import co.bytemark.authentication.AuthenticationActivity;
import co.bytemark.base.BaseMvpFragment;
import co.bytemark.base.MasterActivity;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.elerts.ElertsUIComponent;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.helpers.PassDateComparator;
import co.bytemark.helpers.RxEventBus;
import co.bytemark.sdk.Api.ApiUtility;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.Pass;
import co.bytemark.tutorial.TutorialManager;
import co.bytemark.use_tickets.adapter.UseTicketsAdapterNew;
import co.bytemark.widgets.CircleBorderImageView;
import co.bytemark.widgets.ProgressViewLayout;
import co.bytemark.widgets.swiperefreshlayout.BmSwipeRefreshLayout;
import kotlin.Unit;
import rx.Observer;
import rx.functions.Func1;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static android.view.accessibility.AccessibilityEvent.TYPE_VIEW_FOCUSED;
import static co.bytemark.analytics.AnalyticsPlatformsContract.AnalyticsPlatformEntry.GOT_IT;
import static co.bytemark.analytics.AnalyticsPlatformsContract.AnalyticsPlatformEntry.SHOW_ME_LATER;
import static co.bytemark.analytics.AnalyticsPlatformsContract.AnalyticsPlatformEntry.TICKET_STORAGE_TUTORIAL_SCREEN;
import static java.lang.System.currentTimeMillis;

public class UseTicketsActiveFragment extends BaseMvpFragment<UseTickets.View, UseTickets.Presenter> implements UseTickets.View, UseTicketsAdapterNew.ClickListener {
    // No reason
    private static final int NONE = -1;
    // User navigated to v3 screen
    private static final int V3 = 0;
    // User requested sign in
    private static final int SIGN_IN = 1;
    // Buy tickets
    private static final int BUY_TICKETS = 2;

    // Buy tickets
    private static final int NOTIFICATIONS = 3;

    private static final int OFFLINE = 4;

    private static final int REFRESH = 5;

    private int count = 0;

    private boolean isUnlimitedCallbackCalled;

    private View supplementaryViewLayout;
    private List<Pass> passesToDisplay = new ArrayList<>();
    private boolean isItFirstTime = true;
    private List<Pass> totalPasses = new ArrayList<>();
    private LinearLayout selectedOverlay;
    private int previousNavigateAwayReason;
    private Pass passToRepurchase;
    private BottomSheetDialog moreOptionsBottomSheetDialog;
    private PassActivationReceiver passActivationReceiver;

    // To properly determine navigation cause and then refresh the use tickets screen.
    // Annotation is better than maintaining flags.
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({V3, SIGN_IN, BUY_TICKETS, NONE, NOTIFICATIONS, OFFLINE, REFRESH})
    private @interface NavigateReason {
    }

    @UseTicketsActiveFragment.NavigateReason
    private static int navigateAwayReason = NONE;


    @BindView(R.id.ll_loading_use_tickets)
    LinearLayout loadingUseTickets;
    @BindView(R.id.ll_no_tickets)
    LinearLayout linearLayoutNoTickets;
    @BindView(R.id.active_tickets_layout)
    LinearLayout activeTicketsLayout;
    @BindView(R.id.ll_no_virtual_fare_medium)
    LinearLayout linearLayoutNoVirtualFareMedium;

    @BindView(R.id.imageViewNoFareMedium)
    CircleBorderImageView imageViewNoFareMedium;
    @BindView(R.id.btn_use_tickets_no_fare_medium)
    Button buttonNoFareMedium;

    @BindView(R.id.ll_network_connectivity_error_view)
    LinearLayout linearLayoutNetworkDown;
    @BindView(R.id.loginParentLinearLayout)
    LinearLayout linearLayoutLogin;
    @BindView(R.id.ll_no_tickets_text)
    LinearLayout linearLayoutNoTicketsText;
    @BindView(R.id.ll_no_network_texts)
    LinearLayout linearLayoutNoNetworkText;
    @BindView(R.id.loginLinearLayout)
    LinearLayout linearLayoutNoLoggedInText;
    @BindView(R.id.pvl)
    ProgressViewLayout progressViewLayout;
    @BindView(R.id.iv_loading_tickets)
    ImageView imageViewLoadingTickets;
    @BindView(R.id.iv_network_error)
    CircleBorderImageView imageViewNetworkError;
    @BindView(R.id.use_tickets_image_view)
    CircleBorderImageView imageViewNoTickets;
    @BindView(R.id.use_tickets_logged_out_image)
    CircleBorderImageView imageViewLoggedOut;
    @BindView(R.id.btn_buy_tickets)
    Button buttonBuyTickets;
    @BindView(R.id.btn_buy_tickets_no_tickets)
    Button buttonBuyTicketsNoTickets;
    @BindView(R.id.btn_refresh_tickets)
    TextView buttonRefreshTickets;
    @BindView(R.id.swipeRefreshLayout)
    BmSwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.btn_use_tickets_sign_in)
    Button buttonSignIn;
    @BindView(R.id.btn_retry)
    Button buttonRetry;
    @BindViews({R.id.tv_network_error_1, R.id.tv_network_error_2, R.id.tv_loading_1,
            R.id.tv_no_tickets_1, R.id.tv_no_tickets_2, R.id.tv_no_fare_medium_1})
    List<TextView> textViewNetworkError;
    @BindView(R.id.ll_ticket_storage)
    LinearLayout linearLayoutTicketStorage;
    @BindView(R.id.tv_ticket_storage)
    TextView textViewTicketStorage;
    @BindView(R.id.btn_got_it)
    Button buttonGotIt;
    @BindView(R.id.btn_show_me_later)
    Button buttonShowMeLater;

    @BindView(R.id.active_tickets_recycler_view)
    RecyclerView activeTicketsRecycler;

    @BindView(R.id.active_tickets_text)
    TextView activeTicketsText;

    @BindView(R.id.display_button)
    Button displayButton;

    @BindView(R.id.tv_no_tickets_1)
    TextView noTicketsText;
    @BindView(R.id.tv_no_tickets_2)
    TextView tv_no_tickets_2;

    @Inject
    ConfHelper confHelper;
    @Inject
    UseTickets.Presenter presenter;
    @Inject
    AnalyticsPlatformAdapter analyticsPlatformAdapter;
    @Inject
    RxEventBus eventBus;
    @Inject
    TutorialManager tutorialManager;

    private CircleBorderImageView imageViewTapToActivate;
    private TextView textViewSupplementary;

    private UseTicketsAdapterNew useTicketsAdapterNew;
    private RecyclerView.LayoutManager layoutManager;

    // Flag to know if cloud passes were loaded.
    private boolean cloudPasses;
    // Need a flag to announce stack view list mode entry only for the first time.
    private boolean initialLoad = true;
    // Flag to track if user manually did swipe to refresh
    private boolean forceReloaded;


    private MaterialDialog cloudErrorDialog;

    private UseTicketsUIComponent.NotificationsListener notificationsListener;

    private CompositeSubscription subs = new CompositeSubscription();

    private long lastPauseMs = Long.MAX_VALUE;

    NavigateAwayReason navigateCallback;

    private UseTicketsUIComponent useTicketsUIComponent = new UseTicketsUIComponent();
    private ElertsUIComponent elertsUIComponent = new ElertsUIComponent();

    private ActiveAndAvailableUIThemes activeAndAvailableUIThemes;

    private LinearLayout previousSelectedOverlay;
    private Pass previousSelectedPass;

    private Long previousClick = 0L;

    private List<Pass> cloudActivePasses;
    private MaterialDialog transferDialog;

    private BottomSheetDialog elertsBottomSheetDialog;

    @NonNull
    @Override
    public UseTickets.Presenter createPresenter() {
        return presenter;
    }

    @Override
    protected void onInject() {
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            notificationsListener = (UseTicketsUIComponent.NotificationsListener) activity;
            navigateCallback = (NavigateAwayReason) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NavigateAwayReason and NotificationListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigateAwayReason = BUY_TICKETS;
        //getFinalActivePasses();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_active_tickets;
    }

    public void setNavigateAwayReason(int navigateReason) {
        navigateAwayReason = navigateReason;
    }

    public interface NavigateAwayReason {
        void setNavigateAwayReason(int navigateAwayReason, String fragment);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setSwipeRefreshLayoutColorSchemes();
        initSwipeRefreshLayout();
        setupBottomSheet();
        subs.add(new RxPermissions(getActivity()).request(Manifest.permission.ACCESS_FINE_LOCATION).subscribe(granted -> Timber.tag("TAG").d("Permission " + granted.toString())));

        //presenter.initialize();
        useTicketsUIComponent.initialisePresenter(presenter);
        //presenter.downloadGTFS();
        useTicketsUIComponent.downloadGTFS(presenter);

        observeEventBus();

        layoutManager = new LinearLayoutManager(getActivity());
        useTicketsAdapterNew = new UseTicketsAdapterNew(getActivity());
        useTicketsUIComponent.initialiseAdapterAndAttachToRecycler((LinearLayoutManager) layoutManager,
                activeTicketsRecycler,
                useTicketsAdapterNew,
                PassType.ACTIVE,
                this);
        UseTicketsAccessibility.announceForAccessibility(linearLayoutNoTicketsText, getString(R.string.use_tickets_you_have_no_tickets) + ". " + getString(R.string.use_tickets_you_have_no_tickets_message));
        UseTicketsAccessibility.announceForAccessibilityEmptyStates(getActivity(),
                linearLayoutNoNetworkText,
                linearLayoutNoLoggedInText);
    }

    private void setSwipeRefreshLayoutColorSchemes() {
        useTicketsUIComponent.setSwipeRefreshLayoutColorSchemes(swipeRefreshLayout, confHelper);
    }

    private void initSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(() -> {
            isUnlimitedCallbackCalled = false;
            removePassSelection();
            showLoading();
            presenter.clearPassCache();
            useTicketsUIComponent.clearPassesListAndHideButton(passesToDisplay, displayButton);
            fetchPasses();
            forceReloaded = true;
            // without this navigate away reason, whenever you refresh in active tab if there are not active tickets then
            // it automatically moves you to available tab. To avoid this we need the navigateawayreason
            navigateAwayReason = NONE;
        });
    }

    private void initAccentThemes() {
        activeAndAvailableUIThemes = new ActiveAndAvailableUIThemes.Builder()
                .addConfHelper(confHelper)
                .buyTicketsButton(buttonBuyTickets)
                .buyTicketsNoTicketsButton(buttonBuyTicketsNoTickets)
                .noFareMediumButton(buttonNoFareMedium)
                .refreshTicketsButton(buttonRefreshTickets)
                .signInButton(buttonSignIn)
                .retryButton(buttonRetry)
                .gotItButton(buttonGotIt)
                .showMeLaterButton(buttonShowMeLater)
                .noTicketsImageView(imageViewNoTickets)
                .loggedOutImageView(imageViewLoggedOut)
                .loadingTicketsImageView(imageViewLoadingTickets)
                .networkErrorImageView(imageViewNetworkError)
                .noFareMediumImageView(imageViewNoFareMedium)
                .layoutProgressView(progressViewLayout)
                .useticketsLoading(loadingUseTickets)
                .noTicketsLayout(linearLayoutNoTickets)
                .noVirtualFareMediumLayout(linearLayoutNoVirtualFareMedium)
                .networkDownLayout(linearLayoutNetworkDown)
                .ticketStorageLayout(linearLayoutTicketStorage)
                .build();
    }

    @Override
    public void onMoreDotsClick(Pass pass) {
        if (!pass.isUnavailable()) {
            passToRepurchase = pass;
            useTicketsUIComponent.assignPassToRepurchase(pass);
            if (pass.isProductRepurchaseEnabled()) {
                useTicketsUIComponent.showRepurchaseLabel();
            } else {
                useTicketsUIComponent.hideRepurchaseLabel();
            }
            if (confHelper.isEnableTransferPassInUseTicket()) {
                if (TextUtils.isEmpty(passToRepurchase.getLockedToDevice()) && passToRepurchase.isUnlimitedPassActivated() && passToRepurchase.getStatus().equals("USABLE")) {
                    useTicketsUIComponent.showMoveToCloudLabel();
                    useTicketsUIComponent.setMoveToCloudText(getActivity().getString(R.string.use_tickets_move_to_device));
                    useTicketsUIComponent.setPassType(UseTickets.Presenter.Device);
                } else if (passToRepurchase.getLockedToDeviceAppInstallationId().equals(BytemarkSDK.getAppInstallationId()) && passToRepurchase.isUnlimitedPassActivated() && passToRepurchase.getStatus().equals("USABLE")) {
                    useTicketsUIComponent.showMoveToCloudLabel();
                    useTicketsUIComponent.setMoveToCloudText(getActivity().getString(R.string.use_tickets_move_to_cloud));
                    useTicketsUIComponent.setPassType(UseTickets.Presenter.Cloud);
                } else {
                    useTicketsUIComponent.hideMoveToCloudLabel();
                }
            } else {
                useTicketsUIComponent.hideMoveToCloudLabel();
            }
            useTicketsUIComponent.assignPassToRepurchase(pass);

            if (pass.getOrderItem() != null && !TextUtils.isEmpty(pass.getOrderItem().getOrderUuid()))
                useTicketsUIComponent.showResendReceipt();
            else useTicketsUIComponent.hideResendReceipt();

            moreOptionsBottomSheetDialog.show();
        }
    }

    @Override
    public void onLoadMoreClick() {
    }

    @Override
    public void onClick(Pass pass, LinearLayout selectedOverlay, int noOfPasses) {
        if (!pass.isUnavailable() && activeTicketsRecycler.isClickable()) {
            if (isMultiPassActivationEnabled(pass)) {
                Long currentTime = Calendar.getInstance().getTimeInMillis();
                if (currentTime - previousClick > 300) {
                    previousClick = currentTime;
                    removeOrAddPassFromOrToList(pass);
                    selectUnselectPass(pass, selectedOverlay);
                    if (displayButton.getVisibility() == VISIBLE) {
                        if (passesToDisplay.isEmpty()) {
                            fadeOutButton();
                        } else {
                            displayButton.setText(getString(R.string.use_tickets_display) + " (" + passesToDisplay.size() + ")");
                            announceNoOfTicketsSelectedAccessibility();
                        }
                    } else {
                        if (noOfPasses > 1) {
                            fadeInButtonWithBounce();
                        } else {
                            removeSelectionAndDisplayTickets();
                        }
                    }
                }
            } else {
                displaySingleTicket(pass, selectedOverlay);
            }
        }
    }

    private void clearSelection() {
        displayButton.setVisibility(GONE);
        if (passesToDisplay != null) {
            passesToDisplay.clear();
        }
    }

    private void setupBottomSheet() {
        moreOptionsBottomSheetDialog = new BottomSheetDialog(getActivity());
        useTicketsUIComponent.setupBottomSheet(moreOptionsBottomSheetDialog, getActivity(), this, AppConstants.USE_TICKETS_ACTIVE_FRAGMENT_TYPE
                , confHelper.isEnableTransferPassInUseTicket());

        if(confHelper.isElertSdkEnabled()) {
            elertsBottomSheetDialog = new BottomSheetDialog(getActivity());
            elertsUIComponent.setupElertsBottomSheet(
                    elertsBottomSheetDialog,
                    getActivity(),
                    this
            );
        }
    }

    public void showElertsBottomSheet() {
        if(getActivity() != null)
            elertsBottomSheetDialog.show();
    }

    public void showTransferPassDialog() {
        dismissDialog();
        transferDialog = new MaterialDialog.Builder(getActivity())
                .content(R.string.ticket_storage_popup_transferring_passes)
                .progress(true, 0)
                .cancelable(false).show();
    }

    public void hideTransferPassDialog() {
        // This flag needs because when there is error moving device passes to cloud
        // we need to reset this flag else unlimited passes which are active will vanish
        isUnlimitedCallbackCalled = false;
        dismissDialog();
        fetchPasses();
    }

    private void dismissDialog() {
        if (transferDialog != null) {
            transferDialog.dismiss();
        }
    }


    private void announceNoOfTicketsSelectedAccessibility() {
        ViewCompat.setAccessibilityDelegate(displayButton, new EmptyStringDelegate());
        displayButton.performAccessibilityAction(AccessibilityNodeInfo.ACTION_ACCESSIBILITY_FOCUS, null);
        displayButton.performAccessibilityAction(AccessibilityNodeInfo.ACTION_CLEAR_ACCESSIBILITY_FOCUS, null);
        if (passesToDisplay.size() > 1) {
            ViewCompat.setAccessibilityDelegate(displayButton, new ActionButtonDelegate(getString(R.string.use_tickets_display) + " " + passesToDisplay.size() + " tickets"));
            //UseTicketsAccessibility.announceForAccessibility(displayButton, "Ticket activation button \n" + passesToDisplay.size() + " tickets selected");
        } else {
            ViewCompat.setAccessibilityDelegate(displayButton, new ActionButtonDelegate(getString(R.string.use_tickets_display) + " " + passesToDisplay.size() + " ticket"));
            //UseTicketsAccessibility.announceForAccessibility(displayButton, "Ticket activation button \n" + passesToDisplay.size() + " ticket selected");
        }
    }

    private boolean isMultiPassActivationEnabled(Pass pass) {
        return confHelper.getOrganization().getMultiPassesActivation() != null && confHelper.isMultiPassesActivationEnabled(pass.getIssuer().getUserUuid());
    }

    private void removeOrAddPassFromOrToList(Pass pass) {
        if (!passesToDisplay.contains(pass)) {
            passesToDisplay.add(pass);
        } else {
            passesToDisplay.remove(pass);
        }
    }

    private void fadeOutButton() {
        final Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                displayButton.setVisibility(GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        displayButton.startAnimation(anim);
    }

    private void fadeInButtonWithBounce() {
        displayButton.setVisibility(VISIBLE);
        final Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
        anim.setInterpolator(new BounceInterpolator());
        displayButton.startAnimation(anim);
        displayButton.setText(getString(R.string.use_tickets_display) + " (" + passesToDisplay.size() + ")");
        announceNoOfTicketsSelectedAccessibility();
    }

    private void displaySingleTicket(Pass pass, LinearLayout selectedOverlay) {
        registerPassActivationReceiver(1);
        selectUnselectPass(pass, selectedOverlay);
        displayButton.setVisibility(GONE);
        navigateCallback.setNavigateAwayReason(V3, AppConstants.USE_TICKETS_ACTIVE_FRAGMENT_TYPE);
        presenter.activateTickets(pass);
    }

    private void selectUnselectPass(Pass pass, LinearLayout selectedOverlay) {
        pass.setSelected(!pass.getSelected());
        if (pass.getSelected()) {
            selectedOverlay.setVisibility(View.VISIBLE);
        } else {
            selectedOverlay.setVisibility(View.GONE);
        }
    }

    private void figureOutPreviouslySelectedPass(Pass pass, LinearLayout selectedOverlay) {
        if (previousSelectedOverlay == null) {
            previousSelectedOverlay = selectedOverlay;
            previousSelectedPass = pass;
        }

        if (previousSelectedOverlay != selectedOverlay) {
            previousSelectedOverlay.setVisibility(GONE);
            previousSelectedOverlay = selectedOverlay;
            // negate the selected value only if previousSelectedPass was already selected and user clicked on other pass
            // else don't negate.
            if (previousSelectedPass.getSelected()) {
                previousSelectedPass.setSelected(!previousSelectedPass.getSelected());
            }
            previousSelectedPass = pass;
        }
    }

    @OnClick(R.id.display_button)
    public void onDisplayButtonClicked() {
        removeSelectionAndDisplayTickets();
    }

    private void removeSelectionAndDisplayTickets() {
        if (passesToDisplay.size() == 1) {
            if (isThisPassHasEnabler(passesToDisplay.get(0))) {
                isThisPassEnableeActivated(passesToDisplay.get(0));
            }
        }
        registerPassActivationReceiver(passesToDisplay.size());
        presenter.activateTickets(passesToDisplay);
        displayButton.setVisibility(GONE);
        removePassSelection();
        passesToDisplay.clear();
        navigateCallback.setNavigateAwayReason(V3, AppConstants.USE_TICKETS_ACTIVE_FRAGMENT_TYPE);
    }


    private void registerPassActivationReceiver(int count) {
        passActivationReceiver = new PassActivationReceiver(analyticsPlatformAdapter,
                2, count, false, false);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(passActivationReceiver, new IntentFilter(BytemarkSDK.PASS_ACTIVATION_EVENT));
    }

    private boolean isThisPassHasEnabler(Pass pass) {
        return pass.getProductEnablers() != null && pass.getProductEnablers().size() > 0;
    }

    private void isThisPassEnableeActivated(Pass enablerPass) {
        presenter.getDiskPassObservable()
                .flatMapIterable((Func1<ArrayList<Pass>, Iterable<Pass>>) passes -> passes)
                .filter(pass -> {
                    return pass.getStatus().equalsIgnoreCase("USING");
                })
                .toBlocking()
                .subscribe(new Observer<Pass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Timber.e(e.getMessage());
                    }

                    @Override
                    public void onNext(Pass pass) {
                        // Check if any of the selected pass enablers have the same product UUID
                        // as any of the active passes
                        boolean passEnableeActivated = false;
                        for (String productEnabler : enablerPass.getProductEnablers()) {
                            if (productEnabler.equalsIgnoreCase(pass.getProductUuid())) {
                                passEnableeActivated = true;
                                break;
                            }
                        }
                        if (passEnableeActivated && !isThisPassAlreadyPresent(pass)) {
                            passesToDisplay.add(pass);
                        }
                    }
                });
    }

    private boolean isThisPassAlreadyPresent(Pass pass) {
        for (Pass passToDisplay : passesToDisplay) {
            if (passToDisplay.getProductUuid().equalsIgnoreCase(pass.getProductUuid()))
                return true;
        }
        return false;
    }

    private void removePassSelection() {
        for (Pass pass : passesToDisplay) {
            if (pass.getSelected()) {
                pass.setSelected(!pass.getSelected());
            }
        }
    }

    private void observeEventBus() {
        subs.add(eventBus.observeEvents(UseTicketsEvents.ForceReload.class).subscribe(forceReload -> fetchPasses()));
        subs.add(eventBus.observeEvents(UseTicketsEvents.QueueReload.class).subscribe(queueReload -> queueReload()));
    }

    @OnClick({R.id.btn_retry, R.id.btn_refresh_tickets})
    public void retry() {
        navigateAwayReason = NONE;
        isUnlimitedCallbackCalled = false;
        clearSelection();
        fetchPasses();
    }

    @OnClick(R.id.btn_use_tickets_sign_in)
    public void signClick() {
        if (confHelper.isAuthenticationNative()) {
            Intent intent = new Intent(getActivity(), AuthenticationActivity.class);
            intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.USE_TICKETS);
            getActivity().startActivity(intent);
        } else {
            if (isOnline())
                getActivity().startActivityForResult(new Intent(getActivity(), MainActivity.class), MasterActivity.REQUEST_CODE_LOGIN);
            else
                Snackbar.make(buttonSignIn, R.string.network_connectivity_error, Snackbar.LENGTH_LONG).show();
        }
        navigateAwayReason = SIGN_IN;
        navigateCallback.setNavigateAwayReason(SIGN_IN, AppConstants.USE_TICKETS_ACTIVE_FRAGMENT_TYPE);
    }

    @OnClick({R.id.btn_buy_tickets, R.id.btn_buy_tickets_no_tickets})
    public void onBuyTicketsClick() {
        useTicketsUIComponent.onBuyTicketsButtonClick(analyticsPlatformAdapter, (UseTicketsActivity) getActivity());
        navigateAwayReason = BUY_TICKETS;
    }

    private void populatePassesInAdapter(List<Pass> passes) {
        Collections.sort(passes, new PassDateComparator());
        totalPasses = passes;
        useTicketsUIComponent.populatePassesInAdapter(passes, useTicketsAdapterNew);
    }

    @Override
    protected void onOnline() {
        clearSelection();
        //fetchPasses();
    }

    private Unit fetchPasses() {
        useTicketsUIComponent.fetchPasses(PassType.ACTIVE,
                "active",
                presenter,
                (UseTicketsActivity) getActivity(),
                new EmptyStates(activeTicketsText,
                        linearLayoutLogin,
                        null,
                        linearLayoutNoTickets,
                        linearLayoutNoVirtualFareMedium,
                        linearLayoutNetworkDown,
                        loadingUseTickets,
                        linearLayoutNoLoggedInText,
                        null,
                        null));

        return Unit.INSTANCE;

//        if (BytemarkSDK.isLoggedIn()) {
//            presenter.loadContent(PassType.ACTIVE, "using", String.valueOf(100), String.valueOf(1));
//        } else {
//            showLoginView();
//        }
    }

    @Override
    public void setSwipeRefreshingFalse() {
        swipeRefreshLayout.setRefreshing(false);
        hideLoading();
    }

    void showLoginView() {
        if (!isOnline() && !confHelper.isNativeLogin()) {
            swipeRefreshLayout.setEnabled(false);
            useTicketsUIComponent.showLoginView(new EmptyStates(activeTicketsText,
                            linearLayoutLogin,
                            null,
                            linearLayoutNoTickets,
                            linearLayoutNoVirtualFareMedium,
                            linearLayoutNetworkDown,
                            loadingUseTickets,
                            linearLayoutNoLoggedInText,
                            null,
                            linearLayoutNoNetworkText = null),
                    (UseTicketsActivity) getActivity());
        } else {
            signClick();
        }
    }

    private void moveToActiveTabsIfActivePassesAreAvailable(boolean isActivePassesAvailable) {
        if (isActivePassesAvailable) {
            ((UseTicketsActivity) getActivity()).useTicketsViewPager.setCurrentItem(0);
        }
    }

    @Override
    protected void onOffline() {
        // presenter.cancelRequests();
        navigateAwayReason = OFFLINE;
        isUnlimitedCallbackCalled = false;
        loadingUseTickets.setVisibility(INVISIBLE);

//        if (BytemarkSDK.isLoggedIn()) {
//            if (useTicketsAdapterNew.isEmpty()) {
//                showNetworkConnectionErrorView();
//                // Handle the case where user initially comes to screen with offline mode.
//                // We should load content in that case.
//                presenter.loadContent(PassType.ACTIVE, "using", "100", "");
//            }
//        } else {
//            showLoginView();
//        }

        if (displayButton.getVisibility() == VISIBLE) {
            displayButton.setVisibility(GONE);
        }

        useTicketsUIComponent.onOffline(useTicketsAdapterNew,
                presenter,
                PassType.ACTIVE,
                AppConstants.USE_TICKETS_ACTIVE_FRAGMENT_TYPE,
                new EmptyStates(activeTicketsText,
                        linearLayoutLogin,
                        null,
                        linearLayoutNoTickets,
                        linearLayoutNoVirtualFareMedium,
                        linearLayoutNetworkDown,
                        loadingUseTickets,
                        linearLayoutNoLoggedInText,
                        null,
                        linearLayoutNoNetworkText),
                (UseTicketsActivity) getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        useTicketsUIComponent.onResume(lastPauseMs, navigateAwayReason, presenter, swipeRefreshLayout, () -> fetchPasses());
        useTicketsUIComponent.clearPassesListAndHideButton(passesToDisplay, displayButton);
        previousNavigateAwayReason = navigateAwayReason;
        navigateAwayReason = NONE;
    }

    @Override
    public void onPause() {
        super.onPause();
        lastPauseMs = currentTimeMillis();
    }

    @Override
    protected void setAccentThemeColors() {
        initAccentThemes();
        activeAndAvailableUIThemes.setAccentThemeColors();
    }

    @Override
    protected void setBackgroundThemeColors() {
        activeAndAvailableUIThemes.setBackgroundThemeColors();
        noTicketsText.setTextColor(confHelper.getBackgroundThemePrimaryTextColor());
        tv_no_tickets_2.setTextColor(confHelper.getBackgroundThemePrimaryTextColor());
    }

    @Override
    protected void setDataThemeColors() {

    }

    @Override
    public void onLoadingStarted() {
        cloudPasses = false;
        initialLoad = true;
        showLoading();
    }

    @Override
    public void showLoading() {
        activeTicketsRecycler.setClickable(false);
        useTicketsUIComponent.showLoading(new EmptyStates(
                null,
                linearLayoutLogin,
                null,
                linearLayoutNoTickets,
                linearLayoutNoVirtualFareMedium,
                linearLayoutNetworkDown,
                loadingUseTickets,
                null,
                null,
                null
        ));


        // We need circle indicator mainly for cloud passes.
        // So we will show this only when the device is online.
        if (isOnline()) {
            useTicketsUIComponent.enableSwiperefresh(swipeRefreshLayout);
        }
        UseTicketsAccessibility.announceForAccessibility(loadingUseTickets, getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        Timber.tag("TAG").d("HIDE LOADING CALLED");
//        activeTicketsText.setVisibility(VISIBLE);
//        linearLayoutLogin.setVisibility(INVISIBLE);
//        loadingUseTickets.setVisibility(INVISIBLE);

        activeTicketsRecycler.setClickable(true);
        useTicketsUIComponent.hideLoading(new EmptyStates(
                activeTicketsText,
                linearLayoutLogin,
                null,
                null,
                null,
                null,
                loadingUseTickets,
                null,
                null,
                null

        ));

        if (useTicketsAdapterNew.isEmpty()) {
            if (isOnline()) {
                if (!swipeRefreshLayout.isRefreshing()) {
                    showNoTicketsView();
                }
            } else {
                showNetworkConnectionErrorView();
            }
        } else {
            linearLayoutNoTickets.setVisibility(GONE);
            linearLayoutNetworkDown.setVisibility(GONE);
        }
    }


    @Override
    public void hideRefreshIndicatorIfCloudLoaded() {

    }

    @Override
    public void localPassesLoaded(int size, List<Pass> passes) {
        if (!isUnlimitedCallbackCalled) {
            hideLoading();
        }
//        showAdaptiveLoadingForLocalPasses(size);
    }

    private void showTicketStorageTutorial() {
        analyticsPlatformAdapter.tutorialScreenDisplayed(TICKET_STORAGE_TUTORIAL_SCREEN);
        linearLayoutTicketStorage.setVisibility(VISIBLE);
    }

    @OnClick(R.id.btn_got_it)
    public void onGotItButtonClick() {
        analyticsPlatformAdapter.tutorialButtonPressed(GOT_IT);
        tutorialManager.setPassTutorialShown();
        linearLayoutTicketStorage.setVisibility(GONE);
        presenter.clearPassCache();
        fetchPasses();
        forceReloaded = false;
    }


    // this is needed for accessibility, if we don't add
    // this click listener clicking this text selects the whole screen layout
    // instead of just the text.
//    @OnClick(R.id.active_tickets_text)
//    public void onActiveTextClick() {
//
//    }

    @OnClick(R.id.btn_show_me_later)
    public void onShowMeLaterButtonClick() {
        analyticsPlatformAdapter.tutorialButtonPressed(SHOW_ME_LATER);
        tutorialManager.setAppLaunchedFalse();
        linearLayoutTicketStorage.setVisibility(GONE);
        presenter.clearPassCache();
        fetchPasses();
        forceReloaded = false;
    }

    @Override
    public void onPassVPTDownloaded(@NonNull Pass updatedPass) {
        if (updatedPass.getStatus().equals("USING")) {
            linearLayoutNoTickets.setVisibility(GONE);
            activeTicketsLayout.setVisibility(VISIBLE);
            useTicketsAdapterNew.updateActivePasses(updatedPass);
            useTicketsAdapterNew.notifyDataSetChanged();
        }
    }

    @Nullable
    @Override
    public Pass getPassFromAdapterWithUuid(final String passUUID) {
        //return useTicketsAdapter.getPassWithUuid(passUUID);
        return null;
    }

    @Override
    public void setUnreadNotificationsCount(@NonNull int unreadNotificationsCount) {
        notificationsListener.onUnreadNotificationsCountChanged(unreadNotificationsCount);
    }

    @Override
    public void updateFareMedium(FareMedium fareMedium) {

    }

    @Override
    public void queueReload() {

    }

    @Override
    public void setRecentActivePasses(List<Pass> passes) {
//        populatePassesInAdapter(passes);
    }

    @Override
    public void closeSession() {

    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void enforceSecurityQuestions() {
    }

    @Override
    public void showResendReceiptResultDialog() {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.popup_success)
                .content(getString(R.string.sign_in_change_password_success))
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void showResendReceiptDialog() {
        dismissDialog();
        transferDialog = new MaterialDialog.Builder(getActivity())
                .content(R.string.use_tickets_resending_receipt_text)
                .progress(true, 0)
                .cancelable(false).show();
    }

    @Override
    public void hideResendReceiptDialog() {
        dismissDialog();
    }

    @Override
    public void showCreditPassDialog(boolean isSuccess, String message) {
    }

    @Override
    public void setSwipeRefreshingState() {
        swipeRefreshLayout.setRefreshing(false);
        hideLoading();
    }


    @Override
    public void cloudPassesLoaded() {
        cloudPasses = true;
        if (!isOnline()) {
            forceReloaded = false;
        }
    }

    @Override
    public void setFareMediums(@NonNull List<FareMedium> fareMedia) {
        //useTicketsAdapter.addFareMedia(fareMedia);
    }

    @Override
    public void setAvailablePasses(@NonNull List<Pass> availablePasses) {

        //hideLoading();
    }

    @Override
    public void setDeviceAvailablePasses(@NonNull List<Pass> activePasses) {
        //hideLoading();
    }

    @Override
    public void setActivePasses(@NonNull List<Pass> activePasses) {
        Timber.tag("TAG").d("ACTIVE PASSES CALLED AND NO OF ACTIVE PASSES: " + activePasses.size());
        activeTicketsLayout.setVisibility(VISIBLE);
        if (activePasses.size() > 0 && navigateAwayReason != REFRESH) {
            ((UseTicketsActivity) getActivity()).moveToActiveTabsIfActivePassesAreAvailable(true);
        } else if (activePasses.size() == 0 && navigateAwayReason != NONE) {
            ((UseTicketsActivity) getActivity()).moveToActiveTabsIfActivePassesAreAvailable(false);
        }
        //setSwipeRefreshingFalse();
        if (activePasses.size() > 0 || !isUnlimitedCallbackCalled) {
            populatePassesInAdapter(activePasses);
            hideLoading();
        }

        analyticsPlatformAdapter.devicePassesLoaded(AnalyticsPlatformsContract.PassType.ACTIVE, activePasses.size(),
                forceReloaded ? 1 : 0, AnalyticsPlatformsContract.Status.SUCCESS, "");

        //deviceActivePassesObservable.onNext(activePasses);

    }

    @Override
    public void setUnlimitedPasses(@NonNull Set<Pass> unlimitedPasses) {
        if (!isUnlimitedCallbackCalled) {
            isUnlimitedCallbackCalled = true;
            activeTicketsLayout.setVisibility(VISIBLE);
            if (unlimitedPasses.size() > 0 && navigateAwayReason != REFRESH) {
                ((UseTicketsActivity) getActivity()).moveToActiveTabsIfActivePassesAreAvailable(true);
            } else if (unlimitedPasses.size() == 0 && navigateAwayReason != NONE) {
                ((UseTicketsActivity) getActivity()).moveToActiveTabsIfActivePassesAreAvailable(false);
            }
            useTicketsAdapterNew.addPasses(unlimitedPasses);
            useTicketsAdapterNew.notifyDataSetChanged();
            hideLoading();
        }
    }

    public void setSwipeRefreshingToFalse() {
        hideLoading();
    }

    public void setUnlimitedCallbackCalled(boolean value) {
        isUnlimitedCallbackCalled = value;
    }

    /**
     * Sometimes the user will have only cloud passes and no local passes. In that case,
     * instead of showing swipe refresh layout's indicator we switch to showing normal
     * loading screen.
     * <p>
     * Without this method an empty swipe refresh only loading will be shown.
     *
     * @param size no of local passes
     */
    private void showAdaptiveLoadingForLocalPasses(int size) {
        final boolean cloudPendingToLoad = !cloudPasses && ApiUtility.internetIsAvailable(getActivity());
        if (size == 0 && cloudPendingToLoad && useTicketsAdapterNew.isEmpty()) {
            //showLoading();
        } else if (!useTicketsAdapterNew.isEmpty()) {
//            hideLoading();
        }
    }

    @Override
    public void showNoTicketsView() {
        //useTicketsUIComponent.disableSwipeRefresh(swipeRefreshLayout);
//        linearLayoutNoTickets.setVisibility(VISIBLE);
//        activeTicketsLayout.setVisibility(GONE);
//        linearLayoutNoVirtualFareMedium.setVisibility(INVISIBLE);
//        linearLayoutNetworkDown.setVisibility(INVISIBLE);
//        loadingUseTickets.setVisibility(INVISIBLE);
//        linearLayoutLogin.setVisibility(INVISIBLE);
        useTicketsUIComponent.showNoTicketsView(new EmptyStates(
                activeTicketsText,
                linearLayoutLogin,
                activeTicketsLayout,
                linearLayoutNoTickets,
                linearLayoutNoVirtualFareMedium,
                linearLayoutNetworkDown,
                loadingUseTickets,
                null,
                null,
                null

        ));
        linearLayoutNoTicketsText.sendAccessibilityEvent(TYPE_VIEW_FOCUSED);
    }


    @Override
    public void showNoVirtualFareMediumView() {
        linearLayoutNoVirtualFareMedium.setVisibility(VISIBLE);

        linearLayoutNoTickets.setVisibility(INVISIBLE);
        linearLayoutNetworkDown.setVisibility(INVISIBLE);
        loadingUseTickets.setVisibility(INVISIBLE);
        linearLayoutLogin.setVisibility(INVISIBLE);

        linearLayoutNoTicketsText.sendAccessibilityEvent(TYPE_VIEW_FOCUSED);
    }

    @Override
    public void showNetworkConnectionErrorView() {
        linearLayoutNetworkDown.setVisibility(VISIBLE);
        linearLayoutNoTickets.setVisibility(INVISIBLE);
        linearLayoutNoVirtualFareMedium.setVisibility(INVISIBLE);
        loadingUseTickets.setVisibility(INVISIBLE);
        linearLayoutLogin.setVisibility(INVISIBLE);
        linearLayoutNoNetworkText.sendAccessibilityEvent(TYPE_VIEW_FOCUSED);
        linearLayoutNoNetworkText.announceForAccessibility(getString(R.string.network_connectivity_error));
    }

    @Override
    public void hideNetworkConnectionErrorView() {
        linearLayoutNetworkDown.setVisibility(INVISIBLE);
    }

    @Override
    public void showDeviceTimeErrorDialog() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.popup_error)
                .content(R.string.device_time_error_message)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .cancelable(false)
                .onPositive((dialog, which) -> showLoginView())
                .show();
    }

    @Override
    public void showDeviceLostOrStolenError() {
        if (((UseTicketsActivity) getActivity()).getDeviceStolenFlag()) {
            showDeviceLostOrStolenErrorDialog();
        }
    }

    public Unit onDeviceLostOrStolenClick() {
        showLoginView();
        return Unit.INSTANCE;
    }

    @Override
    public void showDeviceLostOrStolenErrorDialog() {
        useTicketsUIComponent.showDeviceLostOrStolenErrorDialog(getActivity(), () -> onDeviceLostOrStolenClick());
//        if (((UseTicketsActivity)getActivity()).getDeviceStolenFlag()) {
//            ((UseTicketsActivity)getActivity()).setDeviceStolenFlag(false);
//            new MaterialDialog.Builder(getContext())
//                    .title(R.string.device_lost_stolen_title)
//                    .content(R.string.device_lost_stolen_body)
//                    .cancelable(false)
//                    .positiveText(R.string.ok)
//                    .onPositive((dialog, which) -> {
//                        ((UseTicketsActivity)getActivity()).setDeviceStolenFlag(true);
//                        onDeviceLostOrStolenButtonClick();
//                        dialog.dismiss();
//                    })
//                    .show();
//        }
    }

    @Override
    public Context getActivityContext() {
        return getActivity();
    }


    @Override
    public void showCloudPassesErrorDialog() {
        if (useTicketsAdapterNew.isEmpty()) {
            // No offline passes are there, we don't want to show an empty screen.
            // So we ll show network connection error screen which makes the most sense.
            showNetworkConnectionErrorView();
        }

        if (cloudErrorDialog != null && cloudErrorDialog.isShowing()) {
            return;
        }
    }

    @Override
    public void defaultError(@NonNull String message) {
        showDefaultErrorDialog(message);
        analyticsPlatformAdapter.devicePassesLoaded(AnalyticsPlatformsContract.PassType.ACTIVE, 0,
                forceReloaded ? 1 : 0, AnalyticsPlatformsContract.Status.FAILURE, message);
    }

    @Override
    public void showSessionExpiredErrorDialog(String message) {
        new MaterialDialog.Builder(getContext())
                .title(R.string.popup_error)
                .content(message)
                .cancelable(false)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    showLoginView();
                })
                .show();
    }

    @Override
    public void showAppUpdateDialog() {
        if (((UseTicketsActivity) getActivity()).isShouldShowAppUpdateDialog()) {
            BytemarkSDK.logout(getActivity());
            ((UseTicketsActivity) getActivity()).setShouldShowAppUpdateDialog(false);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle(R.string.popup_update_required_title);
            alertDialog.setMessage(R.string.popup_update_required_message);
            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
            alertDialog.setPositiveButton(R.string.update, (dialog, which) -> {
                ((UseTicketsActivity) getActivity()).setShouldShowAppUpdateDialog(true);
                showLoginView();
                Intent goToMarket = new Intent(Intent.ACTION_VIEW)
                        .setData(Uri.parse("market://details?id=" + getContext().getPackageName()));
                getContext().startActivity(goToMarket);
            });
            alertDialog.setNegativeButton(R.string.exit, (dialog, which) -> {
                ((UseTicketsActivity) getActivity()).setShouldShowAppUpdateDialog(true);
                dialog.cancel();
                showLoginView();
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }
    }

    @Override
    public void loadPassesOnExpiration() {
        ((UseTicketsActivity) getActivity()).clearEnablerAndSelectedTicketsList();
        getActivity().runOnUiThread(this::refreshNetworkActivity);
    }


    @Override
    public void onStop() {
        super.onStop();
        isUnlimitedCallbackCalled = false;
    }

    @Override
    public void onDestroy() {
        if (passActivationReceiver != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(passActivationReceiver);
        }
        super.onDestroy();
    }

    public void showElertsDialog(boolean isSuccess, String errorMessage, int errorCode) {
        new MaterialDialog.Builder(getActivity())
                .title(isSuccess? R.string.report_success_pop_up_title : R.string.report_failure_pop_up_title)
                .content(isSuccess ? getString(R.string.elert_report_success) : (errorCode == 0 ? getString(R.string.report_connection_error_message) : errorMessage))
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    if(!isOnline())
                        ((UseTicketsActivity) getActivity()).showCloudPassErrorDialog();
                })
                .show();
    }
}
