package co.bytemark.use_tickets

import androidx.lifecycle.ViewModel
import co.bytemark.domain.interactor.resend_receipt.ResendReceiptUseCase
import co.bytemark.domain.interactor.resend_receipt.ResendReceiptUseCaseValue
import co.bytemark.domain.interactor.ticket_history.TicketHistoryUseCase
import co.bytemark.domain.interactor.ticket_history.TicketHistoryUseCaseValue
import javax.inject.Inject

class TicketHistoryViewModel @Inject constructor(
        private val orderHistoryUseCase: TicketHistoryUseCase,
        private val resendReceiptUseCase: ResendReceiptUseCase
) : ViewModel() {

    fun getTicketHistory(perPage: Int, status: String, nextPageURL: String?) =
            orderHistoryUseCase.getLiveData(TicketHistoryUseCaseValue(perPage, status, nextPageURL))

    fun resendReceipt(orderUUID: String) = resendReceiptUseCase.getLiveData(ResendReceiptUseCaseValue(orderUUID))

}