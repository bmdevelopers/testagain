package co.bytemark.use_tickets;

/**
 * Created by Arunkumar on 16/06/17.
 */
public interface BackHandler {
    boolean onBackPressed();
}
