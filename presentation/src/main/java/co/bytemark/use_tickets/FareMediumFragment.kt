package co.bytemark.use_tickets

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.accessibility.AccessibilityEvent.TYPE_VIEW_FOCUSED
import android.widget.Button
import android.widget.LinearLayout
import androidx.core.app.TaskStackBuilder
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.OnClick
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseMvpFragment
import co.bytemark.domain.model.fare_medium.FARE_MEDIUM_BLOCK_STATE
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.ConfHelper
import co.bytemark.helpers.RxEventBus
import co.bytemark.manage.createOrLinkVirtualCard.CreateOrLinkCardActivity
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.Pass
import co.bytemark.securityquestion.SecurityQuestionActivity
import co.bytemark.transfer_virtual_card.TransferVirtualCardActivity
import co.bytemark.tutorial.TutorialManager
import co.bytemark.use_tickets.adapter.UseTicketsAdapterFareMedium
import co.bytemark.widgets.swiperefreshlayout.BmSwipeRefreshLayout
import co.bytemark.widgets.util.Util
import com.afollestad.materialdialogs.MaterialDialog
import com.tbruyelle.rxpermissions.RxPermissions
import kotlinx.android.synthetic.main.no_virtual_fare_medium_view.*
import rx.Observable
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.subscriptions.CompositeSubscription
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class FareMediumFragment : BaseMvpFragment<UseTickets.View, UseTickets.Presenter>(), UseTickets.View {

    @Inject
    lateinit var configHelper: ConfHelper

    @Inject
    lateinit var useTicketsPresenter: UseTickets.Presenter

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    @Inject
    lateinit var eventBus: RxEventBus

    @Inject
    lateinit var tutorialManager: TutorialManager

    @BindView(R.id.swipeRefreshLayout)
    lateinit var swipeRefreshLayout: BmSwipeRefreshLayout

    @BindView(R.id.fare_medium_recycler_view)
    lateinit var fareMediumRecycler: RecyclerView

    @BindView(R.id.ll_no_virtual_fare_medium)
    lateinit var linearLayoutNoVirtualFareMedium: LinearLayout

    @BindView(R.id.loginParentLinearLayout)
    lateinit var linearLayoutNotLoggedIn: LinearLayout

    @BindView(R.id.ll_network_connectivity_error_view)
    lateinit var linearLayoutNetworkDown: LinearLayout

    @BindView(R.id.loginLinearLayout)
    lateinit var linearLayoutNoLoggedInText: LinearLayout

    @BindView(R.id.ll_no_network_texts)
    lateinit var linearLayoutNoNetworkText: LinearLayout

    @BindView(R.id.btn_use_tickets_no_fare_medium)
    lateinit var buttonNoFareMedium: Button

    @BindView(R.id.btn_retry)
    lateinit var buttonRetry: Button

    @BindView(R.id.btn_use_tickets_sign_in)
    lateinit var buttonSignIn: Button

    @BindView(R.id.transferVirtualCardButton)
    lateinit var buttonTransferCard: Button

    private lateinit var fareMediumAdapter: UseTicketsAdapterFareMedium

    private var fareMediumList: List<FareMedium> = emptyList()

    private var timerSubscription: Subscription? = null
    val subs = CompositeSubscription()

    private var initQrCodeLoadSubscription: Subscription? = null

    private var transferVirtualCardEnabled: Boolean = false

    override fun showLoading() {
        swipeRefreshLayout.isRefreshing = true
        // Reset Brightness
        setUpBrightness(Util.getSystemBrightness(activity))

        // Remove QRCode loaded subscription
        removeQrCodeLoadedSubscription()
    }

    override fun hideLoading() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun showNetworkConnectionErrorView() {
        linearLayoutNetworkDown.visibility = VISIBLE
        linearLayoutNoVirtualFareMedium.visibility = INVISIBLE
        linearLayoutNoNetworkText.sendAccessibilityEvent(TYPE_VIEW_FOCUSED)
        linearLayoutNoNetworkText.announceForAccessibility(getString(R.string.network_connectivity_error))
    }

    override fun getActivityContext(): Context {
        return activity!!
    }

    override fun getPassFromAdapterWithUuid(passUUID: String?): Pass? {
        return null
    }

    override fun getLayoutRes() = R.layout.fragment_fare_medium

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setSwipeRefreshLayoutColorSchemes()
        initSwipeRefreshLayout()
        subs.add(RxPermissions(activity as UseTicketsActivity).request(Manifest.permission.ACCESS_FINE_LOCATION).subscribe { granted -> Timber.tag("TAG").d("Permission " + granted.toString()) })
        fareMediumRecycler.layoutManager = LinearLayoutManager(activity)
        fareMediumAdapter = UseTicketsAdapterFareMedium(activity)
        fareMediumRecycler.adapter = fareMediumAdapter

        transferVirtualCardEnabled = confHelper.isTransferVirtualCardEnabled
    }

    private fun setSwipeRefreshLayoutColorSchemes() {
        swipeRefreshLayout.setColorSchemeColors(
                confHelper.accentThemeBacgroundColor,
                confHelper.headerThemeAccentColor,
                confHelper.backgroundThemeBackgroundColor,
                confHelper.dataThemeAccentColor
        )
    }

    private fun initSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener {
            useTicketsPresenter.clearPassCache()
            fetchPasses()
        }
    }

    override fun onInject() {
        CustomerMobileApp.component.inject(this)
    }

    override fun onOnline() {
        useTicketsPresenter.clearPassCache()
        fetchPasses()
    }

    private fun fetchPasses() {
        if (BytemarkSDK.isLoggedIn()) {
            useTicketsPresenter.loadContent()
        } else {
            showLoginView()
        }
    }

    private fun showLoginView() {
        linearLayoutNotLoggedIn.visibility = VISIBLE
        linearLayoutNoVirtualFareMedium.visibility = INVISIBLE
        linearLayoutNetworkDown.visibility = INVISIBLE

        linearLayoutNoLoggedInText.sendAccessibilityEvent(TYPE_VIEW_FOCUSED)
        linearLayoutNoLoggedInText.announceForAccessibility(getString(R.string.you_are_signed_out))
    }

    override fun onOffline() {
        if (BytemarkSDK.isLoggedIn()) {
            if (fareMediumAdapter.isEmpty) {
                // Handle the case where user initially comes to screen with offline mode.
                // We should load content in that case.
                useTicketsPresenter.loadContent(PassType.AVAILABLE, "", "", "")
                // Adding this delay because we were seeing no fare medium empty state flash
                // before displaying cached fare medium
                Handler().postDelayed(
                        {
                            if (fareMediumAdapter.isEmpty) {
                                showNoVirtualFareMediumView()
                            }
                        },
                        500
                )
                // showNetworkConnectionErrorView()
            }
        } else {
            showLoginView()
        }
    }

    override fun createPresenter() = useTicketsPresenter

    override fun setFareMediums(fareMedia: MutableList<FareMedium>) {
        if (isFareMediumNotEmpty(fareMedia)) hideFareMediumEmptyState()
        this.fareMediumList = fareMedia
        fareMediumAdapter.addFareMedia(fareMedia)
        if (fareMedia.isNotEmpty() && fareMedia.first().state != FARE_MEDIUM_BLOCK_STATE) {
            invalidateRefreshTimer(
                    fareMedia.first().barcodePayloadV2?.refreshRate?.toLong()
                            ?: configHelper.qrCodeRefreshRate
            )
            setUpInitQrLoadSubscription()
        }
        if (isOnline) useTicketsPresenter.checkIfSecurityQuestionsAreAnswered()

        analyticsPlatformAdapter.fareMediumsLoaded(AnalyticsPlatformsContract.Status.SUCCESS, "")
    }

    override fun showResendReceiptDialog() {
    }

    private fun isFareMediumNotEmpty(fareMedium: MutableList<FareMedium>): Boolean {
        return fareMedium.size > 0
    }

    private fun hideFareMediumEmptyState() {
        linearLayoutNoVirtualFareMedium.visibility = INVISIBLE
    }

    // QR Code Visibility Subscription
    private fun setUpInitQrLoadSubscription() {
        initQrCodeLoadSubscription = eventBus
                .observeEvents(UseTicketsEvents.InitQrCodeReady::class.java)
                .onBackpressureDrop()
                .doOnError { Timber.e(it) }
                .subscribe { result -> setUpBrightness(1f) }
        subs.add(initQrCodeLoadSubscription)
    }

    // Set Brightness to full when QR code appears for init apps
    private fun setUpBrightness(brightness: Float) {
        if (isAdded && activity != null) {
            val layout = activity?.window?.attributes
            layout?.screenBrightness = brightness
            activity?.window?.attributes = layout
        }
    }

    override fun enforceSecurityQuestions() {
        startActivity(Intent(context, SecurityQuestionActivity::class.java))
    }

    override fun setUnlimitedPasses(activePasses: MutableSet<Pass>) {}

    private fun removeQrCodeLoadedSubscription() {

        // Reset Brightness
        setUpBrightness(Util.getSystemBrightness(activity))

        // Unsubscribe Any QR Code Refresh Events
        if (initQrCodeLoadSubscription != null && initQrCodeLoadSubscription?.isUnsubscribed == true) {
            initQrCodeLoadSubscription?.unsubscribe()
            subs.remove(initQrCodeLoadSubscription)
        }
    }

    override fun onStart() {
        super.onStart()
        if(fareMediumList.isNotEmpty()) {
            // this is for invalidating barcode immediately, if app came from background to foreground
            // the Barcode might have invalid timestamp.
            fareMediumAdapter.notifyDataSetChanged()
            // this is to trigger subsequent refresh
            invalidateRefreshTimer(
                    fareMediumList.first().barcodePayloadV2?.refreshRate?.toLong()
                            ?: configHelper.qrCodeRefreshRate
            )
            setUpInitQrLoadSubscription()
        }
    }


    override fun onStop() {
        unSubscribeTimer()
        super.onStop()
    }

    override fun showNoVirtualFareMediumView() {
        linearLayoutNoVirtualFareMedium.visibility = VISIBLE
        Handler().postDelayed(
                {
                    linearLayoutNoVirtualFareMedium.isFocusable = true
                    linearLayoutNoVirtualFareMedium.sendAccessibilityEvent(TYPE_VIEW_FOCUSED)
                },
                5000
        )
        if (transferVirtualCardEnabled && useTicketsPresenter.showTransferVirtualCard) {
            // hide when No inactive virtual cards available
            transferVirtualCardButton.visibility = VISIBLE
        }
        if (isOnline) useTicketsPresenter.checkIfSecurityQuestionsAreAnswered()
    }

    override fun updateFareMedium(fareMedium: FareMedium?) {
        fareMedium?.let {
            linearLayoutNetworkDown.visibility = INVISIBLE
            fareMediumAdapter.updateFareMedium(fareMedium)
        }
    }

    // Fare medium refresh timer
    private fun invalidateRefreshTimer(refreshRate: Long) {

        if (refreshRate <= 0) {
            return
        }

        unSubscribeTimer()

        timerSubscription = Observable.interval(refreshRate, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .onBackpressureDrop()
                .doOnError { Timber.e(it) }
                .doOnEach { x ->
                    fareMediumAdapter.notifyDataSetChanged()
                }
                .subscribe()

        subs.add(timerSubscription)
    }

    private fun unSubscribeTimer() {
        if (timerSubscription != null && !timerSubscription?.isUnsubscribed!!) {
            timerSubscription?.unsubscribe()
            subs.remove(timerSubscription)
        }
    }

    @OnClick(R.id.btn_use_tickets_no_fare_medium)
    fun onNoFareMediumClick() {
        activity?.let {
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
            val builder = TaskStackBuilder.create(activityContext)
            builder.addNextIntentWithParentStack(Intent(activity, CreateOrLinkCardActivity::class.java))
            builder.startActivities()
            activity?.finish()
        }
    }

    @OnClick(R.id.transferVirtualCardButton)
    fun onTransferVirtualCardClick() {
        if (isOnline) {
            MaterialDialog.Builder(context!!)
                    .title(getString(R.string.transfer_existing_virtual_card))
                    .titleColor(Color.BLACK)
                    .content(getString(R.string.msg_transfer_virtual_card_alert))
                    .positiveText(getString(R.string.continuee))
                    .positiveColor(confHelper.accentThemeBacgroundColor)
                    .onPositive { dialog, _ ->
                        dialog.dismiss()
                        val intent = Intent(activity, TransferVirtualCardActivity::class.java)
                        startActivityForResult(intent, AppConstants.TRANSFER_VIRTUAL_CARD_REQ_CODE)
                    }
                    .negativeText(getString(R.string.popup_cancel))
                    .negativeColor(confHelper.collectionThemePrimaryTextColor)
                    .onNegative { dialog, _ -> dialog.dismiss() }
                    .show()
        } else {
            connectionErrorDialog()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AppConstants.TRANSFER_VIRTUAL_CARD_REQ_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    // Reloading
                    hideFareMediumEmptyState()
                    showLoading()
                    useTicketsPresenter.forceNetworkFareMediumReload = true
                    useTicketsPresenter.clearPassCache()
                    fetchPasses()
                }
                Activity.RESULT_CANCELED -> {
                    // do nothing
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDestroy() {
        subs.clear()
        useTicketsPresenter.cleanUp(activity)
        super.onDestroy()
    }

    override fun setBackgroundThemeColors() {
        linearLayoutNoVirtualFareMedium.setBackgroundColor(confHelper.backgroundThemeBackgroundColor)
        linearLayoutNetworkDown.setBackgroundColor(confHelper.backgroundThemeBackgroundColor)
    }

    override fun setDataThemeColors() {
    }

    override fun setAccentThemeColors() {
        imageViewNoFareMedium?.imageTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        buttonSignIn.setTextColor(confHelper.backgroundThemeSecondaryTextColor)
        buttonSignIn.backgroundTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
        buttonRetry.setTextColor(confHelper.backgroundThemeSecondaryTextColor)
        buttonRetry.backgroundTintList = ColorStateList.valueOf(confHelper.backgroundThemeAccentColor)
    }

    override fun onLoadingStarted() {
    }

    override fun setSwipeRefreshingFalse() {
    }

    override fun hideRefreshIndicatorIfCloudLoaded() {
    }

    override fun setAvailablePasses(availablePasses: MutableList<Pass>) {
    }

    override fun setDeviceAvailablePasses(availablePasses: MutableList<Pass>) {
    }

    override fun setActivePasses(activePasses: MutableList<Pass>) {
    }

    override fun showNoTicketsView() {
    }

    override fun hideNetworkConnectionErrorView() {
    }

    override fun showDeviceLostOrStolenError() {
    }

    override fun showCloudPassesErrorDialog() {
    }

    override fun defaultError(message: String) {
        analyticsPlatformAdapter.fareMediumsLoaded(AnalyticsPlatformsContract.Status.FAILURE, message)
    }

    override fun showSessionExpiredErrorDialog(message: String?) {
    }

    override fun loadPassesOnExpiration() {
    }

    override fun cloudPassesLoaded() {
    }

    override fun localPassesLoaded(size: Int, passes: MutableList<Pass>?) {
    }

    override fun onPassVPTDownloaded(updatedPass: Pass) {
    }

    override fun hideResendReceiptDialog() {
    }

    override fun showCreditPassDialog(isSuccess: Boolean, message: String?) {
    }

    override fun setSwipeRefreshingState() {
        TODO("Not yet implemented")
    }

    override fun setUnreadNotificationsCount(unreadNotificationsCount: Int) {
    }

    override fun queueReload() {
    }

    override fun showResendReceiptResultDialog() {
    }

    override fun setRecentActivePasses(passes: MutableList<Pass>?) {
    }

    override fun closeSession() {
    }

    override fun onRefresh() {
    }
}
