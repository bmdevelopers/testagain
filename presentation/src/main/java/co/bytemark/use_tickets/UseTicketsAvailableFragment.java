package co.bytemark.use_tickets;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import co.bytemark.CustomerMobileApp;
import co.bytemark.MainActivity;
import co.bytemark.R;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.analytics.AnalyticsPlatformsContract;
import co.bytemark.authentication.AuthenticationActivity;
import co.bytemark.base.BaseMvpFragment;
import co.bytemark.base.MasterActivity;
import co.bytemark.buy_tickets.BuyTicketsActivity;
import co.bytemark.domain.model.credit_pass.CreditPassItemData;
import co.bytemark.domain.model.credit_pass.CreditPassRequest;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.elerts.ElertsUIComponent;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.helpers.RxEventBus;
import co.bytemark.sdk.Api.ApiUtility;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.Pass;
import co.bytemark.tutorial.TutorialManager;
import co.bytemark.use_tickets.adapter.UseTicketsAdapterNew;
import co.bytemark.widgets.CircleBorderImageView;
import co.bytemark.widgets.ProgressViewLayout;
import co.bytemark.widgets.swiperefreshlayout.BmSwipeRefreshLayout;
import kotlin.Unit;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Func1;
import rx.observables.BlockingObservable;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static android.view.accessibility.AccessibilityEvent.TYPE_VIEW_FOCUSED;
import static co.bytemark.analytics.AnalyticsPlatformsContract.AnalyticsPlatformEntry.GOT_IT;
import static co.bytemark.analytics.AnalyticsPlatformsContract.AnalyticsPlatformEntry.SHOW_ME_LATER;
import static co.bytemark.analytics.AnalyticsPlatformsContract.AnalyticsPlatformEntry.TICKET_STORAGE_TUTORIAL_SCREEN;
import static co.bytemark.helpers.AppConstants.AUTH_TOKEN_FOR_DEEPLINK;
import static co.bytemark.helpers.AppConstants.NAVIGATION_TICKET_DETAILS;
import static co.bytemark.helpers.AppConstants.PRODUCT_ID;
import static co.bytemark.helpers.AppConstants.QUANTITY;
import static co.bytemark.sdk.passcache.PassCacheManager.CACHE_INVALIDATION_TIMEOUT_MS;
import static java.lang.System.currentTimeMillis;

public class UseTicketsAvailableFragment extends BaseMvpFragment<UseTickets.View, UseTickets.Presenter> implements UseTickets.View, UseTicketsAdapterNew.ClickListener {
    // No reason
    private static final int NONE = -1;
    // User navigated to v3 screen
    private static final int V3 = 0;
    // User requested sign in
    private static final int SIGN_IN = 1;
    // Buy tickets
    private static final int BUY_TICKETS = 2;

    // Buy tickets
    private static final int NOTIFICATIONS = 3;

    private static final int OFFLINE = 4;

    private static final int REFRESH = 5;

    private int pageCount = 1;
    private int perPage = 10;

    private boolean shouldShowAppUpdateDialog = true;

    private View supplementaryViewLayout;
    private List<Pass> totalPasses = new ArrayList<>();
    private final List<Pass> recentActivePasses = new ArrayList<>();
    private List<Pass> passesToActivate = new ArrayList<>();
    private List<Pass> enableePasses = new ArrayList<>();
    private List<Pass> selectedTicketsWithoutEnablee = new ArrayList<>();
    private PublishSubject<List<Pass>> cloudAvailablePassesObservable = PublishSubject.create();
    private PublishSubject<List<Pass>> deviceAvailablePassesObservable = PublishSubject.create();
    private LinearLayout selectedOverlay;
    private boolean isNextPageAvailable;
    private Subscription d;
    private MaterialDialog transferDialog;

    private PassActivationReceiver passActivationReceiver;

    // To properly determine navigation cause and then refresh the use tickets screen.
    // Annotation is better than maintaining flags.
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({V3, SIGN_IN, BUY_TICKETS, NONE, NOTIFICATIONS, OFFLINE, REFRESH})
    private @interface NavigateReason {
    }

    @UseTicketsAvailableFragment.NavigateReason
    private static int navigateAwayReason = NONE;


    @BindView(R.id.ll_loading_use_tickets)
    LinearLayout loadingUseTickets;
    @BindView(R.id.ll_no_tickets)
    LinearLayout linearLayoutNoTickets;
    @BindView(R.id.available_tickets_layout)
    LinearLayout availableTicketsLayout;
    @BindView(R.id.ll_no_virtual_fare_medium)
    LinearLayout linearLayoutNoVirtualFareMedium;

    @BindView(R.id.imageViewNoFareMedium)
    CircleBorderImageView imageViewNoFareMedium;
    @BindView(R.id.btn_use_tickets_no_fare_medium)
    Button buttonNoFareMedium;

    @BindView(R.id.ll_network_connectivity_error_view)
    LinearLayout linearLayoutNetworkDown;
    @BindView(R.id.loginParentLinearLayout)
    LinearLayout linearLayoutLogin;
    @BindView(R.id.ll_no_tickets_text)
    LinearLayout linearLayoutNoTicketsText;
    @BindView(R.id.ll_no_network_texts)
    LinearLayout linearLayoutNoNetworkText;
    @BindView(R.id.loginLinearLayout)
    LinearLayout linearLayoutNoLoggedInText;
    @BindView(R.id.pvl)
    ProgressViewLayout progressViewLayout;
    @BindView(R.id.iv_loading_tickets)
    ImageView imageViewLoadingTickets;
    @BindView(R.id.iv_network_error)
    CircleBorderImageView imageViewNetworkError;
    @BindView(R.id.use_tickets_image_view)
    CircleBorderImageView imageViewNoTickets;
    @BindView(R.id.use_tickets_logged_out_image)
    CircleBorderImageView imageViewLoggedOut;
    @BindView(R.id.btn_buy_tickets)
    Button buttonBuyTickets;
    @BindView(R.id.btn_buy_tickets_no_tickets)
    Button buttonBuyTicketsNoTickets;
    @BindView(R.id.btn_refresh_tickets)
    TextView buttonRefreshTickets;
    @BindView(R.id.swipeRefreshLayout)
    BmSwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.btn_use_tickets_sign_in)
    Button buttonSignIn;
    @BindView(R.id.btn_retry)
    Button buttonRetry;
    @BindViews({R.id.tv_network_error_1, R.id.tv_network_error_2, R.id.tv_loading_1,
            R.id.tv_no_tickets_2, R.id.tv_no_fare_medium_1})
    List<TextView> textViewNetworkError;
    @BindView(R.id.tv_no_tickets_1)
    TextView noTicketsText;
    @BindView(R.id.ll_ticket_storage)
    LinearLayout linearLayoutTicketStorage;
    @BindView(R.id.tv_ticket_storage)
    TextView textViewTicketStorage;
    @BindView(R.id.btn_got_it)
    Button buttonGotIt;
    @BindView(R.id.btn_show_me_later)
    Button buttonShowMeLater;

    @BindView(R.id.available_tickets_recycler_view)
    RecyclerView availableTicketsRecycler;

    @BindView(R.id.activate_button)
    Button activateButton;

    @BindView(R.id.load_more_button)
    Button loadMoreButton;

    @Inject
    ConfHelper confHelper;
    @Inject
    UseTickets.Presenter presenter;
    @Inject
    AnalyticsPlatformAdapter analyticsPlatformAdapter;
    @Inject
    RxEventBus eventBus;
    @Inject
    TutorialManager tutorialManager;

    @BindView(R.id.available_ticktes_text)
    TextView availableTicketsText;

    private CircleBorderImageView imageViewTapToActivate;
    private TextView textViewSupplementary;

    private UseTicketsAdapterNew useTicketsAdapterNew;
    private RecyclerView.LayoutManager layoutManager;

    // Flag to know if cloud passes were loaded.
    private boolean cloudPasses;
    // Need a flag to announce stack view list mode entry only for the first time.
    private boolean initialLoad = true;
    // Flag to track if user manually did swipe to refresh
    private boolean forceReloaded;


    private MaterialDialog cloudErrorDialog;

    private UseTicketsUIComponent.NotificationsListener notificationsListener;

    private CompositeSubscription subs = new CompositeSubscription();

    private long lastPauseMs = Long.MAX_VALUE;

    private Pass pass;

    NavigateAwayReason navigateCallback;

    private boolean isItFirstTime = true;

    private Observable<List<Pass>> finalAvailablePassesList;

    private UseTicketsUIComponent useTicketsUIComponent = new UseTicketsUIComponent();
    private ElertsUIComponent elertsUIComponent = new ElertsUIComponent();

    private Long previousClick = 0L;

    private BottomSheetDialog moreOptionsBottomSheetDialog;

    private BottomSheetDialog elertsBottomSheetDialog;

    private Pass passToRepurchase;
    private boolean isCloudErrorDialogOnHold = false;

    @Inject
    SharedPreferences sharedPreferences;
    private boolean isCreditPassAlreadyInitiated = false;
    private SwipeRefreshLayout.OnRefreshListener refreshListener;

    public static UseTicketsAvailableFragment newInstance(boolean isFromIncomm) {
        UseTicketsAvailableFragment availableFragment = new UseTicketsAvailableFragment();
        Bundle args = new Bundle();
        args.putBoolean("isFromIncomm", isFromIncomm);
        availableFragment.setArguments(args);
        return availableFragment;
    }

    @NonNull
    @Override
    public UseTickets.Presenter createPresenter() {
        return presenter;
    }

    @Override
    protected void onInject() {
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            notificationsListener = (UseTicketsUIComponent.NotificationsListener) activity;
            navigateCallback = (NavigateAwayReason) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NavigateAwayReason and NotificationListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigateAwayReason = BUY_TICKETS;
        getFinalAvailablePasses();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_available_tickets;
    }

    public void setNavigateAwayReason(int navigateReason) {
        navigateAwayReason = navigateReason;
    }

    public interface NavigateAwayReason {
        void setNavigateAwayReason(int navigateAwayReason, String fragment);
    }

    @Override
    public void onViewCreated(android.view.View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setSwipeRefreshLayoutColorSchemes();
        initSwipeRefreshLayout();
        setupBottomSheet();
        //subscribeActivateButton();
        subs.add(new RxPermissions(getActivity()).request(Manifest.permission.ACCESS_FINE_LOCATION).subscribe(granted -> Timber.tag("TAG").d("Permission " + granted.toString())));
        presenter.initialize();
        presenter.downloadGTFS();
        observeEventBus();

        layoutManager = new LinearLayoutManager(getActivity());
        availableTicketsRecycler.setLayoutManager(layoutManager);
        useTicketsAdapterNew = new UseTicketsAdapterNew(getActivity());
        useTicketsAdapterNew.setClickListener(this);
        useTicketsAdapterNew.setPassType(PassType.AVAILABLE);
        availableTicketsRecycler.setAdapter(useTicketsAdapterNew);
        UseTicketsAccessibility.announceForAccessibility(linearLayoutNoTicketsText, getString(R.string.use_tickets_you_have_no_available_tickets) + ". " + getString(R.string.use_tickets_you_have_no_tickets_message));
        UseTicketsAccessibility.announceForAccessibilityEmptyStates(getActivity(),
                linearLayoutNoNetworkText,
                linearLayoutNoLoggedInText);
        presenter.setIncommConfigForAvailable(getArguments().getBoolean("isFromIncomm", false));
    }

    private void setSwipeRefreshLayoutColorSchemes() {
        swipeRefreshLayout.setColorSchemeColors(
                confHelper.getAccentThemeBacgroundColor(),
                confHelper.getHeaderThemeAccentColor(),
                confHelper.getBackgroundThemeBackgroundColor(),
                confHelper.getDataThemeAccentColor());
    }

    private void initSwipeRefreshLayout() {
        refreshListener = () -> {
            navigateCallback.setNavigateAwayReason(REFRESH, AppConstants.USE_TICKETS_AVAILABLE_FRAGMENT_TYPE);
            notifyRefreshToActiveFragment();
            removePassSelection();
            showLoading();
            clearEnablerAndSelectedTicketsList();
            presenter.clearPassCache();
            if (activateButton.getVisibility() == View.VISIBLE) {
                activateButton.setVisibility(View.GONE);
                passesToActivate.clear();
            }
            fetchPasses();
            forceReloaded = true;
           if(isCreditPassAlreadyInitiated) {
               swipeRefreshLayout.setRefreshing(false);
               isCreditPassAlreadyInitiated = false;
           }
        };
        swipeRefreshLayout.setOnRefreshListener(refreshListener);
    }

    private void notifyRefreshToActiveFragment() {
        ((UseTicketsActivity) getActivity()).notifyRefreshToActiveFragment();
    }

    void clearEnablerAndSelectedTicketsList() {
        enableePasses.clear();
        selectedTicketsWithoutEnablee.clear();
    }

    private void clearSelection() {
        activateButton.setVisibility(GONE);
        if (passesToActivate != null) {
            for (Pass pass : passesToActivate) {
                pass.setSelected(false);
            }
        }
        if (passesToActivate != null) {
            passesToActivate.clear();
        }
    }


    private void setupBottomSheet() {
//        moreOptionsBottomSheetDialog = new BottomSheetDialog(getActivity());
//        View bottomSheetView = LayoutInflater.from(getActivity()).inflate(R.layout.more_options_bottom_sheet, null);
//        TextView repurchase = bottomSheetView.findViewById(R.id.repurchase);
//        TextView moveToCloud = bottomSheetView.findViewById(R.id.move_to_cloud);
//        moreOptionsBottomSheetDialog.setContentView(bottomSheetView);
//        setupRepurchaseClickListener(repurchase);
//        setupMoveToCloudClickListener(moveToCloud);

        moreOptionsBottomSheetDialog = new BottomSheetDialog(getActivity());
        useTicketsUIComponent.setupBottomSheet(
                moreOptionsBottomSheetDialog,
                getActivity(),
                this,
                AppConstants.USE_TICKETS_AVAILABLE_FRAGMENT_TYPE,
                confHelper.isEnableTransferPassInUseTicket()
        );

        if(confHelper.isElertSdkEnabled()) {
            elertsBottomSheetDialog = new BottomSheetDialog(getActivity());
            elertsUIComponent.setupElertsBottomSheet(
                    elertsBottomSheetDialog,
                    getActivity(),
                    this
            );
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == AppConstants.ELERTS_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String errorMessage = data.getExtras().getString("elert_error_message", null);
                int errorCode =  data.getExtras().getInt("elert_error_code");
                showElertsDialog(errorMessage == null, errorMessage, errorCode);
            }
        }
    }

    public void showElertsBottomSheet() {
        if(getActivity() != null)
            elertsBottomSheetDialog.show();
    }
    public void showTransferPassDialog() {
        dismissDialog();
        transferDialog = new MaterialDialog.Builder(getActivity())
                .content(R.string.ticket_storage_popup_transferring_passes)
                .progress(true, 0)
                .cancelable(false).show();
    }

    public void hideTransferPassDialog() {
        dismissDialog();
        navigateCallback.setNavigateAwayReason(REFRESH, AppConstants.USE_TICKETS_AVAILABLE_FRAGMENT_TYPE);
        fetchPasses();
    }


    private void dismissDialog() {
        if (transferDialog != null) {
            transferDialog.dismiss();
        }
    }


//    private void setupRepurchaseClickListener(TextView repurchase) {
//        repurchase.setOnClickListener(v -> {
//            moreOptionsBottomSheetDialog.hide();
//            startBuyTicketsActivity();
//        });
//    }
//
//    private void startBuyTicketsActivity() {
//        getActivity().startActivity(createBuyTicketsIntentWithExtrasAdded());
//    }
//
//    private Intent createBuyTicketsIntentWithExtrasAdded() {
//        Intent i = new Intent(getActivity(), BuyTicketsActivity.class);
//        i.putExtra("product_uuid_repurchase", passToRepurchase.getProductUuid());
//        return i;
//    }

    @Override
    public void onMoreDotsClick(Pass pass) {
        if (!pass.isUnavailable()) {
            passToRepurchase = pass;

            if (pass.isProductRepurchaseEnabled()) {
                useTicketsUIComponent.showRepurchaseLabel();
            } else {
                useTicketsUIComponent.hideRepurchaseLabel();
            }

            if (pass.isSendTicketToUserEnabled()) {
                useTicketsUIComponent.showSendTicketLabel();
            } else {
                useTicketsUIComponent.hideSendTicketLabel();
            }

            if (TextUtils.isEmpty(passToRepurchase.getLockedToDevice())) {
                useTicketsUIComponent.setMoveToCloudText(getActivity().getString(R.string.use_tickets_move_to_device));
                useTicketsUIComponent.setPassType(UseTickets.Presenter.Device);
            } else {
                useTicketsUIComponent.setMoveToCloudText(getActivity().getString(R.string.use_tickets_move_to_cloud));
                useTicketsUIComponent.setPassType(UseTickets.Presenter.Cloud);
            }
            useTicketsUIComponent.assignPassToRepurchase(pass);

            if (pass.getOrderItem() != null && !TextUtils.isEmpty(pass.getOrderItem().getOrderUuid()))
                useTicketsUIComponent.showResendReceipt();
            else useTicketsUIComponent.hideResendReceipt();

            moreOptionsBottomSheetDialog.show();
        }
    }

    @Override
    public void onLoadMoreClick() {
    }

    @Override
    public void onClick(Pass pass, LinearLayout selectedOverlay, int noOfPasses) {
        this.selectedOverlay = selectedOverlay;
        Long currentTime = Calendar.getInstance().getTimeInMillis();
        if (currentTime - previousClick > 300) {
            previousClick = currentTime;
            //presenter.activateTickets(pass);
            selectUnselectPass(pass);
            if (!pass.isUnavailable() && availableTicketsRecycler.isClickable()) {
                activateButton.clearAnimation();
                this.pass = pass;
                if (confHelper.getOrganization().getMultiPassesActivation() != null && confHelper.isMultiPassesActivationEnabled(pass.getIssuer().getUserUuid())) {
                    if (!passesToActivate.contains(pass)) {
                        passesToActivate.add(pass);
                    } else {
                        passesToActivate.remove(pass);
                    }
                    if (activateButton.getVisibility() == VISIBLE) {
                        if (passesToActivate.isEmpty()) {
                            enableePasses.clear();
                            selectedTicketsWithoutEnablee.clear();
                            final Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
                            anim.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    activateButton.setVisibility(GONE);
                                    if (isNextPageAvailable) {
                                        //loadMoreButton.setVisibility(VISIBLE);
                                    }
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });
                            activateButton.startAnimation(anim);
                        } else {
                            activateButton.setText(activateButton.getContext().getString(R.string.use_tickets_activate) + " (" + passesToActivate.size() + ")");
                            announceNoOfTicketsSelectedAccessibility();
                        }
                    } else {
                        if (noOfPasses > 1) {
                            activateButton.setText(activateButton.getContext().getString(R.string.use_tickets_activate) + " (" + passesToActivate.size() + ")");
                            activateButton.setVisibility(VISIBLE);
                            announceNoOfTicketsSelectedAccessibility();
                            loadMoreButton.setVisibility(GONE);
                            final Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                            anim.setInterpolator(new BounceInterpolator());
                            activateButton.startAnimation(anim);
                        } else {
                            removeSelectionAndActivateTickets();
                        }
                    }
                } else {
//                if (noOfPasses > 1) {
//                    passesToActivate.clear();
//                    passesToActivate.add(pass);
//
//                    if (previousSelectedOverlay == null) {
//                        previousSelectedOverlay = selectedOverlay;
//                        previousSelectedPass = pass;
//                    }
//
//                    if (previousSelectedOverlay != selectedOverlay) {
//                        previousSelectedOverlay.setVisibility(GONE);
//                        previousSelectedOverlay = selectedOverlay;
//                        // negate the selected value only if previousSelectedPass was already selected and user clicked on other pass
//                        // else don't negate.
//                        if (previousSelectedPass.getSelected()) {
//                            previousSelectedPass.setSelected(!previousSelectedPass.getSelected());
//                        }
//                        previousSelectedPass = pass;
//                    }
//
//                    if (pass.getSelected()) {
//                        passesToActivate.remove(0);
//                        selectedOverlay.setVisibility(View.GONE);
//                        final Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
//                        anim.setAnimationListener(new Animation.AnimationListener() {
//                            @Override
//                            public void onAnimationStart(Animation animation) {
//
//                            }
//
//                            @Override
//                            public void onAnimationEnd(Animation animation) {
//                                activateButton.setVisibility(GONE);
//                            }
//
//                            @Override
//                            public void onAnimationRepeat(Animation animation) {
//
//                            }
//                        });
//                        activateButton.startAnimation(anim);
//                    } else {
//                        //passesToActivate.add(pass);
//                        activateButton.setVisibility(VISIBLE);
//                        announceNoOfTicketsSelectedAccessibility();
//                        selectedOverlay.setVisibility(View.VISIBLE);
//                        final Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
//                        anim.setInterpolator(new BounceInterpolator());
//                        activateButton.startAnimation(anim);
//                        activateButton.setText("Activate (1)");
//                    }
//                    pass.setSelected(!pass.getSelected());
//                } else {
                    //presenter.activateTickets(pass);
                    passesToActivate.clear();
                    passesToActivate.add(pass);
                    removeSelectionAndActivateTickets();
//                }
                }
            }
        }
    }

    private void announceNoOfTicketsSelectedAccessibility() {
        ViewCompat.setAccessibilityDelegate(activateButton, new EmptyStringDelegate());
        // we are giving focus and removing focus for a view because whenever user swipe all the way to activate button
        // voice over was not announcing text only for the first focus.
        activateButton.performAccessibilityAction(AccessibilityNodeInfo.ACTION_ACCESSIBILITY_FOCUS, null);
        activateButton.performAccessibilityAction(AccessibilityNodeInfo.ACTION_CLEAR_ACCESSIBILITY_FOCUS, null);
        if (passesToActivate.size() > 1) {
            ViewCompat.setAccessibilityDelegate(activateButton, new ActionButtonDelegate(getContext().getString(R.string.use_tickets_activate) + " " + passesToActivate.size() + getString(R.string.use_tickets_tickets)));
            //UseTicketsAccessibility.announceForAccessibility(activateButton, "Ticket activation button \n" + passesToActivate.size() + " tickets selected");
        } else {
            //UseTicketsAccessibility.announceForAccessibility(activateButton, "Ticket activation button \n" + passesToActivate.size() + " ticket selected");
            ViewCompat.setAccessibilityDelegate(activateButton, new ActionButtonDelegate(getContext().getString(R.string.use_tickets_activate) + " " + passesToActivate.size() + getString(R.string.use_tickets_ticket)));
        }
    }

    private void selectUnselectPass(Pass pass) {
        if (!pass.isUnavailable()) {
            pass.setSelected(!pass.getSelected());
            if (pass.getSelected()) {
                selectedOverlay.setVisibility(View.VISIBLE);
            } else {
                selectedOverlay.setVisibility(View.GONE);
            }
        }
    }

    private void observeEventBus() {
        subs.add(eventBus.observeEvents(UseTicketsEvents.ForceReload.class).subscribe(forceReload -> fetchPasses()));
        subs.add(eventBus.observeEvents(UseTicketsEvents.QueueReload.class).subscribe(queueReload -> queueReload()));
        subs.add(eventBus.observeEvents(UseTicketsEvents.ClearCacheReload.class).subscribe(queueReload -> clearCacheAndReload(), Throwable::printStackTrace));
    }

    private void clearCacheAndReload() {
        showLoading();
        boolean isDevicePass = !TextUtils.isEmpty(useTicketsUIComponent.getPassToRepurchase().getLockedToDevice());
        presenter.removePass(useTicketsUIComponent.getPassToRepurchase().getUuid(), isDevicePass);
        removePassSelection();
        clearEnablerAndSelectedTicketsList();
        presenter.clearPassCache();
        if (activateButton.getVisibility() == View.VISIBLE) {
            activateButton.setVisibility(View.GONE);
            passesToActivate.clear();
        }
        fetchPasses();
        forceReloaded = true;
    }


    private void activateSingleTicket(Pass pass) {
        if (pass.getSelected()) {
            pass.setSelected(!pass.getSelected());
        }
        activateButton.setVisibility(GONE);
        navigateCallback.setNavigateAwayReason(V3, AppConstants.USE_TICKETS_AVAILABLE_FRAGMENT_TYPE);
        presenter.activateTickets(pass);
    }

    private void removeSelectionAndActivateTickets() {
        if (passesToActivate.size() == 1) {
            if (isThisPassHasEnabler(passesToActivate.get(0))) {
                isThisPassEnableeActivated(passesToActivate.get(0));
            }
            if (passesToActivate.size() > 1) {
                presenter.activateTickets(passesToActivate);
            } else {
                presenter.activateTickets(passesToActivate.get(0));
            }
        } else {
            segregateEnableeAndEnablerTickets();
            presenter.activateTickets(getFinalPassesListToBeActivated());
        }
        logTicketActivation();
        removePassSelection();
        activateButton.setVisibility(GONE);
        if (isNextPageAvailable) {
            //loadMoreButton.setVisibility(VISIBLE);
        }
        passesToActivate.clear();
        navigateCallback.setNavigateAwayReason(V3, AppConstants.USE_TICKETS_AVAILABLE_FRAGMENT_TYPE);
    }

    private void logTicketActivation() {
        int noOfTickets = passesToActivate.size();
        boolean hasEnablers = false;
        boolean hasCloudPass = false;
        for (Pass pass : passesToActivate) {
            if (!pass.getProductEnablers().isEmpty()) {
                hasEnablers = true;
            }
            if (pass.getLockedToDevice().isEmpty()) {
                hasCloudPass = true;
            }
        }
        passActivationReceiver = new PassActivationReceiver(analyticsPlatformAdapter,
                1, noOfTickets, hasCloudPass, hasEnablers);

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(passActivationReceiver, new IntentFilter(BytemarkSDK.PASS_ACTIVATION_EVENT));
    }


    private void removePassSelection() {
        for (Pass pass : passesToActivate) {
            if (pass.getSelected()) {
                pass.setSelected(!pass.getSelected());
            }
        }
    }

    private List<Pass> getFinalPassesListToBeActivated() {

        for (Pass enableePass : enableePasses) {
            isThisPassEnableeActivated(enableePass);
        }
        return passesToActivate;
    }

    private void segregateEnableeAndEnablerTickets() {

        for (Pass pass : passesToActivate) {
            if (isThisPassHasEnabler(pass)) {
                enableePasses.add(pass);
            } else {
                selectedTicketsWithoutEnablee.add(pass);
            }
        }
    }

    private boolean isThisPassHasEnabler(Pass pass) {
        return pass.getProductEnablers() != null && pass.getProductEnablers().size() > 0;
    }

    private void isThisPassEnableeActivated(Pass enablerPass) {
        presenter.getDiskPassObservable()
                .flatMapIterable((Func1<ArrayList<Pass>, Iterable<Pass>>) passes -> passes)
                .filter(pass -> {
                    return pass.getStatus().equalsIgnoreCase("USING");
                })
                .toBlocking()
                .subscribe(new Observer<Pass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Timber.e(e.getMessage());
                    }

                    @Override
                    public void onNext(Pass pass) {
                        // Check if any of the selected pass enablers have the same product UUID
                        // as any of the active passes
                        boolean passEnableeActivated = false;
                        for (String productEnabler : enablerPass.getProductEnablers()) {
                            if (productEnabler.equalsIgnoreCase(pass.getProductUuid())) {
                                passEnableeActivated = true;
                                break;
                            }
                        }
                        if (passEnableeActivated && !isThisPassAlreadyPresent(pass)) {
                            passesToActivate.add(pass);
                        }
                    }
                });
    }

    private boolean isThisPassAlreadyPresent(Pass pass) {
        for (Pass passToActivate : passesToActivate) {
            if (passToActivate.getProductUuid().equalsIgnoreCase(pass.getProductUuid()))
                return true;
        }
        return false;
    }

    private BlockingObservable<Pass> getActivePasses(Pass enablerPass) {
        return presenter.getDiskPassObservable()
                .flatMapIterable((Func1<ArrayList<Pass>, Iterable<Pass>>) passes -> passes)
                .filter(pass -> {
                    return pass.getStatus().equalsIgnoreCase("USING") && enablerPass.getProductEnablers().stream().anyMatch(s -> s.equalsIgnoreCase(pass.getProductUuid()));
                }).toBlocking();
    }

    @OnClick(R.id.activate_button)
    public void onActivateButtonClicked() {
        removeSelectionAndActivateTickets();
    }

//    private void subscribeActivateButton() {
//        d = RxView.clicks(activateButton)
//                .throttleFirst(500, TimeUnit.MILLISECONDS)
//                .subscribe(new Observer<Void>() {
//            @Override
//            public void onCompleted() {
//                activateButton.clearAnimation();
//                removeSelectionAndActivateTickets();
//            }
//
//            @Override
//            public void onError(Throwable e) {
//
//            }
//
//            @Override
//            public void onNext(Void aVoid) {
//
//            }
//        });
//    }


    @OnClick(R.id.load_more_button)
    public void onLoadMoreButtonClicked() {
        pageCount++;
        presenter.loadContent(PassType.AVAILABLE, "usable", String.valueOf(perPage), String.valueOf(pageCount));
    }

    @OnClick({R.id.btn_retry, R.id.btn_refresh_tickets})
    public void retry() {
        navigateCallback.setNavigateAwayReason(REFRESH, AppConstants.USE_TICKETS_AVAILABLE_FRAGMENT_TYPE);
        notifyRefreshToActiveFragment();
        refreshNetworkActivity();
    }

    @OnClick(R.id.btn_use_tickets_sign_in)
    public void signClick() {
        if (confHelper.isAuthenticationNative()) {
            Intent intent = new Intent(getActivity(), AuthenticationActivity.class);
            intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.USE_TICKETS);
            getActivity().startActivity(intent);
            getActivity().finish();
        } else {
            if (isOnline())
                getActivity().startActivityForResult(new Intent(getActivity(), MainActivity.class), MasterActivity.REQUEST_CODE_LOGIN);
            else
                Snackbar.make(buttonSignIn, R.string.network_connectivity_error, Snackbar.LENGTH_LONG).show();
        }
        navigateAwayReason = SIGN_IN;
        navigateCallback.setNavigateAwayReason(SIGN_IN, AppConstants.USE_TICKETS_AVAILABLE_FRAGMENT_TYPE);
    }

    @OnClick({R.id.btn_buy_tickets, R.id.btn_buy_tickets_no_tickets})
    public void onBuyTicketsClick() {
        Intent intent = new Intent(getActivity(), BuyTicketsActivity.class);
        intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.USE_TICKETS);
        getActivity().startActivity(intent);
        navigateAwayReason = BUY_TICKETS;
    }

    private void populatePassesInAdapter(List<Pass> passes) {
        totalPasses = passes;
//        if (pageCount > 1) {
//            useTicketsAdapterNew.addPasses(passes);
//            useTicketsAdapterNew.notifyDataSetChanged();
//        } else {
        useTicketsAdapterNew.setPasses(passes);
        useTicketsAdapterNew.notifyDataSetChanged();
//        }
    }

    @Override
    protected void onOnline() {
        useTicketsUIComponent.enableSwiperefresh(swipeRefreshLayout);
        clearSelection();
        presenter.clearPassCache();
        handleDeepLink();
    }

    private void handleDeepLink() {
        if(((UseTicketsActivity) getActivity()).deepLink != null) {
            if(!isCreditPassAlreadyInitiated) {
                if (BytemarkSDK.isLoggedIn()) {
                    Uri deeplinkUrl = ((UseTicketsActivity) getActivity()).deepLink;
                    ArrayList<CreditPassItemData> items = new ArrayList<>();
                    try {
                        items.add(new CreditPassItemData(deeplinkUrl.getQueryParameter(PRODUCT_ID),
                                deeplinkUrl.getQueryParameter(QUANTITY) != null ? Integer.parseInt(deeplinkUrl.getQueryParameter(QUANTITY)) : 1));
                            useTicketsUIComponent.disableSwipeRefresh(swipeRefreshLayout);
                            isCreditPassAlreadyInitiated = true;
                            presenter.creditPass(new CreditPassRequest(items, sharedPreferences.getInt(AppConstants.DEFAULT_DEVICE_STORAGE, 0) != 1, true),
                                    deeplinkUrl.getQueryParameter(AUTH_TOKEN_FOR_DEEPLINK));

                    } catch (Exception e) {
                        fetchPasses();
                    }
                } else {
                    showLoginView();
                }
            }
        } else {
            fetchPasses();
        }
    }

    private void fetchPasses() {
        if (BytemarkSDK.isLoggedIn()) {
            presenter.loadContent(PassType.AVAILABLE, "usable", String.valueOf(perPage), String.valueOf(pageCount));
        } else {
            showLoginView();
        }
    }

    void showLoginView() {
//        availableTicketsText.setVisibility(GONE);
//        linearLayoutLogin.setVisibility(VISIBLE);
//        linearLayoutNoTickets.setVisibility(INVISIBLE);
//        linearLayoutNoVirtualFareMedium.setVisibility(INVISIBLE);
//        linearLayoutNetworkDown.setVisibility(INVISIBLE);
//        loadingUseTickets.setVisibility(INVISIBLE);
//
//        linearLayoutNoLoggedInText.sendAccessibilityEvent(TYPE_VIEW_FOCUSED);
//        linearLayoutNoLoggedInText.announceForAccessibility(getString(R.string.you_are_signed_out));

        if (!isOnline() && !confHelper.isNativeLogin()) {
            swipeRefreshLayout.setEnabled(false);
            linearLayoutLogin.setVisibility(VISIBLE);
            linearLayoutNoTickets.setVisibility(INVISIBLE);
            linearLayoutNoVirtualFareMedium.setVisibility(INVISIBLE);
            linearLayoutNetworkDown.setVisibility(INVISIBLE);
            loadingUseTickets.setVisibility(INVISIBLE);

            linearLayoutNoLoggedInText.sendAccessibilityEvent(TYPE_VIEW_FOCUSED);
            linearLayoutNoLoggedInText.announceForAccessibility(getString(R.string.you_are_signed_out));
        } else {
            signClick();
        }
    }

    @Override
    protected void onOffline() {
        // presenter.cancelRequests();
        navigateAwayReason = OFFLINE;
        loadingUseTickets.setVisibility(INVISIBLE);

        if (BytemarkSDK.isLoggedIn()) {
            if (useTicketsAdapterNew.isEmpty()) {
                showNetworkConnectionErrorView();
            }

            if(((UseTicketsActivity) getActivity()).deepLink != null && isCreditPassAlreadyInitiated) {
                isCreditPassAlreadyInitiated = false;
            }
            // Handle the case where user initially comes to screen with offline mode.
            // We should load content in that case.
            presenter.loadContent(PassType.AVAILABLE, "usable", String.valueOf(perPage), String.valueOf(pageCount));
        } else {
            showLoginView();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (navigateAwayReason != NAVIGATION_TICKET_DETAILS && currentTimeMillis() - lastPauseMs >= CACHE_INVALIDATION_TIMEOUT_MS) {
            useTicketsUIComponent.enableSwiperefresh(swipeRefreshLayout);
            useTicketsUIComponent.clearPassesListAndHideButton(passesToActivate, activateButton);
            clearEnablerAndSelectedTicketsList();
            fetchPasses();
        } else {
            switch (navigateAwayReason) {
                case V3:
                case BUY_TICKETS:
                    useTicketsUIComponent.enableSwiperefresh(swipeRefreshLayout);
                    clearEnablerAndSelectedTicketsList();
                    handleDeepLink();
                    break;
                case SIGN_IN:
                case NOTIFICATIONS:
                    if (BytemarkSDK.isLoggedIn()) {
                        useTicketsUIComponent.enableSwiperefresh(swipeRefreshLayout);
                        Timber.d("User signed in newly");
                        clearEnablerAndSelectedTicketsList();
                        fetchPasses();
                    }
                    break;
                case NONE:
                    //fetchPasses();
                    Timber.i("Resuming normally");
                    break;
            }
        }
        navigateAwayReason = NONE;
        presenter.loadNotifications();
    }

    @Override
    public void onPause() {
        super.onPause();
        lastPauseMs = currentTimeMillis();
    }

    @Override
    protected void setAccentThemeColors() {
        buttonBuyTickets.setTextColor(confHelper.getBackgroundThemeSecondaryTextColor());
        buttonBuyTickets.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        buttonBuyTicketsNoTickets.setTextColor(confHelper.getBackgroundThemeSecondaryTextColor());
        buttonBuyTicketsNoTickets.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        buttonNoFareMedium.setTextColor(confHelper.getBackgroundThemeSecondaryTextColor());
        buttonNoFareMedium.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        buttonRefreshTickets.setTextColor(confHelper.getCollectionThemeAccentColor());
        //textRefreshTickets.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        buttonSignIn.setTextColor(confHelper.getBackgroundThemeSecondaryTextColor());
        buttonSignIn.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        buttonRetry.setTextColor(confHelper.getBackgroundThemeSecondaryTextColor());
        buttonRetry.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        buttonGotIt.setTextColor(confHelper.getBackgroundThemeSecondaryTextColor());
        buttonGotIt.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        buttonShowMeLater.setTextColor(confHelper.getBackgroundThemeAccentColor());
    }

    @Override
    protected void setBackgroundThemeColors() {
        imageViewNoTickets.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        imageViewLoggedOut.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        imageViewLoadingTickets.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        imageViewNetworkError.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        imageViewNoFareMedium.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        progressViewLayout.setProgressColor(confHelper.getBackgroundThemeAccentColor(), confHelper.getBackgroundThemeBackgroundColor());

        loadingUseTickets.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        linearLayoutNoTickets.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        linearLayoutNoVirtualFareMedium.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        linearLayoutNetworkDown.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        //stackViewScroller.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        //stackViewLayout.setHeaderColor(confHelper.getBackgroundThemePrimaryTextColor());
        for (TextView textView : textViewNetworkError)
            textView.setTextColor(confHelper.getBackgroundThemePrimaryTextColor());

        noTicketsText.setTextColor(confHelper.getBackgroundThemePrimaryTextColor());


        linearLayoutTicketStorage.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
    }

    @Override
    protected void setDataThemeColors() {

    }

    @Override
    public void onLoadingStarted() {
        cloudPasses = false;
        initialLoad = true;
        showLoading();
    }

    @Override
    public void showLoading() {
        availableTicketsRecycler.setClickable(false);
        loadingUseTickets.setVisibility(VISIBLE);
        linearLayoutNetworkDown.setVisibility(INVISIBLE);
        linearLayoutNoTickets.setVisibility(INVISIBLE);
        linearLayoutNoVirtualFareMedium.setVisibility(INVISIBLE);
        linearLayoutLogin.setVisibility(INVISIBLE);
        // We need circle indicator mainly for cloud passes.
        // So we will show this only when the device is online.
        if (isOnline()) {
            useTicketsUIComponent.enableSwiperefresh(swipeRefreshLayout);
        }
        loadingUseTickets.announceForAccessibility(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        availableTicketsRecycler.setClickable(true);
        availableTicketsText.setVisibility(VISIBLE);
        linearLayoutLogin.setVisibility(INVISIBLE);
        loadingUseTickets.setVisibility(INVISIBLE);

        if (useTicketsAdapterNew.isEmpty()) {
            if (isOnline()) {
                if (!swipeRefreshLayout.isRefreshing()) {
                    showNoTicketsView();
                }
            } else {
                showNetworkConnectionErrorView();
            }
        } else {
            linearLayoutNoTickets.setVisibility(GONE);
            linearLayoutNetworkDown.setVisibility(GONE);
        }
    }


    @Override
    public void hideRefreshIndicatorIfCloudLoaded() {

    }

    @Override
    public void localPassesLoaded(int size, List<Pass> passes) {
        //populatePassesInAdapter(passes);
        //showAdaptiveLoadingForLocalPasses(size);
    }

    private void showTicketStorageTutorial() {
        analyticsPlatformAdapter.tutorialScreenDisplayed(TICKET_STORAGE_TUTORIAL_SCREEN);
        linearLayoutTicketStorage.setVisibility(VISIBLE);
    }

    @OnClick(R.id.btn_got_it)
    public void onGotItButtonClick() {
        analyticsPlatformAdapter.tutorialButtonPressed(GOT_IT);
        tutorialManager.setPassTutorialShown();
        linearLayoutTicketStorage.setVisibility(GONE);
        presenter.clearPassCache();
        fetchPasses();
        forceReloaded = false;
    }

    @OnClick(R.id.btn_show_me_later)
    public void onShowMeLaterButtonClick() {
        analyticsPlatformAdapter.tutorialButtonPressed(SHOW_ME_LATER);
        tutorialManager.setAppLaunchedFalse();
        linearLayoutTicketStorage.setVisibility(GONE);
        presenter.clearPassCache();
        fetchPasses();
        forceReloaded = false;
    }

    @Override
    public void onPassVPTDownloaded(@NonNull Pass updatedPass) {
        useTicketsAdapterNew.replace(updatedPass);
        useTicketsAdapterNew.notifyDataSetChanged();
    }

    @Nullable
    @Override
    public Pass getPassFromAdapterWithUuid(final String passUUID) {
        return useTicketsAdapterNew.getPassWithUUID(passUUID);
    }

    @Override
    public void setUnreadNotificationsCount(@NonNull int unreadNotificationsCount) {
        notificationsListener.onUnreadNotificationsCountChanged(unreadNotificationsCount);
    }

    @Override
    public void updateFareMedium(FareMedium fareMedium) {

    }

    @Override
    public void queueReload() {

    }

    @Override
    public void setRecentActivePasses(List<Pass> passes) {

    }

    @Override
    public void closeSession() {

    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void enforceSecurityQuestions() {
    }

    @Override
    public void showResendReceiptResultDialog() {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.popup_success)
                .content(getString(R.string.sign_in_change_password_success))
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();
    }

    public void showElertsDialog(boolean isSuccess, String errorMessage, int errorCode) {
        new MaterialDialog.Builder(getActivity())
                .title(isSuccess? R.string.report_success_pop_up_title : R.string.report_failure_pop_up_title)
                .content(isSuccess ? getString(R.string.elert_report_success) : (errorCode == 0 ? getString(R.string.report_connection_error_message) : errorMessage))
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    isCloudErrorDialogOnHold = false;
                    if (!isOnline()) {
                        showCloudPassesErrorDialog();
                    }
                })
                .show();
    }

    public void putHoldonCloudErrorDialog() {
        isCloudErrorDialogOnHold = true;
        if (cloudErrorDialog != null && cloudErrorDialog.isShowing())
            cloudErrorDialog.dismiss();
    }
    @Override
    public void showResendReceiptDialog() {
        dismissDialog();
        transferDialog = new MaterialDialog.Builder(getActivity())
                .content(R.string.use_tickets_resending_receipt_text)
                .progress(true, 0)
                .cancelable(false).show();
    }

    @Override
    public void hideResendReceiptDialog() {
        dismissDialog();
    }

    @Override
    public void showCreditPassDialog(boolean isSuccess, String message) {
        if(getActivity() != null) {
            ((UseTicketsActivity) getActivity()).deepLink = null;
        }
        if(!isSuccess) {
            isCreditPassAlreadyInitiated = false;
        }
        new MaterialDialog.Builder(getContext())
                .title(isSuccess ? R.string.popup_success : R.string.popup_error)
                .content(isSuccess ? getString(R.string.popup_credit_pass_success) : message)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .cancelable(false)
                .onPositive((dialog, which) -> {
                    useTicketsUIComponent.enableSwiperefresh(swipeRefreshLayout);
                    dialog.dismiss();
                    fetchPasses();
                })
                .show();
    }

    @Override
    public void setSwipeRefreshingState() {
        if(isCreditPassAlreadyInitiated) {
            swipeRefreshLayout.post(() -> {
                swipeRefreshLayout.setRefreshing(true);
                refreshListener.onRefresh();
            });
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void cloudPassesLoaded() {
        cloudPasses = true;
        if (!isOnline()) {
            forceReloaded = false;
        }
    }

    @Override
    public void setFareMediums(@NonNull List<FareMedium> fareMedia) {
        //useTicketsAdapter.addFareMedia(fareMedia);
    }

    @Override
    public void setAvailablePasses(@NonNull List<Pass> availablePasses) {
        if (availablePasses.size() > 0 && availablePasses.get(0).isNextPageAvailable()) {
            isNextPageAvailable = availablePasses.get(0).isNextPageAvailable();
            //loadMoreButton.setVisibility(View.VISIBLE);
        } else {
            isNextPageAvailable = false;
            //loadMoreButton.setVisibility(View.GONE);
        }
        analyticsPlatformAdapter.cloudPassesLoaded(AnalyticsPlatformsContract.PassType.AVAILABLE, availablePasses.size(),
                forceReloaded ? 1 : 0, AnalyticsPlatformsContract.Status.SUCCESS, "");
        setSwipeRefreshingFalse();
        ((UseTicketsActivity) getActivity()).CloudPassesLoaded();
        clearSelection();
        forceReloaded = false;
        cloudAvailablePassesObservable.onNext(availablePasses);
        //deviceAvailablePassesObservable.onNext(new ArrayList<>());
    }

    @Override
    public void setDeviceAvailablePasses(@NonNull List<Pass> availablePasses) {
        analyticsPlatformAdapter.devicePassesLoaded(AnalyticsPlatformsContract.PassType.AVAILABLE, availablePasses.size(),
                forceReloaded ? 1 : 0, AnalyticsPlatformsContract.Status.SUCCESS, "");
        deviceAvailablePassesObservable.onNext(availablePasses);
        if (swipeRefreshLayout.isRefreshing()) {
            cloudAvailablePassesObservable.onNext(new ArrayList<>());
        }
    }

    @Override
    public void setSwipeRefreshingFalse() {
        swipeRefreshLayout.setRefreshing(false);
    }


    private void getFinalAvailablePasses() {
        finalAvailablePassesList = Observable.combineLatest(deviceAvailablePassesObservable, cloudAvailablePassesObservable, this::populateFinalAvailable);

        finalAvailablePassesList.subscribe(new Observer<List<Pass>>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<Pass> passes) {
                populatePassesInAdapter(passes);
                hideLoading();
            }
        });
    }


    private List<Pass> populateFinalAvailable(List<Pass> deviceAvaliablePasses, List<Pass> cloudAvailablePasses) {
        List<Pass> availablePasses = new ArrayList<>();
        availablePasses.addAll(deviceAvaliablePasses);
        availablePasses.addAll(cloudAvailablePasses);
        return availablePasses;
    }

    @Override
    public void setActivePasses(@NonNull List<Pass> activePasses) {
        //populatePassesInAdapter(cloudNonActivePasses);
        //hideLoading();
        ((UseTicketsActivity) getActivity()).setActivePasses(activePasses);
    }

    @Override
    public void setUnlimitedPasses(@NonNull Set<Pass> unlimitedPasses) {
        ((UseTicketsActivity) getActivity()).setUnlimitedPasses(unlimitedPasses);
    }

    /**
     * Sometimes the user will have only cloud passes and no local passes. In that case,
     * instead of showing swipe refresh layout's indicator we switch to showing normal
     * loading screen.
     * <p>
     * Without this method an empty swipe refresh only loading will be shown.
     *
     * @param size no of local passes
     */
    private void showAdaptiveLoadingForLocalPasses(int size) {
        final boolean cloudPendingToLoad = !cloudPasses && ApiUtility.internetIsAvailable(getActivity());
        if (size == 0 && cloudPendingToLoad && useTicketsAdapterNew.isEmpty()) {
            showLoading();
        } else if (!useTicketsAdapterNew.isEmpty()) {
            hideLoading();
        }
    }

    @Override
    public void showNoTicketsView() {
        //useTicketsUIComponent.disableSwipeRefresh(swipeRefreshLayout);
        availableTicketsText.setVisibility(View.INVISIBLE);
        linearLayoutNoTickets.setVisibility(VISIBLE);
        //availableTicketsLayout.setVisibility(GONE);
        noTicketsText.setText(getString(R.string.use_tickets_you_have_no_available_tickets));
        linearLayoutNoVirtualFareMedium.setVisibility(INVISIBLE);
        linearLayoutNetworkDown.setVisibility(INVISIBLE);
        loadingUseTickets.setVisibility(INVISIBLE);
        linearLayoutLogin.setVisibility(INVISIBLE);
        linearLayoutNoTicketsText.sendAccessibilityEvent(TYPE_VIEW_FOCUSED);
    }

    @Override
    public void showNoVirtualFareMediumView() {
        linearLayoutNoVirtualFareMedium.setVisibility(VISIBLE);

        linearLayoutNoTickets.setVisibility(INVISIBLE);
        linearLayoutNetworkDown.setVisibility(INVISIBLE);
        loadingUseTickets.setVisibility(INVISIBLE);
        linearLayoutLogin.setVisibility(INVISIBLE);

        linearLayoutNoTicketsText.sendAccessibilityEvent(TYPE_VIEW_FOCUSED);
    }

    @Override
    public void showNetworkConnectionErrorView() {
        linearLayoutNetworkDown.setVisibility(VISIBLE);
        linearLayoutNoTickets.setVisibility(INVISIBLE);
        linearLayoutNoVirtualFareMedium.setVisibility(INVISIBLE);
        loadingUseTickets.setVisibility(INVISIBLE);
        linearLayoutLogin.setVisibility(INVISIBLE);
        linearLayoutNoNetworkText.sendAccessibilityEvent(TYPE_VIEW_FOCUSED);
        linearLayoutNoNetworkText.announceForAccessibility(getString(R.string.network_connectivity_error));
    }

    @Override
    public void hideNetworkConnectionErrorView() {
        linearLayoutNetworkDown.setVisibility(INVISIBLE);
    }

    @Override
    public void showDeviceTimeErrorDialog() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.popup_error)
                .content(R.string.device_time_error_message)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .cancelable(false)
                .onPositive((dialog, which) -> showLoginView())
                .show();
    }

    @Override
    public void showDeviceLostOrStolenError() {
        if (((UseTicketsActivity) getActivity()).getDeviceStolenFlag()) {
            showDeviceLostOrStolenErrorDialog();
        }
    }

    @Override
    public void showDeviceLostOrStolenErrorDialog() {
        useTicketsUIComponent.showDeviceLostOrStolenErrorDialog(getActivity(), () -> onDeviceLostOrStolenClick());
//        if (((UseTicketsActivity)getActivity()).getDeviceStolenFlag()) {
//            ((UseTicketsActivity)getActivity()).setDeviceStolenFlag(false);
//            new MaterialDialog.Builder(getContext())
//                    .title(R.string.device_lost_stolen_title)
//                    .content(R.string.device_lost_stolen_body)
//                    .cancelable(false)
//                    .positiveText(R.string.ok)
//                    .onPositive((dialog, which) -> {
//                        ((UseTicketsActivity)getActivity()).setDeviceStolenFlag(true);
//                        onDeviceLostOrStolenButtonClick();
//                        dialog.dismiss();
//                    })
//                    .show();
//        }
    }


    public Unit onDeviceLostOrStolenClick() {
        showLoginView();
        return Unit.INSTANCE;
    }

    @Override
    public Context getActivityContext() {
        return getActivity();
    }

    @Override
    public void showCloudPassesErrorDialog() {

        if (isCloudErrorDialogOnHold) {
            if (cloudErrorDialog != null && cloudErrorDialog.isShowing())
                cloudErrorDialog.dismiss();
            return;
        }
        setAvailablePasses(new ArrayList<>());
        if (useTicketsAdapterNew.isEmpty()) {
            // No offline passes are there, we don't want to show an empty screen.
            // So we ll show network connection error screen which makes the most sense.
            showNetworkConnectionErrorView();
        }

        if (cloudErrorDialog != null && cloudErrorDialog.isShowing()) {
            return;
        }

        cloudErrorDialog = new MaterialDialog.Builder(getContext())
                .title(R.string.use_tickets_popup_could_not_load_cloud)
                .content(R.string.use_tickets_popup_could_not_load_cloud_body)
                .positiveText(R.string.popup_retry)
                .negativeText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> {
                    useTicketsUIComponent.enableSwiperefresh(swipeRefreshLayout);
                    fetchPasses();
                }).show();
    }

    @Override
    public void defaultError(@NonNull String message) {
        analyticsPlatformAdapter.cloudPassesLoaded(AnalyticsPlatformsContract.PassType.AVAILABLE, 0,
                forceReloaded ? 1 : 0, AnalyticsPlatformsContract.Status.FAILURE, message);
        showDefaultErrorDialog(message);
    }

    @Override
    public void showSessionExpiredErrorDialog(String message) {
        new MaterialDialog.Builder(getContext())
                .title(R.string.popup_error)
                .content(message)
                .cancelable(false)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    showLoginView();
                })
                .show();
    }

    @Override
    public void showAppUpdateDialog() {
        //useTicketsUIComponent.showAppUpdateDialog(getActivity(), "available", this);
        if (((UseTicketsActivity) getActivity()).isShouldShowAppUpdateDialog()) {
            BytemarkSDK.logout(getActivity());
            ((UseTicketsActivity) getActivity()).setShouldShowAppUpdateDialog(false);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle(R.string.popup_update_required_title);
            alertDialog.setMessage(R.string.popup_update_required_message);
            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
            alertDialog.setPositiveButton(R.string.update, (dialog, which) -> {
                ((UseTicketsActivity) getActivity()).setShouldShowAppUpdateDialog(true);
                showLoginView();
                Intent goToMarket = new Intent(Intent.ACTION_VIEW)
                        .setData(Uri.parse("market://details?id=" + getContext().getPackageName()));
                getContext().startActivity(goToMarket);
            });
            alertDialog.setNegativeButton(R.string.exit, (dialog, which) -> {
                ((UseTicketsActivity) getActivity()).setShouldShowAppUpdateDialog(true);
                dialog.cancel();
                showLoginView();
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }
    }

    @Override
    public void loadPassesOnExpiration() {
        clearEnablerAndSelectedTicketsList();
        getActivity().runOnUiThread(this::refreshNetworkActivity);
    }

    @Override
    public void onDestroy() {
        if (passActivationReceiver != null) {
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(passActivationReceiver);
        }
        super.onDestroy();

        pageCount = 1;
    }
}
