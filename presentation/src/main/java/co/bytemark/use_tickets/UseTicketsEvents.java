package co.bytemark.use_tickets;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.sdk.Pass;

/**
 * Created by Arunkumar on 05/06/17.
 */

public class UseTicketsEvents {

    /**
     * Immediately reloads the use tickets page.
     */
    public static class ForceReload {
    }

    /**
     * Queues a reload after {@link Fragment#onResume()} is called.
     */
    public static class QueueReload {

    }

    public static class ClearCacheReload {

    }

    public static class InitQrCodeReady {
    }


    /**
     * Binds the given fare medium payload to UI. Needed to reflect latest value of fare medium.
     */
    public static class UpdateFareMedium {
        final FareMedium fareMedium;

        public UpdateFareMedium(@NonNull FareMedium fareMedium) {
            this.fareMedium = fareMedium;
        }
    }


    static class VptDownloaded {
        @NonNull
        final Pass pass;

        VptDownloaded(@NonNull Pass pass) {
            this.pass = pass;
        }
    }
}

