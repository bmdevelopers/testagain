package co.bytemark.use_tickets.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import co.bytemark.R
import co.bytemark.use_tickets.TicketHistoryFragment
import co.bytemark.use_tickets.UseTicketsActiveFragment
import co.bytemark.use_tickets.UseTicketsAvailableFragment


class UseTicketsViewPagerAdapter(fm: FragmentManager, val context: Context, private val isFromIncomm: Boolean) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> UseTicketsActiveFragment()

            1 -> UseTicketsAvailableFragment.newInstance(isFromIncomm)

            else -> TicketHistoryFragment()
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {

            0 -> context.getString(R.string.use_tickets_active)

            1 -> context.getString(R.string.use_tickets_available)

            else -> context.resources.getString(R.string.use_tickets_history)
        }
    }
}