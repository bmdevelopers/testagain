package co.bytemark.use_tickets.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.domain.model.fare_medium.fares.Fare;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.model.config.PassViewRowConfiguration;
import co.bytemark.sdk.model.config.RowType;
import co.bytemark.use_tickets.passview.PassViewFactory;
import co.bytemark.use_tickets.passview.SingleItemRowHolder;
import co.bytemark.use_tickets.viewholder.FareMediumViewHolder;
import co.bytemark.widgets.GlideRotationTransformation;

import static co.bytemark.domain.model.fare_medium.FareMediumKt.FARE_MEDIUM_BLOCK_STATE;

/**
 * Created by Anirudh on 27/07/16.
 */
public class UseTicketsAdapterFareMedium extends RecyclerView.Adapter<FareMediumViewHolder> {

    @Inject
    PassViewFactory passViewFactory;

    @Inject
    ConfHelper confHelper;

    private Context context;

    private List<FareMedium> passes = new ArrayList<FareMedium>();

    private FareMediumViewHolder viewHolder;
    private final List<FareMedium> fareMedia = new ArrayList<>();

    public UseTicketsAdapterFareMedium(Context context) {
        super();
        this.context = context;
        CustomerMobileApp.Companion.getComponent().inject(this);
    }


    public boolean isEmpty() {
        if (passes != null) {
            return passes.isEmpty();
        }
        return true;
    }

    public void setPasses(List<FareMedium> passes) {
        this.passes = passes;
        if (viewHolder != null) {
            viewHolder.setPasses(passes);
        }
    }

    public void addFareMedia(List<FareMedium> passes) {
        this.fareMedia.clear();
        this.fareMedia.addAll(passes);
    }

    @Override
    public FareMediumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int section) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fare_medium_single_row, parent, false);
        viewHolder = new FareMediumViewHolder(view);
        viewHolder.setPasses(passes);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FareMediumViewHolder holder, int position) {
        FareMedium pass = passes.get(position);
        // pass the updated passes list to view holder as well so that it does not stick to stale data
        holder.setPasses(passes);
        holder.removeRows();
        SingleItemRowHolder.accessibilityText.setLength(0);

        final RowType qrCode = new RowType();
        qrCode.setItem(RowType.IMAGE);
        holder.addSingleItemRow(qrCode, position);
        if (confHelper.getOrganizationFareMediumConfigs() != null) {
            for (PassViewRowConfiguration config : confHelper.getOrganizationFareMediumConfigs()) {
                if (config != null && config.getSingleItemRowType() != null && !config.getSingleItemRowType().getItem().isEmpty()) {
                    holder.addSingleItemRow(config.getSingleItemRowType(), position);
                }
            }
        }
        setPassImage(holder, pass);

        // Blocked message banner
        if (pass.getState() == FARE_MEDIUM_BLOCK_STATE) {
            holder.getBlockedMessageBanner().setVisibility(View.VISIBLE);
        } else {
            holder.getBlockedMessageBanner().setVisibility(View.GONE);
        }
    }

    private void setPassImage(FareMediumViewHolder holder, FareMedium pass) {
        int backgroundColor = confHelper.getDataThemeAccentColor();
        if (pass.getTheme() != null && pass.getTheme().getBackgroundColor() != null) {
            backgroundColor = Color.parseColor(pass.getTheme().getBackgroundColor());
        }
        holder.getProductImage().setBackgroundColor(backgroundColor);

        if (pass.getImagePath() != null && !pass.getImagePath().isEmpty()) {
            Glide.with(context)
                    .load(pass.getImagePath())
                    .animate(android.R.anim.fade_in)
                    .transform(new GlideRotationTransformation(context, -90))
                    .into(holder.getProductImage());
        } else {

            String uuid = confHelper.getOrganization().getUuid().replaceAll("-", "_");

            Drawable drawable = confHelper.getDrawableByName("wide_use_screen_" + uuid);
            String drawableName = "wide_use_screen_" + uuid;

            if (drawable == null) {
                drawableName = "wide_" + uuid;
            }

            Glide.with(context)
                    .load(context.getResources().getIdentifier(drawableName, "drawable", context.getPackageName()))
                    .animate(android.R.anim.fade_in)
                    .transform(new GlideRotationTransformation(context, -90))
                    .into(holder.getProductImage());
        }
    }

    @Override
    public int getItemCount() {
        if (passes != null) {
            return passes.size();
        }
        return 0;
    }

    public void updateFareMedium(FareMedium fareMedium) {
        for (int i = 0; i < fareMedia.size(); i++) {
            if (fareMedium.getUuid().equalsIgnoreCase(fareMedia.get(i).getUuid())) {
                fareMedia.set(i, fareMedium);
                setPasses(fareMedia);
                notifyItemChanged(i);
                break;
            }
        }

    }
}

