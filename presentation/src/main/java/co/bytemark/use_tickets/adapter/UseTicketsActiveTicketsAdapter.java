package co.bytemark.use_tickets.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.Pass;
import co.bytemark.sdk.model.config.PassViewRowConfiguration;
import co.bytemark.sdk.model.config.RowType;
import co.bytemark.use_tickets.passview.BaseItemRowHolder;
import co.bytemark.use_tickets.passview.PassViewFactory;
import co.bytemark.use_tickets.passview.SingleItemRowHolder;

/**
 * Created by Anirudh on 27/07/16.
 */
public class UseTicketsActiveTicketsAdapter extends RecyclerView.Adapter<UseTicketsActiveTicketsAdapter.MyViewHolder> {

    @Inject
    PassViewFactory passViewFactory;

    @Inject
    ConfHelper confHelper;

    private Context context;

    private List<Pass> passes = new ArrayList<Pass>();

    public UseTicketsActiveTicketsAdapter(Context context) {
        super();
        this.context = context;
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    public boolean isEmpty() {
        if (passes != null) {
            return passes.isEmpty();
        }
        return true;
    }

    public void setPasses(List<Pass> passes) {
        this.passes = passes;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView card;
        LinearLayout singleProduct;
//        ImageView remainingUsesImage;
//        ImageView expirationImage;
//        TextView remainingUses;
//        TextView expiration;
        TextView passType;

        public MyViewHolder(View itemView) {
            super(itemView);
//            remainingUsesImage = itemView.findViewById(R.id.remaining_uses_image);
//            expirationImage = itemView.findViewById(R.id.expiration_image);
//            remainingUses = itemView.findViewById(R.id.remaining_uses_value);
//            expiration = itemView.findViewById(R.id.expiration_value);
            passType = itemView.findViewById(R.id.pass_type);
            card = itemView.findViewById(R.id.card);
            singleProduct = itemView.findViewById(R.id.single_product);

            card.setOnClickListener(v -> {

            });
        }

        void bind(String passTypeValue) {
//            remainingUses.setText(String.valueOf(remainingUsesValue));
//            expiration.setText(expirationValue);
            passType.setText(passTypeValue);

        }

        void addSingleItemRow(@Nullable RowType singleItemRowType, int position) {
            if (singleItemRowType != null) {
                final BaseItemRowHolder baseItemRowHolder = new SingleItemRowHolder(singleItemRowType, card);
                baseItemRowHolder.bindPass(passes.get(position), false);
                singleProduct.addView(baseItemRowHolder.rootView);
            }
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int section) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.active_tickets_single_row, parent, false);
        //return passViewFactory.createPassViewHolderConfigs(parent, true);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String passType = passes.get(position).getLabelName();
        holder.bind(passType);
        if(confHelper.getOrganizationPassRowConfigs() != null) {
            for (PassViewRowConfiguration config : confHelper.getOrganizationPassRowConfigs()) {
                if (config != null && config.getSingleItemRowType() != null && !config.getSingleItemRowType().getItem().isEmpty()) {
                    holder.addSingleItemRow(config.getSingleItemRowType(), position);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (passes != null) {
            return passes.size();
        }
        return 0;
    }
}
