package co.bytemark.use_tickets.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.Pass;
import co.bytemark.sdk.model.config.ChildOrganization;
import co.bytemark.sdk.model.config.PassViewRowConfiguration;
import co.bytemark.sdk.model.config.RowType;
import co.bytemark.use_tickets.PassType;
import co.bytemark.use_tickets.passview.PassViewFactory;
import co.bytemark.use_tickets.passview.SingleItemRowHolder;
import co.bytemark.use_tickets.viewholder.SingleItemViewHolder;
import co.bytemark.widgets.GlideRotationTransformation;
import timber.log.Timber;

/**
 * Created by Anirudh on 27/07/16.
 */
public class UseTicketsAdapterNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @Inject
    PassViewFactory passViewFactory;

    @Inject
    ConfHelper confHelper;

    private Context context;

    private List<Pass> passes = new ArrayList<Pass>();
    private ClickListener clickListener;
    private Enum passType;

    private Set<Integer> pos = new HashSet<Integer>();
    private SingleItemViewHolder viewHolder;
    private boolean isRowZeroPresent;
    private boolean showLoadMoreButton;
    private static final int SINGLE_ITEM_HOLDER = 0;
    private static final int FOOTER_VIEW_HOLDER = 1;

    public UseTicketsAdapterNew(Context context) {
        super();
        this.context = context;
        CustomerMobileApp.Companion.getComponent().inject(this);
        isRowZeroPresent = isRowZeroPresentInConfig();
    }

    private boolean isRowZeroPresentInConfig() {
        if(confHelper.getOrganizationPassRowConfigs() != null) {
            for (PassViewRowConfiguration config : confHelper.getOrganizationPassRowConfigs()) {
                if (config != null && config.getSingleItemRowType().getRowNumber() == 0 && !config.getSingleItemRowType().getItem().isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isEmpty() {
        if (passes != null) {
            return passes.isEmpty();
        }
        return true;
    }

    public void setPasses(List<Pass> passes) {
        this.passes = passes;
        if (viewHolder != null) {
            viewHolder.setPasses(passes);
        }
    }

    public void addPasses(Set<Pass> passes) {
        for (Pass pass : passes) {
            if (!this.passes.contains(passes)) {
                this.passes.add(pass);
            }
        }
    }

    public void replace(Pass updatedPass) {
        if (updatedPass.getStatus().equals("USING")) {
            this.passes.remove(updatedPass);
        } else if (updatedPass.getStatus().equals("USABLE") || updatedPass.getLockedForStatusUsing()) {
            for (Pass pass : this.passes) {
                if (pass.getUuid().equals(updatedPass.getUuid()) && updatedPass.getStatus().equals("USABLE")) {
                    pass.setLockedReason(Pass.NONE);
                } else if (pass.getUuid().equals(updatedPass.getUuid()) && updatedPass.getLockedForStatusUsing()) {
                    pass.setLockedReason(Pass.LOCKED_TO_OTHER_DEVICE);
                }
            }
        }
    }

    public Pass getPassWithUUID(String uuid) {
        for (Pass pass : this.passes) {
            if (pass.getUuid().equals(uuid)) {
                return pass;
            }
        }
        return null;
    }

    public void updateActivePasses(Pass updatedPass) {
        if (this.passes.contains(updatedPass))
            this.passes.remove(updatedPass);
        this.passes.add(updatedPass);
    }

    public void clearPasses() {
        this.passes.clear();
        notifyDataSetChanged();
    }

    public void setPassType(Enum passType) {
        this.passType = passType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == SINGLE_ITEM_HOLDER) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.available_tickets_single_row, parent, false);
            viewHolder = new SingleItemViewHolder(view);
            viewHolder.setClickListener(clickListener);
            viewHolder.setPasses(passes);
            viewHolder.setPassType(passType);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_history_footer_item, parent, false);
            return new TicketHistoryFooterViewHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SingleItemViewHolder) {
            Pass pass = passes.get(position);
            // pass the updated passes list to view holder as well so that it does not stick to stale data
            ((SingleItemViewHolder) holder).setPasses(passes);
            ((SingleItemViewHolder) holder).bind(pass);
            ((SingleItemViewHolder) holder).removeRows();
            clearAccessibilityText();
            if (isRowZeroPresent) {
                ((SingleItemViewHolder) holder).addSingleItemRow(getPassDisplayNameRowType(), position);
            } else {
                ((SingleItemViewHolder) holder).addSingleItemRow(getPassDisplayNameRowType(), position);
                ((SingleItemViewHolder) holder).addSingleItemRow(getDummyRow(), position);
            }
//        holder.addSingleItemRow(getPassDisplayNameRowType(), position);
            TreeMap<Integer, RowType> sortedRowType = new TreeMap<>();
            if (passType == PassType.EXPIRED) {
                sortedRowType.put(1, getExpirationRow());
            } else {
                if(confHelper.getOrganizationPassRowConfigs() != null) {
                    for (PassViewRowConfiguration config : confHelper.getOrganizationPassRowConfigs()) {
                        if (config != null && config.getSingleItemRowType() != null && !config.getSingleItemRowType().getItem().isEmpty()) {
                            sortedRowType.put(config.getSingleItemRowType().getRowNumber(), config.getSingleItemRowType());
                            //holder.addSingleItemRow(config.getSingleItemRowType(), position);
                        }
                    }
                }
            }

            for (Map.Entry<Integer, RowType> entry : sortedRowType.entrySet()) {
                ((SingleItemViewHolder) holder).addSingleItemRow(entry.getValue(), position);
            }
            setPassImage(((SingleItemViewHolder) holder), pass);

            if (passType == PassType.EXPIRED) {
                DrawableCompat.setTint(
                        DrawableCompat.wrap(((SingleItemViewHolder) holder).getMoreDots().getDrawable()),
                        ContextCompat.getColor(context, R.color.orgBackgroundAccentColor)
                );
            } else {
                DrawableCompat.setTint(
                        DrawableCompat.wrap(((SingleItemViewHolder) holder).getMoreDots().getDrawable()),
                        ContextCompat.getColor(context, R.color.Black)
                );
            }
        } else if (holder instanceof TicketHistoryFooterViewHolder) {
            ((TicketHistoryFooterViewHolder) holder).buttonLoadMore.setVisibility(showLoadMoreButton ? View.VISIBLE : View.GONE);
            ((TicketHistoryFooterViewHolder) holder).buttonLoadMore.setOnClickListener(view -> {
                clickListener.onLoadMoreClick();
                view.setVisibility(View.GONE);
            });
        }
    }

    private void clearAccessibilityText() {
        SingleItemRowHolder.accessibilityText.setLength(0);
    }

    private RowType getPassDisplayNameRowType() {
        RowType rowType = new RowType();
        rowType.setRowNumber(-1);
        rowType.setItem(RowType.PASS_DISPLAY_NAME);
        return rowType;
    }

    private RowType getDummyRow() {
        RowType rowType = new RowType();
        rowType.setRowNumber(0);
        rowType.setItem(RowType.DUMMY);
        return rowType;
    }

    private RowType getExpirationRow() {
        RowType rowType = new RowType();
        rowType.setRowNumber(1);
        rowType.setItem(RowType.EXPIRATION);
        return rowType;
    }

    private void setPassImage(SingleItemViewHolder holder, Pass pass) {
        int backgroundColor = confHelper.getHeaderThemeBackgroundColor();
        String uuid = confHelper.getOrganization().getUuid().replaceAll("-", "_");

        Drawable drawable = null;
        String drawableName = null;
        if (passType == PassType.EXPIRED) {
            holder.getFrameLayout().setBackgroundColor(ContextCompat.getColor(context, R.color.light_grey));
            holder.getFrameLayout().getBackground().setAlpha(102);
        }
        if (pass.getTheme() != null && pass.getTheme().getBackgroundColor() != null) {
            try {
                backgroundColor = confHelper.parseColor(pass.getTheme().getBackgroundColor());
				uuid = pass.getIssuer().getUserUuid();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        } else if (confHelper.getChildOrganizations() != null && confHelper.getChildOrganizations().size() > 0) {
            try {
                for (ChildOrganization childOrganization : confHelper.getChildOrganizations()) {
                    if (childOrganization.getUuid().equals(pass.getIssuer().getUserUuid())) {
                        String color = childOrganization.getBranding().getThemes().getHeaderTheme().getBackgroundColor();
                        uuid = childOrganization.getUuid();
                        backgroundColor = confHelper.parseColor(color);
                    }
                }
            } catch (Exception e) {
                Timber.e("Failure to get drawable id. %s", e.toString());
            }
        }
        uuid = uuid.replaceAll("-", "_");

        holder.getProductImage().setBackgroundColor(backgroundColor);

		try {
			drawable = confHelper.getDrawableByName("wide_" + uuid);
			drawableName = "wide_" + uuid;
		} catch (Exception e) {
			Timber.e("Failure to get drawable id. %s", e.toString());
		}

        if (drawable == null) {
            final String parentOrgUuid = CustomerMobileApp.Companion.getConf().getOrganization().getUuid().replaceAll("-", "_");
            drawableName = "wide_" + parentOrgUuid;
        }

        if (pass.getProductImagePath() != null && !pass.getProductImagePath().isEmpty()) {
            Glide
                    .with(context)
                    .load(pass.getProductImagePath())
                    .animate(android.R.anim.fade_in)
                    .transform(new GlideRotationTransformation(context, -90))
                    .into(holder.getProductImage());
        } else if (drawableName != null) {
            Glide
                    .with(context)
                    .load(context.getResources().getIdentifier(drawableName, "drawable", context.getPackageName()))
                    .animate(android.R.anim.fade_in)
                    .transform(new GlideRotationTransformation(context, -90))
                    .into(holder.getProductImage());
        }
    }

    @Override
    public int getItemCount() {
        if (passes != null) {
            if (passType == PassType.EXPIRED && showLoadMoreButton)
                return passes.size() + 1;
            return passes.size();
        }
        return 0;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemViewType(int position) {
        if (passType == PassType.EXPIRED && position == passes.size())
            return FOOTER_VIEW_HOLDER;
        return SINGLE_ITEM_HOLDER;
    }

    private static class TicketHistoryFooterViewHolder extends RecyclerView.ViewHolder {
        Button buttonLoadMore;

        public TicketHistoryFooterViewHolder(View itemView) {
            super(itemView);
            buttonLoadMore = itemView.findViewById(R.id.buttonLoadMore);
        }
    }

    public void showLoadMoreButton(boolean showLoadMoreButton) {
        this.showLoadMoreButton = showLoadMoreButton;
    }

    public interface ClickListener {
        void onClick(Pass pass, LinearLayout selectedOverlay, int noOfPasses);

        void onMoreDotsClick(Pass pass);

        void onLoadMoreClick();
    }
}
