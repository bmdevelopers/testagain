//package co.bytemark.use_tickets.adapter;
//
//import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.view.ViewGroup;
//
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//
//import javax.inject.Inject;
//
//import co.bytemark.CustomerMobileApp;
//import co.bytemark.domain.model.fare_medium.FareMedium;
//import co.bytemark.helpers.ConfHelper;
//import co.bytemark.sdk.Pass;
//import co.bytemark.use_tickets.passview.PassViewFactory;
//import co.bytemark.use_tickets.passview.RowViewHolder;
//import co.bytemark.widgets.stackview.StackAdapter;
//import co.bytemark.widgets.stackview.StackViewLayout;
//import rx.Subscription;
//import rx.android.schedulers.AndroidSchedulers;
//import rx.schedulers.Schedulers;
//import rx.subjects.PublishSubject;
//import timber.log.Timber;
//
//import static co.bytemark.sdk.Pass.NONE;
//import static co.bytemark.widgets.stackview.StackViewLayout.FIRST;
//import static co.bytemark.widgets.stackview.StackViewLayout.FOURTH;
//import static co.bytemark.widgets.stackview.StackViewLayout.SECOND;
//import static co.bytemark.widgets.stackview.StackViewLayout.THIRD;
//
///**
// * Created by Arunkumar on 27/07/16.
// */
//public class UseTicketsAdapter extends StackAdapter<Pass, RowViewHolder> {
//    /**
//     * Publish subject to receive new list of active passes, sort them on the fly in background and
//     * finally update the list and {@link co.bytemark.widgets.stackview.StackViewLayout} on the UI thread.
//     * {@link rx.subjects.Subject} is used since they facilitate adding items to already running
//     * observable stream.
//     * <p>
//     * Eg:
//     * So to sort new active passes and add to list just call {@code activePassSorter.onNext(passList)}
//     */
//    private final PublishSubject<List<Pass>> activePassSorter = PublishSubject.create();
//
//    @Inject
//    PassViewFactory passViewFactory;
//    @Inject
//    ConfHelper confHelper;
//    /**
//     * The reference to subscription that {@link #activePassSorter} establishes. Prime need is to
//     * enable unsubscribing when the fragment is destroyed.
//     */
//    private Subscription activePassSortSubscription;
//
//    private final List<FareMedium> fareMedia = new ArrayList<>();
//
//    public UseTicketsAdapter(Context context) {
//        super(context.getApplicationContext());
//        CustomerMobileApp.Companion.getComponent().inject(this);
//
//        activePassSortSubscription = createSortingSubscription();
//    }
//
//    @Override
//    protected RowViewHolder onCreateViewHolder(int section, ViewGroup parent) {
//        switch (section) {
//            case FIRST:
//                return passViewFactory.createFareMediumHolderConfigs(parent);
//            case StackViewLayout.SECOND:
//            case StackViewLayout.THIRD:
//            case StackViewLayout.FOURTH:
//                return passViewFactory.createPassViewHolderConfigs(parent, true);
//            default:
//                return passViewFactory.createPassViewHolderConfigs(parent, true);
//        }
//    }
//
//
//    public void addFareMedia(@NonNull List<FareMedium> fareMedia) {
//        this.fareMedia.addAll(fareMedia);
//        notifyFirstListReset();
//    }
//
//    @Override
//    public int getFirstSectionCount() {
//        return fareMedia.size();
//    }
//
//    @Override
//    public void onBindFirstSectionViewHolder(RowViewHolder holder, int position) {
//        holder.bindFareMedium(fareMedia.get(position));
//    }
//
//    @Override
//    protected void bindFirstSectionView(Pass pass, int position, RowViewHolder holder) {
//        // No - op
//    }
//
//    @Override
//    protected void bindSecondSectionView(Pass pass, int position, RowViewHolder holder) {
//        holder.bindPass(pass, false);
//    }
//
//    @Override
//    protected void bindThirdSectionView(Pass pass, int position, RowViewHolder holder) {
//        holder.bindPass(pass, false);
//    }
//
//    @Override
//    protected void bindFourthSectionView(Pass data, int position, RowViewHolder holder) {
//        holder.bindPass(data, true);
//    }
//
//
//    @Override
//    public void clear() {
//        fareMedia.clear();
//        super.clear();
//    }
//
//    @Override
//    public void addSecondList(@NonNull List<Pass> secondList) {
//        if (!isSortingSubActive()) {
//            activePassSortSubscription = createSortingSubscription();
//        }
//        activePassSorter.onNext(secondList);
//    }
//
//    /**
//     * Helper to easily return the selected pass. You must provide the stack view for which the adapter
//     * is providing for else things will break.
//     *
//     * @param stackViewLayout StackView for which the adapter is attached to.
//     * @return selected card
//     */
//    @Nullable
//    public Pass getSelectedPass(@NonNull StackViewLayout stackViewLayout) {
//        return getItemAt(stackViewLayout.getSelectedSection(), stackViewLayout.getSelectedItemIndex());
//    }
//
//    /**
//     * After VPT is downloaded, we receive a new pass object with updated Locked reason.
//     * So we determine where is the pass has to go now since its VPT criteria is satisfied.
//     * <p>
//     * If any criteria is not satisfied, then pass stays in Unavailable section.
//     *
//     * @param updatedPass The pass to move
//     */
//    public void updatePassFromUnavailableSection(@NonNull final Pass updatedPass) {
//        int fromIndex = -1;
//        for (int i = 0; i < fourthData.size(); i++) {
//            final Pass pass = fourthData.get(i);
//            if (pass.getUuid().trim().equalsIgnoreCase(updatedPass.getUuid())) {
//                fromIndex = i;
//                break;
//            }
//        }
//
//        if (fromIndex == -1) {
//            Timber.e("Move command issued, but pass not found in list");
//            return;
//        }
//
//        int destination = -1;
//        // Find the destination section for the pass.
//        switch (updatedPass.getStatus()) {
//            case "USABLE":
//                destination = THIRD;
//                break;
//            case "USING":
//                destination = SECOND;
//                break;
//        }
//        // We still have some criteria mismatch.
//        if (updatedPass.getLockedReason() != NONE) {
//            // Stay in unavailable section.
//            destination = FOURTH;
//            // We don't need locked reason anymore.
//            updatedPass.setLockedReason(NONE);
//            replaceItem(updatedPass, fromIndex, destination);
//        } else if (destination != -1) {
//            // We don't need locked reason anymore.
//            fourthData.get(fromIndex).setLockedReason(NONE);
//            moveItem(FOURTH, fromIndex, destination);
//        } else {
//            Timber.e("Error in determining destination of pass");
//        }
//    }
//
//
//    private boolean isSortingSubActive() {
//        return activePassSortSubscription != null && !activePassSortSubscription.isUnsubscribed();
//    }
//
//    private Subscription createSortingSubscription() {
//        return activePassSorter
//                .subscribeOn(Schedulers.computation())
//                .map(passes -> {
//                    secondData.addAll(passes);
//                    Collections.sort(secondData, ACTIVE_PASS_COMPARATOR);
//                    return secondData;
//                })
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnNext(passes -> notifySecondListReset())
//                .doOnError(throwable -> Timber.e(throwable.toString()))
//                .doOnSubscribe(() -> Timber.d("Created sorting subscription"))
//                .subscribe();
//    }
//
//    @Override
//    public void cleanUp() {
//        super.cleanUp();
//        if (activePassSortSubscription != null) {
//            activePassSortSubscription.unsubscribe();
//        }
//        Timber.d("Active pass sorter unsubscribed");
//    }
//
//
//    private static final Comparator<Pass> ACTIVE_PASS_COMPARATOR = (lhs, rhs) -> {
//        final Calendar lhsEarliestUseEnd = lhs != null ? lhs.getEarliestPassUseEnd() : null;
//        final Calendar rhsEarliestUseEnd = rhs != null ? rhs.getEarliestPassUseEnd() : null;
//
//        if (lhsEarliestUseEnd == null ^ rhsEarliestUseEnd == null)
//            return lhs == null ? -1 : 1;
//
//        if (lhsEarliestUseEnd == null && rhsEarliestUseEnd == null) return 0;
//
//        return lhsEarliestUseEnd.after(rhsEarliestUseEnd) ? -1 : 1;
//    };
//
//    @Nullable
//    public Pass getPassWithUuid(@NonNull final String passUUID) {
//        for (Pass pass : secondData) {
//            if (pass.getUuid().equalsIgnoreCase(passUUID)) {
//                return pass;
//            }
//        }
//        for (Pass pass : thirdData) {
//            if (pass.getUuid().equalsIgnoreCase(passUUID)) {
//                return pass;
//            }
//        }
//        for (Pass pass : fourthData) {
//            if (pass.getUuid().equalsIgnoreCase(passUUID)) {
//                return pass;
//            }
//        }
//        Timber.e("Fatal, pass not found in adapter");
//        return null;
//    }
//
//    public ArrayList<Pass> getAllPasses() {
//        final ArrayList<Pass> passes = new ArrayList<>();
//        passes.addAll(secondData);
//        passes.addAll(thirdData);
//        passes.addAll(fourthData);
//        return passes;
//    }
//
//    public void updateFareMedium(FareMedium fareMedium) {
//        for (int i = 0; i < fareMedia.size(); i++) {
//            if (fareMedium.getUuid().equalsIgnoreCase(fareMedia.get(i).getUuid())) {
//                fareMedia.set(i, fareMedium);
//                notifyFirstListReset();
//                break;
//            }
//        }
//    }
//}
