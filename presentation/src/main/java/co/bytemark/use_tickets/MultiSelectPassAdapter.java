package co.bytemark.use_tickets;

import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.sdk.Pass;
import co.bytemark.use_tickets.passview.PassViewFactory;
import co.bytemark.use_tickets.passview.RowViewHolder;
import co.bytemark.widgets.util.Util;
import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * Created by Arunkumar on 13/03/17.
 */
public class MultiSelectPassAdapter extends RecyclerView.Adapter<MultiSelectPassAdapter.PassViewHolder> {
    @Inject
    PassViewFactory passViewFactory;
    /**
     * BitSet to store user selections and preserve them across scrolling and recycler view binding.
     */
    private final BitSet selectionSet = new BitSet();
    /**
     * Temporary set to store position for binding pass overlay when a selected ticket cannot be
     * enabled.
     */
    private final BitSet disablerSet = new BitSet();

    private final List<Pass> passes = new ArrayList<>();
    private final List<Pass> selectedPasses = new ArrayList<>();

    private final CompositeSubscription subs = new CompositeSubscription();

    private final PublishSubject<Integer> clicks = PublishSubject.create();
    private final PublishSubject<Pair<Boolean, Integer>> selectionChanges = PublishSubject.create();
    private final PublishSubject<Integer> snapRequests = PublishSubject.create();

    private int itemWidths = -1;

    private final Handler handler = new Handler();

    private SnapLookup snapLookup = () -> {
        return -1; // By default no position.
    };

    MultiSelectPassAdapter(@NonNull List<Pass> passes) {
        CustomerMobileApp.Companion.getComponent().inject(this);
        this.passes.clear();
        this.passes.addAll(passes);
        notifyDataSetChanged();

        subs.add(selectionChanges()
                .doOnNext(selection -> {
                    final Pass pass = getPass(selection.second);
                    final boolean selected = selection.first;
                    if (selected) {
                        if (!selectedPasses.contains(pass)) {
                            selectedPasses.add(pass);
                        }
                    } else {
                        selectedPasses.remove(pass);
                    }
                }).subscribe());
    }

    @Override
    public PassViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PassViewHolder(passViewFactory.createPassViewHolderConfigs(parent, true));
    }

    @Override
    public void onBindViewHolder(PassViewHolder holder, int position) {
        final boolean unavailable = isPositionUnavailable(position);
        final Pass pass = passes.get(position);
        holder.rowViewHolder.bindPass(pass, pass.isUnavailable(), disablerSet.get(position));

        if (itemWidths != -1) {
            holder.rowViewHolder.card.getLayoutParams().width = itemWidths;
            holder.rowViewHolder.card.requestLayout();
        }

        holder.setItemSelected(isSelected(position) && !unavailable);
    }

    /**
     * Combination of checks to determine if a pass can be enabled for selection.
     *
     * @param position Position to check.
     * @return True if its unavailable.
     */
    private boolean isPositionUnavailable(int position) {
        return passes.get(position).isUnavailable() || disablerSet.get(position);
    }

    @Override
    public int getItemCount() {
        return passes.size();
    }

    void setReferenceWidth(final int itemWidth) {
        if (itemWidth != 0 && getItemCount() > 1) {
            this.itemWidths = itemWidth - Util.dpToPx(16);
        }
        notifyDataSetChanged();
    }

    /**
     * Sets selection on the poistion and immediately queues an UI update.
     *
     * @param position Position to modify
     * @param selected True if selected.
     */
    void setSelectionAndNotify(int position, boolean selected) {
        if (isSelected(position) ^ selected) {
            setSelection(position, selected);
        }
        handler.post(() -> notifyItemChanged(position));
    }

    /**
     * Same as {@link #setSelectionAndNotify(int, boolean)} but notification is queued.
     *
     * @param position Position to modify
     * @param selected True if selected.
     */
    private void setSelection(int position, boolean selected) {
        if (isSelected(position) ^ selected) {
            selectionSet.set(position, selected);
            selectionChanges.onNext(Pair.create(selectionSet.get(position), position));
        }
    }

    /**
     * Check if a position is selected.
     *
     * @param position Position to check.
     * @return
     */
    @SuppressWarnings("unused")
    boolean isSelected(int position) {
        return selectionSet.get(position);
    }

    /**
     * Disable a particular position. On this event, selection is removed, and UI updated.
     * If you are subscribed to {@link #selectionChanges} then you will receive an change event.
     *
     * @param position Position to disable.
     */
    void setDisabled(int position) {
        if (!disablerSet.get(position)) {
            disablerSet.set(position);
            setSelectionAndNotify(position, false);
        }
    }

    /**
     * Clear disabled state on particular position. Selection is removed, and UI updated.
     * If you are subscribed to {@link #selectionChanges} then you will receive an change event.
     *
     * @param position Position to disable.
     */
    void clearDisabled(int position) {
        if (disablerSet.get(position)) {
            disablerSet.clear(position);
            setSelectionAndNotify(position, false);
        }
    }

    /**
     * Retrieve pass at particular position.
     *
     * @param position Position to get.
     * @return
     */
    @NonNull
    Pass getPass(int position) {
        return passes.get(position);
    }

    /**
     * Observe clicks on pass item as a whole.
     *
     * @return Observable emitting clicked item position.
     */
    @SuppressWarnings("unused")
    @NonNull
    Observable<Integer> passClicks() {
        return clicks.asObservable();
    }

    /**
     * Called whenever selection is changed. There can be duplicates.
     *
     * @return Observable emitting a pair of position and whether it was selected.
     */
    @NonNull
    Observable<Pair<Boolean, Integer>> selectionChanges() {
        return selectionChanges
                .asObservable()
                .distinctUntilChanged();
    }

    /**
     * Returns a list of passes that are selected.
     *
     * @return Selected passes.
     */
    @NonNull
    List<Pass> getSelectedPasses() {
        return new ArrayList<>(selectedPasses);
    }

    /**
     * Get a list of indices which are currently selected.
     *
     * @return Selected indices.
     */
    @NonNull
    List<Integer> getSelectedIndices() {
        final List<Integer> selectedIndices = new ArrayList<>();
        int i = selectionSet.nextSetBit(0);
        if (i != -1) {
            selectedIndices.add(i);
            for (i = selectionSet.nextSetBit(i + 1); i >= 0; i = selectionSet.nextSetBit(i + 1)) {
                int endOfRun = selectionSet.nextClearBit(i);
                do {
                    selectedIndices.add(i);
                }
                while (++i < endOfRun);
            }
        }
        return Collections.unmodifiableList(selectedIndices);
    }


    /**
     * Provide an implementation of {@link SnapLookup} which this adapter will use to know where the
     * RecyclerView is currently snapped to.
     * <p>
     * see {@link PassViewHolder#handlePositionClick(int)}
     *
     * @param snapLookup Implementation providing position lookup.
     */
    void setSnapLookup(@NonNull SnapLookup snapLookup) {
        this.snapLookup = snapLookup;
    }

    /**
     * This observable will emit position information whenever this adapter determines that position
     * needs to be snapped to user view.
     *
     * @return Observable emitting positions.
     */
    public Observable<Integer> snapRequests() {
        return snapRequests.asObservable();
    }

    void cleanUp() {
        handler.removeCallbacksAndMessages(null);
        subs.clear();
    }

    public void updatePass(Pass pass) {
        for (int i = 0; i < passes.size(); i++) {
            if (passes.get(i).getUuid().equalsIgnoreCase(pass.getUuid())) {
                passes.set(i, pass);
                notifyItemChanged(i);
            }
        }
    }

    class PassViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final RowViewHolder rowViewHolder;

        PassViewHolder(RowViewHolder rowViewHolder) {
            super(rowViewHolder.card);
            //rowViewHolder.onFullyVisible();

            final LayoutParams params;
            if (getItemCount() > 1) {
                params = new LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
            } else {
                params = new LayoutParams(MATCH_PARENT, WRAP_CONTENT);
            }
            final int margin = Util.dpToPx(8);
            params.setMargins(margin, margin, margin, margin);

            rowViewHolder.card.setLayoutParams(params);

            this.rowViewHolder = rowViewHolder;
            itemView.setOnClickListener(this);
        }


        /**
         * Update UI of row holder to show selection status. Broadcast event if this is a change.
         * UI update queue is not required as we are updating the UI directly.
         *
         * @param selected True if selected.
         */
        void setItemSelected(boolean selected) {
            if (selected ^ rowViewHolder.isSelected()) {
                rowViewHolder.setSelected(selected);
                MultiSelectPassAdapter.this.setSelection(getAdapterPosition(), rowViewHolder.isSelected());
            }
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {

                // We are handling clicks on the Pass user is seeing. All good.
                if (position == snapLookup.getSnappedPosition()) {
                    handlePositionClick(position);
                } else { // User clicked on a Pass on the edges, so don't do anything. Just snap to that View.
                    Timber.d("Ignoring user click as the view was hidden.");
                    snapRequests.onNext(position);
                }
            }
        }

        /**
         * Handles selection, UI updates and click broadcasts on the given position.
         *
         * @param position The position to handle clicks on.
         */
        private void handlePositionClick(int position) {
            if (isPositionUnavailable(position)) {
                setItemSelected(false);
            } else {
                // Update selection
//                if (getItemCount() > 1) {
                    rowViewHolder.toggleSelection();
//                } else {
//                    rowViewHolder.toggleSelection();
//                    // When only one pass is there, then consider it selected always.
//                    if (!rowViewHolder.isSelected()) {
//                        rowViewHolder.setSelected(true);
//                    }
//                }
            }
            MultiSelectPassAdapter.this.setSelectionAndNotify(position, rowViewHolder.isSelected());

            handler.postDelayed(() -> {
                // Broadcast to click observers
                clicks.onNext(position);
            }, 200);
        }
    }

    interface SnapLookup {
        /**
         * Should return currently snapped position of RecyclerView.
         *
         * @return Snapped position.
         */
        int getSnappedPosition();
    }
}
