package co.bytemark.use_tickets;

public enum PassType {
    AVAILABLE,
    ACTIVE,
    EXPIRED
}
