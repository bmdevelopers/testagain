package co.bytemark.use_tickets

import android.content.Context
import androidx.core.view.AccessibilityDelegateCompat
import androidx.core.view.ViewCompat
import android.view.View
import android.view.accessibility.AccessibilityEvent
import android.widget.LinearLayout
import co.bytemark.R

class UseTicketsAccessibility {

    companion object {
        @JvmStatic
        fun announceForAccessibility(view: View, textToBeAnnounced: String) {
            view.contentDescription = textToBeAnnounced
        }

        @JvmStatic
        fun announceForAccessibilityEmptyStates(context: Context,
                                                linearLayoutNoNetworkText: LinearLayout,
                                                linearLayoutNoLoggedInText: LinearLayout) {

            linearLayoutNoNetworkText.contentDescription = context.getString(R.string.network_connectivity_error) + ". " +
                    context.getString(R.string.network_connectivity_error_message);
            linearLayoutNoLoggedInText.contentDescription = context.getString(R.string.you_are_signed_out) + " " +
                    context.getString(R.string.use_tickets_popup_signin_to_view_your_ticket);

        }

        @JvmStatic
        fun announceAccessibilityWithoutDoubleTapHint(view: View) {
            ViewCompat.setAccessibilityDelegate(view, object : AccessibilityDelegateCompat() {
                override fun onPopulateAccessibilityEvent(host: View, event: AccessibilityEvent) {
                    host.isClickable = false
                    host.isLongClickable = false
                }
            })

        }
    }
}