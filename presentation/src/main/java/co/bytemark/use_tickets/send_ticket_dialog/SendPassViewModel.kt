package co.bytemark.use_tickets.send_ticket_dialog

import androidx.lifecycle.ViewModel
import co.bytemark.domain.interactor.product.send_pass.SendPassUseCase
import co.bytemark.domain.interactor.product.send_pass.SendPassUseCaseValue
import javax.inject.Inject

class SendPassViewModel @Inject constructor(
        private val sendPassUseCase: SendPassUseCase
) : ViewModel() {

    fun sendPass(passUuid: String, receiverEmail: String) =
            sendPassUseCase.getLiveData(SendPassUseCaseValue(passUuid, receiverEmail))
}