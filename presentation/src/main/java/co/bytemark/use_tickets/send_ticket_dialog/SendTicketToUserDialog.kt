package co.bytemark.use_tickets.send_ticket_dialog

import android.content.Context
import android.os.Bundle
import android.os.IBinder
import androidx.fragment.app.DialogFragment
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.domain.model.common.Result
import co.bytemark.helpers.RxEventBus
import co.bytemark.use_tickets.UseTicketsEvents
import co.bytemark.widgets.util.isOnline
import com.afollestad.materialdialogs.MaterialDialog
import com.jakewharton.rxbinding.widget.RxTextView
import kotlinx.android.synthetic.main.send_ticket_to_user_dialog.*
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject


const val KEY_PASS_UUID = "PASS_UUID"
const val KEY_PASS_NAME = "PASS_NAME"

const val RESULT_UNKNOWN = 0
const val RESULT_SUCCESS = 1
const val RESULT_FAILURE = 2

class SendTicketToUserDialog: DialogFragment() {

    private lateinit var passUUID: String
    private lateinit var passName: String

    @Inject
    lateinit var sendPassViewModel: SendPassViewModel

    @Inject
    lateinit var eventBus: RxEventBus

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    private var subscription: Subscription? = null

    private var resultState: Int = RESULT_UNKNOWN

    companion object {

        fun newInstance(passUuid: String, passName: String): SendTicketToUserDialog {
            return SendTicketToUserDialog().apply {
                arguments = Bundle().apply {
                    putString(KEY_PASS_UUID, passUuid)
                    putString(KEY_PASS_NAME, passName)
                }
            }
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        passUUID = arguments?.getString(KEY_PASS_UUID)!!
        passName = arguments?.getString(KEY_PASS_NAME)!!
        CustomerMobileApp.component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.send_ticket_to_user_dialog, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog?.setCancelable(false)
        send.isEnabled = false
        isCancelable = false

        subscription = RxTextView.textChanges(email).throttleWithTimeout(500, TimeUnit.MILLISECONDS)
                .skip(1)
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.toString() }
                .map { isValidEmail(it) }
                .subscribe { send.isEnabled = it }

        cancel.setOnClickListener { dialog?.dismiss() }
        send.setOnClickListener {
            if (resultState == RESULT_SUCCESS) {
                // when success message positive action clicked
                eventBus.postEvent(UseTicketsEvents.ClearCacheReload())
                dialog?.dismiss()
            } else {
                dialog?.hide()
                askSendPassConfirmation()
            }
        }

    }

    private fun askSendPassConfirmation(){
        activity?.let {
            val builder = MaterialDialog.Builder(it)
                    .content(String.format(getString(R.string.send_ticket_dialog_confirmation),email.text.toString()))
                    .positiveText(R.string.ok)
                    .onPositive { confirmationPopUp, _ ->
                        confirmationPopUp.dismiss()
                        dialog?.show()
                        sendPass()
                    }.negativeText(R.string.popup_cancel)
                     .onNegative { confirmationPopUp, _ ->
                         confirmationPopUp.dismiss()
                         dialog?.dismiss()
                    }
            builder.show()
        }

    }


    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        if (email.text.toString().isNotEmpty()) {
            send.isEnabled = isValidEmail(email.text.toString())
        }

        // handling network connection
        if (!activity!!.isOnline()) {
            title.text = getString(R.string.change_password_popup_conErrorTitle)
            message.text = getString(R.string.change_password_Popup_con_error_body)
            send.visibility = View.GONE
            email_layout.visibility = View.GONE
            cancel.text = getString(R.string.popup_dismiss)
        }

    }

    private fun isValidEmail(emailStr: String): Boolean {
        return if (Patterns.EMAIL_ADDRESS.matcher(emailStr).matches()) {
            email_layout.error = ""
            true
        } else {
            email_layout.error = getString(R.string.sign_in_invalid_email)
            false
        }
    }

    private fun sendPass() {
        // closing input
        closeKeyBoard(email.windowToken)

        val receiverEmail = email.text.toString()
        sendPassViewModel.sendPass(passUUID, receiverEmail).observe(this, { result ->
            result?.let {
                when (it) {
                    is Result.Loading -> {
                        showProgressBar()
                    }
                    is Result.Success -> {
                        hideProgressBar()
                        email_layout.visibility = View.GONE
                        cancel.visibility = View.GONE
                        send.text = getString(R.string.ok)
                        message.text = getString(R.string.msg_send_ticket_success)
                        title.text = getString(R.string.send_ticket_success_alert_title)
                        resultState = RESULT_SUCCESS
                        analyticsPlatformAdapter.featureSendTicket(passUUID, passName, AnalyticsPlatformsContract.Status.SUCCESS, "")
                    }
                    is Result.Failure -> {
                        hideProgressBar()
                        if (it.bmError.isNotEmpty()) {
                            email_layout.visibility = View.GONE
                            send.visibility = View.GONE
                            cancel.text = getString(R.string.ok)
                            message.text = it.bmError.first().message
                            title.text = getString(R.string.send_ticket_failure_alert_title)
                            analyticsPlatformAdapter.featureSendTicket(passUUID, passName, AnalyticsPlatformsContract.Status.FAILURE, it.bmError.first().message)
                        }
                        resultState = RESULT_FAILURE
                    }
                }
            }
        })
    }

    private fun closeKeyBoard(windowToken: IBinder) {
        val imm: InputMethodManager =
                context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun showProgressBar() {
        message.visibility = View.INVISIBLE
        email_layout.visibility = View.INVISIBLE
        buttonLayout.visibility = View.INVISIBLE
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        message.visibility = View.VISIBLE
        email_layout.visibility = View.VISIBLE
        buttonLayout.visibility = View.VISIBLE
        progressBar.visibility = View.INVISIBLE
    }

    override fun onDestroy() {
        subscription?.unsubscribe()
        super.onDestroy()
    }

}