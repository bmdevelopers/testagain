package co.bytemark.use_tickets

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import co.bytemark.analytics.AnalyticsPlatformAdapter

class PassActivationReceiver(val analyticsPlatformAdapter: AnalyticsPlatformAdapter,
                             val type: Int, private val noOfPasses: Int,
                             private val hasCloudPass: Boolean, private val hasEnablers: Boolean
) : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        context?.let {
            val status = intent?.getStringExtra("status") ?: ""
            val errorMessage = intent?.getStringExtra("errorMessage") ?: ""
            if (type == 1) {
                // activate ticket
                analyticsPlatformAdapter.activateTicket(noOfPasses,
                        if (hasEnablers) 1 else 0,
                        if (hasCloudPass) 1 else 0,
                        status, errorMessage)
            } else {
                // display ticket
                analyticsPlatformAdapter.displayTicket(noOfPasses,
                        status, errorMessage)
            }
            LocalBroadcastManager.getInstance(it).unregisterReceiver(this)
        }
    }
}