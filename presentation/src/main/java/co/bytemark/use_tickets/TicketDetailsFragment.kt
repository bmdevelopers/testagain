package co.bytemark.use_tickets

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.helpers.AppConstants.PAYMENT_TYPE_INCOMM
import co.bytemark.helpers.AppConstants.USE_TICKETS_FRAGMENT_TYPE
import co.bytemark.helpers.AppConstants.USE_TICKETS_STATUS_EXPIRED
import co.bytemark.helpers.AppConstants.USE_TICKETS_STATUS_USABLE
import co.bytemark.helpers.AppConstants.USE_TICKETS_STATUS_USING
import co.bytemark.sdk.Api.ApiUtility
import co.bytemark.sdk.Pass
import co.bytemark.sdk.hide
import kotlinx.android.synthetic.main.fragment_ticket_details.*
import java.util.*
import javax.inject.Inject

class TicketDetailsFragment : BaseMvvmFragment() {

    private lateinit var pass: Pass

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.fragment_ticket_details, container, false)!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments?.getParcelable<Pass>("pass") != null) {
            pass = arguments?.getParcelable("pass")!!
            setTicketDetails()
        }

        if (arguments?.getString(USE_TICKETS_FRAGMENT_TYPE) != null) {
            val type = arguments?.getString(USE_TICKETS_FRAGMENT_TYPE) ?: ""
            analyticsPlatformAdapter.featureViewTicketDetails(type, pass.productUuid)
        }
    }

    private fun setTicketDetails() {

        pass.getLabelName(Locale.getDefault().language)?.let {
            labelName.text = it
        }

        if (!pass.serial.isNullOrEmpty()) {
            ticketHeader.visibility = View.VISIBLE
            ticketHeader.text = StringBuilder().append(getString(R.string.ticket_details_serial_number))
                .append(pass.serial).toString()
        }

        val originDestinationBuilder = StringBuilder()
        pass.getFilterAttribute("origin", confHelper.isDisplayLongNameForOd(pass.issuer?.userUuid))?.let {
            originDestinationBuilder.append(it)
        }
        pass.getFilterAttribute("destination", confHelper.isDisplayLongNameForOd(pass.issuer?.userUuid))?.let {
            if (originDestinationBuilder.isNotEmpty()) originDestinationBuilder.append(" / ")
            originDestinationBuilder.append(it)
        }

        if (originDestinationBuilder.isNotEmpty()) {
            orginDestination.visibility = View.VISIBLE
            orginDestination.text = originDestinationBuilder.toString()
        }

        if (pass.firstUseDate != null) {
            firstActivationText.visibility = View.VISIBLE
            firstActivationTextValue.visibility = View.VISIBLE
            firstActivationTextValue.text = StringBuilder().append(getDate(pass.firstUseDate!!.time))
                .append(" ").append(getTime(pass.firstUseDate.time))
        }

        pass.orderItem?.let {
            if (!it.orderTimePurchased.isNullOrEmpty()) {
                purchaseTimeText.visibility = View.VISIBLE
                purchaseTimeValue.visibility = View.VISIBLE
                val purchasedTime = ApiUtility.getCalendarFromS(it.orderTimePurchased)
                purchasedTime?.time?.let { purchasedDateTime ->
                    purchaseTimeValue.text = StringBuilder().append(getDate(purchasedDateTime))
                        .append(" ").append(getTime(purchasedDateTime))
                }
            }

            if (!it.orderCard.isNullOrEmpty()) {
                paymentMethodText.visibility = View.VISIBLE
                paymentMethodTextValue.visibility = View.VISIBLE
                if (it.orderCard?.toLowerCase(Locale.getDefault())?.trim() == PAYMENT_TYPE_INCOMM) {
                    paymentMethodTextValue.text = getString(R.string.receipt_payment_method_cash)
                } else {
                    paymentMethodTextValue.text = it.orderCard
                }
            }

            price.text = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(it.price)
        }

        if (pass.allowedUses == 0) {
            remainingUsesTextValue.text = activity?.getString(R.string.use_tickets_unlimited)
        } else {
            remainingUsesTextValue.text = pass.remainingUses.toString()
        }

        if (pass.expiration != null) {
            expiredTimeText.visibility = View.VISIBLE
            expiredTimeTextValue.visibility = View.VISIBLE
            expiredTimeTextValue.text = StringBuilder().append(getDate(pass.expiration!!.time))
                .append(" ").append(getTime(pass.expiration!!.time))
        }

        statusTag.text = when (pass.status) {
            USE_TICKETS_STATUS_USABLE -> getString(R.string.use_tickets_available)
            USE_TICKETS_STATUS_USING -> getString(R.string.use_tickets_active)
            else -> getString(R.string.use_tickets_expired)
        }

        if (pass.status.equals(USE_TICKETS_STATUS_EXPIRED, true)) {
            statusTag.setBackgroundResource(R.drawable.bg_subscription_error_status)
            statusTag.setTextColor(confHelper.dataThemePrimaryTextColor)
            remainingUsesTextValue.hide()
            remainingUsesText.hide()
        }
    }

    private fun getDate(date: Date): String {
        return confHelper.dateDisplayFormat.format(date)
    }

    private fun getTime(date: Date): String {
        return confHelper.timeDisplayFormat.format(date)
    }

    companion object {
        fun newInstance(pass: Pass, fragmentType: String): Fragment {
            val fragment = TicketDetailsFragment()
            val arguments = Bundle()
            arguments.putParcelable("pass", pass)
            arguments.putString(USE_TICKETS_FRAGMENT_TYPE, fragmentType)
            fragment.arguments = arguments
            return fragment
        }
    }

}
