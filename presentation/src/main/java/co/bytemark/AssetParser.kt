package co.bytemark

import android.content.Context
import androidx.core.os.ConfigurationCompat
import co.bytemark.add_payment_card.Items
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.helpers.GsonFactory
import co.bytemark.sdk.model.config.Conf
import co.bytemark.sdk.model.menu.MenuItem
import co.bytemark.sdk.model.more_info.MoreInfoList
import co.bytemark.sdk.model.navigationitems.NavigationItems
import co.bytemark.sdk.model.settings.SettingsList
import co.bytemark.sdk.schedules.ScheduleItem
import co.bytemark.sdk.schedules.Schedules
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import timber.log.Timber
import java.io.IOException
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.ArrayList

@Singleton
class AssetParser @Inject constructor(context: Context?) {
    private val context: Context? = context?.applicationContext
    private val gson: Gson?
        get() = GsonFactory.gson

    private fun readAssetFile(fileName: String): String? {
        var string: String? = null
        try {
            val inputStream = context?.resources?.assets?.open(fileName)
            val size = inputStream?.available()
            val buffer = size?.let { ByteArray(it) }
            inputStream?.read(buffer)
            inputStream?.close()
            string = buffer?.let { String(it, Charsets.UTF_8) }
        } catch (ex: IOException) {
            Timber.e(ex.toString())
        }
        return string
    }

    fun loadConf(): Conf? {
        return gson?.fromJson(
            readAssetFile(CONF),
            Conf::class.java
        )
    }

    fun loadNavigationMenuItems(localeCode: String?): List<MenuItem>? {
        return getClazz(
            NAVIGATION_ITEMS,
            localeCode,
            NavigationItems::class.java
        )?.navigationItems
    }

    fun loadScheduleItems(localeCode: String?): List<ScheduleItem>? {
        return getClazz(
            SCHEDULE_ITEMS,
            localeCode,
            Schedules::class.java
        )?.schedule_items
    }

    fun getGTFSURL(localeCode: String?): String? {
        return getClazz(
            SCHEDULE_ITEMS,
            localeCode,
            Schedules::class.java
        )?.gtfs_url
    }

    fun loadDefaultFareMediumCardDetails(localeCode: String?): MoreInfoList? {
        return getClazz(
            FARE_MEDIUM_CARD_DETAILS,
            localeCode,
            MoreInfoList::class.java
        )
    }

    fun loadDefaultSettingsList(localeCode: String?): SettingsList? {
        return getClazz(
            SETTINGS,
            localeCode,
            SettingsList::class.java
        )
    }

    fun loadDefaultMoreInfoList(localeCode: String?): MoreInfoList? {
        return getClazz(
            MORE_INFO,
            localeCode,
            MoreInfoList::class.java
        )
    }

    fun loadDefaultForgotPasswordFormList(localeCode: String?): List<Formly>? {
        return getClazz(
            FORMLY_FORGOT_PASSWORD,
            localeCode,
            BMResponse::class.java
        )?.data?.formly
    }

    fun loadDefaultVoucherCodeFormList(localeCode: String?): List<Formly>? {
        return getClazz(
            FORMLY_VOUCHER_CODE,
            localeCode,
            BMResponse::class.java
        )?.data?.formly
    }

    fun loadDefaultSignUpFormList(localeCode: String?): List<Formly>? {
        return getClazz(
            FORMLY_SIGN_UP,
            localeCode,
            BMResponse::class.java
        )?.data?.formly
    }

    fun loadDefaultChangePasswordFormList(localeCode: String?): List<Formly>? {
        return getClazz(
            FORMLY_CHANGE_PASSWORD,
            localeCode,
            BMResponse::class.java
        )?.data?.formly
    }

    fun loadDefaultAccountManagementFormList(localeCode: String?): List<Formly>? {
        return getClazz(
            FORMLY_ACCOUNT_MANAGEMENT,
            localeCode,
            BMResponse::class.java
        )?.data?.formly
    }

    fun loadCountries(): List<Items> {
        val fileContents = readAssetFile(COUNTRIES)
        return gson?.fromJson(
            fileContents,
            object :
                TypeToken<List<Items?>?>() {}.type
        ) ?: ArrayList()
    }

    fun loadStates(): List<Items> {
        val fileContents = readAssetFile(STATES_US)
        return gson?.fromJson(
            fileContents,
            object :
                TypeToken<List<Items?>?>() {}.type
        ) ?: ArrayList()
    }

    fun loadNativeAppSupportFormList(localeCode: String?): List<Formly>? {
        return getClazz(
            FORMLY_NATIVE_APP_SUPPORT,
            localeCode,
            BMResponse::class.java
        )?.data?.formly
    }

    private fun <T> getClazz(
        name: String,
        localeCode: String?,
        clazz: Class<T>
    ): T? {
        var localeCode = localeCode
        val deviceLocale = ConfigurationCompat.getLocales(context!!.resources.configuration).get(0)
        localeCode = deviceLocale.language + "-" + deviceLocale.country
        Timber.e("localeCode: $localeCode")
        var fileContents = readAssetFile(String.format(name, localeCode))
        return if (fileContents != null) {
            gson?.fromJson(fileContents, clazz)
        } else {
            val locale = localeCode.substring(0, localeCode.indexOf("-"))
            fileContents = readAssetFile(String.format(name, locale))
            if (fileContents != null) {
                gson?.fromJson(fileContents, clazz)
            } else {
                fileContents = readAssetFile(String.format(name, "en"))
                if (fileContents != null) {
                    gson?.fromJson(fileContents, clazz)
                } else {
                    Timber.d("Did not find any preferred language file, falling back to English")
                    gson?.fromJson(
                        readAssetFile(String.format(name, "en-US")),
                        clazz
                    )
                }
            }
        }
    }

    companion object {
        private const val NAVIGATION_ITEMS = "navigation_%s.json"
        private const val SETTINGS = "settings_%s.json"
        private const val FARE_MEDIUM_CARD_DETAILS = "fare_medium_card_details_%s.json"
        private const val MORE_INFO = "more_menu_%s.json"
        private const val FORMLY_FORGOT_PASSWORD = "formly_forgot_password_%s.json"
        private const val FORMLY_VOUCHER_CODE = "formly_voucher_code_%s.json"
        private const val FORMLY_SIGN_UP = "formly_sign_up_%s.json"
        private const val FORMLY_CHANGE_PASSWORD = "formly_change_password_%s.json"
        private const val FORMLY_ACCOUNT_MANAGEMENT = "formly_account_management_%s.json"
        private const val SCHEDULE_ITEMS = "schedules_%s.json"
        private const val COUNTRIES = "countries.json"
        private const val STATES_US = "states_us.json"
        private const val FORMLY_NATIVE_APP_SUPPORT = "app_support_%s.json"
        private const val CONF = "conf.json"
    }
}