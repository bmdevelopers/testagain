package co.bytemark.di.modules

import co.bytemark.data.annotation.Remote
import co.bytemark.data.authentication.remote.AuthenticationRemoteEntityStore
import co.bytemark.data.authentication.remote.AuthenticationRemoteEntityStoreImpl
import co.bytemark.data.credit_pass.CreditPassRepositoryImpl
import co.bytemark.data.credit_pass.remote.CreditPassRemoteEntityStore
import co.bytemark.data.credit_pass.remote.CreditPassRemoteEntityStoreImpl
import co.bytemark.data.discount.remote.DiscountRemoteEntityStore
import co.bytemark.data.discount.remote.DiscountRemoteEntityStoreImpl
import co.bytemark.data.fare_capping.remote.FareCappingRemoteEntityStore
import co.bytemark.data.fare_capping.remote.FareCappingRemoteEntityStoreImpl
import co.bytemark.data.fare_medium.remote.FareMediumRemoteEntityStore
import co.bytemark.data.fare_medium.remote.FareMediumRemoteEntityStoreImpl
import co.bytemark.data.filters.remote.FiltersRemoteEntityStore
import co.bytemark.data.filters.remote.FiltersRemoteEntityStoreImpl
import co.bytemark.data.manage.remote.ManageRemoteEntityStore
import co.bytemark.data.manage.remote.ManageRemoteEntityStoreImpl
import co.bytemark.data.native_app_support.remote.NativeAppSupportRemoteEntityStore
import co.bytemark.data.native_app_support.remote.NativeAppSupportRemoteEntityStoreImpl
import co.bytemark.data.newStoreFilters.remote.NewFiltersRemoteEntityStore
import co.bytemark.data.newStoreFilters.remote.NewFiltersRemoteEntityStoreImpl
import co.bytemark.data.notification.remote.NotificationRemoteEntityStore
import co.bytemark.data.notification.remote.NotificationRemoteEntityStoreImpl
import co.bytemark.data.notification_settings.remote.NotificationSettingsRemoteEntityStore
import co.bytemark.data.notification_settings.remote.NotificationSettingsRemoteEntityStoreImpl
import co.bytemark.data.passes.remote.PassesRemoteEntityStore
import co.bytemark.data.passes.remote.PassesRemoteEntityStoreImpl
import co.bytemark.data.payments.remote.PaymentsRemoteEntityStore
import co.bytemark.data.payments.remote.PaymentsRemoteEntityStoreImpl
import co.bytemark.data.private_key.remote.PrivateKeyRemoteEntityStore
import co.bytemark.data.private_key.remote.PrivateKeyRemoteEntityStoreImpl
import co.bytemark.data.product.remote.ProductsRemoteEntityStore
import co.bytemark.data.product.remote.ProductsRemoteStoreImpl
import co.bytemark.data.purchase_history.remote.OrderHistoryRemoteEntityStore
import co.bytemark.data.purchase_history.remote.OrderHistoryRemoteEntityStoreImpl
import co.bytemark.data.resend_receipt.remote.ResendReceiptRemoteEntityStore
import co.bytemark.data.resend_receipt.remote.ResendReceiptRemoteRemoteEntityStoreImpl
import co.bytemark.data.schedules.remote.GTFSRemoteEntityStore
import co.bytemark.data.schedules.remote.GTFSRemoteEntityStoreImpl
import co.bytemark.data.securityquestions.remote.SecurityQuestionRemoteEntityStore
import co.bytemark.data.securityquestions.remote.SecurityQuestionRemoteEntityStoreImpl
import co.bytemark.data.send_ticket.remote.SendPassRemoteEntityStore
import co.bytemark.data.send_ticket.remote.SendPassRemoteEntityStoreImpl
import co.bytemark.data.subscriptions.Remote.SubscriptionsRemoteEntityStore
import co.bytemark.data.subscriptions.Remote.SubscriptionsRemoteEntityStoreImpl
import co.bytemark.data.ticket_history.remote.TicketHistoryRemoteEntityStore
import co.bytemark.data.ticket_history.remote.TicketHistoryRemoteEntityStoreImpl
import co.bytemark.data.ticket_storage.remote.TransferPassRemoteEntityStore
import co.bytemark.data.ticket_storage.remote.TransferPassRemoteEntityStoreImpl
import co.bytemark.data.userphoto.remote.UserPhotoRemoteEntityStore
import co.bytemark.data.userphoto.remote.UserPhotoRemoteEntityStoreImpl
import co.bytemark.data.voucher_redeem.remote.VoucherRedeemRemoteEntityStore
import co.bytemark.data.voucher_redeem.remote.VoucherRedeemRemoteEntityStoreImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteEntityStoreModule {
    @Remote
    @Provides
    @Singleton
    fun providesNotificationRemoteEntityStore(notificationRemoteEntityStore: NotificationRemoteEntityStoreImpl): NotificationRemoteEntityStore =
        notificationRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesAuthenticationRemoteEntityStore(authenticationRemoteEntityStore: AuthenticationRemoteEntityStoreImpl): AuthenticationRemoteEntityStore =
        authenticationRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesFareMediumRemoteEntityStore(fareMediumRemoteEntityStore: FareMediumRemoteEntityStoreImpl): FareMediumRemoteEntityStore =
        fareMediumRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesFiltersRemoteEntityStore(filtersRemoteEntityStore: FiltersRemoteEntityStoreImpl): FiltersRemoteEntityStore =
        filtersRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesUserPhotoRemoteEntityStore(userPhotoRemoteEntityStore: UserPhotoRemoteEntityStoreImpl): UserPhotoRemoteEntityStore =
        userPhotoRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun privateKeyRemoteEntityStore(privateKeyRemoteEntityStore: PrivateKeyRemoteEntityStoreImpl): PrivateKeyRemoteEntityStore =
        privateKeyRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun productsRemoteEntityStore(productsRemoteStore: ProductsRemoteStoreImpl): ProductsRemoteEntityStore =
        productsRemoteStore

    @Remote
    @Provides
    @Singleton
    fun paymentsRemoteEntityStore(paymentsRemoteEntityStore: PaymentsRemoteEntityStoreImpl): PaymentsRemoteEntityStore =
        paymentsRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun manageRemoteEntityStore(manageRemoteEntityStore: ManageRemoteEntityStoreImpl): ManageRemoteEntityStore =
        manageRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesNotificationSettingsRemoteEntityStore(notificationSettingsRemoteEntityStore: NotificationSettingsRemoteEntityStoreImpl): NotificationSettingsRemoteEntityStore =
        notificationSettingsRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesGTFSRemoteEntityStore(gtfsRemoteEntityStore: GTFSRemoteEntityStoreImpl): GTFSRemoteEntityStore =
        gtfsRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesOrderHistoryRemoteEntityStore(orderHistoryRemoteEntityStore: OrderHistoryRemoteEntityStoreImpl): OrderHistoryRemoteEntityStore =
        orderHistoryRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesSubscriptionsRemoteEntityStore(subscriptionsRemoteEntityStore: SubscriptionsRemoteEntityStoreImpl): SubscriptionsRemoteEntityStore =
        subscriptionsRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesSecurityRemoteEntityStore(securityQuestionRemoteEntityStore: SecurityQuestionRemoteEntityStoreImpl): SecurityQuestionRemoteEntityStore =
        securityQuestionRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesPassesRemoteEntityStore(passesRemoteEntityStore: PassesRemoteEntityStoreImpl): PassesRemoteEntityStore =
        passesRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesNewFiltersRemoteEntityStore(filtersRemoteEntityStore: NewFiltersRemoteEntityStoreImpl): NewFiltersRemoteEntityStore =
        filtersRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesVoucherRedeemRemoteEntityStore(voucherRedeemRemoteEntityStore: VoucherRedeemRemoteEntityStoreImpl): VoucherRedeemRemoteEntityStore =
        voucherRedeemRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesHistoryPassesRemoteEntityStore(historyPassesRemoteEntityStore: TicketHistoryRemoteEntityStoreImpl): TicketHistoryRemoteEntityStore =
        historyPassesRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesResendReceiptRemoteEntityStore(resendReceiptRemoteEntityStore: ResendReceiptRemoteRemoteEntityStoreImpl): ResendReceiptRemoteEntityStore =
        resendReceiptRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesSendPassRemoteEntityStore(sendPassRemoteEntityStore: SendPassRemoteEntityStoreImpl): SendPassRemoteEntityStore =
        sendPassRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesNativeAppSupportLocalEntityStore(nativeAppSupportRemoteEntityStore: NativeAppSupportRemoteEntityStoreImpl): NativeAppSupportRemoteEntityStore =
        nativeAppSupportRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesFareCappingRemoteEntityStore(entityStore: FareCappingRemoteEntityStoreImpl): FareCappingRemoteEntityStore =
        entityStore


    @Remote
    @Provides
    @Singleton
    fun providesTransferPassRemoteEntityStore(transferPassRemoteEntityStore: TransferPassRemoteEntityStoreImpl): TransferPassRemoteEntityStore {
        return transferPassRemoteEntityStore
    }

    @Remote
    @Provides
    @Singleton
    fun providesDiscountsRemoteEntityStore(discountRemoteEntityStore: DiscountRemoteEntityStoreImpl): DiscountRemoteEntityStore =
        discountRemoteEntityStore

    @Remote
    @Provides
    @Singleton
    fun providesCreditPassRemoteEntityStore(creditPassRemoteEntityStore: CreditPassRemoteEntityStoreImpl): CreditPassRemoteEntityStore =
        creditPassRemoteEntityStore
}