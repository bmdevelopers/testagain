package co.bytemark.di.modules

import co.bytemark.data.annotation.Local
import co.bytemark.data.authentication.local.AuthenticationLocalEntityStore
import co.bytemark.data.authentication.local.AuthenticationLocalEntityStoreImpl
import co.bytemark.data.credit_pass.local.CreditPassLocalEntityStore
import co.bytemark.data.credit_pass.local.CreditPassLocalEntityStoreImpl
import co.bytemark.data.discount.local.DiscountLocalEntityStore
import co.bytemark.data.discount.local.DiscountLocalEntityStoreImpl
import co.bytemark.data.fare_capping.local.FareCappingLocalEntityStore
import co.bytemark.data.fare_capping.local.FareCappingLocalEntityStoreImpl
import co.bytemark.data.fare_medium.local.FareMediumLocalEntityStore
import co.bytemark.data.fare_medium.local.FareMediumLocalEntityStoreImpl
import co.bytemark.data.filters.local.FiltersLocalEntityStore
import co.bytemark.data.filters.local.FiltersLocalEntityStoreImpl
import co.bytemark.data.manage.local.ManageLocalEntityStore
import co.bytemark.data.manage.local.ManageLocalEntityStoreImpl
import co.bytemark.data.native_app_support.local.NativeAppSupportLocalEntityStore
import co.bytemark.data.native_app_support.local.NativeAppSupportLocalEntityStoreImpl
import co.bytemark.data.newStoreFilters.local.NewFiltersLocalEntityStore
import co.bytemark.data.newStoreFilters.local.NewFiltersLocalEntityStoreImpl
import co.bytemark.data.notification.local.NotificationLocalEntityStore
import co.bytemark.data.notification.local.NotificationLocalEntityStoreImpl
import co.bytemark.data.notification_settings.local.NotificationSettingsLocalEntityStore
import co.bytemark.data.notification_settings.local.NotificationSettingsLocalEntityStoreImpl
import co.bytemark.data.passes.local.PassesLocalEntityStore
import co.bytemark.data.passes.local.PassesLocalEntityStoreImpl
import co.bytemark.data.payments.local.PaymentsLocalEntityStore
import co.bytemark.data.payments.local.PaymentsLocalEntityStoreImpl
import co.bytemark.data.private_key.local.PrivateKeyLocalEntityStore
import co.bytemark.data.private_key.local.PrivateKeyLocalEntityStoreImpl
import co.bytemark.data.product.local.ProductsLocalEntityStore
import co.bytemark.data.product.local.ProductsLocalStoreImpl
import co.bytemark.data.purchase_history.local.OrderHistoryLocalEntityStore
import co.bytemark.data.purchase_history.local.OrderHistoryLocalEntityStoreImpl
import co.bytemark.data.resend_receipt.local.ResendReceiptLocalEntityStore
import co.bytemark.data.resend_receipt.local.ResendReceiptLocalEntityStoreImpl
import co.bytemark.data.schedules.local.GTFSLocalEntityStore
import co.bytemark.data.schedules.local.GTFSLocalEntityStoreImpl
import co.bytemark.data.securityquestions.local.SecurityQuestionLocalEntityStore
import co.bytemark.data.securityquestions.local.SecurityQuestionLocalEntityStoreImpl
import co.bytemark.data.send_ticket.local.SendPassLocalEntityStore
import co.bytemark.data.send_ticket.local.SendPassLocalEntityStoreImpl
import co.bytemark.data.subscriptions.local.SubscriptionsLocalEntityStore
import co.bytemark.data.subscriptions.local.SubscriptionsLocalEntityStoreImpl
import co.bytemark.data.ticket_history.local.TicketHistoryLocalEntityStore
import co.bytemark.data.ticket_history.local.TicketHistoryLocalEntityStoreImpl
import co.bytemark.data.ticket_storage.local.TransferPassLocalEntityStore
import co.bytemark.data.ticket_storage.local.TransferPassLocalEntityStoreImpl
import co.bytemark.data.userphoto.local.UserPhotoLocalEntityStore
import co.bytemark.data.userphoto.local.UserPhotoLocalEntityStoreImpl
import co.bytemark.data.voucher_redeem.local.VoucherRedeemLocalEntityStore
import co.bytemark.data.voucher_redeem.local.VoucherRedeemLocalEntityStoreImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalEntityStoreModule {
    @Local
    @Provides
    @Singleton
    fun providesAuthenticationLocalEntityStore(authenticationLocalEntityStore: AuthenticationLocalEntityStoreImpl): AuthenticationLocalEntityStore =
        authenticationLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesNotificationLocalEntityStore(notificationLocalEntityStore: NotificationLocalEntityStoreImpl): NotificationLocalEntityStore =
        notificationLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesFareMediumLocalEntityStore(fareMediumLocalEntityStore: FareMediumLocalEntityStoreImpl): FareMediumLocalEntityStore =
        fareMediumLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesUserPhotoLocalEntityStore(userPhotoLocalEntityStore: UserPhotoLocalEntityStoreImpl): UserPhotoLocalEntityStore =
        userPhotoLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun privateKeyLocalEntityStore(privateKeyLocalEntityStore: PrivateKeyLocalEntityStoreImpl): PrivateKeyLocalEntityStore =
        privateKeyLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun productsLocalEntityStore(productsLocalStore: ProductsLocalStoreImpl): ProductsLocalEntityStore =
        productsLocalStore

    @Local
    @Provides
    @Singleton
    fun paymentsLocalEntityStore(paymentsLocalEntityStore: PaymentsLocalEntityStoreImpl): PaymentsLocalEntityStore =
        paymentsLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun manageLocalEntityStore(manageLocalEntityStore: ManageLocalEntityStoreImpl): ManageLocalEntityStore =
        manageLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesNotificationSettingsLocalEntityStore(notificationSettingsLocalEntityStore: NotificationSettingsLocalEntityStoreImpl): NotificationSettingsLocalEntityStore =
        notificationSettingsLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesGTFSLocalEntityStoreImpl(gtfsLocalEntityStore: GTFSLocalEntityStoreImpl): GTFSLocalEntityStore =
        gtfsLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesOrderHistoryLocalEntityStore(orderHistoryLocalEntityStore: OrderHistoryLocalEntityStoreImpl): OrderHistoryLocalEntityStore =
        orderHistoryLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesSubscriptionsLocalEntityStore(subscriptionsLocalEntityStore: SubscriptionsLocalEntityStoreImpl): SubscriptionsLocalEntityStore =
        subscriptionsLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesSecurityLocalEntityStore(securityQuestionLocalEntityStore: SecurityQuestionLocalEntityStoreImpl): SecurityQuestionLocalEntityStore =
        securityQuestionLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesPassLocalEntityStore(passesLocalEntityStore: PassesLocalEntityStoreImpl): PassesLocalEntityStore =
        passesLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesFiltersLocalEntityStore(filtersLocalEntitiyStore: FiltersLocalEntityStoreImpl): FiltersLocalEntityStore =
        filtersLocalEntitiyStore

    @Local
    @Provides
    @Singleton
    fun providersNewFiltersLocalEntityStore(filtersLocalEntitiyStore: NewFiltersLocalEntityStoreImpl): NewFiltersLocalEntityStore =
        filtersLocalEntitiyStore

    @Local
    @Provides
    @Singleton
    fun providesVoucherRedeemLocalEntityStore(voucherRedeemLocalEntityStore: VoucherRedeemLocalEntityStoreImpl): VoucherRedeemLocalEntityStore =
        voucherRedeemLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesTicketHistoryLocalEntityStore(ticketHistoryLocalEntityStore: TicketHistoryLocalEntityStoreImpl): TicketHistoryLocalEntityStore =
        ticketHistoryLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesResendReceiptLocalEntityStore(resendReceiptLocalEntityStore: ResendReceiptLocalEntityStoreImpl): ResendReceiptLocalEntityStore =
        resendReceiptLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesSendPassLocalEntityStore(sendPassLocalEntityStore: SendPassLocalEntityStoreImpl): SendPassLocalEntityStore =
        sendPassLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesNativeAppSupportLocalEntityStore(nativeAppSupportLocalEntityStore: NativeAppSupportLocalEntityStoreImpl): NativeAppSupportLocalEntityStore =
        nativeAppSupportLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun provideFareCappingLocalEntityStore(localEntityStore: FareCappingLocalEntityStoreImpl): FareCappingLocalEntityStore =
        localEntityStore

    @Local
    @Provides
    @Singleton
    fun providesTransferPassLocalEntityStore(authenticationLocalEntityStore: TransferPassLocalEntityStoreImpl): TransferPassLocalEntityStore =
        authenticationLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesDiscountLocalEntityStore(discountLocalEntityStore: DiscountLocalEntityStoreImpl): DiscountLocalEntityStore =
        discountLocalEntityStore

    @Local
    @Provides
    @Singleton
    fun providesCreditPassLocalEntityStore(creditPassLocalEntityStore: CreditPassLocalEntityStoreImpl): CreditPassLocalEntityStore =
        creditPassLocalEntityStore
}