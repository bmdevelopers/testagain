package co.bytemark.di.modules

import android.app.Application
import co.bytemark.BuildConfig
import co.bytemark.CustomerMobileApp.Companion.getConf
import co.bytemark.data.annotation.Account
import co.bytemark.data.annotation.CoroutineAccount
import co.bytemark.data.annotation.CoroutineOverture
import co.bytemark.data.annotation.Overture
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.net.manager.NetworkManagerImpl
import co.bytemark.helpers.GsonFactory
import co.bytemark.helpers.RxUtils
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.network_impl.API
import co.bytemark.sdk.network_impl.BMNetwork
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@Singleton
class NetworkModule(private val baseUrl: String, private val accountUrl: String) {
    @Provides
    @Singleton
    fun providesOkHttpCache(application: Application): Cache {
        val cacheSize = 12 * 1024 * 1024
        return Cache(application.cacheDir, cacheSize.toLong())
    }

    @Provides
    @Singleton
    fun providesGson(): Gson = GsonFactory.gson

    @Provides
    @Singleton
    fun providesOkHttpClient(cache: Cache): OkHttpClient {
        val builder = OkHttpClient.Builder()
        /***
         * ATTENTION: Do not remove this check ever. If been removed, remove the interceptor too.
         *
         * The interceptor should NEVER be enabled while in release mode.
         * It must only be enabled for debugging purpose.
         * Enabling it in release mode might result in logging secure credentials.
         */
        builder.connectTimeout(60 * 1000.toLong(), TimeUnit.MILLISECONDS)
            .readTimeout(60 * 1000.toLong(), TimeUnit.MILLISECONDS)
            .addInterceptor { chain: Interceptor.Chain ->
                jwtAuthorizationToken = chain.request().tag(String::class.java)
                val request = chain.request().newBuilder()
                    .header("Accept-Language", supportedTransactionLocale())
                    .addHeader(
                        AUTHORIZATION,
                        authorizationHeader
                    ).build()
                chain.proceed(request)
            }
            .cache(cache)
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(logging)

            // for testing fare-capping, as no setup for fare-capping there. using this to intercept fare-capping request,
            // and sending testing data
//             builder.addInterceptor(new FareCappingInterceptor());
//            builder.addInterceptor(new UPassAPIInterceptor());
        }
        return builder.build()
    }

    private fun supportedTransactionLocale(): String {
        val supportedLocaleList =
            getConf()!!.organization.supportedLocales
        val deviceLocale = Locale.getDefault()
        val defaultTransactionCode = deviceLocale.toLanguageTag()
        var transactionLocale: Locale? = null
        for (supportedLocale in supportedLocaleList) {
            val locale =
                supportedLocale.languageCode + "_" + supportedLocale.countryCode
            if (locale == deviceLocale.toString() && supportedLocale.isDefault) {
                transactionLocale = deviceLocale
            }
        }
        if (transactionLocale == null) {
            transactionLocale = Locale.forLanguageTag(defaultTransactionCode)
        }
        return transactionLocale!!.toLanguageTag()
    }

    @Overture
    @Provides
    @Singleton
    fun provideOvertureRetrofit(
        okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder().client(okHttpClient)
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @CoroutineOverture
    @Provides
    @Singleton
    fun provideCoroutineOvertureRetrofit(
        okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder().client(okHttpClient)
            .baseUrl(baseUrl)
            .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Account
    @Provides
    @Singleton
    fun provideAccountRetrofit(
        okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder().client(okHttpClient)
            .baseUrl(accountUrl)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @CoroutineAccount
    @Provides
    @Singleton
    fun provideCoroutineAccountRetrofit(
        okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder().client(okHttpClient)
            .baseUrl(accountUrl)
            .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Provides
    @Singleton
    fun providesBmNetwork(
        context: Application,
        api: API
    ): BMNetwork {
        return BMNetwork(context, api)
    }

    @Provides
    @Singleton
    fun providesRxUtils(): RxUtils = RxUtils.instance

    @Provides
    @Singleton
    fun provideNetworkManger(application: Application): NetworkManager =
        NetworkManagerImpl(application)

    companion object {
        private const val AUTHORIZATION = "Authorization"
        private var jwtAuthorizationToken: String? = null
        private val authorizationHeader: String
            get() {
                if (BytemarkSDK.SDKUtility.getAuthToken() != null) {
                    return if(jwtAuthorizationToken != null) {
                        " Bearer " + BytemarkSDK.SDKUtility.getAuthToken() + ";Deep_Link_Token deep_link_code=${jwtAuthorizationToken}"
                    } else {
                        " Bearer " + BytemarkSDK.SDKUtility.getAuthToken()
                    }
                } else if (BytemarkSDK.SDKUtility.getClientId() != null) {
                    return " Client client_id=" + BytemarkSDK.SDKUtility.getClientId()
                }
                return ""
            }
    }
}