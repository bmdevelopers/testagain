package co.bytemark.di.modules

import android.content.Context
import co.bytemark.AssetParser
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.remote_config.RemoteConfigHelper
import dagger.Module
import dagger.Provides

@Module
class ConfigModule(private val remoteConfigHelper: RemoteConfigHelper) {

    @Provides
    fun provideConfHelper(context: Context, assetParser: AssetParser) =
        ConfHelper(context, assetParser, remoteConfigHelper)
}