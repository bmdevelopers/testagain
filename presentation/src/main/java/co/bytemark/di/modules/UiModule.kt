package co.bytemark.di.modules

import android.app.Application
import co.bytemark.base.NavigationDrawer
import co.bytemark.base.NavigationDrawerImpl
import co.bytemark.helpers.NavigationUtils
import co.bytemark.use_tickets.passview.PassViewFactory
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import javax.inject.Named
import javax.inject.Singleton

@Module
class UiModule {
    @Provides
    fun providesNavigationDrawer(navigationUtils: NavigationUtils): NavigationDrawer =
        // Third party can provide their own implementation of NavigationDrawer here.
        NavigationDrawerImpl.getInstance(navigationUtils)

    @Provides
    @Singleton
    fun providePassViewFactory(application: Application): PassViewFactory =
        PassViewFactory(application)

    @Provides
    @Named("ioScope")
    fun coroutineIoScope(): CoroutineScope = CoroutineScope(Job() + Dispatchers.IO)

    @Provides
    @Named("uiScope")
    fun coroutineUiScope(): CoroutineScope = CoroutineScope(Job() + Dispatchers.Main)
}