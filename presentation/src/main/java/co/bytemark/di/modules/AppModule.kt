package co.bytemark.di.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import co.bytemark.CustomerMobileApp.Companion.getConf
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsFactory.getAnalyticsPlatforms
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.shopping_cart.GooglePayUtil
import dagger.Module
import dagger.Provides
import rx.Scheduler
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by Arunkumar on 03/10/16.
 * Dagger module to provide application context and other related thing to app across modules
 */
@Module
@Singleton
class AppModule(private val application: Application) {
    @Provides
    @Singleton
    fun providesApplication(): Application = application

    @Provides
    @Singleton
    fun providesContext(): Context = application

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(application)

    @Provides
    @Named("Thread")
    fun providesThreadScheduler(): Scheduler = Schedulers.io()

    @Provides
    @Named("PostExecution")
    fun providesPostExecutionScheduler(): Scheduler = AndroidSchedulers.mainThread()

    @Provides
    @Singleton
    fun providesAnalyticsHandler(application: Application): AnalyticsPlatformAdapter {
        val buildIdentifiers =
            getConf()?.clients?.customerMobile?.android?.buildIdentifiers
        val analyticsPlatformList =
            buildIdentifiers?.let { getAnalyticsPlatforms(it) }
        return AnalyticsPlatformAdapter(application, analyticsPlatformList!!)
    }

    @Provides
    @Singleton
    fun providesGooglePayUtil(): GooglePayUtil = GooglePayUtil()

    @Provides
    @Singleton
    fun provideErrorHandler(): ErrorHandler = BmErrorHandler()
}