package co.bytemark.di.modules

import co.bytemark.data.authentication.AuthenticationRepositoryImpl
import co.bytemark.data.credit_pass.CreditPassRepositoryImpl
import co.bytemark.data.discount.DiscountRepositoryImpl
import co.bytemark.data.fare_capping.FareCappingRepositoryImpl
import co.bytemark.data.fare_medium.FareMediumRepositoryImpl
import co.bytemark.data.filters.FiltersRespositoryImpl
import co.bytemark.data.manage.ManageRepositoryImpl
import co.bytemark.data.native_app_support.NativeAppSupportRepositoryImpl
import co.bytemark.data.newStoreFilters.NewFiltersRepositoryImpl
import co.bytemark.data.notification.NotificationRepositoryImpl
import co.bytemark.data.notification_settings.NotificationSettingsRepositoryImpl
import co.bytemark.data.passes.PassesRepositoryImpl
import co.bytemark.data.payments.PaymentsRepositoryImpl
import co.bytemark.data.private_key.PrivateKeyRepositoryImpl
import co.bytemark.data.product.ProductRepositoryImpl
import co.bytemark.data.purchase_history.OrderHistoryRepositoryImpl
import co.bytemark.data.resend_receipt.ResendReceiptRepositoryImpl
import co.bytemark.data.schedules.GTFSRepositoryImpl
import co.bytemark.data.securityquestions.SecurityQuestionRepositoryImpl
import co.bytemark.data.send_ticket.SendPassRepositoryImpl
import co.bytemark.data.subscriptions.SubscriptionsRepositoryImpl
import co.bytemark.data.ticket_history.TicketHistoryRepositoryImpl
import co.bytemark.data.ticket_storage.TransferPassRepositoryImpl
import co.bytemark.data.userphoto.UserPhotoRepositoryImpl
import co.bytemark.data.voucher_redeem.VoucherRedeemRepositoryImpl
import co.bytemark.domain.repository.*
import co.bytemark.sdk.data.identifiers.IdentifiersRepository
import co.bytemark.sdk.data.identifiers.IdentifiersRepositoryImpl
import co.bytemark.sdk.data.userAccount.UserAccountRepository
import co.bytemark.sdk.data.userAccount.UserAccountRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Provides
    @Singleton
    fun providesAuthenticationRepository(authenticationRepository: AuthenticationRepositoryImpl): AuthenticationRepository =
        authenticationRepository

    @Provides
    @Singleton
    fun providesNotificationRepository(notificationRepository: NotificationRepositoryImpl): NotificationRepository =
        notificationRepository

    @Provides
    @Singleton
    fun providesFareMediumRepository(fareMediumRepository: FareMediumRepositoryImpl): FareMediumRepository =
        fareMediumRepository

    @Provides
    @Singleton
    fun providesUserPhotoRepository(userPhotoRepository: UserPhotoRepositoryImpl): UserPhotoRepository =
        userPhotoRepository

    @Provides
    @Singleton
    fun providesPrivateKeyRepository(privateKeyRepository: PrivateKeyRepositoryImpl): PrivateKeyRepository =
        privateKeyRepository

    @Provides
    @Singleton
    fun productRepository(productRepository: ProductRepositoryImpl): ProductRepository =
        productRepository

    @Provides
    @Singleton
    fun paymentsRepository(paymentsRepository: PaymentsRepositoryImpl): PaymentsRepository =
        paymentsRepository

    @Provides
    @Singleton
    fun manageRepository(manageRepository: ManageRepositoryImpl): ManageRepository =
        manageRepository

    @Provides
    @Singleton
    fun providesNotificationSettingsRepository(notificationSettingsRepository: NotificationSettingsRepositoryImpl): NotificationSettingsRepository =
        notificationSettingsRepository

    @Provides
    @Singleton
    fun providesGTFSRepository(gtfsRepository: GTFSRepositoryImpl): GTFSRepository =
        gtfsRepository

    @Provides
    @Singleton
    fun providesOrderHistoryRepository(orderHistoryRepository: OrderHistoryRepositoryImpl): OrderHistoryRepository =
        orderHistoryRepository

    @Provides
    @Singleton
    fun providesVoucherRedeemRepository(voucherRedeemRepository: VoucherRedeemRepositoryImpl): VoucherRedeemRepository =
        voucherRedeemRepository

    @Provides
    @Singleton
    fun provides(subscriptionsRepository: SubscriptionsRepositoryImpl): SubscriptionsRepository =
        subscriptionsRepository

    @Provides
    @Singleton
    fun providesSecurityQuestions(securityQuestionRepository: SecurityQuestionRepositoryImpl): SecurityQuestionRepository =
        securityQuestionRepository

    @Provides
    @Singleton
    fun providesPassesRepository(passesRepository: PassesRepositoryImpl): PassesRepository =
        passesRepository

    @Provides
    @Singleton
    fun providesStoreFiltersRepository(filtersRepository: NewFiltersRepositoryImpl): StoreFiltersRepository =
        filtersRepository

    @Provides
    @Singleton
    fun providesFiltersRepository(filtersRepository: FiltersRespositoryImpl): FiltersRepository =
        filtersRepository

    @Provides
    @Singleton
    fun providesTicketHistoryRepository(ticketHistoryRepository: TicketHistoryRepositoryImpl): TicketHistoryRepository =
        ticketHistoryRepository

    @Provides
    @Singleton
    fun providesSendPassRepository(sendPassRepository: SendPassRepositoryImpl): SendPassRepository =
        sendPassRepository

    @Provides
    @Singleton
    fun providesResendReceiptRepository(resendReceiptRepository: ResendReceiptRepositoryImpl): ResendReceiptRepository =
        resendReceiptRepository

    @Provides
    @Singleton
    fun providesNativeAppSupportRepository(nativeAppSupportRepository: NativeAppSupportRepositoryImpl): NativeAppSupportRepository =
        nativeAppSupportRepository

    @Provides
    @Singleton
    fun providesFareCappingRepository(fareMediumRepository: FareCappingRepositoryImpl): FareCappingRepository =
        fareMediumRepository

    @Provides
    @Singleton
    fun providesIdentifiersRepository(identifiersRepository: IdentifiersRepositoryImpl): IdentifiersRepository {
        return identifiersRepository
    }

    @Provides
    @Singleton
    fun providesTransferPassRepository(transferPassRepository: TransferPassRepositoryImpl): TransferPassRepository {
        return transferPassRepository
    }

    @Provides
    @Singleton
    fun providesUserAccountRepository(userAccountRepository: UserAccountRepositoryImpl): UserAccountRepository {
        return userAccountRepository
    }

    @Provides
    @Singleton
    fun providesDiscountRepository(discountRepository: DiscountRepositoryImpl): DiscountRepository =
        discountRepository

    @Provides
    @Singleton
    fun providesCreditPassRepository(creditPassRepository: CreditPassRepositoryImpl): CreditPassRepository =
        creditPassRepository
}