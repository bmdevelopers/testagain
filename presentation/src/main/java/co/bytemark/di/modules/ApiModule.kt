package co.bytemark.di.modules

import co.bytemark.data.annotation.Account
import co.bytemark.data.annotation.CoroutineAccount
import co.bytemark.data.annotation.CoroutineOverture
import co.bytemark.data.annotation.Overture
import co.bytemark.data.net.AccountRestApi
import co.bytemark.data.net.CoroutineAccountApi
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.sdk.network_impl.API
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApiModule {
    // All legacy code uses overture
    @Provides
    @Singleton
    fun providesAPI(@Overture retrofit: Retrofit): API =
        retrofit.create(API::class.java)

    @Provides
    @Singleton
    fun providesOvertureRestApi(@Overture retrofit: Retrofit): OvertureRestApi =
        retrofit.create(OvertureRestApi::class.java)

    @Provides
    @Singleton
    fun providesCoroutineOvertureApi(@CoroutineOverture retrofit: Retrofit): CoroutineOvertureApi =
        retrofit.create(CoroutineOvertureApi::class.java)

    @Provides
    @Singleton
    fun providesAccountRestApi(@Account retrofit: Retrofit): AccountRestApi =
        retrofit.create(AccountRestApi::class.java)

    @Provides
    @Singleton
    fun providesCoroutineAccountApi(@CoroutineAccount retrofit: Retrofit): CoroutineAccountApi =
        retrofit.create(CoroutineAccountApi::class.java)
}