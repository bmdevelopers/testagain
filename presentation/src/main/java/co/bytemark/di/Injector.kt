package co.bytemark.di

import co.bytemark.MainActivity
import co.bytemark.addPaymentMethods.AddPaymentMethodsFragment
import co.bytemark.add_payment_card.AddPaymentCardActivity
import co.bytemark.add_payment_card.AddPaymentCardFragment
import co.bytemark.add_wallet.AddWalletFragment
import co.bytemark.appnotification.NotificationDetailFragmentNew
import co.bytemark.appnotification.NotificationFragment
import co.bytemark.appnotification.NotificationViewModel
import co.bytemark.appnotification.NotificationsAdapter
import co.bytemark.authentication.account_management.AccountManagementFragment
import co.bytemark.authentication.change_password.ChangePasswordFragment
import co.bytemark.authentication.delete_account.DeleteAccountFragment
import co.bytemark.authentication.forgot_password.ForgotPasswordFragment
import co.bytemark.authentication.forgot_password.ForgotPasswordViewModel
import co.bytemark.authentication.sign_up.SignUpFragment
import co.bytemark.authentication.sign_up.SignUpViewModel
import co.bytemark.authentication.signin.SignInFragment
import co.bytemark.authentication.voucher_code.VoucherCodeFragment
import co.bytemark.autoload.AutoloadFragment
import co.bytemark.base.BaseViewModel
import co.bytemark.base.MasterActivity
import co.bytemark.base.NavigationMenuAdapter
import co.bytemark.base.ToolbarActivity
import co.bytemark.buy_tickets.*
import co.bytemark.buy_tickets.filters.NewStoreFiltersFragment
import co.bytemark.buy_tickets.filters.StoreFiltersAdapter
import co.bytemark.buy_tickets.filters.StoreFiltersFragment
import co.bytemark.card_history.CardHistoryFragment
import co.bytemark.card_history.CardHistoryViewModel
import co.bytemark.di.components.AppComponent
import co.bytemark.fare_capping.FareCappingFragment
import co.bytemark.fare_capping.FareCappingViewModel
import co.bytemark.formly.adapter.FormlyAdapter
import co.bytemark.formly.adapterdelegates.ButtonAdapterDelegate.ButtonViewHolder
import co.bytemark.formly.adapterdelegates.DatePickerFragment
import co.bytemark.helpers.EmailIntentBuilder
import co.bytemark.helpers.RatingAndFeedBackHelper
import co.bytemark.incomm.SelectRetailerFragment
import co.bytemark.incomm.SelectRetailerViewModel
import co.bytemark.incomm.retailer_details.RetailerDetailsFragment
import co.bytemark.init_fare_capping.InitFareCappingFragment
import co.bytemark.manage.CardDetailsAdapter
import co.bytemark.manage.ManageCardsFragment
import co.bytemark.manage.available_passes.AvailablePassesFragment
import co.bytemark.manage.block_unblock_card.BlockUnblockCardFragment
import co.bytemark.manage.block_unblock_card.BlockUnblockCardViewModel
import co.bytemark.manage.createOrLinkVirtualCard.createVirtualCard.CreateVirtualCardFragment
import co.bytemark.manage.createOrLinkVirtualCard.createVirtualCard.CreateVirtualCardViewModel
import co.bytemark.manage.createOrLinkVirtualCard.linkExistingCard.LinkExistingCardFragment
import co.bytemark.manage.createOrLinkVirtualCard.linkExistingCard.LinkExistingCardViewModel
import co.bytemark.menu.MenuAdapter
import co.bytemark.menu.MenuFragment
import co.bytemark.nativeappsupport.NativeAppSupportFragment
import co.bytemark.nativeappsupport.NativeAppSupportViewModel
import co.bytemark.notification_settings.NotificationSettingsFragment
import co.bytemark.notification_settings.NotificationSettingsGroupAdapter
import co.bytemark.notification_settings.NotificationSettingsItemAdapter
import co.bytemark.notification_settings.NotificationSettingsViewModel
import co.bytemark.payment_methods.*
import co.bytemark.product_details.ProductDetailsActivity
import co.bytemark.product_details.ProductDetailsActivityFragment
import co.bytemark.purchase_history.OrderHistoryFragment
import co.bytemark.receipt.CardsPaidWithAdapter
import co.bytemark.receipt.ReceiptFragment
import co.bytemark.receipt.ReceiptPassAdapter
import co.bytemark.receipt.ReceiptViewModel
import co.bytemark.schedules.OriginDestinationSpinnerAdapter
import co.bytemark.schedules.SchedulesActivityFragment
import co.bytemark.schedules.SchedulesViewModel
import co.bytemark.securityquestion.UserSecurityQuestionChecker
import co.bytemark.securityquestion.base.SecurityQuestionBaseFragment
import co.bytemark.securityquestion.settings.SecurityQuestionSettingViewModel
import co.bytemark.securityquestion.signup.SecurityQuestionSignUpFragment
import co.bytemark.securityquestion.signup.SecurityQuestionSignUpViewModel
import co.bytemark.settings.SettingsFragment
import co.bytemark.settings.SettingsViewModel
import co.bytemark.settings.menu.SettingsMenuAdapter
import co.bytemark.shopping_cart.AcceptPaymentActivity
import co.bytemark.shopping_cart.AcceptPaymentFragment
import co.bytemark.shopping_cart.AcceptPaymentPresenter
import co.bytemark.shopping_cart_new.NewShoppingCartActivityFragment
import co.bytemark.splash.SplashScreenActivity
import co.bytemark.static_resource.ImageRendererFragment
import co.bytemark.subscriptions.ListSubscriptionsFragment
import co.bytemark.ticket_storage.TicketStorageAdapter
import co.bytemark.ticket_storage.TicketStorageFragment
import co.bytemark.transfer_virtual_card.TransferVirtualCardFragment
import co.bytemark.use_tickets.*
import co.bytemark.use_tickets.adapter.UseTicketsActiveTicketsAdapter
import co.bytemark.use_tickets.adapter.UseTicketsAdapterFareMedium
import co.bytemark.use_tickets.adapter.UseTicketsAdapterNew
import co.bytemark.use_tickets.passview.*
import co.bytemark.use_tickets.send_ticket_dialog.SendTicketToUserDialog
import co.bytemark.use_tickets.viewholder.FareMediumViewHolder
import co.bytemark.use_tickets.viewholder.SingleItemViewHolder
import co.bytemark.userphoto.AccountPhotoFragment
import co.bytemark.userphoto.AccountPhotoStandardsFragment
import co.bytemark.userphoto.AccountPhotoUploadFragment
import co.bytemark.userphoto.AccountPhotoViewModel
import co.bytemark.voucher_redeem.VoucherRedeemActivity
import co.bytemark.voucher_redeem.VoucherRedeemFragment
import co.bytemark.webview.WebViewActivity
import co.bytemark.webview.WebViewFragment
import co.bytemark.widgets.emptystateview.EmptyStateLayout
import co.bytemark.widgets.emptystateview.EmptyStateView
import co.bytemark.widgets.stackview.ViewHolder
import co.bytemark.widgets.swiperefreshlayout.BmSwipeRefreshLayout

class Injector private constructor(private val appComponent: AppComponent) {
    fun inject(useTicketsAvailableFragment: UseTicketsAvailableFragment) =
        appComponent.inject(useTicketsAvailableFragment)

    fun inject(useTicketsActiveFragment: UseTicketsActiveFragment) =
        appComponent.inject(useTicketsActiveFragment)

    fun inject(fareMediumFragment: FareMediumFragment) = appComponent.inject(fareMediumFragment)

    fun inject(rowViewHolder: RowViewHolder) = appComponent.inject(rowViewHolder)

    fun inject(useTicketsAdapterNew: UseTicketsAdapterNew) =
        appComponent.inject(useTicketsAdapterNew)

    fun inject(useTicketsActiveTicketsAdapter: UseTicketsActiveTicketsAdapter) =
        appComponent.inject(useTicketsActiveTicketsAdapter)

    fun inject(useTicketsAdapterFareMedium: UseTicketsAdapterFareMedium) =
        appComponent.inject(useTicketsAdapterFareMedium)

    fun inject(paymentMethodsPresenter: PaymentMethodsPresenter) =
        appComponent.inject(paymentMethodsPresenter)

    fun inject(masterActivity: MasterActivity) = appComponent.inject(masterActivity)

    fun inject(toolbarActivity: ToolbarActivity) = appComponent.inject(toolbarActivity)

    fun inject(webViewActivity: WebViewActivity) = appComponent.inject(webViewActivity)

    fun inject(addPaymentCardActivity: AddPaymentCardActivity) =
        appComponent.inject(addPaymentCardActivity)

    fun inject(splashScreenActivity: SplashScreenActivity) =
        appComponent.inject(splashScreenActivity)

    fun inject(paymentMethodsActivity: PaymentMethodsActivity) =
        appComponent.inject(paymentMethodsActivity)

    fun inject(useTicketsActivity: UseTicketsActivity) = appComponent.inject(useTicketsActivity)

    fun inject(buyTicketsActivity: BuyTicketsActivity) = appComponent.inject(buyTicketsActivity)

    fun inject(buyTicketsActivityFragment: BuyTicketsActivityFragment) =
        appComponent.inject(buyTicketsActivityFragment)

    fun inject(buyTicketsAdapter: BuyTicketsAdapter) = appComponent.inject(buyTicketsAdapter)

    fun inject(buyTicketsPresenter: BuyTicketsPresenter) = appComponent.inject(buyTicketsPresenter)

    fun inject(presenter: UseTickets.Presenter) = appComponent.inject(presenter)

    fun inject(ticketStorageAdapter: TicketStorageAdapter) = appComponent.inject(ticketStorageAdapter)

    fun inject(acceptPaymentActivity: AcceptPaymentActivity) =
        appComponent.inject(acceptPaymentActivity)

    fun inject(acceptPaymentFragment: AcceptPaymentFragment) =
        appComponent.inject(acceptPaymentFragment)

    fun inject(acceptPaymentPresenter: AcceptPaymentPresenter) =
        appComponent.inject(acceptPaymentPresenter)

    fun inject(ticketStorageFragment: TicketStorageFragment) =
        appComponent.inject(ticketStorageFragment)

    fun inject(webViewFragment: WebViewFragment) = appComponent.inject(webViewFragment)

    fun inject(receiptFragment: ReceiptFragment) = appComponent.inject(receiptFragment)

    fun inject(receiptPassAdapter: ReceiptPassAdapter) = appComponent.inject(receiptPassAdapter)

    fun inject(viewModel: ReceiptViewModel) = appComponent.inject(viewModel)

    fun inject(cardsPaidWithAdapter: CardsPaidWithAdapter) =
        appComponent.inject(cardsPaidWithAdapter)

    fun inject(productDetailsActivity: ProductDetailsActivity) =
        appComponent.inject(productDetailsActivity)

    fun inject(productDetailsActivityFragment: ProductDetailsActivityFragment) =
        appComponent.inject(productDetailsActivityFragment)

    fun inject(passViewFactory: PassViewFactory) = appComponent.inject(passViewFactory)

    fun inject(baseItemRowHolder: BaseItemRowHolder) = appComponent.inject(baseItemRowHolder)

    fun inject(singleItemRowHolder: SingleItemRowHolder) = appComponent.inject(singleItemRowHolder)

    fun inject(imageViewRowHolder: ImageViewRowHolder) = appComponent.inject(imageViewRowHolder)

    fun inject(viewHolder: ViewHolder) = appComponent.inject(viewHolder)

    fun inject(paymentMethodsFragment: PaymentMethodsFragment) =
        appComponent.inject(paymentMethodsFragment)

    fun inject(settingsFragment: SettingsFragment) = appComponent.inject(settingsFragment)

    fun inject(settingsMenuAdapter: SettingsMenuAdapter) = appComponent.inject(settingsMenuAdapter)

    fun inject(navigationMenuAdapter: NavigationMenuAdapter) =
        appComponent.inject(navigationMenuAdapter)

    fun inject(menuAdapter: MenuAdapter) = appComponent.inject(menuAdapter)

    fun inject(menuFragment: MenuFragment) = appComponent.inject(menuFragment)

    fun inject(notificationsAdapter: NotificationsAdapter) =
        appComponent.inject(notificationsAdapter)

    fun inject(notificationDetailFragment: NotificationDetailFragmentNew) =
        appComponent.inject(notificationDetailFragment)

    fun inject(multiSelectPassAdapter: MultiSelectPassAdapter) =
        appComponent.inject(multiSelectPassAdapter)

    fun inject(storeFiltersAdapter: StoreFiltersAdapter) = appComponent.inject(storeFiltersAdapter)

    fun inject(emptyStateView: EmptyStateView) = appComponent.inject(emptyStateView)

    fun inject(storeFiltersFragment: StoreFiltersFragment) =
        appComponent.inject(storeFiltersFragment)

    fun inject(accountPhotoUploadFragment: AccountPhotoUploadFragment) =
        appComponent.inject(accountPhotoUploadFragment)

    fun inject(multiSelectFragment: MultiSelectFragment) = appComponent.inject(multiSelectFragment)

    fun inject(mainActivity: MainActivity) = appComponent.inject(mainActivity)

    fun inject(accountPhotoStandardsFragment: AccountPhotoStandardsFragment) =
        appComponent.inject(accountPhotoStandardsFragment)

    fun inject(accountPhotoFragment: AccountPhotoFragment) =
        appComponent.inject(accountPhotoFragment)

    fun inject(emptyStateLayout: EmptyStateLayout) = appComponent.inject(emptyStateLayout)

    fun inject(forgotPasswordFragment: ForgotPasswordFragment) =
        appComponent.inject(forgotPasswordFragment)

    fun inject(formlyAdapter: FormlyAdapter) = appComponent.inject(formlyAdapter)

    fun inject(signUpFragment: SignUpFragment) = appComponent.inject(signUpFragment)

    fun inject(buttonViewHolder: ButtonViewHolder) = appComponent.inject(buttonViewHolder)

    fun inject(changePasswordFragment: ChangePasswordFragment) =
        appComponent.inject(changePasswordFragment)

    fun inject(accountManagementFragment: AccountManagementFragment) =
        appComponent.inject(accountManagementFragment)

    fun inject(voucherCodeFragment: VoucherCodeFragment) = appComponent.inject(voucherCodeFragment)

    fun inject(imageRendererFragment: ImageRendererFragment) =
        appComponent.inject(imageRendererFragment)

    fun inject(bmSwipeRefreshLayout: BmSwipeRefreshLayout) =
        appComponent.inject(bmSwipeRefreshLayout)

    fun inject(paymentsFragment: PaymentsFragment) = appComponent.inject(paymentsFragment)

    fun inject(newShoppingCartActivityFragment: NewShoppingCartActivityFragment) =
        appComponent.inject(newShoppingCartActivityFragment)

    fun inject(addPaymentMethodsFragment: AddPaymentMethodsFragment) =
        appComponent.inject(addPaymentMethodsFragment)

    fun inject(linkExistingCardFragment: LinkExistingCardFragment) =
        appComponent.inject(linkExistingCardFragment)

    fun inject(createVirtualCardFragment: CreateVirtualCardFragment) =
        appComponent.inject(createVirtualCardFragment)

    fun inject(manageCardsFragment: ManageCardsFragment) = appComponent.inject(manageCardsFragment)

    fun inject(buyProductsAdapter: BuyProductsAdapter) = appComponent.inject(buyProductsAdapter)

    fun inject(notificationSettingsFragment: NotificationSettingsFragment) =
        appComponent.inject(notificationSettingsFragment)

    fun inject(notificationSettingsGroupAdapter: NotificationSettingsGroupAdapter) =
        appComponent.inject(notificationSettingsGroupAdapter)

    fun inject(notificationSettingsItemAdapter: NotificationSettingsItemAdapter) =
        appComponent.inject(notificationSettingsItemAdapter)

    fun inject(availablePassesFragment: AvailablePassesFragment) =
        appComponent.inject(availablePassesFragment)

    fun inject(schedulesActivityFragment: SchedulesActivityFragment) =
        appComponent.inject(schedulesActivityFragment)

    fun inject(presenter: OriginDestinationSpinnerAdapter) = appComponent.inject(presenter)

    fun inject(datePickerFragment: DatePickerFragment) = appComponent.inject(datePickerFragment)

    fun inject(singleItemViewHolder: SingleItemViewHolder) =
        appComponent.inject(singleItemViewHolder)

    fun inject(fareMediumViewHolder: FareMediumViewHolder) =
        appComponent.inject(fareMediumViewHolder)

    fun inject(cardHistoryFragment: CardHistoryFragment) = appComponent.inject(cardHistoryFragment)

    fun inject(autoloadFragment: AutoloadFragment) = appComponent.inject(autoloadFragment)

    fun inject(fareCappingFragment: InitFareCappingFragment) =
        appComponent.inject(fareCappingFragment)

    fun inject(cardDetailsAdapter: CardDetailsAdapter) = appComponent.inject(cardDetailsAdapter)

    fun inject(paymentMethodOrderComparator: PaymentMethodOrderComparator) =
        appComponent.inject(paymentMethodOrderComparator)

    fun inject(emailIntentBuilder: EmailIntentBuilder) = appComponent.inject(emailIntentBuilder)

    fun inject(orderHistoryFragment: OrderHistoryFragment) =
        appComponent.inject(orderHistoryFragment)

    fun inject(listSubscriptionsFragment: ListSubscriptionsFragment) =
        appComponent.inject(listSubscriptionsFragment)

    fun inject(ratingAndFeedBackHelper: RatingAndFeedBackHelper) =
        appComponent.inject(ratingAndFeedBackHelper)

    fun inject(viewModel: SecurityQuestionSignUpViewModel) = appComponent.inject(viewModel)

    fun inject(viewModel: SecurityQuestionSettingViewModel) = appComponent.inject(viewModel)

    fun inject(viewModel: NotificationViewModel) = appComponent.inject(viewModel)

    fun inject(viewModel: BlockUnblockCardViewModel) = appComponent.inject(viewModel)

    fun inject(viewModel: SecurityQuestionSignUpFragment) = appComponent.inject(viewModel)

    fun inject(deleteAccountFragment: DeleteAccountFragment) =
        appComponent.inject(deleteAccountFragment)

    fun inject(fragment: SecurityQuestionBaseFragment) = appComponent.inject(fragment)

    fun inject(fragmentKt: NotificationFragment) = appComponent.inject(fragmentKt)

    fun inject(fragment: NewStoreFiltersFragment) = appComponent.inject(fragment)

    fun inject(addWalletFragment: AddWalletFragment) = appComponent.inject(addWalletFragment)

    fun inject(addPaymentCardFragment: AddPaymentCardFragment) =
        appComponent.inject(addPaymentCardFragment)

    fun inject(securityQuestionChecker: UserSecurityQuestionChecker) =
        appComponent.inject(securityQuestionChecker)

    fun inject(voucherRedeemActivity: VoucherRedeemActivity) =
        appComponent.inject(voucherRedeemActivity)

    fun inject(voucherRedeemFragment: VoucherRedeemFragment) =
        appComponent.inject(voucherRedeemFragment)

    fun inject(ticketHistoryFragment: TicketHistoryFragment) =
        appComponent.inject(ticketHistoryFragment)

    fun inject(ticketDetailsFragment: TicketDetailsFragment) =
        appComponent.inject(ticketDetailsFragment)

    fun inject(sendTicketToUserDialog: SendTicketToUserDialog) =
        appComponent.inject(sendTicketToUserDialog)

    fun inject(viewModel: BaseViewModel) = appComponent.inject(viewModel)

    fun inject(viewModel: NotificationSettingsViewModel) = appComponent.inject(viewModel)

    fun inject(transferVirtualCardFragment: TransferVirtualCardFragment) =
        appComponent.inject(transferVirtualCardFragment)

    fun inject(nativeAppSupportFragment: NativeAppSupportFragment) =
        appComponent.inject(nativeAppSupportFragment)

    fun inject(fragment: FareCappingFragment) = appComponent.inject(fragment)

    fun inject(viewModel: NativeAppSupportViewModel) = appComponent.inject(viewModel)

    fun inject(viewModel: SchedulesViewModel) = appComponent.inject(viewModel)

    fun inject(viewModel: AccountPhotoViewModel) = appComponent.inject(viewModel)

    fun inject(viewModel: CardHistoryViewModel) = appComponent.inject(viewModel)

    fun inject(fragment: BlockUnblockCardFragment) = appComponent.inject(fragment)

    fun inject(viewModel: FareCappingViewModel) = appComponent.inject(viewModel)

    fun inject(viewModel: ForgotPasswordViewModel) = appComponent.inject(viewModel)

    fun inject(selectRetailerFragment: SelectRetailerFragment) =
        appComponent.inject(selectRetailerFragment)

    fun inject(retailerDetailsFragment: RetailerDetailsFragment) =
        appComponent.inject(retailerDetailsFragment)

    fun inject(viewModel: SelectRetailerViewModel) = appComponent.inject(viewModel)

    fun inject(viewModel: CreateVirtualCardViewModel) = appComponent.inject(viewModel)

    fun inject(viewModel: LinkExistingCardViewModel) = appComponent.inject(viewModel)

    fun inject(signUpViewModel: SignUpViewModel) = appComponent.inject(signUpViewModel)

    fun inject(fragment: SignInFragment) = appComponent.inject(fragment)

    fun inject(viewModel: SettingsViewModel) = appComponent.inject(viewModel)

    companion object {
        private var injector: Injector? = null
        fun getInjector(appComponent: AppComponent): Injector {
            if (injector == null) {
                injector = Injector(appComponent)
            }
            return injector!!
        }
    }
}