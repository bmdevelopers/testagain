package co.bytemark.di.components

import co.bytemark.CustomerMobileApp
import co.bytemark.MainActivity
import co.bytemark.addPaymentMethods.AddPaymentMethodsFragment
import co.bytemark.addPaymentMethods.AddPaymentMethodsViewModel
import co.bytemark.add_payment_card.AddPaymentCardActivity
import co.bytemark.add_payment_card.AddPaymentCardFragment
import co.bytemark.add_payment_card.AddPaymentCardViewModel
import co.bytemark.add_wallet.AddWalletFragment
import co.bytemark.add_wallet.AddWalletViewModel
import co.bytemark.appnotification.NotificationDetailFragmentNew
import co.bytemark.appnotification.NotificationFragment
import co.bytemark.appnotification.NotificationViewModel
import co.bytemark.appnotification.NotificationsAdapter
import co.bytemark.authentication.account_management.AccountManagementFragment
import co.bytemark.authentication.change_password.ChangePasswordFragment
import co.bytemark.authentication.delete_account.DeleteAccountFragment
import co.bytemark.authentication.forgot_password.ForgotPasswordFragment
import co.bytemark.authentication.forgot_password.ForgotPasswordViewModel
import co.bytemark.authentication.manage_pin.ManagePinFragment
import co.bytemark.authentication.manage_pin.ManagePinViewModel
import co.bytemark.authentication.sign_up.SignUpFragment
import co.bytemark.authentication.sign_up.SignUpViewModel
import co.bytemark.authentication.signin.SignInFragment
import co.bytemark.authentication.signin.SignInViewModel
import co.bytemark.authentication.voucher_code.VoucherCodeFragment
import co.bytemark.autoload.AutoloadFragment
import co.bytemark.autoload.AutoloadViewModel
import co.bytemark.base.BaseViewModel
import co.bytemark.base.MasterActivity
import co.bytemark.base.NavigationMenuAdapter
import co.bytemark.base.ToolbarActivity
import co.bytemark.buy_tickets.*
import co.bytemark.buy_tickets.filters.NewStoreFiltersFragment
import co.bytemark.buy_tickets.filters.StoreFiltersAdapter
import co.bytemark.buy_tickets.filters.StoreFiltersFragment
import co.bytemark.buy_tickets.filters.StoreFiltersViewModel
import co.bytemark.card_history.CardHistoryFragment
import co.bytemark.card_history.CardHistoryViewModel
import co.bytemark.di.modules.*
import co.bytemark.fare_capping.FareCappingFragment
import co.bytemark.fare_capping.FareCappingViewModel
import co.bytemark.formly.adapter.FormlyAdapter
import co.bytemark.formly.adapterdelegates.ButtonAdapterDelegate.ButtonViewHolder
import co.bytemark.formly.adapterdelegates.DatePickerFragment
import co.bytemark.helpers.EmailIntentBuilder
import co.bytemark.helpers.RatingAndFeedBackHelper
import co.bytemark.incomm.SelectRetailerFragment
import co.bytemark.incomm.SelectRetailerViewModel
import co.bytemark.incomm.retailer_details.RetailerDetailsFragment
import co.bytemark.init_fare_capping.InitFareCappingFragment
import co.bytemark.init_fare_capping.InitFareCappingViewModel
import co.bytemark.manage.CardDetailsAdapter
import co.bytemark.manage.ManageCardActionFragment
import co.bytemark.manage.ManageCardsFragment
import co.bytemark.manage.available_passes.AvailablePassesFragment
import co.bytemark.manage.available_passes.AvailablePassesViewModel
import co.bytemark.manage.block_unblock_card.BlockUnblockCardFragment
import co.bytemark.manage.block_unblock_card.BlockUnblockCardViewModel
import co.bytemark.manage.createOrLinkVirtualCard.createVirtualCard.CreateVirtualCardFragment
import co.bytemark.manage.createOrLinkVirtualCard.createVirtualCard.CreateVirtualCardViewModel
import co.bytemark.manage.createOrLinkVirtualCard.linkExistingCard.LinkExistingCardFragment
import co.bytemark.manage.createOrLinkVirtualCard.linkExistingCard.LinkExistingCardViewModel
import co.bytemark.manage.remove_physical_card.RemovePhysicalFareMediumViewModel
import co.bytemark.manage.upass.UPassValidationFragment
import co.bytemark.manage.upass.UPassValidationViewModel
import co.bytemark.menu.MenuAdapter
import co.bytemark.menu.MenuFragment
import co.bytemark.menu.MenuViewModel
import co.bytemark.nativeappsupport.NativeAppSupportFragment
import co.bytemark.nativeappsupport.NativeAppSupportViewModel
import co.bytemark.notification_settings.NotificationSettingsFragment
import co.bytemark.notification_settings.NotificationSettingsGroupAdapter
import co.bytemark.notification_settings.NotificationSettingsItemAdapter
import co.bytemark.notification_settings.NotificationSettingsViewModel
import co.bytemark.payment_methods.*
import co.bytemark.product_details.ProductDetailsActivity
import co.bytemark.product_details.ProductDetailsActivityFragment
import co.bytemark.purchase_history.OrderHistoryFragment
import co.bytemark.purchase_history.OrderHistoryViewModel
import co.bytemark.receipt.CardsPaidWithAdapter
import co.bytemark.receipt.ReceiptFragment
import co.bytemark.receipt.ReceiptPassAdapter
import co.bytemark.receipt.ReceiptViewModel
import co.bytemark.rewards.velocia.VelociaRewardsFragment
import co.bytemark.rewards.velocia.VelociaRewardsViewModel
import co.bytemark.schedules.OriginDestinationSpinnerAdapter
import co.bytemark.schedules.SchedulesActivityFragment
import co.bytemark.schedules.SchedulesViewModel
import co.bytemark.securityquestion.UserSecurityQuestionChecker
import co.bytemark.securityquestion.base.SecurityQuestionBaseFragment
import co.bytemark.securityquestion.settings.SecurityQuestionSettingViewModel
import co.bytemark.securityquestion.signup.SecurityQuestionSignUpFragment
import co.bytemark.securityquestion.signup.SecurityQuestionSignUpViewModel
import co.bytemark.settings.SettingsFragment
import co.bytemark.settings.SettingsViewModel
import co.bytemark.settings.menu.SettingsMenuAdapter
import co.bytemark.shopping_cart.AcceptPaymentActivity
import co.bytemark.shopping_cart.AcceptPaymentFragment
import co.bytemark.shopping_cart.AcceptPaymentPresenter
import co.bytemark.shopping_cart_new.NewShoppingCartActivityFragment
import co.bytemark.splash.SplashScreenActivity
import co.bytemark.static_resource.ImageRendererFragment
import co.bytemark.subscriptions.ListSubscriptionsFragment
import co.bytemark.subscriptions.SubscriptionsViewModel
import co.bytemark.ticket_storage.TicketStorageAdapter
import co.bytemark.ticket_storage.TicketStorageFragment
import co.bytemark.ticket_storage.TicketStorageViewModel
import co.bytemark.transfer_balance.TransferBalanceFragment
import co.bytemark.transfer_balance.TransferBalanceViewModel
import co.bytemark.transfer_virtual_card.TransferVirtualCardFragment
import co.bytemark.transfer_virtual_card.TransferVirtualCardViewModel
import co.bytemark.use_tickets.*
import co.bytemark.use_tickets.adapter.UseTicketsActiveTicketsAdapter
import co.bytemark.use_tickets.adapter.UseTicketsAdapterFareMedium
import co.bytemark.use_tickets.adapter.UseTicketsAdapterNew
import co.bytemark.use_tickets.passview.*
import co.bytemark.use_tickets.send_ticket_dialog.SendTicketToUserDialog
import co.bytemark.use_tickets.viewholder.FareMediumViewHolder
import co.bytemark.use_tickets.viewholder.SingleItemViewHolder
import co.bytemark.userphoto.AccountPhotoFragment
import co.bytemark.userphoto.AccountPhotoStandardsFragment
import co.bytemark.userphoto.AccountPhotoUploadFragment
import co.bytemark.userphoto.AccountPhotoViewModel
import co.bytemark.voucher_redeem.VoucherRedeemFragment
import co.bytemark.voucher_redeem.VoucherRedeemViewModel
import co.bytemark.webview.WebViewActivity
import co.bytemark.webview.WebViewFragment
import co.bytemark.widgets.emptystateview.EmptyStateLayout
import co.bytemark.widgets.emptystateview.EmptyStateView
import co.bytemark.widgets.stackview.ViewHolder
import co.bytemark.widgets.swiperefreshlayout.BmSwipeRefreshLayout
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AppModule::class,
        NetworkModule::class,
        ApiModule::class,
        UiModule::class,
        RepositoryModule::class,
        LocalEntityStoreModule::class,
        RemoteEntityStoreModule::class,
        ConfigModule::class]
)
interface AppComponent {
    fun inject(rowViewHolder: RowViewHolder)
    fun inject(useTicketsAvailableFragment: UseTicketsAvailableFragment)
    fun inject(useTicketsActiveFragment: UseTicketsActiveFragment)
    fun inject(useTicketsAdapterNew: UseTicketsAdapterNew)
    fun inject(useTicketsActiveTicketsAdapter: UseTicketsActiveTicketsAdapter)
    fun inject(useTicketsAdapterFareMedium: UseTicketsAdapterFareMedium)
    fun inject(paymentMethodsPresenter: PaymentMethodsPresenter)
    fun inject(masterActivity: MasterActivity)
    fun inject(toolbarActivity: ToolbarActivity)
    fun inject(webViewActivity: WebViewActivity)
    fun inject(splashScreenActivity: SplashScreenActivity)
    fun inject(paymentMethodsActivity: PaymentMethodsActivity)
    fun inject(useTicketsActivity: UseTicketsActivity)
    fun inject(buyTicketsActivity: BuyTicketsActivity)
    fun inject(addPaymentCardActivity: AddPaymentCardActivity)
    fun inject(buyTicketsActivityFragment: BuyTicketsActivityFragment)
    fun inject(buyTicketsAdapter: BuyTicketsAdapter)
    fun inject(buyTicketsPresenter: BuyTicketsPresenter)
    fun inject(presenter: UseTickets.Presenter)
    fun inject(fareMediumFragment: FareMediumFragment)
    fun inject(acceptPaymentActivity: AcceptPaymentActivity)
    fun inject(acceptPaymentFragment: AcceptPaymentFragment)
    fun inject(acceptPaymentPresenter: AcceptPaymentPresenter)
    fun inject(ticketStorageFragment: TicketStorageFragment)
    fun inject(webViewFragment: WebViewFragment)
    fun inject(receiptFragment: ReceiptFragment)
    fun inject(receiptPassAdapter: ReceiptPassAdapter)
    fun inject(ticketStorageAdapter: TicketStorageAdapter)
    fun inject(cardsPaidWithAdapter: CardsPaidWithAdapter)
    fun inject(productDetailsActivity: ProductDetailsActivity)
    fun inject(productDetailsActivityFragment: ProductDetailsActivityFragment)
    fun inject(passViewFactory: PassViewFactory)
    fun inject(baseItemRowHolder: BaseItemRowHolder)
    fun inject(singleItemRowHolder: SingleItemRowHolder)
    fun inject(baseItemRowHolder: ImageViewRowHolder)
    fun inject(viewHolder: ViewHolder)
    fun inject(paymentMethodsFragment: PaymentMethodsFragment)
    fun inject(settingsFragment: SettingsFragment)
    fun inject(settingsMenuAdapter: SettingsMenuAdapter)
    fun inject(navigationMenuAdapter: NavigationMenuAdapter)
    fun inject(menuViewModel: MenuViewModel)
    fun inject(menuAdapter: MenuAdapter)
    fun inject(menuFragment: MenuFragment)
    fun inject(customerMobileApp: CustomerMobileApp)
    fun inject(notificationFragment: NotificationFragment)
    fun inject(notificationsAdapter: NotificationsAdapter)
    fun inject(notificationDetailFragment: NotificationDetailFragmentNew)
    fun inject(multiSelectPassAdapter: MultiSelectPassAdapter)
    fun inject(storeFiltersAdapter: StoreFiltersAdapter)
    fun inject(emptyStateView: EmptyStateView)
    fun inject(storeFiltersFragment: StoreFiltersFragment)
    fun inject(accountPhotoUploadFragment: AccountPhotoUploadFragment)
    fun inject(multiSelectFragment: MultiSelectFragment)
    fun inject(mainActivity: MainActivity)
    fun inject(accountPhotoStandardsFragment: AccountPhotoStandardsFragment)
    fun inject(accountPhotoFragment: AccountPhotoFragment)
    fun inject(emptyStateLayout: EmptyStateLayout)
    fun inject(forgotPasswordFragment: ForgotPasswordFragment)
    fun inject(formlyAdapter: FormlyAdapter)
    fun inject(signUpFragment: SignUpFragment)
    fun inject(buttonViewHolder: ButtonViewHolder)
    fun inject(changePasswordFragment: ChangePasswordFragment)
    fun inject(accountManagementFragment: AccountManagementFragment)
    fun inject(voucherCodeFragment: VoucherCodeFragment)
    fun inject(imageRendererFragment: ImageRendererFragment)
    fun inject(bmSwipeRefreshLayout: BmSwipeRefreshLayout)
    fun inject(paymentsFragment: PaymentsFragment)
    fun inject(newShoppingCartActivityFragment: NewShoppingCartActivityFragment)
    fun inject(linkExistingCardFragment: LinkExistingCardFragment)
    fun inject(createVirtualCardFragment: CreateVirtualCardFragment)
    fun inject(manageCardsFragment: ManageCardsFragment)
    fun inject(buyProductsAdapter: BuyProductsAdapter)
    fun inject(notificationSettingsFragment: NotificationSettingsFragment)
    fun inject(notificationSettingsGroupAdapter: NotificationSettingsGroupAdapter)
    fun inject(notificationSettingsItemAdapter: NotificationSettingsItemAdapter)
    fun inject(availablePassesFragment: AvailablePassesFragment)
    fun inject(schedulesActivityFragment: SchedulesActivityFragment)
    fun inject(originDestinationSpinnerAdapter: OriginDestinationSpinnerAdapter)
    fun inject(datePickerFragment: DatePickerFragment)
    fun inject(singleItemViewHolder: SingleItemViewHolder)
    fun inject(fareMediumViewHolder: FareMediumViewHolder)
    fun inject(cardHistoryFragment: CardHistoryFragment)
    fun inject(autoloadFragment: AutoloadFragment)
    fun inject(fareCappingFragment: InitFareCappingFragment)
    fun inject(cardDetailsAdapter: CardDetailsAdapter)
    fun inject(paymentMethodOrderComparator: PaymentMethodOrderComparator)
    fun inject(addPaymentMethodsFragment: AddPaymentMethodsFragment)
    fun inject(emailIntentBuilder: EmailIntentBuilder)
    fun inject(addPaymentMethodsViewModel: AddPaymentMethodsViewModel)
    fun inject(orderHistoryViewModel: OrderHistoryViewModel)
    fun inject(orderHistoryFragment: OrderHistoryFragment)
    fun inject(listSubscriptionsFragment: ListSubscriptionsFragment)
    fun inject(subscriptionsViewModel: SubscriptionsViewModel)
    fun inject(ratingAndFeedBackHelper: RatingAndFeedBackHelper)
    fun inject(securityQuestionSignUpViewModel: SecurityQuestionSignUpViewModel)
    fun inject(securityQuestionSettingViewModel: SecurityQuestionSettingViewModel)
    fun inject(securityQuestionSignUpFragment: SecurityQuestionSignUpFragment)
    fun inject(deleteAccountFragment: DeleteAccountFragment)
    fun inject(questionBaseFragment: SecurityQuestionBaseFragment)
    fun inject(addWalletFragment: AddWalletFragment)
    fun inject(securityQuestionChecker: UserSecurityQuestionChecker)
    fun inject(voucherRedeemFragment: VoucherRedeemFragment)
    fun inject(newStoreFiltersFragment: NewStoreFiltersFragment)
    fun inject(sendTicketToUserDialog: SendTicketToUserDialog)
    fun inject(addPaymentCardFragment: AddPaymentCardFragment)
    fun inject(viewModel: NotificationViewModel)
    fun inject(blockUnblockCardViewModel: BlockUnblockCardViewModel)
    fun inject(ticketHistoryFragment: TicketHistoryFragment)
    fun inject(ticketDetailsFragment: TicketDetailsFragment)
    fun inject(transferVirtualCardFragment: TransferVirtualCardFragment)
    fun inject(nativeAppSupportFragment: NativeAppSupportFragment)
    fun inject(fragment: BlockUnblockCardFragment)
    fun inject(viewModel: BaseViewModel)
    fun inject(selectRetailerFragment: SelectRetailerFragment)
    fun inject(managePinFragment: ManagePinFragment)
    fun inject(manageCardActionFragment: ManageCardActionFragment)
    fun inject(retailerDetailsFragment: RetailerDetailsFragment)
    fun inject(fragment: FareCappingFragment)
    fun inject(signInFragment: SignInFragment)
    fun inject(transferBalanceFragment: TransferBalanceFragment)
    fun inject(uPassValidationFragment: UPassValidationFragment)
    fun inject(velociaRewardsFragment: VelociaRewardsFragment)


    val addPaymentMethodsViewModel: AddPaymentMethodsViewModel
    val orderHistoryViewModel: OrderHistoryViewModel
    val subscriptionsViewModel: SubscriptionsViewModel
    val addWalletViewModel: AddWalletViewModel
    val voucherRedeemViewModel: VoucherRedeemViewModel
    val transferVirtualCardViewModel: TransferVirtualCardViewModel
    val notificationViewModel: NotificationViewModel
    val blockUnblockCardViewModel: BlockUnblockCardViewModel
    val removePhysicalFareMediumViewModel: RemovePhysicalFareMediumViewModel
    val ticketHistoryViewModel: TicketHistoryViewModel
    val addPaymentCardViewModel: AddPaymentCardViewModel
    val notificationSettingsViewModel: NotificationSettingsViewModel
    val nativeAppSupportViewModel: NativeAppSupportViewModel
    val initFareCappingViewModel: InitFareCappingViewModel
    val securityQuestionSettingViewModel: SecurityQuestionSettingViewModel
    val securityQuestionSignUpViewModel: SecurityQuestionSignUpViewModel
    val accountPhotoViewModel: AccountPhotoViewModel
    val schedulesViewModel: SchedulesViewModel
    val createVirtualCardViewModel: CreateVirtualCardViewModel
    val linkExistingCardViewModel: LinkExistingCardViewModel
    val cardHistoryViewModel: CardHistoryViewModel
    val selectRetailerViewModel: SelectRetailerViewModel
    val managePinViewModel: ManagePinViewModel
    val receiptViewModel: ReceiptViewModel
    val fareCappingViewModel: FareCappingViewModel
    val forgotPasswordViewModel: ForgotPasswordViewModel
    val signInViewModel: SignInViewModel
    val signUpViewModel: SignUpViewModel
    val transferBalanceViewModel: TransferBalanceViewModel
    val uPassValidationViewModel: UPassValidationViewModel
    val storeFiltersViewModel: StoreFiltersViewModel
    val settingsViewModel: SettingsViewModel
    val availablePassViewModel: AvailablePassesViewModel
    val menuViewModel: MenuViewModel
    val ticketStorageViewModel: TicketStorageViewModel
    val autoloadViewModel: AutoloadViewModel
    val velociaRewardsViewModel: VelociaRewardsViewModel

}