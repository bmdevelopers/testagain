package co.bytemark.transfer_balance

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.ToolbarActivity
import co.bytemark.helpers.AppConstants

class TransferBalanceActivity : ToolbarActivity() {

    override fun getLayoutRes() = R.layout.activity_transfer_balance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(intent?.extras?.getString(AppConstants.TITLE))
    }

    override fun onBackPressed() {
        supportFragmentManager.findFragmentById(R.id.transferBalanceFragment)?.let {
            if ((it as TransferBalanceFragment).canGoBack()) {
                super.onBackPressed()
            }
        }
    }

}