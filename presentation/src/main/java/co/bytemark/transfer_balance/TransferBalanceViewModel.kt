package co.bytemark.transfer_balance

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.bytemark.domain.interactor.fare_medium.*
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.fare_medium.FARE_MEDIUM_UNBLOCK_STATE
import co.bytemark.domain.model.fare_medium.FareMedium
import kotlinx.coroutines.launch
import rx.SingleSubscriber
import javax.inject.Inject

class TransferBalanceViewModel @Inject constructor(
        private val getFareMedia: GetFareMedia,
        private val getAutoLoadConfigUseCase: GetAutoLoadConfigUseCase,
        private val transferBalanceUseCase: TransferBalanceUseCase
) : ViewModel() {

    val fareMediumList by lazy {
        MutableLiveData<List<FareMedium>>()
    }
    val displayLoading by lazy {
        MutableLiveData<DisplayState>()
    }

    val transferBalanceStatus by lazy {
        MutableLiveData<Boolean>()
    }

    val errorLiveData by lazy {
        MutableLiveData<BMError>()
    }

    var maxCardLimit = 0

    fun getFareMediums() = viewModelScope.launch {
        displayLoading.postValue(DisplayState.GETTING_FARE_MEDIUM_LIST)
        getFareMedia.singleExecute(null, object : SingleSubscriber<List<FareMedium>>() {

            override fun onSuccess(fareMedia: List<FareMedium>) {
                fareMediumList.postValue(fareMedia.filter { it.state == FARE_MEDIUM_UNBLOCK_STATE })
                displayLoading.postValue(DisplayState.NONE)
            }

            override fun onError(error: Throwable) {
                errorLiveData.postValue(BMError.fromThrowable(error))
                displayLoading.postValue(DisplayState.NONE)
            }
        })
    }

    fun getAutoLoadConfig() = viewModelScope.launch {
        when (val result = getAutoLoadConfigUseCase.invoke(Unit)) {
            is Result.Success -> {
                result.data?.let {
                    maxCardLimit = it.walletLoadMoneyConfig?.maxWalletLoadValue ?: 0
                }
            }
            is Result.Failure -> {
                errorLiveData.postValue(result.bmError.first())
            }
        }
    }

    fun transferBalance(fromUUID: String, toUUID: String, amount: Int) = viewModelScope.launch {
        displayLoading.postValue(DisplayState.TRANSFERRING_BALANCE)
        when (val result = transferBalanceUseCase
                .invoke(TransferBalanceRequestData(fromUUID, toUUID, amount))) {
            is Result.Success -> {
                transferBalanceStatus.postValue(true)
            }
            is Result.Failure -> {
                errorLiveData.postValue(result.bmError.first())
            }
        }

    }


    enum class DisplayState {
        GETTING_FARE_MEDIUM_LIST,
        TRANSFERRING_BALANCE,
        NONE
    }

}