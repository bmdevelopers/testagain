package co.bytemark.transfer_balance

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.PopupWindow
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.helpers.AppConstants
import co.bytemark.manage.ManageCardsActivity
import co.bytemark.transfer_virtual_card.FareMediumSelectionAdapter
import co.bytemark.widgets.util.getViewModel
import co.bytemark.widgets.util.postDelay
import co.bytemark.widgets.util.requestAccessibilityFocus
import kotlinx.android.synthetic.main.fragment_transfer_balance.*
import kotlinx.android.synthetic.main.virtual_card_selection_dialog.view.*

class TransferBalanceFragment : BaseMvvmFragment() {

	private var fromFareMedium: FareMedium? = null
	private var toFareMedium: FareMedium? = null

	private lateinit var viewModel: TransferBalanceViewModel

	private var popupWindow: PopupWindow? = null

	private var showingPopUp = false

	override fun onInject() {
		CustomerMobileApp.appComponent.inject(this)
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		viewModel = getViewModel(CustomerMobileApp.appComponent.transferBalanceViewModel)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
							  savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_transfer_balance, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		activity?.intent?.getParcelableExtra<FareMedium>(AppConstants.FARE_MEDIA)?.let {
			onFromCardSelected(it)
		}

		fromCardTextView.setOnClickListener { showFromCardSelectionPopup() }
		fromCardArrow.setOnClickListener { showFromCardSelectionPopup() }
		toCardTextView.setOnClickListener { showToCardSelectionPopup() }
		toCardArrow.setOnClickListener { showToCardSelectionPopup() }
		toCardTextView.contentDescription = getString(R.string.transfer_balance_select_card_to_transfer_voonly)

		valueToTransferEditText.setText(confHelper.getConfigurationPurchaseOptionsCurrencySymbol(0))

		valueToTransferEditText.addTextChangedListener(object : TextWatcher {
			override fun afterTextChanged(s: Editable?) {
			}

			override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
			}

			override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
				s?.let {
					valueToTransferEditText.removeTextChangedListener(this)
					val cleanString = it.toString().replace("\\D".toRegex(), "")
					if (cleanString.isNotBlank()) {
						var parsed = cleanString.toInt()
						if (parsed > fromFareMedium!!.getTransferableBalance()) {
							parsed = fromFareMedium!!.getTransferableBalance()
						}
						transferBalance.isEnabled = parsed > 0
						val formatted = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(parsed)

						valueToTransferEditText.setText(formatted)
						valueToTransferEditText.setSelection(formatted.length)
						toFareMedium?.let {
							updateNewCardBalance(parsed, it)
						}

					}
					valueToTransferEditText.addTextChangedListener(this)

				}
			}

		})
		transferBalance.setOnClickListener {
			if (isOnline()) {
				fromFareMedium?.let { from ->
					toFareMedium?.let { to ->
						val valueToTransfer = valueToTransferEditText.text.toString().replace("\\D".toRegex(), "").toInt()
						viewModel.transferBalance(from.uuid, to.uuid, valueToTransfer)
					}
				}
			} else {
				connectionErrorDialog()
			}
		}

		emptyStateLayout.showContent()
		setupObservers()
		viewModel.getFareMediums()
		viewModel.getAutoLoadConfig()

		AccessibilityNodeInfo.obtain(fromCardTextView).setCanOpenPopup(true)
		AccessibilityNodeInfo.obtain(toCardTextView).setCanOpenPopup(true)
	}

	override fun onResume() {
		super.onResume()
		postDelay(200) {
			toCardTextView.requestAccessibilityFocus()
		}
	}

	private fun updateNewCardBalance(valueToTransfer: Int, fareMedium: FareMedium) {
		val newCardBalance = valueToTransfer + fareMedium.getTransferableBalance()
		newCardBalanceTextView.text = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(newCardBalance)
		if ((valueToTransfer + fareMedium.getTransferableBalance()) > viewModel.maxCardLimit) {
			newBalanceErrorMsg.visibility = View.VISIBLE
			transferBalance.isEnabled = false
			newBalanceErrorMsg.text = getString(R.string.transfer_balance_msg_card_balance_exceeds_max_value, confHelper.getConfigurationPurchaseOptionsCurrencySymbol(viewModel.maxCardLimit))
			postDelay(200) {
				newBalanceErrorMsg.requestAccessibilityFocus()
			}
		} else if (valueToTransfer > 0 && newCardBalance <= viewModel.maxCardLimit) {
			transferBalance.isEnabled = true
			newBalanceErrorMsg.visibility = View.INVISIBLE
		} else {
			newBalanceErrorMsg.visibility = View.INVISIBLE
		}
	}

	private fun showFromCardSelectionPopup() {
		viewModel.fareMediumList.value?.let {
			showSelectionPopUp(it, underLineFromCard, ::onFromCardSelected)
		}
	}

	private fun showToCardSelectionPopup() {
		viewModel.fareMediumList.value?.let {
			showSelectionPopUp(it.filter { it.fareMediumId != fromFareMedium?.fareMediumId }, underLineToCard, ::onToCardSelected)
		}
	}

	private fun onFromCardSelected(fareMedium: FareMedium) {
		fromFareMedium = fareMedium

		fromCardTextView.text = getString(R.string.transfer_balance_fare_medium_name_display_format, fareMedium.nickname, fareMedium.printedCardNumber)

		cardBalance.text = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(fareMedium.getTransferableBalance())
		if (fareMedium.getTransferableBalance() == 0) {
			cardBalanceErrorMsg.visibility = View.VISIBLE
			toCardTextView.isEnabled = false
			toCardArrow.isEnabled = false
			postDelay(200) {
				cardBalanceErrorMsg.requestAccessibilityFocus()
			}
		} else {
			cardBalanceErrorMsg.visibility = View.GONE
			toCardTextView.isEnabled = true
			toCardArrow.isEnabled = true
		}

		if(fareMedium.preTaxBalance != 0){
			cardBalanceLabel.setText(R.string.transfer_balance_transferable_balance)
		}else{
			cardBalanceLabel.setText(R.string.transfer_balance_card_balance)
		}

		toCardTextView.text = ""
		toCardTextView.contentDescription = getString(R.string.transfer_balance_select_card_to_transfer_voonly)
		valueToTransferEditText.setText(confHelper.getConfigurationPurchaseOptionsCurrencySymbol(0))
		newCardBalanceTextView.text = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(0)
		valueToTransferEditText.isEnabled = false
		transferBalance.isEnabled = false

		postDelay(200) {
			cardBalanceLabel.requestAccessibilityFocus()
		}
	}

	private fun onToCardSelected(fareMedium: FareMedium) {
		toFareMedium = fareMedium
		val displayName = getString(R.string.transfer_balance_fare_medium_name_display_format, fareMedium.nickname, fareMedium.printedCardNumber)
		toCardTextView.text = displayName
		toCardTextView.contentDescription = displayName
		valueToTransferEditText.isEnabled = true
		val valueToTransfer = valueToTransferEditText.text.toString().replace("\\D".toRegex(), "").toInt()
		updateNewCardBalance(valueToTransfer, fareMedium)
		postDelay(200) {
			valueToTransferLabel.requestAccessibilityFocus()
		}
	}


	private fun setupObservers() {
		viewModel.displayLoading.observe(viewLifecycleOwner, Observer {
			when (it) {
				TransferBalanceViewModel.DisplayState.TRANSFERRING_BALANCE -> {
					emptyStateLayout.showLoading(R.drawable.tickets_material, getString(R.string.loading))
				}

				TransferBalanceViewModel.DisplayState.GETTING_FARE_MEDIUM_LIST -> {
					emptyStateLayout.showLoading(R.drawable.tickets_material, getString(R.string.loading))
				}
				else -> {
					emptyStateLayout.showContent()
				}
			}
		})

		viewModel.errorLiveData.observe(viewLifecycleOwner, Observer {
			handleError(it)
		})

		viewModel.transferBalanceStatus.observe(viewLifecycleOwner, Observer {
			if (it) {
				emptyStateLayout.showEmpty(
						R.drawable.check_material,
						getString(R.string.success),
						getString(R.string.transfer_balance_msg_balance_transfer_success),
						getString(R.string.fare_medium_card_action_done))
				{

					activity?.setResult(Activity.RESULT_OK, Intent().apply {
						putExtra(ManageCardsActivity.FARE_MEDIUM_ID_TO_FOCUS, toFareMedium?.fareMediumId)
					})
					activity?.finish()
				}
				emptyStateLayout.sendAccessibilityEvent(AccessibilityEvent.TYPE_ANNOUNCEMENT)
			}
		})
	}

	private fun showSelectionPopUp(fareMediumList: List<FareMedium>, attachTo: View, itemSelectAction: (fareMedium: FareMedium) -> Unit) {
		if (!showingPopUp) {
			popupWindow = PopupWindow(context)
			popupWindow?.let {
				val popupView = LayoutInflater.from(context).inflate(R.layout.virtual_card_selection_dialog, null, false)
				popupView.virtualCardSelectorView.layoutManager = LinearLayoutManager(context)
				val adapter = FareMediumSelectionAdapter(fareMediumList, confHelper, FareMediumSelectionAdapter.ITEM_TYPE_MINIMAL) { fm ->
					postDelay(200) {
						it.dismiss()
						showingPopUp = false
						itemSelectAction(fm)
					}
				}
				popupView.virtualCardSelectorView.adapter = adapter
				popupView.minimumWidth = underLineFromCard.width
				it.setBackgroundDrawable(ColorDrawable(Color.WHITE))
				it.elevation = 28F
				it.width = underLineFromCard.width
				it.contentView = popupView
				it.showAsDropDown(attachTo)
				showingPopUp = true
				postDelay(500) {
					popupView.virtualCardSelectorView.getChildAt(0)?.apply {
						requestFocus()
						sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
					}

				}
			}
		}
	}

	override fun connectionErrorDialog(positiveTextId: Int, enableNegativeAction: Boolean, negativeTextId: Int,
									   finishOnDismiss: Boolean, positiveAction: () -> Unit, negativeAction: () -> Unit) {
		emptyStateLayout.showError(
				R.drawable.error_material,
				getString(R.string.connection_required),
				getString(R.string.connection_required_message),
				getString(R.string.transfer_balance_back_to_manage_screen)
		) {
			activity?.onBackPressed()
		}
	}

	fun canGoBack() =
			if (showingPopUp) {
				popupWindow?.dismiss()
				showingPopUp = false
				false
			} else {
				true
			}

	override fun showDefaultErrorDialog(title: String?, errorMsg: String?, finishActivity: Boolean) {
		emptyStateLayout.showError(
				R.drawable.error_material,
				title,
				errorMsg,
				getString(R.string.transfer_balance_back_to_manage_screen)
		) {
			activity?.onBackPressed()
		}
	}

}