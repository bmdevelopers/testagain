package co.bytemark.static_resource;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.helpers.ConfHelper;

/**
 * Created by Omkar on 10/23/17.
 */

public class ImageRendererFragment extends Fragment {

    private static final String FILENAME = "FILENAME";

    private String fileName;

    @BindView(R.id.photoView)
    ImageView imageView;

    @Inject
    ConfHelper confHelper;

    public ImageRendererFragment() {
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    public static ImageRendererFragment newInstance(String fileName) {
        ImageRendererFragment fragment = new ImageRendererFragment();
        Bundle args = new Bundle();
        args.putString(FILENAME, fileName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fileName = getArguments().getString(FILENAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image_renderer, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            showImage(fileName);
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void showImage(String fileName) {
        Glide.with(getActivity())
                .load(confHelper.getDrawableRes(fileName))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }
}
