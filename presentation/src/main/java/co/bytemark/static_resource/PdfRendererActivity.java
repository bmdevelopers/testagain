package co.bytemark.static_resource;

import android.os.Bundle;

import co.bytemark.R;
import co.bytemark.base.MasterActivity;

/**
 * Created by omkaramberkar on 10/20/17.
 */

public class PdfRendererActivity extends MasterActivity {

    public static final String EXTRA_TITLE = "title";
    public static final String EXTRA_FILENAME = "filename";

    private String title, fileName;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_pdf_renderer;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null) {
            title = getIntent().getStringExtra(EXTRA_TITLE);
            fileName = getIntent().getStringExtra(EXTRA_FILENAME);
        }

        setTitle(title);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, PdfRendererFragment.newInstance(fileName))
                .commit();
    }

    @Override
    protected boolean useHamburgerMenu() {
        return false;
    }
}
