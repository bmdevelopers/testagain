package co.bytemark

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

internal class IAndroid {
    @SerializedName("icons")
    @Expose
    var icons: Icons? = null

    @SerializedName("notification_icons")
    @Expose
    var notificationIcons: Icons? = null

    @SerializedName("splash_screens")
    @Expose
    var splashScreens: SplashScreens? = null

    @SerializedName("reverse_domain")
    @Expose
    var reverseDomain: String? = null

    @SerializedName("identifier")
    @Expose
    var identifier: String? = null

    @SerializedName("gradle_version")
    @Expose
    var gradleVersion: String? = null

    @SerializedName("build_identifiers")
    @Expose
    var buildIdentifiers: List<BuildIdentifier> = ArrayList()

    @SerializedName("hacon_lib_link")
    @Expose
    var haconLibLink: String? = null

    @SerializedName("hacon_piwik_lib_link")
    @Expose
    var haconPiwikLibLink: String? = null
}

internal class Branding {
    @SerializedName("logo_images")
    @Expose
    var logoImages: LogoImages? = null

    @SerializedName("indicators")
    @Expose
    var indicators: Indicators? = null
}

internal class BuildIdentifier {
    @SerializedName("service")
    @Expose
    var service: String? = null

    @SerializedName("key")
    @Expose
    var key: String? = null

    @SerializedName("web_client_id")
    @Expose
    var webClientId: String? = null
}

internal class ChildOrganization {
    @SerializedName("uuid")
    @Expose
    var uuid: String? = null

    @SerializedName("abbreviation")
    var abbreviation: String? = null

    @SerializedName("branding")
    @Expose
    var branding: Branding? = null
}

internal class Client {
    @SerializedName("client_id")
    @Expose
    var clientId: String? = null
}

internal class Clients {
    @SerializedName("customer_mobile")
    @Expose
    var customerMobile: CustomerMobile? = null
}

internal class CopyOperation {
    var from: String
    var to: String
    var renameTo = ""

    constructor(from: String, to: String, renameTo: String) {
        this.from = from
        this.to = to
        this.renameTo = renameTo
    }

    constructor(from: String, to: String) {
        this.from = from
        this.to = to
        renameTo = from
    }
}

internal class CustomerMobile {
    @SerializedName("display_name")
    @Expose
    var displayName: String? = null

    @SerializedName("reverse_domain")
    @Expose
    var reverseDomain: String? = null

    @SerializedName("identifier")
    @Expose
    var identifier: String? = null

    @SerializedName("location_enabled")
    @Expose
    var locationEnabled: Boolean? = null

    @SerializedName("hacon_sdk_enabled")
    @Expose
    var haconEnabled: Boolean? = null

    @SerializedName("remote_notification_enabled")
    @Expose
    var notificationEnabled = false

    @SerializedName("schemes")
    @Expose
    var schemes: List<String> = ArrayList()

    @SerializedName("client")
    @Expose
    var client: Client? = null

    @SerializedName("android")
    @Expose
    var android: IAndroid? = null
}

internal class Domain {
    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName("overture_url")
    @Expose
    var overtureUrl: String? = null

    @SerializedName("accounts_url")
    @Expose
    var accountsUrl: String? = null
}

internal class GoogleServices {
    @SerializedName("client")
    @Expose
    var clients: ArrayList<GoogleServicesClient>? = null
}

internal class GoogleServicesClient {
    @SerializedName("client_info")
    @Expose
    var clientInfo: ClientInfo? = null
}

internal class ClientInfo {
    @SerializedName("mobilesdk_app_id")
    @Expose
    var mobilesdkAppId: String? = null

    @SerializedName("android_client_info")
    @Expose
    var androidClientInfo: AndroidClientInfo? = null
}

internal class AndroidClientInfo {
    @SerializedName("package_name")
    @Expose
    var packageName: String? = null
}

internal class Conf {
    @SerializedName("domain")
    @Expose
    var domain: Domain? = null

    @SerializedName("organization")
    @Expose
    var organization: Organization? = null

    @SerializedName("clients")
    @Expose
    var clients: Clients? = null
}

internal class Icons {
    @SerializedName("mdpi")
    @Expose
    var mdpi: String? = null

    @SerializedName("hdpi")
    @Expose
    var hdpi: String? = null

    @SerializedName("xhdpi")
    @Expose
    var xhdpi: String? = null

    @SerializedName("xxhdpi")
    @Expose
    var xxhdpi: String? = null

    @SerializedName("xxxhdpi")
    @Expose
    var xxxhdpi: String? = null

    @SerializedName("google_play")
    @Expose
    private val googlePlay: String? = null
}

internal class Indicators {
    @SerializedName("success")
    @Expose
    var success: String? = null

    @SerializedName("error")
    @Expose
    var error: String? = null

    @SerializedName("warning")
    @Expose
    var warning: String? = null
}

internal class LogoImages {
    @SerializedName("logo_monochrome_mask")
    @Expose
    var logoMonochromeMask: String? = null

    @SerializedName("logo_square")
    @Expose
    var logoSquare: String? = null

    @SerializedName("logo_wide")
    @Expose
    var logoWide: String? = null

    @SerializedName("fare_medium_logo")
    @Expose
    var fareMediumLogo: String? = null
}

internal class Organization {
    @SerializedName("uuid")
    @Expose
    var uuid: String? = null

    @SerializedName("display_name")
    @Expose
    var displayName: String? = null

    @SerializedName("supported_locales")
    @Expose
    var supportedLocales: List<SupportedLocale> = ArrayList()

    @SerializedName("branding")
    @Expose
    var branding: Branding? = null

    @SerializedName("child_organizations")
    @Expose
    var childOrganizations: List<ChildOrganization> = ArrayList()

    @SerializedName("fare_medium_card_details")
    @Expose
    var fareMediumCardDetailsList: List<String> = ArrayList()
}

internal class SplashScreens {
    @SerializedName("mdpi")
    @Expose
    var mdpi: String? = null

    @SerializedName("hdpi")
    @Expose
    var hdpi: String? = null

    @SerializedName("xhdpi")
    @Expose
    var xhdpi: String? = null

    @SerializedName("xxhdpi")
    @Expose
    var xxhdpi: String? = null

    @SerializedName("xxxhdpi")
    @Expose
    var xxxhdpi: String? = null
}

internal class SupportedLocale {
    @SerializedName("display_name")
    @Expose
    var displayName: String? = null

    @SerializedName("language_code")
    @Expose
    var languageCode: String? = null

    @SerializedName("country_code")
    @Expose
    var countryCode: String? = null
}