package co.bytemark.tutorial;

import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;

/**
 * Created by Arunkumar on 22/09/17.
 */
@Singleton
public class TutorialManager {
    private final ConfHelper confHelper;
    private final SharedPreferences sharedPreferences;

    @Inject
    public TutorialManager(ConfHelper confHelper, SharedPreferences sharedPreferences) {
        this.confHelper = confHelper;
        this.sharedPreferences = sharedPreferences;
    }

    public boolean isPassTutorialEnabled() {
        return confHelper.getOrganizationPassRowConfigs() != null && !confHelper.getOrganizationPassRowConfigs().isEmpty()
                && confHelper.getOrganization().getPassConfiguration().getTutorialEnabled();
    }

    public void appLaunched() {
        if (isPassTutorialEnabled()) {
            sharedPreferences.edit().putBoolean(AppConstants.IS_LAUNCH, true).apply();
        }
    }

    public void setAppLaunchedFalse() {
        if (isPassTutorialEnabled()) {
            sharedPreferences.edit().putBoolean(AppConstants.IS_LAUNCH, false).apply();
        }
    }

    private boolean isAppLaunched() {
        return isPassTutorialEnabled() && sharedPreferences.getBoolean(AppConstants.IS_LAUNCH, false);
    }

    public boolean shouldShowPassTutorial() {
        return isPassTutorialEnabled() && isAppLaunched() && sharedPreferences.getBoolean(AppConstants.SHOW_TUTORIAL, true);
    }

    public void setPassTutorialShown() {
        if (isPassTutorialEnabled()) {
            sharedPreferences.edit().putBoolean(AppConstants.IS_LAUNCH, false).apply();
            sharedPreferences.edit().putBoolean(AppConstants.SHOW_TUTORIAL, false).apply();
        }
    }
}
