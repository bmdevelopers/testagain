package co.bytemark.helpers

import android.app.Application
import android.content.Intent
import androidx.core.app.TaskStackBuilder
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NavigationUtils @Inject internal constructor(private val application: Application) {

    /**
     * Enables back navigation for activities that are launched from the NavBar. See
     * `AndroidManifest.xml` to find out the parent activity names for each activity.
     */
    fun launchWithBackStack(intent: Intent) {
        if (application.applicationContext == null) return
        val builder =
            TaskStackBuilder.create(application.applicationContext)
        builder.addNextIntentWithParentStack(intent)
        builder.startActivities(intent.extras)
    }
}