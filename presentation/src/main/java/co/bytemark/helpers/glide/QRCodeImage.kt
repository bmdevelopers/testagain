package co.bytemark.helpers.glide

import co.bytemark.sdk.model.config.BarcodeValidation

class QRCodeImage(
    val qrPayload: ByteArray,
    val barcodeValidation: BarcodeValidation?
)