package co.bytemark.helpers.glide

import androidx.annotation.ColorInt

data class MenuItemImage(
    val imageKey: String? = null,
    @ColorInt
    val color: Int = 0
)