package co.bytemark.helpers.glide

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import co.bytemark.sdk.QrBitmapFactory
import com.bumptech.glide.Priority
import com.bumptech.glide.load.data.DataFetcher
import com.bumptech.glide.load.model.GenericLoaderFactory
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoaderFactory
import com.bumptech.glide.load.model.stream.StreamModelLoader
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.util.*
import kotlin.jvm.Throws

internal class QRCodeLoader :
    StreamModelLoader<QRCodeImage> {
    override fun getResourceFetcher(
        menuItemImage: QRCodeImage,
        width: Int,
        height: Int
    ): DataFetcher<InputStream> {
        return object : DataFetcher<InputStream> {
            var inputStream: InputStream? = null

            @Throws(Exception::class)
            override fun loadData(priority: Priority): InputStream? {
                inputStream = generateInputStreamForItem(menuItemImage, width, height)
                return inputStream
            }

            override fun cleanup() {
                closeStream()
            }

            private fun closeStream() {
                if (inputStream != null) {
                    try {
                        inputStream!!.close()
                    } catch (e: IOException) {
                        inputStream = null
                    }
                }
            }

            // Random id to force reload
            override fun getId(): String {
                return Random().nextInt().toString()
            }

            override fun cancel() {
                closeStream()
            }
        }
    }

    private fun generateInputStreamForItem(
        qrCodeImage: QRCodeImage?,
        width: Int,
        height: Int
    ): InputStream? {
        if (qrCodeImage == null) {
            return null
        }
        val bitmap: Bitmap = QrBitmapFactory.createIataAztec(qrCodeImage.qrPayload, width, height)
        return getInputStreamFromBitmap(bitmap)
    }

    private fun getInputStreamFromBitmap(bitmap: Bitmap): ByteArrayInputStream {
        val stream = ByteArrayOutputStream()
        bitmap.compress(CompressFormat.PNG, 0, stream)
        return ByteArrayInputStream(stream.toByteArray())
    }

    internal class Factory : ModelLoaderFactory<QRCodeImage, InputStream> {
        override fun build(
            context: Context,
            factories: GenericLoaderFactory
        ): ModelLoader<QRCodeImage, InputStream> {
            return QRCodeLoader()
        }

        override fun teardown() {}
    }
}