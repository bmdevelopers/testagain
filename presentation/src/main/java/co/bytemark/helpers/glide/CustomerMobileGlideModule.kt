package co.bytemark.helpers.glide

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.module.GlideModule
import java.io.InputStream

class CustomerMobileGlideModule : GlideModule {

    override fun applyOptions(context: Context, builder: GlideBuilder) {}

    override fun registerComponents(context: Context, glide: Glide) {
        glide.register(QRCodeImage::class.java, InputStream::class.java, QRCodeLoader.Factory())
        glide.register(CardTypeImage::class.java, InputStream::class.java, CardTypeImageLoader.Factory())
        glide.register(
            PaymentMarkerTypeImage::class.java,
            InputStream::class.java,
            PaymentMarkerTypeImageLoader.Factory()
        )
    }
}