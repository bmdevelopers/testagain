package co.bytemark.helpers.glide

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.graphics.Canvas
import android.graphics.RectF
import co.bytemark.sdk.model.payment_methods.Card
import co.bytemark.stylekit.BMWLPaymentAssets
import com.bumptech.glide.Priority
import com.bumptech.glide.load.data.DataFetcher
import com.bumptech.glide.load.model.GenericLoaderFactory
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoaderFactory
import com.bumptech.glide.load.model.stream.StreamModelLoader
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import kotlin.jvm.Throws

internal class PaymentMarkerTypeImageLoader :
    StreamModelLoader<PaymentMarkerTypeImage> {
    override fun getResourceFetcher(
        card: PaymentMarkerTypeImage,
        width: Int,
        height: Int
    ): DataFetcher<InputStream> {
        return object : DataFetcher<InputStream> {
            var inputStream: InputStream? = null

            @Throws(Exception::class)
            override fun loadData(priority: Priority): InputStream? {
                inputStream = generateInputStreamForItem(card, width, height)
                return inputStream
            }

            override fun cleanup() {
                closeStream()
            }

            private fun closeStream() {
                if (inputStream != null) {
                    try {
                        inputStream!!.close()
                    } catch (e: IOException) {
                        inputStream = null
                    }
                }
            }

            override fun getId(): String {
                return card.paymentMarkerType
            }

            override fun cancel() {
                closeStream()
            }
        }
    }

    private fun generateInputStreamForItem(
        card: PaymentMarkerTypeImage?,
        width: Int,
        height: Int
    ): InputStream? {
        val bitmap = Bitmap.createBitmap(
            width,
            height,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        val targetFrame = RectF(0F, 0F, width.toFloat(), height.toFloat())
        if (card == null) return null else when (card.paymentMarkerType) {
            Card.VISA -> BMWLPaymentAssets.drawVisaPaymentMark(
                canvas,
                targetFrame
            )
            Card.MASTERCARD -> BMWLPaymentAssets.drawMasterCardPaymentMark(
                canvas,
                targetFrame
            )
            Card.AMERICAN_EXPRESS -> BMWLPaymentAssets.drawAmericanExpressPaymentMark(
                canvas,
                targetFrame
            )
            Card.DISCOVER -> BMWLPaymentAssets.drawDiscoverPaymentMark(
                canvas,
                targetFrame
            )
            Card.DINERS_CLUB -> BMWLPaymentAssets.drawDinersClubPaymentMark(
                canvas,
                targetFrame
            )
            Card.JCB -> BMWLPaymentAssets.drawJCBPaymentMark(
                canvas,
                targetFrame
            )
            else -> BMWLPaymentAssets.drawDefaultPaymentMark(canvas, targetFrame)
        }
        return getInputStreamFromBitmap(bitmap)
    }

    private fun getInputStreamFromBitmap(bitmap: Bitmap): ByteArrayInputStream {
        val stream = ByteArrayOutputStream()
        bitmap.compress(CompressFormat.PNG, 0, stream)
        return ByteArrayInputStream(stream.toByteArray())
    }

    internal class Factory :
        ModelLoaderFactory<PaymentMarkerTypeImage, InputStream> {
        override fun build(
            context: Context,
            factories: GenericLoaderFactory
        ): ModelLoader<PaymentMarkerTypeImage, InputStream> {
            return PaymentMarkerTypeImageLoader()
        }

        override fun teardown() {}
    }
}