package co.bytemark.helpers

object AppConstants {
    // Request Codes
    const val MY_SCAN_REQUEST_CODE = 101
    const val ADD_CARD_REQUEST_CODE = 102
    const val PAYMENT_METHODS_REQUEST_CODE = 103
    const val PRODUCT_DETAILS_REQUEST_CODE = 104
    const val REQUEST_IMAGE_CAPTURE = 105
    const val REQUEST_SELECT_PICTURE = 106
    const val WEB_VIEW_PAYMENT_METHODS_REQUEST_CODE = 107
    const val ADD_PAYMENT_METHOD_REQUEST_CODE = 108
    const val GOOGLE_LOAD_PAYMENT_DATA_REQUEST_CODE = 109
    const val HAFAS_ACTIVITY_REQUEST_CODE = 110
    const val PAY_NEAR_ME_WEBVIEW_REQUEST_CODE = 111
    const val ADD_OR_LINK_CARD_REQUEST_CODE = 112
    const val LOAD_MONEY_REQUEST_CODE = 113
    const val DELETE_ACCOUNT_REQUEST_CODE = 114
    const val SECURITY_QUESTIONS_REQUEST_CODE = 115
    const val BLOCK_UNBLOCK_CARD_REQUEST_CODE = 116
    const val TRANSFER_BALANCE_REQUEST_CODE = 117
    const val INCOMM_WALLET_LOAD_REQUEST_CODE = 117
    const val REMOVE_PHYSICAL_CARD_REQUEST_CODE = 118
    const val FAREMEDIUM_LOAD_MONEY_REQUEST_CODE = 119
    const val ELERTS_REQUEST_CODE = 120
    const val UPASS_VALIDATION_REQUEST_CODE = 121

    @JvmField
    val MANAGE_SCREEN_REQUEST_CODES = listOf(
        ADD_OR_LINK_CARD_REQUEST_CODE,
        BLOCK_UNBLOCK_CARD_REQUEST_CODE,
        TRANSFER_BALANCE_REQUEST_CODE,
        REMOVE_PHYSICAL_CARD_REQUEST_CODE,
        FAREMEDIUM_LOAD_MONEY_REQUEST_CODE,
        UPASS_VALIDATION_REQUEST_CODE
    )

    /*Result Codes*/
    const val ADD_CARD_SUCCESS_CODE = 700
    const val ADD_WALLET_SUCCESS_CODE = 800
    const val TRANSFER_VIRTUAL_CARD_REQ_CODE = 100

    // Constants for HaCon sdk integration
    const val HACON_TRIP = "Trip"
    const val HACON_SCHEDULE = "Schedule"
    const val HACON_ALARMS = "Alarms"
    const val HACON_NEXT_DEPARTURE = "Next Departure"
    const val HACON_TRIP_PLANNER = "Trip Planner"
    const val HACON_SYSTEM_MAP = "System Map"
    const val HACON_SYSTEM_MAP_PDF = "System Map PDFs"
    const val HACON_NEXT_BUS = "Next Bus"
    const val HACON_NEXT_DART_BUS = "Next DART Bus"
    const val HACON_PRODUCT_UUID = "product_uuid"
    const val HACON_FARE_ID = "fare_id"
    const val HACON_SERVICE_LEVEL = "serviceLevel"
    const val HACON_BUY_PASS = "buyPass"
    const val ACTION = "action"
    const val HACONT_INTENT_MENU_DRAWER = "de.hafas.app.menu.drawer"
    const val HACON_NAVIGATION_CLASS_NAME = "co.bytemark.hacon.HaconActivity"
    const val HACON_NAVIGATION_MENU_CLASS_NAME = "co.bytemark.hacon.MenuProvider"
    const val HACON_NAVIGATION_METHOD_NAME = "getInstance"

    // Intent actions for show the trip request page
    const val ACTION_CONNECTION = "de.hafas.android.ACTION_SHOW_CONNECTION"

    // Intent actions for show the station board request page
    const val ACTION_STATIONBOARD = "de.hafas.android.ACTION_SHOW_STATIONBOARD"
    const val ACTION_ALARM = "de.hafas.android.ACTION_SHOW_PUSH"

    // Intent action for show the Network Maps page
    const val ACTION_SHOW_NETWORKMAPS = "de.hafas.android.ACTION_SHOW_NETWORKMAPS"

    // Intent action for show the Mobility Map page
    const val ACTION_SHOW_MOBILITYMAP = "de.hafas.android.ACTION_SHOW_MOBILITYMAP"

    // Constant for Authentication
    const val LOGIN_TYPE_FACEBOOK = "Facebook"
    const val LOGIN_TYPE_GOOGLE = "googleplus"
    const val LOGIN_TYPE_APPLE = "AppleId"
    const val FIREBASE_OAUTH_PROVIDER_APPLE = "apple.com"
    const val SOCIAL_LOGIN_SCOPE_NAME = "name"
    const val SOCIAL_LOGIN_SCOPE_EMAIL = "email"

    // Constants for Add PostCard Screen
    const val MAX_CARD_CVV_LENGTH = 4
    const val MAX_CARD_ZIP_POSTAL_CODE_LENGTH = 10
    const val MINIMUM_CARD_ZIP_POSTAL_CODE_LENGTH = 3
    const val MAX_CARD_ADDRESS_LENGTH = 30

    //Autoload and Load Money Screen
    const val RELOAD = "reload"
    const val RELOADING_WALLET = "reloading_wallet"

    // Constants for Manage Screen
    const val FARE_MEDIA = "fare_media"
    const val FARE_MEDIA_UUID = "fare_media_uuid"
    const val TITLE = "title"
    const val TRANSFER_TIME = "transfer_time"

    // Constants for Buy Tickets Screen
    const val ENTITY_RESULT_LIST = "entity_result_list"
    const val FARE_MEDIA_ID = "fare_media_id"
    const val PRODUCT_UUID = "product_uuid"
    const val PRODUCT_UUID_REPURCHASE = "product_uuid_repurchase"
    const val FARE_ID = "fare_id"
    const val GTFS_FARE_ID = "gtfs_fare_id"
    const val LICENSE_NUMBER = "licence_no"
    const val SERVICE_LEVEL = "serviceLevel"

    // Constants for Shopping Cart Screen
    const val SHOPPING_CART_INTENT = "shopping_cart_intent"
    const val SHOPPING_CART_FROM_HACON = "fromHacon"
    const val FARE_MEDIUM_ID = "fareMediumId"
    const val SHOPPING_CART_FROM_DEEPLINK = "fromDeepLink"

    // Constants for Payment Methods Screen
    const val SELECTED_CARD = "selected_card"
    const val SPLIT_PAYMENT = "split_payment"
    const val ORG_UUID = "organization_UUID"

    // Constants for Buy Tickets Screen
    const val SELECTED_ENTITY_RESULT = "selected_entity_result"
    const val ADDED_ENTITY_RESULT = "added_entity_result"
    const val DEFAULT_DEVICE_STORAGE = "default_ticket_storage_location"
    const val SHOW_TUTORIAL = "SHOW_TUTORIAL"
    const val IS_LAUNCH = "IS_LAUNCH"

    // Payments results
    const val RESULT_AUTHORIZED = "AUTHORISED"
    const val RESULT_REFUSED = "REFUSED"
    const val RESULT_CANCELLED = "CANCELLED"
    const val RESULT_PENDING = "PENDING"
    const val PAYMENT_RESULT = "PAYMENT_RESULT"

    //Payment Types
    const val PAYMENT_TYPE_PAYPAL = "paypal"
    const val PAYMENT_TYPE_GOOGLE_PAY = "googlepay"
    const val PAYMENT_TYPE_IDEAL = "ideal"
    const val PAYMENT_TYPE_PAY_NEAR_ME = "paynearme"
    const val PAYMENT_TYPE_DOT_PAY = "dotpay"
    const val PAYMENT_TYPE_WALLET = "walletpay"
    const val PAYMENT_TYPE_CARD = "card"
    const val PAYMENT_TYPE_INCOMM = "incomm"

    //Payment Types
    const val CARD_TYPE_VISA = "visa"
    const val CARD_TYPE_MASTERCARD = "mastercard"
    const val CARD_TYPE_MAESTRO = "Maestro"
    const val CARD_TYPE_AMERICAN_EXPRESS = "americanexpress"
    const val CARD_TYPE_DISCOVER = "discover"
    const val CARD_TYPE_DINERS_CLUB = "dinersclub"
    const val CARD_TYPE_JCB = "jcb"
    const val CARD_TYPE_UNION_PAY = "unionpay"
    const val CARD_TYPE_SOLO_DEBIT = "solodebit"
    const val CARD_TYPE_CIRRUS_DEBIT = "cirrusdebit"
    const val DELETED_PAYMENT = "deleted_payment"
    const val ACCEPTED_PAYMENTS = "accepted_payments"
    const val MULTIPLE_PRODUCT_USAGE = "MULTIPLE_PRODUCT_USAGE"
    const val AUTOLOAD = "AUTOLOAD"
    const val GTFS_ORIGIN = "gtfs_origin_stop_id"
    const val GTFS_DESTINATION = "gtfs_destination_stop_id"
    const val CATEGORY_NAME = "categoryName"
    const val WALLET = "wallet"
    const val HIDE_ADD_PAYMENT = "HIDE_ADD_PAYMENT"
    const val HIDE_DELETE_PAYMENT = "HIDE_DELETE_PAYMENT"
    const val LICENSE_PLATE_NUMBER = "license_plate_number"
    const val SPOT_NUMBER = "spot_number"
    const val FROM_NATIVE_SCHEDULE = "from_native_schedule"

    //Security Question
    const val SECURITY_QUESTIONS_DATA = "SECURITY_QUESTIONS_DATA"
    const val SECURITY_QUESTIONS_SIGN_UP = "SECURITY_QUESTIONS_SIGN_UP"
    const val SECURITY_QUESTIONS_SETTING = "SECURITY_QUESTIONS_SETTING"

    //Subscription
    const val SUBSCRIPTION_FAILED = "SUBSCRIPTION_FAILED"
    const val SUBSCRIPTION_FARE_MEDIA_ID = "faremedia_id"
    const val CREATE_SUBSCRIPTION = "create_subscription"
    const val SUBSCRIPTION_SUSPENDED = "SUSPENDED"

    // TVM Voucher redeem
    const val PAYMENT_TYPE_VOUCHER_CODE = "VOUCHER_CODE"

    //Constants for New Filter Screen
    const val LIST_FILTER_WIDE_WIDTH = "wide"

    //Constants for Use Tickets
    const val USE_TICKETS_AVAILABLE_FRAGMENT_TYPE = "available"
    const val USE_TICKETS_ACTIVE_FRAGMENT_TYPE = "active"
    const val USE_TICKETS_HISTORY_FRAGMENT_TYPE = "history"
    const val USE_TICKETS_STATUS_USABLE = "USABLE"
    const val USE_TICKETS_STATUS_USING = "USING"
    const val USE_TICKETS_STATUS_EXPIRED = "EXPIRED"
    const val USE_TICKETS_STATUS_USES_GONE = "USES_GONE"
    const val NAVIGATION_TICKET_DETAILS = 6

    // Purchase history receipt type constants
    const val ORDER_TYPE_SALE = 1
    const val ORDER_TYPE_REFUND = 2
    const val ORDER_TYPE_LOAD_STORE_WALLET = "LOAD_STORED_VALUE"

    // Intent parameter for Analytics
    const val EXTRA_ORIGIN = "origin"
    const val USE_TICKETS_FRAGMENT_TYPE = "fragment_type"
    const val INTENT_FOR_REPURCHASE = "repurchase"
    const val PRODUCT_NAME = "product_name"
    const val CLIENT_ID = "client_id"

    // Payment status for incomm
    const val PAYMENT_STATUS_PENDING = "PAYMENTS_PENDING"
    const val INTENT_INCOMM_BARCODE_DETAIL = "INCOMM_BARCODE_DETAIL"
    const val INTENT_INCOMM_STORE = "INCOMM_STORE"
    const val INTENT_INCOMM_ORDER_AMOUNT = "ORDER_INCOMM_AMOUNT"
    const val INTENT_INCOMM_WALLET_LOAD = "INCOMM_WALLET_LOAD"
    const val INTENT_INCOMM_FAREMEDIUM_LOAD = "INCOMM_FAREMEDIUM_LOAD"
    const val INTENT_INCOMM_HISTORY_WALLET_LOAD = "INCOMM_HISTORY_WALLET_LOAD"
    const val INTENT_INCOMM_USE_TICKET = "INTENT_INCOMM_USE_TICKET"
    const val INTENT_INCOMM_ORDER_UUID = "INTENT_INCOMM_ORDER_UUID"

    //Elerts
    const val ELERTS_UTIL_CLASS_NAME = "co.bytemark.elerts.ElertsUtil"
    const val ELERTS_REG_METHOD_NAME = "registerECUIOrg"
    const val ELERTS_MESSAGE_CLASS_NAME = "com.elerts.ecsdk.ui.ECUISDK"
    const val ELERTS_MESSAGE_CLASS_FIELD_NAME = "MessageListClass"
    const val ELERTS_REPORT_CLASS_NAME = "co.bytemark.elerts.ElertsReportActivity"
    const val ELERTS_MESSAGE_SERVICE_CLASS_NAME =
            "com.elerts.ecsdk.ui.services.ECMessageListService"
    const val ELERTS_MESSAGE_SERVICE_RESOURCE_NAME = "LIST_MESSAGE_SERVICE"
    const val ELERTS_INIT_CONTEXT_METHOD_NAME = "getBaseContext"
    const val ELERTS_API_HELPER_METHOD_NAME = "fetchECUINotifications"
    const val ELERTS_SHOW_CALL_PROMPT_METHOD_NAME = "showCallPromptDialog"
    const val ELERTS_SEND_DEVICE_TOKEN_METHOD_NAME = "sendDeviceTokenToElert"
    const val ELERTS_CLEAR_ALERTS_METHOD_NAME = "clearAlerts"
    const val ELERTS_GET_ORG_METHOD_NAME = "getOrg"


    const val DEFAULT_LOCALE_CODE = "en-US"

    // info icon screen names
    const val SCREEN_CREATE_VIRTUAL_CARD = "create_virtual_card"
    const val SCREEN_LINK_PHYSICAL_CARD = "link_physical_card"
    const val SCREEN_INIT_FARE_CAPPING = "init_fare_capping"


    //Deeplink Discount
    const val DEEPLINK_JWT_TOKEN = "jwtTokenForDeeplink"
    const val DEEPLINK_DISCOUNT = "deeplinkDiscount"
    const val AUTH_TOKEN_FOR_DEEPLINK = "oauth_token"
    const val PRODUCT_ID = "product_id"
    const val DISCOUNT_PERCENT = "discount_percent"
    const val QUANTITY = "quantity"
    const val DEEPLINK_URL = "deepLinkUrl"

}