package co.bytemark.helpers


import android.content.Context
import android.content.Intent
import android.net.Uri
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.sdk.Api.ApiUtility
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.BytemarkSDK.SDKUtility.getConf
import co.bytemark.sdk.model.common.AppRatingAndFeedBack
import com.afollestad.materialdialogs.MaterialDialog
import java.util.*
import javax.inject.Inject

/**
 * Created by vally on 24/07/19.
 */

class RatingAndFeedBackHelper internal constructor(internal var context: Context) {

    @Inject
    lateinit var confHelper: ConfHelper

    val RATING_POP_UP = "ratingPopUp"


    init {
        CustomerMobileApp.component.inject(this)
    }


    var appRatingFeedback: AppRatingAndFeedBack
        get() {
            context.getSharedPreferences("SHARED_PREFS", Context.MODE_PRIVATE).getString(RATING_POP_UP, "").let {
                if (it.isNullOrEmpty()) {
                    return AppRatingAndFeedBack()
                }
                return BytemarkSDK.getGson().fromJson(it, AppRatingAndFeedBack::class.java)
            }
        }
        set(value) {
            context.getSharedPreferences("SHARED_PREFS", Context.MODE_PRIVATE).edit().putString(RATING_POP_UP, BytemarkSDK.getGson().toJson(value)).apply()
        }


    fun checkAndUpdateTheCount() {

        appRatingFeedback = appRatingFeedback.apply {

            val date = ApiUtility.getCalendarFromS(startDate)
            val currentDate: Calendar? = Calendar.getInstance()
            val calender = getTheDate(date, getConf().organization.appRatingConfiguration?.resetTheCountMinutes)
            if (startDate==null || startDate!!.isEmpty() || (calender != null && currentDate!! >= calender)) {
                noOfPopUpShown = 0
                sessions = 1
                this.startDate = ApiUtility.getSFromCalendar(GregorianCalendar())
                popUpLastShownDate = ""
            } else {
                sessions += 1
            }

        }

    }


    fun checkDisplayRequired(): Boolean? {

        with(appRatingFeedback) {

            val date = if (popUpLastShownDate!!.isEmpty()) {
                ApiUtility.getCalendarFromS(startDate)
            } else {
                ApiUtility.getCalendarFromS(popUpLastShownDate)
            }


            val calendar = getTheDate(date, confHelper.organization?.appRatingConfiguration?.delayForEachPopUpMinutes)
            if (sessions >= confHelper.organization?.appRatingConfiguration?.sessions ?: 0 &&
                    (calendar != null && Calendar.getInstance() >= calendar) &&
                    noOfPopUpShown <= confHelper.organization?.appRatingConfiguration?.noOfPopUps ?: 0) {
                return true
            }
        }
        return false
    }


    fun showRatingPopUP() {
        appRatingFeedback = appRatingFeedback.apply {
            noOfPopUpShown += 1
            popUpLastShownDate = ApiUtility.getSFromCalendar(GregorianCalendar())
        }
        with(context) {
            confHelper.let {
                MaterialDialog.Builder(this)
                        .title(getString(R.string.rating_pop_up_title) + " " + it.organization?.displayName?.toString())
                        .content(getString(R.string.rating_pop_up_content) + " " + it.organization?.displayName.toString() + getString(R.string.rating_pop_up_content_remaining))
                        .positiveText(getString(R.string.rating_pop_up_yes_button) + " " + it.organization?.displayName.toString())
                        .negativeText(getString(R.string.rating_pop_up_feed_back_button))
                        .neutralText(getString(R.string.rating_pop_up_no_thanks_button))
                        .positiveColor(it.dataThemeAccentColor)
                        .negativeColor(it.dataThemeAccentColor)
                        .neutralColor(it.dataThemeAccentColor)
                        .callback(object : MaterialDialog.ButtonCallback() {
                            override fun onPositive(dialog: MaterialDialog) {
                                super.onPositive(dialog)
                                startActivity(Intent(Intent.ACTION_VIEW).apply {
                                    data = Uri.parse(it.organization?.appRatingConfiguration?.playStoreLink)
                                })
                                dialog.dismiss()
                            }

                            override fun onNegative(dialog: MaterialDialog) {
                                super.onNegative(dialog)
                                it.organization?.appRatingConfiguration?.feedBackEmail?.let {
                                  EmailIntentBuilder(this@with).to(it).subject(context.getString(R.string.feed_back_email_subject)).build().start()
                              } ?: kotlin.run {
                                  EmailIntentBuilder(this@with).to(getString(R.string.feed_back_email)).subject(context.getString(R.string.feed_back_email_subject)).build().start()

                              }
                                dialog.dismiss()
                            }

                            override fun onNeutral(dialog: MaterialDialog) {
                                super.onNeutral(dialog)
                                dialog.dismiss()
                            }
                        }).show()
            }
        }
    }


    private fun getTheDate(date: Calendar?, time: Int?): Calendar? {
        date?.add(Calendar.MINUTE, time ?: 0)
        return date
    }
}
