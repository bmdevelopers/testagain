package co.bytemark.helpers

import co.bytemark.sdk.Pass
import java.util.*

class PassDateComparator : Comparator<Pass> {
    override fun compare(lhs: Pass?, rhs: Pass?): Int {
        val lhsEarliestUseEnd = lhs?.expiration
        val rhsEarliestUseEnd = rhs?.expiration
        if ((lhsEarliestUseEnd == null) xor (rhsEarliestUseEnd == null)) return if (lhs == null) -1 else 1
        if (lhsEarliestUseEnd == null && rhsEarliestUseEnd == null) return 0
        return if (lhsEarliestUseEnd!!.before(rhsEarliestUseEnd)) -1 else 1
    }
}