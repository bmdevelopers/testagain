package co.bytemark.helpers

import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.widget.CheckBox
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import com.google.android.material.textfield.TextInputLayout
import java.util.regex.Pattern

class FormValidator(val formly: Formly, val textInputLayout: TextInputLayout) : TextWatcher {
    private val required: Boolean? = formly.templateOptions.required
    private val maxLength: Int? = formly.templateOptions.maxLength
    private val minLength: Int? = formly.templateOptions.minLength
    private val pattern: String? = formly.templateOptions.pattern
    private var str: String? = null
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    override fun afterTextChanged(s: Editable) {
        str = s.toString().trim { it <= ' ' }
    }

    private val isTextValid: Boolean
        get() = TextUtils.isEmpty(pattern) || !TextUtils.isEmpty(str) && Pattern.compile(pattern).matcher(str).matches()

    private val isRequiredEmpty: Boolean
        get() = required != null && required && TextUtils.isEmpty(str)

    private val isLessThanMinLength: Boolean
        get() = minLength != null && (TextUtils.isEmpty(str) || str!!.length < minLength)

    private val isGreaterThanMaxLength: Boolean
        get() = maxLength != null && str != null && str!!.length > maxLength

    fun validate() {
        val stringBuilder = StringBuilder()
        val context = textInputLayout.context
        if (!isTextValid) {
            stringBuilder.append(context.getString(R.string.validation_invalid, formly.templateOptions.label))
        }
        if (isRequiredEmpty) {
            stringBuilder.append(context.getString(R.string.validation_required, formly.templateOptions.label))
        }
        if (isGreaterThanMaxLength) {
            stringBuilder.append(context.getString(R.string.validation_max_length, maxLength, formly.templateOptions.label))
        }
        if (isLessThanMinLength) {
            stringBuilder.append(context.getString(R.string.validation_min_length, minLength, formly.templateOptions.label))
        }
        if (!TextUtils.isEmpty(stringBuilder)) {
            textInputLayout.error = stringBuilder.toString().trim { it <= ' ' }
        } else {
            textInputLayout.isErrorEnabled = false
        }
    }

    val isValid: Boolean
        get() {
            var isValid = true
            if (!isTextValid) isValid = false
            if (isRequiredEmpty) isValid = false
            if (isGreaterThanMaxLength) isValid = false
            if (isLessThanMinLength) isValid = false
            return isValid
        }

}