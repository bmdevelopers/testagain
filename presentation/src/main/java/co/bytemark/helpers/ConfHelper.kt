package co.bytemark.helpers

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.text.TextUtils
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.os.ConfigurationCompat
import co.bytemark.AssetParser
import co.bytemark.CustomerMobileApp.Companion.getConf
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.sdk.model.config.*
import co.bytemark.sdk.model.menu.MenuGroup
import co.bytemark.sdk.model.menu.MenuItem
import co.bytemark.sdk.model.more_info.MoreInfoList
import co.bytemark.sdk.model.payment_methods.PaymentsOrderName
import co.bytemark.sdk.model.settings.SettingsList
import co.bytemark.sdk.remote_config.RemoteConfigHelper
import co.bytemark.sdk.schedules.ScheduleItem
import timber.log.Timber
import java.math.BigDecimal
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.ArrayList

@Singleton
class ConfHelper @Inject constructor(
        context: Context,
        private val assetParser: AssetParser,
        private val remoteConfigHelper: RemoteConfigHelper
) {
    private val context: Context = context.applicationContext
    val conf: Conf? = assetParser.loadConf()

    fun getDrawableByName(drawableName: String): Drawable? {
        val idRes = getDrawableRes(drawableName)
        return if (idRes > 0) {
            ContextCompat.getDrawable(context, idRes)
        } else null
    }

    fun getDrawableById(resourceId: Int): Drawable? {
        return ContextCompat.getDrawable(context, resourceId)
    }

    @DrawableRes
    fun getDrawableRes(drawableName: String): Int {
        return getResId(drawableName, R.drawable::class.java)
    }

    private fun getResId(resName: String, c: Class<*>): Int {
        return try {
            val idField = c.getDeclaredField(resName)
            idField.getInt(idField)
        } catch (e: Exception) {
            Timber.e(e.localizedMessage)
            -1
        }
    }

    private fun supportedTransactionLocale(): Locale? {
        val supportedLocaleList =
                organization?.supportedLocales
        val deviceLocale = ConfigurationCompat.getLocales(context!!.resources.configuration).get(0)
        val defaultTransactionCode = deviceLocale.toLanguageTag()
        var transactionLocale: Locale? = null
        if (supportedLocaleList != null) {
            for (supportedLocale in supportedLocaleList) {
                if (supportedLocale.languageCode
                        .equals(deviceLocale.language, ignoreCase = true)
                    && supportedLocale.countryCode
                        .equals(deviceLocale.country, ignoreCase = true)
                ) {
                    transactionLocale = deviceLocale
                    break
                }
            }
        }
        if (transactionLocale == null) {
            transactionLocale = defaultSupportedLocale
        }
        if (transactionLocale == null) {
            transactionLocale = Locale.forLanguageTag(defaultTransactionCode)
        }
        return transactionLocale
    }

    private val defaultSupportedLocale: Locale?
        get() {
            var transactionLocale: Locale? = null
            if (organization?.supportedLocales != null) {
                for (supportedLocale in organization?.supportedLocales!!) {
                    val locale =
                        supportedLocale.languageCode + "-" + supportedLocale.countryCode
                    if (supportedLocale.isDefault) {
                        transactionLocale = Locale.forLanguageTag(locale)
                        break
                    }
                }
            }
            return transactionLocale
        }

    val supportedLocaleLanguageTag: String?
        get() {
            val supportedLocale = supportedTransactionLocale()
            return supportedLocale?.language + supportedLocale?.country
        }

    val supportedNavigationMenuItems: List<MenuItem>
        get() = assetParser.loadNavigationMenuItems(supportedLocaleLanguageTag) ?: ArrayList()


    fun getNavigationMenuScreenTitleFromTheConfig(action_screen: String?): String {
        val menuItemList =
            supportedNavigationMenuItems
        if (menuItemList.isNotEmpty()) {
            for (menuItem in menuItemList) {
                if (menuItem.action != null && menuItem.action?.screen != null
                    && menuItem.action?.screen
                        .equals(action_screen, ignoreCase = true)
                ) {
                    return menuItem.title
                }
            }
        }
        return ""
    }

    //default refresh rate for init QRCode
    val qrCodeRefreshRate: Long
        get() {
            var qRCodeValidityTimeInSeconds: Long = 150 //default refresh rate for init QRCode
            if (organization != null && organization?.passConfiguration != null &&
                organization?.passConfiguration?.barcodeValidation != null &&
                organization?.passConfiguration?.barcodeValidation?.refreshRate != null &&
                organization?.passConfiguration?.barcodeValidation?.refreshRate!! > 0
            ) {
                qRCodeValidityTimeInSeconds =
                    organization?.passConfiguration?.barcodeValidation?.refreshRate?.toLong()!!
            }
            return qRCodeValidityTimeInSeconds
        }

    fun getSettingsMenuScreenTitleFromTheConfig(
            isSignInList: Boolean,
            isCompareScreen: Boolean,
            action_screen: String?
    ): String {
        val menuGroupList: List<MenuGroup>? =
            if (isSignInList) supportedSettingsList.signedInMenuGroups else supportedSettingsList.signedOutMenuGroups
        if (menuGroupList != null && menuGroupList.isNotEmpty()) {
            for (menuGroup in menuGroupList) {
                val menuItemList =
                    menuGroup.menuItems
                if (menuItemList != null && menuItemList.size > 0) {
                    for (menuItem in menuItemList) {
                        if (menuItem.action != null) {
                            if (isCompareScreen && menuItem.action?.screen != null
                                && menuItem.action?.screen
                                    .equals(action_screen, ignoreCase = true)
                            ) {
                                return menuItem.title
                            } else if (menuItem.action?.type != null
                                && menuItem.action?.type
                                    .equals(action_screen, ignoreCase = true)
                            ) {
                                return menuItem.title
                            }
                        }
                    }
                }
            }
        }
        return ""
    }

    val scheduleItems: List<ScheduleItem>
        get() = assetParser.loadScheduleItems(supportedLocaleLanguageTag) ?: ArrayList()

    val gTFSURL: String?
        get() = assetParser.getGTFSURL(supportedLocaleLanguageTag)

    val fareMediumCardDetails: MoreInfoList
        get() = assetParser.loadDefaultFareMediumCardDetails(supportedLocaleLanguageTag) ?: MoreInfoList()

    val supportedSettingsList: SettingsList
        get() = assetParser.loadDefaultSettingsList(supportedLocaleLanguageTag) ?: SettingsList()

    val supportedMoreInfoList: MoreInfoList
        get() = assetParser.loadDefaultMoreInfoList(supportedLocaleLanguageTag) ?: MoreInfoList()

    val supportedFormlySignUpList: MutableList<Formly>
        get() = assetParser.loadDefaultSignUpFormList(supportedLocaleLanguageTag) as MutableList<Formly>

    val forgotPasswordForms: MutableList<Formly>
        get() = assetParser.loadDefaultForgotPasswordFormList(supportedLocaleLanguageTag) as MutableList<Formly>


    val voucherCodeForms: MutableList<Formly>
        get() = assetParser.loadDefaultVoucherCodeFormList(supportedLocaleLanguageTag) as MutableList<Formly>

    val changePasswordForms: MutableList<Formly>
        get() = assetParser.loadDefaultChangePasswordFormList(supportedLocaleLanguageTag) as MutableList<Formly>

    val nativeAppSupportForms: MutableList<Formly>
        get() = assetParser.loadNativeAppSupportFormList(supportedLocaleLanguageTag) as MutableList<Formly>

    val accountManagementForms: MutableList<Formly>
        get() = assetParser.loadDefaultAccountManagementFormList(supportedLocaleLanguageTag) as MutableList<Formly>

    fun getConfigurationPurchaseOptionsCurrencySymbol(listPrice: Int): String {
        return getConfigurationPurchaseOptionsCurrencySymbol(listPrice, false)
    }

    fun getConfigurationPurchaseOptionsCurrencySymbol(
            listPrice: Int,
            hideDecimalPlaces: Boolean
    ): String {
        val currencyCode =
            conf()?.configuration?.purchaseOptions?.currencyCode
        val currency = Currency.getInstance(currencyCode)
        val locale = supportedTransactionLocale()
        val currencyFormatter =
            NumberFormat.getCurrencyInstance(locale)
        if(currencyFormatter.maximumFractionDigits == 0) {
            // in some languages fraction count is 0 by default. Ex Vietnamese.
            // so round off is happening (ex. 1.60 becomes 2).
            // To prevent this setting the min and max fraction digits to 2.
            currencyFormatter.minimumFractionDigits = 2
            currencyFormatter.maximumFractionDigits = 2
        }
        currencyFormatter.currency = currency
        if (hideDecimalPlaces) {
            currencyFormatter.maximumFractionDigits = 0
        }
        val priceFraction = convertToFractionPriceForCurrency(listPrice, currency)
        return currencyFormatter.format(priceFraction)
    }

    fun getConfigurationPurchaseOptionsCurrency(listPrice: Int): Double {
        val currencyCode =
            conf()?.configuration?.purchaseOptions?.currencyCode
        val currency = Currency.getInstance(currencyCode)
        return convertToFractionPriceForCurrency(listPrice, currency)
    }

    private fun convertToFractionPriceForCurrency(
            convertPenny: Int,
            currency: Currency
    ): Double {
        var fractionPrice = BigDecimal(convertPenny)
        val fractionDigits = currency.defaultFractionDigits
        fractionPrice = fractionPrice.movePointLeft(fractionDigits)
        return fractionPrice.toDouble()
    }

    val isAuthenticationNative: Boolean
        get() {
            val authenticationMethod =
                conf()?.clients?.customerMobile?.authenticationMethod
            return !TextUtils.isEmpty(authenticationMethod) && authenticationMethod.equals(
                    "native",
                    ignoreCase = true
            )
        }

    val isSignInLogoVisible: Boolean
        get() = conf()?.clients?.customerMobile?.shouldShowLogoInSignin ?: false

    val accentThemeBacgroundColor: Int
        get() {
            val color = themes?.accentTheme?.backgroundColor
            return parseColor(color)
        }

    val accentThemePrimaryTextColor: Int
        get() {
            val color = themes?.accentTheme?.primaryColor
            return parseColor(color)
        }

    val accentThemeSecondaryTextColor: Int
        get() {
            val color = themes?.accentTheme?.secondaryColor
            return parseColor(color)
        }

    val accentThemeAccentColor: Int
        get() {
            val color = themes?.accentTheme?.accentColor
            return parseColor(color)
        }

    val dataThemeBackgroundColor: Int
        get() {
            val color = themes?.dataTheme?.backgroundColor
            return parseColor(color)
        }

    val dataThemePrimaryTextColor: Int
        get() {
            val color = themes?.dataTheme?.primaryColor
            return parseColor(color)
        }

    val dataThemeAccentColor: Int
        get() {
            val color = themes?.dataTheme?.accentColor
            return parseColor(color)
        }

    val backgroundThemeBackgroundColor: Int
        get() {
            val color = themes?.backgroundTheme?.backgroundColor
            return parseColor(color)
        }

    val backgroundThemePrimaryTextColor: Int
        get() {
            val color = themes?.backgroundTheme?.primaryColor
            return parseColor(color)
        }

    val backgroundThemeSecondaryTextColor: Int
        get() {
            val color = themes?.backgroundTheme?.secondaryColor
            return parseColor(color)
        }

    val backgroundThemeAccentColor: Int
        get() {
            val color = themes?.backgroundTheme?.accentColor
            return parseColor(color)
        }

    val headerThemeBackgroundColor: Int
        get() {
            val color = themes?.headerTheme?.backgroundColor
            return parseColor(color)
        }

    val collectionThemeBackgroundColor: Int
        get() {
            val color = themes?.collectionTheme?.backgroundColor
            return parseColor(color)
        }

    val collectionThemePrimaryTextColor: Int
        get() {
            val color = themes?.collectionTheme?.primaryColor
            return parseColor(color)
        }

    val collectionThemeSecondaryTextColor: Int
        get() {
            val color = themes?.collectionTheme?.secondaryColor
            return parseColor(color)
        }

    val collectionThemeAccentColor: Int
        get() {
            val color = themes?.collectionTheme?.accentColor
            return parseColor(color)
        }

    val navigationThemeBackgroundColor: Int
        get() {
            val color =
                organization?.branding?.themes?.navigationTheme?.backgroundColor
            return parseColor(color)
        }

    val navigationThemePrimaryTextColor: Int
        get() {
            val color =
                organization?.branding?.themes?.navigationTheme?.primaryColor
            return parseColor(color)
        }

    val navigationThemeAccentColor: Int
        get() {
            val color =
                organization?.branding?.themes?.navigationTheme?.accentColor
            return parseColor(color)
        }

    val statusBarColor: Int
        get() = colorPrimaryToColorPrimaryDark(headerThemeBackgroundColor)

    private fun colorPrimaryToColorPrimaryDark(headerThemeBackgroundColor: Int): Int {
        val hsv = FloatArray(3)
        Color.colorToHSV(headerThemeBackgroundColor, hsv)
        hsv[2] *= 0.7f
        return Color.HSVToColor(hsv)
    }

    val headerThemePrimaryTextColor: Int
        get() {
            val color = themes?.headerTheme?.primaryColor
            return parseColor(color)
        }

    val headerThemeSecondaryTextColor: Int
        get() {
            val color = themes?.headerTheme?.secondaryColor
            return parseColor(color)
        }

    val headerThemeAccentColor: Int
        get() {
            val color = themes?.headerTheme?.accentColor
            return parseColor(color)
        }

    val indicatorsSuccess: Int
        get() {
            val color = branding?.indicators?.success
            return parseColor(color)
        }

    private val branding: Branding?
        get() = organization?.branding

    val indicatorsError: Int
        get() {
            val color = branding?.indicators?.error
            return parseColor(color)
        }

    val configurationPurchaseOptionsCurrencyCode: String?
        get() = conf()?.configuration?.purchaseOptions?.currencyCode

    val organizationPassRowConfigs: List<PassViewRowConfiguration>?
        get() = organization?.passConfiguration?.passViewRowConfiguration

    val organizationFareMediumConfigs: List<PassViewRowConfiguration>?
        get() = organization?.passConfiguration?.fareMediumRowConfiguration

    val dateDisplayFormat: SimpleDateFormat
        get() {
            val supportedLocaleList =
                organization?.supportedLocales
            val deviceLocale = Locale.getDefault()
            var dateFormat: String? = null
            var defaultDateFormat: String? = null
            var transactionLocale: Locale? = null
            var defaultTransactionCode: String? = null
            if (supportedLocaleList != null) {
                for (supportedLocale in supportedLocaleList) {
                    val locale =
                        supportedLocale.languageCode + "_" + supportedLocale.countryCode
                    if (locale == deviceLocale.toString()) {
                        dateFormat = supportedLocale.dateDisplayFormat
                        transactionLocale = deviceLocale
                    } else if (supportedLocale.isDefault) {
                        defaultDateFormat = supportedLocale.dateDisplayFormat
                        defaultTransactionCode = supportedLocale.languageCode
                    }
                }
            }
            if (dateFormat == null) {
                dateFormat = defaultDateFormat
            }
            if (transactionLocale == null && defaultTransactionCode != null) {
                transactionLocale = Locale.forLanguageTag(defaultTransactionCode)
            }
            return if (dateFormat != null && transactionLocale != null) {
                SimpleDateFormat(dateFormat, transactionLocale)
            } else throw IllegalStateException("Date format not found.")
        }

    val timeDisplayFormat: SimpleDateFormat
        get() {
            val supportedLocaleList =
                organization?.supportedLocales
            val deviceLocale = Locale.getDefault()
            var timeFormat: String? = null
            var defaultTimeFormat: String? = null
            var transactionLocale: Locale? = null
            var defaultTransactionCode: String? = null
            if (supportedLocaleList != null) {
                for (supportedLocale in supportedLocaleList) {
                    val locale =
                        supportedLocale.languageCode + "_" + supportedLocale.countryCode
                    if (locale == deviceLocale.toString()) {
                        timeFormat = supportedLocale.timeDisplayFormat
                        transactionLocale = deviceLocale
                    } else if (supportedLocale.isDefault) {
                        defaultTimeFormat = supportedLocale.timeDisplayFormat
                        defaultTransactionCode = supportedLocale.languageCode
                    }
                }
            }
            if (timeFormat == null) {
                timeFormat = defaultTimeFormat
            }
            if (transactionLocale == null && defaultTransactionCode != null) {
                transactionLocale = Locale.forLanguageTag(defaultTransactionCode)
            }
            return if (timeFormat != null && transactionLocale != null) {
                SimpleDateFormat(timeFormat, transactionLocale)
            } else throw IllegalStateException("Date format not found.")
        }

    fun getFormattedDateAndTime(timeMs: Long): String? {
        val dateDisplayFormat: SimpleDateFormat? = dateDisplayFormat
        val timeDisplayFormat: SimpleDateFormat? = timeDisplayFormat
        return if (timeMs != 0L && dateDisplayFormat != null && timeDisplayFormat != null) {
            val date = Date(timeMs)
            (dateDisplayFormat.format(date)
                    + ", " +
                    timeDisplayFormat.format(date))
        } else null
    }

    val organizationUuid: String?
        get() = organization?.uuid?.replace("-".toRegex(), "_")

    @Throws(IllegalArgumentException::class)
    fun parseColor(color: String?): Int {
        return Color.parseColor(color)
    }

    val upArrowDrawable: Drawable?
        get() {
            val drawable =
                ContextCompat.getDrawable(context, R.drawable.ic_arrow_back_icon)
            drawable?.setTintList(ColorStateList.valueOf(headerThemePrimaryTextColor))
            return drawable
        }

    val isPhotoValidationEnabled: Boolean
        get() = organization?.passConfiguration?.photoValidation?.photoValidationEnabled ?: false

    val isNotificationEnabled: Boolean
        get() = if (conf()?.configuration?.notificationEnabled != null) {
            conf()?.configuration?.notificationEnabled ?: false
        } else {
            true
        }

    private val themes: Themes?
        get() = branding?.themes

    val organization: Organization?
        get() = conf()?.organization

    val domain: Domain?
        get() = conf()?.domain

    private fun conf(): Conf? {
        return getConf()
    }

    val childOrganizations: List<ChildOrganization>?
        get() = organization?.childOrganizations

    val barcodeValidation: BarcodeValidation?
        get() = conf()?.organization?.passConfiguration?.barcodeValidation

    fun clientId(): String? {
        return try {
            conf()?.clients?.customerMobile?.client?.clientId
        } catch (e: Exception) {
            null
        }
    }

    val isNativeLogin: Boolean
        get() = try {
            conf()?.clients?.customerMobile?.authenticationMethod.equals("native", ignoreCase = true)
        } catch (e: Exception) {
            false
        }

    fun getOrganizationHeaderThemeColor(uuid: String): Int {
        val childOrgList =
            conf()?.organization?.childOrganizations
        if(childOrgList != null) {
            for (childOrg in childOrgList) {
                if (childOrg.uuid.contentEquals(uuid)) {
                    val color =
                        childOrg.branding.themes.headerTheme.backgroundColor
                    if (color != null && color.isNotEmpty()) {
                        return parseColor(color)
                    }
                }
            }
        }
        return headerThemeBackgroundColor
    }

    val supportedPaymentTypes: List<SupportedPaymentType>?
        get() = conf()?.configuration?.purchaseOptions?.supportedPaymentTypes

    val isGooglePayEnabled: Boolean
        get() {
            val supportedPaymentTypes: List<SupportedPaymentType>? =
                supportedPaymentTypes
            if (supportedPaymentTypes == null || supportedPaymentTypes.isEmpty()) {
                return false
            }
            if (supportedPaymentTypes.isNotEmpty()) {
                for (paymentType in supportedPaymentTypes) {
                    if (paymentType.type == "GooglePay") {
                        return true
                    }
                }
            }
            return false
        }

    val isUsingOldOrdersEndPoint: Boolean
        get() = conf()?.configuration?.purchaseOptions?.usingOldOrderEndpoint ?: false

    val googlePayConfiguration: GooglePayConfiguration?
        get() = conf()?.configuration?.purchaseOptions?.googlePayConfiguration

    val isAdditionalCCFieldsRequired: Boolean
        get() = conf()?.configuration?.purchaseOptions?.isAdditionalCCFieldsRequired ?: false

    val maxSplitPayments: Int
        get() = conf()?.configuration?.purchaseOptions?.maxSplitPayment ?: 1

    fun savePassToDevice(): Boolean {
        return conf()?.configuration?.purchaseOptions?.saveToDevice ?: false
    }

    val isPromotionalCodesAllowed: Boolean
        get() {
            val promotionalCodeAllowed =
                conf()?.configuration?.purchaseOptions?.allowsPromotionalCodes
            return promotionalCodeAllowed ?: false
        }

    fun getChildOrganizationName(Uuid: String): String? {
        var organizationName: String? = null
        if(conf()?.organization?.childOrganizations != null && conf()?.organization?.childOrganizations?.isNotEmpty()!!) {
            for (i in conf()?.organization?.childOrganizations?.indices!!) {
                if (conf()?.organization?.childOrganizations!![i].uuid == Uuid) {
                    organizationName =
                        conf()?.organization?.childOrganizations!![i].displayName
                }
            }
        }
        if (organizationName == null) {
            organizationName = organizationDisplayName
        }
        return organizationName
    }

    private val organizationDisplayName: String?
        get() = conf()?.organization?.displayName

    val isV2PaymentMethodsEnabled: Boolean
        get() = conf()?.configuration?.purchaseOptions?.v2PaymentMethodEnabled ?: false

    fun isMultiPassesActivationEnabled(orgUuid: String?): Boolean {
        if(conf()?.organization?.childOrganizations != null && conf()?.organization?.childOrganizations?.isNotEmpty()!!) {
            for (i in conf()?.organization?.childOrganizations?.indices!!) {
                val childOrganization =
                    conf()?.organization?.childOrganizations?.get(i)
                if (childOrganization?.uuid == orgUuid) {
                    if (childOrganization?.multiPassesActivation != null) return childOrganization.multiPassesActivation!!
                }
            }
        }
        return conf()?.organization?.multiPassesActivation ?: false
    }

    fun getOrderOf(paymentMethod: PaymentsOrderName): Int {
        val paymentMethodsOrder: HashMap<String, Int>? =
            conf()?.configuration?.purchaseOptions?.paymentMethodsOrder
        if(paymentMethodsOrder != null) {
            return if (paymentMethodsOrder?.containsKey(paymentMethod.name)!!) {
                paymentMethodsOrder[paymentMethod.name]!!
            } else 1000
        }
        return 1000
        // returning 1000 as it will be higher than other payment methods
    }

    fun isDisplayLongNameForOd(uuid: String?): Boolean {
        if(uuid != null && conf()?.organization?.childOrganizations != null && conf()?.organization?.childOrganizations?.isNotEmpty()!!) {
            for (i in conf()?.organization?.childOrganizations?.indices!!) {
                if (conf()?.organization?.childOrganizations!![i].uuid == uuid) {
                    if (conf()?.organization?.childOrganizations!![i]
                            .isDisplayLongNameForOd != null
                    ) {
                        return conf()?.organization?.childOrganizations!![i]
                            .isDisplayLongNameForOd!!
                    }
                }
            }
        }
        return if (conf()?.organization?.isDisplayLongNameForOd != null) {
            conf()?.organization?.isDisplayLongNameForOd ?: false
        } else {
            false
        }
    }

    val isSecurityQuestionsEnabled: Boolean
        get() = conf()?.configuration?.securityQuestion != null &&
                conf()?.configuration?.securityQuestion?.isEnabled ?: false

    fun blockAccessWithoutSecurityQuestions(): Boolean {
        return isSecurityQuestionsEnabled && conf()?.configuration?.securityQuestion?.blockAppAccess ?: false
    }

    val isNewStoreScreen: Boolean
        get() = conf()?.clients?.customerMobile?.isNewStoreScreen ?: false

    val voucherCodeLength: Int
        get() = conf()?.configuration?.purchaseOptions?.voucherCodeLength ?: 0

    val isEnableTransferPassInUseTicket: Boolean
        get() = conf()?.organization?.enableTransferPassInUseTicket ?: true

    val isTransferTimeEnabled: Boolean
        get() {
            if(conf()?.organization?.passConfiguration?.fareMediumRowConfiguration != null &&
                conf()?.organization?.passConfiguration?.fareMediumRowConfiguration?.isNotEmpty()!!)
            for (passViewRowConfiguration in conf()?.organization?.passConfiguration?.fareMediumRowConfiguration!!) {
                if (passViewRowConfiguration.singleItemRowType != null && passViewRowConfiguration.singleItemRowType
                        .item == AppConstants.TRANSFER_TIME
                ) {
                    return true
                }
            }
            return false
        }

    val pinLength: Int
        get() = remoteConfigHelper.getPinLength()

    val isTransferVirtualCardEnabled: Boolean
        get() = remoteConfigHelper.isTransferVirtualCardEnabled()

    val countryCode: String
        get() = remoteConfigHelper.getCountryCode()

    val zipCode: String
        get() = remoteConfigHelper.getZipCode()

    val incommMinValue: Int
        get() = remoteConfigHelper.getIncommMinValue()

    val incommMaxValue: Int
        get() = remoteConfigHelper.getIncommMaxValue()

    val isCreditCardAlertMessageEnabled: Boolean
        get() = remoteConfigHelper.isCreditCardAlertMessageEnabled()

    val isPaymentMandatory: Boolean
        get() = remoteConfigHelper.isPaymentMandatoryForZeroTotal()

    val isNMIEnabled: Boolean
        get() = remoteConfigHelper.isNMIEnabled()

    val isGooglePayEnabledOnStoreScreen: Boolean
        get() = remoteConfigHelper.isGooglePayEnabledOnStoreScreen()

    val isAppRatingDisabled: Boolean
        get() = remoteConfigHelper.isAppRatingDisabled()

    fun getServiceLevelUUID(key: String?): String? {
        return key?.let { remoteConfigHelper.getServiceLevelUUID(it) }
    }

    fun getConfigurationPurchaseOptionsEnglishCurrencySymbol(listPrice: Int): String? {
        val currencyCode =
            conf()!!.configuration?.purchaseOptions?.currencyCode
        val currency = Currency.getInstance(currencyCode)
        val locale = Locale.ENGLISH
        val currencyFormatter =
            NumberFormat.getCurrencyInstance(locale)
        currencyFormatter.currency = currency
        val priceFraction = convertToFractionPriceForCurrency(listPrice, currency)
        return currencyFormatter.format(priceFraction)
    }

    fun isElertSdkEnabled(): Boolean {
        return conf()?.organization?.isElertSdkEnabled ?: false
    }

    fun isElertSdkEnabledOnTicketsScreen(): Boolean {
        return conf()?.organization?.isElertSdkEnabledOnTicketsScreen ?: false
    }

    fun isFaremediaApp(): Boolean {
        return conf()?.organization?.passConfiguration?.passViewRowConfiguration == null
                || conf()?.organization?.passConfiguration?.passViewRowConfiguration?.isEmpty() == true
    }

    fun setAppRatingConfig() {
        if(null == conf()?.organization?.appRatingConfiguration) {
            conf()?.organization?.appRatingConfiguration = AppRatingConfig(
                3,
                2,
                43200,
                525600,
                "support@bytemark.co",
                "https://play.google.com/store/apps/details?id=${context.packageName}"
            )
        }
    }

    fun isInfoIconEnabledFor(screen: String): Boolean =
            conf?.clients?.customerMobile?.infoIconScreenList?.contains(screen) ?: false

    fun getMaxTransferCount() = remoteConfigHelper.getMaxTransferCount()

    fun getTransferCountResetPeriod() = remoteConfigHelper.getTransferCountResetPeriod()

    fun isTransferRestrictionEnabled() = remoteConfigHelper.isTransferRestrictionEnabled()

}