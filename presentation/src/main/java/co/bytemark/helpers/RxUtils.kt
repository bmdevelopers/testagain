package co.bytemark.helpers

import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class RxUtils {
    /**
     * @param <T> The observable to apply schedulers for
     * @return Applied observable
    </T> */
    fun <T> applySchedulers(): Observable.Transformer<T, T> {
        return Observable.Transformer { observable: Observable<T> ->
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }
    }

    companion object {
        @JvmStatic
        val instance: RxUtils
            get() = RxUtils()
    }
}