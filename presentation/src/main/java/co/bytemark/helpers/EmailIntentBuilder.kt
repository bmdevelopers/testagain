package co.bytemark.helpers

import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_SENDTO
import android.net.Uri
import android.os.Build
import androidx.core.content.ContextCompat.startActivity
import co.bytemark.BuildConfig
import co.bytemark.CustomerMobileApp
import co.bytemark.sdk.BytemarkSDK
import java.util.*
import javax.inject.Inject

class EmailIntentBuilder(val context: Context) {

    private var to = ""
    private var subject = ""
    private var body = ""
    private var emailIntent: Intent? = null

    @Inject
    lateinit var confHelper: ConfHelper

    init {
        CustomerMobileApp.component.inject(this)
        init()
    }

    private fun init() {
        subject = defaultSubject()
        body = defaultBody()
    }

    fun from(): EmailIntentBuilder {
        return EmailIntentBuilder(context)
    }

    fun to(to: String) = returnWithInstance { this.to = to }

    fun subject(subject: String) =
        returnWithInstance { this.subject = subject + "- ${currentDate()}" }

    fun body(body: String) = returnWithInstance { this.body = body }

    fun start(): Boolean {
        emailIntent?.let {
            try {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(context, it, null)
            } catch (e: Exception) {
                return false
            }
        } ?: kotlin.run {
            return false
        }
        return true
    }

    fun build() = returnWithInstance {
        emailIntent = Intent(ACTION_SENDTO).apply {
            type = "plain/text"
            data = Uri.parse("mailto:")
            putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
            putExtra(Intent.EXTRA_SUBJECT, subject)
            putExtra(Intent.EXTRA_TEXT, body)
        }
    }

    private fun returnWithInstance(func: () -> Unit): EmailIntentBuilder {
        func.invoke()
        return this
    }

    private fun currentDate() =
        confHelper.dateDisplayFormat.format(Calendar.getInstance().time)

    private fun defaultBody() = StringBuilder().apply {
        appendln("\n") //Extra space

        if (BytemarkSDK.isLoggedIn()) {
            BytemarkSDK.SDKUtility.getUserInfo()?.let {
                appendln("${it.firstName} ${it.lastName}");appendln(it.email)
            }
        }

        appendln("App Name: ${confHelper.organization?.displayName}")
        appendln("App Version : ${BuildConfig.VERSION_NAME} ${BuildConfig.VERSION_CODE}")
        append("OS Version : ${Build.VERSION.RELEASE}")

    }.toString()

    private fun defaultSubject() = "Android Support - ${currentDate()}"
}