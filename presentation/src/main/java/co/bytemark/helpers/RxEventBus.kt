package co.bytemark.helpers

import rx.Observable
import rx.subjects.PublishSubject
import rx.subjects.Subject
import javax.inject.Inject
import javax.inject.Singleton

/**
 * A simple event bus implemented using RxJava.
 */
@Singleton
class RxEventBus @Inject constructor() {
    // Our bus to post events to. Should be serialized for thread safety.
    private val publishSubject: Subject<Any, Any>
    fun postEvent(event: Any) {
        publishSubject.onNext(event)
    }

    /**
     * Observable that will emmit everything posted to the event bus.
     */
    private fun observeEvents(): Observable<Any> {
        return publishSubject
    }

    /**
     * Observable that only emits events of a specific class.
     * Use this if you only want to subscribe to one type of event.
     */
    fun <T> observeEvents(eventClass: Class<T>?): Observable<T> {
        return observeEvents().ofType(eventClass)
    }

    init {
        publishSubject = PublishSubject.create<Any>().toSerialized()
    }
}