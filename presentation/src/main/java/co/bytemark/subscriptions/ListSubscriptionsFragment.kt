package co.bytemark.subscriptions


import androidx.lifecycle.Observer
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent.TYPE_VIEW_FOCUSED
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.Result
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.model.subscriptions.Subscriptions
import co.bytemark.use_tickets.UseTicketsAccessibility
import co.bytemark.widgets.util.ConnectivityLiveData
import co.bytemark.widgets.util.createViewModel
import co.bytemark.widgets.util.isOnline
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.StackingBehavior
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.fragment_subcriptions.*
import me.grantland.widget.AutofitHelper


/**
 * Created by vally on 07/06/19.
 */
class ListSubscriptionsFragment : BaseMvvmFragment() {


    private lateinit var viewModel: SubscriptionsViewModel


    private var subscriptionList: MutableList<Subscriptions> = mutableListOf()

    private lateinit var subscriptionsAdapter: SubscriptionsAdapter


    private lateinit var connectivityLiveData: ConnectivityLiveData
    private var fareMediaId: String? = null
    private var materialDialog: MaterialDialog? = null

    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.fragment_subcriptions, container)!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fareMediaId = activity?.intent?.getStringExtra(AppConstants.FARE_MEDIA_ID)
        viewModel = createViewModel { CustomerMobileApp.appComponent.subscriptionsViewModel }
        connectivityLiveData = ConnectivityLiveData(activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
        connectivityLiveData.observe(this, Observer<Boolean> { isOnline ->
            if (isOnline!!) {
                loadSubscriptions()
            } else {
                showError()
            }
        })

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscriptionsAdapter = SubscriptionsAdapter(confHelper, subscriptionList, getCancelButtonText()) {
            if(!TextUtils.isEmpty(fareMediaId))
                showAutoRenewalDialog(it)
            else
                unSubscribe(it)
        }

        subscriptionsRecyclerView.adapter = subscriptionsAdapter
        loadSubscriptions()

        swipeRefreshLayout.setOnRefreshListener({ this.loadSubscriptions() })


    }

    private fun loadSubscriptions() {
        emptyStateLayout.showLoading(R.drawable.ic_subscription_ticket_filled,
                (
                        if(!TextUtils.isEmpty(fareMediaId))
                            getString(R.string.pass_auto_renewals_removing_loading)
                        else
                            getString(R.string.popup_getting).plus(" ").plus((activity as ListSubscriptionsActivity).title)

                        ).plus("..."))

        swipeRefreshLayout.isRefreshing = true

        viewModel.getSubscriptions(fareMediaId).observe(this, Observer { result ->
            result?.let {
                when (it) {

                    is Result.Success -> {
                        swipeRefreshLayout.isRefreshing = false
                        val data = it.data ?: return@let
                        if (data.subscriptions.isNullOrEmpty()) {
                            showEmpty()
                        } else {
                            emptyStateLayout.showContent()
                            subscriptionsAdapter = SubscriptionsAdapter(confHelper, data.subscriptions, getCancelButtonText()) { subscriptions ->
                                if(!TextUtils.isEmpty(fareMediaId))
                                    showAutoRenewalDialog(subscriptions)
                                else
                                    unSubscribe(subscriptions)
                            }
                            subscriptionsRecyclerView.adapter = subscriptionsAdapter
                        }
                    }

                    is Result.Failure -> {
                        if (!context?.isOnline()!!) {
                            showError()
                        } else {
                            handleError(it.bmError[0])
                        }
                        swipeRefreshLayout.isRefreshing = false
                    }
                }
            }
        })
    }

    private fun unSubscribe(subscriptions: Subscriptions) {
        if (!context?.isOnline()!!) {
            showError()
            return
        }

        emptyStateLayout.showLoading(R.drawable.ic_subscription_ticket_filled,
                (getString(
                        if(TextUtils.isEmpty(fareMediaId))
                            R.string.subscription_unsubscribing
                        else
                            R.string.pass_auto_renewals_removing_loading
                ).plus("...")))

        val jsonBody = JsonObject()
        jsonBody.addProperty(AppConstants.CREATE_SUBSCRIPTION, false)
        if(!TextUtils.isEmpty(fareMediaId)) jsonBody.addProperty(AppConstants.SUBSCRIPTION_FARE_MEDIA_ID, fareMediaId)

        viewModel.cancelSubscriptions(subscriptions.subscriptionUuid, jsonBody).observe(this, Observer { result ->
            result.let {
                when (it) {

                    is Result.Success -> {
                        subscriptionsAdapter.subscriptionList.remove(subscriptions)
                        subscriptionsAdapter.notifyDataSetChanged()
                        if (subscriptionsAdapter.subscriptionList.size > 0){
                            emptyStateLayout.showContent()
                        }else{
                            showEmpty()
                        }
                    }

                    is Result.Failure -> {

                        handleError(it.bmError[0])
                    }
                }
            }
        })

    }

    private fun showEmpty() {
        emptyStateLayout.showEmpty(R.drawable.ic_subscription_ticket_filled,
                if(TextUtils.isEmpty(fareMediaId))
                    R.string.subscriptions_popup_no_data
                else
                    R.string.pass_auto_renewals_no_data)
        setAccessibilityForEmptyView()
    }

    private fun showError() {
        if(!TextUtils.isEmpty(fareMediaId))
            emptyStateLayout.showError(R.drawable.error_material, R.string.network_connectivity_error,
                    R.string.network_connectivity_error_message, R.string.autoload_retry) {
                loadSubscriptions()
            }
        else
            emptyStateLayout.showError(R.drawable.error_material, R.string.network_connectivity_error, R.string.network_connectivity_error_message)
    }

    private fun setAccessibilityForEmptyView() {
        emptyStateLayout?.postDelayed({
            if(activity != null) {
                emptyStateLayout?.isFocusable = true
                emptyStateLayout?.sendAccessibilityEvent(TYPE_VIEW_FOCUSED)
            }
        }, 5000)
    }

    override fun showDefaultErrorDialog(title: String?, errorMsg: String?, finishActivity: Boolean) {
        super.showDefaultErrorDialog(title, errorMsg, subscriptionList.size == 0)
    }

    private fun getCancelButtonText(): String {
        return if(!TextUtils.isEmpty(fareMediaId))
            getString(R.string.shopping_cart_remove).toUpperCase()
        else
            getString(R.string.subscription_unsubscribe)
    }

    private fun showAutoRenewalDialog(subscriptions: Subscriptions) {
        materialDialog = MaterialDialog.Builder(activity!!)
                .title(R.string.pass_auto_renewals_cancel_popup_title)
                .stackingBehavior(StackingBehavior.ALWAYS)
                .content(R.string.pass_auto_renewals_cancel_popup_content)
                .positiveText(getString(R.string.pass_auto_renewals_cancel_popup_button))
                .negativeText(getString(R.string.popup_cancel).toUpperCase())
                .positiveColor(confHelper.dataThemeAccentColor)
                .negativeColor(confHelper.dataThemeAccentColor)
                .callback(object : MaterialDialog.ButtonCallback() {
                    override fun onPositive(dialog: MaterialDialog) {
                        super.onPositive(dialog)
                        unSubscribe(subscriptions)
                    }

                    override fun onNegative(dialog: MaterialDialog) {
                        super.onNegative(dialog)
                        dialog.dismiss()
                    }
                }).build()
        try {
            AutofitHelper.create(materialDialog?.getActionButton(DialogAction.POSITIVE))
        }catch (e : Exception){
           e.stackTrace
        }

        materialDialog?.show()
        materialDialog?.contentView?.let {
            UseTicketsAccessibility.announceAccessibilityWithoutDoubleTapHint(it)
        }
    }

    override fun onOffline() {
        super.onOffline()
        if(materialDialog?.isShowing == true)
            materialDialog?.dismiss()
    }
}


