package co.bytemark.subscriptions

import androidx.lifecycle.ViewModel
import co.bytemark.domain.interactor.subscriptions.CancelSubscriptionsUseCase
import co.bytemark.domain.interactor.subscriptions.CancelSubscriptionsUseCaseValue
import co.bytemark.domain.interactor.subscriptions.SubscriptionsUseCase
import co.bytemark.domain.interactor.subscriptions.SubscriptionsUseCaseValue
import com.google.gson.JsonObject
import javax.inject.Inject

/**
 * Created by vally on 07/06/19.
 */
class SubscriptionsViewModel @Inject constructor(private val subscriptionsUseCase: SubscriptionsUseCase,private val cancelSubscriptionsUseCase: CancelSubscriptionsUseCase) : ViewModel() {

    fun getSubscriptions(fareMediaId: String?) = subscriptionsUseCase.getLiveData(SubscriptionsUseCaseValue(fareMediaId))

    fun cancelSubscriptions(subscriptionUuid: String, jsonObject: JsonObject) = cancelSubscriptionsUseCase.getLiveData(CancelSubscriptionsUseCaseValue(subscriptionUuid, jsonObject))

}