package co.bytemark.subscriptions


import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.model.common.Action

/**
 * Created by vally on 07/06/19.
 */
class ListSubscriptionsActivity: MasterActivity(){

    var title: String? = null

    override fun getLayoutRes()= R.layout.activity_subscriptions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setNavigationViewCheckedItem(Action.SUBSCRIPTION)
        title = intent?.extras?.getString(AppConstants.TITLE) ?: ""
        setToolbarTitle(title,true)
    }
    override fun useHamburgerMenu(): Boolean {
        return false
    }

}