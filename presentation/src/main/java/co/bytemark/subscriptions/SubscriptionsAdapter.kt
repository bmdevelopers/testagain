package co.bytemark.subscriptions


import androidx.recyclerview.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.R
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.model.subscriptions.Subscriptions
import co.bytemark.use_tickets.UseTicketsAccessibility
import co.bytemark.widgets.util.hide
import co.bytemark.widgets.util.show
import kotlinx.android.synthetic.main.subscriptions_row_item.view.*
import java.lang.StringBuilder

/**
 * Created by vally on 09/06/19.
 */
class SubscriptionsAdapter(private val confHelper: ConfHelper,
                           val subscriptionList: MutableList<Subscriptions>, private val cancelButtonText: String,
                           private val unSubscribeListener: (Subscriptions) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return LayoutInflater.from(parent.context)
                .inflate(R.layout.subscriptions_row_item, parent, false)
                .run { SubscriptionViewHolder(this) }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is SubscriptionsAdapter.SubscriptionViewHolder -> holder.bind(subscriptionList.get(position))


        }
    }

    inner class SubscriptionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(subscriptions: Subscriptions) {
            itemView.apply {
                nameTextView.text = subscriptions.passName
                subscriptionStatusInfo.hide()
                negativeActionButton.text = cancelButtonText
                val stringBuilder = StringBuilder()
                if (!TextUtils.isEmpty(subscriptions.status) && (subscriptions.status.equals(AppConstants.SUBSCRIPTION_FAILED,true) ||
                                subscriptions.status.equals(AppConstants.SUBSCRIPTION_SUSPENDED,true))){
                    priceTextView.hide()
                    subscriptionErrorStatus.show()
                    if (subscriptions.status.equals(AppConstants.SUBSCRIPTION_FAILED, true))
                        subscriptionErrorStatus.text = context.getString(R.string.subscriptions_invalid_status)
                    else
                        subscriptionErrorStatus.text = context.getString(R.string.pass_auto_renewals_suspended_status)

                    if(subscriptions.errors?.isNotEmpty() == true) {

                        subscriptions.errors?.forEach {
                            if(!TextUtils.isEmpty(it.message)) {
                                stringBuilder.append("\n\n")
                                stringBuilder.append(it.message)
                            }
                        }
                        subscriptionStatusInfo.show()
                        subscriptionStatusInfo.text = stringBuilder.trim().toString()
                    }
                } else {
                    priceTextView.show()
                    priceTextView.text = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(subscriptions.price).toString()
                    subscriptionErrorStatus.hide()
                    subscriptions.executionTrigger?.let { executionTrigger ->
                        executionTrigger.timeRemainingUnit?.let { unit ->
                            executionTrigger.timeRemainingValue?.let {value ->
                                stringBuilder.append(context.getString(R.string.subscriptions_renews_when)).append(" ").append(value)
                                        .append(" ").append(unit).append("(s) ").append(context.getString(R.string.subscription_remain))
                            }

                        } ?: kotlin.run {
                            executionTrigger.usesRemaining?.let {
                                stringBuilder.append(context.getString(R.string.subscriptions_renews_when)).append(" ")
                                        .append(it).append(" ").append(context.getString(R.string.subscription_uses_remain))
                            }
                        }
                    }

                    subscriptions.notificationTrigger?.let { notificationTrigger ->
                        notificationTrigger.timeRemainingUnit?.let { unit ->
                            notificationTrigger.timeRemainingValue?.let { value ->
                                if(!TextUtils.isEmpty(stringBuilder)) stringBuilder.append("\n")
                                stringBuilder.append(context.getString(R.string.subscription_notification))
                                        .append(" ").append(value).append(" ").append(unit).append("(s) ")
                                        .append(context.getString(R.string.subscription_remain))
                            }
                        } ?: kotlin.run {
                            notificationTrigger.usesRemaining?.let {
                                if(!TextUtils.isEmpty(stringBuilder)) stringBuilder.append("\n")
                                stringBuilder.append(context.getString(R.string.subscription_notification)).append(" ").append(it)
                                        .append(" ").append(context.getString(R.string.subscription_uses_remain))
                            }
                        }

                    }

                    if (!TextUtils.isEmpty(stringBuilder)){
                        subscriptionStatusInfo.show()
                        subscriptionStatusInfo.text = stringBuilder.toString()
                    }
                }

                negativeActionButton.setOnClickListener {
                    unSubscribeListener(subscriptions)
                }

                var builder = StringBuilder()
                builder.append(nameTextView.text.toString()).append("\n ")
                if(subscriptionErrorStatus.visibility == View.VISIBLE) {
                    builder.append(subscriptionErrorStatus.text.toString()).append("\n ")
                } else {
                    builder.append(priceTextView.text.toString()).append("\n")
                }
                builder.append(subscriptionStatusInfo.text.toString())
                subscriptionLayout.contentDescription = builder.toString()
                UseTicketsAccessibility.announceAccessibilityWithoutDoubleTapHint(subscriptionLayout)
            }


        }
    }


    override fun getItemCount(): Int {
        return subscriptionList.size
    }

}



