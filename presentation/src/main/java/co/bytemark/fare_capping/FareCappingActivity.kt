package co.bytemark.fare_capping

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.ToolbarActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.util.replaceFragment

class FareCappingActivity : ToolbarActivity() {

    override fun getLayoutRes() = R.layout.activity_fare_capping

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(intent.extras?.getString(AppConstants.TITLE))
        replaceFragment(FareCappingFragment(), R.id.container)
    }

}