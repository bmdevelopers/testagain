package co.bytemark.fare_capping

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.databinding.FragmentFareCappingBinding
import co.bytemark.domain.model.common.Display
import co.bytemark.widgets.util.getViewModel
import kotlinx.android.synthetic.main.fragment_notification.*

class FareCappingFragment : BaseMvvmFragment() {

    private lateinit var binding: FragmentFareCappingBinding

    lateinit var viewModel: FareCappingViewModel

    override fun onInject() = CustomerMobileApp.component.inject(this)

    init {
        CustomerMobileApp.component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFareCappingBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = getViewModel(CustomerMobileApp.appComponent.fareCappingViewModel)

        with(viewModel) {

            getFareCapping()

            errorLiveData.observe(this@FareCappingFragment, Observer {
                it?.let { handleError(it) }
            })

            fareCappingLiveData.observe(this@FareCappingFragment, Observer {
                binding.adapter = FareCappingAdapter(context, confHelper, it)
            })

            displayLiveData.observe(this@FareCappingFragment, Observer {
                when (it) {
                    is Display.EmptyState.ShowContent -> {
                        emptyStateLayout.showContent()
                    }

                    is Display.EmptyState.Loading -> {
                        emptyStateLayout.showLoading(R.drawable.ic_farecapping_filled, getString(R.string.loading))
                    }

                    is Display.EmptyState.ShowNoData -> {
                        showNoData()
                    }
                }
            })

        }
    }

    private fun showNoData() {
        emptyStateLayout.showEmpty(
            R.drawable.ic_farecapping_filled, getString(
                R.string.fare_capping_no_fare_caps_found
            ), "", getString(R.string.fare_medium_card_action_done)
        ) {
            activity?.finish()
        }
    }

    override fun connectionErrorDialog(
        positiveTextId: Int, enableNegativeAction: Boolean, negativeTextId: Int, finishOnDismiss: Boolean, positiveAction: () -> Unit, negativeAction: () -> Unit
    ) {
        showErrorStateWithRetry(R.string.network_connectivity_error, R.string.something_went_wrong)
    }

    override fun showDefaultErrorDialog(title: String?, errorMsg: String?, finishActivity: Boolean) {
        showErrorStateWithRetry()
    }

    private fun showErrorStateWithRetry(
        title: Int = R.string.popup_error, content: Int = R.string.something_went_wrong
    ) {
        emptyStateLayout.showError(
            R.drawable.ic_error_alert, getString(title), getString(content), getString(
                R.string.popup_retry
            ))
        {
            viewModel.getFareCapping()
        }

    }
}