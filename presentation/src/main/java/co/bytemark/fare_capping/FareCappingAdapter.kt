package co.bytemark.fare_capping

import android.content.Context
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.bold
import androidx.core.text.color
import androidx.core.text.scale
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.BR
import co.bytemark.R
import co.bytemark.data.util.Utils
import co.bytemark.databinding.ItemFareCapInProgressBinding
import co.bytemark.databinding.ItemFareCapStateCompleteBinding
import co.bytemark.databinding.ItemFareCapStateNoProgressBinding
import co.bytemark.domain.model.fare_capping.FareCapping
import co.bytemark.domain.model.fare_capping.FareCappingBindingData
import co.bytemark.helpers.ConfHelper

class FareCappingAdapter(val context: Context?, val confHelper: ConfHelper, fareCappingList:
MutableList<FareCapping> = mutableListOf()) : RecyclerView.Adapter<FareCappingAdapter.BaseFareCapViewHolder>() {

    companion object {
        const val EMPTY_PROGRESS_STATE = 0
        const val IN_PROGRESS_STATE = 1
        const val COMPLETED_STATE = 2
    }

    private var fareCappingBindingDataList = fareCappingList.map { it.toBindingData() }.toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {

        EMPTY_PROGRESS_STATE -> EmptyProgressViewHolder(
                ItemFareCapStateNoProgressBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

        IN_PROGRESS_STATE -> InProgressViewHolder(
                ItemFareCapInProgressBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

        else -> CompleteProgressViewHolder(
                ItemFareCapStateCompleteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: BaseFareCapViewHolder, position: Int) =
            holder.bind(fareCappingBindingDataList[position])

    override fun getItemCount() = fareCappingBindingDataList.size

    override fun getItemViewType(position: Int) = fareCappingBindingDataList[position].run {
        when {
            currentQuantity == 0 -> EMPTY_PROGRESS_STATE
            currentQuantity >= targetQuantity -> COMPLETED_STATE
            else -> IN_PROGRESS_STATE
        }
    }

    class EmptyProgressViewHolder(binding: ItemFareCapStateNoProgressBinding) : BaseFareCapViewHolder(binding)

    class InProgressViewHolder(binding: ItemFareCapInProgressBinding) : BaseFareCapViewHolder(binding)

    class CompleteProgressViewHolder(binding: ItemFareCapStateCompleteBinding) : BaseFareCapViewHolder(binding)

    abstract class BaseFareCapViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        open fun bind(bindingData: FareCappingBindingData) = binding.run {
            setVariable(BR.fareCapping, bindingData)
            executePendingBindings()
        }
    }

    private fun FareCapping.toBindingData() = FareCappingBindingData(
            type = SpannableStringBuilder().apply {
                if (context == null) return@apply
                if (confHelper.childOrganizations?.isNotEmpty() == true && organizationName.isNotEmpty()) {
                    bold {color(context.getColor(R.color.orgAccentBackgroundColor)) { append(organizationName) }}
                    append("\n")
                }
                color(context.getColor(R.color.orgCollectionPrimaryTextColor)) { append(type) }
            },
            currentQuantity = currentQuantity,
            targetQuantity = targetQuantity,
            description = description,
            statusMessage = SpannableStringBuilder().apply {
                if (context == null) return@apply
                bold { color(context.getColor(R.color.orgAccentBackgroundColor)) { append("$currentQuantity ") } }
                color(context.getColor(R.color.orgCollectionPrimaryTextColor)) { append(context.getString(R.string.fare_capping_of) + " ") }
                bold { color(context.getColor(R.color.orgAccentBackgroundColor)) { append("$targetQuantity ") } }
                color(context.getColor(R.color.orgCollectionPrimaryTextColor)) { append(context.getString(R.string.fare_capping_pass_used_plural)) }
            },
            formattedCycleStartTime = confHelper.getFormattedDateAndTime(
                    Utils.convertToCalendarFormServer(cycleStartTime)?.timeInMillis ?: 0L) ?: "",
            formattedCycleEndTime = confHelper.getFormattedDateAndTime(
                    Utils.convertToCalendarFormServer(cycleEndTime)?.timeInMillis ?: 0L) ?: "",
            formattedMoreInfo = moreInfo.joinToString(separator = "\n\n")
    )
}
