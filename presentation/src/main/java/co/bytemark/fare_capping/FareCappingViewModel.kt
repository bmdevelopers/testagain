package co.bytemark.fare_capping

import androidx.lifecycle.MutableLiveData
import co.bytemark.CustomerMobileApp
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.fare_capping.FareCappingResponse
import co.bytemark.domain.interactor.fare_capping.GetFareCappingUseCase
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.fare_capping.FareCapping
import kotlinx.coroutines.launch
import javax.inject.Inject

class FareCappingViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var fareCappingUseCase: GetFareCappingUseCase

    val fareCappingLiveData by lazy {
        MutableLiveData<MutableList<FareCapping>>()
    }

    val displayLiveData by lazy {
        MutableLiveData<Display>()
    }

    init {
        CustomerMobileApp.component.inject(this)
    }

    fun getFareCapping() = uiScope.launch {
        displayLiveData.value = Display.EmptyState.Loading()
        handleResult(fareCappingUseCase(Unit))
    }

    fun handleResult(result: Result<FareCappingResponse>) {
        when (result) {
            is Result.Success -> {
                result.data?.let {
                    if (it.fareCappingList.isNullOrEmpty()) {
                        displayLiveData.value = Display.EmptyState.ShowNoData()
                    } else {
                        fareCappingLiveData.value = it.fareCappingList
                        displayLiveData.value = Display.EmptyState.ShowContent()
                    }
                }
            }

            is Result.Failure -> {
                errorLiveData.value = result.bmError.first()
                displayLiveData.value = Display.EmptyState.Error()
            }
        }
    }


}