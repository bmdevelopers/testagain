package co.bytemark.notification_settings

import android.os.Bundle
import android.transition.Slide
import co.bytemark.R
import co.bytemark.base.ToolbarActivity
import co.bytemark.widgets.util.replaceFragment

class NotificationSettingsActivity : ToolbarActivity() {

    override fun getLayoutRes() = R.layout.activity_notification

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(getString(R.string.screen_title_notification))
        replaceFragment(NotificationSettingsFragment().apply { enterTransition = Slide() }, R.id.fragment)
    }
}
