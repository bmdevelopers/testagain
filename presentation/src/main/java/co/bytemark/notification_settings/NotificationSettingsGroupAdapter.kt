package co.bytemark.notification_settings

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.R
import co.bytemark.domain.model.notificationSettings.NotificationSettingTypes
import co.bytemark.domain.model.notificationSettings.NotificationSettings
import kotlinx.android.synthetic.main.settings_menu_group_item_template.view.*

class NotificationSettingsGroupAdapter(inline val onClick: (NotificationSettingTypes, Boolean) -> Unit = { _, _ -> }): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val notificationSettings = mutableListOf<NotificationSettings>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.settings_menu_group_item_template, parent, false))

    override fun getItemCount() = notificationSettings.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.apply {
            with(notificationSettings[position]) {
                menuGroupHeader.text = name
                dynamicMenuItemRecyclerView.adapter = NotificationSettingsItemAdapter({ notificationSettingTypes, isPermissionEnabled ->
                    onClick(notificationSettingTypes,isPermissionEnabled)
                }, notificationSettingTypes ?: mutableListOf())
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    infix fun setNotificationSettings(notificationList : MutableList<NotificationSettings>?) {
        notificationList?.let {
            notificationSettings.clear()
            notificationSettings.addAll(notificationList)
            notifyDataSetChanged()
        }
    }
}