package co.bytemark.notification_settings

import androidx.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Display
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.widgets.util.getViewModel
import kotlinx.android.synthetic.main.fragment_notification_settings.*
import javax.inject.Inject

class NotificationSettingsFragment : BaseMvvmFragment() {

    lateinit var viewModel: NotificationSettingsViewModel
    private lateinit var notificationsAdapter: NotificationSettingsGroupAdapter

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        LayoutInflater.from(context)
            .inflate(R.layout.fragment_notification_settings, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = getViewModel(CustomerMobileApp.appComponent.notificationSettingsViewModel)
        observerLiveData()

        notificationsAdapter =
            NotificationSettingsGroupAdapter { notificationSettingsTypes, isPermissionEnabled ->
                viewModel.updateNotificationPermissions(
                    notificationSettingsTypes,
                    isPermissionEnabled
                )
            }.also {
                notificationRecyclerView.adapter = it
            }

        swipeRefreshLayout.setOnRefreshListener { viewModel.loadNotificationSettings() }
    }

    private fun observerLiveData() {
        observeDisplayLiveData()
        observeErrorLiveData()
        observeNotificationLiveData()
        observeAccessibilityLiveData()
    }

    private fun observeDisplayLiveData() {
        viewModel.displayLiveData.observe(this, Observer {
            when (it) {
                is Display.SwipeRefreshLayout -> {
                    swipeRefreshLayout.isRefreshing = it.isRefreshing
                }

                is Display.EmptyState.Error -> {
                    it.errorTextContent?.let { it1 ->
                        emptyStateLayout.showError(it.errorImageDrawable, it.errorTextTitle, it1)
                        analyticsPlatformAdapter.notificationSettingsLoaded(
                            AnalyticsPlatformsContract.Status.FAILURE,
                            getString(it1)
                        )
                    }
                }

                is Display.EmptyState.ShowContent -> {
                    emptyStateLayout.showContent()
                    analyticsPlatformAdapter.notificationSettingsLoaded(
                        AnalyticsPlatformsContract.Status.SUCCESS,
                        ""
                    )
                }

                is Display.EmptyState.Loading -> {
                    emptyStateLayout.showLoading(it.drawable, it.title)
                }

                is Display.EmptyState.ShowNoData -> {
                    emptyStateLayout.showEmpty(it.drawable, it.descriptionText)
                    analyticsPlatformAdapter.notificationSettingsLoaded(
                        AnalyticsPlatformsContract.Status.SUCCESS,
                        getString(it.descriptionText)
                    )
                }
            }
        })
    }

    private fun observeErrorLiveData() {
        viewModel.errorLiveData.observe(this, Observer {
            it?.let { handleError(it) }
        })
    }

    private fun observeNotificationLiveData() {
        viewModel.notificationSettingsLiveData.observe(this, Observer {
            notificationsAdapter setNotificationSettings it
        })
    }

    private fun observeAccessibilityLiveData() {
        viewModel.accessibilityLiveData.observe(this, Observer {
            if (it is NotificationSettingsViewModel.TalkBack.EmptyStateLayout) {
                emptyStateLayout.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
                emptyStateLayout.announceForAccessibility(getString(it.contentDescription))
            }
        })
    }

    override fun connectionErrorDialog(
        positiveTextId: Int,
        enableNegativeAction: Boolean,
        negativeTextId: Int,
        finishOnDismiss: Boolean,
        positiveAction: () -> Unit,
        negativeAction: () -> Unit
    ) {
        viewModel.onConnectionError()
    }

    override fun onOnline() {
        super.onOnline()
        viewModel.loadNotificationSettings()
    }

    override fun onOffline() {
        super.onOffline()
        handleError(BMError(BytemarkSDK.ResponseCode.NETWORK_NOT_AVAILABLE))
    }

    override fun setUpColors() {
        super.setUpColors()
        with(confHelper) {
            swipeRefreshLayout.setColorSchemeColors(
                accentThemeBacgroundColor,
                headerThemeAccentColor,
                backgroundThemeBackgroundColor,
                dataThemeAccentColor
            )
        }
    }
}
