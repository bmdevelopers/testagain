package co.bytemark.notification_settings

import androidx.lifecycle.MutableLiveData
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.notification_settings.GetNotificationSettingsUseCase
import co.bytemark.domain.interactor.notification_settings.NotificationPermissionsRequestValues
import co.bytemark.domain.interactor.notification_settings.UpdateNotificationPermissionsUseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.notificationSettings.NotificationSettingTypes
import co.bytemark.domain.model.notificationSettings.NotificationSettings
import kotlinx.coroutines.launch
import javax.inject.Inject

class NotificationSettingsViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var getNotificationSettingsUseCaseUseCase: GetNotificationSettingsUseCase

    @Inject
    lateinit var updateNotificationPermissionsUseCase: UpdateNotificationPermissionsUseCase

    init {
        CustomerMobileApp.component.inject(this)
    }

    val displayLiveData by lazy {
        MutableLiveData<Display>()
    }

    val notificationSettingsLiveData by lazy {
        MutableLiveData<MutableList<NotificationSettings>>()
    }

    val accessibilityLiveData by lazy {
        MutableLiveData<TalkBack>()
    }

    fun loadNotificationSettings() = uiScope.launch {
        displayLiveData.value = Display.EmptyState.Loading(R.drawable.notification_material, R.string.loading)
        displayLiveData.value = Display.SwipeRefreshLayout(true)
        val result = getNotificationSettingsUseCaseUseCase(Unit)
        displayLiveData.value = Display.SwipeRefreshLayout(false)
        when (result) {
            is Result.Success -> {
                result.data?.let {
                    handleNotifications(it.notificationSettings?.toMutableList()
                            ?: mutableListOf())
                }
            }
            is Result.Failure -> {
                displayLiveData.value = Display.SwipeRefreshLayout(false)
                displayLiveData.value = Display.EmptyState.Error(R.drawable.error_material, R.string.popup_error, R.string.something_went_wrong)
                errorLiveData.value = result.bmError.first()
            }

        }
    }

    fun updateNotificationPermissions(notificationSettingTypes: NotificationSettingTypes, isPermissionEnabled: Boolean) = uiScope.launch {
        notificationSettingTypes.notificationPermissions?.get(0)?.allow = isPermissionEnabled
        when (val result = updateNotificationPermissionsUseCase(NotificationPermissionsRequestValues(notificationSettingTypes))) {
            is Result.Failure -> {
                displayLiveData.value = Display.EmptyState.Error(R.drawable.error_material, R.string.popup_error, R.string.something_went_wrong)
                errorLiveData.value = result.bmError.first()
            }
        }
    }

    private fun handleNotifications(notificationSettings: MutableList<NotificationSettings>) {
        displayLiveData.value = Display.EmptyState.ShowContent()
        notificationSettingsLiveData.value = notificationSettings
        if(notificationSettings.isEmpty()) {
            displayLiveData.value = Display.EmptyState.ShowNoData(R.drawable.notification_material, R.string.notifications_no_notification_groups)
            accessibilityLiveData.value = TalkBack.EmptyStateLayout(R.string.notifications_no_notification_groups)
        }
    }

    fun onConnectionError() {
        displayLiveData.value = Display.EmptyState.Error(R.drawable.error_material, R.string.network_connectivity_error, R.string.network_connectivity_error_message)
        accessibilityLiveData.value = TalkBack.EmptyStateLayout(R.string.network_connectivity_error)
    }

    sealed class TalkBack {
        class EmptyStateLayout(val contentDescription : Int) : TalkBack()
    }
}