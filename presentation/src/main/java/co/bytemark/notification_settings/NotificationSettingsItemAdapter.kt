package co.bytemark.notification_settings

import android.content.res.ColorStateList
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.SwitchCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.domain.model.notificationSettings.NotificationSettingTypes
import co.bytemark.helpers.ConfHelper
import kotlinx.android.synthetic.main.notification_settings_sub_category.view.*
import java.lang.StringBuilder
import javax.inject.Inject

class NotificationSettingsItemAdapter(inline val onClick: (NotificationSettingTypes, Boolean) -> Unit = { _, _ -> }, private val notificationSettings: List<NotificationSettingTypes>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    @Inject
    lateinit var confHelper: ConfHelper


    init {
        CustomerMobileApp.component.inject(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.notification_settings_sub_category, parent, false))

    override fun getItemCount() = notificationSettings.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.apply {
            with(notificationSettings[position]) {
                subCategoryText.text = name
                subscribe.isChecked = notificationPermissions?.get(0)?.allow == true
                setSwitchColor(subscribe)
                subscribe.setOnClickListener {
                    setSwitchColor(subscribe)
                    subCategoryItemLinearLayout.announceForAccessibility(StringBuilder().append(name).append(subscribe.isChecked))
                    onClick(this,subscribe.isChecked)
                }
            }
        }
    }

    private fun setSwitchColor(subscribe: SwitchCompat) {
        subscribe.thumbTintList = ColorStateList.valueOf(subscribe.context.resources.getColor(R.color.bt_very_light_gray))
        if (subscribe.isChecked)
            subscribe.trackTintList = ColorStateList.valueOf(confHelper.accentThemeBacgroundColor)
        else
            subscribe.trackTintList = ColorStateList.valueOf(subscribe.context.resources.getColor(R.color.warm_grey))
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}