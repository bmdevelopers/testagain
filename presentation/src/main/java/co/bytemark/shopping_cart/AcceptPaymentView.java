package co.bytemark.shopping_cart;

import android.content.Context;


import java.util.ArrayList;
import java.util.List;

import co.bytemark.domain.interactor.product.order.PurchaseRequestValues;
import co.bytemark.domain.model.discount.DiscountData;
import co.bytemark.domain.model.incomm.IncommBarcodeDetail;
import co.bytemark.domain.model.order.OfferServe;
import co.bytemark.mvp.MvpView;
import co.bytemark.sdk.model.common.Data;
import co.bytemark.sdk.model.payment_methods.Card;
import co.bytemark.sdk.model.payment_methods.Issuer;
import co.bytemark.sdk.model.product_search.entity.EntityResult;
import co.bytemark.sdk.post_body.PaymentTypes;

interface AcceptPaymentView extends MvpView {
    void showNoPaymentMethodsView();

    void setPaymentMethods(Data data);

    void showLoadingPaymentMethodsErrorDialog();

    void showLoadingPaymentMethodsErrorDialog(String errors);

    void showDeviceTimeErrorDialog();

    void showLoadingPaymentMethods();

    void showPlacingOrderView();

    void showOrderSuccessful(List<EntityResult> savedEntityResults, PaymentTypes orderType, IncommBarcodeDetail incommBarcodeDetail,String uuid);

    void showPlacingOrderWithError();

    void showPlacingOrderWithError(String title, String errors);

    void showPlacingOrderWithDeviceTimeError();

    void showCvvRequiredEditTextDialog(Card card);

    void showDeviceLostOrStolenErrorDialog();

    void showDeviceLostOrStolenError();

    Context getActivityContext();

    void showDefaultError(String message);

    void showSessionExpiredError(String message);

    void initializeBrainTree(String payPalToken);

    void launchWebViewPaymentsFlow(PaymentTypes paymentType, String url, String postData);

    void showIssuerPopUp(String orderUuid, List<Issuer> issuers, PurchaseRequestValues requestValues);

    void showStripView(List<Integer> imageResources, List<String> supportedPaymentTypes);

    void setGooglePayReady(boolean googlePayReady);

    void setPromoCodeProgressViewVisibility(int visibility);

    void showPromoCodeSuccess(OfferServe offerServe);

    void showPromoCodeError(String message);

    void hidePlacingOrderView();

    void showAppUpdateDialog();

    void setWalletLoadMoneyConfig(co.bytemark.domain.model.common.Data data);

    void setAcceptedPaymentMethods(ArrayList<String> acceptedPaymentMethods);

    void setAddPaymentMethodsScreenPresent(boolean value);

    void showReloadValueFetchError();

    void showLoading();

    void hideLoading();

    void showUpdatedSummaryWithDiscount(DiscountData discountData);

    void handleErrorOnDiscountCalculation(String message);
}
