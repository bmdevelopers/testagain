package co.bytemark.shopping_cart

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri

object RedirectUtil {
    private const val SYSTEM_PACKAGE_NAME = "android"

    @JvmStatic
    fun determineResolveResult(
        context: Context,
        uri: Uri?
    ): ResolveResult {
        val intent = Intent(Intent.ACTION_VIEW, uri)
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://"))
        val packageManager = context.packageManager
        val resolveInfo = packageManager.resolveActivity(intent, 0)
        val browserInfo =
            packageManager.resolveActivity(browserIntent, PackageManager.MATCH_DEFAULT_ONLY)
        val resolvedPackageName =
            if (resolveInfo?.activityInfo != null) resolveInfo.activityInfo.packageName else null
        val browserPackageName =
            if (browserInfo?.activityInfo != null) browserInfo.activityInfo.packageName else null
        return if (resolvedPackageName != null) {
            when (resolvedPackageName) {
                SYSTEM_PACKAGE_NAME -> {
                    ResolveResult(ResolveType.SYSTEM_COMPONENT)
                }
                browserPackageName -> {
                    ResolveResult(ResolveType.DEFAULT_BROWSER)
                }
                else -> {
                    ResolveResult(ResolveType.APPLICATION)
                }
            }
        } else ResolveResult(ResolveType.UNKNOWN)
    }

    enum class ResolveType {
        SYSTEM_COMPONENT,
        DEFAULT_BROWSER,
        APPLICATION,
        UNKNOWN
    }

    data class ResolveResult(val resolveType: ResolveType)
}