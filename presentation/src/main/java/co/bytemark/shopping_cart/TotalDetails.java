package co.bytemark.shopping_cart;

import androidx.annotation.ColorRes;

public class TotalDetails {

    private final int color;
    private final String title;
    private final String price;
    private final String contentDescription;
    private final int priceColor;
    private final String quantity;
    private final String originDestination;
    private final boolean subscribed;
    private final String licenceSpot;
    private boolean isFareMediumScreen;

    private TotalDetails(String title, String price, int color, int priceColor, String contentDescription, String quantity, String originDestination, String licenceSpot, boolean subscribed, boolean isFareMediumScreen) {
        this.title = title;
        this.price = price;
        this.color = color;
        this.priceColor = priceColor;
        this.contentDescription = contentDescription;
        this.quantity = quantity;
        this.originDestination = originDestination;
        this.licenceSpot = licenceSpot;
        this.subscribed = subscribed;
        this.isFareMediumScreen = isFareMediumScreen;
    }

    public int getTextColor() {
        return color;
    }

    public String getTitle() {
        return title;
    }

    String getPrice() {
        return price;
    }

    String getContentDescription() {
        return contentDescription;
    }

    int getPriceColor() {
        return priceColor;
    }

    public String getOriginDestination() {
        return originDestination;
    }

    public String getQuantity() {
        return quantity;
    }

    public boolean getSubscribed() {
        return subscribed;
    }

    public String getLicenceSpot() {
        return licenceSpot;
    }

    public boolean isFareMediumScreen() {
        return isFareMediumScreen;
    }

    public static class Builder {
        private String title;
        private String quantity;
        private String originDestination;
        private String licenseSpot;
        private boolean subscribed;
        private String price;
        private int color;
        private int priceColor;
        private String contentDescription;
        private boolean isFareMediumScreen;

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setPrice(String price) {
            this.price = price;
            return this;
        }

        public Builder setTextColor(@ColorRes int color) {
            this.color = color;
            return this;
        }

        public Builder setContentDescription(String contentDescription) {
            this.contentDescription = contentDescription;
            return this;
        }

        public Builder setPriceColor(int priceColor) {
            this.priceColor = priceColor;
            return this;
        }

        public Builder setQuantity(String quantity) {
            this.quantity = quantity;
            return this;
        }

        public Builder setOriginDestination(String originDestination) {
            this.originDestination = originDestination;
            return this;
        }

        public Builder setLicenseSpot(String licenseSpot) {
            this.licenseSpot = licenseSpot;
            return this;
        }


        public Builder setSubscribed(boolean subscribed) {
            this.subscribed = subscribed;
            return this;
        }

        public Builder setFareMediumScreen(boolean fareMediumScreen) {
            isFareMediumScreen = fareMediumScreen;
            return this;
        }

        public TotalDetails build() {
            return new TotalDetails(title, price, color, priceColor, contentDescription, quantity, originDestination, licenseSpot, subscribed, isFareMediumScreen);
        }


    }
}
