package co.bytemark.shopping_cart

import android.text.InputFilter
import android.text.Spanned

class AmountValidator(private val min: Int, private val max: Int) : InputFilter {

    override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence {
        try {
            val oldString = dest.toString().replace("[$,.]", "")
            val newString = oldString.replace("[^\\d]".toRegex(), "") +
                    source.toString().replace("[$,.]", "")
                            .replace("[^\\d]".toRegex(), "")
            val parsed = Integer.parseInt(newString)
            return if (isInRange(min, max, parsed)) {
                source
            } else {
                ""
            }
        } catch (e: NumberFormatException) {
            return ""
        }
    }

    private fun isInRange(a: Int, b: Int, c: Int): Boolean {
        return if (b > a) c in a..b else c in b..a
    }
}