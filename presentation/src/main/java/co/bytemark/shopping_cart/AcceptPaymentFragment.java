
package co.bytemark.shopping_cart;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.TaskStackBuilder;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.exceptions.BraintreeError;
import com.braintreepayments.api.exceptions.ErrorWithResponse;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import co.bytemark.CustomerMobileApp;
import co.bytemark.MainActivity;
import co.bytemark.R;
import co.bytemark.addPaymentMethods.AddPaymentMethodsActivity;
import co.bytemark.add_payment_card.AddPaymentCardActivity;
import co.bytemark.add_payment_card.Helper.TextWatcherHelper;
import co.bytemark.add_payment_card.PaymentCard;
import co.bytemark.add_payment_card.validators.CardCvvValidator;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.analytics.AnalyticsPlatformsContract;
import co.bytemark.authentication.AuthenticationActivity;
import co.bytemark.base.BaseMvpFragment;
import co.bytemark.base.MasterActivity;
import co.bytemark.base.TabsActivity;
import co.bytemark.buy_tickets.BuyTicketsActivity;
import co.bytemark.domain.interactor.product.order.PurchaseRequestValues;
import co.bytemark.domain.model.discount.DiscountData;
import co.bytemark.domain.model.incomm.IncommBarcodeDetail;
import co.bytemark.domain.model.order.OfferServe;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.helpers.DigitsInputFilter;
import co.bytemark.helpers.RxEventBus;
import co.bytemark.incomm.SelectRetailerActivity;
import co.bytemark.manage.ManageCardsActivity;
import co.bytemark.payment_methods.LoadMoneyWalletActivity;
import co.bytemark.payment_methods.PaymentMethodOrderComparator;
import co.bytemark.payment_methods.PaymentMethodsActivity;
import co.bytemark.payment_methods.PaymentsClassName;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.GPSTracker;
import co.bytemark.sdk.Helpers.PaymentMethod;
import co.bytemark.sdk.Utils;
import co.bytemark.sdk.model.common.Data;
import co.bytemark.sdk.model.common.Google3DSPaylaod;
import co.bytemark.sdk.model.payment_methods.BraintreePaypalPaymentMethod;
import co.bytemark.sdk.model.payment_methods.Card;
import co.bytemark.sdk.model.payment_methods.DotPay;
import co.bytemark.sdk.model.payment_methods.GooglePay;
import co.bytemark.sdk.model.payment_methods.Incomm;
import co.bytemark.sdk.model.payment_methods.Issuer;
import co.bytemark.sdk.model.payment_methods.Wallet;
import co.bytemark.sdk.model.payment_methods.WalletLoadMoneyConfig;
import co.bytemark.sdk.model.product_search.entity.EntityResult;
import co.bytemark.sdk.post_body.Ideal;
import co.bytemark.sdk.post_body.PayNearMe;
import co.bytemark.sdk.post_body.PaymentTypes;
import co.bytemark.shopping_cart_new.NewShoppingCartActivity;
import co.bytemark.use_tickets.BackHandler;
import co.bytemark.use_tickets.UseTicketsActivity;
import co.bytemark.widgets.BmRadioButton;
import co.bytemark.widgets.CircleBorderImageView;
import co.bytemark.widgets.ImageViewStrip;
import co.bytemark.widgets.ProgressViewLayout;
import co.bytemark.widgets.util.ExtensionsKt;
import icepick.State;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

import static android.text.TextUtils.isEmpty;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static androidx.core.graphics.ColorUtils.setAlphaComponent;
import static co.bytemark.sdk.model.config.RowType.DESTINATION_ATTRIBUTE_NAME;
import static co.bytemark.sdk.model.config.RowType.ORIGIN_ATTRIBUTE_NAME;
import static co.bytemark.widgets.util.Util.dpToPx;

@SuppressWarnings("all")
public class AcceptPaymentFragment extends BaseMvpFragment<AcceptPaymentView, AcceptPaymentPresenter>
		implements AcceptPaymentView, SharedPreferences.OnSharedPreferenceChangeListener, BackHandler {

	private static final int NONE = -1;
	private static final int ADD = 1;
	private static final int CHANGE = 2;
	private static final int RETRY = 3;
	private static final int ACCOUNT = 4;
	private static final int BACK_PRESSED = 5;
	private static final int UPDATE_SHOPPING_CARD = 6;
	private static final int VIEW_TICKETS = 7;


	@State
	int navigateAwayReason = NONE;

	@Inject
	Gson gson;

	@Inject
	ConfHelper confHelper;

	@Inject
	SharedPreferences sharedPreferences;

	@Inject
	AcceptPaymentPresenter presenter;

	@Inject
	RxEventBus eventBus;

	@BindView(R.id.constraintLayoutReload)
	ConstraintLayout constraintLayoutReload;
	@BindView(R.id.constraintLayoutReloadOptions)
	ConstraintLayout constraintLayoutReloadOptions;
	@BindView(R.id.radioGroup)
	RadioGroup radioGroup;
	@BindView(R.id.buttonOtherAmount)
	Button buttonOtherAmount;
	@BindView(R.id.buttonRetryReload)
	Button buttonRetryAutoLoad;
	@BindView(R.id.constraintLayoutReloadAmount)
	ConstraintLayout constraintLayoutReloadAmount;
	@BindView(R.id.editTextReloadAmount)
	EditText editTextReloadAmount;
	@BindView(R.id.textViewReloadError)
	TextView textViewReloadError;
	@BindView(R.id.textViewErrorLoadingReloadValue)
	TextView textViewErrorLoadingReloadValue;
    @BindView(R.id.textViewReloadAmount)
    TextView textViewReloadAmount;
    @BindView(R.id.buttonDoneAmount)
    Button buttonDoneAmount;
    @BindView(R.id.imageViewCloseAmount)
    ImageView imageViewCloseAmount;

    @BindView(R.id.rlShoppingCart)
    LinearLayout rootLayout;

    @BindView(R.id.tv_no_payment_methods_message)
    TextView textViewNoPaymentMethods;

    @BindView(R.id.ll_payment_methods_loading)
    LinearLayout linearLayoutPaymentMethodsLoading;

    @BindView(R.id.ll_payment_methods)
    LinearLayout linearLayoutPaymentMethods;

    @BindView(R.id.ll_second_payment_methods)
    LinearLayout linearLayoutSecondPaymentMethods;

	@BindView(R.id.iv_card_type)
	ImageView imageViewCardType;

	@BindView(R.id.iv_second_card_type)
	ImageView imageViewSecondCardType;

	@BindView(R.id.tv_card_label)
	TextView textViewCardLabel;

	@BindView(R.id.tv_second_card_label)
	TextView textViewSecondCardLabel;

	@BindView(R.id.tv_card_number)
	TextView textViewCardNumber;

	@BindView(R.id.tv_second_card_number)
	TextView textViewSecondCardNumber;

    @BindView(R.id.tv_card_expiration)
    TextView textViewCardExpiration;

    @BindView(R.id.tv_second_card_expiration)
    TextView textViewSecondCardExpiration;

    @BindView(R.id.tv_payment_methods)
    TextView textViewPaymentMethods;

    @BindView(R.id.splitPaymentSwitch)
    Switch splitPaymentSwitch;

    @BindView(R.id.textViewSwitchLabel)
    TextView switchLabel;

    @BindView(R.id.tv_select_two_payment_methods_message)
    TextView textViewSelectTwoPaymentMethods;

    @BindView(R.id.tv_error_loading_payment_method)
    TextView textViewErrorLoadingPaymentMethod;

    @BindView(R.id.btn)
    Button changePayment;

    @BindView(R.id.ll_network_connectivity_error_cart)
    LinearLayout linearLayoutNetworkConnectivityError;

    @BindView(R.id.iv_network_error)
    CircleBorderImageView circleBorderImageViewNetwork;

	@BindViews({R.id.tv_network_error_1, R.id.tv_network_error_2, R.id.tv_network_error_3})
	List<TextView> textViewNetworkError;

	@BindViews({R.id.btn_view_tickets, R.id.btn_retry})
	List<Button> buttonsNetwork;

	@BindView(R.id.shoppingCartEmptyLayout)
	LinearLayout linearLayoutEmptyShoppingCart;

	@BindView(R.id.linearLayoutOrderSuccessTexts)
	LinearLayout linearLayoutOrderSuccessfulText;

	@BindView(R.id.emptyCartTextViews)
	LinearLayout linearLayoutEmptyCartText;

	@BindView(R.id.emptyCartImageView)
	CircleBorderImageView circleBorderImageView;

	@BindView(R.id.buyTicketsEmptyCartButton)
	Button buttonBuyTickets;

	@BindViews(R.id.emptyCartTextview)
	List<TextView> textViews;

	@BindView(R.id.scr_shopping_cart)
	NestedScrollView scrollViewShoppingCart;

	@BindView(R.id.ll_shopping_cart_total_details)
	LinearLayout linearLayoutShoppingCartTotalDetails;

	@BindView(R.id.totalValueTextView)
	TextView textViewTotal;

	@BindView(R.id.tv_total_title)
	TextView textViewTotalTitle;

	@BindView(R.id.btn_place_order)
	Button buttonPlaceOrder;

	@BindView(R.id.buttonApply)
	Button buttonApply;

	@BindView(R.id.ll_loading_screen)
	LinearLayout linearLayoutLoadingScreen;

	@BindView(R.id.ll_loading)
	LinearLayout linearLayoutLoading;

	@BindView(R.id.pvl_placing_your_order)
	ProgressViewLayout progressViewLayoutPlacingYourOrder;

	@BindView(R.id.loadingImageView)
	ProgressViewLayout loadingImageView;

	@BindView(R.id.iv_placing_your_order)
	ImageView imageViewPlacingYourOrder;

	@BindView(R.id.iv_loading)
	ImageView imageViewLoading;

	@BindViews({R.id.tv_loading_title, R.id.tv_loading_message})
	List<TextView> textViewPlacingYourOrder;

	@BindView(R.id.tv_loading)
	TextView textViewLoding;

	@BindView(R.id.linearLayoutOrderSuccessful)
	LinearLayout linearLayoutOrderSuccessful;

	@BindView(R.id.circleImageViewOrderSuccessful)
	ImageView imageViewOrderSuccessful;

	@BindViews({R.id.textViewOrderSuccessful1, R.id.textViewOrderSuccessful2})
	List<TextView> textViewOrderSuccessful;

	@BindView(R.id.buttonMyTickets)
	Button buttonMyTickets;

	@BindView(R.id.iv_info_icon)
	ImageView infoIcon;

	@BindView(R.id.ll_payment_method)
	LinearLayout linearLayoutPaymentMethod;

	@BindView(R.id.llTotalShoppingCart)
	LinearLayout linearLayoutTotalShoppingCart;

	@BindView(R.id.textViewReload)
	TextView textViewReload;

	@BindView(R.id.textViewSummary)
	TextView textViewSummary;

	@BindView(R.id.ll_promo_code)
	LinearLayout linearLayoutPromoCode;

	@BindView(R.id.ll_promo_code_applied)
	LinearLayout linearLayoutPromoApplied;

	@BindView(R.id.tvPromoCode)
	TextView tvPromoCode;

	@BindView(R.id.imageViewCross)
	ImageView imageViewCross;

	@BindView(R.id.textPromoCode)
	TextView textPromoCode;

	@BindView(R.id.tv_sign_in_message)
	TextView textViewSignInMessage;

	@BindView(R.id.textViewOrderSuccessful1)
	TextView textViewSuccessTitle;

	@BindView(R.id.textViewOrderSuccessful2)
	TextView textViewSuccessMessage;

	@BindViews({R.id.tv_no_payment_methods_message, R.id.tv_error_loading_payment_method})
	List<TextView> textViewNoPaymentMethodsMessage;

	@BindView(R.id.tv_retrieving_payment_method)
	TextView textViewRetrievingPaymentMethod;

	@BindView(R.id.ll_payment_cvv)
	LinearLayout linearLayoutPaymentCVV;

	@BindView(R.id.ll_second_payment_cvv)
	LinearLayout linearLayoutSecondPaymentCVV;

	@BindView(R.id.et_payment_cvv)
	EditText editTextPaymentCVV;

	@BindView(R.id.et_second_payment_cvv)
	EditText editTextSecondPaymentCVV;

	@BindView(R.id.ll_info_bar)
	LinearLayout linearLayoutInfoBar;

	@BindView(R.id.tv_info_bar)
	TextView textViewInfoBar;

	@BindView(R.id.ll_payment)
	LinearLayout linearLayoutPayment;

	@BindView(R.id.iv_type)
	ImageView imageViewType;

	@BindView(R.id.tv_label)
	TextView textViewLabel;

	@BindView(R.id.tv_label_2)
	TextView textViewLabel2;

	@BindView(R.id.bottom_sheet)
	View bottomSheet;

	@BindView(R.id.issuer_list)
	RecyclerView issuerList;

	@BindView(R.id.acceptedPaymentsImages)
	ImageViewStrip imageViewStrip;

	@BindView(R.id.linearLayoutSummary)
	LinearLayout linearLayoutSummary;

	@BindView(R.id.ll_amount_first_card)
	LinearLayout linearLayoutFirstCardAmount;

	@BindView(R.id.ll_amount_second_card)
	LinearLayout linearLayoutSecondCardAmount;

	@BindView(R.id.et_enter_amount_card1)
	EditText editTextFirstCardAmount;

	@BindView(R.id.et_enter_amount_card2)
	EditText editTextSecondCardAmount;

	@BindView(R.id.buttonUpdateOrder)
	Button buttonUpdateOrder;

	@BindView(R.id.editPromoCode)
	EditText editTextPromoCode;

	@BindView(R.id.progressBarLoadReloadValues)
	ProgressBar progressBarLoadReloadAmount;

	@BindView(R.id.progressViewLayoutLoading)
	ProgressViewLayout progressViewLayoutLoading;

	@BindView(R.id.tv_sub_discount_total_title)
	LinearLayout linearLayoutSubTotalShoppingCart;

	@BindView(R.id.llDiscountShoppingCart)
	LinearLayout llDiscountShoppingCart;

	@BindView(R.id.swipeRefreshLayout)
	SwipeRefreshLayout swipeRefreshLayout;

	@BindView(R.id.tv_discount_total)
	TextView tvDiscountTotal;

	@BindView(R.id.tv_sub_total)
	TextView textViewSubTotal;

	@BindView(R.id.textViewReloadMessage)
	TextView tvReloadMessage;

	@BindViews({R.id.line1, R.id.line2})
	List<View> viewList;
	@State
	Card card;
	@State
	BraintreePaypalPaymentMethod payPal;
	@State
	Ideal ideal;
	@State
	GooglePay googlePay;
	@State
	PayNearMe payNearMe;
	@State
	Incomm incomm;
	@State
	String orderUuid;
	@State
	DotPay dotPay;
	@State
	Wallet wallet;

	@BindView(R.id.updatePaymentLinearLayout)
	LinearLayout updatePaymentLinearLayout;

	@Inject
	AnalyticsPlatformAdapter analyticsPlatformAdapter;

	private String traceNumber;
	private BottomSheetBehavior bottomSheetBehavior;
	private List<EntityResult> savedEntityResults = new ArrayList<>();
	private int total;
	private int subTotal;

	private boolean isPaymentMethodsListEmpty = false;
	private boolean isOrderSuccess = false;

	private RxPermissions rxPermissions;

	private BraintreeFragment brainTreeFragment;

	private boolean isIntentForReload = false;
    private String fareMediaUuid = null;
    private Wallet reloadingWallet = null;
    private ArrayList<PaymentMethod> selectedPaymentMethods = new ArrayList<>();
    private PaymentMethod defaultPaymentMethod;
    private boolean googlePayReady;
    private OfferServe offerServe;
    private boolean addPaymentMethodsScreenPresent;
    private ArrayList<String> acceptedPaymentMethods;
    private WalletLoadMoneyConfig walletLoadMoneyConfig;
    private final CompositeSubscription subs = new CompositeSubscription();
    private boolean isSplitPaymentEnabled = false;
	private DiscountData discountData = null;
	private boolean isDeepLinkDiscountInitiatedOnLogout = false;
	private boolean allowBackPressOnDiscountCalculation = false;

    private String orderTypeStr = AnalyticsPlatformsContract.OrderType.PASSES;

    @NonNull
    @Override
    public AcceptPaymentPresenter createPresenter() {
        return presenter;
    }

    @Override
	protected int getLayoutRes() {
		return R.layout.fragment_shopping_cart;
	}

	@Override
	protected void onInject() {
		CustomerMobileApp.Companion.getComponent().inject(this);
	}

	public static AcceptPaymentFragment newInstance() {
		return new AcceptPaymentFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		isIntentForReload = getActivity().getIntent().getBooleanExtra(AppConstants.RELOAD, false);
		fareMediaUuid = getActivity().getIntent().getStringExtra("fareMediumId");
		reloadingWallet = getActivity().getIntent().getParcelableExtra(AppConstants.WALLET);
		PaymentMethod paymentMethod = getActivity().getIntent().getParcelableExtra("paymentMethod");
		if (paymentMethod != null && paymentMethod instanceof GooglePay) {
			defaultPaymentMethod = paymentMethod;
		}
		Timber.d("fareMediumId: " + fareMediaUuid);
		Timber.d("walletID: " + reloadingWallet);
	}

	private boolean isActionView(String action) {
		return action.contentEquals("android.intent.action.VIEW");
	}

	private boolean isHostIdeal(Uri data) {
		return data.getHost().contentEquals("adyen_result");
	}

	private void showResult(Uri data) {
		String result = parseAndGetAuthResult(data.getQuery());
		if (result != null) {
			switch (result) {
				case AppConstants.RESULT_AUTHORIZED:
					showOrderSuccessful(savedEntityResults, PaymentTypes.IDEAL, null, null);
					break;
				case AppConstants.RESULT_REFUSED:
					webViewTransactionFailed(getString(R.string.payment_refused), getString(R.string.payment_order_refused_message));
					break;
				case AppConstants.RESULT_PENDING:
					webViewTransactionFailed(getString(R.string.payment_pending), getString(R.string.payment_order_pending_message));
					break;
				default:
					webViewTransactionFailed(getString(R.string.payment_cancelled), getString(R.string.payment_order_cancelled_message));
			}
		} else {
			webViewTransactionFailed(getString(R.string.payment_cancelled), getString(R.string.payment_order_cancelled_message));
		}
	}

	private String parseAndGetAuthResult(String url) {
		try {
			return url.split("authResult")[1].split("&")[0].replace("=", "");
		} catch (Exception e) {
			Timber.e(e);
			return null;
		}
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		swipeRefreshLayout.setColorSchemeColors(
				confHelper.getAccentThemeBacgroundColor(),
				confHelper.getHeaderThemeAccentColor(),
				confHelper.getBackgroundThemeBackgroundColor(),
				confHelper.getDataThemeAccentColor());
		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				if (isOnline() && !isZerDollerPurchase())
					presenter.loadPaymentMethods(getOrganizationUUIDFromEntities());
				else
					swipeRefreshLayout.setRefreshing(false);
			}
		});

		presenter.createPaymentClientForGooglePay(getActivityContext());
		if (confHelper.isGooglePayEnabled())
			presenter.isGooglePayReady();

        rxPermissions = new RxPermissions(getActivity());
        makeAuditoryAnnouncement(view);

        if (isIntentForReload) {
            fetchLoadValues();
            subs.add(eventBus.observeEvents(co.bytemark.domain.model.common.Data.class)
                    .subscribe(data -> setWalletLoadMoneyConfig(data)));
			tvReloadMessage.setVisibility(VISIBLE);
			if (fareMediaUuid != null) {
				orderTypeStr = AnalyticsPlatformsContract.OrderType.LOAD_FARE_MEDIUM;
				tvReloadMessage.setText(R.string.shopping_cart_reload_card_message_text);
			} else {
				orderTypeStr = AnalyticsPlatformsContract.OrderType.LOAD_WALLET;
				tvReloadMessage.setText(R.string.shopping_cart_reload_wallet_message_text);
			}
        }

        textViewErrorLoadingPaymentMethod.setVisibility(GONE);
        textViewSelectTwoPaymentMethods.setVisibility(GONE);
        textViewSignInMessage.setVisibility(GONE);
        textViewNoPaymentMethods.setVisibility(GONE);
        linearLayoutPaymentMethodsLoading.setVisibility(GONE);
        linearLayoutSecondPaymentMethods.setVisibility(GONE);
        llDiscountShoppingCart.setVisibility(GONE);

        handlePromoCodeLayout();

        setBottomSheet();
        getProductEntityListFromSharedPrefs();
        if (!isIntentForReload) {
            updateTotalDetails();
		}

		if (confHelper.getOrganization().getPassConfiguration().getAutoActivation()) {
			buttonPlaceOrder.setText(getString(R.string.payment_buy_and_activate));
		}
		validatePlaceOrderButton();

		editTextFirstCardAmount.setFilters(new InputFilter[]{new AmountValidator(0, total)});
		editTextSecondCardAmount.setFilters(new InputFilter[]{new AmountValidator(0, total)});
		editTextReloadAmount.setFilters(new InputFilter[]{new DigitsInputFilter(3, 2)});

		editTextFirstCardAmount.addTextChangedListener(new TextWatcherHelper() {
			@Override
			public void afterTextChanged(Editable editable) {
				try {
					String text = editable.toString();
					if (text!= null && !text.isEmpty() && !text.equals(editTextFirstCardAmount.getText())) {
						editTextFirstCardAmount.removeTextChangedListener(this);
						String cleanString = text.replaceAll("[$,.]", "");
						Integer parsed = Integer.parseInt(cleanString.replaceAll("[^\\d]", ""));
						Integer secondAmount = total - parsed;
						String formatted = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(parsed);
						String secondAmountFormatted = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(secondAmount);
						editTextFirstCardAmount.setText(formatted);
						editTextSecondCardAmount.setText(secondAmountFormatted);
						editTextFirstCardAmount.setSelection(formatted.length());
						editTextSecondCardAmount.setSelection(secondAmountFormatted.length());
						editTextFirstCardAmount.addTextChangedListener(this);
						validatePlaceOrderButton();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		editTextSecondCardAmount.addTextChangedListener(new TextWatcherHelper() {
			@Override
			public void afterTextChanged(Editable editable) {
				try {
					String text = editable.toString();
					if (text!= null && !text.isEmpty() && text.equals(editTextSecondCardAmount.getText())) {
						editTextSecondCardAmount.removeTextChangedListener(this);
						String cleanString = text.replaceAll("[$,.]", "");
						Integer parsed = Integer.parseInt(cleanString.replaceAll("[^\\d]", ""));
						Integer firstAmount = total - parsed;
						String formatted = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(parsed);
						String firstAmountFormatted = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(firstAmount);
						editTextSecondCardAmount.setText(formatted);
						editTextSecondCardAmount.setSelection(formatted.length());
						editTextFirstCardAmount.setText(firstAmountFormatted);
						editTextFirstCardAmount.setSelection(firstAmountFormatted.length());
						editTextSecondCardAmount.addTextChangedListener(this);
						validatePlaceOrderButton();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		// for supporting splitpayment in load money. total value is not set during the preset value selection
		// So in the splitpayment, amount is considered as 0. This will solve that
		radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				total = (int) group.findViewById(checkedId).getTag();
				clearAmountFields();
				editTextFirstCardAmount.setFilters(new InputFilter[]{new AmountValidator(0, total)});
				editTextSecondCardAmount.setFilters(new InputFilter[]{new AmountValidator(0, total)});
				validatePlaceOrderButton();
			}
		});
	}

	private void handleDeepLinkDiscountAndPaymentsMethods() {
		if(getActivity().getIntent().getStringExtra(AppConstants.DEEPLINK_JWT_TOKEN) != null && discountData == null) {
			buttonApply.setEnabled(false);
			buttonApply.setAlpha(0.5f);
			if(!BytemarkSDK.isLoggedIn()) {
				isDeepLinkDiscountInitiatedOnLogout = true;
				swipeRefreshLayout.setVisibility(GONE);
				showSigninMaterialDialog(R.string.sign_in_required, R.string.deeplink_discount_sign_in_required_message);
			} else {
				presenter.discountCalculation(getActivity().getIntent().getParcelableExtra(AppConstants.DEEPLINK_DISCOUNT), getActivity().getIntent().getStringExtra(AppConstants.DEEPLINK_JWT_TOKEN));
			}
		} else {
			if (defaultPaymentMethod == null && !isZerDollerPurchase()) {
				linearLayoutPaymentMethod.setVisibility(VISIBLE);
				updatePaymentLinearLayout.setVisibility(VISIBLE);
				loadPaymentMethodsIfLoggedIn();
			} else if(isZerDollerPurchase()){
				linearLayoutPaymentMethod.setVisibility(GONE);
				updatePaymentLinearLayout.setVisibility(GONE);
				linearLayoutPaymentMethods.setVisibility(GONE);
			}
		}
	}

	public void clearAmountFields(){
    	editTextFirstCardAmount.setText("");
    	editTextSecondCardAmount.setText("");
	}

	public void setBottomSheet() {
		bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
		bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
		bottomSheetBehavior.setPeekHeight(0);
		bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
			@Override
			public void onStateChanged(@NonNull View bottomSheet, int newState) {
				if (newState == BottomSheetBehavior.STATE_HIDDEN) {
					buttonPlaceOrder.setVisibility(VISIBLE);
				}
			}

			@Override
			public void onSlide(@NonNull View bottomSheet, float slideOffset) {

			}
		});
		bottomSheet.setVisibility(GONE);
	}

	private void fetchLoadValues() {
		presenter.getWalletLoadMoneyConfig();
		fetchLoadValuesUpdateVisibilityState(true);
		constraintLayoutReload.setVisibility(VISIBLE);
	}

	private void fetchLoadValuesUpdateVisibilityState(boolean isFetching) {
		buttonRetryAutoLoad.setVisibility(GONE);
		progressBarLoadReloadAmount.setVisibility(isFetching ? VISIBLE : GONE);
		textViewErrorLoadingReloadValue.setVisibility(GONE);
		buttonOtherAmount.setVisibility(isFetching ? GONE : VISIBLE);
	}

	@Override
	public void showReloadValueFetchError() {
		buttonRetryAutoLoad.setVisibility(VISIBLE);
		buttonOtherAmount.setVisibility(View.INVISIBLE);
		textViewErrorLoadingReloadValue.setVisibility(VISIBLE);
		constraintLayoutReloadOptions.setVisibility(VISIBLE);
		constraintLayoutReloadAmount.setVisibility(GONE);
		progressBarLoadReloadAmount.setVisibility(GONE);
	}

	private void makeAuditoryAnnouncement(View view) {
		linearLayoutPaymentMethodsLoading.setContentDescription(getString(R.string.purchase_history_payment_method));
		view.announceForAccessibility(getString(R.string.buy_tickets_button_name));
		linearLayoutOrderSuccessfulText.setContentDescription(getString(R.string.shopping_cart_order_successful) +
				getString(R.string.shopping_cart_order_successful_message));

		linearLayoutEmptyCartText.setContentDescription(getString(R.string.shopping_cart_is_empty));
	}

	@Override
	public void onResume() {
		super.onResume();
		sharedPreferences.registerOnSharedPreferenceChangeListener(this);

		if (isOnline() && defaultPaymentMethod != null && !isZerDollerPurchase()) {
			linearLayoutPaymentMethod.setVisibility(VISIBLE);
			updatePaymentLinearLayout.setVisibility(VISIBLE);
			loadPaymentMethodsIfLoggedIn();
		}

		Intent intent = getActivity().getIntent();
		String action = intent.getAction();
		Uri data = intent.getData();

		if (action != null && data != null) {
			if (isActionView(action)) {
				if (isHostIdeal(data)) {
					showResult(data);
				}
			}
		}
		initializeCardAmountEditTexts();
		validatePlaceOrderButton();

		if (linearLayoutLoadingScreen.getVisibility() == VISIBLE)
			buttonPlaceOrder.setVisibility(GONE);

		if(!isOnline()) {
			buttonPlaceOrder.setEnabled(false);
			int alphaComponent = setAlphaComponent(confHelper.getAccentThemeBacgroundColor(), 26);
			buttonPlaceOrder.setBackgroundTintList(ColorStateList.valueOf(alphaComponent));
			buttonPlaceOrder.setBackgroundColor(getResources().getColor(R.color.warm_grey));
		}
	}

	private void initializeCardAmountEditTexts() {
		editTextFirstCardAmount.setImeOptions(EditorInfo.IME_ACTION_DONE);
	}

	private void getProductEntityListFromSharedPrefs() {
		if (!isIntentForReload) {

			String entityResultString = sharedPreferences.getString(AppConstants.ENTITY_RESULT_LIST, null);

			prepareTransition();

			if (entityResultString != null) {
				savedEntityResults = gson.fromJson(entityResultString, new TypeToken<List<EntityResult>>() {
				}.getType());

				if (savedEntityResults != null && !savedEntityResults.isEmpty()) {
					linearLayoutEmptyShoppingCart.setVisibility(GONE);
					if (linearLayoutNetworkConnectivityError.getVisibility() != View.VISIBLE)
						buttonPlaceOrder.setVisibility(VISIBLE);
					scrollViewShoppingCart.setVisibility(VISIBLE);
				} else {
					linearLayoutEmptyShoppingCart.setVisibility(VISIBLE);
					scrollViewShoppingCart.setVisibility(GONE);
					buttonPlaceOrder.setVisibility(GONE);
					linearLayoutEmptyCartText.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
					linearLayoutEmptyCartText.announceForAccessibility(getString(R.string.shopping_cart_is_empty));
					hideInfoBar();
				}
			} else {
				linearLayoutEmptyShoppingCart.setVisibility(VISIBLE);
				scrollViewShoppingCart.setVisibility(GONE);
				buttonPlaceOrder.setVisibility(GONE);
				linearLayoutEmptyCartText.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
				linearLayoutEmptyCartText.announceForAccessibility(getString(R.string.shopping_cart_is_empty));
			}
		} else if ((confHelper.getOrganization().getPassConfiguration().getFareMediumRowConfiguration() != null &&
				!confHelper.getOrganization().getPassConfiguration().getFareMediumRowConfiguration().isEmpty())
				|| reloadingWallet != null) {
			linearLayoutSummary.setVisibility(GONE);
		}
	}

	private void prepareTransition() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && rootLayout != null)
			TransitionManager.beginDelayedTransition(rootLayout, new AutoTransition().setDuration(200));
	}

	private void updateTotalDetails() {
		total = 0;
		subTotal = 0;

		linearLayoutShoppingCartTotalDetails.removeAllViews();

		if (savedEntityResults != null) {
			for (int i = 0; i < savedEntityResults.size(); i++) {

				int salePrice = savedEntityResults.get(i).getSalePrice();
				int quantity = savedEntityResults.get(i).getQuantity();
				subTotal += salePrice * quantity;

				String productName, origin, destination, originDestination = null, licenceSpot = null;
				if (!isEmpty(savedEntityResults.get(i).getName())) {
					productName = savedEntityResults.get(i).getName();
				} else {
					productName = savedEntityResults.get(i).getLabelName();
				}

				Boolean displayLongNameForOd = confHelper.isDisplayLongNameForOd(savedEntityResults.get(i).getOrganization().getUuid());
				origin = savedEntityResults.get(i).getAppliedFilters(ORIGIN_ATTRIBUTE_NAME, displayLongNameForOd);
				destination = savedEntityResults.get(i).getAppliedFilters(DESTINATION_ATTRIBUTE_NAME, displayLongNameForOd);
				if (origin != null && destination != null) {
					originDestination = origin + " / " + destination;
				}

				if (savedEntityResults.get(i).getInputFieldRequired() != null && savedEntityResults.get(i).getInputFields() != null &&
						savedEntityResults.get(i).getInputFields().getLicensePlateNumber() != null && savedEntityResults.get(i).getInputFields().getSpotNumber() != null) {

					licenceSpot = savedEntityResults.get(i).getInputFields().getLicensePlateNumber() + " / " + savedEntityResults.get(i).getInputFields().getSpotNumber();
				}


				linearLayoutShoppingCartTotalDetails.addView(new TotalDetailsItemView(getContext(), new TotalDetails.Builder()
						.setTitle(productName)
						.setQuantity("x" + quantity)
						.setOriginDestination(originDestination)
						.setLicenseSpot(licenceSpot)
						.setSubscribed(savedEntityResults.get(i).getCreateSubscription())
						.setPrice(confHelper.getConfigurationPurchaseOptionsCurrencySymbol(salePrice * quantity))
						.setTextColor(confHelper.getDataThemePrimaryTextColor())
						.setFareMediumScreen(!TextUtils.isEmpty(fareMediaUuid))
						.setContentDescription(quantity + " " + productName + " " + getString(R.string.payment_amount_is_voonly) + "\n"
								+ confHelper.getConfigurationPurchaseOptionsCurrencySymbol(salePrice * quantity) +
								(savedEntityResults.get(i).getCreateSubscription() ?
										(!TextUtils.isEmpty(fareMediaUuid) ? getString(R.string.pass_auto_renewals_shopping_cart_auto_renewed)
												: getString(R.string.shopping_cart_subscribed)) : " ")
						)
						.build()));
			}
			total = subTotal;
			textViewSubTotal.setText(String.format("%s", confHelper.getConfigurationPurchaseOptionsCurrencySymbol(subTotal)));
			textViewTotal.setTextColor(confHelper.getDataThemePrimaryTextColor());
			textViewTotal.setText(String.format("%s", confHelper.getConfigurationPurchaseOptionsCurrencySymbol(total)));
			textViewTotalTitle.setTextColor(confHelper.getDataThemePrimaryTextColor());
			linearLayoutTotalShoppingCart.setContentDescription(getString(R.string.shopping_cart_total_amount_is_voonly) + "\n" + confHelper.getConfigurationPurchaseOptionsCurrencySymbol(total));
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		updateShoppingCart(savedEntityResults, isNewTraceRequried());
		sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
	}

	public boolean isNewTraceRequried() {
		return (linearLayoutNetworkConnectivityError.getVisibility() != View.VISIBLE && !confHelper.isUsingOldOrdersEndPoint()) ? true : false;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		presenter.unSubscribe();
		subs.clear();
	}

	@Override
	protected void onOnline() {
        linearLayoutFirstCardAmount.setVisibility(GONE);
        textViewErrorLoadingPaymentMethod.setVisibility(GONE);
        handleDeepLinkDiscountAndPaymentsMethods();
        validatePlaceOrderButton();

        if (defaultPaymentMethod != null && defaultPaymentMethod instanceof GooglePay) {
            setGooglePay((GooglePay) defaultPaymentMethod);
            onPlaceOrderButtonClick();
        }

        if (isIntentForReload && radioGroup.getChildCount() <= 0) {
			fetchLoadValues();
		}

		defaultPaymentMethod = null;
	}

	private void loadPaymentMethodsIfLoggedIn() {
		if (BytemarkSDK.isLoggedIn()) {
            prepareTransition();

            presenter.getAcceptedPaymentMethods(savedEntityResults, reloadingWallet);

            textViewSignInMessage.setVisibility(GONE);

            if (card == null && payPal == null && ideal == null && payNearMe == null && incomm == null && googlePay == null && dotPay == null && wallet == null) {
                presenter.loadPaymentMethods(getOrganizationUUIDFromEntities());
            }
            if (selectedPaymentMethods.size() > 1) {
                setCardFields(card);
                setSecondCardFields((Card) selectedPaymentMethods.get(1));
            } else if (card != null) {
                setCardFields(card);
            } else if (ideal != null) {
				setIdeal(ideal);
			} else if (payNearMe != null) {
				setPayNearMe(payNearMe);
			} else if (incomm != null) {
				setIncomm(incomm);
			} else if (payPal != null) {
				setPayPal(payPal);
			} else if (googlePay != null) {
				setGooglePay(googlePay);
			} else if (dotPay != null) {
				setDotPay(dotPay);
			} else if (wallet != null) {
				setWallet(wallet);
			}
		} else {
			showLoginView();
			setVisibilityForSplitPaymentText(GONE);
		}
	}

	@Nullable
	private String getOrganizationUUIDFromEntities() {
		return (savedEntityResults != null && savedEntityResults.size() != 0) ? savedEntityResults.get(0).getOrganization().getLegacyUuid() : null;
	}

	private void showLoginView() {
        prepareTransition();

        textViewPaymentMethods.setText(getString(R.string.payment_text));
        changePayment.setText(R.string.settings_sign_in);
        changePayment.setTag(ACCOUNT);
        changePayment.setContentDescription(getString(R.string.settings_sign_in));
        setVisibilityForSplitPaymentText(VISIBLE);
        linearLayoutPaymentMethods.setVisibility(GONE);
        textViewNoPaymentMethods.setVisibility(GONE);
        textViewSelectTwoPaymentMethods.setVisibility(GONE);
        linearLayoutSecondPaymentMethods.setVisibility(GONE);
        linearLayoutPaymentMethodsLoading.setVisibility(GONE);
        textViewSignInMessage.setVisibility(VISIBLE);
        validatePlaceOrderButton();
    }

	private void setVisibilityForSplitPaymentText(int visibility) {
        if (reloadingWallet != null || visibility == GONE ) {
			splitPaymentSwitch.setVisibility(GONE);
			switchLabel.setVisibility(GONE);
		}else if (visibility == VISIBLE && BytemarkSDK.isLoggedIn() && confHelper.getMaxSplitPayments() > 1) {
			splitPaymentSwitch.setVisibility(VISIBLE);
			switchLabel.setVisibility(VISIBLE);
		}
    }

	@Override
	protected void onOffline() {
        prepareTransition();

        swipeRefreshLayout.setRefreshing(false);
        textViewNoPaymentMethods.setVisibility(GONE);
        linearLayoutPaymentMethods.setVisibility(GONE);
        textViewSelectTwoPaymentMethods.setVisibility(GONE);
        linearLayoutSecondPaymentMethods.setVisibility(GONE);
        linearLayoutPaymentMethodsLoading.setVisibility(GONE);
        textViewSignInMessage.setVisibility(GONE);
        linearLayoutPayment.setVisibility(GONE);
        if(!isZerDollerPurchase()) {
			textViewErrorLoadingPaymentMethod.setVisibility(VISIBLE);
			textViewPaymentMethods.setText(getString(R.string.payment_text));
			changePayment.setText(getString(R.string.popup_retry));
			changePayment.setTag(RETRY);
			changePayment.setContentDescription(getString(R.string.payment_retry_loading_payment_options_voonly));
		}
        setVisibilityForSplitPaymentText(GONE);
        buttonPlaceOrder.setEnabled(false);
        int alphaComponent = setAlphaComponent(confHelper.getAccentThemeBacgroundColor(), 26);
		buttonPlaceOrder.setBackgroundTintList(ColorStateList.valueOf(alphaComponent));
		buttonPlaceOrder.setBackgroundColor(getResources().getColor(R.color.warm_grey));

        if (isIntentForReload && radioGroup.getChildCount() <= 0) {
            showReloadValueFetchError();
        }
	}

	@Override
	protected void setAccentThemeColors() {
		linearLayoutOrderSuccessful.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
		linearLayoutLoadingScreen.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
		for (int i = 0; i < textViewPlacingYourOrder.size(); i++) {
			textViewPlacingYourOrder.get(i).setTextColor(confHelper.getBackgroundThemePrimaryTextColor());
		}
		textViewLoding.setTextColor(confHelper.getBackgroundThemePrimaryTextColor());
		for (int i = 0; i < textViewOrderSuccessful.size(); i++) {
			textViewOrderSuccessful.get(i).setTextColor(confHelper.getBackgroundThemePrimaryTextColor());
		}
		buttonMyTickets.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
		buttonMyTickets.setTextColor(confHelper.getBackgroundThemeSecondaryTextColor());

		progressViewLayoutPlacingYourOrder.setProgressColor(confHelper.getBackgroundThemeAccentColor(), confHelper.getBackgroundThemeBackgroundColor());
		progressViewLayoutPlacingYourOrder.setProgressColor(confHelper.getBackgroundThemeAccentColor(), confHelper.getBackgroundThemeBackgroundColor());
		imageViewPlacingYourOrder.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
		imageViewLoading.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
		imageViewOrderSuccessful.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));

		buttonPlaceOrder.setTextColor(confHelper.getAccentThemePrimaryTextColor());

		buttonApply.setTextColor(confHelper.getAccentThemePrimaryTextColor());
		buttonApply.setBackgroundTintList(ColorStateList.valueOf(confHelper.getAccentThemeBacgroundColor()));

		linearLayoutInfoBar.setBackgroundColor(confHelper.getHeaderThemeAccentColor());
		textViewInfoBar.setTextColor(confHelper.getHeaderThemeSecondaryTextColor());

		infoIcon.setImageTintList(ColorStateList.valueOf(confHelper.getHeaderThemeSecondaryTextColor()));
		changePayment.setTextColor(ColorStateList.valueOf(confHelper.getCollectionThemeAccentColor()));
	}

	@Override
	protected void setBackgroundThemeColors() {
		textViewPaymentMethods.setTextColor(confHelper.getCollectionThemePrimaryTextColor());
		switchLabel.setTextColor(confHelper.getCollectionThemePrimaryTextColor());
		textViewSummary.setTextColor(confHelper.getCollectionThemePrimaryTextColor());
		textViewReload.setTextColor(confHelper.getCollectionThemePrimaryTextColor());
		tvPromoCode.setTextColor(confHelper.getCollectionThemePrimaryTextColor());
		for (View view : viewList)
			view.setBackgroundColor(confHelper.getCollectionThemeBackgroundColor());

	}

	@Override
	protected void setDataThemeColors() {
		for (int i = 0; i < textViewNetworkError.size(); i++) {
			textViewNetworkError.get(i).setTextColor(ColorStateList.valueOf(confHelper.getBackgroundThemePrimaryTextColor()));
		}

		linearLayoutNetworkConnectivityError.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeBackgroundColor()));
		circleBorderImageViewNetwork.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));

		for (int i = 0; i < buttonsNetwork.size(); i++) {
			buttonsNetwork.get(i).setTextColor(ColorStateList.valueOf(confHelper.getBackgroundThemeBackgroundColor()));
			buttonsNetwork.get(i).setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
		}
		for (int i = 0; i < textViews.size(); i++) {
			textViews.get(i).setTextColor(ColorStateList.valueOf(confHelper.getBackgroundThemePrimaryTextColor()));
		}

        linearLayoutEmptyShoppingCart.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeBackgroundColor()));
        circleBorderImageView.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));

        buttonBuyTickets.setTextColor(ColorStateList.valueOf(confHelper.getAccentThemePrimaryTextColor()));
        buttonBuyTickets.setBackgroundTintList(ColorStateList.valueOf(confHelper.getAccentThemeBacgroundColor()));

        linearLayoutPromoCode.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        linearLayoutShoppingCartTotalDetails.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        linearLayoutTotalShoppingCart.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        textViewSignInMessage.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        linearLayoutPaymentMethodsLoading.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        textViewErrorLoadingPaymentMethod.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        linearLayoutPayment.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        linearLayoutPaymentMethods.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        linearLayoutSecondPaymentMethods.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        textViewNoPaymentMethods.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        textViewSelectTwoPaymentMethods.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        linearLayoutSubTotalShoppingCart.setBackgroundColor(confHelper.getDataThemeBackgroundColor());

    }

	@Override
	public void showLoadingPaymentMethods() {
        prepareTransition();

        changePayment.setText(getString(R.string.popup_retry));
        changePayment.setTag(RETRY);
        changePayment.setContentDescription(getString(R.string.payment_retry_loading_payment_options_voonly));
        textViewNoPaymentMethods.setVisibility(GONE);
        textViewSelectTwoPaymentMethods.setVisibility(GONE);
        linearLayoutPaymentMethods.setVisibility(GONE);
        linearLayoutSecondPaymentMethods.setVisibility(GONE);
        linearLayoutPayment.setVisibility(GONE);
        linearLayoutPaymentMethodsLoading.setVisibility(VISIBLE);
    }

	@Override
	public void showPlacingOrderView() {
		prepareTransition();
		allowBackPressOnDiscountCalculation = true;
		hideActionBarAndDisableSwipe();
		linearLayoutLoadingScreen.setVisibility(VISIBLE);
		linearLayoutLoadingScreen.announceForAccessibility(getString(R.string.shopping_cart_placing_your_order));
		linearLayoutOrderSuccessful.setVisibility(GONE);
		linearLayoutEmptyShoppingCart.setVisibility(GONE);
		scrollViewShoppingCart.setVisibility(GONE);
		buttonPlaceOrder.setVisibility(GONE);
	}

	private void hideActionBarAndDisableSwipe() {
		((AppCompatActivity) getActivity()).getSupportActionBar().hide();
		if (reloadingWallet != null) {
			((TabsActivity) getActivity()).hideTabLayout();
			((TabsActivity) getActivity()).disableSwipe();
		}
	}

	@Override
	public void hidePlacingOrderView() {
		showActionBarAndEnableSwipe();
		linearLayoutLoadingScreen.setVisibility(GONE);
		scrollViewShoppingCart.setVisibility(VISIBLE);
		buttonPlaceOrder.setVisibility(VISIBLE);
	}

	@Override
	public void showOrderSuccessful(List<EntityResult> items, PaymentTypes orderType, IncommBarcodeDetail incommBarcodeDetail, String orderUuid) {
		editTextPaymentCVV.setText(null);
		hideBottomSheet();
		prepareTransition();

		isOrderSuccess = true;
		int totalOrderQuantity = 0;
		StringBuilder uuidList = new StringBuilder();
		for (EntityResult entityResult : this.savedEntityResults) {
			totalOrderQuantity += entityResult.getQuantity();
			uuidList.append(entityResult.getUuid());
			uuidList.append("\t");
			if (orderTypeStr.equals(AnalyticsPlatformsContract.OrderType.PASSES)) {
				// upload event for individual products
				Double sum = confHelper.getConfigurationPurchaseOptionsCurrency(entityResult.getQuantity() * entityResult.getSalePrice());
				analyticsPlatformAdapter.productPurchased(entityResult.getUuid(), entityResult.getName(),
						confHelper.getConfigurationPurchaseOptionsCurrency(entityResult.getSalePrice()), sum, entityResult.getQuantity());
			}
		}

		double totalOrderSum = confHelper.getConfigurationPurchaseOptionsCurrency(total);

		analyticsPlatformAdapter.orderPlaced(totalOrderQuantity, totalOrderSum, "",
				orderTypeStr, uuidList.toString(), AnalyticsPlatformsContract.Status.SUCCESS, "");


		hideActionBarAndDisableSwipe();
		this.savedEntityResults.clear();
		sharedPreferences.edit().remove(AppConstants.ENTITY_RESULT_LIST).apply();

		if (orderType == PaymentTypes.IDEAL || orderType == PaymentTypes.DOTPAY) {
			textViewSuccessMessage.setText(R.string.payment_ideal_order_success_message);
		}
		if ((fareMediaUuid != null && !fareMediaUuid.equals("")) || reloadingWallet != null) {
			textViewSuccessTitle.setText(getString(R.string.success));
			textViewSuccessMessage.setText(isIntentForReload ?
					R.string.shopping_cart_value_added_successful_message :
					R.string.shopping_cart_pass_added_successful_message);
			buttonMyTickets.setText(getString(R.string.ok));
		}
		if (orderType == PaymentTypes.INCOMM && !incommBarcodeDetail.getAccountNumber().isEmpty()) {
			Intent intent = new Intent(getActivity(), SelectRetailerActivity.class);
			intent.putExtra(AppConstants.INTENT_INCOMM_BARCODE_DETAIL, incommBarcodeDetail);
			intent.putExtra(AppConstants.INTENT_INCOMM_ORDER_AMOUNT, total);
			intent.putExtra(AppConstants.INTENT_INCOMM_ORDER_UUID, orderUuid);
			if (fareMediaUuid != null) {
				intent.putExtra(AppConstants.INTENT_INCOMM_FAREMEDIUM_LOAD, true);
			}
			if (isIntentForReload) {
				intent.putExtra(AppConstants.INTENT_INCOMM_WALLET_LOAD, true);
				startActivity(intent);
				getActivity().finish();
			} else {
				startActivity(intent);
			}
		} else {
			discountData = null;
			linearLayoutOrderSuccessful.setVisibility(VISIBLE);
			linearLayoutLoadingScreen.setVisibility(GONE);
			linearLayoutEmptyShoppingCart.setVisibility(GONE);
			scrollViewShoppingCart.setVisibility(GONE);
			buttonPlaceOrder.setVisibility(GONE);

			linearLayoutOrderSuccessfulText.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
			linearLayoutOrderSuccessfulText.announceForAccessibility(textViewSuccessTitle.getText());
			linearLayoutOrderSuccessfulText.setContentDescription(textViewSuccessTitle.getText() + " " + textViewSuccessMessage.getText());
			buttonMyTickets.announceForAccessibility(buttonMyTickets.getText());

			buttonMyTickets.postDelayed(() -> {
				buttonMyTickets.requestFocus();
				buttonMyTickets.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
			}, 1200);
		}
	}

	@Override
	public void showPlacingOrderWithError() {
		allowBackPressOnDiscountCalculation = false;
		prepareTransition();
		String uuidList = "";
		if (savedEntityResults != null) {
			uuidList = savedEntityResults.stream().map(EntityResult::getUuid).collect(Collectors.joining(","));
		}
		analyticsPlatformAdapter.orderPlaced(0, 0.0, orderUuid,
				orderTypeStr, uuidList, AnalyticsPlatformsContract.Status.FAILURE, "");
		linearLayoutOrderSuccessful.setVisibility(GONE);
		linearLayoutLoadingScreen.setVisibility(GONE);
		linearLayoutEmptyShoppingCart.setVisibility(GONE);
		scrollViewShoppingCart.setVisibility(GONE);
		buttonPlaceOrder.setVisibility(GONE);
		linearLayoutNetworkConnectivityError.setVisibility(VISIBLE);
	}

	@Override
	public void showPlacingOrderWithError(@Nullable String title, String errors) {

		if (errors.equals("") && !isOnline()) {
			showPlacingOrderWithError();
			return;
		}
		if (title == null) {
			title = getString(R.string.shopping_cart_popup_order_failed);
		}
		prepareTransition();
		String uuidList = "";
		if (savedEntityResults != null) {
			uuidList = savedEntityResults.stream().map(EntityResult::getUuid).collect(Collectors.joining(","));
		}
		analyticsPlatformAdapter.orderPlaced(0, 0.0, "",
				orderTypeStr, uuidList, AnalyticsPlatformsContract.Status.FAILURE, errors);
		showActionBarAndEnableSwipe();
		linearLayoutOrderSuccessful.setVisibility(GONE);
		linearLayoutLoadingScreen.setVisibility(GONE);
		linearLayoutEmptyShoppingCart.setVisibility(GONE);
		scrollViewShoppingCart.setVisibility(VISIBLE);
		buttonPlaceOrder.setVisibility(VISIBLE);
		if (errors.equals("")) {
			errors = getString(R.string.something_went_wrong);
		}
		showSimpleMaterialDialog(title, errors);
	}

	@Override
	public void showPlacingOrderWithDeviceTimeError() {
		prepareTransition();
		showActionBarAndEnableSwipe();
		linearLayoutOrderSuccessful.setVisibility(GONE);
		linearLayoutLoadingScreen.setVisibility(GONE);
		linearLayoutEmptyShoppingCart.setVisibility(GONE);
		scrollViewShoppingCart.setVisibility(VISIBLE);
		buttonPlaceOrder.setVisibility(VISIBLE);

		String uuidList = "";
		if (savedEntityResults != null) {
			uuidList = savedEntityResults.stream().map(EntityResult::getUuid).collect(Collectors.joining(","));
		}
		analyticsPlatformAdapter.orderPlaced(0, 0.0, "",
				orderTypeStr, uuidList, AnalyticsPlatformsContract.Status.FAILURE, getString(R.string.device_clock_off_message));

		showSimpleMaterialDialog(R.string.popup_error, R.string.device_clock_off_message);
	}

	@Override
	public void showCvvRequiredEditTextDialog(final Card card) {
		editTextPaymentCVV.setText("");
		showInfoBar(getString(R.string.payment_incorrect_cvv));
	}

	@Override
	public void showDeviceLostOrStolenErrorDialog() {
		showLoginView();
	}

	@Override
	public void showDeviceLostOrStolenError() {
		showDeviceLostOrStolenErrorDialog();
	}

	@Override
	public Context getActivityContext() {
		return getActivity();
	}

	@Override
	public void showDefaultError(String message) {
		showPlacingOrderWithError();
		showDefaultErrorDialog(message);
	}

	@Override
	public void showNoPaymentMethodsView() {
        isPaymentMethodsListEmpty = true;
        textViewPaymentMethods.setText(getString(R.string.payment_text));
        changePayment.setText(getString(R.string.shopping_cart_add_payment_method));
        changePayment.setTag(ADD);
        changePayment.setContentDescription(getString(R.string.payment_add_payment_method_voonly));
        linearLayoutPaymentMethodsLoading.setVisibility(GONE);
        linearLayoutPaymentMethods.setVisibility(GONE);
        linearLayoutSecondPaymentMethods.setVisibility(GONE);
        linearLayoutPayment.setVisibility(GONE);
        textViewSelectTwoPaymentMethods.setVisibility(GONE);
        textViewNoPaymentMethods.setVisibility(VISIBLE);
        validatePlaceOrderButton();
    }

	@Override
	public void setPaymentMethods(Data data) {
		swipeRefreshLayout.setRefreshing(false);
		selectedPaymentMethods.clear();
		ArrayList<PaymentMethod> paymentMethods = new ArrayList() {
			{
				addAll(data.cards);
				addAll(data.paypalList);
				add(data.googlePay);
				add(data.ideal);
				add(data.dotPay);
				add(data.payNearMe);
				add(data.incomm);
				if (reloadingWallet == null && data.storedWallets != null && data.storedWallets.getWallets() != null) {
					addAll(data.storedWallets.getWallets());
				}
			}
		};
		List<Card> cardArrayList = data.cards;
		Collections.sort(paymentMethods, new PaymentMethodOrderComparator(googlePayReady));
		PaymentMethod defaultPayemntmethod = getDefaultpaymentMethod(paymentMethods);
		if (defaultPayemntmethod != null) {
			String paymentType = "";
			String cardType = "";
			switch (PaymentsClassName.valueOf(defaultPayemntmethod.getClass().getSimpleName())) {
				case Card:
					this.card = (Card) defaultPayemntmethod;
					setCardFields(this.card);
					selectedPaymentMethods.add(this.card);
					cardType = this.card.getTypeName();
					paymentType = AnalyticsPlatformsContract.PaymentType.CARD;
					break;
				case BraintreePaypalPaymentMethod:
					this.payPal = (BraintreePaypalPaymentMethod) defaultPayemntmethod;
					setPayPal((BraintreePaypalPaymentMethod) defaultPayemntmethod);
					paymentType = AnalyticsPlatformsContract.PaymentType.PAYPAL;
					break;
				case GooglePay:
					setGooglePay((GooglePay) defaultPayemntmethod);
					paymentType = AnalyticsPlatformsContract.PaymentType.GOOGLE_PAY;
					break;
				case Ideal:
					setIdeal((Ideal) defaultPayemntmethod);
					paymentType = AnalyticsPlatformsContract.PaymentType.IDEAL;
					break;
				case DotPay:
					setDotPay((DotPay) defaultPayemntmethod);
					paymentType = AnalyticsPlatformsContract.PaymentType.DOT_PAY;
					break;
				case PayNearMe:
					setPayNearMe((PayNearMe) defaultPayemntmethod);
					paymentType = AnalyticsPlatformsContract.PaymentType.PAY_NEAR_ME;
					break;
				case Incomm:
					setIncomm((Incomm) defaultPayemntmethod);
					paymentType = AnalyticsPlatformsContract.PaymentType.INCOMM;
					break;
				case Wallet:
					setWallet((Wallet) defaultPayemntmethod);
					paymentType = AnalyticsPlatformsContract.PaymentType.WALLET;
			}
			analyticsPlatformAdapter.defaultPaymentLoading(paymentType, cardType, AnalyticsPlatformsContract.Status.SUCCESS, "");
		} else {
			showNoPaymentMethodsView();
		}
		validatePlaceOrderButton();
	}

	/**
	 * Returns first payment method of sorted payment methods if accepted or return next accepted Payment Method
	 * If no accepted Payment method is present return non accepted Payment method.
	 **/
	private PaymentMethod getDefaultpaymentMethod(ArrayList<PaymentMethod> paymentMethods) {
		PaymentMethod defaultpaymentMethod = null;
		Card nonAcceptedCard = null;
		paymentMethods.removeAll(Collections.singleton(null));
		if (paymentMethods.size() > 0) {
			for (PaymentMethod paymentMethod : paymentMethods) {
				if (confHelper.isV2PaymentMethodsEnabled() && paymentMethod instanceof Card) {
					if (((Card) paymentMethod).getAccepted()) {
						defaultpaymentMethod = paymentMethod;
						break;
					} else if (nonAcceptedCard == null) {
						nonAcceptedCard = (Card) paymentMethod;
					}
				} else {
					defaultpaymentMethod = paymentMethod;
					break;
				}
			}
			if (defaultpaymentMethod == null) {
				defaultpaymentMethod = nonAcceptedCard;
			}
		}
		return defaultpaymentMethod;
	}

	private void setCardFields(final Card card) {
		if (card != null) {
            prepareTransition();
            hideInfoBar();
            hideCvvError();

            if (isNewTraceRequried())
                traceNumber = generateTraceNumber();
            isPaymentMethodsListEmpty = false;
            textViewPaymentMethods.setText(getString(R.string.payment_text));
            linearLayoutFirstCardAmount.postDelayed(() -> linearLayoutFirstCardAmount.setVisibility(GONE), 200);
            changePayment.setText(getString(R.string.shopping_cart_change_payment));
            changePayment.setTag(CHANGE);
            changePayment.setContentDescription(getString(R.string.payment_change_payment_method_voonly));
            setVisibilityForSplitPaymentText(VISIBLE);

            linearLayoutPaymentMethodsLoading.setVisibility(GONE);
            textViewNoPaymentMethods.setVisibility(GONE);
            textViewSelectTwoPaymentMethods.setVisibility(GONE);
            linearLayoutPayment.setVisibility(GONE);
            linearLayoutPaymentMethods.setVisibility(VISIBLE);

            imageViewCardType.postDelayed(() -> {
                checkCardType(card.getTypeName(), imageViewCardType);
            }, 200);

            imageViewCardType.setColorFilter(null);
            linearLayoutPaymentMethods.setOnClickListener(null);
            if (confHelper.isV2PaymentMethodsEnabled() && !card.getAccepted()) {
				ColorMatrix colorMatrix = new ColorMatrix();
				colorMatrix.setSaturation(0);
				ColorMatrixColorFilter filter = new ColorMatrixColorFilter(colorMatrix);
				imageViewCardType.setColorFilter(filter);
				linearLayoutPaymentMethods.setOnClickListener(v ->
						Snackbar.make(v, R.string.payment_payment_type_not_accepted, Snackbar.LENGTH_LONG).show());
			}

			if (card.getNickname().isEmpty()) {
				textViewCardLabel.setText(card.getTypeName());
			} else {
				textViewCardLabel.setText(card.getNickname());
			}
			textViewCardLabel.setTextColor(ColorStateList.valueOf(confHelper.getDataThemePrimaryTextColor()));
			textViewCardLabel.requestFocus();

			String cardNumber;
			cardNumber = String.format("*%s", card.getLastFour());

			textViewCardNumber.setText(cardNumber);
			textViewCardNumber.setContentDescription(getString(R.string.payment_card_ending_with_voonly) + card.getLastFour());
			textViewCardNumber.setTextColor(ColorStateList.valueOf(confHelper.getDataThemePrimaryTextColor()));

//            String[] expirationDateSplit = card.getExpirationDate().split("/");
//            char[] yearArray = expirationDateSplit[1].toCharArray();
//            String expirationDate = expirationDateSplit[0] + "/" + yearArray[2] + yearArray[3];
			String cardExpiration = String.format(getString(R.string.shopping_cart_exp), card.getExpirationDate());

			textViewCardExpiration.setText(cardExpiration);
			textViewCardExpiration.setContentDescription(getString(R.string.payment_card_expiry_is_voonly) + card.getExpirationDate());
			//textViewCardExpiration.setTextColor(ColorStateList.valueOf(confHelper.getDataThemePrimaryTextColor()));

			TransitionManager.beginDelayedTransition(linearLayoutPaymentMethods);

			if (card.getCvvRequired()) {
				linearLayoutPaymentCVV.setVisibility(VISIBLE);
				validateAndSaveCardCvv(editTextPaymentCVV, null, card);
			} else {
				linearLayoutPaymentCVV.setVisibility(GONE);
			}

			editTextPaymentCVV.clearFocus();
			if (isNewTraceRequried())
				editTextPaymentCVV.setText(null);
			hideCvvError();
		}
	}

	private void setSecondCardFields(final Card card) {
		if (card != null) {
			prepareTransition();
			hideInfoBar();
			hideCvvError();

			setVisibilityForSplitPaymentText(VISIBLE);

			linearLayoutPayment.setVisibility(GONE);
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					linearLayoutFirstCardAmount.setVisibility(VISIBLE);
					editTextFirstCardAmount.requestFocus();
				}
			}, 200);

			linearLayoutSecondPaymentMethods.setVisibility(VISIBLE);

			checkCardType(card.getTypeName(), imageViewSecondCardType);

			if (card.getNickname().isEmpty()) {
				textViewSecondCardLabel.setText(card.getTypeName());
			} else {
				textViewSecondCardLabel.setText(card.getNickname());
			}
			textViewSecondCardLabel.setTextColor(ColorStateList.valueOf(confHelper.getDataThemePrimaryTextColor()));

			String cardNumber;
			cardNumber = String.format("*%s", card.getLastFour());

			textViewSecondCardNumber.setText(cardNumber);
			textViewSecondCardNumber.setContentDescription(getString(R.string.payment_card_ending_with_voonly) + card.getLastFour());
			textViewSecondCardNumber.setTextColor(ColorStateList.valueOf(confHelper.getDataThemePrimaryTextColor()));

//            String[] expirationDateSplit = card.getExpirationDate().split("/");
//            char[] yearArray = expirationDateSplit[1].toCharArray();
//            String expirationDate = expirationDateSplit[0] + "/" + yearArray[2] + yearArray[3];
			String cardExpiration = String.format(getString(R.string.shopping_cart_exp), card.getExpirationDate());

			textViewSecondCardExpiration.setText(cardExpiration);
			textViewSecondCardExpiration.setContentDescription(getString(R.string.payment_card_expiry_is_voonly) + card.getExpirationDate());
			//textViewCardExpiration.setTextColor(ColorStateList.valueOf(confHelper.getDataThemePrimaryTextColor()));

			TransitionManager.beginDelayedTransition(linearLayoutSecondPaymentMethods);

			if (card.getCvvRequired()) {
				linearLayoutSecondPaymentCVV.setVisibility(VISIBLE);
				validateAndSaveCardCvv(editTextSecondPaymentCVV, null, card);
			} else {
				linearLayoutSecondPaymentCVV.setVisibility(GONE);
			}

			editTextSecondPaymentCVV.clearFocus();
			editTextSecondPaymentCVV.setText(null);
			hideSecondCvvError();
		}
	}

	private void validateAndSaveCardCvv(final EditText editTextPaymentCVV, final View positiveAction, final Card card) {
		final CardCvvValidator cardCvvValidator;
		if (card.getTypeId() == 3) {
			cardCvvValidator = new CardCvvValidator(4);
		} else {
			cardCvvValidator = new CardCvvValidator(3);
		}

		editTextPaymentCVV.setFilters(new InputFilter[]{
				new DigitsKeyListener(), cardCvvValidator
		});

		editTextPaymentCVV.addTextChangedListener(cardCvvValidator);
		editTextPaymentCVV.addTextChangedListener(new TextWatcherHelper() {
			@Override
			public void afterTextChanged(Editable editable) {
				validatePlaceOrderButton();
				if (editable == editTextPaymentCVV.getText()) {
					if (cardCvvValidator.hasFullLengthText()) {
						hideInfoBar();
						hideCvvError();
						card.setCvv(editTextPaymentCVV.getText().toString());
						if (positiveAction != null) {
							positiveAction.setEnabled(true);
						}
					} else {
						card.setCvv(null);
						if (positiveAction != null) {
							positiveAction.setEnabled(false);
						}
					}
				} else {
					card.setCvv(null);
					if (positiveAction != null) {
						positiveAction.setEnabled(false);
					}
				}
			}
		});
	}

	private void checkCardType(String paymentCardType, ImageView imageViewCardType) {
        PaymentCard paymentCard = PaymentCard.Companion.fromPaymentCardType(paymentCardType);
        if (ExtensionsKt.isValidContext(getContext())) {
            Glide.with(getContext())
                    .load(paymentCard.getTypeImage())
                    .crossFade()
                    .skipMemoryCache(false)
                    .into(imageViewCardType);
        }
    }

	@Override
	public void showSessionExpiredError(String message) {
		showSimpleMaterialDialog(R.string.popup_error, message);
	}

	@Override
	public void initializeBrainTree(String payPalToken) {
		try {
			brainTreeFragment = BraintreeFragment.newInstance(getActivity(), payPalToken);
			payPal.setPayPalNonce(payPalToken);
		} catch (InvalidArgumentException e) {
			// There was an issue with your authorization string.
		}
	}

	@Override
	public void showLoadingPaymentMethodsErrorDialog() {
		getProductEntityListFromSharedPrefs();
		allowBackPressOnDiscountCalculation = false;
		analyticsPlatformAdapter.defaultPaymentLoading("", "", AnalyticsPlatformsContract.Status.FAILURE, "");
		if(getActivity().getIntent().getStringExtra(AppConstants.DEEPLINK_JWT_TOKEN) != null
				&& discountData == null ) {
			new MaterialDialog.Builder(getContext())
					.title(R.string.popup_error)
					.content(R.string.report_connection_error_message)
					.positiveText(R.string.ok)
					.positiveColor(confHelper.getDataThemeAccentColor())
					.cancelable(false)
					.onPositive((dialog, which) ->dialog.dismiss())
					.show();
		} else {
			new MaterialDialog.Builder(getContext())
					.title(R.string.payment_popup_error_loading_title)
					.content(R.string.payment_popup_error_loading_message)
					.positiveText(R.string.popup_retry)
					.positiveColor(confHelper.getDataThemeAccentColor())
					.negativeText(R.string.popup_cancel)
					.cancelable(false)
					.onPositive((dialog, which) -> onOnline())
					.onNegative((dialog, which) -> {
						if (!isOnline()) {
							onOffline();
						}
						dialog.dismiss();
					})
					.show();
		}
	}

	@Override
	public void showLoadingPaymentMethodsErrorDialog(String errors) {
		allowBackPressOnDiscountCalculation = false;
		getProductEntityListFromSharedPrefs();
		analyticsPlatformAdapter.defaultPaymentLoading("", "", AnalyticsPlatformsContract.Status.FAILURE, errors);
		new MaterialDialog.Builder(getContext())
				.title(R.string.payment_popup_error_loading_title)
				.content(errors)
				.positiveText(R.string.popup_retry)
				.positiveColor(confHelper.getDataThemeAccentColor())
				.negativeText(R.string.popup_cancel)
				.onPositive((dialog, which) -> refreshNetworkActivity())
				.onNegative((dialog, which) -> dialog.dismiss())
				.show();
	}

	@Override
	public void showDeviceTimeErrorDialog() {
		getProductEntityListFromSharedPrefs();
		showSimpleMaterialDialog(R.string.popup_error, R.string.device_clock_off_message);
	}

	@OnClick(R.id.btn)
	public void onButtonClick() {
		hideBottomSheet();
		int tag = (int) changePayment.getTag();
		switch (tag) {
			case ADD:
				navigateAwayReason = ADD;
				if (isAddPaymentMehodsScreenPresent()) {
					Intent intent = new Intent(getContext(), AddPaymentMethodsActivity.class);
					intent.putStringArrayListExtra(AppConstants.ACCEPTED_PAYMENTS, acceptedPaymentMethods);
					startActivityForResult(intent, AppConstants.ADD_PAYMENT_METHOD_REQUEST_CODE);
                } else {
                    Intent intent = new Intent(getContext(), AddPaymentCardActivity.class);
                    intent.putExtra(AppConstants.SHOPPING_CART_INTENT, true);
                    startActivityForResult(intent, AppConstants.ADD_CARD_REQUEST_CODE);
                }
                analyticsPlatformAdapter.addPaymentSelected(AnalyticsPlatformsContract.Screen.CHECKOUT);
                break;
            case CHANGE:
                navigateAwayReason = CHANGE;
                Intent selectCardIntent = new Intent(getContext(), PaymentMethodsActivity.class);
                selectCardIntent.putExtra(AppConstants.ORG_UUID, getOrganizationUUIDFromEntities());
                selectCardIntent.putExtra(AppConstants.SHOPPING_CART_INTENT, true);
                if (isSplitPaymentEnabled) {
                    selectCardIntent.putExtra(AppConstants.SPLIT_PAYMENT, true);
                    selectCardIntent.putParcelableArrayListExtra(AppConstants.SELECTED_CARD, getSelectedCards());
                } else {
                    selectCardIntent.putExtra(AppConstants.RELOADING_WALLET, reloadingWallet != null);
                }
                startActivityForResult(selectCardIntent, AppConstants.PAYMENT_METHODS_REQUEST_CODE);
                break;
            case RETRY:
                refreshNetworkActivity();
                break;
            case ACCOUNT:
                navigateAwayReason = ACCOUNT;
                if (confHelper.isAuthenticationNative()) {
                    startAuthenticationActivity();
                } else {
					getActivity().startActivityForResult(new Intent(getActivity(), MainActivity.class), MasterActivity.REQUEST_CODE_LOGIN);
				}
				break;
		}
	}

	private void startAuthenticationActivity() {
		Intent intent = new Intent(getActivity(), AuthenticationActivity.class);
		intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.CHECKOUT);
		getActivity().startActivityForResult(intent, MasterActivity.REQUEST_CODE_LOGIN);
	}

	private ArrayList<PaymentMethod> getSelectedCards() {
		ArrayList<PaymentMethod> selectedCards = new ArrayList<>();
		for (PaymentMethod paymentMethod : selectedPaymentMethods) {
			if (paymentMethod instanceof Card)
				selectedCards.add((Card) paymentMethod);
		}
		return selectedCards;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		bottomSheet.setVisibility(GONE);
		hideInfoBar();
		switch (requestCode) {
			case AppConstants.PAYMENT_METHODS_REQUEST_CODE:
				if (data != null) {
					ArrayList<PaymentMethod> deletedPaymentMethods = data.getParcelableArrayListExtra(AppConstants.DELETED_PAYMENT);
					if (deletedPaymentMethods != null && deletedPaymentMethods.size() > 0) {
						if (deletedPaymentMethods.size() > 1) {
							for (PaymentMethod paymentMethod : deletedPaymentMethods) {
								if (selectedPaymentMethods.contains(paymentMethod))
									selectedPaymentMethods.remove(paymentMethod);
							}
							if (selectedPaymentMethods.size() > 0 && selectedPaymentMethods.get(0) instanceof Card)
								card = (Card) selectedPaymentMethods.get(0);
							else
								card = null;
							presenter.loadPaymentMethods(getOrganizationUUIDFromEntities());
						} else {
							if (deletedPaymentMethods.get(0) instanceof Card) {
								if (selectedPaymentMethods.contains(deletedPaymentMethods.get(0))) {
									selectedPaymentMethods.remove(deletedPaymentMethods.get(0));
									if (selectedPaymentMethods.size() > 0 && selectedPaymentMethods.get(0) instanceof Card) {
										card = (Card) selectedPaymentMethods.get(0);
										linearLayoutSecondPaymentMethods.setVisibility(GONE);
										linearLayoutFirstCardAmount.setVisibility(GONE);
										setCardFields(card);
									} else {
										card = null;
										presenter.loadPaymentMethods(getOrganizationUUIDFromEntities());
									}
								}
							} else if (deletedPaymentMethods.get(0) instanceof BraintreePaypalPaymentMethod) {
								if (payPal != null && payPal.getToken().equals(((BraintreePaypalPaymentMethod) deletedPaymentMethods.get(0)).getToken())) {
									payPal = null;
									presenter.loadPaymentMethods(getOrganizationUUIDFromEntities());
								}
							}
						}
					} else {
						String paymentType = "";
						selectedPaymentMethods = data.getParcelableArrayListExtra(AppConstants.SELECTED_CARD);
						if (selectedPaymentMethods != null && selectedPaymentMethods.size() == 1) {
							PaymentMethod paymentMethod = selectedPaymentMethods.get(0);

							if (!(paymentMethod instanceof GooglePay && !googlePayReady)) {
								linearLayoutSecondPaymentMethods.setVisibility(GONE);
								linearLayoutFirstCardAmount.setVisibility(GONE);
								setCardFields(card);
								paymentType = AnalyticsPlatformsContract.PaymentType.GOOGLE_PAY;
							}

							if (paymentMethod instanceof Card) {
								card = (Card) paymentMethod;
								ideal = null;
								payPal = null;
								googlePay = null;
								payNearMe = null;
								incomm = null;
								dotPay = null;
								wallet = null;
								setCardFields(card);
								paymentType = AnalyticsPlatformsContract.PaymentType.CARD;
							} else if (paymentMethod instanceof BraintreePaypalPaymentMethod) {
								card = null;
								ideal = null;
								googlePay = null;
								payNearMe = null;
								incomm = null;
								dotPay = null;
								wallet = null;
								BraintreePaypalPaymentMethod braintreePaypalPaymentMethod = (BraintreePaypalPaymentMethod) paymentMethod;
								setPayPal(braintreePaypalPaymentMethod);
								presenter.getPayPalToken();
								paymentType = AnalyticsPlatformsContract.PaymentType.PAYPAL;
							} else if (paymentMethod instanceof Ideal) {
								card = null;
								payPal = null;
								googlePay = null;
								payNearMe = null;
								incomm = null;
								dotPay = null;
								wallet = null;
								Ideal ideal = (Ideal) paymentMethod;
								setIdeal(ideal);
								paymentType = AnalyticsPlatformsContract.PaymentType.IDEAL;
							} else if (paymentMethod instanceof PayNearMe) {
								card = null;
								payPal = null;
								googlePay = null;
								incomm = null;
								ideal = null;
								dotPay = null;
								wallet = null;
								setPayNearMe((PayNearMe) paymentMethod);
								paymentType = AnalyticsPlatformsContract.PaymentType.PAY_NEAR_ME;
							} else if (paymentMethod instanceof Incomm) {
								card = null;
								payPal = null;
								googlePay = null;
								payNearMe = null;
								ideal = null;
								dotPay = null;
								wallet = null;
								setIncomm((Incomm) paymentMethod);
								paymentType = AnalyticsPlatformsContract.PaymentType.INCOMM;
							} else if (paymentMethod instanceof DotPay) {
								card = null;
								payPal = null;
								googlePay = null;
								ideal = null;
								payNearMe = null;
								incomm = null;
								wallet = null;
								setDotPay((DotPay) paymentMethod);
								paymentType = AnalyticsPlatformsContract.PaymentType.DOT_PAY;
							} else if (paymentMethod instanceof Wallet) {
								card = null;
								payPal = null;
								googlePay = null;
								ideal = null;
								payNearMe = null;
								incomm = null;
								dotPay = null;
								setWallet((Wallet) paymentMethod);
								paymentType = AnalyticsPlatformsContract.PaymentType.WALLET;
							} else if (paymentMethod instanceof GooglePay) {
								if (googlePayReady) {
									card = null;
									payPal = null;
									ideal = null;
									payNearMe = null;
									incomm = null;
									dotPay = null;
									wallet = null;
									googlePay = (GooglePay) paymentMethod;
									showGooglePayReady();
								} else
									showGooglePayNotReady();
								paymentType = AnalyticsPlatformsContract.PaymentType.GOOGLE_PAY;
							}
						} else if (selectedPaymentMethods != null && selectedPaymentMethods.size() == 2) {
							card = (Card) selectedPaymentMethods.get(0);
							Card secondCard = (Card) selectedPaymentMethods.get(1);
							ideal = null;
							payPal = null;
							googlePay = null;
							payNearMe = null;
							incomm = null;
							dotPay = null;
							wallet = null;
							setCardFields(card);
							setSecondCardFields(secondCard);
							paymentType = AnalyticsPlatformsContract.PaymentType.CARD;
						}
						String source = AnalyticsPlatformsContract.Screen.ORDER;
						if (isIntentForReload) {
							if (fareMediaUuid != null) {
								source = AnalyticsPlatformsContract.Screen.FARE_MEDIUM;
							} else {
								source = AnalyticsPlatformsContract.Screen.WALLET;
							}
						}
						analyticsPlatformAdapter.paymentMethodChanged(source, paymentType);
					}
				}
				break;
			case AppConstants.ADD_PAYMENT_METHOD_REQUEST_CODE:
			case AppConstants.ADD_CARD_REQUEST_CODE:
				if (selectedPaymentMethods.size() == 0) {
					presenter.loadPaymentMethods(getOrganizationUUIDFromEntities());
				}
				if (resultCode == AppConstants.ADD_CARD_SUCCESS_CODE && confHelper.isCreditCardAlertMessageEnabled()) {
					showAddCardSuccessDialog();
				}
				break;
			case AppConstants.WEB_VIEW_PAYMENT_METHODS_REQUEST_CODE:
				switch (resultCode) {
					case Activity.RESULT_OK:
						final String authResult = data.getStringExtra(AppConstants.PAYMENT_RESULT);
						if (authResult != null) {
							switch (authResult) {
								case AppConstants.RESULT_AUTHORIZED:
									showOrderSuccessful(savedEntityResults, PaymentTypes.IDEAL, null, null);
									break;
								case AppConstants.RESULT_REFUSED:
									webViewTransactionFailed(getString(R.string.payment_refused), getString(R.string.payment_order_refused_message));
									break;
								case AppConstants.RESULT_PENDING:
									webViewTransactionFailed(getString(R.string.payment_pending), getString(R.string.payment_order_pending_message));
									break;
								default:
									webViewTransactionFailed(getString(R.string.payment_cancelled), getString(R.string.payment_order_cancelled_message));
							}
						} else {
							webViewTransactionFailed(getString(R.string.payment_cancelled), getString(R.string.payment_order_cancelled_message));
						}
						break;
					case Activity.RESULT_CANCELED:
						webViewTransactionFailed(getString(R.string.payment_cancelled), getString(R.string.payment_order_cancelled_message));
						break;
				}
				break;
			case AppConstants.GOOGLE_LOAD_PAYMENT_DATA_REQUEST_CODE:
				switch (resultCode) {
					case Activity.RESULT_OK:
						PaymentData paymentData = PaymentData.getFromIntent(data);
						String token = paymentData.getPaymentMethodToken().getToken();
						Google3DSPaylaod google3DSPaylaod = gson.fromJson(token, Google3DSPaylaod.class);
						google3DSPaylaod.setData(google3DSPaylaod.getSignedMessage());
						google3DSPaylaod.setVersion(google3DSPaylaod.getProtocolVersion());
						google3DSPaylaod.setSignedMessage(null);
						google3DSPaylaod.setProtocolVersion(null);
						googlePay.setGoogle3DSPaylaod(google3DSPaylaod);
						proceedToPostOrder();
						break;
					case Activity.RESULT_CANCELED:
						break;
					case AutoResolveHelper.RESULT_ERROR:
						Status status = AutoResolveHelper.getStatusFromIntent(data);
						Timber.d(status.getStatusCode() + status.getStatusMessage());
						break;
					default:
				}
				break;
			case AppConstants.PAY_NEAR_ME_WEBVIEW_REQUEST_CODE:
				//Paynear me result code is not handled.
				clearCartAndNavigateTo(new Intent(getContext(), UseTicketsActivity.class));
				break;
			case MasterActivity.REQUEST_CODE_LOGIN:
				if (BytemarkSDK.isLoggedIn() && defaultPaymentMethod == null) {
					if(isDeepLinkDiscountInitiatedOnLogout) {
						swipeRefreshLayout.setVisibility(VISIBLE);
						isDeepLinkDiscountInitiatedOnLogout = false;
						presenter.discountCalculation(getActivity().getIntent().getParcelableExtra(AppConstants.DEEPLINK_DISCOUNT), getActivity().getIntent().getStringExtra(AppConstants.DEEPLINK_JWT_TOKEN));
					}
					if (!isZerDollerPurchase()) {
						linearLayoutPaymentMethod.setVisibility(VISIBLE);
						updatePaymentLinearLayout.setVisibility(VISIBLE);
						loadPaymentMethodsIfLoggedIn();
					}
				}
				break;
		}
	}

	private void showAddCardSuccessDialog() {
		new MaterialDialog.Builder(getContext())
				.title(R.string.payment_popup_new_card_added_title)
				.content(R.string.payment_popup_new_card_added_message)
				.positiveText(R.string.bt_confirm)
				.positiveColor(confHelper.getDataThemeAccentColor())
				.onPositive((dialog, which) -> dialog.dismiss())
				.show();
	}

	private void handlePromoCodeLayout() {
		if (!isIntentForReload && confHelper.isPromotionalCodesAllowed()) {
			tvPromoCode.setVisibility(VISIBLE);
			linearLayoutPromoCode.setVisibility(VISIBLE);
		}else{
			tvPromoCode.setVisibility(GONE);
			linearLayoutPromoCode.setVisibility(GONE);
		}
	}

	private void webViewTransactionFailed(String title, String errorMessage) {
		hideBottomSheet();
		buttonPlaceOrder.setVisibility(VISIBLE);
		showPlacingOrderWithError(title, errorMessage);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
		if (!isOrderSuccess) {
			getProductEntityListFromSharedPrefs();
			if (!isIntentForReload && discountData == null) {
				updateTotalDetails();
				if (offerServe != null) {
					updateSummarywithDiscount(offerServe);
				}
			}
		}
	}

	@OnClick(R.id.buyTicketsEmptyCartButton)
	public void onBuyTicketsButtonClick() {
		getActivity().finish();
	}

	@OnClick(R.id.buttonApply)
	public void onPromoCodeApplyButtonClick() {
		if (!isOnline()) {
			showSimpleMaterialDialog(R.string.payment_popup_promo_code_failed, R.string.network_connectivity_error_message);
			return;
		}
		if (!BytemarkSDK.isLoggedIn()) {
			showSigninMaterialDialog(R.string.sign_in_required, R.string.sign_in_required_message_promo_popup);
			return;
		}
		buttonApply.setVisibility(GONE);
		presenter.applyPromoCode(editTextPromoCode.getText().toString(), savedEntityResults);
	}

	@OnClick(R.id.imageViewCross)
	public void onPromoCodeRemoveClick() {
		new MaterialDialog.Builder(getContext())
				.title(R.string.shopping_cart_popup_remove_promo_code)
				.content(R.string.shopping_cart_popup_remove_promo_message)
				.positiveText(R.string.v3_popup_yes)
				.positiveColor(confHelper.getDataThemeAccentColor())
				.onPositive((dialog, which) -> {
					if(isZerDollerPurchase()) {
						linearLayoutPaymentMethod.setVisibility(VISIBLE);
						updatePaymentLinearLayout.setVisibility(VISIBLE);
						loadPaymentMethodsIfLoggedIn();
					}
					removePromoCode();
					dialog.dismiss();
				})
				.negativeText(R.string.v3_popup_no)
				.show();
	}

	private void removePromoCode() {
		this.offerServe = null;
		updateTotalDetails();
		linearLayoutPromoApplied.setVisibility(GONE);
		linearLayoutPromoCode.setVisibility(VISIBLE);
		buttonApply.setVisibility(VISIBLE);
		llDiscountShoppingCart.setVisibility(View.GONE);
		if(!isOnline()) {
			int alphaComponent = setAlphaComponent(confHelper.getAccentThemeBacgroundColor(), 26);
			buttonPlaceOrder.setBackgroundTintList(ColorStateList.valueOf(alphaComponent));
			buttonPlaceOrder.setBackgroundColor(getResources().getColor(R.color.warm_grey));
		}
	}

	@Override
	public void setPromoCodeProgressViewVisibility(int visibility) {
		progressViewLayoutLoading.setVisibility(visibility);
	}

	@Override
	public void showPromoCodeSuccess(OfferServe offerServe) {
		this.offerServe = offerServe;
		hideKeyboard();
		linearLayoutPromoCode.setVisibility(GONE);
		linearLayoutPromoApplied.setVisibility(VISIBLE);
		textPromoCode.setText(offerServe.getOfferConfig().getOfferCodes().get(0).getText());
		updateSummarywithDiscount(offerServe);
		analyticsPlatformAdapter.promoCodeApplied(AnalyticsPlatformsContract.Screen.ORDER,
				editTextPromoCode.getText().toString(), AnalyticsPlatformsContract.Status.SUCCESS, "");
	}

	private void updateSummarywithDiscount(OfferServe offerServe) {
		total = subTotal - presenter.getDiscountPrice(offerServe, savedEntityResults);
		tvDiscountTotal.setText("-" + confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
				presenter.getDiscountPrice(offerServe, savedEntityResults)));
		llDiscountShoppingCart.setVisibility(VISIBLE);
		updateSummaryViews();
	}
	private void updateSummarywithDiscountFromdeeplink() {
		if(discountData != null) {
			tvDiscountTotal.setText("-" + confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
					discountData.getOriginalPrice() - discountData.getDiscountPrice()));
			total = discountData.getDiscountPrice();
			llDiscountShoppingCart.setVisibility(VISIBLE);
		}
		updateSummaryViews();
	}
	private void updateSummaryViews(){
		textViewTotal.setText(String.format("%s", confHelper.getConfigurationPurchaseOptionsCurrencySymbol(total)));
		linearLayoutTotalShoppingCart.setContentDescription(getString(R.string.shopping_cart_total_amount_is_voonly) + confHelper.getConfigurationPurchaseOptionsCurrencySymbol(total));
		if(isZerDollerPurchase()) {
			linearLayoutPaymentMethod.setVisibility(GONE);
			textViewErrorLoadingPaymentMethod.setVisibility(GONE);
			textViewSelectTwoPaymentMethods.setVisibility(GONE);
			textViewSignInMessage.setVisibility(GONE);
			textViewNoPaymentMethods.setVisibility(GONE);
			linearLayoutPaymentMethodsLoading.setVisibility(GONE);
			linearLayoutSecondPaymentMethods.setVisibility(GONE);
			updatePaymentLinearLayout.setVisibility(GONE);
			linearLayoutPaymentMethods.setVisibility(GONE);
			linearLayoutPayment.setVisibility(GONE);
			imageViewStrip.setVisibility(GONE);
		}
	}

	@Override
	public void showPromoCodeError(String message) {
		showDefaultErrorDialog(message);
		analyticsPlatformAdapter.promoCodeApplied(AnalyticsPlatformsContract.Screen.ORDER,
				editTextPromoCode.getText().toString(), AnalyticsPlatformsContract.Status.FAILURE, message);
		buttonApply.setVisibility(VISIBLE);
	}

	@OnClick(R.id.buttonRetryReload)
	public void retryAutoload() {
		fetchLoadValues();
	}

	@OnClick(R.id.btn_place_order)
	public void onPlaceOrderButtonClick() {
		updateShoppingCart(savedEntityResults, true);
		placeTheOrder();

	}

	@OnClick(R.id.btn_retry)
	public void onRetryButtonClick() {
		allowBackPressOnDiscountCalculation = true;

		if (confHelper.isUsingOldOrdersEndPoint())
			updateShoppingCart(savedEntityResults, false);
		else
			updateShoppingCart(savedEntityResults, true);

		placeTheOrder();
	}

	void placeTheOrder() {
		hideKeyboard();
		if (isOnline()) {
			if (BytemarkSDK.isLoggedIn()) {
				if (total == 0 && !confHelper.isPaymentMandatory()) {
					proceedToPostOrder();
				} else if (!isPaymentMethodsListEmpty) {
					if (card != null || ideal != null || payPal != null || googlePay != null ||
							payNearMe != null || incomm != null || dotPay != null || wallet != null) {
						Card secondCard = null;
						if (selectedPaymentMethods.size() > 1) {
							secondCard = (Card) selectedPaymentMethods.get(1);
						}
						if (card != null && secondCard != null && card.getCvvRequired() && secondCard.getCvvRequired()) {
							if (card.getCvv() != null && secondCard.getCvv() != null) {
								proceedToPostOrder();
							} else {
								linearLayoutNetworkConnectivityError.setVisibility(GONE);
								getProductEntityListFromSharedPrefs();
								showCvvError();
								showInfoBar(getString(R.string.payment_please_type_your_cvv));
							}
						} else if (card != null && card.getCvvRequired()) {
							if (card.getCvv() != null) {
								proceedToPostOrder();
							} else {
								linearLayoutNetworkConnectivityError.setVisibility(GONE);
								getProductEntityListFromSharedPrefs();
								showCvvError();
								showInfoBar(getString(R.string.payment_please_type_your_cvv));
							}
						} else if (payPal != null) {
							if (!TextUtils.isEmpty(payPal.getToken()) || payPal.getPayPalNonce() != null) {
								proceedToPostOrder();
							} else {
								PayPal.authorizeAccount(brainTreeFragment);
							}
							linearLayoutNetworkConnectivityError.setVisibility(GONE);
						} else if (googlePay != null) {
							if (confHelper.getGooglePayConfiguration() != null) {
								PaymentDataRequest request = presenter.getPaymentDataRequest(String.valueOf(total));
								if (request != null) {
									AutoResolveHelper.resolveTask(
											presenter.getPaymentClient().loadPaymentData(request),
											getActivity(),
											AppConstants.GOOGLE_LOAD_PAYMENT_DATA_REQUEST_CODE);
								}
							} else {
								showGooglePayNotReady();
							}
						} else {
							linearLayoutNetworkConnectivityError.setVisibility(GONE);
							proceedToPostOrder();
						}
					} else {
						showSimpleMaterialDialog(R.string.shopping_cart_popup_order_failed, R.string.payment_popup_order_failed_message);
					}
				} else {
					new MaterialDialog.Builder(getContext())
							.title(R.string.payment_popup_required)
							.content(R.string.payment_popup_please_add)
							.positiveText(R.string.add)
							.positiveColor(confHelper.getDataThemeAccentColor())
							.negativeText(R.string.popup_cancel)
							.cancelable(false)
							.onPositive((dialog, which) -> {
								if (isAddPaymentMehodsScreenPresent()) {
									Intent intent = new Intent(getContext(), AddPaymentMethodsActivity.class);
									intent.putExtra(AppConstants.ORG_UUID, getOrganizationUUIDFromEntities());
									intent.putStringArrayListExtra(AppConstants.ACCEPTED_PAYMENTS, acceptedPaymentMethods);
									startActivityForResult(intent, AppConstants.ADD_PAYMENT_METHOD_REQUEST_CODE);
								} else {
									Intent intent = new Intent(getContext(), AddPaymentCardActivity.class);
									intent.putExtra(AppConstants.SHOPPING_CART_INTENT, true);
									startActivityForResult(intent, AppConstants.ADD_CARD_REQUEST_CODE);
								}
							})
							.onNegative((dialog, which) -> dialog.dismiss())
							.show();
				}
			} else {
				showSigninMaterialDialog(R.string.sign_in_required, R.string.sign_in_required_message);
			}
		} else {
			showSimpleMaterialDialog(R.string.shopping_cart_popup_order_failed, R.string.network_connectivity_error_message);
		}
	}

	private void showSigninMaterialDialog(@StringRes int title, @StringRes int content) {
		new MaterialDialog.Builder(getContext())
				.title(title)
				.content(content)
				.positiveText(R.string.settings_sign_in)
				.positiveColor(confHelper.getDataThemeAccentColor())
				.negativeText(R.string.popup_cancel)
				.cancelable(!isDeepLinkDiscountInitiatedOnLogout)
				.onPositive((dialog, which) -> {
					dialog.dismiss();
					if (confHelper.isNativeLogin()) {
						startAuthenticationActivity();
					} else {
						getActivity().startActivityForResult(new Intent(getActivity(), MainActivity.class), MasterActivity.REQUEST_CODE_LOGIN);
					}
				})
				.onNegative((dialog, which) -> {
					if(isDeepLinkDiscountInitiatedOnLogout) {
						swipeRefreshLayout.setVisibility(VISIBLE);
						buttonApply.setEnabled(true);
						buttonApply.setAlpha(1f);
						isDeepLinkDiscountInitiatedOnLogout = false;
					}
					dialog.dismiss();
				})
				.show();
	}

	private void showSimpleMaterialDialog(@StringRes int title, @StringRes int content) {
		showSimpleMaterialDialog(title, getString(content));
	}

	private void showSimpleMaterialDialog(@StringRes int title, String content) {
		showSimpleMaterialDialog(getString(title), content);
	}

	private void showSimpleMaterialDialog(String title, String content) {
        new MaterialDialog.Builder(getContext())
                .title(title)
                .content(content)
                .positiveText(R.string.ok)
                .cancelable(false)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();
    }

    @OnClick({R.id.splitPaymentSwitch})
    public void onClickSplitPayments() {
        isSplitPaymentEnabled = splitPaymentSwitch.isChecked();
        if (splitPaymentSwitch.isChecked()) {
            showSelectTwoPaymentMethodsView();
            Intent selectCardIntent = new Intent(getContext(), PaymentMethodsActivity.class);
            selectCardIntent.putExtra(AppConstants.ORG_UUID, getOrganizationUUIDFromEntities());
            selectCardIntent.putExtra(AppConstants.SHOPPING_CART_INTENT, true);
            selectCardIntent.putExtra(AppConstants.SPLIT_PAYMENT, true);
            selectCardIntent.putParcelableArrayListExtra(AppConstants.SELECTED_CARD, getSelectedCards());
            startActivityForResult(selectCardIntent, AppConstants.PAYMENT_METHODS_REQUEST_CODE);
        } else {
            linearLayoutSecondPaymentMethods.setVisibility(GONE);
            selectedPaymentMethods.clear();
            if (card != null) {
                selectedPaymentMethods.add(card);
                setCardFields(card);
            } else {
                if (isOnline())
                    presenter.loadPaymentMethods(getOrganizationUUIDFromEntities());
                else
                    swipeRefreshLayout.setRefreshing(false);
            }
            validatePlaceOrderButton();
        }
    }

    @OnClick({R.id.textViewSwitchLabel})
    public void onClickSplitPaymentsLable(){
		splitPaymentSwitch.setChecked(!splitPaymentSwitch.isChecked());
		onClickSplitPayments();
	}

	private void showCvvError() {
		GradientDrawable gradientDrawable = (GradientDrawable) linearLayoutPaymentCVV.getBackground();
		gradientDrawable.setStroke(dpToPx(2), confHelper.getIndicatorsError());
	}

	private void hideCvvError() {
		GradientDrawable gradientDrawable = (GradientDrawable) linearLayoutPaymentCVV.getBackground();
		gradientDrawable.setStroke(dpToPx(2), Color.TRANSPARENT);
	}

	private void showSecondCvvError() {
		GradientDrawable gradientDrawable = (GradientDrawable) linearLayoutSecondPaymentCVV.getBackground();
		gradientDrawable.setStroke(dpToPx(2), confHelper.getIndicatorsError());
	}

	private void hideSecondCvvError() {
		GradientDrawable gradientDrawable = (GradientDrawable) linearLayoutSecondPaymentCVV.getBackground();
		gradientDrawable.setStroke(dpToPx(2), Color.TRANSPARENT);
	}

	private void showInfoBar(String message) {
		getView().announceForAccessibility(message);
		linearLayoutInfoBar.setVisibility(VISIBLE);
		textViewInfoBar.setText(message);
		editTextPaymentCVV.requestFocus();
		editTextPaymentCVV.postDelayed(() -> {
			InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			keyboard.showSoftInput(editTextPaymentCVV, 0);
		}, 100);
	}


	private void hideInfoBar() {
		linearLayoutInfoBar.setVisibility(GONE);
	}

	/***
	 * Trace number is always required to be set for an order.
	 * This allows check if the order is been placed twice.
	 * <p>
	 * Trace number must be generated when:
	 * A payment method has been changed.
	 * A quantity for a product has been changed.
	 * A product has been added/removed from the cart.
	 */
	private String generateTraceNumber() {
		return UUID.randomUUID().toString();
	}

	@SuppressWarnings("MissingPermission")
	private void proceedToPostOrder() {
		boolean isQuantityZero = false;

		if (savedEntityResults.size() > 0 || fareMediaUuid != null || reloadingWallet != null) {
			for (EntityResult entityResult : savedEntityResults) {
				if (entityResult.getQuantity() == 0)
					isQuantityZero = true;
			}
			if (fareMediaUuid == null && isQuantityZero) {
				showSimpleMaterialDialog(R.string.payment_popup_product_item_required, R.string.payment_popup_product_item_required_message);
			} else {
				rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION)
						.doOnError(throwable -> {
							throwable.printStackTrace();
						})
						.subscribe(granted -> {
							double lat = 0;
							double log = 0;
							final Location location = GPSTracker.getLocation(getActivity());
							if (granted && location != null) {
								lat = location.getLatitude();
								log = location.getLongitude();
							}
							if (isIntentForReload) {
								if (constraintLayoutReloadOptions.getVisibility() == View.VISIBLE) {
									if (radioGroup.getCheckedRadioButtonId() == -1) return;
									int id = radioGroup.getCheckedRadioButtonId();
									total = (int) getActivity().findViewById(id).getTag();
								} else if (constraintLayoutReloadAmount.getVisibility() == View.VISIBLE) {
									total = (int) textViewReloadAmount.getTag();
								} else return;

								Card secondCard = null;
								if (selectedPaymentMethods.size() > 1) {
									card.setAmount(Integer.parseInt(editTextFirstCardAmount.getText().toString().replaceAll("[$,.]", "").replaceAll("[^\\d]", "")));
									secondCard = (Card) selectedPaymentMethods.get(1);
									secondCard.setAmount(Integer.parseInt(editTextSecondCardAmount.getText().toString().replaceAll("[$,.]", "").replaceAll("[^\\d]", "")));
								}
								String reloadingWalletUUId = reloadingWallet != null ? reloadingWallet.getUuid() : null;
								presenter.createOrder(savedEntityResults, fareMediaUuid, reloadingWalletUUId,
										true, card, secondCard, ideal, dotPay,
										payNearMe, incomm, googlePay, wallet, total, traceNumber, lat, log,
										payPal, offerServe, discountData != null ? discountData.getDiscountServeUuid() : null, discountData != null ? true : false, getActivity().getIntent().getStringExtra(AppConstants.DEEPLINK_JWT_TOKEN) != null ?
												getActivity().getIntent().getStringExtra(AppConstants.DEEPLINK_JWT_TOKEN) : null);
							} else {
								Card secondCard = null;
								if (selectedPaymentMethods.size() > 1) {
									card.setAmount(Integer.parseInt(editTextFirstCardAmount.getText().toString().replaceAll("[$,.]", "").replaceAll("[^\\d]", "")));
									secondCard = (Card) selectedPaymentMethods.get(1);
									secondCard.setAmount(Integer.parseInt(editTextSecondCardAmount.getText().toString().replaceAll("[$,.]", "").replaceAll("[^\\d]", "")));
								}
								presenter.createOrder(savedEntityResults, fareMediaUuid, null, false, card, secondCard, ideal, dotPay, payNearMe, incomm, googlePay, wallet, total, traceNumber, lat, log, payPal, offerServe
										, discountData != null ?discountData.getDiscountServeUuid() : null, discountData != null ? true : false, getActivity().getIntent().getStringExtra(AppConstants.DEEPLINK_JWT_TOKEN) != null ?
												getActivity().getIntent().getStringExtra(AppConstants.DEEPLINK_JWT_TOKEN) : null);
							}
						});
			}
		} else {
			showActionBarAndEnableSwipe();
			linearLayoutEmptyShoppingCart.setVisibility(VISIBLE);
		}
	}

	private void showActionBarAndEnableSwipe() {
		((AppCompatActivity) getActivity()).getSupportActionBar().show();
		if (reloadingWallet != null) {
			((TabsActivity) getActivity()).showTabLayout();
			((TabsActivity) getActivity()).enableSwipe();
		}
	}

	@OnClick({R.id.buttonMyTickets, R.id.btn_view_tickets})
	public void onMyTicketsButtonClick() {
		if(discountData != null) {
			showDiscountDiscardWarningOnNavigation(VIEW_TICKETS);
		} else if (reloadingWallet == null && TextUtils.isEmpty(fareMediaUuid)) {
			// Pass purchase Flow
			clearCartAndNavigateTo(new Intent(getContext(), UseTicketsActivity.class));
		} else if (fareMediaUuid != null) {
			// FareMedium flow
			if (isIntentForReload) {
				// Load money to FM
				Intent data = new Intent();
				data.putExtra(ManageCardsActivity.FARE_MEDIUM_ID_TO_FOCUS, fareMediaUuid);
				getActivity().setResult(Activity.RESULT_OK, data);
				getActivity().finish();
			} else {
				// Add Pass to FM
				Intent intent = new Intent(getContext(), ManageCardsActivity.class);
				intent.putExtra(ManageCardsActivity.FARE_MEDIUM_ID_TO_FOCUS, fareMediaUuid);
				clearCartAndNavigateTo(intent);
			}
		} else {
			// wallet reload flow
			getActivity().setResult(AppConstants.LOAD_MONEY_REQUEST_CODE);
			getActivity().finish();
		}
	}

	private void clearCartAndNavigateTo(Intent intent) {
		clearCart();
		createBackStack(intent);
	}

	private void clearCart() {
		if (!savedEntityResults.isEmpty()) {
			savedEntityResults.clear();
			sharedPreferences.edit().remove(AppConstants.ENTITY_RESULT_LIST).apply();
		}
	}

	private void createBackStack(Intent intent) {
		TaskStackBuilder builder = TaskStackBuilder.create(getActivity());
		builder.addNextIntentWithParentStack(intent);
		builder.startActivities();
	}

	public void updateShoppingCart(List<EntityResult> entityResults, boolean updateTraceNumber) {
		if (updateTraceNumber)
			traceNumber = generateTraceNumber();
		savedEntityResults = entityResults;
		String composedSavedEntityResults = gson.toJson(entityResults);
		sharedPreferences.edit()
				.putString(AppConstants.ENTITY_RESULT_LIST, composedSavedEntityResults)
				.apply();
	}

	public void onCancel(int requestCode) {
		webViewTransactionFailed(getString(R.string.payment_cancelled), getString(R.string.payment_order_cancelled_message));
	}

	public void onError(Exception error) {
		if (error instanceof ErrorWithResponse) {
			ErrorWithResponse errorWithResponse = (ErrorWithResponse) error;
			BraintreeError cardErrors = errorWithResponse.errorFor("creditCard");
			if (cardErrors != null) {
				// There is an issue with the credit card.
				BraintreeError expirationMonthError = cardErrors.errorFor("expirationMonth");
				if (expirationMonthError != null) {
					// There is an issue with the expiration month.
					//setErrorMessage(expirationMonthError.getMessage());
					webViewTransactionFailed(getString(R.string.popup_error), expirationMonthError.getMessage());
				}
			}
		}

	}

	public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
		payPal.setPayPalNonce(paymentMethodNonce.getNonce());
		proceedToPostOrder();
	}

	public void setPayPal(BraintreePaypalPaymentMethod payPal) {
        this.payPal = payPal;
        prepareTransition();
        hideInfoBar();

        isPaymentMethodsListEmpty = false;

        textViewPaymentMethods.setText(getString(R.string.payment_text));
        changePayment.setText(getString(R.string.shopping_cart_change_payment));
        changePayment.setTag(CHANGE);
        changePayment.setContentDescription(getString(R.string.payment_change_payment_method_voonly));
        setVisibilityForSplitPaymentText(VISIBLE);

        linearLayoutPaymentMethodsLoading.setVisibility(GONE);
        textViewNoPaymentMethods.setVisibility(GONE);
        textViewSelectTwoPaymentMethods.setVisibility(GONE);
        linearLayoutPaymentMethods.postDelayed(() -> linearLayoutPaymentMethods.setVisibility(GONE), 200);
        linearLayoutSecondPaymentMethods.postDelayed(() -> linearLayoutSecondPaymentMethods.setVisibility(GONE), 250);
        linearLayoutPayment.setVisibility(VISIBLE);
        imageViewType.setImageDrawable(getResources().getDrawable(R.drawable.bt_ic_paypal));
        textViewLabel.setText(getText(R.string.payment_paypal));
        textViewLabel2.setVisibility(GONE);
    }

	protected void setPayNearMe(PayNearMe payNearMe) {
        this.payNearMe = payNearMe;
        prepareTransition();
        hideInfoBar();

        textViewPaymentMethods.setText(getString(R.string.payment_text));
        changePayment.setText(getString(R.string.shopping_cart_change_payment));
        changePayment.setTag(CHANGE);
        changePayment.setContentDescription(getString(R.string.payment_change_payment_method_voonly));
        setVisibilityForSplitPaymentText(VISIBLE);
        linearLayoutPaymentMethodsLoading.setVisibility(GONE);
        textViewNoPaymentMethods.setVisibility(GONE);
        textViewSelectTwoPaymentMethods.setVisibility(GONE);
        linearLayoutPaymentMethods.postDelayed(() -> linearLayoutPaymentMethods.setVisibility(GONE), 200);
        linearLayoutPayment.setVisibility(VISIBLE);
        imageViewType.setImageDrawable(getResources().getDrawable(R.drawable.pay_near_me_logo));
        textViewLabel.setText(getText(R.string.payment_pay_with_pay_near_me));
        textViewLabel2.setVisibility(GONE);
    }

	protected void setIncomm(Incomm incomm) {
        this.incomm = incomm;
        prepareTransition();
        hideInfoBar();

        textViewPaymentMethods.setText(getString(R.string.payment_text));
        changePayment.setText(getString(R.string.shopping_cart_change_payment));
        changePayment.setTag(CHANGE);
        changePayment.setContentDescription(getString(R.string.payment_change_payment_method_voonly));
        setVisibilityForSplitPaymentText(VISIBLE);
        linearLayoutPaymentMethodsLoading.setVisibility(GONE);
        textViewNoPaymentMethods.setVisibility(GONE);
        textViewSelectTwoPaymentMethods.setVisibility(GONE);
        linearLayoutPaymentMethods.postDelayed(() -> linearLayoutPaymentMethods.setVisibility(GONE), 200);
        linearLayoutPayment.setVisibility(VISIBLE);
        imageViewType.setImageDrawable(getResources().getDrawable(R.drawable.ic_incomm));
        String paymentMethodName = String.format(getString(R.string.payment_pay_cash_at_store),
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(confHelper.getIncommMinValue(), true),
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(confHelper.getIncommMaxValue(), true)
        );
        String paymentMethodNameAnnouncement = String.format(getString(R.string.payment_pay_cash_at_store_announcement),
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(confHelper.getIncommMinValue(), true),
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(confHelper.getIncommMaxValue(), true)
		);
		textViewLabel.setContentDescription(paymentMethodNameAnnouncement);
		textViewLabel.setText(paymentMethodName);
		textViewLabel2.setVisibility(GONE);
	}

	public void setIdeal(Ideal ideal) {
        this.ideal = ideal;
        prepareTransition();
        hideInfoBar();

        textViewPaymentMethods.setText(getString(R.string.payment_text));
        changePayment.setText(getString(R.string.shopping_cart_change_payment));
        changePayment.setTag(CHANGE);
        changePayment.setContentDescription(getString(R.string.payment_change_payment_method_voonly));
        setVisibilityForSplitPaymentText(VISIBLE);

        linearLayoutPaymentMethodsLoading.setVisibility(GONE);
        textViewNoPaymentMethods.setVisibility(GONE);
        textViewSelectTwoPaymentMethods.setVisibility(GONE);
        linearLayoutPaymentMethods.postDelayed(() -> linearLayoutPaymentMethods.setVisibility(GONE), 200);
        linearLayoutSecondPaymentMethods.postDelayed(() -> linearLayoutSecondPaymentMethods.setVisibility(GONE), 250);
        linearLayoutPayment.setVisibility(VISIBLE);
        imageViewType.setImageDrawable(getResources().getDrawable(R.drawable.ic_ideal));
        textViewLabel.setText(getText(R.string.payment_ideal));
        textViewLabel2.setVisibility(GONE);
    }

	public void setDotPay(DotPay dotPay) {
        this.dotPay = dotPay;
        prepareTransition();
        hideInfoBar();

        textViewPaymentMethods.setText(getString(R.string.payment_text));
        changePayment.setText(getString(R.string.shopping_cart_change_payment));
        changePayment.setTag(CHANGE);
        changePayment.setContentDescription(getString(R.string.payment_change_payment_method_voonly));
        setVisibilityForSplitPaymentText(VISIBLE);

        linearLayoutPaymentMethodsLoading.setVisibility(GONE);
        textViewNoPaymentMethods.setVisibility(GONE);
        textViewSelectTwoPaymentMethods.setVisibility(GONE);
        linearLayoutPaymentMethods.postDelayed(() -> linearLayoutPaymentMethods.setVisibility(GONE), 200);
        linearLayoutSecondPaymentMethods.postDelayed(() -> linearLayoutSecondPaymentMethods.setVisibility(GONE), 250);
        linearLayoutPayment.setVisibility(VISIBLE);
        imageViewType.setImageDrawable(getResources().getDrawable(R.drawable.ic_dot_pay));
        textViewLabel.setText(getText(R.string.payment_dot_pay));
        textViewLabel2.setVisibility(GONE);
    }

	public void setWallet(Wallet wallet) {
        this.wallet = wallet;
        prepareTransition();
        hideInfoBar();

        textViewPaymentMethods.setText(getString(R.string.payment_text));
        changePayment.setText(getString(R.string.shopping_cart_change_payment));
        changePayment.setTag(CHANGE);
        changePayment.setContentDescription(getString(R.string.payment_change_payment_method_voonly));
        setVisibilityForSplitPaymentText(VISIBLE);

        linearLayoutPaymentMethodsLoading.setVisibility(GONE);
        textViewNoPaymentMethods.setVisibility(GONE);
        textViewSelectTwoPaymentMethods.setVisibility(GONE);
        textViewSelectTwoPaymentMethods.setVisibility(GONE);
        linearLayoutPaymentMethods.postDelayed(() -> linearLayoutPaymentMethods.setVisibility(GONE), 200);
        linearLayoutSecondPaymentMethods.postDelayed(() -> linearLayoutSecondPaymentMethods.setVisibility(GONE), 250);
        linearLayoutPayment.setVisibility(VISIBLE);
        imageViewType.setImageDrawable(getResources().getDrawable(R.drawable.ic_wallet));
        String walletAvailableBalanceText = String.format(getString(R.string.payments_available),
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(wallet.getAvailableAmount()));
        if (wallet.getNickName() != null && !wallet.getNickName().isEmpty()) {
            textViewLabel.setText(wallet.getNickName());
            textViewLabel2.setVisibility(VISIBLE);
            textViewLabel2.setText(walletAvailableBalanceText);
		} else {
			textViewLabel.setText(walletAvailableBalanceText);
			textViewLabel2.setVisibility(GONE);
		}
	}

	public void setGooglePay(GooglePay googlePay) {
		this.googlePay = googlePay;
		setGooglePay();
	}

	public void setGooglePay() {
        card = null;
        payPal = null;
        ideal = null;
        dotPay = null;
        wallet = null;

        prepareTransition();
        hideInfoBar();

        textViewPaymentMethods.setText(getString(R.string.payment_text));
        changePayment.setText(getString(R.string.shopping_cart_change_payment));
        changePayment.setTag(CHANGE);
        changePayment.setContentDescription(getString(R.string.payment_change_payment_method_voonly));
        setVisibilityForSplitPaymentText(VISIBLE);

        linearLayoutPaymentMethodsLoading.setVisibility(GONE);
        textViewNoPaymentMethods.setVisibility(GONE);
        linearLayoutPaymentMethods.postDelayed(() -> linearLayoutPaymentMethods.setVisibility(GONE), 200);
        linearLayoutSecondPaymentMethods.postDelayed(() -> linearLayoutSecondPaymentMethods.setVisibility(GONE), 250);
        linearLayoutPayment.setVisibility(VISIBLE);
        imageViewType.setImageDrawable(getResources().getDrawable(R.drawable.ic_google_pay_mark));
        textViewLabel.setText(getText(R.string.payment_google_pay));
        textViewLabel2.setVisibility(GONE);
        validatePlaceOrderButton();
    }

	@Override
	public boolean onBackPressed() {
		if(discountData != null && !allowBackPressOnDiscountCalculation) {
			showDiscountDiscardWarningOnNavigation(BACK_PRESSED);
			return true;
		} else if (isBottomSheetExpanded()) {
			hideBottomSheet();
			return true;
		} else if (linearLayoutOrderSuccessful.getVisibility() == VISIBLE) {
			clearCartAndNavigateTo(new Intent(
					getContext(),
					!TextUtils.isEmpty(fareMediaUuid) ? ManageCardsActivity.class : BuyTicketsActivity.class
			));
			return false;
		} else
			return linearLayoutLoadingScreen.getVisibility() == VISIBLE;
	}


	public void hideBottomSheet() {
		if (isBottomSheetExpanded()) {
			bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
			bottomSheetBehavior.setPeekHeight(0);
			buttonPlaceOrder.setVisibility(VISIBLE);
		}
	}

	public boolean isBottomSheetExpanded() {
		return bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED;
	}

	public void launchWebViewPaymentsFlow(PaymentTypes paymentType, String url, String postData) {
		bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
		String urlString = url + "?" + postData;
		Intent redirectIntent = createRedirectIntent(urlString);
		redirectIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		if (paymentType == PaymentTypes.PAY_NEAR_ME) {
			startActivityForResult(redirectIntent, AppConstants.PAY_NEAR_ME_WEBVIEW_REQUEST_CODE);
		} else {
			startActivity(redirectIntent);
		}
	}

	private Intent createRedirectIntent(@NonNull String urlString) {
		Intent result;

		Uri uri = Uri.parse(urlString);
		RedirectUtil.ResolveResult resolveResult = RedirectUtil.determineResolveResult(getActivity(), uri);

		if (resolveResult.getResolveType() == RedirectUtil.ResolveType.APPLICATION) {
			// If the user has selected a default app for this URL, open with app.
			result = new Intent(Intent.ACTION_VIEW, uri);
		} else {
			CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
					.setShowTitle(true)
					.setToolbarColor(ContextCompat.getColor(getActivity(), R.color.white))
					.build();
			customTabsIntent.intent.setData(uri);

			// Create chooser with Chrome, but don't use CustomTabsIntent.
			Intent target = new Intent(Intent.ACTION_VIEW, uri);
			result = Intent.createChooser(target, null);
		}

		return result;
	}

	@Override
	public void showIssuerPopUp(String orderUuid, List<Issuer> issuers, PurchaseRequestValues purchaseInformation) {
		this.orderUuid = orderUuid;

		bottomSheet.setVisibility(VISIBLE);
		prepareTransition();
		// isOrderSuccess = true;
		showActionBarAndEnableSwipe();
		linearLayoutOrderSuccessful.setVisibility(GONE);
		linearLayoutLoadingScreen.setVisibility(GONE);
		linearLayoutEmptyShoppingCart.setVisibility(GONE);
		scrollViewShoppingCart.setVisibility(VISIBLE);
		buttonPlaceOrder.setVisibility(GONE);

		if (issuers != null) {
			issuerList.setLayoutManager(new LinearLayoutManager(getActivityContext()));
			issuerList.setAdapter(new IssuerAdapter(issuers, issuerId -> {
				bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
				bottomSheet.setVisibility(GONE);
				presenter.makePayment(orderUuid, issuerId, purchaseInformation);
			}));
			bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
			bottomSheetBehavior.setPeekHeight(Utils.dpToPx(150));
			bottomSheetBehavior.setHideable(true);
		}
	}

	@Override
	public void showStripView(List<Integer> imageResources, List<String> supportedPaymentTypes) {
		imageViewStrip.setVisibility(VISIBLE);
		imageViewStrip.setImageResources(imageResources);
		if (supportedPaymentTypes != null && supportedPaymentTypes.size() > 0)
			imageViewStrip.setContentDescription(getString(R.string.payment_accepted_payment_voonly) + supportedPaymentTypes.toString());
	}

	private void showGooglePayNotReady() {
		googlePay = null;
		showSimpleMaterialDialog(R.string.popup_error, R.string.payment_popup_google_pay_not_ready);
	}

	private void showGooglePayReady() {
		setGooglePay();
	}

	private void validatePlaceOrderButton() {
		boolean isEnabled = true;

		if (progressViewLayoutLoading.getVisibility() == VISIBLE) {
			isEnabled = false;
		}

		if (isEnabled && BytemarkSDK.isLoggedIn() && (card != null || payPal != null || ideal != null
				|| googlePay != null || dotPay != null || wallet != null)) {
			if (card != null && card.getCvvRequired()) {
				if (card.getTypeId() == 3 && editTextPaymentCVV.getText().toString().length() == 4) {
					isEnabled = true;
				} else if (editTextPaymentCVV.getText().toString().length() == 3) {
					isEnabled = true;
				}
			} else {
				isEnabled = true;
			}

			if (card != null && confHelper.isV2PaymentMethodsEnabled() && !card.getAccepted()) {
				isEnabled = false;
			}
		}

		if (isIntentForReload) {
			if (constraintLayoutReloadOptions.getVisibility() == VISIBLE) {
				isEnabled = radioGroup.getCheckedRadioButtonId() != -1;
			} else if (constraintLayoutReloadAmount.getVisibility() == VISIBLE) {
				Integer amount = (Integer) textViewReloadAmount.getTag();
				isEnabled = amount != null && amount != 0;
			}
		}

		if (BytemarkSDK.isLoggedIn() && selectedPaymentMethods.size() > 1) {
			Card secondCard = (Card) selectedPaymentMethods.get(1);
			if (card != null && card.getCvvRequired()) {
				if (card.getTypeId() == 3 && editTextPaymentCVV.getText().toString().length() == 4) {
					isEnabled = true;
				} else if (editTextPaymentCVV.getText().toString().length() == 3) {
					isEnabled = true;
				}
			} else {
				isEnabled = true;
			}

			if (isEnabled) {
				if (secondCard != null && secondCard.getCvvRequired()) {
					isEnabled = false;
					if (secondCard.getTypeId() == 3 && editTextSecondPaymentCVV.getText().toString().length() == 4) {
						isEnabled = true;
					} else if (editTextSecondPaymentCVV.getText().toString().length() == 3) {
						isEnabled = true;
					}
				} else {
					isEnabled = true;
				}
			}

			try {
				if (isEnabled) {
					String firstAmountString = editTextFirstCardAmount.getText().toString().replaceAll("[$,.]", "");
					Integer firstAmountParsed = Integer.parseInt(firstAmountString.replaceAll("[^\\d]", ""));
					String secondAmountString = editTextSecondCardAmount.getText().toString().replaceAll("[$,.]", "");
					Integer secondAmountParsed = Integer.parseInt(secondAmountString.replaceAll("[^\\d]", ""));
					if (firstAmountParsed + secondAmountParsed == total) {
						isEnabled = true;
					} else {
						isEnabled = false;
					}
				}
			} catch (Exception e) {
				isEnabled = false;
			}
		}

		if (!isEnabled) {
			int alphaComponent = setAlphaComponent(confHelper.getAccentThemeBacgroundColor(), 26);
			buttonPlaceOrder.setBackgroundTintList(ColorStateList.valueOf(alphaComponent));
		} else {
			buttonPlaceOrder.setBackgroundTintList(ColorStateList.valueOf(confHelper.getAccentThemeBacgroundColor()));
		}

		buttonPlaceOrder.setEnabled(isEnabled);
	}

	@OnClick(R.id.buttonUpdateOrder)
	public void onUpdateButtonClick() {
		if (getActivity() != null) {
			if(discountData != null) {
				showDiscountDiscardWarningOnNavigation(UPDATE_SHOPPING_CARD);
			} else {
				analyticsPlatformAdapter.updateOptionSelected();
				Intent intent = new Intent(getActivity(), NewShoppingCartActivity.class);
				intent.putExtra("fareMediumId", fareMediaUuid);
				getActivity().startActivity(intent);
				getActivity().finish();
			}
		}
	}

	public void setGooglePayReady(boolean googlePayReady) {
		this.googlePayReady = googlePayReady;
	}

	@OnClick(R.id.buttonOtherAmount)
	public void onOtherClickListener() {
		constraintLayoutReloadOptions.setVisibility(GONE);
		constraintLayoutReloadAmount.setVisibility(VISIBLE);
		buttonDoneAmount.setVisibility(VISIBLE);
		editTextReloadAmount.setVisibility(VISIBLE);
		textViewReloadAmount.setVisibility(GONE);
		validatePlaceOrderButton();
	}

	@OnClick(R.id.buttonDoneAmount)
	public void onDoneAmountClickListener() {
		try {
			Double minAmount = 0D;
			Double maxAmount = 0D;
			if (reloadingWallet != null || fareMediaUuid != null) {
				minAmount = Double.valueOf(Double.parseDouble(String.valueOf(walletLoadMoneyConfig.getMinWalletLoadValue())) / 100);
				maxAmount = Double.valueOf(Double.parseDouble(String.valueOf(walletLoadMoneyConfig.getMaxWalletLoadValue())) / 100);
			}
			String otherAmount = editTextReloadAmount.getText().toString().trim();
			Double dollars = Double.valueOf(otherAmount);
			if (dollars < minAmount || dollars > maxAmount) {
				textViewReloadError.setVisibility(View.VISIBLE);
			} else {
				int cents = (int) Math.round(100 * dollars);
				total = cents;
				clearAmountFields();
				editTextFirstCardAmount.setFilters(new InputFilter[]{new AmountValidator(0, total)});
				editTextSecondCardAmount.setFilters(new InputFilter[]{new AmountValidator(0, total)});
				String prettyAmount = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(cents);
				buttonDoneAmount.setVisibility(View.GONE);
				editTextReloadAmount.setVisibility(View.GONE);
				textViewReloadError.setVisibility(View.GONE);
				textViewReloadAmount.setVisibility(View.VISIBLE);
				textViewReloadAmount.setText(prettyAmount);
				textViewReloadAmount.setTag(cents);
				validatePlaceOrderButton();
				hideKeyboard();
			}
		} catch (Exception e) {
			textViewReloadError.setVisibility(View.VISIBLE);
		}
	}

	@OnClick(R.id.imageViewCloseAmount)
	public void onCloseAmountClickListener() {
		constraintLayoutReloadAmount.setVisibility(GONE);
		editTextReloadAmount.setText("");
		textViewReloadAmount.setText("");
		textViewReloadAmount.setTag(null);
		textViewReloadError.setVisibility(GONE);
		constraintLayoutReloadOptions.setVisibility(VISIBLE);
		((BmRadioButton)radioGroup.getChildAt(0)).setChecked(true);
		clearAmountFields();
		validatePlaceOrderButton();
		hideKeyboard();
	}

	@Override
	public void showAppUpdateDialog() {
		prepareTransition();
		showActionBarAndEnableSwipe();
		linearLayoutOrderSuccessful.setVisibility(GONE);
		linearLayoutLoadingScreen.setVisibility(GONE);
		linearLayoutEmptyShoppingCart.setVisibility(GONE);
		scrollViewShoppingCart.setVisibility(VISIBLE);
		buttonPlaceOrder.setVisibility(VISIBLE);
		super.showAppUpdateDialog();
	}

	@Override
	public void setWalletLoadMoneyConfig(co.bytemark.domain.model.common.Data data) {
		fetchLoadValuesUpdateVisibilityState(false);
		this.walletLoadMoneyConfig = data.getWalletLoadMoneyConfig();
		if (walletLoadMoneyConfig == null) return;
		constraintLayoutReload.setVisibility(VISIBLE);
		radioGroup.removeAllViews();
		for (int value : walletLoadMoneyConfig.getLoadValues()) {

			BmRadioButton radioButton = new BmRadioButton(getActivity());
			radioButton.setText(confHelper.getConfigurationPurchaseOptionsCurrencySymbol(value, true));
			radioButton.setTag(value);
			radioGroup.addView(radioButton);

		}
		if (radioGroup.getChildCount() > 0)
			((BmRadioButton) radioGroup.getChildAt(0)).setChecked(true);

		boolean shouldHideDecimals = !TextUtils.isEmpty(fareMediaUuid);

		editTextReloadAmount.setHint(String.format(getString(R.string.autoload_from_upto),
				confHelper.getConfigurationPurchaseOptionsCurrencySymbol(walletLoadMoneyConfig.getMinWalletLoadValue(), shouldHideDecimals),
				confHelper.getConfigurationPurchaseOptionsCurrencySymbol(walletLoadMoneyConfig.getMaxWalletLoadValue(), shouldHideDecimals)));
		textViewReloadError.setText(String.format(getString(R.string.autoload_validation_between),
				confHelper.getConfigurationPurchaseOptionsCurrencySymbol(walletLoadMoneyConfig.getMinWalletLoadValue(), shouldHideDecimals),
				confHelper.getConfigurationPurchaseOptionsCurrencySymbol(walletLoadMoneyConfig.getMaxWalletLoadValue(), shouldHideDecimals)));

		editTextReloadAmount.setFilters(new InputFilter[]{new DigitsInputFilter(
				String.valueOf(walletLoadMoneyConfig.getMaxWalletLoadValue()).length() - 2,
				2)});
	}

	@Override
	public void setAcceptedPaymentMethods(ArrayList<String> acceptedPaymentMethods) {
		this.acceptedPaymentMethods = acceptedPaymentMethods;
		for (String paymentMethod : acceptedPaymentMethods) {
			if (isIntentForReload && getActivity() instanceof LoadMoneyWalletActivity &&
					paymentMethod.toLowerCase().contains(AppConstants.PAYMENT_TYPE_PAYPAL)) {
				addPaymentMethodsScreenPresent = true;
			} else if (paymentMethod.toLowerCase().contains(AppConstants.PAYMENT_TYPE_PAYPAL)
                // TODO v3232 Uncomment this if add wallet functionality is required
                //|| paymentMethod.toLowerCase().contains(PAYMENT_TYPE_WALLET)
            ) {
                addPaymentMethodsScreenPresent = true;
            }
		}
    }

    @Override
    public void setAddPaymentMethodsScreenPresent(boolean value) {
        addPaymentMethodsScreenPresent = value;
    }

    private boolean isAddPaymentMehodsScreenPresent() {
        return addPaymentMethodsScreenPresent;
    }

    public void showSelectTwoPaymentMethodsView() {
        isPaymentMethodsListEmpty = true;
        selectedPaymentMethods.clear();
        textViewPaymentMethods.setText(getString(R.string.payment_text));
        changePayment.setText(getString(R.string.shopping_cart_change_payment));
        changePayment.setTag(CHANGE);
        changePayment.setContentDescription(getString(R.string.payment_change_payment_method_voonly));
        linearLayoutPaymentMethodsLoading.setVisibility(GONE);
        linearLayoutPaymentMethods.setVisibility(GONE);
        linearLayoutSecondPaymentMethods.setVisibility(GONE);
        linearLayoutPayment.setVisibility(GONE);
        textViewNoPaymentMethods.setVisibility(GONE);
        textViewSelectTwoPaymentMethods.setVisibility(VISIBLE);
        validatePlaceOrderButton();
    }

	@Override
	public void showLoading() {
		swipeRefreshLayout.setVisibility(GONE);
		linearLayoutLoading.setVisibility(VISIBLE);
	}

	@Override
	public void hideLoading() {
		linearLayoutLoading.setVisibility(GONE);
		swipeRefreshLayout.setVisibility(VISIBLE);
	}

	@Override
	public void showUpdatedSummaryWithDiscount(DiscountData discountData) {
		this.discountData = discountData;
		updateSummarywithDiscountFromdeeplink();
		if (defaultPaymentMethod == null && !isZerDollerPurchase()) {
			updatePaymentLinearLayout.setVisibility(VISIBLE);
			linearLayoutPaymentMethod.setVisibility(VISIBLE);
			loadPaymentMethodsIfLoggedIn();
		}
	}

	private void showDiscountDiscardWarningOnNavigation(int navigationFrom) {
		new MaterialDialog.Builder(getContext())
				.title(R.string.popup_warning)
				.content(R.string.discount_calculation_popup_warning_message)
				.negativeText(R.string.popup_cancel)
				.positiveText(R.string.ok)
				.positiveColor(confHelper.getDataThemeAccentColor())
				.onPositive((dialog, which) -> {
					discountData = null;
					dialog.dismiss();
					switch (navigationFrom) {
						case BACK_PRESSED:
							((AcceptPaymentActivity)getActivity()).onBackPressed();
							break;
						case UPDATE_SHOPPING_CARD:
							onUpdateButtonClick();
							break;
						case VIEW_TICKETS:
							onMyTicketsButtonClick();
							break;
					}
				})
				.onNegative((dialog, which) -> {
					dialog.dismiss();
				})
				.show();
	}

	@Override
	public void handleErrorOnDiscountCalculation(String message) {
		if (defaultPaymentMethod == null && !isZerDollerPurchase()) {
			linearLayoutPaymentMethod.setVisibility(VISIBLE);
			updatePaymentLinearLayout.setVisibility(VISIBLE);
			loadPaymentMethodsIfLoggedIn();
		}
		new MaterialDialog.Builder(getContext())
				.title(R.string.popup_error)
				.content(message)
				.cancelable(false)
				.positiveText(R.string.ok)
				.positiveColor(confHelper.getDataThemeAccentColor())
				.onPositive((dialog, which) -> {
					dialog.dismiss();
					hideLoading();
					buttonApply.setEnabled(true);
					buttonApply.setAlpha(1f);
				})
				.show();
	}

	private boolean isZerDollerPurchase() {
		return total == 0 && !confHelper.isPaymentMandatory();
	}
}