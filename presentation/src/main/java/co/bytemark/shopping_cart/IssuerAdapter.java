package co.bytemark.shopping_cart;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;
import co.bytemark.sdk.model.payment_methods.Issuer;

/**
 * Created by Omkar on 12/6/17.
 */
class IssuerAdapter extends RecyclerView.Adapter<IssuerAdapter.IssuerViewHolder> {

    private ItemCallback itemCallback;
    private List<Issuer> issuers = new ArrayList<>();

    IssuerAdapter(List<Issuer> issuers, ItemCallback itemCallback) {
        this.issuers = issuers;
        this.itemCallback = itemCallback;
        setHasStableIds(true);
    }

    @Override
    public IssuerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new IssuerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.issuer_item, parent, false));
    }

    @Override
    public void onBindViewHolder(IssuerViewHolder holder, int position) {
        final Issuer issuer = issuers.get(position);
        holder.textView.setText(issuer.getName());
        holder.linearLayout.setOnClickListener(view ->
                itemCallback.onItemClicked(issuer.getIssuerId()));
    }

    @Override
    public int getItemCount() {
        return issuers.size();
    }

    @Override
    public long getItemId(int position) {
        return issuers.get(position).getIssuerId().hashCode();
    }

    interface ItemCallback {
        void onItemClicked(String id);
    }

    class IssuerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.linearLayout)
        LinearLayout linearLayout;
        @BindView(R.id.textView)
        TextView textView;

        IssuerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
