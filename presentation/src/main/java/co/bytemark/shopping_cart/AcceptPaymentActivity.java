package co.bytemark.shopping_cart;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.MenuItem;

import com.braintreepayments.api.interfaces.BraintreeCancelListener;
import com.braintreepayments.api.interfaces.BraintreeErrorListener;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.base.MasterActivity;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.RxUtils;
import co.bytemark.sdk.model.product_search.entity.EntityResult;
import co.bytemark.use_tickets.BackHandler;
import timber.log.Timber;

public class AcceptPaymentActivity extends MasterActivity
        implements PaymentMethodNonceCreatedListener, BraintreeErrorListener, BraintreeCancelListener {

    public static final String SOURCE = "Source";

    @Inject
    Gson gson;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    RxUtils rxUtils;
    @Inject
    AnalyticsPlatformAdapter analyticsPlatformAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomerMobileApp.Companion.getComponent().inject(this);

        setToolbarTitle(R.string.screen_title_payment);

        if (getIntent().hasExtra(AppConstants.EXTRA_ORIGIN)) {
            final String source = getIntent().getStringExtra(AppConstants.EXTRA_ORIGIN);
            analyticsPlatformAdapter.checkoutScreenDisplayed(source);
        }
    }


    private List<EntityResult> getProductsCount() {
        final String entityResultString = sharedPreferences.getString(AppConstants.ENTITY_RESULT_LIST, null);
        if (entityResultString != null) {
            List<EntityResult> savedEntityResults = gson.fromJson(entityResultString, new TypeToken<List<EntityResult>>() {
            }.getType());
            if (savedEntityResults != null) {
                return savedEntityResults;
            }
        }
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
//            case R.id.nav_buy:
//                analyticsPlatformAdapter.cartViewStorePressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected boolean useHamburgerMenu() {
        return false;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_shopping_cart;
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (fragment instanceof BackHandler) {
            if (!((BackHandler) fragment).onBackPressed()) {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onError(Exception error) {
        Timber.e(error.getMessage());
        AcceptPaymentFragment fragment = (AcceptPaymentFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment);

        if (fragment != null) {
            fragment.onError(error);
        }
    }

    @Override
    public void onCancel(int requestCode) {
        AcceptPaymentFragment fragment = (AcceptPaymentFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment);

        if (fragment != null) {
            fragment.onCancel(requestCode);
        }
    }

    @Override
    public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
        AcceptPaymentFragment fragment = (AcceptPaymentFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment);

        if (fragment != null) {
            fragment.onPaymentMethodNonceCreated(paymentMethodNonce);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            if (fragment instanceof AcceptPaymentFragment) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
