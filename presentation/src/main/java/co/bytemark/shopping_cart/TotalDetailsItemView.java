package co.bytemark.shopping_cart;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;

public class TotalDetailsItemView extends LinearLayout {

    @BindView(R.id.tv_detail)
    TextView textViewDetail;

    @BindView(R.id.tv_price)
    TextView textViewPrice;

    @BindView(R.id.llTotalDetailsView)
    LinearLayout linearLayoutTotalDetailsView;

    @BindView(R.id.quantity)
    TextView quantity;

    @BindView(R.id.origindestination)
    TextView originDestination;

    @BindView(R.id.subscribe)
    TextView subscribe;

    @BindView(R.id.licenseSpotTV)
    TextView licenseSpotTV;

    public TotalDetailsItemView(Context context) {
        super(context);
    }

    public TotalDetailsItemView(Context context, TotalDetails totalDetails) {
        super(context);

        View view = inflate(context, R.layout.shopping_cart_total_details, this);
        ButterKnife.bind(view);

        init(totalDetails);
    }

    private void init(TotalDetails totalDetails) {
        textViewDetail.setText(totalDetails.getTitle());
        textViewDetail.setTextColor(totalDetails.getTextColor());
        textViewPrice.setText(totalDetails.getPrice());
        textViewPrice.setTextColor(totalDetails.getPriceColor() != 0 ? totalDetails.getPriceColor() : totalDetails.getTextColor());
        linearLayoutTotalDetailsView.setContentDescription(totalDetails.getContentDescription());
        if (totalDetails.getOriginDestination() != null) {
            originDestination.setText(totalDetails.getOriginDestination());
            originDestination.setVisibility(VISIBLE);
            originDestination.setTextColor(totalDetails.getTextColor());
        } else {
            originDestination.setVisibility(GONE);
        }
        if (totalDetails.getSubscribed()) {
            if(totalDetails.isFareMediumScreen()) subscribe.setText(R.string.pass_auto_renewals_shopping_cart_auto_renewed);
            else subscribe.setText(R.string.shopping_cart_subscribed);
            subscribe.setVisibility(VISIBLE);
        } else {
            subscribe.setVisibility(GONE);
        }

        if(totalDetails.getLicenceSpot()!=null && !totalDetails.getLicenceSpot().equals("")){
            licenseSpotTV.setText(totalDetails.getLicenceSpot());
            licenseSpotTV.setVisibility(VISIBLE);
        }else{
            licenseSpotTV.setVisibility(GONE);
        }
        quantity.setText(totalDetails.getQuantity());
        quantity.setTextColor(totalDetails.getTextColor());
    }
}
