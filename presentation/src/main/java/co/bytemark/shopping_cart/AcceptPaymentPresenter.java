package co.bytemark.shopping_cart;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Pair;

import androidx.lifecycle.LifecycleOwner;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentsClient;

import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import co.bytemark.R;
import co.bytemark.domain.error.BytemarkException;
import co.bytemark.domain.interactor.discount.DiscountCalculationUseCase;
import co.bytemark.domain.interactor.payments.GetPaymentMethodsUseCase;
import co.bytemark.domain.interactor.payments.GetPaymentMethodsUseCaseValue;
import co.bytemark.domain.interactor.autoload.GetWalletLoadMoneyConfigUseCase;
import co.bytemark.domain.interactor.autoload.GetWalletLoadMoneyConfigUseCaseValues;
import co.bytemark.domain.interactor.payments.GetPaymentMethodsUseCase;
import co.bytemark.domain.interactor.payments.GetPaymentMethodsUseCaseValue;
import co.bytemark.domain.interactor.product.GetAcceptedPaymentMethodsUseCase;
import co.bytemark.domain.interactor.product.GetAcceptedPaymentMethodsUseCaseValue;
import co.bytemark.domain.interactor.product.GetPayPalTokenRequestValue;
import co.bytemark.domain.interactor.product.PurchaseOrderRequestValues;
import co.bytemark.domain.interactor.product.PurchaseOrderUseCase;
import co.bytemark.domain.interactor.product.order.ApplyPromoCodeRequestValues;
import co.bytemark.domain.interactor.product.order.ApplyPromoCodeUseCase;
import co.bytemark.domain.interactor.product.order.CreateOrderUseCase;
import co.bytemark.domain.interactor.product.order.MakePaymentRequestValues;
import co.bytemark.domain.interactor.product.order.PurchaseRequestValues;
import co.bytemark.domain.interactor.product.order.card.CardPurchaseUseCase;
import co.bytemark.domain.interactor.product.order.ideal.GetPaymentUrlUseCase;
import co.bytemark.domain.interactor.product.order.paypal.GetPayPalTokenUseCase;
import co.bytemark.domain.model.common.Result;
import co.bytemark.domain.model.discount.DiscountCalculationRequest;
import co.bytemark.domain.model.discount.DiscountData;
import co.bytemark.domain.model.incomm.IncommBarcodeDetail;
import co.bytemark.domain.model.order.ApplyPromoCode;
import co.bytemark.domain.model.order.OfferServe;
import co.bytemark.domain.model.order.PriceAdjustment;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.helpers.RxEventBus;
import co.bytemark.mvp.MvpBasePresenter;
import co.bytemark.sdk.BMError;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.GetPaymentMethods;
import co.bytemark.sdk.model.common.Data;
import co.bytemark.sdk.model.payment_methods.BraintreePaypalPaymentMethod;
import co.bytemark.sdk.model.payment_methods.Card;
import co.bytemark.sdk.model.payment_methods.DotPay;
import co.bytemark.sdk.model.payment_methods.GooglePay;
import co.bytemark.sdk.model.payment_methods.Incomm;
import co.bytemark.sdk.model.payment_methods.Issuer;
import co.bytemark.sdk.model.payment_methods.PaymentMethods;
import co.bytemark.sdk.model.payment_methods.Wallet;
import co.bytemark.sdk.model.product_search.entity.EntityResult;
import co.bytemark.sdk.network_impl.BMNetwork;
import co.bytemark.sdk.network_impl.BaseNetworkRequestCallback;
import co.bytemark.sdk.post_body.Ideal;
import co.bytemark.sdk.post_body.Item;
import co.bytemark.sdk.post_body.PayNearMe;
import co.bytemark.sdk.post_body.Payment;
import co.bytemark.sdk.post_body.PaymentTypes;
import okhttp3.internal.http2.StreamResetException;
import rx.SingleSubscriber;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static co.bytemark.helpers.AppConstants.CARD_TYPE_AMERICAN_EXPRESS;
import static co.bytemark.helpers.AppConstants.CARD_TYPE_CIRRUS_DEBIT;
import static co.bytemark.helpers.AppConstants.CARD_TYPE_DINERS_CLUB;
import static co.bytemark.helpers.AppConstants.CARD_TYPE_DISCOVER;
import static co.bytemark.helpers.AppConstants.CARD_TYPE_JCB;
import static co.bytemark.helpers.AppConstants.CARD_TYPE_MAESTRO;
import static co.bytemark.helpers.AppConstants.CARD_TYPE_MASTERCARD;
import static co.bytemark.helpers.AppConstants.CARD_TYPE_SOLO_DEBIT;
import static co.bytemark.helpers.AppConstants.CARD_TYPE_UNION_PAY;
import static co.bytemark.helpers.AppConstants.CARD_TYPE_VISA;
import static co.bytemark.helpers.AppConstants.PAYMENT_TYPE_DOT_PAY;
import static co.bytemark.helpers.AppConstants.PAYMENT_TYPE_GOOGLE_PAY;
import static co.bytemark.helpers.AppConstants.PAYMENT_TYPE_IDEAL;
import static co.bytemark.helpers.AppConstants.PAYMENT_TYPE_INCOMM;
import static co.bytemark.helpers.AppConstants.PAYMENT_TYPE_PAYPAL;
import static co.bytemark.helpers.AppConstants.PAYMENT_TYPE_PAY_NEAR_ME;
import static co.bytemark.helpers.AppConstants.PAYMENT_TYPE_WALLET;
import static co.bytemark.sdk.BytemarkSDK.ResponseCode.CARD_CVV_REQUIRED;
import static co.bytemark.sdk.BytemarkSDK.ResponseCode.DEVICE_LOST_OR_STOLEN;
import static co.bytemark.sdk.BytemarkSDK.ResponseCode.NETWORK_NOT_AVAILABLE;
import static co.bytemark.sdk.BytemarkSDK.ResponseCode.SESSION_EXPIRED;
import static co.bytemark.sdk.BytemarkSDK.ResponseCode.TIME_INCONSISTENCY_DETECTED;
import static co.bytemark.sdk.post_body.PaymentTypes.IDEAL;
import static co.bytemark.sdk.post_body.PaymentTypes.INCOMM;
import static co.bytemark.sdk.post_body.PaymentTypes.PAY_NEAR_ME;

public class AcceptPaymentPresenter extends MvpBasePresenter<AcceptPaymentView> {
	private final BMNetwork bmNetwork;
	private final SharedPreferences sharedPreferences;
	private final PurchaseOrderUseCase purchaseOrderUseCase;
	private final GetPayPalTokenUseCase payPalTokenUseCase;
	private final ConfHelper confHelper;
	private final GetAcceptedPaymentMethodsUseCase getAcceptedPaymentMethodsUseCase;
	private final GooglePayUtil googlePayUtil;
	private final GetPaymentUrlUseCase getPaymentUrlUseCase;
	private final CardPurchaseUseCase cardPurchaseUseCase;
	private final CreateOrderUseCase createOrderUseCase;
	private final ApplyPromoCodeUseCase applyPromoCodeUseCase;
	private GetPaymentMethods getPaymentMethods;
	private List<String> acceptedPaymentMethods;
	private GetPaymentMethodsUseCase getPaymentMethodsUseCase;
	private GetWalletLoadMoneyConfigUseCase getWalletLoadMoneyConfigUseCase;
	private RxEventBus rxEventBus;
	private final DiscountCalculationUseCase discountCalculationUseCase;

	@Inject
	AcceptPaymentPresenter(ConfHelper confHelper,
						   BMNetwork bmNetwork,
						   SharedPreferences sharedPreferences,
						   GooglePayUtil googlePayUtil,
						   PurchaseOrderUseCase purchaseOrderUseCase,
						   GetPaymentUrlUseCase getPaymentUrlUseCase,
						   CardPurchaseUseCase cardPurchaseUseCase,
						   CreateOrderUseCase createOrderUseCase,
						   GetPayPalTokenUseCase payPalTokenUseCase,
						   GetAcceptedPaymentMethodsUseCase getAcceptedPaymentMethodsUseCase,
						   ApplyPromoCodeUseCase applyPromoCodeUseCase,
						   GetPaymentMethodsUseCase getPaymentMethodsUseCase,
						   GetWalletLoadMoneyConfigUseCase getWalletLoadMoneyConfigUseCase,
						   RxEventBus rxEventBus,
						   DiscountCalculationUseCase discountCalculationUseCase) {
		this.confHelper = confHelper;
		this.bmNetwork = bmNetwork;
		this.sharedPreferences = sharedPreferences;
		this.googlePayUtil = googlePayUtil;
		this.purchaseOrderUseCase = purchaseOrderUseCase;
		this.payPalTokenUseCase = payPalTokenUseCase;
		this.getPaymentUrlUseCase = getPaymentUrlUseCase;
		this.cardPurchaseUseCase = cardPurchaseUseCase;
		this.createOrderUseCase = createOrderUseCase;
		this.getAcceptedPaymentMethodsUseCase = getAcceptedPaymentMethodsUseCase;
		this.applyPromoCodeUseCase = applyPromoCodeUseCase;
		this.getPaymentMethodsUseCase = getPaymentMethodsUseCase;
		this.getWalletLoadMoneyConfigUseCase = getWalletLoadMoneyConfigUseCase;
		this.rxEventBus = rxEventBus;
		this.discountCalculationUseCase = discountCalculationUseCase;
	}

	void createPaymentClientForGooglePay(Context context) {
		googlePayUtil.makePaymentClient(context);
	}

	void getWalletLoadMoneyConfig() {
		getWalletLoadMoneyConfigUseCase.singleExecute(new GetWalletLoadMoneyConfigUseCaseValues(),
				new SingleSubscriber<co.bytemark.domain.model.common.Data>() {
					@Override
					public void onSuccess(co.bytemark.domain.model.common.Data data) {
						Timber.d("WalletConfig" + data.getWalletLoadMoneyConfig());
						rxEventBus.postEvent(data);
					}

					@Override
					public void onError(Throwable error) {
						if (getView() != null) {
							getView().showReloadValueFetchError();
						}
					}
				});
	}

	void loadPaymentMethods(String organizationUUID) {
		if (isViewAttached()) {
			getView().showLoadingPaymentMethods();
		}

		unSubscribe();

		if (confHelper.isV2PaymentMethodsEnabled()) {
			getPaymentMethodsUseCase.singleExecute(new GetPaymentMethodsUseCaseValue(organizationUUID),
					new SingleSubscriber<PaymentMethods>() {
						@Override
						public void onSuccess(PaymentMethods paymentMethods) {
							Data data = new Data();
							data.cards = paymentMethods.getCards();
							if (paymentMethods.getPayPalAccounts() != null) {
								data.paypalList = paymentMethods.getPayPalAccounts().getBraintreePaypalPaymentMethods();
							}
							data.ideal = paymentMethods.getIdeal();
							data.googlePay = paymentMethods.getGooglePay();
							data.dotPay = paymentMethods.getDotPay();
							data.payNearMe = paymentMethods.getPayNearMe();
							data.incomm = paymentMethods.getIncomm();
							if (isViewAttached()) {
								getView().setPaymentMethods(data);
							}
						}

						@Override
						public void onError(Throwable error) {
							if (error instanceof BytemarkException) {
								switch (((BytemarkException) error).getStatusCode()) {
									case BytemarkSDK.ResponseCode.DEVICE_LOST_OR_STOLEN:
										if (isViewAttached()) {
											getView().showDeviceLostOrStolenError();
										}
										break;
									case BytemarkSDK.ResponseCode.SESSION_EXPIRED:
										if (isViewAttached()) {
											getView().showSessionExpiredError(error.getMessage());
										}
										break;
									case BytemarkSDK.ResponseCode.SDK_VERSION_OUT_OF_DATE:
										if (isViewAttached()) {
											getView().showAppUpdateDialog();
										}
										break;
									default:
										if (isViewAttached()) {
											getView().showLoadingPaymentMethodsErrorDialog(error.getMessage());
										}
								}
							} else {
								if (isViewAttached()) {
									getView().showLoadingPaymentMethodsErrorDialog(error.getMessage());
								}
							}
						}
					}
			);

		} else {

			getPaymentMethods = bmNetwork.getPaymentMethods(organizationUUID, new BaseNetworkRequestCallback() {
				@Override
				public void onNetworkRequestSuccessfullyCompleted() {
				}

				@Override
				public <T> void onNetworkRequestSuccessWithResponse(ArrayList<T> listOf) {
				}

				@Override
				public void onNetworkRequestSuccessWithResponse(Object object) {
					if (isViewAttached()) {
						getView().setPaymentMethods((Data) object);
					}
				}

				@Override
				public void onNetworkRequestSuccessWithUnexpectedError() {
					if (isViewAttached()) {
						getView().showLoadingPaymentMethodsErrorDialog();
					}
				}

				@Override
				public void onNetworkRequestSuccessWithError(BMError errors) {
					switch (errors.getCode()) {
						case DEVICE_LOST_OR_STOLEN:
							if (isViewAttached())
								getView().showDeviceLostOrStolenError();
							break;
						case SESSION_EXPIRED:
							if (isViewAttached()) {
								getView().showSessionExpiredError(errors.getMessage());
							}
							break;
						case BytemarkSDK.ResponseCode.SDK_VERSION_OUT_OF_DATE:
							if (isViewAttached()) {
								getView().showAppUpdateDialog();
							}
							break;
						default:
							if (isViewAttached())
								getView().showLoadingPaymentMethodsErrorDialog(errors.getMessage());
					}
				}

				@Override
				public void onNetworkRequestSuccessWithDeviceTimeError() {
					if (isViewAttached()) {
						getView().showDeviceTimeErrorDialog();
					}
				}
			});
		}
	}

	public void unSubscribe() {
		if (getPaymentMethods != null) {
			getPaymentMethods.unSubscribe();
		}
	}


	void createOrder(final List<EntityResult> savedEntityResults, final String fareMediaUuid, String reloadingWalletUuid,
					 final boolean isIntentForReload, final Card card, final Card secondCard, final Ideal ideal,
					 final DotPay dotPay, final PayNearMe payNearMe, final Incomm incomm,
					 GooglePay googlePay, Wallet wallet, final int total, final String traceNumber,
					 double lat, double lon, BraintreePaypalPaymentMethod payPal, OfferServe offerServe, String discountServeUuid, boolean isDeeplinkDiscountEnabled, String deeplinkJwtToken) {
		int defaultDeviceStorage = sharedPreferences.getInt(AppConstants.DEFAULT_DEVICE_STORAGE, -1);
		Boolean savePassToDevice = confHelper.savePassToDevice();

		if(defaultDeviceStorage == -1){
			defaultDeviceStorage = savePassToDevice ? 0 : 1;
		}


		if (isViewAttached()) {
			getView().showPlacingOrderView();
		}

		final PurchaseRequestValues requestValues = new PurchaseRequestValues(savedEntityResults, card,
				secondCard, ideal, dotPay, payNearMe, incomm, total, traceNumber, defaultDeviceStorage, lat,
				lon, payPal, googlePay, wallet, offerServe, reloadingWalletUuid, discountServeUuid, isDeeplinkDiscountEnabled, deeplinkJwtToken, confHelper.isPaymentMandatory());

		if (confHelper.isUsingOldOrdersEndPoint()) {
			PurchaseOrderRequestValues requestValue = null;
			if (card == null && !confHelper.isPaymentMandatory()) {
                requestValue = new PurchaseOrderRequestValues(savedEntityResults, fareMediaUuid, isIntentForReload, card, secondCard,
                        total, traceNumber, defaultDeviceStorage, lat, lon, null, null, null, null, offerServe, confHelper.isPaymentMandatory(), reloadingWalletUuid);
			} else if (card != null) {
                requestValue = new PurchaseOrderRequestValues(savedEntityResults, fareMediaUuid, isIntentForReload, card, secondCard,
                        total, traceNumber, defaultDeviceStorage, lat, lon, null, null, null, null, offerServe, confHelper.isPaymentMandatory(), reloadingWalletUuid);
			} else if (payPal != null) {
                requestValue = new PurchaseOrderRequestValues(savedEntityResults, fareMediaUuid, isIntentForReload, null, null,
                        total, traceNumber, defaultDeviceStorage, lat, lon, payPal, null, null, null, offerServe, confHelper.isPaymentMandatory(), reloadingWalletUuid);
            } else if (googlePay != null) {
                requestValue = new PurchaseOrderRequestValues(savedEntityResults, fareMediaUuid, isIntentForReload, null, null,
                        total, traceNumber, defaultDeviceStorage, lat, lon, null, googlePay, null, null, offerServe, confHelper.isPaymentMandatory(), reloadingWalletUuid);
            } else if (incomm != null) {
                requestValue = new PurchaseOrderRequestValues(savedEntityResults, fareMediaUuid, isIntentForReload, null, null,
                        total, traceNumber, defaultDeviceStorage, lat, lon, null, null, incomm, null, offerServe, confHelper.isPaymentMandatory(), reloadingWalletUuid);
            } else if (wallet != null) {
                requestValue = new PurchaseOrderRequestValues(savedEntityResults, fareMediaUuid, isIntentForReload, null, null,
                        total, traceNumber, defaultDeviceStorage, lat, lon, null, null, null, wallet, offerServe, confHelper.isPaymentMandatory(), reloadingWalletUuid);
            }
            purchaseOrderUseCase.singleExecute(requestValue, new SingleSubscriber<co.bytemark.domain.model.common.Data>() {
                @Override
                public void onSuccess(co.bytemark.domain.model.common.Data data) {
                    if (isViewAttached()) {
                        if (confHelper.isUsingOldOrdersEndPoint() && incomm != null) {
                            String accountNumber = "";
                            String iconUrl = "";
                            String orgName = "";
                            if (data.getPayment() != null && data.getPayment().size() > 0) {
                                accountNumber = data.getPayment().get("account_number");
                                iconUrl = data.getPayment().get("icon_url");
                                orgName = data.getPayment().get("org_name");
                            }
                            getView().showOrderSuccessful(new ArrayList<>(), INCOMM, new IncommBarcodeDetail(accountNumber, iconUrl, orgName), data.getOrderUuid());
                        } else {
                            getView().showOrderSuccessful(savedEntityResults, PaymentTypes.STORED_CARD, null, null);
                        }
                    }
				}

				@Override
				public void onError(Throwable throwable) {
					Timber.e(throwable);
					handleError(throwable, card);
				}
			});
		} else if (ideal != null) {
			createOrderUseCase.buildObservable(requestValues)
					.map(data -> Pair.create(data.getOrderUuid(), data.getIdeal().getIssuers()))
					.toSingle()
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(new SingleSubscriber<Pair<String, List<Issuer>>>() {
						@Override
						public void onSuccess(Pair<String, List<Issuer>> orderPair) {
							if (isViewAttached()) {
								getView().showIssuerPopUp(orderPair.first, orderPair.second, requestValues);
							}
						}

						@Override
						public void onError(Throwable throwable) {
							Timber.e(throwable);
							handleError(throwable, null);
						}
					});
		} else if (dotPay != null) {
			createOrderUseCase.buildObservable(requestValues)
					.toSingle()
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(data -> makePayment(data.getOrderUuid(), "", requestValues), throwable -> {
						Timber.e(throwable);
						handleError(throwable, null);
					});

		} else if (payNearMe != null) {
			createOrderUseCase.buildObservable(requestValues)
					.toSingle()
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(data -> {
						makePayment(data.getOrderUuid(), "", requestValues);
					}, throwable -> {
						Timber.e(throwable);
						handleError(throwable, null);
					});

		} else if (incomm != null) {
			createOrderUseCase.buildObservable(requestValues)
					.toSingle()
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(data -> {
						makePayment(data.getOrderUuid(), "", requestValues);
					}, throwable -> {
						Timber.e(throwable);
						handleError(throwable, null);
					});
		} else if ((card != null || payPal != null || googlePay != null || wallet != null) || (total == 0 && !confHelper.isPaymentMandatory())) {
			// For card purchase, we do create order and payments in one use case.
			cardPurchaseUseCase.singleExecute(requestValues, new SingleSubscriber<Boolean>() {
				@Override
				public void onSuccess(Boolean aBoolean) {
					Timber.d(aBoolean.toString());
					if (isViewAttached()) {
						getView().showOrderSuccessful(savedEntityResults, PaymentTypes.STORED_CARD, null, null);
					}
				}

				@Override
				public void onError(Throwable throwable) {
					Timber.e(throwable);
					handleError(throwable, card);
				}
			});
		}
	}

	void makePayment(String orderUuid, String issuerId, PurchaseRequestValues requestValues) {

		final Payment payment = new Payment();

		if (requestValues.makePayment.getPaymentType().equals(IDEAL.name())) {
			List<Payment> payments = new ArrayList<>();
			payment.setIdeal(new Ideal(issuerId));
			payments.add(payment);
			requestValues.makePayment.setPayments(payments);
		}

		final MakePaymentRequestValues makePaymentRequestValues = new MakePaymentRequestValues(orderUuid, requestValues.makePayment, requestValues.deepLinkJwtToken);
		getPaymentUrlUseCase.singleExecute(makePaymentRequestValues,
				new SingleSubscriber<GetPaymentUrlUseCase.ReturnValue>() {
					@Override
					public void onSuccess(GetPaymentUrlUseCase.ReturnValue returnValue) {
						if (isViewAttached()) {

							PaymentTypes paymentType = null;
							if (requestValues.makePayment.getPaymentType().equals(PAY_NEAR_ME.name())) {
								paymentType = PAY_NEAR_ME;
							}
							if (requestValues.makePayment.getPaymentType().equals(INCOMM.name())) {
								paymentType = INCOMM;
							}

							if (TextUtils.isEmpty(returnValue.url)) { // PNM - 0 dollar transaction
								getView().showOrderSuccessful(new ArrayList<>(), paymentType, returnValue.incommBarcodeDetail, orderUuid);
							} else {
								if (paymentType == null) {
									getView().hidePlacingOrderView(); // hide Progress view for iDeal and Dotpay
								}
								getView().launchWebViewPaymentsFlow(paymentType, returnValue.url, returnValue.postData);
							}
						}
					}

					@Override
					public void onError(Throwable throwable) {
						Timber.e(throwable);
						handleError(throwable, null);
					}
				});
	}

	private void handleError(Throwable throwable, Card card) {
		if (isViewAttached()) {
			if (throwable instanceof BytemarkException) {
				final BytemarkException exception = (BytemarkException) throwable;
				switch (exception.getStatusCode()) {
					case TIME_INCONSISTENCY_DETECTED:
						getView().showPlacingOrderWithDeviceTimeError();
					case CARD_CVV_REQUIRED:
						if (card != null)
							getView().showCvvRequiredEditTextDialog(card);
						break;
					case DEVICE_LOST_OR_STOLEN:
						getView().showDeviceLostOrStolenErrorDialog();
						break;
					case SESSION_EXPIRED:
						getView().showSessionExpiredError(exception.getUserFacingMessage());
						break;
					case BytemarkSDK.ResponseCode.SDK_VERSION_OUT_OF_DATE:
						if (isViewAttached()) {
							getView().showAppUpdateDialog();
						}
						break;
					case NETWORK_NOT_AVAILABLE:
						getView().showPlacingOrderWithError();
						break;
					default:
						if (!TextUtils.isEmpty(exception.getUserFacingMessage())) {
							getView().showPlacingOrderWithError(null, exception.getUserFacingMessage());
						} else if (throwable instanceof UnknownHostException || throwable instanceof StreamResetException || throwable instanceof ConnectException) {
							getView().showPlacingOrderWithError();
						} else {
							getView().showPlacingOrderWithError(null, "");
						}
				}
			} else if (throwable instanceof UnknownHostException || throwable instanceof StreamResetException || throwable instanceof ConnectException) {
				getView().showPlacingOrderWithError();
			} else {
				getView().showPlacingOrderWithError(null, "");
			}
		}
	}

	public void getPayPalToken() {
		GetPayPalTokenRequestValue payPalTokenRequestValue = new GetPayPalTokenRequestValue();
		payPalTokenUseCase.singleExecute(payPalTokenRequestValue, new SingleSubscriber<String>() {
			@Override
			public void onSuccess(String s) {
				if (isViewAttached()) {
					getView().initializeBrainTree(s);
				}
			}

			@Override
			public void onError(Throwable throwable) {

			}
		});
	}

	public void getImageResForSupportedPaymentTypes(List<String> supportedCardTypes) {
		List<Integer> imageResources = new ArrayList<>();
		List<String> supportedPaymentTypesAndCards = new ArrayList<>();

		if (supportedCardTypes != null) {
			supportedPaymentTypesAndCards.addAll(supportedCardTypes);

			for (String supportedCardType : supportedCardTypes) {
				switch (supportedCardType.toLowerCase().replaceAll("\\s+", "")) {
					case CARD_TYPE_VISA:
						imageResources.add(R.drawable.visa_small);
						break;
					case CARD_TYPE_AMERICAN_EXPRESS:
						imageResources.add(R.drawable.american_express_small);
						break;
					case CARD_TYPE_MASTERCARD:
						imageResources.add(R.drawable.mastercard_small);
						break;
					case CARD_TYPE_DISCOVER:
						imageResources.add(R.drawable.discover_small);
						break;
					case CARD_TYPE_JCB:
						imageResources.add(R.drawable.bt_ic_vaulted_jcb);
						break;
					case CARD_TYPE_DINERS_CLUB:
						imageResources.add(R.drawable.dinersclub_light);
						break;
					case CARD_TYPE_UNION_PAY:
						imageResources.add(R.drawable.unionpay_light);
						break;
					case CARD_TYPE_SOLO_DEBIT:
						imageResources.add(R.drawable.solo_dark);
						break;
					case CARD_TYPE_CIRRUS_DEBIT:
						imageResources.add(R.drawable.cirrus_light);
						break;
					case CARD_TYPE_MAESTRO:
						imageResources.add(R.drawable.ic_maestro);
						break;
					case PAYMENT_TYPE_PAYPAL:
						imageResources.add(R.drawable.bt_ic_paypal);
						break;
					case PAYMENT_TYPE_GOOGLE_PAY:
						imageResources.add(R.drawable.ic_google_pay_mark);
						break;
					case PAYMENT_TYPE_IDEAL:
						imageResources.add(R.drawable.ic_ideal);
						break;
					case PAYMENT_TYPE_PAY_NEAR_ME:
						imageResources.add(R.drawable.pay_near_me_logo);
						break;
					case PAYMENT_TYPE_DOT_PAY:
						imageResources.add(R.drawable.ic_dot_pay);
						break;
					case PAYMENT_TYPE_WALLET:
						imageResources.add(R.drawable.ic_wallet);
						break;
					case PAYMENT_TYPE_INCOMM:
						imageResources.add(R.drawable.ic_incomm);
						break;
					default:
						break;
				}
			}
		}
		supportedPaymentTypesAndCards.remove("ApplePay");
		if (isViewAttached())
			getView().showStripView(imageResources, supportedPaymentTypesAndCards);

		for (String paymentMethod : supportedPaymentTypesAndCards) {
			if (paymentMethod.toLowerCase().contains(AppConstants.PAYMENT_TYPE_PAYPAL)) {
				if (isViewAttached()) getView().setAddPaymentMethodsScreenPresent(true);
			}
		}
	}

	public void getAcceptedPaymentMethods(List<EntityResult> savedEntityResults, Wallet reloadingWallet) {
		if (BytemarkSDK.isLoggedIn()) {
			String organizationUuid;
			if (savedEntityResults != null && savedEntityResults.size() > 0) {
				organizationUuid = savedEntityResults.get(0).getOrganization().getLegacyUuid();
			} else {
				organizationUuid = confHelper.getOrganization().getUuid();
			}
			GetAcceptedPaymentMethodsUseCaseValue getAcceptedPaymentMethodsUseCaseValue = new GetAcceptedPaymentMethodsUseCaseValue(organizationUuid);
			getAcceptedPaymentMethodsUseCase.singleExecute(getAcceptedPaymentMethodsUseCaseValue, new SingleSubscriber<List<String>>() {
				@Override
				public void onSuccess(List<String> strings) {
					if (reloadingWallet != null) strings.remove("WalletPay");
					acceptedPaymentMethods = strings;
					if (isViewAttached())
						getView().setAcceptedPaymentMethods(new ArrayList<>(strings));
					getImageResForSupportedPaymentTypes(strings);
				}

				@Override
				public void onError(Throwable error) {
					if (error instanceof UnknownHostException) {
					} else {
						getImageResForSupportedPaymentTypes(null);
					}
				}
			});
		} else
			getImageResForSupportedPaymentTypes(null);
	}

	protected void isGooglePayReady() {
		googlePayUtil.getGooglePayReadyTask().addOnCompleteListener(
				task1 -> {
					try {
						boolean result = task1.getResult(ApiException.class);
						if (isViewAttached())
							getView().setGooglePayReady(result);
					} catch (ApiException exception) {
					}
				});
	}

	protected PaymentsClient getPaymentClient() {
		return googlePayUtil.getPaymentClient();
	}

	protected PaymentDataRequest getPaymentDataRequest(String totalPrice) {

        return googlePayUtil.getPaymentDataRequest(totalPrice,
                confHelper.getConfigurationPurchaseOptionsCurrencyCode(), acceptedPaymentMethods,
                confHelper.getGooglePayConfiguration() != null ? confHelper.getGooglePayConfiguration().getMerchantId() : "",
                confHelper.getGooglePayConfiguration() != null ? confHelper.getGooglePayConfiguration().getGateway() : ""
        );
    }

	protected void applyPromoCode(String offerCode, List<EntityResult> savedEntityResults) {
		if (isViewAttached())
			getView().setPromoCodeProgressViewVisibility(VISIBLE);

		ApplyPromoCode applyPromoCode = new ApplyPromoCode();
		List<Item> items = new ArrayList<>();
		for (EntityResult entityResult : savedEntityResults) {
			Item item = new Item();
			item.setProductUuid(entityResult.getUuid());
			item.setPrice(entityResult.getSalePrice());
			item.setQty(entityResult.getQuantity());
			items.add(item);
		}
		applyPromoCode.setItems(items);
		applyPromoCode.setOfferCodeText(offerCode);
		applyPromoCode.setTotal(String.valueOf(getTotal(savedEntityResults)));
		applyPromoCodeUseCase.singleExecute(new ApplyPromoCodeRequestValues(applyPromoCode), new SingleSubscriber<OfferServe>() {
			@Override
			public void onSuccess(OfferServe offerServe) {
				Timber.d("PromoCode Success");
				if (isViewAttached()) {
					getView().setPromoCodeProgressViewVisibility(GONE);
					getView().showPromoCodeSuccess(offerServe);
				}
			}

			@Override
			public void onError(Throwable error) {
				Timber.e("PromoCode Error", error);
				if (isViewAttached()) {
					getView().setPromoCodeProgressViewVisibility(GONE);
					getView().showPromoCodeError(error.getMessage());
				}
			}
		});
	}

	private int getTotal(List<EntityResult> savedEntityResults) {
		int total = 0;
		if (savedEntityResults != null) {
			for (int i = 0; i < savedEntityResults.size(); i++) {
				int salePrice = savedEntityResults.get(i).getSalePrice();
				int quantity = savedEntityResults.get(i).getQuantity();
				total += salePrice * quantity;
			}
		}
		return total;
	}

	int getDiscountPrice(OfferServe offerServe, List<EntityResult> savedEntityResult) {
		int discount = 0;
		if (offerServe != null && offerServe.getOfferConfig() != null &&
				offerServe.getOfferConfig().getPriceAdjustments() != null) {
			for (PriceAdjustment priceAdjustment : offerServe.getOfferConfig().getPriceAdjustments()) {
				int discountForSingleQuantity = priceAdjustment.getAdjustedPrice();
				//Check if discount is for all quantity of the product(usage type = MULTIPLE_PRODUCT_USAGE)
				if (offerServe.getOfferConfig().getUsageType().equals(AppConstants.MULTIPLE_PRODUCT_USAGE)) {
					for (EntityResult entityResult : savedEntityResult) {
						if (entityResult.getUuid().equals(priceAdjustment.getProductUuid())) {
							discount += discountForSingleQuantity * entityResult.getQuantity();
						}
					}
				} else {
					discount += discountForSingleQuantity;
				}
			}
		}
		return discount;
	}

	void discountCalculation(DiscountCalculationRequest discountCalculationRequest, String deeplinkJwtToken) {
		LifecycleOwner view = null;
		if (isViewAttached()) {
			view = (LifecycleOwner) getView();
		}
		discountCalculationUseCase.getLiveData(new DiscountCalculationUseCase.DiscountCalculationUseCaseValue(discountCalculationRequest, deeplinkJwtToken))
				.observe(view, result -> {
					if (result instanceof Result.Loading) {
						if (isViewAttached()) {
							getView().showLoading();
						}
					}

					if (result instanceof Result.Success) {
						if (isViewAttached()) {
							getView().hideLoading();
							getView().showUpdatedSummaryWithDiscount(((Result.Success<DiscountData>) result).getData());
						}
					}

					if (result instanceof Result.Failure) {
						co.bytemark.domain.model.common.BMError error = ((Result.Failure<DiscountData>) result).getBmError().get(0);
						if (isViewAttached()) {
							getView().handleErrorOnDiscountCalculation(error.getMessage());
						}
					}
				});
	}
}
