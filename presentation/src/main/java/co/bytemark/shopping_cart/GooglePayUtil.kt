package co.bytemark.shopping_cart

import android.content.Context
import co.bytemark.BuildConfig
import co.bytemark.helpers.AppConstants.CARD_TYPE_AMERICAN_EXPRESS
import co.bytemark.helpers.AppConstants.CARD_TYPE_DISCOVER
import co.bytemark.helpers.AppConstants.CARD_TYPE_JCB
import co.bytemark.helpers.AppConstants.CARD_TYPE_MASTERCARD
import co.bytemark.helpers.AppConstants.CARD_TYPE_VISA
import com.google.android.gms.tasks.Task
import com.google.android.gms.wallet.*
import com.google.android.gms.wallet.Wallet.WalletOptions
import java.util.*

class GooglePayUtil {
    var paymentClient: PaymentsClient? = null
        private set

    fun makePaymentClient(context: Context?) {
        var environment = WalletConstants.ENVIRONMENT_TEST
        if (BuildConfig.BUILD_TYPE == "release") {
            environment = WalletConstants.ENVIRONMENT_PRODUCTION
        }
        paymentClient = context?.let {
            Wallet.getPaymentsClient(
                it, WalletOptions.Builder()
                    .setEnvironment(environment)
                    .build()
            )
        }
    }

    fun getPaymentDataRequest(
        totalPrice: String?,
        currencyCode: String?,
        supportedCardTypes: List<String>?,
        gatewayMerchantId: String?,
        gateway: String?
    ): PaymentDataRequest {
        val request =
            PaymentDataRequest.newBuilder()
                .setTransactionInfo(
                    TransactionInfo.newBuilder()
                        .setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL)
                        .setTotalPrice(totalPrice!!)
                        .setCurrencyCode(currencyCode!!)
                        .build()
                )
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                .setCardRequirements(
                    CardRequirements.newBuilder()
                        .addAllowedCardNetworks(getAllowedCardNetworks(supportedCardTypes))
                        .build()
                )
        val params =
            PaymentMethodTokenizationParameters.newBuilder()
                .setPaymentMethodTokenizationType(
                    WalletConstants.PAYMENT_METHOD_TOKENIZATION_TYPE_PAYMENT_GATEWAY
                )
                .addParameter("gateway", gateway!!)
                .addParameter("gatewayMerchantId", gatewayMerchantId!!)
                .build()
        request.setPaymentMethodTokenizationParameters(params)
        return request.build()
    }

    private fun getAllowedCardNetworks(supportedCardTypes: List<String>?): List<Int> {
        val allowedCardNetworks: MutableSet<Int> = HashSet()
        if (supportedCardTypes != null && supportedCardTypes.isNotEmpty()) {
            for (supportedCardType in supportedCardTypes) {
                when (supportedCardType.toLowerCase().replace("\\s+".toRegex(), "")) {
                    CARD_TYPE_VISA -> allowedCardNetworks.add(WalletConstants.CARD_NETWORK_VISA)
                    CARD_TYPE_AMERICAN_EXPRESS -> allowedCardNetworks.add(WalletConstants.CARD_NETWORK_AMEX)
                    CARD_TYPE_MASTERCARD -> allowedCardNetworks.add(WalletConstants.CARD_NETWORK_MASTERCARD)
                    CARD_TYPE_DISCOVER -> allowedCardNetworks.add(WalletConstants.CARD_NETWORK_DISCOVER)
                    CARD_TYPE_JCB -> allowedCardNetworks.add(WalletConstants.CARD_NETWORK_JCB)
                    else -> {
                    }
                }
            }
        } else {
            return allCardNetworks
        }
        return if (allowedCardNetworks.size == 0) {
            allCardNetworks
        } else ArrayList(allowedCardNetworks)
    }

    private val allCardNetworks: List<Int>
        get() = listOf(
            WalletConstants.CARD_NETWORK_VISA,
            WalletConstants.CARD_NETWORK_AMEX,
            WalletConstants.CARD_NETWORK_MASTERCARD,
            WalletConstants.CARD_NETWORK_JCB,
            WalletConstants.CARD_NETWORK_DISCOVER
        )

    val googlePayReadyTask: Task<Boolean>
        get() {
            val request = IsReadyToPayRequest.newBuilder()
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                .build()
            return paymentClient?.isReadyToPay(request) as Task<Boolean>
        }
}