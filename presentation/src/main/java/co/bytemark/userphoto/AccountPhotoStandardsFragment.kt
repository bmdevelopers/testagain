package co.bytemark.userphoto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.widgets.util.replaceFragment
import kotlinx.android.synthetic.main.fragment_user_photo_standards.*

class AccountPhotoStandardsFragment : BaseMvvmFragment() {

    override fun onInject() {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_user_photo_standards, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        buttonNext.setOnClickListener {
            (context as AppCompatActivity).replaceFragment(AccountPhotoUploadFragment(), R.id.fragment, true)
        }
    }
}