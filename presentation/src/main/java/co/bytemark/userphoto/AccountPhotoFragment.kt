package co.bytemark.userphoto

import android.content.Context
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.userphoto.UserPhoto
import co.bytemark.widgets.util.getViewModel
import co.bytemark.widgets.util.replaceFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.fragment_account_photo.*

class AccountPhotoFragment : BaseMvvmFragment() {

    override fun onInject() {
        component.inject(this)
    }

    lateinit var viewModel: AccountPhotoViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_account_photo, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = getViewModel(CustomerMobileApp.appComponent.accountPhotoViewModel)
        emptyStateLayout?.showLoading(R.drawable.ic_camera_filled, R.string.loading)
        onUploadPhoto()
        observeSetUserPhotoLiveData(view.context)
    }

    private fun observeSetUserPhotoLiveData(context: Context) {
        viewModel.setUserPhotoLiveData.observe(this, Observer {
            setUserPhoto(it, context)
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadUserPhoto()
    }

    private fun onUploadPhoto() {
        buttonUploadPhoto.setOnClickListener {
            (context as AppCompatActivity).replaceFragment(AccountPhotoStandardsFragment(), R.id.fragment, true)
        }
    }

    private fun setUserPhoto(userPhoto: UserPhoto?, context: Context) {
        emptyStateLayout.showContent()
        when (userPhoto?.status) {
            UserPhoto.REJECTED -> {
                textViewStatus.setText(R.string.rejected)
                textViewStatus.setTextColor(ContextCompat.getColor(context, R.color.v3_photo_red))
                textViewDescription.setText(R.string.account_photo_rejected)
                buttonUploadPhoto.visibility = VISIBLE
            }
            UserPhoto.PENDING -> {
                textViewStatus.setText(R.string.pending)
                textViewStatus.setTextColor(ContextCompat.getColor(context, R.color.v3_photo_yellow))
                textViewDescription.setText(R.string.account_photo_pending)
                buttonUploadPhoto.visibility = GONE
            }
            UserPhoto.ACCEPTED -> {
                textViewStatus.setText(R.string.accepted)
                textViewStatus.setTextColor(ContextCompat.getColor(context, R.color.v3_photo_green))
                textViewDescription.setText(R.string.account_photo_accepted)
                buttonUploadPhoto.visibility = GONE
            }
            UserPhoto.MISSING -> setUserPhotoMissing()
            else -> {
                setUserPhotoMissing()
            }
        }
        if (userPhoto?.data?.isEmpty() == false) {
            val decodedByteArray = Base64.decode(userPhoto.data, Base64.DEFAULT)
            decodedByteArray?.let {
                Glide.with(activity)
                        .load(it)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .into(imageViewAccountPhoto)
            }
        }
    }

    private fun setUserPhotoMissing() {
        context?.let {
            textViewStatus.setText(R.string.account_photo_missing_text)
            textViewStatus.setTextColor(ContextCompat.getColor(it, R.color.v3_photo_gray))
            buttonUploadPhoto.visibility = VISIBLE
            textViewDescription.setText(R.string.account_photo_missing)
        }

    }

}