package co.bytemark.userphoto

import androidx.lifecycle.MutableLiveData
import co.bytemark.CustomerMobileApp
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.interactor.userphoto.GetUserPhoto
import co.bytemark.domain.interactor.userphoto.PhotoRequestValues
import co.bytemark.domain.interactor.userphoto.SetUserPhoto
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.userphoto.UserPhoto
import kotlinx.coroutines.launch
import javax.inject.Inject

class AccountPhotoViewModel @Inject constructor(
        private val getUserPhoto: GetUserPhoto,
        private val setUserPhoto: SetUserPhoto) : BaseViewModel() {

    init {
        CustomerMobileApp.component.inject(this)
    }

    val setUserPhotoLiveData by lazy {
        MutableLiveData<UserPhoto>()
    }

    val uploadUserPhoto by lazy {
        MutableLiveData<UserPhoto>()
    }


    fun uploadUserPhoto(filePath: String) = uiScope.launch {
        when (val result = setUserPhoto(PhotoRequestValues(filePath))) {
            is Result.Success -> {
                uploadUserPhoto.value = result.data?.userPhoto
            }
            is Result.Failure -> {
                errorLiveData.value = result.bmError.first()
            }
        }
    }

    fun loadUserPhoto() = uiScope.launch {
        when (val result = getUserPhoto(UseCase.EmptyRequestValues())) {
            is Result.Success -> {
                setUserPhotoLiveData.value = result.data?.userPhoto
            }
            is Result.Failure -> {
                setUserPhotoLiveData.value = UserPhoto(status = UserPhoto.MISSING)
            }
        }
    }

}