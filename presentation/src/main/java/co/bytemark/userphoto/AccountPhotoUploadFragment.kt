package co.bytemark.userphoto

import android.Manifest.permission
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.util.getViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.yalantis.ucrop.UCrop
import kotlinx.android.synthetic.main.fragment_user_photo_upload.*
import java.io.File
import java.io.IOException

class AccountPhotoUploadFragment : BaseMvvmFragment() {

    companion object {
        private const val REQUEST_CAMERA = 203
        private const val REQUEST_STORAGE_READ_ACCESS_PERMISSION = 204
    }

    private var cameraPhotoPathString: String? = null
    private var uCropPhotoPathString: String? = null

    private var bottomSheetBehavior: BottomSheetBehavior<*>? = null
    private var destination: Uri? = null

    private lateinit var viewModel: AccountPhotoViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_user_photo_upload, container, false)

    override fun onInject() {
        component.inject(this)
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = getViewModel(CustomerMobileApp.appComponent.accountPhotoViewModel)
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        onSelectPhoto()
        onTakePhotoClickListenerOrOnSelectFromGallery()
        onUploadPhoto()
        observeUploadPhotoLiveData()
        observeErrorLiveData()
    }

    private fun observeUploadPhotoLiveData() {
        viewModel.uploadUserPhoto.observe(this, Observer {
            navigateToAccountPhotoFragment()
        })
    }

    private fun observeErrorLiveData() {
        viewModel.errorLiveData.observe(this, Observer {
            if (isOnline()) {
                handleError(it!!)
            }
        })
    }

    private fun onSelectPhoto() {
        buttonSelectPhoto.setOnClickListener { bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED }
        buttonTryAgain.setOnClickListener { bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED }
    }

    private fun onTakePhotoClickListenerOrOnSelectFromGallery() {
        textViewTakePhoto.setOnClickListener {
            if (!mayRequestCamera()) {
                Snackbar.make(textViewTakePhoto, R.string.camera_permission_not_granted, Snackbar.LENGTH_LONG)
            } else {
                dispatchTakePictureIntent()
            }
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        }
        textViewSelectPhotoFromGallery.setOnClickListener {
            if (!mayRequestSelectPicture()) {
                Snackbar.make(textViewTakePhoto, R.string.storage_read_permission_not_granted, Snackbar.LENGTH_LONG)
            } else {
                pickFromGallery()
            }
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(activity!!.packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFileForCamera(context)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (photoFile != null) {
                val photoURI = FileProvider.getUriForFile(context as Activity, getString(R.string.file_provider_authorities), photoFile)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                startActivityForResult(takePictureIntent, AppConstants.REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    private fun pickFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), AppConstants.REQUEST_SELECT_PICTURE)
    }

    private fun mayRequestCamera(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        }
        if (activity?.checkSelfPermission(permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        if (shouldShowRequestPermissionRationale(permission.CAMERA)) {
            requestPermissions(arrayOf(permission.CAMERA), REQUEST_CAMERA)
        } else {
            requestPermissions(arrayOf(permission.CAMERA), REQUEST_CAMERA)
        }
        return false
    }

    private fun mayRequestSelectPicture(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        }
        if (activity?.checkSelfPermission(permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        if (shouldShowRequestPermissionRationale(permission.CAMERA)) {
            requestPermissions(arrayOf(permission.READ_EXTERNAL_STORAGE), REQUEST_STORAGE_READ_ACCESS_PERMISSION)
        } else {
            requestPermissions(arrayOf(permission.READ_EXTERNAL_STORAGE), REQUEST_STORAGE_READ_ACCESS_PERMISSION)
        }
        return false
    }

    private fun onUploadPhoto() {
        buttonUploadPhoto.setOnClickListener {
            if (destination != null) {
                if (isOnline()) {
                    emptyStateLayout?.showLoading(R.drawable.ic_camera_filled, R.string.payment_method_saving)
                    viewModel.uploadUserPhoto(destination?.path!!)
                } else {
                    emptyStateLayout?.showError(R.drawable.warning_material, R.string.network_connectivity_error, R.string.network_connectivity_error_message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AppConstants.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            val source = Uri.fromFile(File(getCameraPhotoPath()))
            if (source != null) {
                startCropActivity(this, source)
            } else {
                view?.let { Snackbar.make(it, R.string.something_went_wrong, Snackbar.LENGTH_LONG).show() }
            }
        } else if (requestCode == UCrop.REQUEST_CROP && resultCode == Activity.RESULT_OK && data != null) {
            destination = UCrop.getOutput(data)
            if (destination != null) {
                textViewTitle?.setText(R.string.confirm_your_photo)
                textViewDescription?.setText(R.string.confirm_your_photo_desc)
                imageViewPhoto?.setImageURI(destination)
                imageViewCamera?.visibility = View.GONE
                buttonSelectPhoto?.visibility = View.GONE
                buttonUploadPhoto?.visibility = View.VISIBLE
                buttonTryAgain?.visibility = View.VISIBLE
            } else {
                view?.let { Snackbar.make(it, R.string.something_went_wrong, Snackbar.LENGTH_LONG) }
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            view?.let { Snackbar.make(it, data?.let { data -> UCrop.getError(data) }?.message!!, Snackbar.LENGTH_SHORT).show() }
        } else if (requestCode == AppConstants.REQUEST_SELECT_PICTURE && resultCode == Activity.RESULT_OK) {
            val selectedUri = data?.data
            if (selectedUri != null) {
                startCropActivity(this, data.data)
            } else {
                view?.let { Snackbar.make(it, R.string.toast_cannot_retrieve_selected_image, BaseTransientBottomBar.LENGTH_SHORT).show() }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CAMERA -> if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent()
            }
            REQUEST_STORAGE_READ_ACCESS_PERMISSION -> if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickFromGallery()
            }
        }
    }

    override fun onOnline() {
        emptyStateLayout?.showContent()
    }

    override fun onOffline() {
        emptyStateLayout?.showError(R.drawable.warning_material, R.string.network_connectivity_error, R.string.network_connectivity_error_message)
    }

    private fun navigateToAccountPhotoFragment() {
        activity?.supportFragmentManager?.popBackStack(AccountPhotoStandardsFragment::class.java.simpleName, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    @Throws(IOException::class)
    fun createImageFileForCamera(context: Context?): File? {
        return createImageFile(context as Activity, "camera")
    }

    @Throws(IOException::class)
    fun createImageFileForUcrop(context: Context?): File? {
        return createImageFile(context as Activity, "ucrop")
    }

    @Throws(IOException::class)
    fun createImageFile(context: Context, type: String): File? {
        // Create an image file name
        val appName = context.getString(R.string.app_name).replace(" ", "_")
        val imageFileName = appName + "_" + type
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        if (type.equals("camera", ignoreCase = true)) {
            cameraPhotoPathString = image.absolutePath
        } else {
            uCropPhotoPathString = image.absolutePath
        }
        return image
    }

    private fun getCameraPhotoPath(): String? {
        return cameraPhotoPathString
    }

    private fun startCropActivity(fragment: Fragment, source: Uri?) {
        var destinationFile: File? = null
        try {
            destinationFile = createImageFileForUcrop(context as Activity)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        if (destinationFile != null) {
            val destination = Uri.fromFile(destinationFile)
            val options = UCrop.Options()
            options.setStatusBarColor(confHelper.statusBarColor)
            options.setToolbarColor(confHelper.headerThemeBackgroundColor)
            source?.let {
                UCrop.of(it, destination)
                        .withAspectRatio(1f, 1f)
                        .withMaxResultSize(600, 600)
                        .withOptions(options)
                        .start(context as Activity, fragment)
            }
        }
    }


}