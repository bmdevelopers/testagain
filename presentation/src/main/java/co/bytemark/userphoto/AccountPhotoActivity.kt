package co.bytemark.userphoto

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.sdk.model.common.Action
import co.bytemark.widgets.util.replaceFragment

class AccountPhotoActivity : MasterActivity() {

    override fun useHamburgerMenu(): Boolean = false

    override fun getLayoutRes(): Int = R.layout.activity_user_photo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setNavigationViewCheckedItem(Action.SETTINGS)
        replaceFragment(AccountPhotoFragment(), R.id.fragment)
    }
}