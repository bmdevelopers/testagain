package co.bytemark.utils.network

import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.init_fare_capping.InitFareCapping
import co.bytemark.domain.model.init_fare_capping.InitFareCappingData
import co.bytemark.sdk.Api.ApiUtility
import com.google.gson.Gson
import okhttp3.*
import java.util.*

/**
 * Created by ranjith on 04/06/20
 */


val farecapping1 = InitFareCapping(
        1, "Testing 1",
        ApiUtility.getSFromCalendar(Calendar.getInstance()),
        ApiUtility.getSFromCalendar((Calendar.getInstance().also { it.add(Calendar.DAY_OF_YEAR, 10) })), 500,
        300, 200, "Demo FareCapping created for testing",
        40.0f
)
val farecapping2 = InitFareCapping(
        2, "Testing 2",
        ApiUtility.getSFromCalendar((Calendar.getInstance().also { it.add(Calendar.DAY_OF_YEAR, 3) })),
        ApiUtility.getSFromCalendar((Calendar.getInstance().also { it.add(Calendar.DAY_OF_YEAR, 33) })), 1000,
        0, 1000, "Demo FareCapping created for testing",
        100.0f
)

val farecapping3 = InitFareCapping(
    3, "Testing NickName",
    ApiUtility.getSFromCalendar((Calendar.getInstance().also { it.add(Calendar.DAY_OF_YEAR, 3) })),
    ApiUtility.getSFromCalendar((Calendar.getInstance().also { it.add(Calendar.DAY_OF_YEAR, 33) })), 1000,
    0, 0, "Demo FareCapping",
    0f
)

val fareCappingSuccessResponse = co.bytemark.domain.model.common.Response<InitFareCappingData>(
        serverTime = ApiUtility.getSFromCalendar(Calendar.getInstance()),
        data = InitFareCappingData(listOf(farecapping1, farecapping2,farecapping3))
)

val fareCappingFailureResponse = co.bytemark.domain.model.common.Response<InitFareCappingData>(
        serverTime = ApiUtility.getSFromCalendar(Calendar.getInstance()),
        errors = listOf(BMError(1, "Invalid faremedium id")),
        data = InitFareCappingData(emptyList())
)

val responseList = listOf(fareCappingSuccessResponse, fareCappingFailureResponse)

class FareCappingInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        try {
            val uri = chain.request().url().uri()
            if (uri.path.contains("fare_cappingpots")) {
                val result = Gson().toJson(responseList.shuffled().take(1).run { first() })
                return Response.Builder()
                        .code(200)
                        .protocol(Protocol.HTTP_2)
                        .message(result)
                        .request(chain.request())
                        .body(ResponseBody.create(MediaType.parse("application/json")
                                , result.toByteArray()))
                        .addHeader("content-type", "application/json")
                        .build()
            }
        } catch (e: Exception) {
            return chain.proceed(chain.request())
        }
        return chain.proceed(chain.request())
    }

}