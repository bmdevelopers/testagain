package co.bytemark.utils.network

import android.os.SystemClock
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.manage.Institution
import co.bytemark.domain.model.manage.InstitutionListData
import co.bytemark.domain.model.manage.UPassEligibilityResponseData
import co.bytemark.sdk.Api.ApiUtility
import com.google.gson.Gson
import okhttp3.*
import java.util.*
import kotlin.random.Random

val INSTITUTION_LIST_SUCCESS_ONE = co.bytemark.domain.model.common.Response<InstitutionListData>(
        errors = listOf(),
        serverTime = ApiUtility.getSFromCalendar(Calendar.getInstance()),
        data = InstitutionListData(listOf(
                Institution("1234", "Institution 1")
        ))
)

val INSTITUTION_LIST_SUCCESS_TWO = co.bytemark.domain.model.common.Response<InstitutionListData>(
        errors = listOf(),
        serverTime = ApiUtility.getSFromCalendar(Calendar.getInstance()),
        data = InstitutionListData(listOf(
                Institution("1234", "Institution 1"),
                Institution("1233", "Institution 2")
        ))
)

val INSTITUTION_LIST_FAILURE = co.bytemark.domain.model.common.Response<InstitutionListData>(
        errors = listOf(BMError(12344, "No associated institution found under this account")),
        serverTime = ApiUtility.getSFromCalendar(Calendar.getInstance()),
        data = InstitutionListData(listOf())
)


val VALIDATION_SUCCESS = co.bytemark.domain.model.common.Response<UPassEligibilityResponseData>(
        errors = listOf(),
        serverTime = ApiUtility.getSFromCalendar(Calendar.getInstance()),
        data = UPassEligibilityResponseData("You have beed Successfully validated.")
)

val VALIDATION_FAILURE = co.bytemark.domain.model.common.Response<UPassEligibilityResponseData>(
        errors = listOf(BMError(12342, "The transit benifits have already applied to another card")),
        serverTime = ApiUtility.getSFromCalendar(Calendar.getInstance()),
        data = UPassEligibilityResponseData("")
)


class UPassAPIInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        try {
            val uri = chain.request().url().uri()
            val status = Random(System.currentTimeMillis()).nextInt() % 3
            val gson = Gson()
            if (uri.path.contains("validate")) {
                val result = when (status) {
                    0 -> gson.toJson(VALIDATION_FAILURE)
                    else -> gson.toJson(VALIDATION_SUCCESS)
                }
                SystemClock.sleep(2000)
                return Response.Builder()
                        .code(200)
                        .protocol(Protocol.HTTP_2)
                        .message(result)
                        .request(chain.request())
                        .body(ResponseBody.create(MediaType.parse("application/json")
                                , result.toByteArray()))
                        .addHeader("content-type", "application/json")
                        .build()

            } else if (uri.path.contains("institution_accounts")) {
                val result = when (status) {
                    0 -> gson.toJson(INSTITUTION_LIST_FAILURE)
                    1 -> gson.toJson(INSTITUTION_LIST_SUCCESS_ONE)
                    else -> gson.toJson(INSTITUTION_LIST_SUCCESS_TWO)
                }
                SystemClock.sleep(2000)
                return Response.Builder()
                        .code(200)
                        .protocol(Protocol.HTTP_2)
                        .message(result)
                        .request(chain.request())
                        .body(ResponseBody.create(MediaType.parse("application/json")
                                , result.toByteArray()))
                        .addHeader("content-type", "application/json")
                        .build()
            }
        } catch (e: Exception) {
            return chain.proceed(chain.request())
        }
        return chain.proceed(chain.request())
    }
}