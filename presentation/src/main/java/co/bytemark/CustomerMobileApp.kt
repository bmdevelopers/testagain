package co.bytemark

import android.app.Application
import android.content.Context
import android.os.StrictMode
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.custom_tabs.CustomTabsActivityLifecycleCallbacks
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.sdk.Bytemark
import co.bytemark.di.Injector
import co.bytemark.di.components.AppComponent
import co.bytemark.di.components.DaggerAppComponent
import co.bytemark.di.modules.*
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.ConfHelper
import co.bytemark.helpers.RatingAndFeedBackHelper
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.BytemarkSDK.fixUrl
import co.bytemark.sdk.RestClientImpl
import co.bytemark.sdk.model.config.Conf
import co.bytemark.widgets.dynamicmenu.MenuClickManager
import co.bytemark.widgets.util.Util
import co.bytemark.widgets.util.getActionBarHeight
import co.bytemark.widgets.util.navigationDrawerWidth
import com.google.firebase.iid.FirebaseInstanceId
import net.sqlcipher.database.SQLiteDatabase
import timber.log.Timber
import java.io.UnsupportedEncodingException
import java.lang.ref.WeakReference
import java.lang.reflect.Constructor
import java.lang.reflect.InvocationTargetException
import javax.inject.Inject

class CustomerMobileApp : Application() {

    @Inject
    lateinit var networkManager: NetworkManager

    @Inject
    lateinit var confHelper: ConfHelper

    override fun onCreate() {
        super.onCreate()
        SQLiteDatabase.loadLibs(this)
        Bytemark.sharedInstance().init(this)
        Bytemark.sharedInstance().setFeaturesConfig("conf.json")
        appReference = WeakReference(this)

        registerActivityLifecycleCallbacks(CustomTabsActivityLifecycleCallbacks())

        //initStrictMode()
        initDebugTools()
        initBytemarkEnvironment()
        iniRestClientImpl()
        initBytemarkSDK()

        appComponent = createAppComponent()
        appComponent.inject(this)
        networkManager.start()

        initConfFile()
        initHacon()
        updateSession()
        BytemarkSDK.SDKUtility.checkAndLogFirstOpen()
        if(confHelper.isElertSdkEnabled()) {
            initECUISdk()
            if(BytemarkSDK.isLoggedIn())
                Util.regUserAndOrgForECUI(this)

            if(BytemarkSDK.SDKUtility.isDeviceTokenSentToElerts()) {
                Util.invokeECUIUtilMethod(AppConstants.ELERTS_API_HELPER_METHOD_NAME, this, null)
            }

            Util.getOrgs(this, null, false) //Notify Elert on language changes
        }
    }

    private fun initECUISdk() {
        try {
            val cls = Class.forName(AppConstants.ELERTS_MESSAGE_CLASS_NAME)
            val constructor: Constructor<*> =
                cls.getConstructor(Application::class.java)
            constructor.newInstance(this)
            val methodName = AppConstants.ELERTS_INIT_CONTEXT_METHOD_NAME
            val method = cls.getMethod(methodName, Context::class.java)
            method.invoke(null, this)
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }
    }

    private fun initConfFile() {
        setConf(confHelper.conf)
    }

    override fun onTerminate() {
        super.onTerminate()
        networkManager.stop()
    }

    private fun createAppComponent(): AppComponent {
        return DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .apiModule(ApiModule())
                .networkModule(NetworkModule(overtureURL, fixUrl(BuildConfig.ACCOUNTS_URL)))
                .uiModule(UiModule())
                .configModule(ConfigModule(BytemarkSDK.SDKUtility.getRemoteConfigHelper()))
                .build()
    }

    private fun initStrictMode() {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()
                    .penaltyLog()
                    .build())
            StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .build())
        }
    }

    private fun initDebugTools() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initBytemarkEnvironment() {
        BytemarkSDK.setupEnvironment(BuildConfig.DISPLAY_NAME,
                BuildConfig.CLIENT_ID,
                fixUrl(BuildConfig.OVERTURE_URL),
                fixUrl(BuildConfig.ACCOUNTS_URL),
                fixUrl(BuildConfig.ACCOUNTS_URL) + "oauth/client/callback")
    }

    private fun iniRestClientImpl() {
        RestClientImpl.setApplication(this, getConf(), BuildConfig.DEBUG)
    }

    private fun initBytemarkSDK() {
        try {
            BytemarkSDK.initInBackground(applicationContext, FirebaseInstanceId.getInstance().token, BuildConfig.ACCOUNTS_URL)
            BytemarkSDK.setAlertOnErrors(false)
        } catch (e: Exception) {
            try {
                BytemarkSDK.initInBackground(applicationContext, "", BuildConfig.ACCOUNTS_URL)
            } catch (e1: UnsupportedEncodingException) {
                e1.printStackTrace()
            }

            Timber.e(e.toString())
        }

    }


    // We had to use Reflection here because of the requirements in HaCon integration
    // that it should only work for NYCF client. Probably there are better ways(Annotation processors?) to solve this.
    // Might need to refactor this in future.
    private fun initHacon() {
        try {
            val cls = Class.forName("de.hafas.app.HafasApplication")
            val methodName = "initialize"
            val menuClass = Class.forName("co.bytemark.hacon.MenuProvider")
            val navigationMenuProviderClass = Class.forName("de.hafas.app.menu.NavigationMenuProvider")
            val menuMethodName = "getInstance"
            // getMethod takes the method name we want to invoke and what type of argument that method name accepts
            // for us method name is "getInstance" and argument type is "Integer", "ConfHelper", "MenuClickManager"
            val method = cls.getMethod(methodName, Context::class.java, navigationMenuProviderClass)
            val menuMethod = menuClass.getMethod(menuMethodName, Integer::class.java, ConfHelper::class.java, MenuClickManager::class.java, AnalyticsPlatformAdapter::class.java)
            // invoke accepts object name on which this method needs to be invoked and followed by parameters.
            // we are passing null because we are invoking static method, object name does not matter
            method.invoke(null, this, menuClass.cast(menuMethod.invoke(null, this.navigationDrawerWidth(this.getActionBarHeight()), confHelper, null, null)))
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }
    }

    companion object {

        lateinit var appComponent: AppComponent
            private set

        private var conf: Conf? = null

        var isRatingPopUpRequired: Boolean? = false

        private var appReference: WeakReference<Application>? = null

        fun getConf(): Conf? {
            if (conf == null) {
                try {
                    val conf = AssetParser(appReference?.get()).loadConf()
                    setConf(conf)
                } catch (e: Exception) {
                    Timber.e(e.toString())
                }

            }
            return conf
        }

        fun setConf(downloadedConf: Conf?) {
            conf = downloadedConf
            BytemarkSDK.SDKUtility.setConf(conf)
        }

        fun getIsRatingPopUPRequired() = isRatingPopUpRequired

        val component: Injector
            get() = Injector.getInjector(appComponent)

        private val overtureURL: String
            get() = fixUrl(BuildConfig.OVERTURE_URL)
    }


    private fun updateSession() {

        if (!confHelper.isAppRatingDisabled) {
            confHelper.setAppRatingConfig()
            RatingAndFeedBackHelper(applicationContext).checkAndUpdateTheCount()
            isRatingPopUpRequired = RatingAndFeedBackHelper(applicationContext).checkDisplayRequired()
        }
    }
}
