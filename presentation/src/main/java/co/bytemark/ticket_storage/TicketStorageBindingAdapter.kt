package co.bytemark.ticket_storage

import android.text.SpannableStringBuilder
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import co.bytemark.R
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.Pass
import java.text.SimpleDateFormat
import java.util.*

object TicketStorageBindingAdapter {

    @JvmStatic
    @BindingAdapter("labelName")
    fun setLabelNameByLocale(view: View?, pass: Pass) {
        if (view != null)
            (view as AppCompatTextView).text = pass.getLabelName(Locale.getDefault().language)
    }

    @JvmStatic
    @BindingAdapter("app:expiration", "app:confHelper")
    fun setPassExpiration(view: View?, calendar: Calendar?, confHelper: ConfHelper) {
        if (calendar != null && view != null) {
            val builder = SpannableStringBuilder()
            val formatter: SimpleDateFormat = confHelper.dateDisplayFormat
            val date = formatter.format(calendar.time)
            builder.append(view.context.getString(R.string.ticket_storage_use_by_XXX, date))
            if (builder.isEmpty()) {
                (view as AppCompatTextView).visibility = View.GONE
            } else {
                (view as AppCompatTextView).visibility = View.VISIBLE
                view.setText(builder, TextView.BufferType.SPANNABLE)
            }
        }
    }

    @JvmStatic
    @BindingAdapter("app:recyclerAdapter", "app:listData")
    fun setRecyclerAdapter(
        recyclerView: RecyclerView,
        adapter: TicketStorageAdapter,
        list: MutableLiveData<List<Pass>>
    ) {
        adapter.clear()
        list.value?.let { adapter.addPasses(it) }
    }
}