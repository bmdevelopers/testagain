package co.bytemark.ticket_storage

import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseHandler
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.databinding.FragmentTicketStorageBinding
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.model.ticket_storage.TicketStorageData
import co.bytemark.sdk.model.ticket_storage.TicketStorageTargetType
import co.bytemark.widgets.util.getViewModel
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import javax.inject.Inject

class TicketStorageFragment : BaseMvvmFragment(), BaseHandler<TicketStorageData> {

    private lateinit var binding: FragmentTicketStorageBinding
    private lateinit var viewModel: TicketStorageViewModel
    private lateinit var cloudPassAdapter: TicketStorageAdapter
    private lateinit var devicePassAdapter: TicketStorageAdapter
    private var dialog: MaterialDialog? = null

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(CustomerMobileApp.appComponent.ticketStorageViewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            FragmentTicketStorageBinding.inflate(inflater, container, false)
        devicePassAdapter = TicketStorageAdapter(confHelper, TicketStorageTargetType.TYPE_CLOUD, this)
        cloudPassAdapter = TicketStorageAdapter(confHelper, TicketStorageTargetType.TYPE_DEVICE, this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        super.onStart()

        binding.devicePassesRecyclerView.adapter = devicePassAdapter
        binding.cloudPassesRecyclerView.adapter = cloudPassAdapter

        setBindingData()
        observeLiveData()
    }

    private fun setBindingData() {
        binding.viewModel = viewModel
        binding.handler = this
        binding.cloudPassAdapter = cloudPassAdapter
        binding.devicePassAdapter = devicePassAdapter
        binding.lifecycleOwner = viewLifecycleOwner
    }

    private fun observeLiveData() {
        viewModel.isLoading.observe(this, Observer {
            showOrHideLoading(it)
        })

        viewModel.isPassTransferInProgress.observe(this, Observer {
            showOrHideTransferPassDialog(it)
        })

        viewModel.errorLiveData.observe(this, Observer {
            it?.let { it1 -> handleError(it1) }
        })
    }

    override fun onStart() {
        initNetworkScanningInOnStart = false
        super.onStart()
    }

    override fun onClick(view: View, data: TicketStorageData?) {
        when (view.id) {
            R.id.editStorageLocationButton -> {
                onEditStorageLocationClick()
            }

            R.id.ticketIcon -> {
                if (data?.iconCustomMessage != null) {
                    showWhyLocked(data.iconCustomMessage!!)
                } else {
                    data?.pass?.let {
                        if (isOnline()) {
                            viewModel.transferPass(it, data.target)
                        } else {
                            showOfflineDialog()
                        }
                    }
                }
            }
        }
    }

    override fun onOnline() {
        viewModel.getPasses()
    }

    override fun onOffline() {
        viewModel.cloudPassesLiveData.value = mutableListOf()
        showOfflineDialog()
    }

    private fun showOfflineDialog() {
        dismissDialog()
        dialog = activity?.let {
            MaterialDialog.Builder(it)
                .title(R.string.change_password_popup_conErrorTitle)
                .content(R.string.change_password_Popup_con_error_body)
                .positiveText(R.string.ok)
                .show()
        }
    }

    private fun showOrHideLoading(isLoading: Boolean) {
        dismissDialog()
        if (isLoading && activity != null) {
            dialog = MaterialDialog.Builder(activity!!)
                .content(R.string.ticket_storage_popup_loading_passes)
                .progress(true, 0)
                .cancelable(false).show()
        }
    }

    private fun showOrHideTransferPassDialog(isLoading: Boolean) {
        dismissDialog()
        if (isLoading && activity != null) {
            dialog = MaterialDialog.Builder(activity!!)
                .content(R.string.ticket_storage_popup_transferring_passes)
                .progress(true, 0)
                .cancelable(false).show()
        }
    }

    private fun showWhyLocked(message: String) {
        dismissDialog()
        dialog = activity?.let {
            MaterialDialog.Builder(it)
                .title(R.string.ticket_storage_Popup_why_the_lock_dialog_title)
                .content(message)
                .positiveText(R.string.v3_popup_ok)
                .show()
        }
    }

    private fun onEditStorageLocationClick() {
        val defaultDeviceStorage: Int =
            sharedPreferences.getInt(AppConstants.DEFAULT_DEVICE_STORAGE, 0)
        dismissDialog()
        val deviceStorage = defaultDeviceStorage
        dialog = activity?.let {
            MaterialDialog.Builder(it)
                .title(R.string.ticket_storage_popup_location_title)
                .customView(R.layout.storage_location_dialog, true)
                .positiveColor(confHelper.dataThemeAccentColor)
                .positiveText(R.string.ticket_storage_popup_confirm)
                .negativeText(R.string.popup_cancel)
                .cancelable(false)
                .onPositive { _: MaterialDialog?, _: DialogAction? ->
                    val view = dialog!!.customView
                    if (view != null) {
                        val group = view.findViewById<RadioGroup>(R.id.storage_location)
                        var selectedLocation = 0
                        when (group.checkedRadioButtonId) {
                            R.id.radio_cloud -> selectedLocation = 1
                            R.id.radio_device -> selectedLocation = 0
                        }
                        if (selectedLocation != defaultDeviceStorage) {
                            sharedPreferences.edit()
                                .putInt(AppConstants.DEFAULT_DEVICE_STORAGE, selectedLocation)
                                .apply()
                            analyticsPlatformAdapter.defaultTicketStorageChanged(
                                AnalyticsPlatformsContract.TicketStorage.DEVICE
                            )
                        }
                    }
                }
                .show()
        }
        val view = dialog?.customView
        if (view != null) {
            val radioButtonDevice = view.findViewById<RadioButton>(R.id.radio_device)
            val radioButtonCloud = view.findViewById<RadioButton>(R.id.radio_cloud)
            when (defaultDeviceStorage) {
                0 -> radioButtonDevice.isChecked = true
                1 -> radioButtonCloud.isChecked = true
            }
        }
    }

    override fun setUpColors() {
        super.setUpColors()
        binding.deviceImageView.imageTintList =
            ColorStateList.valueOf(confHelper.collectionThemeSecondaryTextColor)
        binding.cloudImageView.imageTintList =
            ColorStateList.valueOf(confHelper.collectionThemeSecondaryTextColor)
    }

    private fun dismissDialog() {
        dialog?.dismiss()
    }

    companion object {
        const val DEVICE = "device"
        const val CLOUD = "cloud"
        fun newInstance() = TicketStorageFragment()
    }
}