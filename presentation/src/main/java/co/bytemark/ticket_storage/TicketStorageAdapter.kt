package co.bytemark.ticket_storage

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.BR
import co.bytemark.R
import co.bytemark.base.BaseHandler
import co.bytemark.databinding.TicketStorageItemViewBinding
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.Pass
import co.bytemark.sdk.model.ticket_storage.TicketStorageData
import co.bytemark.sdk.model.ticket_storage.TicketStorageTargetType

class TicketStorageAdapter(
    private val confHelper: ConfHelper,
    private val transferTargetType: TicketStorageTargetType,
    private var baseHandler: BaseHandler<*>? = null
) : RecyclerView.Adapter<TicketStorageAdapter.ViewHolder>() {
    val passes: MutableList<Pass> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.ticket_storage_item_view,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = passes.size

    override fun getItemId(position: Int): Long = passes[position].uuid.hashCode().toLong()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.apply {
            with(passes[position]) {
                var data: TicketStorageData? = null
                if (transferTargetType == TicketStorageTargetType.TYPE_CLOUD) {
                    var customMessage: String? = null
                    when (status) {
                        AppConstants.USE_TICKETS_STATUS_USABLE -> {
                            customMessage = when {
                                BytemarkSDK.SDKUtility.getAppInstallationId() != this.lockedToDeviceAppInstallationId -> {
                                    context.resources
                                        .getString(R.string.ticket_storage_this_ticket_locked_to) + " " + this.lockedToDeviceModel
                                }
                                this.lockedToDeviceExpirationTime != null -> {
                                    context.resources.getString(R.string.ticket_storage_please_wait)
                                }
                                else -> {
                                    null
                                }
                            }
                        }
                        AppConstants.USE_TICKETS_STATUS_USING -> {
                            customMessage =
                                context.resources.getString(R.string.ticket_storage_using_status_lock_message)
                        }
                        AppConstants.USE_TICKETS_STATUS_EXPIRED -> {
                            customMessage =
                                context.resources.getString(R.string.ticket_storage_expired_ticket_message)
                        }
                        AppConstants.USE_TICKETS_STATUS_USES_GONE -> {
                            customMessage =
                                context.resources.getString(R.string.ticket_storage_uses_gone_ticket_message)
                        }
                    }

                    data = TicketStorageData(
                        this,
                        transferTargetType,
                        customMessage,
                        if (customMessage != null) R.drawable.locked_material else R.drawable.to_cloud_material,
                        if (customMessage != null) context.getString(R.string.ticket_storage_locked_to_device_vo_only) else context.getString(
                            R.string.ticket_storage_save_to_cloud
                        )
                    )
                } else {
                    data = TicketStorageData(
                        this,
                        transferTargetType,
                        null,
                        R.drawable.to_device_material,
                        context.getString(R.string.ticket_storage_save_to_device)
                    )
                }
                holder.bind(data, baseHandler, confHelper)
            }
        }
    }

    class ViewHolder(var binding: TicketStorageItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Any?, handler: BaseHandler<*>?, confHelper: ConfHelper) {
            binding.setVariable(BR.data, data)
            binding.setVariable(BR.handler, handler)
            binding.setVariable(BR.confHelper, confHelper)
        }
    }

    fun addPasses(passesToAdd: List<Pass>) {
        val preSize = passes.size
        passes.addAll(passesToAdd)
        notifyItemRangeChanged(preSize - 1, passes.size)
    }

    fun clear() {
        passes.clear()
        notifyDataSetChanged()
    }
}