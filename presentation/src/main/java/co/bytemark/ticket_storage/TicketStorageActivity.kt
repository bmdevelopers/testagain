package co.bytemark.ticket_storage

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.sdk.model.common.Action
import co.bytemark.widgets.util.replaceFragment

class TicketStorageActivity : MasterActivity() {

    override fun getLayoutRes(): Int = R.layout.activity_ticket_storage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setNavigationViewCheckedItem(Action.SETTINGS)

        replaceFragment(TicketStorageFragment.newInstance(), R.id.ticket_storage_container)
    }

    override fun useHamburgerMenu(): Boolean = false
}