package co.bytemark.ticket_storage

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.passes.TransferPassUseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.*
import co.bytemark.sdk.model.ticket_storage.TicketStorageTargetType
import co.bytemark.sdk.network_impl.BMNetwork
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class TicketStorageViewModel @Inject constructor(
    private val transferPassUseCase: TransferPassUseCase,
    private val bmNetwork: BMNetwork,
    private val analyticsPlatformAdapter: AnalyticsPlatformAdapter
) : BaseViewModel() {

    var isLoading = MutableLiveData<Boolean>().apply {
        postValue(false)
    }

    var isPassTransferInProgress = MutableLiveData<Boolean>().apply {
        postValue(false)
    }

    var cloudPassesLiveData = MutableLiveData<List<Pass>>()

    var devicePassesLiveData = MutableLiveData<List<Pass>>()


    fun getPasses() {
        cancelRequests()
        isLoading.postValue(true)
        bmNetwork.getAvailablePasses(application, object : GetAvailablePasses.GetPassesCallback {
            override fun onLocalPasses(localPasses: ArrayList<Pass>?) {
                devicePassesLiveData.postValue(localPasses)
            }

            override fun onCloudPasses(cloudPasses: Passes?) {
                isLoading.postValue(false)

                cloudPasses?.passes?.let {
                    val passList: MutableIterator<Pass> = it.iterator()
                    while (passList.hasNext()) {
                        if (passList.next().lockedToDevice.isNotEmpty()) passList.remove()
                    }
                    cloudPassesLiveData.postValue(it)
                }
            }

            override fun onError(errors: BMErrors?) {
                isLoading.postValue(false)
                errors?.errors?.let {
                    errorLiveData.postValue(BMError(it.first().code, it.first().message))
                }
            }

        }, AppConstants.USE_TICKETS_AVAILABLE_FRAGMENT_TYPE, "100", "1")
    }

    fun transferPass(targetPass: Pass, transferTarget: TicketStorageTargetType) = viewModelScope.launch {

        isPassTransferInProgress.postValue(true)
        var destination =
            if (transferTarget == TicketStorageTargetType.TYPE_CLOUD) TicketStorageFragment.CLOUD else TicketStorageFragment.DEVICE

        var resultValues =
            transferPassUseCase.TransferPassUseCaseValue(targetPass.uuid, destination, transferTarget)

        when (val result = transferPassUseCase(resultValues)) {
            is Result.Failure -> {
                isPassTransferInProgress.postValue(false)
                errorLiveData.postValue(result.bmError.first())

                analyticsPlatformAdapter.featureTransferPass(
                    AnalyticsPlatformsContract.Screen.USE_TICKETS,
                    destination,
                    targetPass.productLabelName,
                    targetPass.productUuid,
                    AnalyticsPlatformsContract.Status.FAILURE,
                    result.bmError.first().message
                )
            }

            is Result.Success -> {
                isPassTransferInProgress.postValue(false)

                val devicePasses =
                    ArrayList<Pass>()
                val cloudPasses =
                    ArrayList<Pass>()

                result.data?.passes?.let {
                    for (pass in it) {
                        if (pass.lockedToDevice.isNotEmpty()) {
                            devicePasses.add(pass)
                        } else {
                            cloudPasses.add(pass)
                        }
                    }
                }
                cloudPassesLiveData.postValue(cloudPasses)
                devicePassesLiveData.postValue(devicePasses)

                analyticsPlatformAdapter.featureTransferPass(
                    AnalyticsPlatformsContract.Screen.USE_TICKETS,
                    destination,
                    targetPass.productLabelName,
                    targetPass.productUuid,
                    AnalyticsPlatformsContract.Status.SUCCESS,
                    ""
                )
            }
        }
    }

    private fun cancelRequests() {
        bmNetwork.cancelGetPasses()
    }
}