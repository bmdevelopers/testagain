package co.bytemark.splash

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.accessibility.AccessibilityManager
import co.bytemark.AssetParser
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.base.BaseActivity
import co.bytemark.helpers.RxUtils
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.tutorial.TutorialManager
import co.bytemark.use_tickets.UseTicketsActivity
import java.util.*
import javax.inject.Inject
import co.bytemark.widgets.util.postDelay

class SplashScreenActivity : BaseActivity() {

    @Inject
    lateinit var rxUtils: RxUtils

    @Inject
    lateinit var assetParser: AssetParser

    @Inject
    lateinit var tutorialManager: TutorialManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)

        //Can only be applied to a Fragment
        tutorialManager.appLaunched()
        startAnalytics()
        postDelay(2) {
            if (BytemarkSDK.isAwaitingLogout(getApplicationContext())) {
                BytemarkSDK.logout(getApplicationContext())
            }
            startActivity(Intent(this, UseTicketsActivity::class.java))
            finish()
            overridePendingTransition(R.anim.splash_enter_animation, R.anim.splash_exit_animation)
        }
    }

    private fun startAnalytics(){
        val configuration = resources.configuration
        val manager = getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager
        val voiceOverState = manager.isEnabled || manager.isTouchExplorationEnabled
        val fontScaleState = configuration.fontScale.toDouble()
        var language: String? = ""
        try {
            language = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                configuration.locales[0].displayLanguage
            } else {
                Locale.getDefault().displayLanguage
            }
        } catch (ignored: Exception) {
        }
        analyticsPlatformAdapter.accessibilityState(if (voiceOverState) 1 else 0, fontScaleState, language)
    }
}
