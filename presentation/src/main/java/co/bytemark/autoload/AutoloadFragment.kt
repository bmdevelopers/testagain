package co.bytemark.autoload

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.InputFilter
import android.text.TextUtils
import android.text.method.DigitsKeyListener
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.graphics.ColorUtils
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.addPaymentMethods.AddPaymentMethodsActivity
import co.bytemark.add_payment_card.AddPaymentCardActivity
import co.bytemark.add_payment_card.Helper.TextWatcherHelper
import co.bytemark.add_payment_card.PaymentCard.Companion.fromPaymentCardType
import co.bytemark.add_payment_card.validators.CardCvvValidator
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.databinding.FragmentAutoloadBinding
import co.bytemark.domain.model.common.CalendarAutoloadConfigItem
import co.bytemark.domain.model.common.CalendarAutoloadConfigItemValues
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.AppConstants.ADD_CARD_REQUEST_CODE
import co.bytemark.helpers.AppConstants.ADD_PAYMENT_METHOD_REQUEST_CODE
import co.bytemark.helpers.AppConstants.ORG_UUID
import co.bytemark.helpers.AppConstants.PAYMENT_METHODS_REQUEST_CODE
import co.bytemark.helpers.DigitsInputFilter
import co.bytemark.payment_methods.PaymentMethodsActivity
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.Helpers.PaymentMethod
import co.bytemark.sdk.model.payment_methods.BraintreePaypalPaymentMethod
import co.bytemark.sdk.model.payment_methods.Card
import co.bytemark.sdk.model.payment_methods.PaymentMethods
import co.bytemark.sdk.model.payment_methods.Wallet
import co.bytemark.sdk.post_body.Payment
import co.bytemark.shopping_cart.AmountValidator
import co.bytemark.widgets.BmRadioButton
import co.bytemark.widgets.util.*
import com.bumptech.glide.Glide
import javax.inject.Inject
import kotlin.math.roundToInt

class AutoloadFragment : BaseMvvmFragment() {

    private lateinit var viewModel: AutoloadViewModel
    private var isUserDefinedAutoLoadValue = false
    private var autoloadSpinnerData = ArrayList<AutoloadType>()
    private var analyticsSource: String? = null
    private var fareMediumId: String? = null
    private var wallet: Wallet? = null
    private var organizationUUID: String? = null
    private var shouldHideDecimals: Boolean = false
    private var loadConfig: LoadConfig? = null
    private var selectedPaymentMethods: MutableList<PaymentMethod> = mutableListOf()
    private var availablePaymentMethods: PaymentMethods? = null
    private var walletCardUuid: String? = null
    private var amountValue = 0
    private var balanceValue = 0
    private var isAutoloadAvailable = false
    private var autoloadTimeValueId: Int? = null
    private var autoloadTimeValue: String? = null
    private lateinit var binding: FragmentAutoloadBinding
    private var action = ACTION_SAVE
    private var acceptedPaymentMethods: ArrayList<String> = ArrayList()
    private var isSplitPaymentSelected = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAutoloadBinding.inflate(inflater)
        return binding.root
    }

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    companion object {
        fun newInstance(fareMediumId: String?, wallet: Wallet?, title: String) =
            AutoloadFragment().withArgs {
                putString(AppConstants.FARE_MEDIUM_ID, fareMediumId)
                putParcelable(AppConstants.WALLET, wallet)
                putString(AppConstants.TITLE, title)
            }

        private const val LOADING = 0
        private const val ADD = 1
        private const val CHANGE = 2
        private const val RETRY = 3

        private const val ACTION_SAVE = 1
        private const val ACTION_UPDATE = 2
        private const val ACTION_DELETE = 3
    }

    override fun onInject() = CustomerMobileApp.component.inject(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = createViewModel { CustomerMobileApp.appComponent.autoloadViewModel }
        organizationUUID = activity?.intent?.getStringExtra(ORG_UUID)
        wallet = arguments?.getParcelable(AppConstants.WALLET)
        fareMediumId = arguments?.getString(AppConstants.FARE_MEDIUM_ID)
        shouldHideDecimals = !TextUtils.isEmpty(fareMediumId)
        setupViewData()
        loadPaymentMethods()
        getAcceptedPaymentMethod()
        observeLiveData()
        initListeners()
    }

    override fun onOnline() {
        if (binding.amountRadioGroup.childCount <= 0) {
            fetchAutoloadConfiguration()
        }
        if (binding.include.buttonPaymentOption.tag as Int == RETRY || selectedPaymentMethods.isEmpty()) {
            loadPaymentMethods()
            getAcceptedPaymentMethod()
        } else {
            setPaymentState(CHANGE)
        }
    }

    override fun onOffline() {
        binding.swipeRefreshLayout.isRefreshing = false
        setPaymentState(RETRY)
        binding.saveAutoloadButton.isEnabled = false
        val alphaComponent =
            ColorUtils.setAlphaComponent(confHelper.accentThemeBacgroundColor, 26)
        binding.saveAutoloadButton.backgroundTintList = ColorStateList.valueOf(alphaComponent)
    }

    private fun getAcceptedPaymentMethod() {
        viewModel.getAcceptedPaymentMethod()
    }

    private fun loadPaymentMethods() {
        selectedPaymentMethods.clear()
        availablePaymentMethods = null
        setPaymentState(LOADING)
        viewModel.getPaymentMethod(organizationUUID, confHelper.isV2PaymentMethodsEnabled)
    }

    private fun setPaymentState(state: Int) {
        binding.include.buttonPaymentOption.tag = state
        when (state) {
            LOADING -> {
                binding.include.buttonPaymentOption.visibility = View.GONE
                binding.include.firstPaymentMethodLayout.hide()
                binding.include.secondPaymentMethodLayout.hide()
                binding.include.progressBar.show()
                binding.include.textViewPaymentState.show()
                binding.include.textViewPaymentMessage.hide()
                binding.include.editTextFirstCardCVV.hide()
                binding.include.textViewPaymentState.text =
                    getString(R.string.autoload_loading_payment_mehtods)
            }
            ADD -> {
                binding.include.buttonPaymentOption.show()
                binding.include.buttonPaymentOption.text =
                    getString(R.string.shopping_cart_add_payment_method)
                binding.include.buttonPaymentOption.contentDescription =
                    getString(R.string.autoload_add_payment_method_voonly)
                binding.include.firstPaymentMethodLayout.hide()
                binding.include.secondPaymentMethodLayout.hide()
                binding.include.progressBar.hide()
                binding.include.textViewPaymentState.hide()
                binding.include.textViewPaymentMessage.show()
                binding.include.textViewPaymentMessage.text =
                    getString(R.string.shopping_cart_no_payment)
            }
            CHANGE -> {
                binding.include.buttonPaymentOption.show()
                binding.include.buttonPaymentOption.text =
                    getString(R.string.shopping_cart_change_payment)
                binding.include.buttonPaymentOption.contentDescription =
                    getString(R.string.autoload_change_payment_method_voonly)
                binding.include.firstPaymentMethodLayout.show()
                binding.include.textViewPaymentState.hide()
                binding.include.textViewPaymentMessage.hide()
                binding.include.progressBar.hide()
                if (isSplitPaymentSelected && selectedPaymentMethods.size > 1) {
                    binding.include.secondPaymentMethodLayout.show()
                } else {
                    binding.include.secondPaymentMethodLayout.hide()
                }
            }
            RETRY -> {
                binding.include.buttonPaymentOption.show()
                binding.include.buttonPaymentOption.text = getString(R.string.autoload_retry)
                binding.include.firstPaymentMethodLayout.hide()
                binding.include.secondPaymentMethodLayout.hide()
                binding.include.progressBar.hide()
                binding.include.textViewPaymentState.hide()
                binding.include.textViewPaymentMessage.show()
                binding.include.textViewPaymentMessage.text =
                    getString(R.string.autoload_error_loading_payments)
            }
        }
    }

    private fun setFirstCardFields(card: Card) {
        with(binding.include) {
            firstPaymentMethodLayout.show()
            secondPaymentMethodLayout.hide()
            imageViewFirstCard.postDelayed({
                checkCardType(
                    card.typeName,
                    imageViewFirstCard
                )
            }, 200)
            val cardNumber = String.format(
                getString(R.string.autoload_card_last_four),
                card.lastFour
            )
            textViewFirstCardNumber.text = cardNumber
            textViewFirstCardNumber.contentDescription =
                getString(R.string.autoload_ending_with_voonly) + card.lastFour
            val cardExpiration = String.format(
                getString(R.string.autoload_exp),
                card.expirationDate
            )
            textViewFirstCardExpiration.text = cardExpiration
            textViewFirstCardExpiration.contentDescription =
                getString(R.string.autoload_card_expiry_is) + card.expirationDate
            textViewFirstCardExpiration.show()
            if (card.nickname.isEmpty()) {
                textViewFirstCardLabel.text = card.typeName
            } else {
                textViewFirstCardLabel.text = card.nickname
            }
            textViewFirstCardLabel.show()
            if (card.cvvRequired) {
                editTextFirstCardCVV.show()
                val validator: CardCvvValidator = if (card.typeId == 3) {
                    CardCvvValidator(4)
                } else {
                    CardCvvValidator(3)
                }
                editTextFirstCardCVV.filters = arrayOf(DigitsKeyListener(), validator)
                editTextFirstCardCVV.addTextChangedListener(validator)
                editTextFirstCardCVV.addTextChangedListener(object : TextWatcherHelper() {
                    override fun afterTextChanged(s: Editable?) {
                        validateSaveAutoloadButton()
                        if (validator.hasFullLengthText()) {
                            card.cvv = editTextFirstCardCVV.text.toString()
                        } else {
                            card.cvv = null
                        }
                    }
                })
            } else {
                editTextFirstCardCVV.hide()
            }
            validateSaveAutoloadButton()
        }
    }

    private fun setSecondCardFields(card: Card) {
        with(binding.include) {
            secondPaymentMethodLayout.show()
            imageViewSecondCard.postDelayed({
                checkCardType(
                    card.typeName,
                    imageViewSecondCard
                )
            }, 200)
            val cardNumber = String.format(
                getString(R.string.autoload_card_last_four),
                card.lastFour
            )
            textViewSecondCardNumber.text = cardNumber
            textViewSecondCardNumber.contentDescription =
                getString(R.string.autoload_ending_with_voonly) + card.lastFour
            val cardExpiration = String.format(
                getString(R.string.autoload_exp),
                card.expirationDate
            )
            textViewSecondCardExpiration.text = cardExpiration
            textViewSecondCardExpiration.contentDescription =
                getString(R.string.autoload_card_expiry_is) + card.expirationDate
            textViewSecondCardExpiration.show()
            if (card.nickname.isEmpty()) {
                textViewSecondCardLabel.text = card.typeName
            } else {
                textViewSecondCardLabel.text = card.nickname
            }
            textViewSecondCardLabel.show()
            if (card.cvvRequired) {
                editTextSecondCardCVV.show()
                val validator: CardCvvValidator = if (card.typeId == 3) {
                    CardCvvValidator(4)
                } else {
                    CardCvvValidator(3)
                }
                editTextSecondCardCVV.filters = arrayOf(DigitsKeyListener(), validator)
                editTextSecondCardCVV.addTextChangedListener(validator)
                editTextSecondCardCVV.addTextChangedListener(object : TextWatcherHelper() {
                    override fun afterTextChanged(s: Editable?) {
                        validateSaveAutoloadButton()
                        if (validator.hasFullLengthText()) {
                            card.cvv = editTextFirstCardCVV.text.toString()
                        } else {
                            card.cvv = null
                        }
                    }
                })
            } else {
                editTextSecondCardCVV.hide()
            }

            textViewFirstCardAmountLabel.show()
            textViewSecondCardAmountLabel.show()
            editTextFirstCardAmount.show()
            editTextSecondCardAmount.show()

            validateSaveAutoloadButton()
        }
    }

    private fun setPayPal() {
        binding.include.textViewFirstCardNumber.text = getText(R.string.payment_paypal)
        binding.include.imageViewFirstCard.setImageDrawable(resources.getDrawable(R.drawable.bt_ic_paypal))
        binding.include.textViewFirstCardExpiration.hide()
        binding.include.editTextFirstCardCVV.hide()
        binding.include.textViewFirstCardLabel.hide()
        validateSaveAutoloadButton()
    }

    private fun checkCardType(paymentCardType: String, imageViewCard: ImageView) {
        val paymentCard = fromPaymentCardType(paymentCardType)
        Glide.with(imageViewCard.context)
            .load(paymentCard.getTypeImage())
            .crossFade()
            .skipMemoryCache(false)
            .into(imageViewCard)
    }

    private fun setPayments(paymentMethod: PaymentMethods?) {
        availablePaymentMethods = paymentMethod
        binding.swipeRefreshLayout.isRefreshing = false
        selectedPaymentMethods.clear()
        var walletCard: Any? = null
        val paymentMethods = convertPaymentMethodsToList(paymentMethod)

        if (paymentMethods.isNotEmpty()) {
            setPaymentState(CHANGE)
            paymentMethods.forEach {
                if (it is Card && it.uuid == walletCardUuid) {
                    selectedPaymentMethods.add(it)
                    walletCard = it
                } else if (it is BraintreePaypalPaymentMethod && it.uuid == walletCardUuid) {
                    selectedPaymentMethods.add(it)
                    walletCard = it
                }
            }
            if (walletCard == null) {
                selectedPaymentMethods.add(paymentMethods[0])
                walletCard = paymentMethods[0]
            }
            if (walletCard is Card) {
                setFirstCardFields(walletCard as Card)
            } else if (walletCard is BraintreePaypalPaymentMethod) {
                setPayPal()
            }
        } else {
            setPaymentState(ADD)
        }
        validateSaveAutoloadButton()
    }

    private fun convertPaymentMethodsToList(paymentMethod: PaymentMethods?): ArrayList<PaymentMethod> {
        val paymentMethods: ArrayList<PaymentMethod> = ArrayList()
        paymentMethods.clear()
        paymentMethod?.cards?.let { paymentMethods.addAll(it) }
        paymentMethod?.braintreePaypalPaymentMethods?.let { paymentMethods.addAll(it) }
        return paymentMethods
    }

    private fun fetchAutoloadConfiguration() {
        viewModel.getAutoloadConfig()
    }

    private fun setVisibilityForSplitPaymentText(visibility: Int) {
        if (wallet != null || visibility == View.GONE) {
            binding.include.splitPaymentSwitch.hide()
            binding.include.textViewSwitchLabel.hide()
        } else if (visibility == View.VISIBLE && BytemarkSDK.isLoggedIn() && confHelper.maxSplitPayments > 1) {
            binding.include.splitPaymentSwitch.show()
            binding.include.textViewSwitchLabel.show()
        }
    }

    private fun setupViewData() {
        with(binding) {
            swipeRefreshLayout.setColorSchemeColors(
                confHelper.accentThemeBacgroundColor,
                confHelper.headerThemeAccentColor,
                confHelper.backgroundThemeBackgroundColor,
                confHelper.dataThemeAccentColor
            )

            swipeRefreshLayout.setOnRefreshListener {
                if (isOnline()) {
                    loadPaymentMethods()
                    // as we are loading the payment methods again, we need to clear the splitpayment
                    binding.include.splitPaymentSwitch.isChecked = false
                    onClickSplitPayments()
                } else swipeRefreshLayout.isRefreshing = false
            }

            amountOtherButton.setOnClickListener {
                amountAddGroup(View.VISIBLE)
                validateSaveAutoloadButton(false)
                amountOtherGroup.visibility = View.INVISIBLE
            }

            doneAmountButton.setOnClickListener {
                try {
                    val amountBalance: String =
                        amountEditText.text.toString().trim { it <= ' ' }
                    val dollars = java.lang.Double.valueOf(amountBalance)
                    if (dollars < loadConfig?.walletAutoLoadConfig?.minWalletAutoloadValue.toString()
                            .toDouble() / 100 ||
                        dollars > loadConfig?.walletAutoLoadConfig?.maxWalletAutoloadValue.toString()
                            .toDouble() / 100
                    ) {
                        amountErrorTextView.show()
                        validateSaveAutoloadButton(false)
                    } else {
                        amountValue = (100 * dollars).roundToInt()
                        clearAmountFields()
                        updateInputFilters()
                        val prettyAmount =
                            confHelper.getConfigurationPurchaseOptionsCurrencySymbol(amountValue)
                        doneAmountButton.visibility = View.INVISIBLE
                        amountEditText.visibility = View.GONE
                        amountErrorTextView.visibility = View.GONE
                        amountOtherTextView.visibility = View.VISIBLE
                        amountOtherTextView.text = prettyAmount
                        amountOtherTextView.tag = amountValue
                        hideKeyboard()
                        validateSaveAutoloadButton()
                    }
                } catch (e: Exception) {
                    amountErrorTextView.show()
                }
            }

            closeAmountImageView.setOnClickListener {
                amountEditText.setText("")
                amountOtherTextView.text = ""
                amountOtherTextView.tag = null
                amountOtherTextView.hide()
                amountErrorTextView.hide()
                amountOtherGroup.visibility = View.VISIBLE
                amountAddGroup(View.GONE)
                hideKeyboard()
                // removed the other amount selection, so setting the first amount radio button value as default value
                val firstButton = amountRadioGroup.getChildAt(0) as BmRadioButton
                amountRadioGroup.check(firstButton.id)
                amountValue = firstButton.tag as Int
                clearAmountFields()
                updateInputFilters()
                validateSaveAutoloadButton()
            }

            balanceOtherButton.setOnClickListener {
                balanceAddGroup(View.VISIBLE)
                validateSaveAutoloadButton(false)
                balanceOtherGroup.visibility = View.INVISIBLE
            }

            doneBalanceButton.setOnClickListener {
                try {
                    val otherBalance: String =
                        balanceEditText.text.toString().trim { it <= ' ' }
                    val dollars = java.lang.Double.valueOf(otherBalance)
                    if (dollars < loadConfig?.walletAutoLoadThresholdConfig?.minAutoLoadThresholdValue.toString()
                            .toDouble() / 100 ||
                        dollars > loadConfig?.walletAutoLoadThresholdConfig?.maxAutoLoadThresholdValue.toString()
                            .toDouble() / 100
                    ) {
                        balanceErrorTextView.show()
                        validateSaveAutoloadButton(false)
                    } else {
                        balanceValue = (100 * dollars).roundToInt()
                        val prettyAmount =
                            confHelper.getConfigurationPurchaseOptionsCurrencySymbol(balanceValue)
                        doneBalanceButton.visibility = View.INVISIBLE
                        balanceEditText.visibility = View.GONE
                        balanceErrorTextView.visibility = View.GONE
                        balanceOtherTextView.visibility = View.VISIBLE
                        balanceOtherTextView.text = prettyAmount
                        balanceOtherTextView.tag = balanceValue
                        hideKeyboard()
                        validateSaveAutoloadButton()
                    }
                } catch (e: Exception) {
                    balanceErrorTextView.show()
                }
            }

            closeBalanceImageView.setOnClickListener {
                balanceEditText.setText("")
                balanceOtherTextView.text = ""
                balanceOtherTextView.tag = null
                balanceOtherTextView.hide()
                balanceErrorTextView.hide()
                balanceOtherGroup.visibility = View.VISIBLE
                balanceAddGroup(View.GONE)
                hideKeyboard()
            }

            saveAutoloadButton.setOnClickListener view@{
                amountValue = 0
                hideKeyboard()
                if (amountHorizontalScrollView.visibility == View.VISIBLE) {
                    if (amountRadioGroup.checkedRadioButtonId == -1) {
                        return@view
                    } else {
                        val id: Int = amountRadioGroup.checkedRadioButtonId
                        activity?.let {
                            amountValue = it.findViewById<View>(id)?.tag as Int
                        }
                    }
                } else if (amountOtherTextView.visibility == View.VISIBLE) {
                    amountValue = amountOtherTextView.tag as Int
                } else
                    return@view

                balanceValue = 0

                if (balanceHorizontalScrollView.visibility == View.VISIBLE) {
                    if (balanceRadioGroup.checkedRadioButtonId == -1)
                        return@view
                    else {
                        val id: Int = balanceRadioGroup.checkedRadioButtonId
                        activity?.let {
                            balanceValue = it.findViewById<View>(id)?.tag as Int
                        }
                    }
                } else if (balanceOtherTextView.visibility == View.VISIBLE) {
                    balanceValue = balanceOtherTextView.tag as Int
                } else if (timeSpinnerGroup.visibility != View.VISIBLE) {
                    return@view
                }

                if (selectedPaymentMethods.isNotEmpty()) {
                    try {
                        val payments = mutableListOf<Payment>()
                        val payment = Payment()
                        if (selectedPaymentMethods[0] is Card) {
                            val (uuid, _, _, _, _, _, _, cvv) = selectedPaymentMethods[0] as Card
                            payment.cardUuid = uuid
                            payment.cvv = cvv
                            payment.amount = if (isSplitPaymentSelected) {
                                val inputAmount = binding.include.editTextFirstCardAmount
                                    .text.toString().replace("[$,.]".toRegex(), "")
                                    .replace("[^\\d]".toRegex(), "")
                                inputAmount.toInt()
                            } else {
                                amountValue
                            }
                        } else if (selectedPaymentMethods[0] is BraintreePaypalPaymentMethod) {
                            payment.amount = amountValue
                            payment.payPalUuid =
                                (selectedPaymentMethods[0] as BraintreePaypalPaymentMethod).uuid
                        }
                        action = if (wallet == null || isAutoloadAvailable) {
                            ACTION_SAVE
                        } else {
                            ACTION_UPDATE
                        }
                        payments.add(payment)

                        if (isSplitPaymentSelected && selectedPaymentMethods.size == 2) {
                            val (uuid, _, _, _, _, _, _, cvv) = selectedPaymentMethods[1] as Card
                            val secondPayment = Payment()
                            secondPayment.cardUuid = uuid
                            secondPayment.cvv = cvv
                            val inputAmount = binding.include.editTextFirstCardAmount
                                .text.toString().replace("[$,.]".toRegex(), "")
                                .replace("[^\\d]".toRegex(), "")
                            secondPayment.amount = inputAmount.toInt()
                            payments.add(secondPayment)
                        }

                        viewModel.saveAutoload(
                            payments,
                            fareMediumId,
                            wallet?.uuid,
                            isAutoloadAvailable,
                            amountValue,
                            balanceValue,
                            autoloadTimeValueId
                        )
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }
            }

            currentAutoloadGroup.setOnClickListener {
                if (isOnline()) {
                    showDialog(getString(R.string.autoload_delete_autoload_dialog_title),
                        getString(R.string.autoload_delete_autoload_dialog_message),
                        getString(R.string.autoload_delete_autoload_dialog_button_delete),
                        getString(R.string.autoload_delete_autoload_dialog_button_cancel),
                        positiveAction = {
                            if (isOnline()) {
                                action = ACTION_DELETE
                                viewModel.deleteAutoload(fareMediumId, wallet?.uuid)
                            } else {
                                connectionErrorDialog { }
                            }
                        })
                } else {
                    connectionErrorDialog { }
                }
            }

            setVisibilityForSplitPaymentText(View.VISIBLE)

            with(include) {
                buttonPaymentOption.setOnClickListener {
                    when (include.buttonPaymentOption.tag as Int) {
                        ADD -> {
                            if (acceptedPaymentMethods.isNotEmpty() && acceptedPaymentMethods.contains(
                                    AppConstants.PAYMENT_TYPE_PAYPAL
                                )
                            ) {
                                val intent = Intent(context, AddPaymentMethodsActivity::class.java)
                                intent.putStringArrayListExtra(
                                    AppConstants.ACCEPTED_PAYMENTS,
                                    acceptedPaymentMethods
                                )
                                startActivityForResult(intent, ADD_PAYMENT_METHOD_REQUEST_CODE)
                            } else {
                                val intent = Intent(context, AddPaymentCardActivity::class.java)
                                intent.putExtra(AppConstants.SHOPPING_CART_INTENT, true)
                                startActivityForResult(intent, ADD_CARD_REQUEST_CODE)
                            }
                        }
                        CHANGE -> {
                            val selectCardIntent =
                                Intent(context, PaymentMethodsActivity::class.java)
                            selectCardIntent.putExtra(AppConstants.SHOPPING_CART_INTENT, true)
                            selectCardIntent.putExtra(AppConstants.AUTOLOAD, true)
                            if (isSplitPaymentSelected) {
                                selectCardIntent.putExtra(AppConstants.SPLIT_PAYMENT, true)
                                selectCardIntent.putParcelableArrayListExtra(
                                    AppConstants.SELECTED_CARD,
                                    getSelectedCards()
                                )
                            }
                            startActivityForResult(selectCardIntent, PAYMENT_METHODS_REQUEST_CODE)
                        }
                        RETRY -> loadPaymentMethods()
                    }
                }

                splitPaymentSwitch.setOnClickListener {
                    onClickSplitPayments()
                }
                textViewSwitchLabel.setOnClickListener {
                    splitPaymentSwitch.isChecked = !splitPaymentSwitch.isChecked
                    onClickSplitPayments()
                }

                editTextFirstCardAmount.addTextChangedListener(object : TextWatcherHelper() {
                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        try {
                            if (s != null && s.isNotEmpty()) {
                                editTextFirstCardAmount.removeTextChangedListener(this)
                                val cleanString = s.toString().replace("[$,.]".toRegex(), "")
                                val parsed = cleanString.replace("[^\\d]".toRegex(), "").toInt()
                                val secondAmount: Int = amountValue - parsed
                                val formatted =
                                    confHelper.getConfigurationPurchaseOptionsCurrencySymbol(parsed)
                                val secondAmountFormatted =
                                    confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
                                        secondAmount
                                    )
                                editTextFirstCardAmount.setText(formatted)
                                editTextSecondCardAmount.setText(secondAmountFormatted)
                                editTextFirstCardAmount.setSelection(formatted.length)
                                editTextSecondCardAmount.setSelection(secondAmountFormatted.length)
                                editTextFirstCardAmount.addTextChangedListener(this)
                                validateSaveAutoloadButton()
                            }
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                    }
                })

                editTextSecondCardAmount.addTextChangedListener(object : TextWatcherHelper() {
                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        try {
                            if (s != null && s.isNotEmpty()) {
                                editTextSecondCardAmount.removeTextChangedListener(this)
                                val cleanString = s.toString().replace("[$,.]".toRegex(), "")
                                val parsed = cleanString.replace("[^\\d]".toRegex(), "").toInt()
                                val firstAmount: Int = amountValue - parsed
                                val formatted =
                                    confHelper.getConfigurationPurchaseOptionsCurrencySymbol(parsed)
                                val firstAmountFormatted =
                                    confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
                                        firstAmount
                                    )
                                editTextSecondCardAmount.setText(formatted)
                                editTextSecondCardAmount.setSelection(formatted.length)
                                editTextFirstCardAmount.setText(firstAmountFormatted)
                                editTextFirstCardAmount.setSelection(firstAmountFormatted.length)
                                editTextSecondCardAmount.addTextChangedListener(this)
                                validateSaveAutoloadButton()
                            }
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                    }
                })
            }
        }
    }

    private fun clearAmountFields() {
        if (isSplitPaymentSelected) {
            with(binding.include) {
                editTextFirstCardAmount.setText("")
                editTextSecondCardAmount.setText("")
            }
        }
    }

    fun onClickSplitPayments() {
        with(binding.include) {
            isSplitPaymentSelected = splitPaymentSwitch.isChecked
            if (splitPaymentSwitch.isChecked) {
                textViewPaymentMessage.setText(R.string.shopping_cart_select_two_payment_method)
                textViewPaymentMessage.show()
                firstPaymentMethodLayout.hide()
                secondPaymentMethodLayout.hide()
                val selectCardIntent = Intent(context, PaymentMethodsActivity::class.java)
                selectCardIntent.putExtra(AppConstants.SHOPPING_CART_INTENT, true)
                selectCardIntent.putExtra(AppConstants.SPLIT_PAYMENT, true)
                selectCardIntent.putParcelableArrayListExtra(
                    AppConstants.SELECTED_CARD,
                    getSelectedCards()
                )
                startActivityForResult(selectCardIntent, PAYMENT_METHODS_REQUEST_CODE)
            } else {
                secondPaymentMethodLayout.hide()
                textViewFirstCardAmountLabel.hide()
                editTextFirstCardAmount.hide()
                val firstPaymentMethod = selectedPaymentMethods.firstOrNull()
                selectedPaymentMethods.clear()
                if (firstPaymentMethod != null && firstPaymentMethod is Card) {
                    selectedPaymentMethods.add(firstPaymentMethod)
                    setFirstCardFields(firstPaymentMethod)
                    setFirstCardFields(firstPaymentMethod)
                } else {
                    if (isOnline())
                        viewModel.getPaymentMethod(
                            organizationUUID,
                            confHelper.isV2PaymentMethodsEnabled
                        )
                    else
                        setPaymentState(RETRY)
                }
                validateSaveAutoloadButton()
            }
        }
    }

    private fun getSelectedCards(): ArrayList<PaymentMethod> {
        val selectedCards = ArrayList<PaymentMethod>()
        for (paymentMethod in selectedPaymentMethods) {
            if (paymentMethod is Card) selectedCards.add(paymentMethod)
        }
        return selectedCards
    }

    private fun initListeners() {
        binding.balanceRadioGroup.setOnCheckedChangeListener { _: RadioGroup?, _: Int ->
            validateSaveAutoloadButton()
            setOtherButtonBackground(false, binding.balanceOtherButton)
        }
        binding.amountEditText.filters = arrayOf<InputFilter>(DigitsInputFilter(3, 2))
        binding.balanceEditText.filters = arrayOf<InputFilter>(DigitsInputFilter(3, 2))
    }

    private fun balanceAddGroup(visibility: Int) {
        binding.balanceEditText.visibility = visibility
        binding.doneBalanceButton.visibility = visibility
        binding.closeBalanceImageView.visibility = visibility
    }

    private fun amountAddGroup(visibility: Int) {
        binding.amountEditText.visibility = visibility
        binding.doneAmountButton.visibility = visibility
        binding.closeAmountImageView.visibility = visibility
        validateSaveAutoloadButton()
    }

    private fun observeLiveData() {
        viewModel.autoloadConfig.observe(viewLifecycleOwner, Observer {
            loadConfig = it
            if (it.calendarAutoloadConfig != null && it.calendarAutoloadConfig?.isNotEmpty() == true) {
                setupCalendarAutoloadAndData(it)
            } else {
                setupNormalAutoloadAndData(it)
            }
            getAutoloadsAfterConfig()
        })

        viewModel.walletAutoloadLiveData.observe(viewLifecycleOwner, Observer {
            isAutoloadAvailable = it != null
            walletCardUuid = it.cardUuid ?: it.paypalUuid
            setPayments(availablePaymentMethods)
            if (it.autoloadConfigValue != null) {
                setAutoLoadValueFromConfig(
                    binding.amountRadioGroup,
                    it.autoloadValue,
                    binding.amountOtherButton
                )
                setAutoloadValueFromConfigForCalendar(
                    it.autoloadConfigName!!,
                    it.autoloadConfigValue!!
                )
            } else {
                setAutoLoadValueFromConfig(
                    binding.amountRadioGroup,
                    it.autoloadValue,
                    binding.amountOtherButton
                )
                setAutoLoadValueFromConfig(
                    binding.balanceRadioGroup,
                    it.autoloadThresholdValue,
                    binding.balanceOtherButton
                )
            }
            showCurrentAutoloadBar(
                it.autoloadValue,
                it.autoloadThresholdValue,
                it.autoloadConfigValue
            )
        })

        viewModel.fareMediumAutoloadLiveData.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                isAutoloadAvailable = it.id != null
                if (it.autoloadConfigValue != null) {
                    setAutoLoadValueFromConfig(
                        binding.amountRadioGroup,
                        it.autoloadValue,
                        binding.amountOtherButton
                    )
                    setAutoloadValueFromConfigForCalendar(
                        it.autoloadConfigName!!,
                        it.autoloadConfigValue!!
                    )
                } else {
                    setAutoLoadValueFromConfig(
                        binding.amountRadioGroup,
                        it.autoloadValue,
                        binding.amountOtherButton
                    )
                    setAutoLoadValueFromConfig(
                        binding.balanceRadioGroup,
                        it.autoloadThresholdvalue,
                        binding.balanceOtherButton
                    )
                }
                showCurrentAutoloadBar(
                    it.autoloadValue,
                    it.autoloadThresholdvalue,
                    it.autoloadConfigValue
                )
            }
        })
        viewModel.updateAutoloadLiveData.observe(viewLifecycleOwner, Observer {
            isAutoloadAvailable = it != null
            showSuccessAutoloadDialog()
            analyticsPlatformAdapter.saveAutoLoad(
                analyticsSource,
                confHelper.getConfigurationPurchaseOptionsCurrency(amountValue),
                confHelper.getConfigurationPurchaseOptionsCurrency(balanceValue),
                AnalyticsPlatformsContract.Status.SUCCESS,
                ""
            )
        })
        viewModel.saveAutoloadLiveData.observe(viewLifecycleOwner, Observer {
            isAutoloadAvailable = it != null
            showSuccessAutoloadDialog()
        })
        viewModel.deleteAutoloadLiveData.observe(viewLifecycleOwner, Observer {
            if (it) {
                isAutoloadAvailable = false
                hideCurrentAutoloadBar()
                showSuccessAutoloadDeleteDialog()
            }
        })
        viewModel.acceptedPaymentLiveData.observe(
            viewLifecycleOwner,
            Observer { acceptedPaymentMethodList ->
                acceptedPaymentMethods.clear()
                acceptedPaymentMethodList.forEach {
                    acceptedPaymentMethods.add(it.toLowerCase())
                }
            })
        viewModel.displayLiveData.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Display.EmptyState.Loading -> {
                    binding.emptyStateLayoutAutoload.showLoading(it.drawable, it.title)
                }
                is Display.EmptyState.ShowContent -> {
                    binding.emptyStateLayoutAutoload.showContent()
                }
                is Display.EmptyState.Error -> {
                    binding.emptyStateLayoutAutoload.showError(
                        it.errorImageDrawable,
                        getString(it.errorTextTitle),
                        it.errorTextContent?.let { it1 -> getString(it1) },
                        it.errorButtonText?.let { it1 -> getString(it1) }
                    ) { _: View? ->
                        if (it.errorButtonText == R.string.autoload_retry && isOnline()) {
                            fetchAutoloadConfiguration()
                            loadPaymentMethods()
                            getAcceptedPaymentMethod()
                        }
                    }
                }
            }
        })
        viewModel.paymentErrorLiveData.observe(viewLifecycleOwner, Observer {
            setPaymentState(RETRY)
        })
        viewModel.paymentLiveData.observe(viewLifecycleOwner, Observer {
            setPayments(it)
        })
        viewModel.errorLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (isOnline()) {
                    analyticsPlatformAdapter.removeAutoLoad(
                        analyticsSource,
                        AnalyticsPlatformsContract.Status.FAILURE,
                        ""
                    )
                    when (action) {
                        ACTION_DELETE -> analyticsPlatformAdapter.removeAutoLoad(
                            analyticsSource,
                            AnalyticsPlatformsContract.Status.FAILURE,
                            it.message
                        )
                        ACTION_SAVE -> analyticsPlatformAdapter.saveAutoLoad(
                            analyticsSource, amountValue.toDouble(), balanceValue.toDouble(),
                            AnalyticsPlatformsContract.Status.FAILURE, it.message
                        )
                        ACTION_UPDATE -> analyticsPlatformAdapter.saveAutoLoad(
                            analyticsSource, amountValue.toDouble(), balanceValue.toDouble(),
                            AnalyticsPlatformsContract.Status.FAILURE, ""
                        )
                        else -> {
                            analyticsPlatformAdapter.removeAutoLoad(
                                analyticsSource,
                                AnalyticsPlatformsContract.Status.FAILURE,
                                it.message
                            )
                        }
                    }
                    handleError(it)
                } else {
                    binding.swipeRefreshLayout.isRefreshing = false
                    setPaymentState(RETRY)
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            PAYMENT_METHODS_REQUEST_CODE -> {
                if (data != null) {
                    val deletedPaymentMethods: java.util.ArrayList<PaymentMethod>? =
                        data.getParcelableArrayListExtra(AppConstants.DELETED_PAYMENT)
                    if (deletedPaymentMethods != null && deletedPaymentMethods.size > 0) {
                        for (paymentMethod in deletedPaymentMethods) {
                            if (selectedPaymentMethods.contains(paymentMethod)) selectedPaymentMethods.remove(
                                paymentMethod
                            )
                        }
                        if (selectedPaymentMethods.size == 1 && selectedPaymentMethods[0] is Card) {
                            setFirstCardFields((selectedPaymentMethods[0] as Card))
                        }
                        loadPaymentMethods()
                    }
                    if (data.getParcelableArrayListExtra<Parcelable>(AppConstants.SELECTED_CARD) != null) {
                        selectedPaymentMethods =
                            data.getParcelableArrayListExtra<PaymentMethod>(AppConstants.SELECTED_CARD) as MutableList<PaymentMethod>
                        if (selectedPaymentMethods != null && selectedPaymentMethods.size >= 1) {
                            val paymentMethod = selectedPaymentMethods[0]
                            if (paymentMethod is Card) {
                                setPaymentState(CHANGE)
                                setFirstCardFields(paymentMethod)
                                analyticsPlatformAdapter.paymentMethodChanged(
                                    AnalyticsPlatformsContract.Screen.FARE_MEDIUM,
                                    AnalyticsPlatformsContract.PaymentType.CARD
                                )
                            } else if (paymentMethod is BraintreePaypalPaymentMethod) {
                                setPaymentState(CHANGE)
                                setPayPal()
                                analyticsPlatformAdapter.paymentMethodChanged(
                                    AnalyticsPlatformsContract.Screen.FARE_MEDIUM,
                                    AnalyticsPlatformsContract.PaymentType.PAYPAL
                                )
                            }

                            if (selectedPaymentMethods.size == 2) {
                                val secondPaymentMethod = selectedPaymentMethods[1]
                                if (secondPaymentMethod is Card) {
                                    setPaymentState(CHANGE)
                                    setSecondCardFields(secondPaymentMethod)
                                    analyticsPlatformAdapter.paymentMethodChanged(
                                        AnalyticsPlatformsContract.Screen.FARE_MEDIUM,
                                        AnalyticsPlatformsContract.PaymentType.CARD
                                    )
                                }
                            }
                        }
                        validateSaveAutoloadButton()
                    } else {
                        loadPaymentMethods()
                    }
                }
            }
            ADD_PAYMENT_METHOD_REQUEST_CODE, ADD_CARD_REQUEST_CODE -> {
                if (selectedPaymentMethods.isEmpty()) {
                    viewModel.getPaymentMethod(
                        organizationUUID,
                        confHelper.isV2PaymentMethodsEnabled
                    )
                }
                if (resultCode == AppConstants.ADD_CARD_SUCCESS_CODE && confHelper.isCreditCardAlertMessageEnabled) {
                    showAddCardSuccessDialog()
                }
            }
        }
    }

    private fun showAddCardSuccessDialog() {
        showDialog(
            getString(R.string.payment_popup_new_card_added_title),
            getString(R.string.payment_popup_new_card_added_message),
            getString(R.string.ticket_storage_popup_confirm)
        )
    }

    private fun setupCalendarAutoloadAndData(loadConfig: LoadConfig) {
        binding.autoloadSpinner.show()
        binding.timeSpinnerGroup.show()
        binding.balanceOtherGroup.hide()
        setupAutoloadSpinnerData()
        loadConfig.calendarAutoloadConfig?.let { setupTimeSpinnerData(it) }
        setAmountRadioGroupAndEditText(loadConfig)
        setBalanceRadioGroupAndEditText(loadConfig)
    }

    private fun setupAutoloadSpinnerData() {
        autoloadSpinnerData.clear()
        autoloadSpinnerData.add(AutoloadType(101, getString(R.string.autoload_type_value)))
        autoloadSpinnerData.add(AutoloadType(102, getString(R.string.autoload_type_time)))

        val adapter: ArrayAdapter<AutoloadType>? = context?.let {
            ArrayAdapter<AutoloadType>(
                it, android.R.layout
                    .simple_spinner_dropdown_item, autoloadSpinnerData
            )
        }
        binding.autoloadSpinner.adapter = adapter

        binding.autoloadSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (autoloadSpinnerData[position].id == 101) {
                        binding.balanceOtherGroup.show()
                        binding.timeSpinnerGroup.hide()
                        binding.autoloadBalanceTextView.show()
                        autoloadTimeValue = null
                        autoloadTimeValueId = null
                    } else if (autoloadSpinnerData[position].id == 102) {
                        binding.balanceOtherGroup.hide()
                        binding.timeSpinnerGroup.show()
                        binding.autoloadBalanceTextView.hide()
                        balanceValue = 0
                    }
                }
            }
    }

    private fun setupTimeSpinnerData(calendarAutoloadConfig: List<CalendarAutoloadConfigItem>) {

        val adapter: ArrayAdapter<CalendarAutoloadConfigItem>? = context?.let {
            ArrayAdapter<CalendarAutoloadConfigItem>(
                it, android.R.layout
                    .simple_spinner_dropdown_item, calendarAutoloadConfig
            )
        }

        binding.timeSpinner.adapter = adapter

        binding.timeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                setupTimeValueSpinnerData(calendarAutoloadConfig[position].configValues)
            }
        }
    }

    private fun setupTimeValueSpinnerData(calendarAutoloadConfigItemValues: MutableList<CalendarAutoloadConfigItemValues>) {
        binding.timeValueSpinner.adapter = null

        val adapter: ArrayAdapter<CalendarAutoloadConfigItemValues>? = context?.let {
            ArrayAdapter<CalendarAutoloadConfigItemValues>(
                it, android.R.layout
                    .simple_spinner_dropdown_item, calendarAutoloadConfigItemValues
            )
        }

        binding.timeValueSpinner.adapter = adapter

        binding.timeValueSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    autoloadTimeValueId = calendarAutoloadConfigItemValues[position].id
                    autoloadTimeValue = calendarAutoloadConfigItemValues[position].value
                    validateSaveAutoloadButton()
                }
            }
    }

    private fun setupNormalAutoloadAndData(loadConfig: LoadConfig) {
        binding.balanceOtherGroup.show()
        binding.autoloadSpinner.hide()
        binding.timeSpinnerGroup.hide()
        setAmountRadioGroupAndEditText(loadConfig)
        setBalanceRadioGroupAndEditText(loadConfig)
    }

    private fun getAutoloadsAfterConfig() {
        if (wallet != null) {
            wallet?.uuid?.let { viewModel.getAutoloadConfigOfWallet(it) }
            analyticsSource = AnalyticsPlatformsContract.Screen.WALLET
        } else if (fareMediumId != null) {
            fareMediumId?.let { viewModel.getAutoloadConfigOfFareMedium(it) }
            analyticsSource = AnalyticsPlatformsContract.Screen.FARE_MEDIUM
        }
    }

    private fun setAmountRadioGroupAndEditText(loadConfig: LoadConfig) {
        binding.amountRadioGroup.removeAllViews()
        for (value in loadConfig.walletAutoLoadConfig?.autoloadValues!!) {
            val radioButton = BmRadioButton(binding.amountRadioGroup.context)
            radioButton.text = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(value, true)
            radioButton.tag = value
            binding.amountRadioGroup.addView(radioButton)
        }

        binding.amountRadioGroup.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId != -1) {
                amountValue = group?.findViewById<View>(checkedId)?.tag as Int
                clearAmountFields()
                updateInputFilters()
                setOtherButtonBackground(false, binding.amountOtherButton)
                validateSaveAutoloadButton()
            }
        }

        if (binding.amountRadioGroup.childCount > 0) {
            binding.amountRadioGroup.clearCheck()
            val bmRadioButton = binding.amountRadioGroup.getChildAt(0) as BmRadioButton
            bmRadioButton.isChecked = true
        }
        binding.amountEditText.hint = String.format(
            getString(R.string.autoload_from_upto),
            loadConfig.walletAutoLoadConfig?.minWalletAutoloadValue?.let {
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
                    it, shouldHideDecimals
                )
            },
            loadConfig.walletAutoLoadConfig?.maxWalletAutoloadValue?.let {
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
                    it, shouldHideDecimals
                )
            }
        )
        binding.amountErrorTextView.text = String.format(
            getString(R.string.autoload_validation_between),
            loadConfig.walletAutoLoadConfig?.minWalletAutoloadValue?.let {
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
                    it, shouldHideDecimals
                )
            },
            loadConfig.walletAutoLoadConfig?.maxWalletAutoloadValue?.let {
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
                    it, shouldHideDecimals
                )
            }
        )
        setOtherButtonBackground(isUserDefinedAutoLoadValue, binding.amountOtherButton)
    }

    private fun updateInputFilters() {
        binding.include.editTextFirstCardAmount.filters =
            arrayOf<InputFilter>(AmountValidator(0, amountValue))
        binding.include.editTextSecondCardAmount.filters =
            arrayOf<InputFilter>(AmountValidator(0, amountValue))
    }

    private fun setBalanceRadioGroupAndEditText(loadConfig: LoadConfig) {
        binding.balanceRadioGroup.removeAllViews()
        for (value in loadConfig.walletAutoLoadThresholdConfig?.autoLoadThresholdValues!!) {
            val radioButton = BmRadioButton(binding.balanceRadioGroup.context)
            radioButton.text = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(value, true)
            radioButton.tag = value
            binding.balanceRadioGroup.addView(radioButton)
        }
        if (binding.balanceRadioGroup.childCount > 0) {
            binding.balanceRadioGroup.clearCheck()
            (binding.balanceRadioGroup.getChildAt(0) as BmRadioButton).isChecked = true
        }

        binding.balanceEditText.hint = String.format(
            getString(R.string.autoload_from_upto),
            loadConfig.walletAutoLoadThresholdConfig?.minAutoLoadThresholdValue?.let {
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
                    it, shouldHideDecimals
                )
            },
            loadConfig.walletAutoLoadThresholdConfig?.maxAutoLoadThresholdValue?.let {
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
                    it, shouldHideDecimals
                )
            }
        )
        binding.balanceErrorTextView.text = String.format(
            getString(R.string.autoload_validation_between),
            loadConfig.walletAutoLoadThresholdConfig?.minAutoLoadThresholdValue?.let {
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
                    it, shouldHideDecimals
                )
            },
            loadConfig.walletAutoLoadThresholdConfig?.maxAutoLoadThresholdValue?.let {
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(
                    it, shouldHideDecimals
                )
            }
        )
        setOtherButtonBackground(isUserDefinedAutoLoadValue, binding.balanceOtherButton)
    }

    private fun setAutoLoadValueFromConfig(radioGroup: RadioGroup, value: Int, button: Button) {
        radioGroup.clearCheck()
        if (radioGroup.childCount > 0) {
            for (i in 0 until radioGroup.childCount) {
                val o = radioGroup.getChildAt(i)
                if ((o as BmRadioButton).tag.toString() == value.toString()) {
                    isUserDefinedAutoLoadValue = false
                    (radioGroup.getChildAt(i) as BmRadioButton).isChecked = true
                    break
                } else {
                    isUserDefinedAutoLoadValue = true
                    (radioGroup.getChildAt(i) as BmRadioButton).isChecked = false
                }
            }
            setOtherButtonBackground(isUserDefinedAutoLoadValue, button)
        }
    }

    private fun setOtherButtonBackground(isSelected: Boolean, button: Button) {
        if (isSelected) {
            button.backgroundTintList = ColorStateList.valueOf(confHelper.accentThemeBacgroundColor)
            button.setTextColor(confHelper.accentThemePrimaryTextColor)
        } else {
            button.backgroundTintList =
                ColorStateList.valueOf(confHelper.accentThemePrimaryTextColor)
            button.setTextColor(confHelper.accentThemeBacgroundColor)
        }
    }

    private fun validateSaveAutoloadButton(setEnabled: Boolean = true) {
        var isEnabled = setEnabled
        if (selectedPaymentMethods.isNotEmpty()) {
            if (selectedPaymentMethods[0] is Card) {
                val (_, _, typeId, _, _, _, cvvRequired) = selectedPaymentMethods[0] as Card
                if (cvvRequired) {
                    if (typeId == 3 && binding.include.editTextFirstCardCVV.text.toString().length != 4) {
                        isEnabled = false
                    } else if (binding.include.editTextFirstCardCVV.text.toString().length != 3) {
                        isEnabled = false
                    }
                }
            }
        } else {
            isEnabled = false
        }

        if (BytemarkSDK.isLoggedIn() && selectedPaymentMethods.size > 1) {
            with(binding.include) {
                val firstCard = selectedPaymentMethods[0] as Card
                val secondCard = selectedPaymentMethods[1] as Card
                if (firstCard.cvvRequired) {
                    if (firstCard.typeId == 3 && editTextFirstCardCVV.text.toString().length == 4) {
                        isEnabled = true
                    } else if (editTextFirstCardCVV.text.toString().length == 3) {
                        isEnabled = true
                    }
                } else {
                    isEnabled = true
                }
                if (isEnabled) {
                    if (secondCard.cvvRequired) {
                        isEnabled = false
                        if (firstCard.typeId == 3 && editTextSecondCardCVV.text.toString().length == 4) {
                            isEnabled = true
                        } else if (editTextSecondCardCVV.text.toString().length == 3) {
                            isEnabled = true
                        }
                    } else {
                        isEnabled = true
                    }
                }
                try {
                    if (isEnabled) {
                        val firstAmountString: String =
                            editTextFirstCardAmount.text.toString().replace("[$,.]".toRegex(), "")
                        val firstAmountParsed =
                            firstAmountString.replace("[^\\d]".toRegex(), "").toInt()
                        val secondAmountString: String =
                            editTextSecondCardAmount.text.toString().replace("[$,.]".toRegex(), "")
                        val secondAmountParsed =
                            secondAmountString.replace("[^\\d]".toRegex(), "").toInt()
                        isEnabled = firstAmountParsed + secondAmountParsed == amountValue
                    }
                } catch (e: java.lang.Exception) {
                    isEnabled = false
                }
            }
        }

        if (binding.amountHorizontalScrollView.visibility == View.VISIBLE) {
            if (binding.amountRadioGroup.checkedRadioButtonId == -1) {
                isEnabled = false
            }
        } else if (binding.amountOtherTextView.visibility == View.VISIBLE) {
            val amount = binding.amountOtherTextView.tag as Int?
            if (amount == null)
                isEnabled = false
        }
        if (binding.balanceHorizontalScrollView.visibility == View.VISIBLE) {
            if (binding.balanceRadioGroup.checkedRadioButtonId == -1) {
                isEnabled = false
            }
        } else if (binding.balanceOtherTextView.visibility == View.VISIBLE) {
            val balance = binding.balanceOtherTextView.tag as Int?
            if (balance == null)
                isEnabled = false
        }
        if(!isOnline()) {
            isEnabled = false
        }
        binding.saveAutoloadButton.isEnabled = isEnabled
        if (!isEnabled) {
            val alphaComponent =
                ColorUtils.setAlphaComponent(confHelper.accentThemeBacgroundColor, 26)
            binding.saveAutoloadButton.backgroundTintList = ColorStateList.valueOf(alphaComponent)
        } else {
            binding.saveAutoloadButton.backgroundTintList =
                ColorStateList.valueOf(confHelper.accentThemeBacgroundColor)
        }
    }

    private fun showCurrentAutoloadBar(
        amountValue: Int,
        balanceValue: Int,
        autoloadConfigValue: String?
    ) {
        if (amountValue != 0) {
            val value = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(amountValue)
            val message = if (autoloadConfigValue != null) {
                String.format(
                    getString(R.string.autoload_update_calendar),
                    value,
                    autoloadConfigValue
                )
            } else {
                val threshold =
                    confHelper.getConfigurationPurchaseOptionsCurrencySymbol(balanceValue)
                String.format(getString(R.string.autoload_update), value, threshold)
            }
            binding.currentAutoloadValueTextView.text = message
            binding.currentAutoloadGroup.show()
            binding.currentAutoloadGroupView.show()
            binding.paymentMethodTextView.show()
            val paymentMethods = convertPaymentMethodsToList(availablePaymentMethods)
            var walletCard: Any? = null
            paymentMethods.forEach {
                if (it is Card && it.uuid == walletCardUuid) {
                    walletCard = it
                    binding.autoloadCardTextView.text =
                        "${(walletCard as Card).typeName} ${(walletCard as Card).lastFour}"
                } else if (it is BraintreePaypalPaymentMethod && it.uuid == walletCardUuid) {
                    binding.autoloadCardTextView.text = getString(R.string.payment_payPal)
                    walletCard = it
                }
            }
            if (walletCard == null) {
                binding.autoloadCardTextView.hide()
                binding.paymentMethodTextView.hide()
            }
        }
    }

    private fun hideCurrentAutoloadBar() {
        binding.currentAutoloadGroup.hide()
        binding.currentAutoloadGroupView.hide()
        binding.paymentMethodTextView.hide()
    }

    private fun setAutoloadValueFromConfigForCalendar(
        autoloadConfigName: String,
        autoloadConfigValue: String
    ) {
        binding.autoloadSpinner.setSelection(1)
        binding.timeSpinner.setSelection(getIndex(binding.timeSpinner, autoloadConfigName))
        binding.timeValueSpinner.postDelayed(
            {
                binding.timeValueSpinner.setSelection(
                    getIndex(
                        binding.timeValueSpinner,
                        autoloadConfigValue
                    )
                )
            }, 300
        )
    }

    private fun getIndex(spinner: Spinner, value: String): Int {
        for (i in 0 until spinner.count) {
            if (spinner.getItemAtPosition(i).toString().equals(value, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }

    private fun showSuccessAutoloadDialog() {
        val value = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(amountValue)
        val message = if (autoloadTimeValue != null) {
            String.format(getString(R.string.autoload_update_calendar), value, autoloadTimeValue)
        } else {
            val threshold = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(balanceValue)
            String.format(getString(R.string.autoload_update), value, threshold)
        }
        showDialog(
            getString(R.string.success),
            message,
            getString(R.string.ok),
            finishActivity = true
        )
    }

    private fun showSuccessAutoloadDeleteDialog() {
        showDialog(
            getString(R.string.success),
            "",
            getString(R.string.ok),
            finishActivity = false
        )
    }
}
