package co.bytemark.autoload

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.util.replaceFragment

class AutoloadActivity : MasterActivity() {

    override fun getLayoutRes(): Int = R.layout.activity_autoload

    override fun useHamburgerMenu(): Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(intent.extras?.getString(AppConstants.TITLE))
        replaceFragment(
            AutoloadFragment.newInstance(
                intent.getStringExtra(AppConstants.FARE_MEDIA_ID),
                null,
                getString(R.string.autoload_title)
            ), R.id.container
        )
    }
}