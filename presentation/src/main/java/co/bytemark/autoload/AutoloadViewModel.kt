package co.bytemark.autoload

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import co.bytemark.R
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.interactor.autoload.*
import co.bytemark.domain.interactor.payments.GetPaymentMethodsUseCase
import co.bytemark.domain.interactor.payments.GetPaymentMethodsUseCaseValue
import co.bytemark.domain.interactor.product.GetAcceptedPaymentMethodsUseCase
import co.bytemark.domain.interactor.product.GetAcceptedPaymentMethodsUseCaseValue
import co.bytemark.domain.model.autoload.Autoload
import co.bytemark.domain.model.autoload.WalletCalendarAutoload
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.payments.CreateWalletAutoload
import co.bytemark.helpers.AppConstants.DEFAULT_DEVICE_STORAGE
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.BMError
import co.bytemark.sdk.BytemarkSDK.ResponseCode.*
import co.bytemark.sdk.model.common.Data
import co.bytemark.sdk.model.payment_methods.PaymentMethods
import co.bytemark.sdk.network_impl.BMNetwork
import co.bytemark.sdk.network_impl.BaseNetworkRequestCallback
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class AutoloadViewModel @Inject constructor(
    private val getPaymentMethodsUseCase: GetPaymentMethodsUseCase,
    private val getAutoloadUseCase: GetAutoloadForFareMediumUseCase,
    private val saveFareMediumAutoloadUseCase: SaveFareMediumAutoloadUseCase,
    private val updateFareMediumAutoloadUseCase: UpdateFareMediumAutoloadUseCase,
    private val deleteFareMediumAutoloadUseCase: DeleteFareMediumAutoloadUseCase,
    private val getAutoLoadForWalletUseCase: GetAutoLoadForWalletUseCase,
    private val saveAutoLoadForWalletUseCase: SaveAutoLoadForWalletUseCase,
    private val updateAutoLoadForWalletUseCase: UpdateAutoLoadForWalletUseCase,
    private val deleteAutoLoadForWalletUseCase: DeleteAutoLoadForWalletUseCase,
    private val getAutoLoadConfigurationUseCase: AutoLoadConfigurationUseCase,
    private var bmNetwork: BMNetwork,
    private var sharedPreferences: SharedPreferences,
    private var confHelper: ConfHelper,
    private val getAcceptedPaymentMethodsUseCase: GetAcceptedPaymentMethodsUseCase
) : BaseViewModel() {

    val paymentLiveData by lazy { MutableLiveData<PaymentMethods>() }
    val acceptedPaymentLiveData by lazy { MutableLiveData<MutableList<String>>() }
    val paymentErrorLiveData by lazy { MutableLiveData<Unit>() }
    val displayLiveData by lazy { MutableLiveData<Display>() }
    val walletAutoloadLiveData by lazy { MutableLiveData<WalletCalendarAutoload>() }
    val deleteAutoloadLiveData by lazy { MutableLiveData<Boolean>() }
    val autoloadConfig by lazy { MutableLiveData<LoadConfig>() }
    val fareMediumAutoloadLiveData by lazy { MutableLiveData<Autoload>() }
    val saveAutoloadLiveData by lazy { MutableLiveData<Autoload>() }
    val updateAutoloadLiveData by lazy { MutableLiveData<Autoload>() }

    fun getAutoloadConfig() = uiScope.launch {
        displayLiveData.value =
            Display.EmptyState.Loading(R.drawable.tickets_material, R.string.loading)
        when (val result = getAutoLoadConfigurationUseCase(UseCase.EmptyRequestValues())) {
            is Result.Success -> {
                autoloadConfig.value = result.data
            }
            is Result.Failure -> {
                errorLiveData.value = result.bmError.first()
            }
        }
    }

    fun getAutoloadConfigOfWallet(walletUuid: String) = uiScope.launch {
        when (val result = getAutoLoadForWalletUseCase(
            GetAutoLoadForWalletRequestValue(
                walletUuid
            )
        )) {
            is Result.Success -> {
                displayLiveData.value = Display.EmptyState.ShowContent()
                walletAutoloadLiveData.value = result.data
            }
            is Result.Failure -> {
                displayLiveData.value = Display.EmptyState.ShowContent()
            }
        }
    }

    fun getAutoloadConfigOfFareMedium(fareMediumId: String) = uiScope.launch {
        when (val result = getAutoloadUseCase(GetAutoLoadForFareMediumRequestValue(fareMediumId))) {
            is Result.Success -> {
                displayLiveData.value = Display.EmptyState.ShowContent()
                fareMediumAutoloadLiveData.value = result.data?.autoload
            }
            is Result.Failure -> {
                displayLiveData.value = Display.EmptyState.ShowContent()
            }
        }
    }

    fun getAcceptedPaymentMethod() = uiScope.launch {
        getAcceptedPaymentMethodsUseCase.getLiveData(
            GetAcceptedPaymentMethodsUseCaseValue(
                confHelper.organizationUuid
            )
        ).observeForever {
            when (it) {
                is Result.Success -> {
                    acceptedPaymentLiveData.value = it.data
                }
            }
        }
    }

    fun getPaymentMethod(organizationUUID: String?, isV2PaymentMethodsEnabled: Boolean) =
        uiScope.launch {
            if (isV2PaymentMethodsEnabled) {
                getPaymentMethodsUseCase.getLiveData(GetPaymentMethodsUseCaseValue(organizationUUID))
                    .observeForever {
                        when (it) {
                            is Result.Success -> {
                                paymentLiveData.value = it.data
                            }
                            is Result.Failure -> {
                                errorLiveData.value = it.bmError.first()
                                paymentErrorLiveData.value = Unit
                            }
                        }
                    }
            } else {
                bmNetwork.getPaymentMethods(organizationUUID, object : BaseNetworkRequestCallback {

                    override fun onNetworkRequestSuccessWithUnexpectedError() {
                        errorLiveData.value =
                            co.bytemark.domain.model.common.BMError(UNEXPECTED_ERROR)
                    }

                    override fun onNetworkRequestSuccessWithDeviceTimeError() {
                    }

                    override fun <T : Any?> onNetworkRequestSuccessWithResponse(listOf: ArrayList<T>?) {
                    }

                    override fun onNetworkRequestSuccessWithResponse(`object`: Any?) {
                        val data = `object` as Data
                        val paymentMethods = PaymentMethods()
                        paymentMethods.cards = data.cards
                        paymentMethods.dotPay = data.dotPay
                        paymentMethods.googlePay = data.googlePay
                        paymentMethods.braintreePaypalPaymentMethods = data.paypalList
                        paymentLiveData.value = paymentMethods
                    }

                    override fun onNetworkRequestSuccessWithError(errors: BMError?) {
                        paymentErrorLiveData.value = Unit
                        var bmError = co.bytemark.domain.model.common.BMError(UNEXPECTED_ERROR)
                        when (errors?.code) {
                            DEVICE_LOST_OR_STOLEN -> bmError =
                                co.bytemark.domain.model.common.BMError(DEVICE_LOST_OR_STOLEN)
                            SESSION_EXPIRED -> bmError =
                                co.bytemark.domain.model.common.BMError(DEVICE_LOST_OR_STOLEN)
                        }
                        errorLiveData.value = bmError
                    }

                    override fun onNetworkRequestSuccessfullyCompleted() {
                    }
                })
            }
        }

    fun deleteAutoload(
        fareMediaId: String?,
        walletUuid: String?
    ) = uiScope.launch {
        displayLiveData.value =
            Display.EmptyState.Loading(R.drawable.tickets_material, R.string.loading)
        if (fareMediaId != null) {
            deleteAutoloadForFareMedium(fareMediaId)
        } else if (walletUuid != null) {
            deleteAutoloadForWallet(walletUuid)
        }
    }

    fun saveAutoload(
        paymentList: MutableList<co.bytemark.sdk.post_body.Payment>,
        fareMediaId: String?,
        walletUuid: String?,
        isAutoloadAvailable: Boolean,
        amountValue: Int,
        balanceValue: Int,
        autoloadValueId: Int?
    ) = uiScope.launch {
        displayLiveData.value =
            Display.EmptyState.Loading(R.drawable.tickets_material, R.string.loading)
        if (fareMediaId != null) {
            var saveToDevice = sharedPreferences.getInt(DEFAULT_DEVICE_STORAGE, -1)
            val savePassToDevice = confHelper.savePassToDevice()
            if (saveToDevice == -1) {
                saveToDevice = if (savePassToDevice) 0 else 1
            }
            val requestValues = FareMediaAutoloadRequestValues(
                paymentList,
                fareMediaId,
                UUID.randomUUID().toString(),
                0.0,
                0.0,
                saveToDevice,
                amountValue,
                balanceValue,
                autoloadValueId
            )
            if (isAutoloadAvailable) {
                updateAutoloadForFareMedium(requestValues)
            } else {
                saveAutoloadForFareMedium(requestValues)
            }
        } else if (walletUuid != null) {
            val requestValues = CreateWalletAutoload(
                amountValue,
                balanceValue, true,
                paymentList, walletUuid, autoloadValueId
            )
            if (isAutoloadAvailable) {
                updateAutoloadForWallet(requestValues)
            } else {
                saveAutoloadForWallet(requestValues)
            }
        }
    }

    private fun saveAutoloadForWallet(createWalletAutoload: CreateWalletAutoload) = uiScope.launch {
        when (val result = saveAutoLoadForWalletUseCase(
            SetAutoLoadForWalletUseCaseValue(
                createWalletAutoload
            )
        )) {
            is Result.Success -> {
                displayLiveData.value = Display.EmptyState.ShowContent()
                saveAutoloadLiveData.value = result.data
            }
            is Result.Failure -> {
                displayLiveData.value = Display.EmptyState.ShowContent()
                errorLiveData.value = result.bmError.first()
            }
        }
    }

    private fun updateAutoloadForWallet(createWalletAutoload: CreateWalletAutoload) =
        uiScope.launch {
            when (val result = updateAutoLoadForWalletUseCase(
                UpdateAutoLoadForWalletUseCaseValue(
                    createWalletAutoload
                )
            )) {
                is Result.Success -> {
                    displayLiveData.value = Display.EmptyState.ShowContent()
                    updateAutoloadLiveData.value = result.data
                }
                is Result.Failure -> {
                    displayLiveData.value = Display.EmptyState.ShowContent()
                    errorLiveData.value = result.bmError.first()
                }
            }
        }

    private fun deleteAutoloadForWallet(walletUuid: String) = uiScope.launch {
        when (val result = deleteAutoLoadForWalletUseCase(
            DeleteAutoLoadForWalletUseCaseValue(
                walletUuid
            )
        )) {
            is Result.Success -> {
                displayLiveData.value = Display.EmptyState.ShowContent()
                deleteAutoloadLiveData.value = result.data?.isAutoloadDeleted
            }
            is Result.Failure -> {
                displayLiveData.value = Display.EmptyState.ShowContent()
                errorLiveData.value = result.bmError.first()
            }
        }
    }

    private fun saveAutoloadForFareMedium(fareMediaAutoloadRequestValues: FareMediaAutoloadRequestValues) =
        uiScope.launch {
            when (val result = saveFareMediumAutoloadUseCase(fareMediaAutoloadRequestValues)) {
                is Result.Success -> {
                    displayLiveData.value = Display.EmptyState.ShowContent()
                    saveAutoloadLiveData.value = result.data
                }
                is Result.Failure -> {
                    displayLiveData.value = Display.EmptyState.ShowContent()
                    errorLiveData.value = result.bmError.first()
                }
            }
        }

    private fun updateAutoloadForFareMedium(fareMediaAutoloadRequestValues: FareMediaAutoloadRequestValues) =
        uiScope.launch {
            when (val result = updateFareMediumAutoloadUseCase(fareMediaAutoloadRequestValues)) {
                is Result.Success -> {
                    displayLiveData.value = Display.EmptyState.ShowContent()
                    updateAutoloadLiveData.value = result.data
                }
                is Result.Failure -> {
                    displayLiveData.value = Display.EmptyState.ShowContent()
                    errorLiveData.value = result.bmError.first()
                }
            }
        }

    private fun deleteAutoloadForFareMedium(fareMediaId: String) = uiScope.launch {
        when (val result =
            deleteFareMediumAutoloadUseCase(GetAutoLoadForFareMediumRequestValue(fareMediaId))) {
            is Result.Success -> {
                displayLiveData.value = Display.EmptyState.ShowContent()
                deleteAutoloadLiveData.value = result.data?.isAutoloadDeleted
            }
            is Result.Failure -> {
                displayLiveData.value = Display.EmptyState.ShowContent()
                errorLiveData.value = result.bmError.first()
            }
        }
    }
}