package co.bytemark.autoload

class AutoloadType(val id: Int, val title: String) {

    override fun toString(): String {
        return title
    }
}