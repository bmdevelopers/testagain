package co.bytemark.payment_methods

enum class PaymentsClassName {
    Card, BraintreePaypalPaymentMethod, GooglePay, DotPay, Ideal, PayNearMe, Wallet, Incomm
}