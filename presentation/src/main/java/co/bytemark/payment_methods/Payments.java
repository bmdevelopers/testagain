package co.bytemark.payment_methods;

import android.content.Context;

import com.google.android.gms.common.api.ApiException;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import co.bytemark.analytics.AnalyticsPlatformsContract;
import co.bytemark.domain.error.BytemarkException;
import co.bytemark.domain.interactor.payments.DeletePayPalAccountUseCase;
import co.bytemark.domain.interactor.payments.DeletePayPalAccountUseCaseValue;
import co.bytemark.domain.interactor.payments.DeletePaymentMethodUseCase;
import co.bytemark.domain.interactor.payments.GetPaymentMethodsUseCase;
import co.bytemark.domain.interactor.payments.GetPaymentMethodsUseCaseValue;
import co.bytemark.domain.interactor.product.GetAcceptedPaymentMethodsUseCase;
import co.bytemark.domain.interactor.product.GetAcceptedPaymentMethodsUseCaseValue;
import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.mvp.MvpBasePresenter;
import co.bytemark.mvp.MvpView;
import co.bytemark.sdk.BMError;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.GetPaymentMethods;
import co.bytemark.sdk.model.common.Data;
import co.bytemark.sdk.model.payment_methods.BraintreePaypalPaymentMethod;
import co.bytemark.sdk.model.payment_methods.Card;
import co.bytemark.sdk.model.payment_methods.PaymentMethods;
import co.bytemark.sdk.network_impl.BMNetwork;
import co.bytemark.sdk.network_impl.BaseNetworkRequestCallback;
import co.bytemark.shopping_cart.GooglePayUtil;
import rx.SingleSubscriber;

import static co.bytemark.sdk.network_impl.BaseNetworkRequest.UNAUTHORISED;

public interface Payments {

    interface View extends MvpView {

        void setCards(Data data);

        void showLoadingPaymentMethodsErrorDialog();

        void showDeviceLostOrStolenError();

        void showSessionExpiredError(String message);

        void closeSession();

        void showDeviceTimeError();

        void showLoadingPaymentMethodsErrorDialog(String message);

        void requestRestart();

        void showDeletingPaymentMethodsErrorDialog();

        void showDefaultError(String message);

        void setGooglePayReady(boolean googlePayReady);

        void showAppUpdateDialog();

        void setAcceptedPaymentMethods(ArrayList<String> acceptedPaymentMethods);

        void showFloatingActionButton();

        void logPaymentMethodRemovedAnalyticsEvent(String paymentType, String status, String errorMessage);
    }

    class Presenter extends MvpBasePresenter<View> {

        private final DeletePayPalAccountUseCase deletePayPalTokenUseCase;
        private BMNetwork bmNetwork;
        private GetPaymentMethods getPaymentMethods;
        private GetPaymentMethodsUseCase getPaymentMethodsUseCase;
        private GooglePayUtil googlePayUtil;
        private ConfHelper confHelper;
        private DeletePaymentMethodUseCase deletePaymentMethodUseCase;
        private GetAcceptedPaymentMethodsUseCase getAcceptedPaymentMethodsUseCase;


        @Inject
        public Presenter(BMNetwork bmNetwork, DeletePayPalAccountUseCase deletePayPalAccountUseCase,
                         GooglePayUtil googlePayUtil, GetPaymentMethodsUseCase getPaymentMethodsUseCase,
                         GetAcceptedPaymentMethodsUseCase getAcceptedPaymentMethodsUseCase,
                         DeletePaymentMethodUseCase deletePaymentMethodUseCase,
                         ConfHelper confHelper) {
            this.deletePayPalTokenUseCase = deletePayPalAccountUseCase;
            this.deletePaymentMethodUseCase = deletePaymentMethodUseCase;
            this.bmNetwork = bmNetwork;
            this.googlePayUtil = googlePayUtil;
            this.getPaymentMethodsUseCase = getPaymentMethodsUseCase;
            this.getAcceptedPaymentMethodsUseCase = getAcceptedPaymentMethodsUseCase;
            this.confHelper = confHelper;
        }

        void loadPayments(String organizationUUID) {
            if (confHelper.isV2PaymentMethodsEnabled()) {
                if (BytemarkSDK.isLoggedIn()) {
                    getPaymentMethodsUseCase.singleExecute(new GetPaymentMethodsUseCaseValue(organizationUUID),
                            new SingleSubscriber<PaymentMethods>() {
                                @Override
                                public void onSuccess(PaymentMethods paymentMethods) {
                                    Data data = new Data();
                                    data.cards = paymentMethods.getCards();
                                    if (paymentMethods.getPayPalAccounts() != null) {
                                        data.paypalList = paymentMethods.getPayPalAccounts().getBraintreePaypalPaymentMethods();
                                    }
                                    data.ideal = paymentMethods.getIdeal();
                                    data.dotPay = paymentMethods.getDotPay();
                                    data.googlePay = paymentMethods.getGooglePay();
                                    data.payNearMe = paymentMethods.getPayNearMe();
                                    data.incomm = paymentMethods.getIncomm();
                                    getView().setCards(data);
                                }

                                @Override
                                public void onError(Throwable error) {
                                    if (error instanceof BytemarkException) {
                                        switch (((BytemarkException) error).getStatusCode()) {
                                            case BytemarkSDK.ResponseCode.DEVICE_LOST_OR_STOLEN:
                                                if (isViewAttached()) {
                                                    getView().showDeviceLostOrStolenError();
                                                }
                                                break;
                                            case BytemarkSDK.ResponseCode.SESSION_EXPIRED:
                                                if (isViewAttached()) {
                                                    getView().showSessionExpiredError(error.getMessage());
                                                }
                                                break;
                                            case UNAUTHORISED:
                                                if (isViewAttached()) {
                                                    getView().closeSession();
                                                }
                                                break;
                                            case BytemarkSDK.ResponseCode.SDK_VERSION_OUT_OF_DATE:
                                                if (isViewAttached()) {
                                                    getView().showAppUpdateDialog();
                                                }
                                                break;
                                            default:
                                                if (isViewAttached()) {
                                                    getView().showLoadingPaymentMethodsErrorDialog(error.getMessage());
                                                }
                                        }
                                    } else {
                                        if (isViewAttached()) {
                                            getView().showDefaultError(error.getMessage());
                                        }
                                    }
                                }
                            }
                    );
                }
            } else {
                getPaymentMethods = bmNetwork.getPaymentMethods(organizationUUID, new BaseNetworkRequestCallback() {
                    @Override
                    public void onNetworkRequestSuccessfullyCompleted() {
                    }

                    @Override
                    public <T> void onNetworkRequestSuccessWithResponse(ArrayList<T> listOf) {
                    }

                    @Override
                    public void onNetworkRequestSuccessWithResponse(Object object) {
                        if (isViewAttached()) {
                            getView().setCards((Data) object);
                        }
                    }

                    @Override
                    public void onNetworkRequestSuccessWithUnexpectedError() {
                        if (isViewAttached()) {
                            getView().showLoadingPaymentMethodsErrorDialog();
                        }
                    }

                    @Override
                    public void onNetworkRequestSuccessWithError(BMError errors) {
                        switch (errors.getCode()) {
                            case BytemarkSDK.ResponseCode.DEVICE_LOST_OR_STOLEN:
                                if (isViewAttached()) {
                                    getView().showDeviceLostOrStolenError();
                                }
                                break;
                            case BytemarkSDK.ResponseCode.SESSION_EXPIRED:
                                if (isViewAttached()) {
                                    getView().showSessionExpiredError(errors.getMessage());
                                }
                                break;
                            case UNAUTHORISED:
                                if (isViewAttached()) {
                                    getView().closeSession();
                                }
                                break;
                            case BytemarkSDK.ResponseCode.SDK_VERSION_OUT_OF_DATE:
                                if (isViewAttached()) {
                                    getView().showAppUpdateDialog();
                                }
                                break;
                            default:
                                if (isViewAttached()) {
                                    getView().showLoadingPaymentMethodsErrorDialog(errors.getMessage());
                                }
                        }
                    }

                    @Override
                    public void onNetworkRequestSuccessWithDeviceTimeError() {
                        if (isViewAttached()) {
                            getView().showDeviceTimeError();
                        }
                    }
                });
            }

        }

        void deletePayments(final Card card) {
            if (confHelper.isV2PaymentMethodsEnabled()) {

                DeletePaymentMethodUseCase.DeletePaymentMethodUseCaseValue requestValue =
                        new DeletePaymentMethodUseCase.DeletePaymentMethodUseCaseValue(card.getUuid());

                deletePaymentMethodUseCase.singleExecute(requestValue, new SingleSubscriber<BMResponse>() {
                    @Override
                    public void onSuccess(BMResponse bmResponse) {
                        if (isViewAttached()) {
                            getView().requestRestart();
                            getView().logPaymentMethodRemovedAnalyticsEvent(AnalyticsPlatformsContract.PaymentType.CARD,
                                    AnalyticsPlatformsContract.Status.SUCCESS, "");
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        if (isViewAttached()) {
                            if (error instanceof BytemarkException) {
                                getView().showAppUpdateDialog();
                                return;
                            }
                            getView().showDefaultError(error.getMessage());
                            getView().logPaymentMethodRemovedAnalyticsEvent(AnalyticsPlatformsContract.PaymentType.CARD,
                                    AnalyticsPlatformsContract.Status.FAILURE, error.getMessage());
                        }
                    }
                });

            } else {
                bmNetwork.deletePaymentMethod(card.getUuid(), new BaseNetworkRequestCallback() {
                    @Override
                    public void onNetworkRequestSuccessfullyCompleted() {
                        if (isViewAttached()) {
                            getView().requestRestart();
                            getView().logPaymentMethodRemovedAnalyticsEvent(AnalyticsPlatformsContract.PaymentType.CARD,
                                    AnalyticsPlatformsContract.Status.SUCCESS, "");
                        }
                    }

                    @Override
                    public <T> void onNetworkRequestSuccessWithResponse(ArrayList<T> listOf) {
                        if (isViewAttached()) {
                        }
                    }

                    @Override
                    public void onNetworkRequestSuccessWithResponse(Object object) {

                    }

                    @Override
                    public void onNetworkRequestSuccessWithUnexpectedError() {
                        if (isViewAttached()) {
                            getView().showDeletingPaymentMethodsErrorDialog();
                        }
                    }

                    @Override
                    public void onNetworkRequestSuccessWithError(BMError errors) {
                        if (isViewAttached()) {
                            switch (errors.getCode()) {
                                case BytemarkSDK.ResponseCode.DEVICE_LOST_OR_STOLEN:
                                    getView().showDeviceLostOrStolenError();
                                    break;
                                case BytemarkSDK.ResponseCode.SESSION_EXPIRED:
                                    getView().showSessionExpiredError(errors.getMessage());
                                    break;
                                case BytemarkSDK.ResponseCode.SDK_VERSION_OUT_OF_DATE:
                                    getView().showAppUpdateDialog();
                                    break;
                                default:
                                    getView().showDefaultError(errors.getMessage());
                            }
                            getView().logPaymentMethodRemovedAnalyticsEvent(AnalyticsPlatformsContract.PaymentType.CARD,
                                    AnalyticsPlatformsContract.Status.FAILURE, errors.getMessage());
                        }
                    }

                    @Override
                    public void onNetworkRequestSuccessWithDeviceTimeError() {
                        if (isViewAttached()) {
                            getView().showDeviceTimeError();
                        }
                    }
                });
            }
        }

        public void unSubscribe() {
            if (getPaymentMethods != null)
                getPaymentMethods.unSubscribe();
        }

        public void deletePayPalAccount(BraintreePaypalPaymentMethod paypalPaymentMethod) {

            if (confHelper.isV2PaymentMethodsEnabled()) {
                DeletePaymentMethodUseCase.DeletePaymentMethodUseCaseValue requestValue =
                        new DeletePaymentMethodUseCase.DeletePaymentMethodUseCaseValue(paypalPaymentMethod.getUuid());

                deletePaymentMethodUseCase.singleExecute(requestValue, new SingleSubscriber<BMResponse>() {
                    @Override
                    public void onSuccess(BMResponse bmResponse) {
                        if (isViewAttached()) {
                            getView().requestRestart();
                            getView().logPaymentMethodRemovedAnalyticsEvent(AnalyticsPlatformsContract.PaymentType.PAYPAL,
                                    AnalyticsPlatformsContract.Status.SUCCESS, "");
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        if (isViewAttached()) {
                            getView().showDefaultError(error.getMessage());
                            getView().logPaymentMethodRemovedAnalyticsEvent(AnalyticsPlatformsContract.PaymentType.PAYPAL,
                                    AnalyticsPlatformsContract.Status.FAILURE, error.getMessage());
                        }
                    }
                });

            } else {
                DeletePayPalAccountUseCaseValue requestValue = new DeletePayPalAccountUseCaseValue(
                        paypalPaymentMethod.getToken());

                deletePayPalTokenUseCase.singleExecute(requestValue, new SingleSubscriber<co.bytemark.domain.model.common.Data>() {
                    @Override
                    public void onSuccess(co.bytemark.domain.model.common.Data data) {

                        if (isViewAttached()) {
                            getView().requestRestart();
                            getView().logPaymentMethodRemovedAnalyticsEvent(AnalyticsPlatformsContract.PaymentType.PAYPAL,
                                    AnalyticsPlatformsContract.Status.SUCCESS, "");
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        if (isViewAttached()) {
                            getView().showDefaultError(error.getMessage());
                            getView().logPaymentMethodRemovedAnalyticsEvent(AnalyticsPlatformsContract.PaymentType.PAYPAL,
                                    AnalyticsPlatformsContract.Status.FAILURE, error.getMessage());
                        }
                    }
                });
            }
        }

        public void createGoolglePayClient(Context context) {
            googlePayUtil.makePaymentClient(context);
        }

        public void isGoolePayReady() {
            googlePayUtil.getGooglePayReadyTask().addOnCompleteListener(
                    task1 -> {
                        try {
                            boolean result = task1.getResult(ApiException.class);
                            if (isViewAttached())
                                getView().setGooglePayReady(result);
                        } catch (ApiException exception) {
                        }
                    });
        }

        public void loadAcceptedPaymentMethod(String organizationUUID) {
            GetAcceptedPaymentMethodsUseCaseValue requestValue = new GetAcceptedPaymentMethodsUseCaseValue(organizationUUID);
            getAcceptedPaymentMethodsUseCase.singleExecute(requestValue, new SingleSubscriber<List<String>>() {
                @Override
                public void onSuccess(List<String> strings) {
                    if (isViewAttached() && strings != null) {
                        ArrayList<String> arrayList = new ArrayList<>();
                        for (String string : strings) {
                            arrayList.add(string.toLowerCase());
                        }
                        getView().setAcceptedPaymentMethods(arrayList);
                        getView().showFloatingActionButton();
                    }
                }

                @Override
                public void onError(Throwable error) {
                    if (isViewAttached()) {
                        getView().showFloatingActionButton();
                    }
                }
            });
        }
    }
}