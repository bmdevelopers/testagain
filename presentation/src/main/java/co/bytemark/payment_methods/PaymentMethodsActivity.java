package co.bytemark.payment_methods;

import android.os.Bundle;

import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.base.MasterActivity;

import static co.bytemark.sdk.model.common.Action.PAYMENT_METHODS;
import static co.bytemark.sdk.model.common.Action.SETTINGS;

public class PaymentMethodsActivity extends MasterActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomerMobileApp.Companion.getComponent().inject(this);
        setToolbarTitle(confHelper.getSettingsMenuScreenTitleFromTheConfig(true, true, PAYMENT_METHODS));
        setNavigationViewCheckedItem(SETTINGS);

    }

    @Override
    protected boolean useHamburgerMenu() {
        return false;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_payment_methods;
    }
}
