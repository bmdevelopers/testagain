package co.bytemark.payment_methods;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hannesdorfmann.adapterdelegates4.AdapterDelegate;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.Helpers.PaymentMethod;
import co.bytemark.sdk.model.payment_methods.BraintreePaypalPaymentMethod;

/**
 * Created by Omkar on 12/4/17.
 */
public class PaypalAdapterDelegate extends AdapterDelegate<List<PaymentMethod>> {
    private ConfHelper confHelper;
    private PaymentsAdapter.Callback callback;
    private ArrayList<PaymentMethod> selectedPayments = new ArrayList<>();

    PaypalAdapterDelegate(ConfHelper confHelper, PaymentsAdapter.Callback callback) {
        this.confHelper = confHelper;
        this.callback = callback;
    }

    @Override
    protected boolean isForViewType(@NonNull List<PaymentMethod> items, int position) {
        return items.get(position) instanceof BraintreePaypalPaymentMethod;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new PaypalViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.paypal_item, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull List<PaymentMethod> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
        PaypalViewHolder paypalViewHolder = (PaypalViewHolder) holder;
        final BraintreePaypalPaymentMethod paypal = (BraintreePaypalPaymentMethod) items.get(position);
        paypalViewHolder.linearLayoutForeground.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        paypalViewHolder.linearLayoutForeground.setOnClickListener(view -> {
            selectedPayments.add(paypal);
            callback.onSelectPaymentMethods(selectedPayments);
        });
        paypalViewHolder.textViewEmail.setText(paypal.getEmail());
        paypalViewHolder.imageViewDelete.setOnClickListener(view -> callback.onDeletePaypal(paypal));

    }

    static class PaypalViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.linearLayoutForeground)
        LinearLayout linearLayoutForeground;
        @BindView(R.id.imageViewCard)
        ImageView imageViewCard;
        @BindView(R.id.textViewLastFour)
        TextView textViewLastFour;
        @BindView(R.id.textViewEmail)
        TextView textViewEmail;
        @BindView(R.id.imageViewDelete)
        ImageView imageViewDelete;


        PaypalViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}