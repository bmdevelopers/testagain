package co.bytemark.payment_methods

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import co.bytemark.R
import co.bytemark.autoload.AutoloadFragment
import co.bytemark.base.TabsActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.model.payment_methods.Wallet
import co.bytemark.shopping_cart.AcceptPaymentFragment

/** Created by Santosh on 2019-08-13.
 */

class LoadMoneyWalletActivity : TabsActivity() {

    private var wallet: Wallet? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        intent.putExtra(AppConstants.TITLE, getString(R.string.payment_load_money))
        wallet = intent.getParcelableExtra(AppConstants.WALLET)
        intent.putExtra(AppConstants.RELOADING_WALLET, wallet)
        intent.putExtra(AppConstants.RELOAD, true)
        super.onCreate(savedInstanceState)
    }

    override fun getFragmentsForTabs(): List<Fragment> {
        return mutableListOf<Fragment>().apply {
            add(AcceptPaymentFragment.newInstance().apply {
                arguments = Bundle().apply {
                    putString(
                        AppConstants.TITLE,
                        this@LoadMoneyWalletActivity.getString(R.string.shopping_cart_reload)
                    )
                }
            })
            add(
                AutoloadFragment.newInstance(
                    null,
                    wallet,
                    this@LoadMoneyWalletActivity.getString(R.string.autoload_title)
                )
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragments = supportFragmentManager.fragments
        for (fragment in fragments) {
            (fragment as? AcceptPaymentFragment)?.onActivityResult(requestCode, resultCode, data)
        }
    }
}