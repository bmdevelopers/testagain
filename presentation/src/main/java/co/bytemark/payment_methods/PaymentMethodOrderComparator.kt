package co.bytemark.payment_methods

import co.bytemark.CustomerMobileApp
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.Helpers.PaymentMethod
import co.bytemark.sdk.model.payment_methods.AbstractPaymentMethod
import co.bytemark.sdk.model.payment_methods.PaymentsOrderName
import javax.inject.Inject

class PaymentMethodOrderComparator(private val googlePayReady: Boolean) : Comparator<PaymentMethod> {

    @Inject
    lateinit var confHelper: ConfHelper

    init {
        CustomerMobileApp.component.inject(this)
    }

    override fun compare(o1: PaymentMethod?, o2: PaymentMethod?): Int {
        val isLhsDefault = isDefaultPaymentMethod(o1)
        val isRhsDefault = isDefaultPaymentMethod(o2)

        val lhsOrder = getOrderOf(o1)
        val rhsOrder = getOrderOf(o2)

        if (isLhsDefault) return -1
        if (isRhsDefault) return 1
        return if (lhsOrder < rhsOrder) -1 else 1
    }

    private fun isDefaultPaymentMethod(paymentMethod: PaymentMethod?): Boolean {
        return (paymentMethod as? AbstractPaymentMethod)?.default ?: false
    }

    private fun getOrderOf(paymentMethod: PaymentMethod?): Int {
        if (paymentMethod != null) {
            val paymentMethodClassName = paymentMethod.javaClass.simpleName
            when (PaymentsClassName.valueOf(paymentMethodClassName)) {
                PaymentsClassName.Card -> return confHelper.getOrderOf(PaymentsOrderName.card)
                PaymentsClassName.BraintreePaypalPaymentMethod -> return confHelper.getOrderOf(PaymentsOrderName.paypal)
                PaymentsClassName.GooglePay -> if (googlePayReady) return confHelper.getOrderOf(PaymentsOrderName.googlepay)
                PaymentsClassName.DotPay -> return confHelper.getOrderOf(PaymentsOrderName.dotpay)
                PaymentsClassName.Ideal -> return confHelper.getOrderOf(PaymentsOrderName.ideal)
                PaymentsClassName.PayNearMe -> return confHelper.getOrderOf(PaymentsOrderName.paynearme)
                PaymentsClassName.Wallet -> return confHelper.getOrderOf(PaymentsOrderName.wallet)
                PaymentsClassName.Incomm -> return confHelper.getOrderOf(PaymentsOrderName.incomm)
            }
        }
        return 1000
    }
}