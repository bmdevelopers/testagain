package co.bytemark.payment_methods

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.Helpers.PaymentMethod
import co.bytemark.sdk.model.payment_methods.Incomm
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.synthetic.main.incomm_item.view.*
import java.util.*

class IncommDelegate(private val confHelper: ConfHelper, val callback: PaymentsAdapter.Callback) :
    AdapterDelegate<MutableList<PaymentMethod>>() {

    private val selectedPaymentMethods = ArrayList<PaymentMethod>()

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        IncommViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.incomm_item, parent, false)
        )

    override fun isForViewType(items: MutableList<PaymentMethod>, position: Int): Boolean =
        items[position] is Incomm

    override fun onBindViewHolder(
        items: MutableList<PaymentMethod>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        with(holder as IncommViewHolder) {
            val paymentMethodName: String = String.format(
                this.itemView.context.getString(R.string.payment_pay_cash_at_store),
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(confHelper.incommMinValue, true),
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(confHelper.incommMaxValue, true)
            )
            val paymentMethodNameAnnouncement: String = String.format(
                this.itemView.context.getString(R.string.payment_pay_cash_at_store_announcement),
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(confHelper.incommMinValue, true),
                confHelper.getConfigurationPurchaseOptionsCurrencySymbol(confHelper.incommMaxValue, true)
            )
            itemView.textViewIncommTitle.contentDescription = paymentMethodNameAnnouncement
            itemView.textViewIncommTitle.text = paymentMethodName
            itemView.constraintLayoutIncommItem.setOnClickListener {
                selectedPaymentMethods.add(items[position])
                callback.onSelectPaymentMethods(selectedPaymentMethods)
            }
        }
    }

    inner class IncommViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}