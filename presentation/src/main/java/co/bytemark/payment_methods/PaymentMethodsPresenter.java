package co.bytemark.payment_methods;


import java.util.ArrayList;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.mvp.MvpBasePresenter;
import co.bytemark.sdk.BMError;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.GetPaymentMethods;
import co.bytemark.sdk.model.payment_methods.Card;
import co.bytemark.sdk.network_impl.BMNetwork;
import co.bytemark.sdk.network_impl.BaseNetworkRequestCallback;

@SuppressWarnings({"ConstantConditions", "unchecked"})
public class PaymentMethodsPresenter extends MvpBasePresenter<PaymentMethodsView> {

    @Inject
    BMNetwork bmNetwork;

    private GetPaymentMethods getPaymentMethods;

    PaymentMethodsPresenter() {
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    void loadPaymentMethods(String organizationUUID) {

        getPaymentMethods = bmNetwork.getPaymentMethods(organizationUUID, new BaseNetworkRequestCallback() {
            @Override
            public void onNetworkRequestSuccessfullyCompleted() {
                if (isViewAttached())
                    getView().hideLoading();
            }

            @Override
            public <T> void onNetworkRequestSuccessWithResponse(ArrayList<T> listOf) {
                ArrayList<Card> cardArrayList = (ArrayList<Card>) listOf;
                if (isViewAttached()) {
                    getView().setSwipeRefreshLayoutRefreshing(false);
                    if (cardArrayList.isEmpty()) {
                        getView().setSwipeRefreshLayoutEnabled(false);
                        getView().showNoPaymentMethodsView();
                    } else {
                        getView().setSwipeRefreshLayoutEnabled(true);
                        getView().setCards(cardArrayList);
                    }
                }
            }

            @Override
            public void onNetworkRequestSuccessWithResponse(Object object) {

            }

            @Override
            public void onNetworkRequestSuccessWithUnexpectedError() {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().setSwipeRefreshLayoutRefreshing(false);
                    getView().showLoadingPaymentMethodsErrorDialog();
                }
            }

            @Override
            public void onNetworkRequestSuccessWithError(BMError errors) {
                switch (errors.getCode()) {
                    case BytemarkSDK.ResponseCode.DEVICE_LOST_OR_STOLEN:
                        if (isViewAttached()) {
                            getView().hideLoading();
                            getView().setSwipeRefreshLayoutRefreshing(false);
                            getView().showDeviceLostOrStolenError();
                        }
                        break;
                    case BytemarkSDK.ResponseCode.SESSION_EXPIRED:
                        if (isViewAttached()) {
                            getView().hideLoading();
                            getView().setSwipeRefreshLayoutRefreshing(false);
                            getView().showSessionExpiredError(errors.getMessage());
                        }
                        break;
                    default:
                        if (isViewAttached()) {
                            getView().hideLoading();
                            getView().setSwipeRefreshLayoutRefreshing(false);
                            getView().showLoadingPaymentMethodsErrorDialog(errors.getMessage());
                        }
                }
            }

            @Override
            public void onNetworkRequestSuccessWithDeviceTimeError() {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().setSwipeRefreshLayoutRefreshing(false);
                    getView().showDeviceTimeError();
                }
            }
        });
    }

    void deletePaymentMethod(final Card card) {

        bmNetwork.deletePaymentMethod(card.getUuid(), new BaseNetworkRequestCallback() {
            @Override
            public void onNetworkRequestSuccessfullyCompleted() {
                if (isViewAttached()) {
                    getView().requestRestart();
                    getView().registerCardDeletionCompletedAnalytics(card.getTypeName());
                }
            }

            @Override
            public <T> void onNetworkRequestSuccessWithResponse(ArrayList<T> listOf) {
                if (isViewAttached()) {
                    getView().hideConfirmDeleting();
                    getView().setSwipeRefreshLayoutRefreshing(false);
                }
            }

            @Override
            public void onNetworkRequestSuccessWithResponse(Object object) {

            }

            @Override
            public void onNetworkRequestSuccessWithUnexpectedError() {
                if (isViewAttached()) {
                    getView().hideConfirmDeleting();
                    getView().setSwipeRefreshLayoutRefreshing(false);
                    getView().showDeletingPaymentMethodsErrorDialog();
                }
            }

            @Override
            public void onNetworkRequestSuccessWithError(BMError errors) {
                switch (errors.getCode()) {
                    case BytemarkSDK.ResponseCode.DEVICE_LOST_OR_STOLEN:
                        if (isViewAttached()) {
                            getView().hideConfirmDeleting();
                            getView().setSwipeRefreshLayoutRefreshing(false);
                            getView().showDeviceLostOrStolenError();
                        }
                        break;
                    case BytemarkSDK.ResponseCode.SESSION_EXPIRED:
                        if (isViewAttached()) {
                            getView().hideLoading();
                            getView().setSwipeRefreshLayoutRefreshing(false);
                            getView().showSessionExpiredError(errors.getMessage());
                        }
                        break;
                    default:
                        if (isViewAttached()) {
                            getView().hideConfirmDeleting();
                            getView().setSwipeRefreshLayoutRefreshing(false);
                            getView().showDefaultError(errors.getMessage());
                        }
                }
            }

            @Override
            public void onNetworkRequestSuccessWithDeviceTimeError() {
                if (isViewAttached()) {
                    getView().hideConfirmDeleting();
                    getView().setSwipeRefreshLayoutRefreshing(false);
                    getView().showDeviceTimeError();
                }
            }
        });
    }

    public void unSubscribe() {
        if (getPaymentMethods != null)
            getPaymentMethods.unSubscribe();
    }
}
