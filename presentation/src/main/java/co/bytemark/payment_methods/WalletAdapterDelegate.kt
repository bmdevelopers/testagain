package co.bytemark.payment_methods

import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.R
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.Helpers.PaymentMethod
import co.bytemark.sdk.model.payment_methods.Wallet
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.synthetic.main.wallet_item.view.*
import java.util.*

class WalletAdapterDelegate(private val confHelper: ConfHelper, private val callback: PaymentsAdapter.Callback)
    : AdapterDelegate<MutableList<PaymentMethod>>() {

    private val selectedPaymentMethods = ArrayList<PaymentMethod>()

    override fun isForViewType(items: MutableList<PaymentMethod>, position: Int): Boolean {
        return items[position] is Wallet
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return WalletViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.wallet_item, parent, false))
    }

    override fun onBindViewHolder(items: MutableList<PaymentMethod>, position: Int, holder: RecyclerView.ViewHolder, payloads: List<Any>) {
        with(holder as WalletViewHolder) {
            val nickName = (items[position] as Wallet).nickName
            val balanceText = itemView.context.getString(R.string.payments_available).format(confHelper.getConfigurationPurchaseOptionsCurrencySymbol((items[position] as Wallet).availableAmount))
            if (nickName != null && nickName.isNotEmpty()) {
                itemView.textViewNickName.text = nickName
                itemView.textViewBalance.text = balanceText
            } else {
                itemView.textViewNickName.text = balanceText
                itemView.textViewBalance.visibility = View.GONE
            }

            itemView.linearLayout.setBackgroundColor(confHelper.dataThemeBackgroundColor)
            itemView.linearLayout.setOnClickListener {
                selectedPaymentMethods.add(items[position])
                callback.onSelectPaymentMethods(selectedPaymentMethods)
            }


            val drawable = ContextCompat.getDrawable(itemView.context, R.drawable.ic_keyboard_arrow_right_white_24dp)
            drawable?.let { myDrawable ->
                DrawableCompat.setTint(myDrawable, confHelper.collectionThemeAccentColor)
                itemView.textViewLoadMoney.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, myDrawable, null)
            }

            itemView.textViewLoadMoney.setOnClickListener {
                callback.onClickLoadMoney(items[position] as Wallet)
            }
        }
    }

    internal class WalletViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}