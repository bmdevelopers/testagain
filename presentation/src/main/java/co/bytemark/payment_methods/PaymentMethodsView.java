package co.bytemark.payment_methods;

import android.content.Context;

import java.util.ArrayList;

import co.bytemark.mvp.MvpView;
import co.bytemark.sdk.model.payment_methods.Card;

interface PaymentMethodsView extends MvpView {

    void showLoadingPaymentMethodsErrorDialog();

    void showDeletingPaymentMethodsErrorDialog();

    void setSwipeRefreshLayoutEnabled(boolean enabled);

    void setSwipeRefreshLayoutRefreshing(boolean refreshing);

    void showLoading();

    void hideLoading();

    void setCards(ArrayList<Card> cardArrayList);

    void showDeviceLostOrStolenError();

    Context getActivityContext();

    void showDeviceTimeError();

    void showDefaultError(String message);

    void showNoPaymentMethodsView();

    void showNetworkConnectionErrorView();

    void hideNetworkConnectionErrorView();

    void showConfirmDeleting();

    void hideConfirmDeleting();

    void requestRestart();

    void showLoadingPaymentMethodsErrorDialog(String errors);

    void showSessionExpiredError(String message);

    void registerCardDeletionCompletedAnalytics(String typeName);
}
