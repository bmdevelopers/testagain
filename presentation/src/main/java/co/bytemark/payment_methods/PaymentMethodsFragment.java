package co.bytemark.payment_methods;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.add_payment_card.AddPaymentCardActivity;
import co.bytemark.base.BaseMvpFragment;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.payment_methods.adapter.PaymentsCardsAdapter;
import co.bytemark.sdk.model.payment_methods.Card;
import co.bytemark.widgets.CircleBorderImageView;
import co.bytemark.widgets.ProgressViewLayout;
import co.bytemark.widgets.stackview.StackAdapter;
import co.bytemark.widgets.stackview.StackViewLayout;
import co.bytemark.widgets.stackview.annotations.Section;
import co.bytemark.widgets.util.Util;
import timber.log.Timber;

public class PaymentMethodsFragment extends BaseMvpFragment<PaymentMethodsView, PaymentMethodsPresenter>
        implements PaymentMethodsView, StackAdapter.StackSelectionListener<Card> {

    @BindView(R.id.ll_loading_payment_methods)
    LinearLayout loadingPaymentMethods;
    @BindView(R.id.ll_no_payment_methods)
    LinearLayout linearLayoutNoPaymentMethods;
    @BindView(R.id.ll_no_network_texts)
    LinearLayout linearLayoutNoNetworkText;
    @BindView(R.id.ll_no_payment_method_texts)
    LinearLayout linearLayoutNoPaymentMethodText;
    @BindView(R.id.ll_network_connectivity_error_view)
    LinearLayout linearLayoutNetworkConnectivityErrorView;
    @BindView(R.id.btn_retry)
    Button buttonRetry;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayoutPaymentMethods;
    @BindView(R.id.payments_card_imageview)
    CircleBorderImageView imageViewPaymentCard;
    @BindView(R.id.iv_network_error)
    CircleBorderImageView imageViewNetworkError;
    @BindView(R.id.pvl)
    ProgressViewLayout progressViewLayout;
    @BindView(R.id.cl_payment_methods)
    CoordinatorLayout coordinatorLayoutPaymentMethods;
    @BindView(R.id.iv_loading_payment)
    ImageView imageViewLoadingPayment;
    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.ll_device_time_error_view)
    LinearLayout linearLayoutDeviceTimeError;
    @BindView(R.id.ll_device_lost_or_stolen_view)
    LinearLayout linearLayoutDeviceLostOrStolen;
    @BindView(R.id.btn_add_card_loading_view)
    Button buttonAddCardLoadingView;
    @BindView(R.id.btn_add_card_no_payment_view)
    Button buttonAddCardNoPaymentView;
    @BindView(R.id.sv_stack_view)
    ScrollView cardScrollView;
    @BindView(R.id.stack_view)
    StackViewLayout cardStackLayout;

    @Inject
    ConfHelper confHelper;
    //@Inject
    //AnalyticsPlatformAdapter analyticsPlatformAdapter;

    private boolean isIntentFromAddCard;
    private boolean isAddedNewCardViewShown;
    private MaterialDialog materialDialog;
    private PaymentsCardsAdapter paymentsCardsAdapter;
    private MaterialDialog errorDeletingMaterialDialog;

    private SupplementaryView supplementaryView;
    private boolean forceLoaded;
    private String organizationUUID;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_payment_methods;
    }

    @Override
    protected void setAccentThemeColors() {
        buttonAddCardLoadingView.setTextColor(confHelper.getBackgroundThemeBackgroundColor());
        buttonAddCardLoadingView.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));

        buttonAddCardNoPaymentView.setTextColor(confHelper.getBackgroundThemeBackgroundColor());
        buttonAddCardNoPaymentView.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));

        buttonRetry.setTextColor(confHelper.getBackgroundThemeBackgroundColor());
        buttonRetry.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));

    }

    @Override
    protected void setBackgroundThemeColors() {
        floatingActionButton.setImageResource(R.drawable.ic_add_icon);
        floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        floatingActionButton.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeBackgroundColor()));
        coordinatorLayoutPaymentMethods.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        linearLayoutNoPaymentMethods.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        linearLayoutDeviceLostOrStolen.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        loadingPaymentMethods.setBackgroundColor(confHelper.getBackgroundThemeBackgroundColor());
        progressViewLayout.setProgressColor(confHelper.getBackgroundThemeAccentColor(), confHelper.getBackgroundThemeBackgroundColor());
        imageViewLoadingPayment.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        imageViewPaymentCard.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        imageViewNetworkError.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemePrimaryTextColor()));
    }

    @Override
    protected void setDataThemeColors() {

    }

    @NonNull
    @Override
    public PaymentMethodsPresenter createPresenter() {
        return new PaymentMethodsPresenter();
    }

    @Override
    protected void onInject() {
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // FIXME: 2/8/17 This is commented out to fix android-1609.
//        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        organizationUUID = getActivity().getIntent().getStringExtra(AppConstants.ORG_UUID);
        announceForAccessibility();
        initStackLayout();
    }

    private void initStackLayout() {
        paymentsCardsAdapter = new PaymentsCardsAdapter(getActivity());
        paymentsCardsAdapter.setSelectionListener(this);

        cardStackLayout.setSecondHeaderText(R.string.active_tickets);
        cardStackLayout.setMinimumScaleFactor(1);
        cardStackLayout.setMinimumScaleFactorSelected(0.98f);
        cardStackLayout.setStackGapSelectedPx(Util.dpToPx(20));
        cardStackLayout.setAdapter(paymentsCardsAdapter);
        cardStackLayout.showTopHeader(false);

        final View supplementaryViewLayout = cardStackLayout.setSupplementaryViewLayout(R.layout.payments_methods_supplementary_view);
        supplementaryView = new SupplementaryView(supplementaryViewLayout);

        supplementaryView.removeCardText.setTextColor(confHelper.getBackgroundThemePrimaryTextColor());

        supplementaryView.textViewSelectCard.setTextColor(confHelper.getBackgroundThemePrimaryTextColor());

        supplementaryView.buttonRemoveCard.setTextColor(confHelper.getBackgroundThemeBackgroundColor());
        supplementaryView.buttonRemoveCard.setBackgroundTintList(ColorStateList.valueOf(confHelper.getIndicatorsError()));

        supplementaryView.buttonSelectCard.setTextColor(confHelper.getBackgroundThemeBackgroundColor());
        supplementaryView.buttonSelectCard.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));

        supplementaryView.buttonSelectCardWhenAdded.setTextColor(confHelper.getBackgroundThemeBackgroundColor());
        supplementaryView.buttonSelectCardWhenAdded.setBackgroundTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));


        supplementaryView.buttonRemoveCardWhenAdded.setTextColor(confHelper.getBackgroundThemeBackgroundColor());
        supplementaryView.buttonRemoveCardWhenAdded.setBackgroundTintList(ColorStateList.valueOf(confHelper.getIndicatorsError()));

        supplementaryView.textViewAddCardSuccess.setTextColor(confHelper.getIndicatorsSuccess());
        supplementaryView.tvAddCardSuccessSelect.setTextColor(confHelper.getIndicatorsSuccess());
    }

    @Override
    public void onResume() {
        super.onResume();
        setSwipeRefreshLayoutListener();
        if (isIntentFromAddCard)
            refreshNetworkActivity();

//        if (supplementaryView.selectCard.getVisibility() == View.VISIBLE) {
//            supplementaryView.selectCard.setVisibility(View.GONE);
//        } else if (supplementaryView.removeCard.getVisibility() == View.VISIBLE) {
//            supplementaryView.removeCard.setVisibility(View.GONE);
//        } else if (supplementaryView.addedNewCardRemoveView.getVisibility() == View.VISIBLE) {
//            supplementaryView.addedNewCardRemoveView.setVisibility(View.GONE);
//        } else if (supplementaryView.addedNewCardSelectView.getVisibility() == View.VISIBLE) {
//            supplementaryView.addedNewCardSelectView.setVisibility(View.GONE);
//        }
    }

    private void announceForAccessibility() {
        linearLayoutNoNetworkText.setContentDescription(getString(R.string.network_connectivity_error) + ". " +
                getString(R.string.network_connectivity_error_message));

        linearLayoutNoPaymentMethodText.setContentDescription(getString(R.string.payment_you_have_no_pay_methods) +
                getString(R.string.payment_no_pay_method_desc));
    }

    private String getContentDescriptionForSelectText(Card card) {
        return String.format(getResources().getString(R.string.payment_select_card_text_cd), card.getTypeName(), card.getLastFour());
    }

    private String getContentDescriptionForSelectButton(Card card) {
        return String.format(getResources().getString(R.string.payment_select_cart_button_cd), card.getTypeName(), card.getLastFour());
    }

    private String getContentDescriptionForRemoveText(Card card) {
        return String.format(getResources().getString(R.string.payment_remove_card_text_cd), card.getTypeName(), card.getLastFour());
    }

    private String getContentDescriptionForRemoveButton(Card card) {
        return String.format(getResources().getString(R.string.payment_remove_card_button_cd), card.getTypeName(), card.getLastFour());
    }

    private String getContentDescriptionForAddCardSuccess(Card card) {
        return String.format(getResources().getString(R.string.payment_add_card_success_text_cd), card.getTypeName(), card.getLastFour());
    }

//    @Override
//    public void onPause() {
//        super.onPause();
//        presenter.unSubscribe();
//        isPaymentMethodsLoaded = false;
//        isAddedNewCardViewShown = false;
//        isIntentFromAddCard = false;
//    }

    @Override
    public void onDestroy() {
        paymentsCardsAdapter.cleanUp();
        super.onDestroy();
        presenter.unSubscribe();
    }

    @Override
    protected void onOnline() {
        setSwipeRefreshLayoutEnabled(true);
        hideNetworkConnectionErrorView();
        showLoading();
        forceLoaded = false;
        presenter.loadPaymentMethods(organizationUUID);
    }

    @Override
    protected void onOffline() {
        setSwipeRefreshLayoutEnabled(false);

        supplementaryView.selectCard.setVisibility(View.GONE);
        showNetworkConnectionErrorView();
    }

    private void setSwipeRefreshLayoutListener() {
        swipeRefreshLayoutPaymentMethods.setOnRefreshListener(() -> {
            forceLoaded = true;
            presenter.loadPaymentMethods(organizationUUID);
        });
    }

    @Override
    public void showConfirmDeleting() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
        materialDialog = new MaterialDialog.Builder(getContext())
                .progress(true, 0)
                .widgetColor(confHelper.getBackgroundThemeAccentColor())
                .content(R.string.payment_popup_deleting_payment_method)
                .show();
    }

    @Override
    public void hideConfirmDeleting() {
        materialDialog.dismiss();
    }

    @Override
    public void showDeletingPaymentMethodsErrorDialog() {
        errorDeletingMaterialDialog = new MaterialDialog.Builder(getContext())
                .title(R.string.payment_popup_something_happened)
                .content(R.string.payment_popup_could_not_delete)
                .positiveText(R.string.popup_retry)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .negativeText(R.string.popup_cancel)
                .onPositive((dialog, which) -> {
                    supplementaryView.removeCard.setVisibility(View.GONE);
                    supplementaryView.addedNewCardRemoveView.setVisibility(View.GONE);
                    supplementaryView.addedNewCardSelectView.setVisibility(View.GONE);
                    try {
                        showConfirmDeleting();
                        presenter.deletePaymentMethod(paymentsCardsAdapter.getSelectedCard(cardStackLayout));
                    } catch (Exception e) {
                        e.printStackTrace();
                        hideConfirmDeleting();
                        setSwipeRefreshLayoutRefreshing(false);
                        showDeletingPaymentMethodsErrorDialog();
                    }
                })
                .onNegative((dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void setSwipeRefreshLayoutEnabled(boolean enabled) {
        swipeRefreshLayoutPaymentMethods.setEnabled(enabled);
    }

    @Override
    public void setSwipeRefreshLayoutRefreshing(boolean refreshing) {
        swipeRefreshLayoutPaymentMethods.setRefreshing(refreshing);
    }

    @Override
    public void showLoadingPaymentMethodsErrorDialog() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.payment_popup_something_happened)
                .content(R.string.payment_popup_could_not_load)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void showLoadingPaymentMethodsErrorDialog(String errors) {
        new MaterialDialog.Builder(getContext())
                .title(R.string.payment_popup_something_happened)
                .content(errors)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void showLoading() {
        if (linearLayoutNoPaymentMethods.getVisibility() == View.VISIBLE) {
            linearLayoutNoPaymentMethods.setVisibility(View.GONE);
        }
        loadingPaymentMethods.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingPaymentMethods.setVisibility(View.GONE);
    }

    @Override
    public void setCards(ArrayList<Card> cardArrayList) {
        loadingPaymentMethods.setVisibility(View.GONE);
        linearLayoutNoPaymentMethods.setVisibility(View.GONE);

        cardScrollView.setVisibility(View.VISIBLE);

        paymentsCardsAdapter.clear();
        paymentsCardsAdapter.addFirstList(cardArrayList);

        if (isIntentFromAddCard && !isAddedNewCardViewShown)
            paymentsCardsAdapter.setSelection(cardArrayList.size() - 1);

        if (errorDeletingMaterialDialog != null && errorDeletingMaterialDialog.isShowing())
            errorDeletingMaterialDialog.dismiss();

//        analyticsPlatformAdapter.cartPaymentsLoadingComplete(forceLoaded, cardArrayList.size(), LIST_PAYMENT_METHODS);
        forceLoaded = false;
    }

    @Override
    public void showDeviceLostOrStolenError() {
        floatingActionButton.setVisibility(View.GONE);
        linearLayoutDeviceLostOrStolen.setVisibility(View.VISIBLE);
    }

    @Override
    public Context getActivityContext() {
        return getActivity();
    }

    @Override
    public void showDeviceTimeError() {
        floatingActionButton.setVisibility(View.GONE);
        linearLayoutDeviceTimeError.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSessionExpiredError(String message) {
        floatingActionButton.setVisibility(View.GONE);
        new MaterialDialog.Builder(getContext())
                .title(R.string.popup_error)
                .content(message)
                .cancelable(false)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    getActivity().finish();
                })
                .show();
    }

    @Override
    public void showDefaultError(String message) {
        showDefaultErrorDialog(message);
    }

    @Override
    public void showNoPaymentMethodsView() {
        loadingPaymentMethods.setVisibility(View.GONE);
        cardScrollView.setVisibility(View.GONE);
        linearLayoutNoPaymentMethods.setVisibility(View.VISIBLE);

        linearLayoutNoPaymentMethodText.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
        linearLayoutNoPaymentMethodText.announceForAccessibility(getString(R.string.payment_you_have_no_pay_methods));
    }

    @Override
    public void showNetworkConnectionErrorView() {
        linearLayoutNoPaymentMethods.setVisibility(View.GONE);
        supplementaryView.removeCard.setVisibility(View.GONE);
        supplementaryView.addedNewCardRemoveView.setVisibility(View.GONE);
        supplementaryView.addedNewCardSelectView.setVisibility(View.GONE);
        loadingPaymentMethods.setVisibility(View.GONE);
        cardScrollView.setVisibility(View.GONE);
        linearLayoutNetworkConnectivityErrorView.setVisibility(View.VISIBLE);

        linearLayoutNoNetworkText.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
        linearLayoutNoNetworkText.announceForAccessibility(getString(R.string.network_connectivity_error));
    }

    @Override
    public void hideNetworkConnectionErrorView() {
        linearLayoutNetworkConnectivityErrorView.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_retry)
    public void retry() {
        refreshNetworkActivity();
    }

    @OnClick({R.id.btn_add_card_no_payment_view, R.id.btn_add_card_loading_view, R.id.fab})
    public void addCard() {
        Intent intent = new Intent(getActivity(), AddPaymentCardActivity.class);
        intent.putExtra(AppConstants.SHOPPING_CART_INTENT, getActivity().getIntent().getBooleanExtra(AppConstants.SHOPPING_CART_INTENT, false));
        startActivityForResult(intent, AppConstants.ADD_CARD_REQUEST_CODE);
//        analyticsPlatformAdapter.cartAddPaymentsMethod(LIST_PAYMENT_METHODS);
    }

    @Override
    public void requestRestart() {
        showLoading();
        forceLoaded = false;
        presenter.loadPaymentMethods(organizationUUID);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.ADD_CARD_REQUEST_CODE) {
            isIntentFromAddCard = resultCode == Activity.RESULT_OK;

            cardScrollView.setVisibility(View.VISIBLE);
            refreshNetworkActivity();
        }
    }

    @Override
    public void onStackItemSelected(@Nullable Card card, @Section int section, int position) {
        if (!isAdded()) {
            Timber.i("Ignored selection since fragment detached");
            return;
        }
        setSwipeRefreshLayoutEnabled(false);

        hideSupplementaryViews();

        if (isIntentFromAddCard && !isAddedNewCardViewShown) {
            if (getActivity().getIntent().getBooleanExtra(AppConstants.SHOPPING_CART_INTENT, false)) {
                supplementaryView.addedNewCardSelectView.setVisibility(View.VISIBLE);
            } else {
                supplementaryView.addedNewCardRemoveView.setVisibility(View.VISIBLE);

            }
            isAddedNewCardViewShown = true;
        } else {
            if (getActivity().getIntent().getBooleanExtra(AppConstants.SHOPPING_CART_INTENT, false)) {
                supplementaryView.selectCard.setVisibility(View.VISIBLE);
            } else {
                supplementaryView.removeCard.setVisibility(View.VISIBLE);
            }
        }

        if (card != null) {
            //analyticsPlatformAdapter.listPaymentMethodSelected(CARD, card.getTypeName());

            supplementaryView.buttonSelectCard.setContentDescription(getContentDescriptionForSelectButton(card));
            supplementaryView.buttonSelectCardWhenAdded.setContentDescription(getContentDescriptionForSelectButton(card));

            supplementaryView.textViewSelectCard.setContentDescription(getContentDescriptionForSelectText(card));
            supplementaryView.textViewAddCardSuccess.setContentDescription(getContentDescriptionForAddCardSuccess(card));

            supplementaryView.buttonRemoveCard.setContentDescription(getContentDescriptionForRemoveButton(card));
            supplementaryView.removeCardText.setContentDescription(getContentDescriptionForRemoveText(card));
        }
    }

    @Override
    public void onStackItemUnSelected(@Nullable Card item, @Section int section, int position) {
        if (!isAdded()) {
            Timber.i("Ignored unselection since fragment detached");
        }
    }

    private void hideSupplementaryViews() {
        supplementaryView.removeCard.setVisibility(View.GONE);
        supplementaryView.selectCard.setVisibility(View.GONE);
        supplementaryView.addedNewCardRemoveView.setVisibility(View.GONE);
        supplementaryView.addedNewCardSelectView.setVisibility(View.GONE);
    }

    @Override
    public void registerCardDeletionCompletedAnalytics(String typeName) {
//        analyticsPlatformAdapter.listPaymentMethodDeletionCompleted(CARD, typeName);
    }

    /**
     * Class holding all view references that comes inside supplementary view.
     * Add, remove, select etc.
     * Since it is dynamically inflated using {@link StackViewLayout}, we can't make them fields to
     * this class. So holder pattern is used.
     */
    public class SupplementaryView {

        @BindView(R.id.ll_remove_card)
        LinearLayout removeCard;
        @BindView(R.id.ll_select_card)
        LinearLayout selectCard;
        @BindView(R.id.ll_added_a_new_card_remove_view)
        LinearLayout addedNewCardRemoveView;
        @BindView(R.id.ll_added_a_new_card_select_view)
        LinearLayout addedNewCardSelectView;
        @BindView(R.id.btn_remove_card)
        Button buttonRemoveCard;
        @BindView(R.id.btn_select_card)
        Button buttonSelectCard;
        @BindView(R.id.select_card_whn_added)
        Button buttonSelectCardWhenAdded;
        @BindView(R.id.btn_remove_card_when_added)
        Button buttonRemoveCardWhenAdded;
        @BindView(R.id.tv_add_card_success)
        TextView textViewAddCardSuccess;
        @BindView(R.id.tv_add_card_success_select)
        TextView tvAddCardSuccessSelect;
        @BindView(R.id.tv_remove_card)
        TextView removeCardText;
        @BindView(R.id.tv_select_card)
        TextView textViewSelectCard;

        SupplementaryView(View view) {
            ButterKnife.bind(this, view);
        }

        @OnClick({R.id.btn_remove_card, R.id.btn_remove_card_when_added})
        void removeCard() {
            final Card card = paymentsCardsAdapter.getSelectedCard(cardStackLayout);
            if (card != null)
                new MaterialDialog.Builder(getContext())
                        .title(R.string.payment_popup_remove_payment_method)
                        .content(R.string.payment_popup_remove_payment_method_message)
                        .positiveText(R.string.shopping_cart_remove)
                        .positiveColor(confHelper.getDataThemeAccentColor())
                        .negativeText(R.string.popup_cancel)
                        .onPositive((dialog, which) -> cardStackLayout.requestResetStack(() -> {
                            supplementaryView.removeCard.setVisibility(View.GONE);
                            supplementaryView.addedNewCardRemoveView.setVisibility(View.GONE);
                            supplementaryView.addedNewCardSelectView.setVisibility(View.GONE);
                            try {
                                showConfirmDeleting();
                                presenter.deletePaymentMethod(card);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }))
                        .onNegative((dialog, which) -> dialog.dismiss())
                        .show();
        }

        @OnClick({R.id.btn_select_card, R.id.select_card_whn_added})
        void setButtonSelectCard() {
            final Card card = paymentsCardsAdapter.getSelectedCard(cardStackLayout);
            if (card != null) {
//                analyticsPlatformAdapter.listPaymentMethodSelected(CARD, card.getTypeName());
                Intent intent = new Intent();
                intent.putExtra(AppConstants.SELECTED_CARD, card);
                getActivity().setResult(AppConstants.PAYMENT_METHODS_REQUEST_CODE, intent);
                getActivity().finish();
            }
        }

    }
}
