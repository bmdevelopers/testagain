package co.bytemark.payment_methods

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.R
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.Helpers.PaymentMethod
import co.bytemark.sdk.model.payment_methods.DotPay
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.synthetic.main.dot_pay_item.view.*
import java.util.*

class DotPayAdapterDelegate(private val confHelper: ConfHelper, private val callback: PaymentsAdapter.Callback)
    : AdapterDelegate<MutableList<PaymentMethod>>() {

    private val selectedPaymentMethods = ArrayList<PaymentMethod>()

    override fun isForViewType(items: MutableList<PaymentMethod>, position: Int): Boolean {
        return items[position] is DotPay
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return DotPayAdapterDelegate.DotPayViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.dot_pay_item, parent, false))
    }

    override fun onBindViewHolder(items: MutableList<PaymentMethod>, position: Int, holder: RecyclerView.ViewHolder, payloads: List<Any>) {
        with(holder as DotPayViewHolder) {
            itemView.linearLayoutForeground.setBackgroundColor(confHelper.dataThemeBackgroundColor)
            itemView.linearLayoutForeground.setOnClickListener {
                selectedPaymentMethods.add(items[position])
                callback.onSelectPaymentMethods(selectedPaymentMethods)
            }
        }
    }

    internal class DotPayViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}