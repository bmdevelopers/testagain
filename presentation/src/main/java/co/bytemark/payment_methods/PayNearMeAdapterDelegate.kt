package co.bytemark.payment_methods

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate

import java.util.ArrayList

import co.bytemark.R
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.Helpers.PaymentMethod
import co.bytemark.sdk.post_body.PayNearMe
import kotlinx.android.synthetic.main.pay_near_me_item.view.*

class PayNearMeAdapterDelegate(private val confHelper: ConfHelper, private val callback: PaymentsAdapter.Callback) : AdapterDelegate<MutableList<PaymentMethod>>() {

    private val selectedPaymentMethods = ArrayList<PaymentMethod>()

    override fun isForViewType(items: MutableList<PaymentMethod>, position: Int) = items[position] is PayNearMe

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
            PayNearMeViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.pay_near_me_item, parent, false))


    override fun onBindViewHolder(items: MutableList<PaymentMethod>, position: Int, holder: RecyclerView.ViewHolder, payloads: List<Any>) {
        (holder as PayNearMeViewHolder).apply {
            itemView.linearLayoutForeground.setBackgroundColor(confHelper.dataThemeBackgroundColor)
            itemView.setOnClickListener {
                selectedPaymentMethods.add(items[position])
                callback.onSelectPaymentMethods(selectedPaymentMethods)
            }
        }
    }

    internal class PayNearMeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}