package co.bytemark.payment_methods;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hannesdorfmann.adapterdelegates4.AdapterDelegate;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.Helpers.PaymentMethod;
import co.bytemark.sdk.post_body.Ideal;

/**
 * Created by Omkar on 12/4/17.
 */
public class IdealAdapterDelegate extends AdapterDelegate<List<PaymentMethod>> {

    private PaymentsAdapter.Callback callback;
    private ConfHelper confHelper;
    private ArrayList<PaymentMethod> selectedPaymentMethods = new ArrayList<>();

    IdealAdapterDelegate(ConfHelper confHelper, PaymentsAdapter.Callback callback) {
        this.confHelper = confHelper;
        this.callback = callback;
    }

    @Override
    protected boolean isForViewType(@NonNull List<PaymentMethod> items, int position) {
        return items.get(position) instanceof Ideal;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new IdealViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ideal_item, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull List<PaymentMethod> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
        IdealViewHolder idealViewHolder = (IdealViewHolder) holder;
        final Ideal ideal = (Ideal) items.get(position);
        idealViewHolder.linearLayoutForeground.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        idealViewHolder.linearLayoutForeground.setOnClickListener(view -> {
            selectedPaymentMethods.add(ideal);
            callback.onSelectPaymentMethods(selectedPaymentMethods);
        });
    }

    static class IdealViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.linearLayoutForeground)
        LinearLayout linearLayoutForeground;
        @BindView(R.id.imageViewCard)
        ImageView imageViewCard;
        @BindView(R.id.textViewLastFour)
        TextView textViewLastFour;

        IdealViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}