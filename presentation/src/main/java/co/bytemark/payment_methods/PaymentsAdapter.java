package co.bytemark.payment_methods;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.ViewGroup;

import com.hannesdorfmann.adapterdelegates4.AdapterDelegatesManager;

import java.util.ArrayList;
import java.util.List;

import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.Helpers.PaymentMethod;
import co.bytemark.sdk.model.payment_methods.BraintreePaypalPaymentMethod;
import co.bytemark.sdk.model.payment_methods.Card;
import co.bytemark.sdk.model.payment_methods.GooglePay;
import co.bytemark.sdk.model.payment_methods.Wallet;
import co.bytemark.sdk.post_body.Ideal;

public class PaymentsAdapter extends RecyclerView.Adapter {

    private final List<PaymentMethod> paymentMethods = new ArrayList<>();
    private final AdapterDelegatesManager<List<PaymentMethod>> delegatesManager;
    private boolean isSplitPaymentSelected;

    PaymentsAdapter(ConfHelper confHelper, Callback callback, boolean isSplitPaymentEnabled,
                    Context context, ArrayList<PaymentMethod> selectedCards, boolean hideDeletePaymentOption) {
        setHasStableIds(true);
        this.isSplitPaymentSelected = isSplitPaymentEnabled;
        delegatesManager = new AdapterDelegatesManager<>();
        delegatesManager.addDelegate(new CardAdapterDelegate(confHelper, callback, context,
                isSplitPaymentSelected, selectedCards, hideDeletePaymentOption));
        delegatesManager.addDelegate(new PaypalAdapterDelegate(confHelper, callback));
        delegatesManager.addDelegate(new IdealAdapterDelegate(confHelper, callback));
        delegatesManager.addDelegate(new GooglePayAdapterDelegate(confHelper, callback));
        delegatesManager.addDelegate(new PayNearMeAdapterDelegate(confHelper, callback));
        delegatesManager.addDelegate(new DotPayAdapterDelegate(confHelper, callback));
        delegatesManager.addDelegate(new WalletAdapterDelegate(confHelper, callback));
        delegatesManager.addDelegate(new IncommDelegate(confHelper, callback));
    }

    @Override
    public int getItemViewType(int position) {
        return delegatesManager.getItemViewType(paymentMethods, position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return delegatesManager.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        delegatesManager.onBindViewHolder(paymentMethods, position, holder);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List payloads) {
        delegatesManager.onBindViewHolder(paymentMethods, position, holder, payloads);
    }

    @Override
    public long getItemId(int position) {
        return paymentMethods.get(position).hashCode();
    }

    @Override
    public int getItemCount() {
        return paymentMethods.size();
    }

    void setPaymentMethods(List<PaymentMethod> paymentMethods) {
        this.paymentMethods.clear();
        this.paymentMethods.addAll(paymentMethods);
        this.notifyDataSetChanged();
    }

    void removeItem(int position) {
        this.paymentMethods.remove(position);
        this.notifyItemRemoved(position);
    }

    void restoreItem(PaymentMethod paymentMethods, int position) {
        this.paymentMethods.add(position, paymentMethods);
        this.notifyItemInserted(position);
    }

    public interface Callback {
        void onSelectCard(Card card);

        void onDeleteCard(Card card);

        void onSelectPaypal(BraintreePaypalPaymentMethod paypal);

        void onSelectIdeal(Ideal ideal);

        void onDeletePaypal(BraintreePaypalPaymentMethod paypal);

        void onSelectGooglePay(GooglePay googlePay);

        void onSelectPaymentMethods(ArrayList<PaymentMethod> paymentMethods);

        void onClickLoadMoney(Wallet wallet);

    }
}