package co.bytemark.payment_methods;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hannesdorfmann.adapterdelegates4.AdapterDelegate;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.Helpers.PaymentMethod;
import co.bytemark.sdk.model.payment_methods.GooglePay;

/**
 * Created by Santosh on 09/05/18.
 */
class GooglePayAdapterDelegate extends AdapterDelegate<List<PaymentMethod>> {

    private ConfHelper confHelper;
    private PaymentsAdapter.Callback callback;
    private ArrayList<PaymentMethod> selectedPaymentMethods = new ArrayList<>();

    public GooglePayAdapterDelegate(ConfHelper confHelper, PaymentsAdapter.Callback callback) {
        this.confHelper = confHelper;
        this.callback = callback;
    }

    @Override
    protected boolean isForViewType(@NonNull List<PaymentMethod> items, int position) {
        return items.get(position) instanceof GooglePay;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new GooglePayViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.google_pay_item, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull List<PaymentMethod> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
        GooglePayViewHolder googlePayViewHolder = (GooglePayViewHolder) holder;
        final GooglePay googlePay = (GooglePay) items.get(position);
        googlePayViewHolder.linearLayoutForeground.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        googlePayViewHolder.linearLayoutForeground.setOnClickListener(view -> {
            selectedPaymentMethods.add(googlePay);
            callback.onSelectPaymentMethods(selectedPaymentMethods);
        });
    }

    static class GooglePayViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.linearLayoutForeground)
        LinearLayout linearLayoutForeground;
        @BindView(R.id.imageViewCard)
        ImageView imageViewCard;
        @BindView(R.id.textViewLastFour)
        TextView textViewLastFour;

        GooglePayViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}