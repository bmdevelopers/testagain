package co.bytemark.payment_methods.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import co.bytemark.R;
import co.bytemark.helpers.glide.CardTypeImage;
import co.bytemark.sdk.model.payment_methods.Card;
import co.bytemark.widgets.stackview.StackAdapter;
import co.bytemark.widgets.stackview.StackViewLayout;
import timber.log.Timber;

/**
 * Created by Arunkumar on 10/11/16.
 */
public class PaymentsCardsAdapter extends StackAdapter<Card, CardHolder> {
    public PaymentsCardsAdapter(Context context) {
        super(context);
    }

    @Override
    protected void bindFirstSectionView(Card card, int position, CardHolder holder) {
        holder.cardLabel.setText(card.getNickname());

        if (!card.getNickname().isEmpty())
            holder.cardLabel.setContentDescription(card.getNickname());

        String cardNumber;
        if (card.getTypeName().equals("American Express"))
            cardNumber = String.format("*** *%s", card.getLastFour());
        else
            cardNumber = String.format("**** %s", card.getLastFour());

        holder.cardNumber.setText(cardNumber);
        holder.cardNumber.setContentDescription(card.getTypeName() + getContext().getString(R.string.payment_card_ending_with_voonly) + card.getLastFour());

        String cardExpiration = String.format(getContext().getString(R.string.shopping_cart_exp), card.getExpirationDate());

        holder.cardExpiration.setText(cardExpiration);
        holder.cardExpiration.setContentDescription(getContext().getString(R.string.payment_card_expiration_voonly) + card.getExpirationDate());

        Glide.with(holder.cardType.getContext())
                .load(new CardTypeImage(card.getTypeName()))
                .crossFade()
                .skipMemoryCache(false)
                .into(holder.cardType);
    }

    @Override
    protected void bindSecondSectionView(Card data, int position, CardHolder holder) {
        // no op
    }

    @Override
    protected void bindThirdSectionView(Card data, int position, CardHolder holder) {
        // no op
    }

    @Override
    protected void bindFourthSectionView(Card data, int position, CardHolder holder) {

    }

    @Override
    public void addThirdList(@NonNull List<Card> thirdList) {
        // no op
        Timber.e("No items to be added in the bottom section");
    }

    @Override
    public void addSecondList(@NonNull List<Card> secondList) {
        Timber.e("No items to be added in the middle section");
    }

    @Override
    protected CardHolder onCreateViewHolder(int section, ViewGroup parent) {
        final View view = LayoutInflater.from(getContext()).inflate(R.layout.card_view, parent, false);
        return new CardHolder(view);
    }

    // TODO Make selection/un selection a native method in adapter and remove dependency of providing a stack view object

    /**
     * Helper to easily return the selected card. You must provide the stack view for which the adapter
     * is providing for else things will break.
     *
     * @param stackViewLayout StackView for which the adapter is attached to.
     * @return selected card
     */
    public Card getSelectedCard(@NonNull StackViewLayout stackViewLayout) {
        return getItemAt(stackViewLayout.getSelectedSection(), stackViewLayout.getSelectedItemIndex());
    }

    public void setSelection(final int index) {
        setSelection(StackViewLayout.FIRST, index);
    }
}
