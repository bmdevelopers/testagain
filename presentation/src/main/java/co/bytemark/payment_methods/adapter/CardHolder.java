package co.bytemark.payment_methods.adapter;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;
import co.bytemark.widgets.stackview.ViewHolder;

/**
 * Created by Arunkumar on 10/11/16.
 */
public class CardHolder extends ViewHolder {
    @BindView(R.id.tv_card_label)
    TextView cardLabel;
    @BindView(R.id.tv_card_number)
    TextView cardNumber;
    @BindView(R.id.tv_card_expiration)
    TextView cardExpiration;
    @BindView(R.id.iv_card_type)
    ImageView cardType;
    @BindView(R.id.cv_payment_card)
    CardView cardViewPaymentCard;

    CardHolder(@NonNull View rootView) {
        super(rootView);
        ButterKnife.bind(this, rootView);
    }

    @Override
    public void onPartiallyVisible() {
        // Accessibility
    }

    @Override
    public void onFullyVisible() {
        // Accessibility
    }
}
