package co.bytemark.payment_methods;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.graphics.ColorUtils;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.bytemark.R;
import co.bytemark.add_payment_card.PaymentCard;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.Helpers.PaymentMethod;
import co.bytemark.sdk.model.payment_methods.Card;

/**
 * Created by Omkar on 12/1/17.
 */
public class CardAdapterDelegate extends AdapterDelegate<List<PaymentMethod>> {

    private ConfHelper confHelper;
    private PaymentsAdapter.Callback callback;
    private Context context;
    private boolean isSplitPaymentSelected;
    private ArrayList<PaymentMethod> selectedCards;
    private boolean hideDeletePaymentOption;

    CardAdapterDelegate(ConfHelper confHelper, PaymentsAdapter.Callback callback, Context context,
                        boolean isSplitPaymentSelected, ArrayList<PaymentMethod> selectedCards, boolean hideDeletePaymentOption) {
        this.confHelper = confHelper;
        this.callback = callback;
        this.context = context;
        this.isSplitPaymentSelected = isSplitPaymentSelected;
        this.selectedCards = selectedCards;
        this.hideDeletePaymentOption = hideDeletePaymentOption;
    }

    @Override
    protected boolean isForViewType(@NonNull List<PaymentMethod> items, int position) {
        return items.get(position) instanceof Card;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new CardViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_item, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull List<PaymentMethod> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
        CardViewHolder cardViewHolder = (CardViewHolder) holder;
        final Card card = (Card) items.get(position);

        cardViewHolder.textViewLastFour.setText(String.format("*%1$s %2$s", card.getLastFour(), card.getNickname()));
        cardViewHolder.textViewLastFour.setContentDescription(
                String.format(cardViewHolder.textViewLastFour.getContext().getString(R.string.payment_ending_with),
                        card.getTypeName(), card.getLastFour(), card.getNickname())
        );
        cardViewHolder.textViewExpiration.setText(
                String.format(cardViewHolder.textViewExpiration.getContext().getString(R.string.payment_expiration_date),
                        card.getExpirationDate())
        );

        PaymentCard paymentCard = PaymentCard.Companion.fromPaymentCardType(card.getTypeName());
        Glide.with(cardViewHolder.imageViewCard.getContext())
                .load(paymentCard.getTypeImage())
                .crossFade()
                .into(cardViewHolder.imageViewCard);

        if (confHelper.isV2PaymentMethodsEnabled() && !card.getAccepted()) {
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.setSaturation(0);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(colorMatrix);
            ((CardViewHolder) holder).imageViewCard.setColorFilter(filter);
        }

        if (selectedCards.contains(card)) {
            cardViewHolder.linearLayout.setBackground(getGradientDrawableForBackground());
        } else {
            cardViewHolder.linearLayout.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        }

        cardViewHolder.linearLayout.setOnClickListener(view -> {
            if (confHelper.isV2PaymentMethodsEnabled() && !card.getAccepted()) {
                Snackbar.make(view, R.string.payment_payment_type_not_accepted, Snackbar.LENGTH_LONG).show();
                return;
            }

            if (!isSplitPaymentSelected) {
                selectedCards.add(card);
                callback.onSelectPaymentMethods(selectedCards);
                return;
            }

            if (selectedCards.contains(card)) {
                selectedCards.remove(card);
                cardViewHolder.linearLayout.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
            } else {
                if (selectedCards.size() < confHelper.getMaxSplitPayments()) {
                    cardViewHolder.linearLayout.setBackground(getGradientDrawableForBackground());
                    selectedCards.add(card);
                    if (selectedCards.size() == confHelper.getMaxSplitPayments()) {
                        callback.onSelectPaymentMethods(selectedCards);
                    }
                } else if (selectedCards.size() == confHelper.getMaxSplitPayments()) {
                    Snackbar.make(cardViewHolder.linearLayout, String.format(context.getString(R.string.payment_split_payment_max_select_msg), confHelper.getMaxSplitPayments()), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        if (hideDeletePaymentOption) {
            cardViewHolder.imageViewDelete.setVisibility(View.GONE);
        } else {
            cardViewHolder.imageViewDelete.setVisibility(View.VISIBLE);
        }

        cardViewHolder.imageViewDelete.setOnClickListener(view -> {
            callback.onDeleteCard(card);
            selectedCards.remove(card);
            cardViewHolder.linearLayout.setBackgroundColor(confHelper.getDataThemeBackgroundColor());
        });
    }

    private GradientDrawable getGradientDrawableForBackground() {
        GradientDrawable gradientDrawable = (GradientDrawable) context.getResources().getDrawable(R.drawable.card_selected_background);
        gradientDrawable.setStroke(4, confHelper.getCollectionThemeAccentColor());
        gradientDrawable.setColor(ColorUtils.setAlphaComponent(confHelper.getCollectionThemeAccentColor(), 20));
        return gradientDrawable;
    }

    static class CardViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewCard)
        ImageView imageViewCard;
        @BindView(R.id.textViewLastFour)
        TextView textViewLastFour;
        @BindView(R.id.textViewExpiration)
        TextView textViewExpiration;
        @BindView(R.id.linearLayout)
        LinearLayout linearLayout;
        @BindView(R.id.imageViewDelete)
        ImageView imageViewDelete;

        CardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}