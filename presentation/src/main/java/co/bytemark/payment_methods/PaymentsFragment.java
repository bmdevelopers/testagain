package co.bytemark.payment_methods;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.addPaymentMethods.AddPaymentMethodsActivity;
import co.bytemark.add_payment_card.AddPaymentCardActivity;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.analytics.AnalyticsPlatformsContract;
import co.bytemark.base.BaseMvpFragment;
import co.bytemark.helpers.AppConstants;
import co.bytemark.sdk.Helpers.PaymentMethod;
import co.bytemark.sdk.model.common.Data;
import co.bytemark.sdk.model.payment_methods.BraintreePaypalPaymentMethod;
import co.bytemark.sdk.model.payment_methods.Card;
import co.bytemark.sdk.model.payment_methods.GooglePay;
import co.bytemark.sdk.model.payment_methods.Wallet;
import co.bytemark.sdk.post_body.Ideal;
import co.bytemark.widgets.LinearRecyclerView;
import co.bytemark.widgets.emptystateview.EmptyStateLayout;

public class PaymentsFragment extends BaseMvpFragment<Payments.View, Payments.Presenter>
        implements Payments.View, PaymentsAdapter.Callback {

    public static final int ACTION_LOAD_PAYMENT_METHOD = 1;
    public static final int ACTION_DELETE_PAYMENT_METHOD = 2;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerViewPayments)
    LinearRecyclerView recyclerView;
    @BindView(R.id.emptyStateLayout)
    EmptyStateLayout emptyStateLayout;
    @BindView(R.id.addPaymentMethodButton)
    Button addPaymentMethodButton;
    @BindView(R.id.tv_info_text)
    TextView textViewInfo;

    @Inject
    Payments.Presenter presenter;
    @Inject
    AnalyticsPlatformAdapter analyticsPlatformAdapter;

    private boolean googlePayReady;
    private boolean isCardSelectionEnabled;
    private boolean isSplitPaymentSelected;
    private boolean reloadWalletIntent;
    private boolean autoloadWalletIntent;
    private boolean hideAddPaymentOption;
    private boolean hideDeletePaymentOption;
    private PaymentsAdapter paymentsAdapter;
    private List<PaymentMethod> paymentMethods = new ArrayList<>();
    private ArrayList<PaymentMethod> selectedPaymentMethods = new ArrayList<>();
    private ArrayList<PaymentMethod> deletedPaymentMethods = new ArrayList<>();
    private String organizationUUID;
    private boolean isAddPaymentMethodScreenPresent;
    private ArrayList<String> acceptedPaymentMethods;

    private int action = ACTION_LOAD_PAYMENT_METHOD;
    private boolean forceReload = false;
    private String deletedPaymentType = null;

    @NonNull
    @Override
    public Payments.Presenter createPresenter() {
        return presenter;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_payments;
    }

    @Override
    protected void onInject() {
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        organizationUUID = getActivity().getIntent().getStringExtra(AppConstants.ORG_UUID);

        presenter.createGoolglePayClient(getActivity());
        presenter.isGoolePayReady();

        isCardSelectionEnabled = getActivity().getIntent()
                .getBooleanExtra(AppConstants.SHOPPING_CART_INTENT, false);
        isSplitPaymentSelected = getActivity().getIntent()
                .getBooleanExtra(AppConstants.SPLIT_PAYMENT, false);
        reloadWalletIntent = getActivity().getIntent().
                getBooleanExtra(AppConstants.RELOADING_WALLET, false);
        autoloadWalletIntent = getActivity().getIntent().
                getBooleanExtra(AppConstants.AUTOLOAD, false);
        hideAddPaymentOption = getActivity().getIntent().
                getBooleanExtra(AppConstants.HIDE_ADD_PAYMENT, false);
        hideDeletePaymentOption = getActivity().getIntent().
                getBooleanExtra(AppConstants.HIDE_DELETE_PAYMENT, false);

        if (getActivity().getIntent().getParcelableArrayListExtra(AppConstants.SELECTED_CARD) != null)
            selectedPaymentMethods = getActivity().getIntent()
                    .getParcelableArrayListExtra(AppConstants.SELECTED_CARD);

        paymentsAdapter = new PaymentsAdapter(confHelper, this, isSplitPaymentSelected,
                getActivity().getApplicationContext(), selectedPaymentMethods, hideDeletePaymentOption);
        recyclerView.setAdapter(paymentsAdapter);
        showLoading();

        if (isSplitPaymentSelected) {
            textViewInfo.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setSwipeRefreshLayoutListener();
    }

    private void setSwipeRefreshLayoutListener() {
        swipeRefreshLayout.setOnRefreshListener(() -> {
            action = ACTION_LOAD_PAYMENT_METHOD;
            forceReload = true;
            presenter.loadPayments(organizationUUID);
        });
    }

    @Override
    protected void onOnline() {
        showLoading();
        action = ACTION_LOAD_PAYMENT_METHOD;
        presenter.loadPayments(organizationUUID);
        presenter.loadAcceptedPaymentMethod(organizationUUID);
        addPaymentMethodButton.setVisibility(View.GONE);
    }

    private void showLoading() {
        emptyStateLayout.showLoading(R.drawable.ic_payment_card_filled, R.string.payment_loading_payment_methods);
    }

    @Override
    protected void onOffline() {
        emptyStateLayout.showError(R.drawable.error_material, R.string.network_connectivity_error, R.string.network_connectivity_error_message);
    }

    @Override
    protected void setAccentThemeColors() {

    }

    @Override
    protected void setBackgroundThemeColors() {

    }

    @Override
    protected void setDataThemeColors() {

    }

    @Override
    public void setCards(Data data) {
        swipeRefreshLayout.setRefreshing(false);
        if (autoloadWalletIntent) {
            if (data.cards.isEmpty() && data.paypalList.isEmpty()) {
                emptyStateLayout.showEmpty(R.drawable.ic_payment_card_filled, R.string.payment_you_have_no_pay_methods, R.string.payment_no_pay_method_desc);
                analyticsPlatformAdapter.paymentMethodsLoaded(0, forceReload ? 1 : 0, AnalyticsPlatformsContract.Status.SUCCESS, getString(R.string.payment_you_have_no_pay_methods));
            } else {
                emptyStateLayout.showContent();
                paymentMethods.clear();
                paymentMethods.addAll(data.cards);
                paymentMethods.addAll(data.paypalList);
                Collections.sort(paymentMethods, new PaymentMethodOrderComparator(googlePayReady));
                paymentsAdapter.setPaymentMethods(paymentMethods);
                if (hideAddPaymentOption) {
                    addPaymentMethodButton.setVisibility(View.GONE);
                } else {
                    addPaymentMethodButton.setVisibility(View.VISIBLE);
                }
                analyticsPlatformAdapter.paymentMethodsLoaded(paymentMethods.size(), forceReload ? 1 : 0, AnalyticsPlatformsContract.Status.SUCCESS, "");
            }
            return;
        }

        if ((data.cards.isEmpty()
                && (data.paypalList == null || data.paypalList.isEmpty())
                && data.ideal == null
                && data.googlePay == null
                && data.payNearMe == null
                && data.dotPay == null
				&& data.incomm == null
                && data.storedWallets == null
                && !isSplitPaymentSelected)) {
            emptyStateLayout.showEmpty(R.drawable.ic_payment_card_filled, R.string.payment_you_have_no_pay_methods, R.string.payment_no_pay_method_desc);
            analyticsPlatformAdapter.paymentMethodsLoaded(0, forceReload ? 1 : 0, AnalyticsPlatformsContract.Status.SUCCESS, getString(R.string.payment_you_have_no_pay_methods));
        } else if (data.cards.isEmpty() && isSplitPaymentSelected) {
            emptyStateLayout.showEmpty(R.drawable.ic_payment_card_filled, R.string.payment_you_have_no_pay_methods, R.string.payment_no_pay_method_desc);
            analyticsPlatformAdapter.paymentMethodsLoaded(0, forceReload ? 1 : 0, AnalyticsPlatformsContract.Status.SUCCESS, getString(R.string.payment_you_have_no_pay_methods));
        } else {
            emptyStateLayout.showContent();
            paymentMethods.clear();
            if (data.cards != null && !data.cards.isEmpty()) {
                paymentMethods.addAll(data.cards);
            }

            if (!isSplitPaymentSelected && data.paypalList != null && !data.paypalList.isEmpty()) {
                paymentMethods.addAll(data.paypalList);
            }

            if (!isSplitPaymentSelected && data.ideal != null) {
                paymentMethods.add(data.ideal);
            }

            if (!isSplitPaymentSelected && data.payNearMe != null) {
                paymentMethods.add(data.payNearMe);
            }
            if (!isSplitPaymentSelected && data.incomm != null) {
                paymentMethods.add(data.incomm);
            }
            if (!isSplitPaymentSelected && data.googlePay != null) {
                paymentMethods.add(data.googlePay);
            }
            if (!isSplitPaymentSelected && data.dotPay != null) {
                paymentMethods.add(data.dotPay);
            }
            if (!isSplitPaymentSelected && !reloadWalletIntent && data.storedWallets != null
                    && data.storedWallets.getWallets().size() > 0) {
                paymentMethods.addAll(data.storedWallets.getWallets());
            }

            Collections.sort(paymentMethods, new PaymentMethodOrderComparator(googlePayReady));
            analyticsPlatformAdapter.paymentMethodsLoaded(paymentMethods.size(), forceReload ? 1 : 0, AnalyticsPlatformsContract.Status.SUCCESS, "");
            paymentsAdapter.setPaymentMethods(paymentMethods);
        }
        if (forceReload)
            forceReload = false;
    }

    private void logFailureAnalytics(String errorMessage) {
        if (action == ACTION_LOAD_PAYMENT_METHOD) {
            analyticsPlatformAdapter.paymentMethodsLoaded(0, forceReload ? 1 : 0, AnalyticsPlatformsContract.Status.FAILURE, errorMessage);
        } else {
            analyticsPlatformAdapter.paymentMethodRemoved(deletedPaymentType, AnalyticsPlatformsContract.Status.FAILURE, errorMessage);
        }

    }

    @Override
    public void showLoadingPaymentMethodsErrorDialog() {
        swipeRefreshLayout.setRefreshing(false);
        logFailureAnalytics("");
        new MaterialDialog.Builder(getContext())
                .title(R.string.payment_popup_something_happened)
                .content(R.string.payment_popup_could_not_load)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void showDeviceLostOrStolenError() {
        logFailureAnalytics(getString(R.string.device_lost_stolen_title));
        addPaymentMethodButton.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        emptyStateLayout.showError(R.drawable.error_material, R.string.device_lost_stolen_title, R.string.device_lost_stolen_body);
    }

    @Override
    public void showSessionExpiredError(String message) {
        addPaymentMethodButton.setVisibility(View.GONE);
        logFailureAnalytics("Session expired");
        if (appUpdateDialog == null && !appUpdateDialog.isShowing()) {
            new MaterialDialog.Builder(getContext())
                    .title(R.string.popup_error)
                    .content(message)
                    .cancelable(false)
                    .positiveText(R.string.ok)
                    .positiveColor(confHelper.getDataThemeAccentColor())
                    .onPositive((dialog, which) -> {
                        dialog.dismiss();
                        getActivity().finish();
                    })
                    .show();
        } else {
            emptyStateLayout.showError(R.drawable.error_material, R.string.popup_error, "");
        }
    }

    @Override
    public void closeSession() {
        getActivity().onBackPressed();
    }

    @Override
    public void showDeviceTimeError() {
        addPaymentMethodButton.setVisibility(View.GONE);
        emptyStateLayout.showError(R.drawable.error_material, R.string.device_time_error, R.string.device_time_error_message);
        logFailureAnalytics(getString(R.string.device_time_error));
    }

    @Override
    public void showLoadingPaymentMethodsErrorDialog(String message) {
        analyticsPlatformAdapter.paymentMethodsLoaded(0, 0, AnalyticsPlatformsContract.Status.FAILURE, message);
        new MaterialDialog.Builder(getContext())
                .title(R.string.payment_popup_something_happened)
                .content(message)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void showAppUpdateDialog() {
        emptyStateLayout.showError(R.drawable.error_material, R.string.popup_error, "");
        super.showAppUpdateDialog();
    }

    @Override
    public void setAcceptedPaymentMethods(ArrayList<String> acceptedPaymentMethods) {
        this.acceptedPaymentMethods = acceptedPaymentMethods;
        for (String paymentMethod : acceptedPaymentMethods) {
            if (reloadWalletIntent && paymentMethod.contains(AppConstants.PAYMENT_TYPE_PAYPAL)) {
                isAddPaymentMethodScreenPresent = true;
            } else if (paymentMethod.contains(AppConstants.PAYMENT_TYPE_PAYPAL)
					// TODO v3232 Uncomment this if add wallet functionality is required
					// || paymentMethod.contains(AppConstants.PAYMENT_TYPE_WALLET)
			) {
                isAddPaymentMethodScreenPresent = true;
            }
        }
    }

    @Override
    public void showFloatingActionButton() {
        if (!hideAddPaymentOption)
            addPaymentMethodButton.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.addPaymentMethodButton)
    public void addCard() {
        analyticsPlatformAdapter.addPaymentSelected(AnalyticsPlatformsContract.Screen.PAYMENT);
        if (isAddPaymentMethodScreenPresent && !isSplitPaymentSelected && !autoloadWalletIntent) {
            Intent intent = new Intent(getActivity(), AddPaymentMethodsActivity.class);
            if (reloadWalletIntent) {
                acceptedPaymentMethods.remove(AppConstants.PAYMENT_TYPE_WALLET);
            }
            intent.putStringArrayListExtra(AppConstants.ACCEPTED_PAYMENTS, acceptedPaymentMethods);
            startActivityForResult(intent, AppConstants.ADD_PAYMENT_METHOD_REQUEST_CODE);
        } else if (autoloadWalletIntent) {
            if (acceptedPaymentMethods.contains(AppConstants.PAYMENT_TYPE_PAYPAL)) {
                Intent intent = new Intent(getActivity(), AddPaymentMethodsActivity.class);
                acceptedPaymentMethods.remove(AppConstants.PAYMENT_TYPE_WALLET);
                acceptedPaymentMethods.remove(AppConstants.PAYMENT_TYPE_GOOGLE_PAY);
                intent.putStringArrayListExtra(AppConstants.ACCEPTED_PAYMENTS, acceptedPaymentMethods);
                startActivityForResult(intent, AppConstants.ADD_PAYMENT_METHOD_REQUEST_CODE);
            } else {
                addCardRequest();
            }
        } else {
            addCardRequest();
        }
    }

    private void addCardRequest() {
        Intent intent = new Intent(getActivity(), AddPaymentCardActivity.class);
        intent.putExtra(AppConstants.SHOPPING_CART_INTENT, getActivity().getIntent().getBooleanExtra(AppConstants.SHOPPING_CART_INTENT, false));
        startActivityForResult(intent, AppConstants.ADD_CARD_REQUEST_CODE);
    }

    @Override
    public void requestRestart() {
        showLoading();
        presenter.loadPayments(organizationUUID);
    }

    @Override
    public void logPaymentMethodRemovedAnalyticsEvent(String paymentType, String status, String errorMessage) {
        analyticsPlatformAdapter.paymentMethodRemoved(paymentType, status, errorMessage);
    }

    @Override
    public void showDeletingPaymentMethodsErrorDialog() {

    }

    @Override
    public void showDefaultError(String message) {
        swipeRefreshLayout.setRefreshing(false);
        showDefaultErrorDialog(message);
    }

    @Override
    public void setGooglePayReady(boolean googlePayReady) {
        this.googlePayReady = googlePayReady;
    }

    @Override
    public void onSelectCard(Card card) {
        if (isCardSelectionEnabled) {
            Intent intent = new Intent();
            intent.putExtra(AppConstants.SELECTED_CARD, card);
            getActivity().setResult(AppConstants.PAYMENT_METHODS_REQUEST_CODE, intent);
            getActivity().finish();
        }
    }


    @Override
    public void onDeleteCard(Card card) {
        String title = String.format(getContext().getString(R.string.payment_popup_remove_card_title), card.getTypeName(), card.getLastFour());
        String content = String.format(getContext().getString(R.string.payment_popup_remove_card_content), card.getTypeName(), card.getLastFour());
        new MaterialDialog.Builder(getContext())
                .title(title)
                .content(content)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    action = ACTION_DELETE_PAYMENT_METHOD;
                    presenter.deletePayments(card);
                    deletedPaymentMethods.add(card);
                    deletedPaymentType = AnalyticsPlatformsContract.PaymentType.CARD;
                    Intent intent = new Intent();
                    intent.putParcelableArrayListExtra(AppConstants.DELETED_PAYMENT, deletedPaymentMethods);
                    getActivity().setResult(AppConstants.PAYMENT_METHODS_REQUEST_CODE, intent);
                })
                .negativeText(R.string.popup_cancel)
                .onNegative((dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void onSelectPaypal(BraintreePaypalPaymentMethod paypal) {
        if (isCardSelectionEnabled) {
            Intent intent = new Intent();
            intent.putExtra(AppConstants.SELECTED_CARD, paypal);
            getActivity().setResult(AppConstants.PAYMENT_METHODS_REQUEST_CODE, intent);
            getActivity().finish();
        }
    }

    @Override
    public void onSelectIdeal(Ideal ideal) {
        if (isCardSelectionEnabled) {
            Intent intent = new Intent();
            intent.putExtra(AppConstants.SELECTED_CARD, ideal);
            getActivity().setResult(AppConstants.PAYMENT_METHODS_REQUEST_CODE, intent);
            getActivity().finish();
        }
    }

    @Override
    public void onDeletePaypal(BraintreePaypalPaymentMethod paypal) {
        String title = getContext().getString(R.string.payment_remove_paypal_title);
        String content = String.format(getContext().getString(R.string.payment_remove_paypal_content), paypal.getEmail());
        new MaterialDialog.Builder(getContext())
                .title(title)
                .content(content)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    presenter.deletePayPalAccount(paypal);
                    deletedPaymentMethods.add(paypal);
                    deletedPaymentType = AnalyticsPlatformsContract.PaymentType.PAYPAL;
                    Intent intent = new Intent();
                    intent.putParcelableArrayListExtra(AppConstants.DELETED_PAYMENT, deletedPaymentMethods);
                    getActivity().setResult(AppConstants.PAYMENT_METHODS_REQUEST_CODE, intent);
                })
                .negativeText(R.string.popup_cancel)
                .onNegative((dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void onSelectGooglePay(GooglePay googlePay) {
        if (isCardSelectionEnabled) {
            Intent intent = new Intent();
            intent.putExtra(AppConstants.SELECTED_CARD, googlePay);
            getActivity().setResult(AppConstants.PAYMENT_METHODS_REQUEST_CODE, intent);
            getActivity().finish();
        }
    }

    @Override
    public void onSelectPaymentMethods(ArrayList<PaymentMethod> paymentMethods) {
        if (isCardSelectionEnabled) {
            Intent intent = new Intent();
            intent.putParcelableArrayListExtra(AppConstants.SELECTED_CARD, paymentMethods);
            getActivity().setResult(AppConstants.PAYMENT_METHODS_REQUEST_CODE, intent);
            getActivity().finish();
        }
    }

    @Override
    public void onClickLoadMoney(Wallet wallet) {
        if (getActivity() != null) {
            if (wallet.isLoadingMoneyAllowed() != null && !wallet.isLoadingMoneyAllowed()) {
                showLoadingMoneyNotAllowedDialog();
                return;
            }
            Intent intent = new Intent(getActivity(), LoadMoneyWalletActivity.class);
            intent.putExtra(AppConstants.WALLET, wallet);
            startActivityForResult(intent, AppConstants.LOAD_MONEY_REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.ADD_CARD_REQUEST_CODE
                || requestCode == AppConstants.ADD_PAYMENT_METHOD_REQUEST_CODE
                || requestCode == AppConstants.LOAD_MONEY_REQUEST_CODE) {

            if (isOnline()) {
                requestRestart();
            }

            if (resultCode == AppConstants.ADD_CARD_SUCCESS_CODE && confHelper.isCreditCardAlertMessageEnabled()) {
                showAddCardSuccessDialog();
            }
        }
    }

    private void showAddCardSuccessDialog() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.payment_popup_new_card_added_title)
                .content(R.string.payment_popup_new_card_added_message)
                .positiveText(R.string.ticket_storage_popup_confirm)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();
    }


    private void showLoadingMoneyNotAllowedDialog() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.payment_load_money_not_allowed_dialog_title)
                .content(R.string.payment_load_money_not_allowed_dialog_body)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();
    }
}