package co.bytemark.add_wallet

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.sdk.model.common.Action

class AddWalletActivity : MasterActivity() {

    override fun getLayoutRes(): Int =
            R.layout.activity_add_wallet

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(R.string.screen_title_add_wallet)
        setNavigationViewCheckedItem(Action.SETTINGS)
    }

    override fun useHamburgerMenu() = false
}
