package co.bytemark.add_wallet

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.Result
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.AppConstants.SHOPPING_CART_INTENT
import co.bytemark.sdk.model.payment_methods.Wallet
import co.bytemark.widgets.util.*
import kotlinx.android.synthetic.main.adding_wallet_progress.*
import kotlinx.android.synthetic.main.app_bar_master.*
import kotlinx.android.synthetic.main.fragment_add_wallet.*
import javax.inject.Inject

class AddWalletFragment : BaseMvvmFragment() {

    private lateinit var viewModel: AddWalletViewModel

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter;

    override fun onInject() =
        CustomerMobileApp.component.inject(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = createViewModel { CustomerMobileApp.appComponent.addWalletViewModel }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_add_wallet, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressViewLayout.setProgressColor(
            confHelper.backgroundThemeAccentColor,
            confHelper.backgroundThemeBackgroundColor
        )
        buttonCreateWallet.setOnClickListener { createWallet() }
    }

    private fun createWallet() {
        if (!context?.isOnline()!!) {
            context?.connectionErrorDialog()
            return
        }
        val wallet = Wallet().apply { walletNickName = editTextWalletNickName.text.toString() }
        viewModel.addWallet(wallet).observe(this, Observer { onCreateWalletResult(it) })
    }

    private fun onCreateWalletResult(result: Result<Data>?) {
        result?.let {
            when (it) {
                is Result.Loading -> {
                    activity?.toolbar?.hide()
                    loadingProgressLayout?.show()
                }

                is Result.Success -> {
                    view?.announceForAccessibility(getString(R.string.payment_successfully_added_a_new_card))
                    activity?.let { activity ->
                        analyticsPlatformAdapter.paymentMethodAdded(
                            AnalyticsPlatformsContract.PaymentType.WALLET, "",
                            AnalyticsPlatformsContract.Status.SUCCESS, ""
                        )
                        val intent = Intent()
                        intent.putExtra(
                            SHOPPING_CART_INTENT,
                            activity.intent?.getBooleanExtra(
                                SHOPPING_CART_INTENT,
                                false
                            )
                        )
                        activity.setResult(AppConstants.ADD_WALLET_SUCCESS_CODE, intent)
                        activity.finish()
                    }
                }

                is Result.Failure -> {
                    activity?.toolbar?.show()
                    loadingProgressLayout?.hide()
                    analyticsPlatformAdapter.paymentMethodAdded(
                        AnalyticsPlatformsContract.PaymentType.WALLET, "",
                        AnalyticsPlatformsContract.Status.FAILURE, it.bmError.first().message
                    )
                    handleError(it.bmError[0])
                }
                else -> {
                }
            }
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment.
         */
        @JvmStatic
        fun newInstance() =
            AddWalletFragment()
    }
}
