package co.bytemark.add_wallet

import androidx.lifecycle.ViewModel
import co.bytemark.domain.interactor.payments.CreateWalletUseCase
import co.bytemark.domain.interactor.payments.CreateWalletUseCaseValue
import co.bytemark.sdk.model.payment_methods.Wallet
import javax.inject.Inject

/** Created by Santosh on 2019-11-19.
 * Copyright (c) 2019 Bytemark Inc. All rights reserved.
 */

class AddWalletViewModel @Inject constructor(
        private val addWalletUseCase: CreateWalletUseCase
) : ViewModel() {

    fun addWallet(wallet: Wallet) =
            addWalletUseCase.getLiveData(CreateWalletUseCaseValue(wallet))
}