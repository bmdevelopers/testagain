package co.bytemark.rewards

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.rewards.velocia.VelociaRewardsFragment
import co.bytemark.sdk.model.common.Action
import co.bytemark.webview.WebViewFragment
import co.bytemark.widgets.util.replaceFragment

class RewardsActivity : MasterActivity() {

    override fun getLayoutRes() = R.layout.activity_rewards

    private val rewardsFragment = VelociaRewardsFragment.newInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setNavigationViewCheckedItem(Action.REWARDS)
        setToolbarTitle(confHelper.getNavigationMenuScreenTitleFromTheConfig(Action.REWARDS))
        replaceFragment(rewardsFragment, R.id.container)
    }
}
