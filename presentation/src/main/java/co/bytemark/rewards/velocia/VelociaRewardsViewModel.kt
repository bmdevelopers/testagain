package co.bytemark.rewards.velocia

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.authentication.Location
import co.bytemark.domain.interactor.authentication.VelociaLoginRequest
import co.bytemark.domain.interactor.rewards.VelociaLoginUseCase
import co.bytemark.domain.model.authentication.VelociaLoginData
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import co.bytemark.transfer_balance.TransferBalanceViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class VelociaRewardsViewModel @Inject constructor(
        val velociaLoginUseCase: VelociaLoginUseCase
) : ViewModel() {

  val velociaResponseData by lazy {
    MutableLiveData<VelociaLoginData>()
  }

  val displayLoading by lazy {
    MutableLiveData<DisplayState>()
  }

  val errorData by lazy {
    MutableLiveData<BMError>()
  }

  fun loginToVelocia(latitude: Double, longitude: Double) = viewModelScope.launch {
    displayLoading.postValue(DisplayState.VELOCIA_LOGIN)
    when(val result = velociaLoginUseCase.invoke(
            VelociaLoginRequest(Location(latitude, longitude)))){
      is Result.Success -> {
        velociaResponseData.postValue(result.data)
      }
      is Result.Failure -> {
        errorData.postValue(result.bmError.first())
      }
    }
  }
  enum class DisplayState {
    VELOCIA_LOGIN,
    NONE
  }
}

