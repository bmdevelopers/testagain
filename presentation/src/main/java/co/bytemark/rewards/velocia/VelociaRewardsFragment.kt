package co.bytemark.rewards.velocia

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.MainActivity
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.authentication.AuthenticationActivity
import co.bytemark.base.MasterActivity
import co.bytemark.customtabs.CustomTabsActivityHelper
import co.bytemark.customtabs.CustomTabsHelper
import co.bytemark.customtabs.WebViewFallback
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.GPSTracker
import co.bytemark.webview.WebViewFragment
import co.bytemark.widgets.emptystateview.EmptyStateLayout
import co.bytemark.widgets.util.getViewModel
import com.google.android.material.snackbar.Snackbar
import com.tbruyelle.rxpermissions.RxPermissions


class VelociaRewardsFragment : WebViewFragment() {

  private var homePageLoaded: Boolean = false

  private lateinit var viewModel: VelociaRewardsViewModel

  private lateinit var emptyStateLayout: EmptyStateLayout

  private lateinit var rxPermissions: RxPermissions

  override fun onInject() {
    super.onInject()
    CustomerMobileApp.appComponent.inject(this)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    viewModel = getViewModel(CustomerMobileApp.appComponent.velociaRewardsViewModel)
  }

  override fun getLayoutRes() = R.layout.fragment_velocia_rewards

  override fun onAttach(context: Context) {
    super.onAttach(context)
    rxPermissions = RxPermissions(context as Activity)
  }

  override fun onOnline() {
    if(BytemarkSDK.isLoggedIn()) {
      if(homePageLoaded) {
        emptyStateLayout.showContent()
      }else{
        getRedirectUrl()
      }
    }else{
      showBytemarkSignInScreen()
    }
  }

  private fun showBytemarkSignInScreen() {
    emptyStateLayout.showError(R.drawable.ic_enter_filled, R.string.you_are_signed_out,
            R.string.velocia_bytemark_sign_in_message, R.string.settings_sign_in) { v ->
      if (activity == null) return@showError Unit
      if (confHelper.isAuthenticationNative) {
        val intent = Intent(activity, AuthenticationActivity::class.java)
        intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.MANAGE_CARD)
        startActivityForResult(intent, MasterActivity.REQUEST_CODE_LOGIN)
      } else {
        if (isOnline) requireActivity().startActivityForResult(Intent(activity, MainActivity::class.java), MasterActivity.REQUEST_CODE_LOGIN) else Snackbar.make(emptyStateLayout, R.string.network_connectivity_error_message, Snackbar.LENGTH_LONG).show()
      }
    }
  }

  override fun onOffline() {
    if(homePageLoaded.not()) {
      connectionErrorDialog()
    }
  }

  @SuppressWarnings("MissingPermission")
  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    emptyStateLayout = view.findViewById(R.id.emptyStateLayout)
    initObservers()
  }

  @SuppressLint("MissingPermission")
  private fun getRedirectUrl() {
    if (isOnline) {
      rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION)
              .doOnError { viewModel.loginToVelocia(0.0, 0.0) }
              .subscribe { granted ->
                var lat = 0.0
                var lon = 0.0
                val location = GPSTracker.getLocation(requireActivity())
                if (granted && location != null) {
                  lat = location.latitude
                  lon = location.longitude
                }
                viewModel.loginToVelocia(lat, lon)
              }
    } else {
      connectionErrorDialog()
    }
  }

  private fun initObservers() {
    viewModel.displayLoading.observe(viewLifecycleOwner, Observer {
      when (it) {
        VelociaRewardsViewModel.DisplayState.VELOCIA_LOGIN -> {
          emptyStateLayout.showLoading(R.drawable.tickets_material, R.string.loading)
        }
        VelociaRewardsViewModel.DisplayState.NONE -> {
          emptyStateLayout.showContent()
        }
      }
    })

    viewModel.velociaResponseData.observe(viewLifecycleOwner, Observer {
      it?.let {
        emptyStateLayout.showContent()
        loadPage(it.redirectUrl)
      }
    })

    viewModel.errorData.observe(viewLifecycleOwner, Observer {
      handleError(it)
    })
  }

  override fun applyWebViewSettings(settings: WebSettings?) {
    settings?.apply {
      javaScriptEnabled = true
      cacheMode = WebSettings.LOAD_DEFAULT
      loadWithOverviewMode = true
      databaseEnabled = true
      useWideViewPort = true
      domStorageEnabled = true
    }
  }

  override fun onPageLoadCompleted() {
    homePageLoaded = true
  }

  override fun overrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
    // simply returning true from this method is not working as expected, It is opening all the page including the home page in
    // browser, as we want to load the homepage in app's webview and only open consecutive links from home page in browser
    // here keeping a variable to track if the home page loaded or not. once home page loaded
    // Although we can check using URL like,
    //  if (url.host?.contains("velocia") == true && url.encodedPath.equals("/")) {
    //            return false
    //  }
    // don't know if this will work for all the scenarios

    if (homePageLoaded) {
      request?.url?.let { url ->
        activity?.let {
          startActivity(Intent(Intent.ACTION_VIEW, url))
          return true
        }
      }
    }
    return false
  }

  override fun connectionErrorDialog() {
    emptyStateLayout.showEmpty(
            R.drawable.error_material,
            getString(R.string.network_connectivity_error),
            getString(R.string.network_connectivity_error_message),
            getString(R.string.popup_retry)
    ) {
      getRedirectUrl()
    }
  }

  override fun showDefaultErrorDialog(message: String?) {
    emptyStateLayout.showError(R.drawable.error_material,R.string.popup_error, message)
  }

  override fun showDeviceTimeErrorDialog() {
    emptyStateLayout.showError(R.drawable.error_material, R.string.popup_error, getString(R.string.device_time_error_message))
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if ((requestCode == MasterActivity.REQUEST_CODE_LOGIN )
            && resultCode == BytemarkSDK.ResponseCode.SUCCESS) {
      getRedirectUrl()
    }
  }

  companion object {
    fun newInstance() =
            VelociaRewardsFragment().apply {
              arguments = Bundle().apply {
                putString(WEBVIEW_URL, "")
                putString(WEBVIEW_TITLE, "")
                putBoolean(WEBVIEW_ADD_DEFAULT_HEADERS, false)
              }
            }
  }

}