package co.bytemark.voucher_redeem


import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import androidx.core.app.TaskStackBuilder
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.use_tickets.UseTicketsActivity
import co.bytemark.voucher_redeem.widgets.VoucherCodeView
import co.bytemark.widgets.util.createViewModel
import co.bytemark.widgets.util.hide
import co.bytemark.widgets.util.postDelay
import co.bytemark.widgets.util.show
import kotlinx.android.synthetic.main.fragment_voucher_redeem.*
import javax.inject.Inject


class VoucherRedeemFragment : BaseMvvmFragment(), VoucherCodeView.VoucherCodeCompletionListener {

    @Inject
    lateinit var preference: SharedPreferences

    private lateinit var voucherRedeemViewModel: VoucherRedeemViewModel

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    override fun onInject() {
        CustomerMobileApp.component.inject(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        voucherRedeemViewModel = createViewModel { CustomerMobileApp.appComponent.voucherRedeemViewModel }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_voucher_redeem, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        message.text = getString(R.string.voucher_redeem_enter_msg, confHelper.voucherCodeLength)
        voucherCode.updateItemCount(confHelper.voucherCodeLength)
        voucherCode.setCompletionListner(this)

        viewTicketsButton.setOnClickListener { startUseTicketsActivity() }
        initializeLiveDataObserver()

        voucherCode.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        voucherCode.contentDescription = message.text
        voucherCode.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
        analyticsPlatformAdapter.voucherCodeScreenDisplayed()

    }

    private fun initializeLiveDataObserver() {
        observeResultLiveData()
        observeDisplayLoadingLiveData()
        observeErrorLiveData()
    }

    private fun observeResultLiveData() {
        voucherRedeemViewModel.voucherRedeemResult.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it) {
                    pinLayout.visibility = View.GONE
                    successLayout.visibility = View.VISIBLE
                    checkmark.isSelected = true
                    postDelay(200) {
                        successLayout.announceForAccessibility("${voucherCodeSuccess.text} \n ${successMessage.text}")
                        successLayout.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
                    }
                    voucherCode.clear()
                    analyticsPlatformAdapter.voucherCodeApplied(AnalyticsPlatformsContract.Status.SUCCESS, "")

                } else {
                    showVoucherCodeInvalid()
                }
            }
        })
    }

    private fun observeDisplayLoadingLiveData() {
        voucherRedeemViewModel.displayLoading.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it) {
                    progressBar.show()
                } else {
                    progressBar.hide()
                }
            }
        })
    }

    private fun observeErrorLiveData() {
        voucherRedeemViewModel.errorResult.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.code == BytemarkSDK.ResponseCode.VOUCHER_CODE_INVALID) {
                    showVoucherCodeInvalid()
                } else {
                    handleError(it)
                }
            }
        })
    }

    override fun onComplete() {
        if (isOnline()) {
            performRedeemVoucherRequest()
            voucherCode.announceForAccessibility(getString(R.string.submitting_voucher_code))
        } else {
            showNetworkUnavailableAlert()
        }
    }

    override fun onResume() {
        super.onResume()
        voucherCode.focus()
    }

    private fun isSavePassToDevice(): Boolean {
        preference.let {
            val flag = it.getInt(AppConstants.DEFAULT_DEVICE_STORAGE, 0)
            if (flag == 1)
                return false
            return true
        }
    }

    private fun performRedeemVoucherRequest() {
        if (!isOnline()) {
            showNetworkUnavailableAlert()
            return
        }
        voucherRedeemViewModel.redeemVoucherCode("tvm_voucher_order", voucherCode.getValue(), isSavePassToDevice())
    }

    private fun showVoucherCodeInvalid() {
        message.visibility = View.GONE
        failureMessage.visibility = View.VISIBLE
        voucherCode.announceForAccessibility(failureMessage.text)
        voucherCode.clear()
        analyticsPlatformAdapter.voucherCodeApplied(AnalyticsPlatformsContract.Status.FAILURE,
                getString(R.string.voucher_redeem_invalid_code_msg))
    }

    private fun showNetworkUnavailableAlert() {
        connectionErrorDialog(
                R.string.autoload_retry,
                false,
                R.string.ok,
                false,
                ::performRedeemVoucherRequest)
    }

    private fun startUseTicketsActivity() {
        val builder = TaskStackBuilder.create(activity!!)
        builder.addNextIntentWithParentStack(Intent(context, UseTicketsActivity::class.java))
        builder.startActivities()
    }

}
