package co.bytemark.voucher_redeem

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.sdk.model.common.Action

class VoucherRedeemActivity : MasterActivity() {

    override fun getLayoutRes() = R.layout.activity_voucher_redeem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setNavigationViewCheckedItem(Action.SETTINGS)

    }

    override fun useHamburgerMenu(): Boolean {
        return false
    }

}