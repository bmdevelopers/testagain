package co.bytemark.voucher_redeem.widgets

import android.content.Context
import android.graphics.Rect
import android.os.IBinder
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.text.method.TransformationMethod
import android.util.AttributeSet
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import co.bytemark.R
import timber.log.Timber

const val DEFAULT_VOUCHER_CODE_LENGTH: Int = 4

const val DEFAULT_ITEM_WIDTH = 40f
const val DEFAULT_ITEM_HEIGHT = 40f
const val DEFAULT_ITEM_SPACING = 8f
const val DEFAULT_ITEM_COUNT = 8
const val DEFAULT_TEXT_SIZE = 16f


// ViewType
const val VIEW_TYPE_LINE = 1
const val VIEW_TYPE_RECTANGLE = 2  // not implemented

// InputType
const val INPUT_TYPE_NUMBER = 1
const val INPUT_TYPE_TEXT = 2

// Voucher Code Type
const val VOUCHER_CODE_TYPE_LIMITED = 1
const val VOUCHER_CODE_TYPE_UNLIMITED = 2  // not implemented

// STATUS

const val STATUS_INCOMPLETE = 1
const val STATUS_COMPLETED = 2


// SUBMIT FLOW
/**
 *  VoucherCode Completion status triggered if user enters the code from 0 - n without skipping any intermediate views
 */
const val COMPLETION_FLOW_AUTO = 0

/**
 *  VoucherCode Completion status NOT triggered if user go(Focus) between voucher code fields and after enters all the chars,
 *  he need to press the done button to trigger the completion
 */
const val COMPLETION_FLOW_WAIT_FOR_DONE = 1


const val TAG = "VoucherCodeView"


class VoucherCodeView : ViewGroup, TextWatcher, View.OnFocusChangeListener, View.OnKeyListener, View.OnTouchListener, TextView.OnEditorActionListener {


    constructor(context: Context?) : super(context) {
        init(context, null, 0, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs, 0, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
            context,
            attrs,
            defStyleAttr
    ) {
        init(context, attrs, defStyleAttr, 0)
    }

    constructor(
            context: Context?,
            attrs: AttributeSet?,
            defStyleAttr: Int,
            defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs, defStyleAttr, defStyleRes)
    }


    private val editTextList: MutableList<EditText> = mutableListOf()


    private var itemCount: Int = DEFAULT_VOUCHER_CODE_LENGTH

    private var itemWidth: Float = DEFAULT_ITEM_WIDTH

    private var itemHeight: Float = DEFAULT_ITEM_HEIGHT

    private var itemSpace: Float = DEFAULT_ITEM_SPACING

    private var textSize: Float = DEFAULT_TEXT_SIZE

    private var maskInput: Boolean = false

    private var showCursor: Boolean = false

    private var inputType: Int = INPUT_TYPE_TEXT

    private var voucherCodeType: Int = VOUCHER_CODE_TYPE_LIMITED

    private var viewType: Int = VIEW_TYPE_LINE

    private var clickListener: OnClickListener? = null

    private var voucherCodeCompletionListener: VoucherCodeCompletionListener? = null


    private val hint: String = ""

    private var delPressed = false

    private var finalEntry = false

    private var fromSetValue = false

    private var rows = 0

    private var cols = 0

    private lateinit var lp: MarginLayoutParams

    private val forceKeyboard = true


    private var status: Int = STATUS_INCOMPLETE

    private var submitFlow = COMPLETION_FLOW_AUTO


    private lateinit var currentFocus: View

    private var prevFocusIndex = 0

    private var currentFocusindex = 0


    interface VoucherCodeCompletionListener {
        fun onComplete()
    }

    fun init(
            context: Context?,
            attrs: AttributeSet?,
            defStyleAttr: Int,
            defStyleRes: Int
    ) {
        // extract resource from attributes set
        setWillNotDraw(false)
        getAttributes(context!!, attrs, defStyleAttr)
        lp = MarginLayoutParams(itemWidth.toInt(), itemHeight.toInt())
        createChildViews()

        currentFocus = editTextList[0]
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        currentFocus.postDelayed({
            openKeyBoard()
        }, 200)
    }


    private fun createChildViews() {
        removeAllViews()
        editTextList.clear()
        var editText: EditText
        for (i in 1..itemCount) {
            editText = EditText(context)
            editText.textSize = textSize
            editTextList.add(editText)
            editText.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS
            addView(editText)
            applyStyle(editText, "" + i)

            editText.removeTextChangedListener(this)
            if (maskInput) {
                editText.transformationMethod = PinTransformation()
            } else {
                editText.transformationMethod = null
            }
            editText.addTextChangedListener(this)
        }
    }

    private fun applyStyle(editText: EditText, hint: String) {
        val space = (itemSpace / 2).toInt()
        lp.setMargins(space, space, space, space)

        editText.minWidth = itemWidth.toInt()
        editText.minHeight = itemHeight.toInt()


        editText.filters = arrayOf(InputFilter.LengthFilter(1))
        editText.layoutParams = lp
        editText.gravity = Gravity.CENTER
        editText.isCursorVisible = showCursor


        editText.setImeActionLabel("Done", EditorInfo.IME_ACTION_DONE)

        editText.setOnTouchListener(this)


        editText.setPadding(12, 12, 12, 12)
        editText.tag = hint
        editText.inputType = getKeyboardInputType()
        editText.addTextChangedListener(this)
        editText.onFocusChangeListener = this
        editText.setOnKeyListener(this)
        editText.setOnEditorActionListener(this)

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        var widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        var heightSize = MeasureSpec.getSize(heightMeasureSpec)

        /*
            Measure one child's width and height  -- Every View is going to be same width and
            height, so skipping other measurement

         */
        val count = childCount

        // measuring one child's spec

        val child = getChildAt(0)

        val lp: MarginLayoutParams = child.layoutParams as MarginLayoutParams

        var childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(widthSize, MeasureSpec.UNSPECIFIED)
        var childHeightMeasureSpec =
                MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.UNSPECIFIED)


        child.measure(childWidthMeasureSpec, childHeightMeasureSpec)

        // width going to be same as what is given, need to measure height and count number of rows for the given width

        val allocatedWidth = widthSize - (paddingLeft + paddingRight)

        val childWidth = child.measuredWidth + lp.leftMargin + lp.rightMargin
        val childHeight = child.measuredHeight + lp.topMargin + lp.bottomMargin

        val requiredWidth = childWidth * count

        if (requiredWidth > allocatedWidth) {
            // need to add one more row

            // calculating how many child will the current width will accommodate

            cols = allocatedWidth / childWidth

            // removing and setting width size to exactly noOfChild in a row
            val newWidth = cols * childWidth

            rows = if (count % cols == 0) {
                count / cols
            } else {
                (count / cols) + 1
            }


            widthSize = newWidth + paddingLeft + paddingRight
            heightSize = (rows * childHeight) + paddingTop + paddingBottom


        } else {

            cols = allocatedWidth / childWidth
            rows = 0

            widthSize = requiredWidth + paddingLeft + paddingRight
            heightSize = childHeight + paddingTop + paddingBottom

        }

        // measuring all the children with exact size

        childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(childWidth, MeasureSpec.EXACTLY)
        childHeightMeasureSpec =
                MeasureSpec.makeMeasureSpec(childHeight, MeasureSpec.EXACTLY)

        for (i in 0 until count) {
            val child = getChildAt(i)
            child.measure(childWidthMeasureSpec, childHeightMeasureSpec)
        }


        setMeasuredDimension(widthSize, heightSize)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {

        var currentRow = 0
        var currentCol = 0
        var offset = 0
        for (i in 0 until childCount) {
            val child = getChildAt(i)

            if (rows > 1 && currentRow == (rows - 1) && currentCol == 0 && (childCount % cols) != 0) {
                // center align the remaining fields

                val remainingViews = (childCount - i)
                offset = ((r - l) / 2) - ((remainingViews * child.measuredWidth) / 2)

            }

            val startX = currentCol * child.measuredWidth + offset
            val startY = currentRow * child.measuredHeight
            val endX = startX + child.measuredWidth
            val endY = startY + child.measuredHeight

            //  Timber.tag(TAG).d("OnLayout : Left : $startX , Top : $startY,  Right : $endX,  Bottom : $endY")


            child.layout(startX, startY, endX, endY)

            if (currentCol < (cols - 1)) {
                currentCol++
            } else {
                currentCol = 0
                currentRow++
            }

        }

    }

    private fun getKeyboardInputType() = when (inputType) {
        INPUT_TYPE_NUMBER -> if (maskInput) {
            InputType.TYPE_NUMBER_VARIATION_PASSWORD
        } else {
            InputType.TYPE_CLASS_NUMBER
        }
        INPUT_TYPE_TEXT -> if (maskInput) {
            InputType.TYPE_TEXT_VARIATION_PASSWORD
        } else {
            InputType.TYPE_CLASS_TEXT
        }
        else -> InputType.TYPE_CLASS_NUMBER
    }

    private fun openKeyBoard() {
        val imm: InputMethodManager =
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(
                InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY
        )
    }

    private fun closeKeyBoard(windowToken: IBinder) {
        val imm: InputMethodManager =
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun getAttributes(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {

        val density = context.resources.displayMetrics.density;

        if (attrs != null) {
            val array =
                    context.obtainStyledAttributes(
                            attrs,
                            R.styleable.VoucherCodeView,
                            defStyleAttr,
                            0
                    )

            itemWidth = array.getDimension(
                    R.styleable.VoucherCodeView_itemWidth,
                    ((DEFAULT_ITEM_WIDTH * density))
            )
            itemHeight = array.getDimension(
                    R.styleable.VoucherCodeView_itemHeight,
                    ((DEFAULT_ITEM_HEIGHT * density))
            )
            itemCount = array.getInt(
                    R.styleable.VoucherCodeView_count,
                    DEFAULT_ITEM_COUNT
            )
            itemSpace = array.getDimension(
                    R.styleable.VoucherCodeView_space,
                    ((DEFAULT_ITEM_SPACING * density))
            )

            textSize = array.getDimension(
                    R.styleable.VoucherCodeView_textSize,
                    ((DEFAULT_TEXT_SIZE * density))
            )

            maskInput = array.getBoolean(R.styleable.VoucherCodeView_maskInput, false)

            showCursor = array.getBoolean(R.styleable.VoucherCodeView_showCursor, false)

            inputType = array.getInt(R.styleable.VoucherCodeView_inputType, INPUT_TYPE_TEXT)

            voucherCodeType =
                    array.getInt(R.styleable.VoucherCodeView_voucherCodeType, VOUCHER_CODE_TYPE_LIMITED)


            viewType = array.getInt(R.styleable.VoucherCodeView_viewType, VIEW_TYPE_LINE)

            array.recycle()
        }


    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        // this check prevents unnecessary focus change to middle middle when talk back is on.
        // And with this user will not manually focus other inputs if talk back is on.
        if (isAccessibilityFocused) {
            openKeyBoard()
            return true
        }

        if (event?.action == MotionEvent.ACTION_UP) {
            if (v != null) {
                currentFocus = v as EditText
            }
        }

        return false
    }

    override fun afterTextChanged(s: Editable?) {
        // Not going to do anything here
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        // Not going to do anything here
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        val currentIndex = getIndexOfCurrentFocus()
        if (s?.length == 1) {
            // This input is already filled, change the focus to next Input
            if (s.isBlank()) {
                editTextList[currentIndex].setText("")
                return
            }
            if (currentIndex < itemCount - 1) {
                // If it is not the last one
                var delay = 1L

                if (maskInput)
                    delay = 25L
                currentFocus = editTextList[currentIndex + 1]
                this.postDelayed({
                    editTextList[currentIndex + 1].apply {
                        isEnabled = true
                        requestFocus()

                    }

                }, delay)

            } else {
                // Last char entered
                if (submitFlow == COMPLETION_FLOW_AUTO) {
                    closeKeyBoard(editTextList[currentIndex].windowToken)
                    this.postDelayed({
                        if (allInputEntered()) {
                            voucherCodeCompletionListener?.let {
                                it.onComplete()
                            }
                        }
                    }, 100)
                }

            }

        } else if (s?.length == 0) {
            if (editTextList[currentIndex].length() > 0) {
                editTextList[currentIndex].setText("")
            }
        }
    }

    override fun onFocusChange(v: View, hasFocus: Boolean) {
        val index = editTextList.indexOf(v)
        Timber.tag(TAG).d("Focus changed : Index : $index , Focus : $hasFocus")

        if (hasFocus) {
            currentFocusindex = index
        } else {
            prevFocusIndex = index
        }

        val diff = currentFocusindex - prevFocusIndex

        // checking if the difference is between -1 to 1 , -1 for deletion, 1 for next item focus
        // 0 for same item focus -- if any other than this happens at-least one time i am changing
        // the flow to wait for done button press
        if (!(diff >= -1 && diff <= 1)) {
            submitFlow = COMPLETION_FLOW_WAIT_FOR_DONE
        }

    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {

        event?.let {
            if (event.action == KeyEvent.ACTION_UP) {

                val currentIndex = editTextList.indexOf(v)

                when (keyCode) {
                    KeyEvent.KEYCODE_DEL -> {
                        if (currentIndex == 0) {
                            editTextList[currentIndex].setText("")
                        } else {
                            if (editTextList[currentIndex].length() == 0) {
                                // Case 2-1 : Input is not filled, change the focus to previous input
                                currentFocus = editTextList[currentIndex - 1]
                                editTextList[currentIndex - 1].requestFocus()
                                editTextList[currentIndex].setText("")
                            } else {
                                // Case 2-2 : Input is filled, clear the content, don't change the Focus
                                editTextList[currentIndex].setText("")
                            }
                        }
                        return true

                    }

                    KeyEvent.KEYCODE_ENTER -> {
                        // not working , so added onEditorActionListener
                    }
                }
            }
        }

        return false
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            if (allInputEntered()) {
                voucherCodeCompletionListener?.let {
                    it.onComplete()
                }
                closeKeyBoard(editTextList[itemCount - 1].windowToken)
            }
        }
        return false
    }

    private fun allInputEntered(): Boolean {
        for (editText in editTextList) {
            // isBlank checks white space chars also, as we don't have any whitespace in voucher code
            if (editText.text.isBlank()) {
                return false
            }
        }
        return true
    }

    private fun getIndexOfCurrentFocus() = editTextList.indexOf(currentFocus)

    fun getValue(): String {
        val stringBuilder = StringBuilder()
        for (editText in editTextList) {
            stringBuilder.append(editText.text.toString())
        }
        return stringBuilder.toString()
    }

    fun updateItemWidth(value: Float) {
        this.itemWidth = value
        lp.width = value.toInt()
        for (editText in editTextList) {
            editText.layoutParams = lp
        }
    }

    fun updateItemHeight(value: Float) {
        this.itemHeight = value
        lp.height = value.toInt()
        for (editText in editTextList) {
            editText.layoutParams = lp
        }
    }

    fun updateItemCount(value: Int) {
        this.itemCount = value
        createChildViews()
    }

    fun updateItemSpacing(value: Float) {
        this.itemSpace = value
        val margin = (value / 2).toInt()
        lp.setMargins(margin, margin, margin, margin)
        for (editText in editTextList) {
            editText.layoutParams = lp
        }
    }

    fun updateTextSize(value: Float) {
        this.textSize = value
        for (editText in editTextList) {
            editText.textSize = textSize
        }
    }

    fun setCompletionListner(listener: VoucherCodeCompletionListener) {
        this.voucherCodeCompletionListener = listener
    }

    fun clear() {
        for (editText in editTextList) {
            editText.setText("")
        }
        currentFocus = editTextList[0]
        currentFocus.requestFocus()
        submitFlow = COMPLETION_FLOW_AUTO
        currentFocusindex = 0
        prevFocusIndex = 0
    }

    fun focus() {
        currentFocus = editTextList[0]
        currentFocus.requestFocus()
        submitFlow = COMPLETION_FLOW_AUTO
        currentFocusindex = 0
        prevFocusIndex = 0
        openKeyBoard()
    }

    override fun getAccessibilityClassName(): CharSequence {
        return javaClass.simpleName
    }


}


class PinTransformation : TransformationMethod {


    override fun onFocusChanged(
            view: View?,
            sourceText: CharSequence?,
            focused: Boolean,
            direction: Int,
            previouslyFocusedRect: Rect?
    ) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun getTransformation(source: CharSequence, view: View?): CharSequence {
        return PasswordSequence(source)
    }

    class PasswordSequence(val charSequence: CharSequence) : CharSequence {

        // todo can we make it configurable , does it needed ?
        private val BULLET = '\u2022'

        override val length: Int
            get() = charSequence.length


        override fun get(index: Int): Char {
            return BULLET
        }

        override fun subSequence(startIndex: Int, endIndex: Int): CharSequence {
            return PasswordSequence(charSequence.subSequence(startIndex, endIndex))
        }

    }


}





