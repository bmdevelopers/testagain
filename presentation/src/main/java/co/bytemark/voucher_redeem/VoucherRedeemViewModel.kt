package co.bytemark.voucher_redeem

import androidx.lifecycle.MutableLiveData
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.voucher_redeem.VoucherRedeemUseCase
import co.bytemark.domain.interactor.voucher_redeem.VoucherRedeemUseCaseValue
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import kotlinx.coroutines.launch
import javax.inject.Inject

class VoucherRedeemViewModel @Inject constructor(
        private val voucherRedeemUseCase: VoucherRedeemUseCase
) : BaseViewModel() {

    val voucherRedeemResult by lazy {
        MutableLiveData<Boolean>()
    }

    val errorResult by lazy {
        MutableLiveData<BMError>()
    }

    val displayLoading by lazy {
        MutableLiveData<Boolean>()
    }

    fun redeemVoucherCode(orderType: String, voucherCode: String, isSavePassToDevice: Boolean) =
            uiScope.launch {
                displayLoading.postValue(true)
                val result = voucherRedeemUseCase.invoke(
                        VoucherRedeemUseCaseValue(
                                orderType,
                                voucherCode,
                                isSavePassToDevice
                        ))
                displayLoading.postValue(false)
                when (result) {
                    is Result.Success -> {
                        result.data?.let {
                            voucherRedeemResult.postValue(true)
                        }
                    }

                    is Result.Failure -> {
                        errorResult.postValue(result.bmError.first())
                    }
                }

            }

}