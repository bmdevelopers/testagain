package co.bytemark.product_details;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import javax.inject.Inject;

import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.base.MasterActivity;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.model.product_search.entity.EntityResult;
import co.bytemark.shopping_cart.AcceptPaymentActivity;
import co.bytemark.shopping_cart_new.NewShoppingCartActivity;

import static co.bytemark.analytics.AnalyticsPlatformsContract.AnalyticsPlatformEntry.HEADER_BUTTON;
import static co.bytemark.sdk.model.common.Action.STORE;

@SuppressWarnings("deprecation")
public class ProductDetailsActivity extends MasterActivity {

    @Inject
    Gson gson;

    @Inject
    ConfHelper confHelper;

    @Inject
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomerMobileApp.Companion.getComponent().inject(this);

        setNavigationViewCheckedItem(STORE);

        EntityResult entityResult = getIntent().getExtras().getParcelable(AppConstants.SELECTED_ENTITY_RESULT);
        if (entityResult != null) {
            if (!TextUtils.isEmpty(entityResult.getName())) {
                setToolbarTitle(entityResult.getName());
            } else {
                setToolbarTitle(entityResult.getLabelName());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    protected boolean useHamburgerMenu() {
        return false;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_product_details;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_buy_tickets, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem cart = menu.findItem(R.id.action_cart);
        cart.setActionView(R.layout.menu_item_cart_layout);

        TextView textViewCartCount = cart.getActionView().findViewById(R.id.tv_cart_count);
        textViewCartCount.setBackgroundTintList(ColorStateList.valueOf(confHelper.getHeaderThemeAccentColor()));
        textViewCartCount.setTextColor(confHelper.getHeaderThemeSecondaryTextColor());
        textViewCartCount.setVisibility(View.GONE);

        ImageView imageViewCart = cart.getActionView().findViewById(R.id.iv_cart);
        imageViewCart.setImageDrawable(getResources().getDrawable(R.drawable.filled_cart_material));
        imageViewCart.setImageTintList(ColorStateList.valueOf(confHelper.getHeaderThemePrimaryTextColor()));
        imageViewCart.setContentDescription(getResources().getString(R.string.shopping_cart_voonly));

        String entityResultString = sharedPreferences.getString(AppConstants.ENTITY_RESULT_LIST, null);

        List<EntityResult> savedEntityResults = gson.fromJson(entityResultString, new TypeToken<List<EntityResult>>() {
        }.getType());

        String string;

        if (savedEntityResults != null) {

            int quantity = 0;

            for (int i = 0; i < savedEntityResults.size(); i++) {
                quantity += savedEntityResults.get(i).getQuantity();
            }

            if (quantity != 0) {
                String count = String.valueOf(quantity);

                textViewCartCount.setVisibility(View.VISIBLE);
                string = getResources().getQuantityString(R.plurals.shopping_cart_item, quantity, quantity);
                textViewCartCount.setContentDescription(string);
                textViewCartCount.setText(count);
            } else {
                textViewCartCount.setVisibility(View.GONE);
            }

        } else {
            textViewCartCount.setVisibility(View.GONE);
        }

        cart.getActionView().setOnClickListener(view -> {
            final Intent intent = new Intent(ProductDetailsActivity.this, NewShoppingCartActivity.class);
            intent.putExtra(AcceptPaymentActivity.SOURCE, HEADER_BUTTON);
            startActivity(intent);
        });

        return true;
    }
}
