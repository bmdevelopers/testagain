package co.bytemark.product_details;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.base.BaseFragment;
import co.bytemark.buy_tickets.QuantityFilter;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.model.product_search.entity.EntityResult;
import timber.log.Timber;

public class ProductDetailsActivityFragment extends BaseFragment {

    @Inject
    Gson gson;

    @Inject
    ConfHelper confHelper;

    @Inject
    SharedPreferences sharedPreferences;

    @BindView(R.id.ll_product_details)
    LinearLayout linearLayoutProductDetails;
    @BindView(R.id.ll_product_qty)
    LinearLayout linearLayoutProductQuantity;
    @BindView(R.id.iv_product_monochrome)
    ImageView imageViewProductMonochrome;
    @BindView(R.id.tv_product_amount)
    TextView textViewProductAmount;
    @BindView(R.id.tv_product_qty)
    TextView textViewProductQty;
    @BindView(R.id.et_product_qty)
    EditText editTextProductQty;
    @BindView(R.id.tv_product_name)
    TextView textViewProductName;
    @BindView(R.id.tv_product_short_description)
    TextView textViewShortDesc;
    @BindView(R.id.tv_product_long_description)
    TextView textViewLongDesc;
    @BindView(R.id.btn_add_to_cart)
    Button buttonAddToCart;

    private EntityResult entityResult;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_product_details;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editTextProductQty.setFilters((new InputFilter[]{new QuantityFilter(1, 10), new InputFilter.LengthFilter(2)}));

        if (getActivity().getIntent().hasExtra(AppConstants.SELECTED_ENTITY_RESULT)) {
            entityResult = getActivity().getIntent().getExtras().getParcelable(AppConstants.SELECTED_ENTITY_RESULT);
            if (entityResult != null) {
                textViewProductAmount.setText(confHelper.getConfigurationPurchaseOptionsCurrencySymbol(entityResult.getSalePrice()));

                final String quantity;
                if (entityResult.getQuantity() != 0) {
                    quantity = String.valueOf(entityResult.getQuantity());
                } else {
                    quantity = String.valueOf(1);
                }
                editTextProductQty.setText(quantity);
                if (!TextUtils.isEmpty(entityResult.getName())) {
                    textViewProductName.setVisibility(View.VISIBLE);
                    textViewProductName.setText(entityResult.getName());
                }
                if (!TextUtils.isEmpty(entityResult.getShortDescription())) {
                    textViewShortDesc.setVisibility(View.VISIBLE);
                    textViewShortDesc.setText(entityResult.getShortDescription());
                }
                if (!TextUtils.isEmpty(entityResult.getLongDescription())) {
                    textViewLongDesc.setVisibility(View.VISIBLE);
                    textViewLongDesc.setText(entityResult.getLongDescription());
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        buttonAddToCart.setTextColor(confHelper.getAccentThemePrimaryTextColor());
        buttonAddToCart.setBackgroundColor(confHelper.getAccentThemeBacgroundColor());
        linearLayoutProductDetails.setBackgroundColor((confHelper.getBackgroundThemeBackgroundColor()));


        int bgColor = 0;
        String childOrgUuid = null;
        if (entityResult != null) {
            childOrgUuid = entityResult.getOrganization().getLegacyUuid().replaceAll("-", "_");
            int defaultBackgroundColor = 0;
            String backgroundColor = null;
            if (entityResult.getTheme() == null) {
                defaultBackgroundColor = confHelper.getOrganizationHeaderThemeColor(entityResult.getOrganization().getLegacyUuid());
            } else if (entityResult.getTheme().getBackgroundColor() != null) {
                backgroundColor = entityResult.getTheme().getBackgroundColor();
            } else {
                backgroundColor = entityResult.getTheme().getProperties().getBackgroundColor();
            }

            bgColor = defaultBackgroundColor != 0 ? defaultBackgroundColor : confHelper.parseColor(backgroundColor);
        }

        Drawable drawable = null;
        try {
            drawable = confHelper.getDrawableByName("wide_" + childOrgUuid);
        } catch (Exception e) {
            Timber.e("Failure to get drawable id. %s", e.toString());
        }

        if (drawable == null) {
            final String parentOrgUuid = CustomerMobileApp.Companion.getConf().getOrganization().getUuid().replaceAll("-", "_");
            drawable = confHelper.getDrawableByName("wide_" + parentOrgUuid);
        }

        imageViewProductMonochrome.setImageDrawable(drawable);

        imageViewProductMonochrome.setBackgroundColor(bgColor);

        announceForAccessibility();
    }

    private void announceForAccessibility() {
        Resources resources = getActivity().getResources();

        String organizationName = null;
        for (int i = 0; i < CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations().size(); i++) {
            String OrgUuid = CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations().get(i).getUuid();
            if (OrgUuid.equals(entityResult.getOrganization().getLegacyUuid())) {
                organizationName = CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations().get(i).getDisplayName();
            }
        }
        imageViewProductMonochrome.setContentDescription(organizationName);

        editTextProductQty.setTextColor(confHelper.getDataThemeAccentColor());
        textViewProductAmount.setTextColor(confHelper.getDataThemeAccentColor());
        textViewProductQty.setContentDescription(resources.getString(R.string.quantity_voonly));
        linearLayoutProductQuantity.setContentDescription(resources.getString(R.string.quantity_voonly) + " " +
                resources.getString(R.string.is) + " " + editTextProductQty.getText().toString());
    }

    @OnClick(R.id.btn_add_to_cart)
    public void onAddToCartClickListener() {

        if (!editTextProductQty.getText().toString().isEmpty()) {

            String entityResultString = sharedPreferences.getString(AppConstants.ENTITY_RESULT_LIST, null);

            if (entityResultString != null) {
                List<EntityResult> savedEntityResults = gson.fromJson(entityResultString, new TypeToken<List<EntityResult>>() {
                }.getType());

                if (savedEntityResults.size() == 0) {
                    addProductAndGoToShoppingCartScreen(entityResult);
                } else if (savedEntityResults.get(0).getOrganization().getLegacyUuid()
                        .equals(entityResult.getOrganization().getLegacyUuid())) {

                    addProductAndGoToShoppingCartScreen(entityResult);
                } else {
                    showAddToCartForDiffOrgErrorDialog(savedEntityResults.get(0).getOrganization().getLegacyUuid(), entityResult);
                }
            } else {
                addProductAndGoToShoppingCartScreen(entityResult);
            }
        }
    }

    private void addProductAndGoToShoppingCartScreen(EntityResult entityResult) {
        if (TextUtils.isEmpty(editTextProductQty.getText())) {
            entityResult.setQuantity(0);
        } else {
            entityResult.setQuantity(Integer.parseInt(editTextProductQty.getText().toString()));
        }

        Intent intent = new Intent();
        intent.putExtra(AppConstants.ADDED_ENTITY_RESULT, entityResult);
        getActivity().setResult(AppConstants.PRODUCT_DETAILS_REQUEST_CODE, intent);
        getActivity().finish();
    }

    public void showAddToCartForDiffOrgErrorDialog(String legacyUuid, final EntityResult entityResult) {

        String displayName = null;

        for (int i = 0; i < CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations().size(); i++) {
            if (CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations().get(i).getUuid().equals(legacyUuid)) {
                displayName = CustomerMobileApp.Companion.getConf().getOrganization().getChildOrganizations().get(i).getDisplayName();
            }
        }

        new MaterialDialog.Builder(getContext())
                .title(R.string.buy_tickets_popup_start_new_order_title)
                .content(R.string.buy_tickets_start_new_order_message_1 + "\"" + displayName + "\"" + R.string.buy_tickets_start_new_order_message_2)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .negativeText(R.string.popup_cancel)
                .onPositive((dialog, which) -> addProductAndGoToShoppingCartScreen(entityResult))
                .onNegative((dialog, which) -> dialog.dismiss())
                .show();
    }
}
