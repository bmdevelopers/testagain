package co.bytemark.manage.upass

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.bytemark.domain.interactor.manage.GetInstitutionListUseCase
import co.bytemark.domain.interactor.manage.UPassVerificationUseCaseValue
import co.bytemark.domain.interactor.manage.VerifyUPassEligibilityUseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.manage.Institution
import co.bytemark.domain.model.manage.UPassEligibilityResponseData
import kotlinx.coroutines.launch
import javax.inject.Inject

class UPassValidationViewModel @Inject constructor(
        private val getInstitutionListUseCase: GetInstitutionListUseCase,
        private val verifyUPassEligibilityUseCase: VerifyUPassEligibilityUseCase
) : ViewModel() {

    val institutionList by lazy {
        MutableLiveData<List<Institution>>()
    }

    val errorLiveData by lazy {
        MutableLiveData<BMError>()
    }

    val displayState by lazy {
        MutableLiveData<DisplayState>()
    }

    val verificationStatus by lazy {
        MutableLiveData<UPassEligibilityResponseData>()
    }


    fun getInstitutionList(pageIndex: Int = 0) = viewModelScope.launch {
        displayState.postValue(DisplayState.LOADING_INSTITUTION)
        when (val result = getInstitutionListUseCase(pageIndex)) {
            is Result.Success -> {
                result.data?.let {
                    institutionList.postValue(it.institutions)
                }
            }
            is Result.Failure -> {
                errorLiveData.postValue(result.bmError.first())
            }
        }
        displayState.postValue(DisplayState.NONE)
    }

    fun verifyEligibility(instituteId: String, fareMediumUuid: String) = viewModelScope.launch {
        displayState.postValue(DisplayState.VERIFYING_ELIGIBILITY)
        when (val result = verifyUPassEligibilityUseCase(UPassVerificationUseCaseValue(instituteId, fareMediumUuid))) {
            is Result.Success -> {
                result.data?.let {
                    verificationStatus.postValue(it)
                }
            }
            is Result.Failure -> {
                errorLiveData.postValue(result.bmError.first())
            }
        }
        displayState.postValue(DisplayState.NONE)
    }

    enum class DisplayState {
        LOADING_INSTITUTION,
        VERIFYING_ELIGIBILITY,
        NONE
    }

}
