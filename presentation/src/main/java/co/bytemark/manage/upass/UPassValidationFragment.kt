package co.bytemark.manage.upass

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.databinding.FragmentUpassBinding
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.manage.Institution
import co.bytemark.helpers.AppConstants
import co.bytemark.manage.ManageCardsActivity
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.widgets.emptystateview.State
import co.bytemark.widgets.util.getViewModel
import kotlinx.android.synthetic.main.fragment_upass.view.*

class UPassValidationFragment : BaseMvvmFragment(), SelectionListener<Institution> {

  private lateinit var binding: FragmentUpassBinding

  private lateinit var viewModel: UPassValidationViewModel

  private lateinit var fareMediaUuid: String
  private lateinit var fareMediaId: String

  private lateinit var adapter: InstitutionSelectionAdapter

  private var isLoading = false
  private var finalPageLoaded = false
  private var pageIndex = 0

  companion object {

    private const val ARG_FARE_MEDIA_ID = "fareMediaId"

    fun newInstance(): UPassValidationFragment {
      return UPassValidationFragment()
    }

  }

  override fun onInject() {
    CustomerMobileApp.appComponent.inject(this)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    viewModel = getViewModel(CustomerMobileApp.appComponent.uPassValidationViewModel)
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    binding = FragmentUpassBinding.inflate(inflater, container, false)
    return binding.root
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    fareMediaUuid = activity?.intent?.getParcelableExtra<FareMedium>(AppConstants.FARE_MEDIA)?.uuid
            ?: ""
    fareMediaId = activity?.intent?.getParcelableExtra<FareMedium>(AppConstants.FARE_MEDIA)?.fareMediumId
            ?: ""

    if (fareMediaId.isEmpty()) {
      showDefaultErrorDialog(
              getString(R.string.popup_error),
              getString(R.string.something_went_wrong)
      )
      return
    }

    adapter = InstitutionSelectionAdapter(this)
    binding.recyclerView.adapter = adapter
    binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
      override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager?
        val position = layoutManager?.findLastVisibleItemPosition()
        if (position == adapter.itemCount - 1) {
          if (!isLoading && !finalPageLoaded) {
            pageIndex += 1
            viewModel.getInstitutionList(pageIndex)
          }
        }
      }
    })

    initObservers()
    viewModel.getInstitutionList(pageIndex)
  }

  override fun optionSelected(selection: Institution) {
    viewModel.verifyEligibility(selection.institutionId, fareMediaUuid)
  }

  private fun initObservers() {
    with(this) {
      viewModel.displayState.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
        when (it) {
          UPassValidationViewModel.DisplayState.LOADING_INSTITUTION -> {
            isLoading = true
            if (pageIndex == 0) {
              binding.emptyStateLayout.showLoading(
                      R.drawable.box_material,
                      getString(R.string.loading))
            } else {
              binding.emptyStateLayout.horizontalProgressBar.visibility = View.VISIBLE
            }
          }
          UPassValidationViewModel.DisplayState.VERIFYING_ELIGIBILITY -> {
            binding.emptyStateLayout.showLoading(
                    R.drawable.box_material,
                    getString(R.string.loading))
          }
        }
      })

      viewModel.errorLiveData.observe(viewLifecycleOwner, {
        it?.let {
          handleUPassError(it)
        }
      })

      viewModel.institutionList.observe(viewLifecycleOwner, {
        it?.let {
          isLoading = false
          binding.emptyStateLayout.horizontalProgressBar.visibility = View.GONE
          when (it.size) {
            0 -> {
              if (pageIndex == 0) {
                showDefaultErrorDialog(getString(R.string.popup_error), getString(R.string.upass_no_associated_institution_found), false)
              } else {
                finalPageLoaded = true
              }
            }
            1 -> {
              if (pageIndex == 0) {
                viewModel.verifyEligibility(it.first().institutionId, fareMediaUuid)
              } else {
                adapter.addAll(it)
              }
            }
            else -> {
              adapter.addAll(it)
              binding.emptyStateLayout.showContent(listOf(R.id.horizontalProgressBar))
            }
          }
        } ?: showDefaultErrorDialog(null, null)
      })

      viewModel.verificationStatus.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
        it?.let {
          binding.emptyStateLayout.show(
                  drawable = R.drawable.check_material,
                  title = getString(R.string.upass_validation_success_title),
                  message = getString(R.string.upass_validation_success_message),
                  positiveButtonText = getString(R.string.autoload_done),
                  positiveActionListener = {
                    setResultAndClose(Activity.RESULT_OK)
                  }
          )
        } ?: showDefaultErrorDialog(null, null)
      })
    }
  }

  override fun showDefaultErrorDialog(title: String?, errorMsg: String?, finishActivity: Boolean) {
    binding.emptyStateLayout.show(
            State.ERROR,
            drawable = R.drawable.error_material,
            title = title ?: getString(R.string.popup_error),
            message = errorMsg ?: getString(R.string.v3_unexpected_error),
            positiveButtonText = getString(R.string.ok),
            positiveActionListener = {
              setResultAndClose()
            }
    )
  }

  private fun setResultAndClose(resultCode: Int = Activity.RESULT_CANCELED) {
    activity?.let {
      it.setResult(resultCode, Intent().apply {
        putExtra(ManageCardsActivity.FARE_MEDIUM_ID_TO_FOCUS, fareMediaId)
      })
      it.finish()
    }
  }


  private fun handleUPassError(bmError: BMError) {
    when (bmError.code) {
      BytemarkSDK.ResponseCode.ERROR_CODE_UPASS_ACCOUNT_ALREADY_LINKED -> {
        binding.emptyStateLayout.show(
                drawable = R.drawable.check_material,
                title = getString(R.string.upass_account_already_linked_title),
                message = bmError.message,
                positiveButtonText = getString(R.string.autoload_done),
                positiveActionListener = {
                  setResultAndClose(Activity.RESULT_OK)
                }
        )
      }
      BytemarkSDK.ResponseCode.ERROR_CODE_UPASS_NOT_ELIGIBLE -> {
        binding.emptyStateLayout.show(
                drawable = R.drawable.error_material,
                title = getString(R.string.upass_validation_unsuccessful_title),
                message = getString(R.string.upass_validation_not_eligible_message),
                positiveButtonText = getString(R.string.ok),
                positiveActionListener = {
                  setResultAndClose()
                }
        )
      }
      BytemarkSDK.ResponseCode.ERROR_CODE_UPASS_LINKED_ANOTHER_CARD -> {
        binding.emptyStateLayout.show(
                drawable = R.drawable.error_material,
                title = getString(R.string.upass_validation_error_title),
                message = bmError.message,
                positiveButtonText = getString(R.string.ok),
                positiveActionListener = {
                  setResultAndClose()
                }
        )
      }
      else -> {
        handleError(bmError)
      }
    }
  }

  override fun connectionErrorDialog(
          positiveTextId: Int,
          enableNegativeAction: Boolean,
          negativeTextId: Int,
          finishOnDismiss: Boolean,
          positiveAction: () -> Unit,
          negativeAction: () -> Unit
  ) {

    binding.emptyStateLayout.show(
            State.ERROR,
            drawable = R.drawable.error_material,
            title = getString(R.string.change_password_popup_conErrorTitle),
            message = getString(R.string.change_password_Popup_con_error_body),
            positiveButtonText = getString(R.string.ok),
            positiveActionListener = {
              setResultAndClose()
            }
    )
  }

  class InstitutionSelectionAdapter(
          private val selectionListener: SelectionListener<Institution>
  ) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val institutionList = mutableListOf<Institution>()

    class InstitutionHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
      val institutionName: TextView = itemView.findViewById(R.id.institutionName)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
      val view: View = LayoutInflater.from(parent.context).inflate(R.layout.upass_institution_selection_row, parent, false)
      return InstitutionHolder(view)
    }

    fun addAll(items: List<Institution>) {
      val currentCount = institutionList.size
      institutionList.addAll(items)
      notifyItemRangeInserted(currentCount, items.size)
    }

    override fun getItemCount() = institutionList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
      (holder as InstitutionHolder).apply {
        institutionName.text = institutionList[position].name
        institutionName.setOnClickListener {
          selectionListener.optionSelected(institutionList[holder.adapterPosition])
        }
      }
    }

  }

}

interface SelectionListener<T> {
  fun optionSelected(selection: T)
}