package co.bytemark.manage.upass

import android.os.Bundle
import android.transition.Slide
import co.bytemark.R
import co.bytemark.base.ToolbarActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.util.replaceFragment

class UPassValidationActivity : ToolbarActivity() {

    override fun getLayoutRes() = R.layout.activity_upass_validation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(intent?.extras?.getString(AppConstants.TITLE))
        replaceFragment(UPassValidationFragment.newInstance().apply { enterTransition = Slide() }, R.id.container)
    }
}