package co.bytemark.manage.available_passes

import androidx.lifecycle.MutableLiveData
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.fare_medium.FareMediumRequestValues
import co.bytemark.domain.interactor.fare_medium.GetFareMediumContents
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents
import co.bytemark.domain.model.fare_medium.fares.Fare
import rx.SingleSubscriber
import javax.inject.Inject

class AvailablePassesViewModel @Inject constructor() : BaseViewModel() {

    val faresLiveData: MutableLiveData<MutableList<Fare>> by lazy {
        MutableLiveData<MutableList<Fare>>()
    }

    val displayLiveData by lazy {
        MutableLiveData<Display>()
    }

    val analyticsStatusMessageLiveData by lazy {
        MutableLiveData<Pair<String, String>>()
    }

    val accessibilityLiveData by lazy {
        MutableLiveData<Boolean>()
    }

    @Inject
    lateinit var  fareMediumContentUseCase : GetFareMediumContents

    fun getFares(uuid : String) {
        displayLiveData.value =
                Display.EmptyState.Loading(R.drawable.available_passes_empty_state, R.string.loading)
        fareMediumContentUseCase.singleExecute(FareMediumRequestValues(uuid), object : SingleSubscriber<FareMediumContents?>() {


            override fun onError(error: Throwable) {
                errorLiveData.value = BMError.fromThrowable(error)
            }

            override fun onSuccess(t: FareMediumContents?) {
                 t?.fares?.let {
                     setFares(it.toMutableList())
                } ?: kotlin.run {
                     errorLiveData.value = BMError(0)
                 }
            }
        })
    }

    fun setFares(fares: MutableList<Fare>){
        if (fares.isNotEmpty()) {

            analyticsStatusMessageLiveData.value = Pair(
                    AnalyticsPlatformsContract.Status.SUCCESS,
                    ""
            )

            displayLiveData.value = Display.EmptyState.ShowContent()
            faresLiveData.value = fares

        } else {

            analyticsStatusMessageLiveData.value = Pair(
                    AnalyticsPlatformsContract.Status.SUCCESS,
                    getString(R.string.use_tickets_no_available_passes)
            )

            displayLiveData.value = Display.EmptyState
                    .ShowNoData(R.drawable.available_passes_empty_state, R.string.use_tickets_no_available_passes)

            accessibilityLiveData.value = true
        }
    }
}