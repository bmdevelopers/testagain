package co.bytemark.manage.available_passes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.helpers.AppConstants
import co.bytemark.manage.FaresAdapter
import co.bytemark.widgets.util.getViewModel
import kotlinx.android.synthetic.main.fragment_available_passes.*
import kotlinx.android.synthetic.main.fragment_available_passes.emptyStateLayout
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.notification_not_logged_in.*
import javax.inject.Inject

class AvailablePassesFragment : BaseMvvmFragment() {

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    lateinit var viewModel : AvailablePassesViewModel

    override fun onInject() = CustomerMobileApp.component.inject(this)

    companion object { fun newInstance() = AvailablePassesFragment() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        LayoutInflater.from(context).inflate(R.layout.fragment_available_passes, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val fareMedium = activity?.intent?.getParcelableExtra<FareMedium>(AppConstants.FARE_MEDIA) ?: return

        textViewFareMediaNickname.text = fareMedium.nickname
        textViewId.text = String.format(
                textViewId.context.getString(R.string.fare_medium_id),
                fareMedium.printedCardNumber
        )

        viewModel = getViewModel(CustomerMobileApp.appComponent.availablePassViewModel)
        viewModel.getFares(fareMedium.uuid)

        observeViewModelLiveData()
    }

    private fun observeViewModelLiveData() {
        observeFaresLiveData()
        observeErrorLiveData()
        observeDisplayLiveData()
        observeAnalyticsLiveData()
        observeAccessibilityLiveData()
    }

    private fun observeAnalyticsLiveData() {
        viewModel.analyticsStatusMessageLiveData.observe(this, Observer {
            analyticsPlatformAdapter.availablePassesLoaded(
                    it.first,
                    it.second
            )
        })
    }

    private fun observeAccessibilityLiveData(){
        viewModel.accessibilityLiveData.observe(this, Observer {
            emptyStateLayout?.postDelayed({
                if (activity != null) {
                    emptyStateLayout?.isFocusable = true
                    emptyStateLayout?.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
                }
            }, 5000)
        })
    }

    private fun observeFaresLiveData(){
        viewModel.faresLiveData.observe(this@AvailablePassesFragment, Observer {
            recyclerViewFares.adapter  = FaresAdapter(confHelper, it)
        })
    }

    private fun observeErrorLiveData(){
        viewModel.errorLiveData.observe(this@AvailablePassesFragment, Observer {
            it?.let { it1 -> handleError(it1) }
        })
    }

    private fun observeDisplayLiveData() {
        viewModel.displayLiveData.observe(this, Observer {
            when (it) {

                is Display.EmptyState.Error -> {
                    it.errorTextContent?.let { errorContent ->
                        emptyStateLayout.showError(
                                it.errorImageDrawable,
                                it.errorTextTitle,
                                errorContent
                        )
                    }
                }

                is Display.EmptyState.ShowNoData -> {
                    emptyStateLayout.showError(
                            it.drawable,
                            getString(it.descriptionText),
                            null,
                            getString(R.string.use_tickets_go_back)
                    ) { activity?.finish() }
                }

                is Display.EmptyState.ShowContent -> {
                    emptyStateLayout.showContent()
                }

                is Display.EmptyState.Loading -> {
                    emptyStateLayout.showLoading(it.drawable, it.title)
                }

                else -> {}
            }
        })
    }


}
