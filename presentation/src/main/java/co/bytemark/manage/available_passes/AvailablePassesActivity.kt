package co.bytemark.manage.available_passes

import android.os.Bundle
import android.transition.Slide
import co.bytemark.R
import co.bytemark.base.ToolbarActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.util.replaceFragment

class AvailablePassesActivity : ToolbarActivity() {

    override fun getLayoutRes() = R.layout.activity_available_passes

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(intent?.extras?.getString(AppConstants.TITLE))
        replaceFragment(AvailablePassesFragment.newInstance().apply { enterTransition = Slide() }, R.id.container)
    }
}
