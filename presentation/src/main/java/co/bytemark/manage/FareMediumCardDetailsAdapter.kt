package co.bytemark.manage

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.helpers.ConfHelper
import co.bytemark.manage.FareMediumCardDetailsAdapter.FareMediumCardDetailsViewHolder
import co.bytemark.sdk.model.menu.MenuItem
import kotlinx.android.synthetic.main.fare_media_card_details_item.view.*

class FareMediumCardDetailsAdapter internal constructor(
		confHelper: ConfHelper,
		callback: ItemClickCallback,
		menuItems: List<MenuItem>
) : RecyclerView.Adapter<FareMediumCardDetailsViewHolder>() {

	private var fareMediaCardDetailsList = mutableListOf<MenuItem>()
	private val confHelper: ConfHelper
	private val callback: ItemClickCallback

	init {
		setHasStableIds(true)
		this.confHelper = confHelper
		this.callback = callback
		filterAndAddMenuItem(menuItems)
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FareMediumCardDetailsViewHolder {
		return FareMediumCardDetailsViewHolder(LayoutInflater.from(parent.context)
				.inflate(R.layout.fare_media_card_details_item, parent, false))
	}

	override fun onBindViewHolder(holder: FareMediumCardDetailsViewHolder, position: Int) {
		holder.bind(fareMediaCardDetailsList[position])
	}

	override fun getItemCount(): Int {
		return fareMediaCardDetailsList.size
	}

	override fun getItemId(position: Int): Long {
		return position.toLong()
	}

	private fun filterAndAddMenuItem(menuItems: List<MenuItem>) {
		fareMediaCardDetailsList.clear()
		menuItems
				.filter { !it.shouldHide }
				.toCollection(fareMediaCardDetailsList)
	}

	fun updateMenuItem(menuItems: List<MenuItem>) {
		filterAndAddMenuItem(menuItems)
		notifyDataSetChanged()
	}

	inner class FareMediumCardDetailsViewHolder(
			view: View?
	) : RecyclerView.ViewHolder(view!!) {

		fun bind(menuItem: MenuItem) {
			with(itemView) {
				linearLayoutFareMediaCardDetailsItem.setBackgroundColor(confHelper.dataThemeBackgroundColor)
				cardDetailsItemName.setTextColor(confHelper.dataThemePrimaryTextColor)
				arrow.imageTintList = ColorStateList.valueOf(confHelper.dataThemeAccentColor)
				cardDetailsItemName.text = menuItem.title
				linearLayoutFareMediaCardDetailsItem.setOnClickListener { view: View? ->
					callback.showScreen(menuItem.action?.screen, menuItem.title)
				}
			}
		}
	}

	internal interface ItemClickCallback {
		fun showScreen(fareType: String?, title: String?)
	}

}