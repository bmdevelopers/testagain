package co.bytemark.manage.remove_physical_card

import co.bytemark.manage.block_unblock_card.BlockUnblockCardActivity
import co.bytemark.manage.block_unblock_card.BlockUnblockCardFragment

class RemovePhysicalFareMediumActivity : BlockUnblockCardActivity() {

    override fun getFragment() : BlockUnblockCardFragment {
        return RemovePhysicalFareMediumFragment.newInstance()
    }

}