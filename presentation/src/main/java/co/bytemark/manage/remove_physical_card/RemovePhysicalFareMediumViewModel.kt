package co.bytemark.manage.remove_physical_card

import android.text.SpannableStringBuilder
import co.bytemark.R
import co.bytemark.domain.interactor.fare_medium.RemoveFareMediumUseCase
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.fare_medium.FARE_MEDIUM_BLOCK_STATE
import co.bytemark.manage.block_unblock_card.BlockUnblockCardViewModel
import javax.inject.Inject

class RemovePhysicalFareMediumViewModel @Inject constructor(val removeFareMediumUseCase : RemoveFareMediumUseCase)
    : BlockUnblockCardViewModel() {

    override suspend fun performOperation(uuid: String): Result<Any> {
        val requestValue = RemoveFareMediumUseCase.RemoveFareMediumRequestValue(uuid)
        return removeFareMediumUseCase(requestValue)
    }

    override fun updateCardUiConfiguration() {
        cardUiConfigurationLiveData.value = CardStateUiConfiguration(
                newState = FARE_MEDIUM_BLOCK_STATE,
                mainIconResource = R.drawable.ic_alert,
                formattedAlertMessage = SpannableStringBuilder().apply {
                    append(getString(R.string.fare_medium_remove_card_message))
                    append(" ")
                    appendln(getString(R.string.fare_medium_remove_card_confirm))
                    appendln()
                    appendln(cardDetailFormattedText)
                },
                formattedSuccessMessage = SpannableStringBuilder().apply {
                    appendln(cardDetailFormattedText)
                    appendln()
                    append(getString(R.string.fare_medium_remove_card_success))
                }, positiveButtonText = getString(R.string.fare_medium_remove_card))
    }

}