package co.bytemark.manage.remove_physical_card

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import co.bytemark.CustomerMobileApp
import co.bytemark.manage.block_unblock_card.BlockUnblockCardFragment
import co.bytemark.manage.block_unblock_card.BlockUnblockCardViewModel
import co.bytemark.widgets.util.getViewModel
import kotlinx.android.synthetic.main.fragment_card_action.*

class RemovePhysicalFareMediumFragment : BlockUnblockCardFragment() {

    companion object {
        fun newInstance() = RemovePhysicalFareMediumFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        positiveActionButton.backgroundTintList = ColorStateList.valueOf(confHelper.indicatorsError)
    }

    override fun getViewModel() : BlockUnblockCardViewModel {
        return getViewModel(CustomerMobileApp.appComponent.removePhysicalFareMediumViewModel)
    }
}