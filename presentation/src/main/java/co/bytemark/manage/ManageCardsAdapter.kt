package co.bytemark.manage

import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Guideline
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import co.bytemark.R
import co.bytemark.domain.model.fare_medium.FARE_MEDIUM_BLOCK_STATE
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.ConfHelper
import co.bytemark.manage.ManageCardsAdapter.ManageCardsViewHolder
import co.bytemark.sdk.Api.ApiUtility
import co.bytemark.widgets.LoadingTextView
import co.bytemark.widgets.util.isOnline
import timber.log.Timber
import java.util.*

class ManageCardsAdapter constructor(
        confHelper: ConfHelper
) : RecyclerView.Adapter<ManageCardsViewHolder>() {

    private val fareMedia: MutableList<FareMedium> = ArrayList()
    private val confHelper: ConfHelper

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ManageCardsViewHolder =
            ManageCardsViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                            R.layout.manage_cards_item,
                            parent,
                            false
                    )
            )


    override fun onBindViewHolder(holder: ManageCardsViewHolder, position: Int) =
            holder.bind(fareMedia[position], confHelper)


    override fun getItemCount(): Int = fareMedia.size


    override fun getItemId(position: Int): Long =
            fareMedia[position].uuid.hashCode().toLong()


    fun addAll(fareMedia: List<FareMedium>) {
        this.fareMedia.clear()
        this.fareMedia.addAll(fareMedia)
        notifyDataSetChanged()
    }

    class ManageCardsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @JvmField
        @BindView(R.id.cardView)
        var cardView: CardView? = null

        @JvmField
        @BindView(R.id.imageViewLogo)
        var imageViewLogo: ImageView? = null

        @JvmField
        @BindView(R.id.textViewStoredValue)
        var textViewStoredValue: LoadingTextView? = null

        @JvmField
        @BindView(R.id.textViewCategory)
        var textViewCategory: TextView? = null

        @JvmField
        @BindView(R.id.cardNameTextView)
        var textViewCardName: TextView? = null

        @JvmField
        @BindView(R.id.cardNumberTextView)
        var textViewCardNumber: TextView? = null

        @JvmField
        @BindView(R.id.textViewLastUpdatedLabel)
        var textViewLastUpdatedLabel: TextView? = null

        @JvmField
        @BindView(R.id.textViewLastUpdated)
        var textViewLastUpdated: TextView? = null

        @JvmField
        @BindView(R.id.textViewTransferTimeLabel)
        var textViewTransferTimeLabel: TextView? = null

        @JvmField
        @BindView(R.id.textViewTransferTime)
        var textViewTransferTime: TextView? = null

        @JvmField
        @BindView(R.id.textViewCardStatus)
        var textViewCardStatus: TextView? = null

        @JvmField
        @BindView(R.id.dividerGuidelineBottomMost)
        var bottomGuideline: Guideline? = null

        fun bind(fareMedium: FareMedium, confHelper: ConfHelper) {
            cardView?.setCardBackgroundColor(confHelper.dataThemeBackgroundColor)
            textViewCardName?.setTextColor(confHelper.dataThemePrimaryTextColor)
            textViewCardNumber?.setTextColor(confHelper.dataThemePrimaryTextColor)
            textViewStoredValue?.setTextColor(confHelper.dataThemePrimaryTextColor)
            textViewCategory?.setTextColor(confHelper.dataThemePrimaryTextColor)
            textViewLastUpdated?.setTextColor(confHelper.dataThemePrimaryTextColor)
            textViewLastUpdatedLabel?.setTextColor(confHelper.dataThemePrimaryTextColor)
            textViewTransferTimeLabel?.setTextColor(confHelper.dataThemePrimaryTextColor)
            textViewTransferTime?.setTextColor(confHelper.dataThemePrimaryTextColor)

            val cardName = fareMedium.nickname
            textViewCardName?.text = cardName
            textViewCardName?.visibility =
                if (TextUtils.isEmpty(cardName)) View.INVISIBLE else View.VISIBLE

            textViewCardNumber?.text = String.format(
                textViewCardNumber?.context!!.getString(R.string.fare_medium_id),
                fareMedium.printedCardNumber
            )

            textViewCategory?.text = fareMedium.category

            val formattedDateAndTime = confHelper.getFormattedDateAndTime(
                fareMedium.lastDownloadTime
                    ?: 0L
            )
            if (formattedDateAndTime != null) {
                textViewLastUpdatedLabel?.visibility = View.VISIBLE
                textViewLastUpdated?.visibility = View.VISIBLE
                val fareMediaLastUpdateLabel =
                    confHelper.conf?.clients?.customerMobile?.fareMediaLastUpdateLabel
                if (fareMediaLastUpdateLabel != null) {
                    val locale = Locale.getDefault().toLanguageTag()
                    val label =
                        when {
                            fareMediaLastUpdateLabel.containsKey(locale) -> fareMediaLastUpdateLabel[locale]
                            fareMediaLastUpdateLabel.containsKey(AppConstants.DEFAULT_LOCALE_CODE) -> fareMediaLastUpdateLabel[AppConstants.DEFAULT_LOCALE_CODE]
                            else -> itemView.context.getString(R.string.fare_medium_last_update)
                        }
                    textViewLastUpdatedLabel?.text = label.plus(" ")
                } else {
                    textViewLastUpdatedLabel?.text = textViewLastUpdated?.context?.getString(
                        R.string.fare_medium_last_update
                    )
                        .plus(" ")
                }
                textViewLastUpdated?.text = formattedDateAndTime
            } else {
                textViewLastUpdated?.visibility = View.INVISIBLE
            }

            var drawable: Drawable? = null
            try {
                drawable = confHelper.getDrawableByName("fare_medium_logo")
            } catch (e: Exception) {
                Timber.e("Failure to get drawable id. %s", e.toString())
            }
            imageViewLogo?.setImageDrawable(drawable)

            val prettyAmount = fareMedium.storedValue.let { confHelper.getConfigurationPurchaseOptionsCurrencySymbol(it) }
            textViewStoredValue?.setLoadedText(prettyAmount)

            fun hideTransferTime() {
                textViewTransferTimeLabel?.visibility = View.GONE
                textViewTransferTime?.visibility = View.GONE
                bottomGuideline?.layoutParams =
                    (bottomGuideline?.layoutParams as ConstraintLayout.LayoutParams)
                        .apply { guidePercent = 0.93f }

                textViewLastUpdated?.setPadding(0, 16, 0, 0)
            }

            if (confHelper.isTransferTimeEnabled) {
                textViewTransferTimeLabel?.visibility = View.VISIBLE
                textViewTransferTime?.visibility = View.VISIBLE
                textViewTransferTimeLabel?.text = textViewTransferTimeLabel?.context?.getString(
                        R.string.fare_medium_transfer_time)
                textViewTransferTime?.text = ""

                if (textViewTransferTime?.context?.isOnline() == true) {
                    if (fareMedium.transferTime != null) {
                        val transferTime = fareMedium.transferTime
                        if (!TextUtils.isEmpty(transferTime)) {
                            val date = ApiUtility.getCalendarFromS(transferTime)?.time
                            textViewTransferTime?.text = textViewTransferTime?.context?.getString(
                                    R.string.fare_medium_transfer_time_text)
                                    .plus(" ")
                                    .plus(confHelper.timeDisplayFormat.format(date))
                        } else {
                            hideTransferTime()
                        }
                    } else {
                        hideTransferTime()
                    }
                } else {
                    textViewTransferTime?.text = textViewTransferTimeLabel?.context?.getString(
                            R.string.fare_medium_transfer_time_offline_msg)
                }
            } else {
                hideTransferTime()
            }

            textViewCardStatus?.visibility = if (fareMedium.state == FARE_MEDIUM_BLOCK_STATE)
                View.VISIBLE
            else
                View.GONE

        }

        init {
            ButterKnife.bind(this, itemView)
        }
    }

    init {
        setHasStableIds(true)
        this.confHelper = confHelper
    }
}