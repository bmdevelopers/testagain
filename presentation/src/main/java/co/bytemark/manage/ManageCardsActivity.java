package co.bytemark.manage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import butterknife.BindView;
import co.bytemark.R;
import co.bytemark.base.MasterActivity;
import co.bytemark.manage.createOrLinkVirtualCard.CreateOrLinkCardActivity;
import co.bytemark.sdk.BytemarkSDK;

import static co.bytemark.helpers.AppConstants.ADD_OR_LINK_CARD_REQUEST_CODE;
import static co.bytemark.helpers.AppConstants.MANAGE_SCREEN_REQUEST_CODES;
import static co.bytemark.sdk.model.common.Action.MANAGE;

public class ManageCardsActivity extends MasterActivity {

    public static final String FARE_MEDIUM_ID_TO_FOCUS = "fareMediumIdToFocus";
    public static final String IS_PHYSICAL_CARD_LINKED = "isPhysicalCardLinked";


    @BindView(R.id.fab)
    FloatingActionButton fab;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_manage_cards;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setNavigationViewCheckedItem(MANAGE);
        setToolbarTitle(confHelper.getNavigationMenuScreenTitleFromTheConfig(MANAGE));

        fab.setOnClickListener(view ->
                startActivityForResult(new Intent(this, CreateOrLinkCardActivity.class), ADD_OR_LINK_CARD_REQUEST_CODE));
        if (getIntent().hasExtra(FARE_MEDIUM_ID_TO_FOCUS)) {
            showMangeCardsScreen(getIntent().getStringExtra(FARE_MEDIUM_ID_TO_FOCUS), false);
        } else if (savedInstanceState == null) {
            showMangeCardsScreen(null, false);
        }
    }

    void showMangeCardsScreen(String fareMediumToFocus, boolean linkedPhysicalCard) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, ManageCardsFragment.newInstance(fareMediumToFocus, linkedPhysicalCard))
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!BytemarkSDK.isLoggedIn()) {
            fab.hide();
        } else {
            fab.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (MANAGE_SCREEN_REQUEST_CODES.contains(requestCode)
                && resultCode == Activity.RESULT_OK) {
            String fareMediumId = null;
            boolean linkedPhysicalCard = false;
            if (data != null) {
                fareMediumId = data.getStringExtra(FARE_MEDIUM_ID_TO_FOCUS);
                linkedPhysicalCard = data.getBooleanExtra(IS_PHYSICAL_CARD_LINKED, false);
            }
            showMangeCardsScreen(fareMediumId, linkedPhysicalCard);
        }
    }

    @Override
    protected boolean shouldReduceFontSizeToDefaultIfLarge() {
        return true;
    }
}
