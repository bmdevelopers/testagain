package co.bytemark.manage.block_unblock_card

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.databinding.FragmentCardActionBinding
import co.bytemark.domain.model.common.Navigate
import co.bytemark.helpers.AppConstants
import co.bytemark.manage.ManageCardsActivity
import co.bytemark.widgets.util.getViewModel

open class BlockUnblockCardFragment : BaseMvvmFragment() {

	private lateinit var binding: FragmentCardActionBinding

	companion object {
		fun newInstance() = BlockUnblockCardFragment()
	}

	lateinit var viewModelBlockUnblock: BlockUnblockCardViewModel

	override fun onInject() = CustomerMobileApp.component.inject(this)

	init {
		CustomerMobileApp.component.inject(this)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
							  savedInstanceState: Bundle?): View? {
		binding = FragmentCardActionBinding.inflate(layoutInflater, container, false)
		return binding.root
	}

	open fun getViewModel(): BlockUnblockCardViewModel {
		return getViewModel(CustomerMobileApp.appComponent.blockUnblockCardViewModel)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		viewModelBlockUnblock = getViewModel()

		with(viewModelBlockUnblock) {

			fareMedium = arguments?.getParcelable(AppConstants.FARE_MEDIA)

			errorLiveData.observe(this@BlockUnblockCardFragment, Observer {
				it?.let { handleError(it) }
			})

			isStateUpdateSuccessful.observe(this@BlockUnblockCardFragment, Observer {
				if (it == true) {
					activity?.setResult(Activity.RESULT_OK, Intent().apply {
						putExtra(ManageCardsActivity.FARE_MEDIUM_ID_TO_FOCUS, fareMedium?.fareMediumId)
					})
				}
			})

			cardUiConfigurationLiveData.observe(this@BlockUnblockCardFragment, Observer {
				binding.setLifecycleOwner(viewLifecycleOwner)
				binding.uiConfig = it
				binding.viewModel = this
			})

			navigationLiveData.observe(this@BlockUnblockCardFragment, Observer { result ->
				when (result) {
					is Navigate.FinishActivity -> {
						activity?.finish()
					}
				}
			})
		}
	}

	override fun showDefaultErrorDialog(title: String?, errorMsg: String?, finishActivity: Boolean) {
		showDialog(
						getString(R.string.fare_medium_unblock_card_failure_title),
						getString(R.string.fare_medium_unblock_card_failure_message),
						positiveText = getString(R.string.ok)
		)
	}

}