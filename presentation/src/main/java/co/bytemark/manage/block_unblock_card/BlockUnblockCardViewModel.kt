package co.bytemark.manage.block_unblock_card

import android.text.SpannableStringBuilder
import androidx.annotation.DrawableRes
import androidx.lifecycle.MutableLiveData
import androidx.core.text.bold
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.fare_medium.UpdateFareMediumStateUseCase
import co.bytemark.domain.model.common.Navigate
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.fare_medium.FARE_MEDIUM_BLOCK_STATE
import co.bytemark.domain.model.fare_medium.FARE_MEDIUM_UNBLOCK_STATE
import co.bytemark.domain.model.fare_medium.FareMedium
import kotlinx.coroutines.launch
import javax.inject.Inject

open class BlockUnblockCardViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var updateStateUseCase : UpdateFareMediumStateUseCase

    var cardUiConfigurationLiveData = MutableLiveData<CardStateUiConfiguration>()

    val navigationLiveData by lazy {
        MutableLiveData<Navigate>()
    }

    var isLoading = MutableLiveData<Boolean>().apply {
        value = false
    }

    var isStateUpdateSuccessful = MutableLiveData<Boolean>().apply {
        value = false
    }

    internal val cardDetailFormattedText
        get() = fareMedium?.let {
            SpannableStringBuilder().apply {
                bold { if (it.nickname?.isNotEmpty() == true) appendln(it.nickname) }
                append(getString(R.string.fare_medium_id_label) + " ")
                append(fareMedium!!.printedCardNumber)
            }
        } ?: SpannableStringBuilder()


    var fareMedium: FareMedium? = null
        set(value) {
            field = value
            if (value != null) {
                updateCardUiConfiguration()
            }
        }

    init {
        CustomerMobileApp.component.inject(this)
    }

    fun onDoneClick() {
        navigationLiveData.value = Navigate.FinishActivity()
    }

    fun onCancelClick() {
        navigationLiveData.value = Navigate.FinishActivity()
    }

    open fun updateCardUiConfiguration() {
        cardUiConfigurationLiveData.value = if (fareMedium?.state == FARE_MEDIUM_UNBLOCK_STATE) {
            getBlockCardActionConfiguration()
        } else {
            getUnBlockCardActionConfiguration()
        }
    }

    fun updateCardState() {
        uiScope.launch {
            if (isLoading.value == false) {
                isLoading.value = true
                fareMedium?.let {
                    isLoading.value = true
                    val result = performOperation(it.uuid)
                    handleResult(result)
                }
            }
        }
    }

    open suspend fun performOperation(uuid : String) : Result<Any> {
        val requestValue = updateStateUseCase.UpdateFareMediumRequestValue(uuid, cardUiConfigurationLiveData.value!!.newState)
        return  updateStateUseCase(requestValue)
    }

    fun handleResult(result : Result<Any>) {
        when (result) {
            is Result.Failure -> {
                errorLiveData.value = result.bmError.first()
                isLoading.value = false
            }

            else -> {
                isStateUpdateSuccessful.value = true
                isLoading.value = false
            }
        }
    }

    private fun getBlockCardActionConfiguration() = CardStateUiConfiguration(
            newState = FARE_MEDIUM_BLOCK_STATE,
            mainIconResource = R.drawable.ic_alert,
            formattedAlertMessage = SpannableStringBuilder().apply {
                appendln(getString(R.string.fare_medium_block_card_message))
                appendln(getString(R.string.fare_medium_block_card_confirm))
                appendln()
                append(cardDetailFormattedText)
            },
            formattedSuccessMessage = SpannableStringBuilder().apply {
                appendln(cardDetailFormattedText)
                appendln()
                append(getString(R.string.fare_medium_block_card_success))
            }, positiveButtonText = getString(R.string.fare_medium_block_card))

    private fun getUnBlockCardActionConfiguration() = CardStateUiConfiguration(
            newState = FARE_MEDIUM_UNBLOCK_STATE,
            mainIconResource = R.drawable.ic_unblock,
            formattedAlertMessage = SpannableStringBuilder().apply {
                appendln(getString(R.string.fare_medium_unblock_card_message))
                appendln(getString(R.string.fare_medium_unblock_card_confirm))
                appendln()
                append(cardDetailFormattedText)
            },
            formattedSuccessMessage = SpannableStringBuilder().apply {
                appendln(cardDetailFormattedText)
                appendln()
                append(getString(R.string.fare_medium_unblock_card_success))
            }, positiveButtonText = getString(R.string.fare_medium_unblock_card))


    inner class CardStateUiConfiguration(val newState: Int,
                                         @DrawableRes val mainIconResource: Int,
                                         @DrawableRes var successIconResource: Int = R.drawable.ic_success,
                                         val formattedAlertMessage: SpannableStringBuilder,
                                         val formattedSuccessMessage: SpannableStringBuilder,
                                         val positiveButtonText: String
    )

}

