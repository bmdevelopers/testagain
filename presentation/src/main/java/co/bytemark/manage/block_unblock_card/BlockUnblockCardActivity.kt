package co.bytemark.manage.block_unblock_card

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.ToolbarActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.util.replaceFragment

open class BlockUnblockCardActivity : ToolbarActivity() {

    override fun getLayoutRes() = R.layout.activity_card_action

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(intent.extras?.getString(AppConstants.TITLE))
        replaceFragment(getFragment().apply { arguments = intent.extras }, R.id.container)
    }

    open fun getFragment() : BlockUnblockCardFragment {
        return BlockUnblockCardFragment.newInstance()
    }

}