package co.bytemark.manage.createOrLinkVirtualCard

import android.os.Bundle
import androidx.fragment.app.Fragment
import co.bytemark.R
import co.bytemark.base.TabsActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.manage.createOrLinkVirtualCard.createVirtualCard.CreateVirtualCardFragment
import co.bytemark.manage.createOrLinkVirtualCard.linkExistingCard.LinkExistingCardFragment
import co.bytemark.sdk.model.common.Action

class CreateOrLinkCardActivity : TabsActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(confHelper.getNavigationMenuScreenTitleFromTheConfig(Action.MANAGE))
    }

    override fun useHamburgerMenu(): Boolean = false

    override fun getFragmentsForTabs(): List<Fragment> {
        return mutableListOf<Fragment>().apply {
            add(CreateVirtualCardFragment.newInstance().apply {
                arguments = Bundle().apply {
                    putString(
                        AppConstants.TITLE,
                        this@CreateOrLinkCardActivity.getString(R.string.fare_medium_create_virtual_card)
                    )
                }
            })
            add(LinkExistingCardFragment.newInstance().apply {
                arguments = Bundle().apply {
                    putString(
                        AppConstants.TITLE,
                        this@CreateOrLinkCardActivity.getString(R.string.fare_medium_link_existing_card)
                    )
                }
            })
        }
    }

}