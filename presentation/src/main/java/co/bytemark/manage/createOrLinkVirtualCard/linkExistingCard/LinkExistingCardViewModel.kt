package co.bytemark.manage.createOrLinkVirtualCard.linkExistingCard

import androidx.lifecycle.MutableLiveData
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.manage.LinkExistingCardUseCase
import co.bytemark.domain.interactor.manage.LinkExistingCardUseCaseValue
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.manage.AddSmartCard
import kotlinx.coroutines.launch
import javax.inject.Inject

class LinkExistingCardViewModel @Inject constructor(private val linkExistingCardUseCase: LinkExistingCardUseCase) : BaseViewModel() {

    init {
        CustomerMobileApp.component.inject(this)
    }

    val displayLiveData by lazy { MutableLiveData<Display>() }

    val linkExistingCardLiveData by lazy {
        MutableLiveData<AddSmartCard>()
    }

    fun linkExistingCard(cardNumber: String, cardNickName: String, securityCode: String) = uiScope.launch {
        displayLiveData.value = Display.EmptyState.Loading(R.drawable.tickets_material, R.string.loading)
        when (val result = linkExistingCardUseCase(LinkExistingCardUseCaseValue(cardNumber, cardNickName, securityCode))) {
            is Result.Loading -> {
                displayLiveData.value = Display.EmptyState.Loading(R.drawable.tickets_material, R.string.loading)
            }
            is Result.Success -> {
                hideLoading()
                linkExistingCardLiveData.value = result.data
            }
            is Result.Failure -> {
                errorLiveData.value = result.bmError.first()
            }
        }
    }

    fun hideLoading() {
        displayLiveData.value = Display.EmptyState.ShowContent()
    }

    fun showOfflineEmptyLayout() {
        displayLiveData.value = Display.EmptyState.Error(R.drawable.error_material, R.string.network_connectivity_error, R.string.network_connectivity_error_message, R.string.autoload_retry)
    }

    fun showSignInEmptyLayout() {
        displayLiveData.value = Display.EmptyState.Error(R.drawable.error_material, R.string.sign_in_required, R.string.sign_in_required_message)
    }
}