package co.bytemark.manage.createOrLinkVirtualCard.linkExistingCard

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.base.InfoMenuBaseFragment
import co.bytemark.domain.model.common.Display
import co.bytemark.helpers.AppConstants
import co.bytemark.manage.ManageCardsActivity
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.widgets.util.getViewModel
import co.bytemark.widgets.util.setCursorForEditTextInAccessibilityMode
import com.jakewharton.rxbinding.widget.RxTextView
import kotlinx.android.synthetic.main.fragment_link_existing_card.*
import kotlinx.android.synthetic.main.order_successful.*
import rx.Observable

class LinkExistingCardFragment : InfoMenuBaseFragment() {

	companion object {
		fun newInstance() = LinkExistingCardFragment()
	}

	override fun onInject() = component.inject(this)

	lateinit var viewModel: LinkExistingCardViewModel

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		if (confHelper.isInfoIconEnabledFor(AppConstants.SCREEN_CREATE_VIRTUAL_CARD)) {
			setHasOptionsMenu(true)
		}
	}

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? =
			inflater.inflate(R.layout.fragment_link_existing_card, container, false)

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		viewModel = getViewModel(CustomerMobileApp.appComponent.linkExistingCardViewModel)
		markRequiredField()
		handleTextChange() //To enable or disable the link-card button
		updateViewText()
		buttonLinkExistingCard.setOnClickListener { linkExistingCard() }
		observeLiveData()
		setAccessibilityOnTextInputField()
	}

	private fun observeLiveData() {
		observeErrorLiveData()
		observeDisplayLiveData()
		observeLinkExistingCardLiveData()
	}

	private fun observeLinkExistingCardLiveData() {
		viewModel.linkExistingCardLiveData.observe(this, Observer {
			showSuccessMessage()
		})
	}

	private fun observeErrorLiveData() {
		viewModel.errorLiveData.observe(this, Observer {
			it?.let {
				if (isOnline()) {
					handleError(it)
					viewModel.hideLoading()
				} else {
					viewModel.showOfflineEmptyLayout()
				}
			}
		})
	}

    private fun observeDisplayLiveData() {
        viewModel.displayLiveData.observe(this, Observer {
            when (it) {
                is Display.EmptyState.ShowContent -> {
                    emptyStateLayout?.showContent()
                    emptyStateLayout?.post {
                        emptyStateLayout?.sendAccessibilityEvent(
                            AccessibilityEvent.TYPE_VIEW_FOCUSED
                        )
                    }
                    linearLayoutOrderSuccessful?.visibility = View.GONE
                }
                is Display.EmptyState.Loading -> {
                    hideKeyboard()
                    emptyStateLayout?.requestFocus() // To fix accessibility issue
                    emptyStateLayout?.post {
                        emptyStateLayout?.sendAccessibilityEvent(
                            AccessibilityEvent.TYPE_VIEW_FOCUSED
                        )
                    }
                    emptyStateLayout?.showLoading(it.drawable, it.title)
                }
                is Display.EmptyState.Error -> {
                    emptyStateLayout?.showError(
                        it.errorImageDrawable,
                        getString(it.errorTextTitle),
                        it.errorTextContent?.let { it1 -> getString(it1) },
                        it.errorButtonText?.let { it1 -> getString(it1) }
                    ){ _: View? ->
                        if (it.errorButtonText == R.string.autoload_retry)
                            linkExistingCard()
                    }
                }
            }
        })
    }

	private fun markRequiredField() {
		textLayoutCardNumber?.hint = "${textLayoutCardNumber?.hint.toString()}*"
		textLayoutSecurityCode?.hint = "${textLayoutSecurityCode?.hint.toString()}*"
	}

	private fun handleTextChange() {
		Observable.combineLatest(
				RxTextView.textChanges(editTextCardNumber),
				RxTextView.textChanges(editTextSecurityCode)
		) { s1: CharSequence, s2: CharSequence ->
			setLinkExistingCardButtonEnabled(!(s1.isEmpty() || s2.isEmpty()))
			true
		}.subscribe()
	}

	private fun setLinkExistingCardButtonEnabled(isButtonEnabled: Boolean) {
		buttonLinkExistingCard?.apply {
			this.isEnabled = isButtonEnabled
			this.alpha = if (isButtonEnabled) 1f else 0.3f
		}
	}

	private fun updateViewText() {
		textViewOrderSuccessful1?.text = String.format(
				"%s%s",
				getString(R.string.popup_success),
				getString(R.string.receipt_exclamation)
		)
		textViewOrderSuccessful2?.text =
				getString(R.string.fare_medium_physical_card_linked_success_body)
		buttonMyTickets?.text = getString(R.string.ok)
		linearLayoutOrderSuccessTexts?.contentDescription =
				textViewOrderSuccessful1?.text.toString() + " " + textViewOrderSuccessful2?.text
	}

	override fun onResume() {
		super.onResume()
		if (!BytemarkSDK.isLoggedIn()) {
			viewModel.showSignInEmptyLayout()
		}
	}

	private fun linkExistingCard() {
		viewModel.linkExistingCard(
				editTextCardNumber?.text.toString(), editTextCardNickname?.text.toString(),
				editTextSecurityCode?.text.toString()
		)
	}

	private fun showSuccessMessage() {
		activity?.setResult(Activity.RESULT_OK)
		linearLayoutMainContent?.visibility = View.INVISIBLE
		linearLayoutOrderSuccessful?.visibility = View.VISIBLE
		linearLayoutOrderSuccessTexts?.importantForAccessibility =
				View.IMPORTANT_FOR_ACCESSIBILITY_YES
		linearLayoutOrderSuccessTexts?.announceForAccessibility(textViewOrderSuccessful1?.text)
		linearLayoutOrderSuccessTexts?.post {
			linearLayoutOrderSuccessTexts!!.sendAccessibilityEvent(
					AccessibilityEvent.TYPE_VIEW_FOCUSED
			)
		}
		buttonMyTickets?.setOnClickListener {
			// when linking physical card success, we need to send some flag, So that
			// it will get focused in the manage screen(if there is any active virtual
			// Card available, because by default the active card will come first
			// and the newly added physical card might get pushed to the 1st position)
			activity?.setResult(Activity.RESULT_OK, Intent().apply {
				putExtra(ManageCardsActivity.IS_PHYSICAL_CARD_LINKED, true)
			})
			activity?.finish()
		}
	}

	private fun setAccessibilityOnTextInputField() {
		context?.setCursorForEditTextInAccessibilityMode(
				mutableListOf(
						editTextCardNumber,
						editTextSecurityCode, editTextCardNickname
				)
		)
	}

	override fun showAboutScreenDialog() {
		val title = getString(R.string.fare_medium_link_existing_card)
		showDialog(
						getString(R.string.info_alert_title, title),
						getString(R.string.info_message_link_physical_card_screen),
						getString(R.string.ok),
		)
	}

	override fun getScreenName() = AppConstants.SCREEN_LINK_PHYSICAL_CARD
}