package co.bytemark.manage.createOrLinkVirtualCard.createVirtualCard

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import co.bytemark.CustomerMobileApp
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.base.InfoMenuBaseFragment
import co.bytemark.domain.model.common.Display
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.widgets.util.getViewModel
import co.bytemark.widgets.util.setCursorForEditTextInAccessibilityMode
import kotlinx.android.synthetic.main.fragment_create_virtual_card.*
import kotlinx.android.synthetic.main.order_successful.*
import java.util.*

class CreateVirtualCardFragment : InfoMenuBaseFragment() {

	companion object {
		fun newInstance() = CreateVirtualCardFragment()
	}

	override fun onInject() = component.inject(this)

	lateinit var viewModel: CreateVirtualCardViewModel

	private var fareCategoryId: String? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    if (confHelper.isInfoIconEnabledFor(AppConstants.SCREEN_CREATE_VIRTUAL_CARD)) {
      setHasOptionsMenu(true)
			optionsMenuEnabled = true
    }
  }

  override fun onCreateView(
          inflater: LayoutInflater,
          container: ViewGroup?,
          savedInstanceState: Bundle?
  ): View? =
          inflater.inflate(R.layout.fragment_create_virtual_card, container, false)

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		viewModel = getViewModel(CustomerMobileApp.appComponent.createVirtualCardViewModel)
		updateViewText()
		observeLiveData(view.context)
		buttonCreateVirtualCard.setOnClickListener { onCreateVirtualCardButtonClick() }
		setAccessibilityOnTextInputField()
	}

	private fun observeLiveData(context: Context) {
		observeFareMediaCategoriesLiveData(context)
		observeCreateVirtualCardLiveData()
		observeErrorLiveData()
		observeDisplayLiveData()
		observeNetworkErrorLiveData()
	}

	private fun observeNetworkErrorLiveData() {
		viewModel.networkErrorLiveData.observe(this, Observer {
			it?.let {
				emptyStateLayout?.showError(
						R.drawable.error_material, R.string.network_connectivity_error,
						R.string.network_connectivity_error_message, R.string.popup_retry
				) { _: View? ->
					when (it.second) {
						1 -> {
							loadFareMediaCategories()
						}
						2 -> {
							viewModel.createVirtualCard(
									fareCategoryId,
									editTextCardNickname?.text.toString()
							)
						}
					}
				}
			}
		})
	}

	private fun observeCreateVirtualCardLiveData() {
		viewModel.createVirtualCartLiveData.observe(this, Observer {
			showCreateVirtualCardSuccess()
		})
	}

	private fun observeDisplayLiveData() {
		viewModel.displayLiveData.observe(this, Observer {
			when (it) {
				is Display.EmptyState.ShowContent -> {
					emptyStateLayout.showContent()
					linearLayoutOrderSuccessful?.visibility = View.GONE
				}
				is Display.EmptyState.Loading -> {
					hideKeyboard()
					emptyStateLayout?.post {
						emptyStateLayout?.sendAccessibilityEvent(
								AccessibilityEvent.TYPE_VIEW_FOCUSED
						)
					}
					emptyStateLayout?.showLoading(it.drawable, it.title)
				}
			}
		})
	}

	private fun observeErrorLiveData() {
		viewModel.errorVirtualLiveData.observe(this, Observer {
			it?.let {
				if (isOnline()) {
					handleError(it.first)
					viewModel.hideLoading()
				} else {
					viewModel.showOfflineEmptyLayout(it.second)
				}
			}
		})
	}

	private fun observeFareMediaCategoriesLiveData(context: Context) {
		viewModel.fareMediaCategoriesLiveData.observe(this, Observer {
			val displayNames: MutableList<String?> = ArrayList()
			for ((_, fareCategoryName) in it!!) {
				displayNames.add(fareCategoryName)
			}
			ArrayAdapter(context, android.R.layout.simple_spinner_item, displayNames).apply {
				setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
				spinnerCategory?.adapter = this
			}
			spinnerCategory?.onItemSelectedListener = object : OnItemSelectedListener {

				override fun onItemSelected(
						parent: AdapterView<*>?,
						view: View,
						position: Int,
						id: Long
				) {
					fareCategoryId = it[position].fareMediaId
				}

				override fun onNothingSelected(parent: AdapterView<*>?) {}
			}
		})
	}

	private fun updateViewText() {
		textViewOrderSuccessful1?.text =
				String.format(
						"%s%s",
						getString(R.string.popup_success),
						getString(R.string.receipt_exclamation)
				)
		textViewOrderSuccessful2?.text = getString(R.string.fare_medium_card_added_success_body)
		buttonMyTickets?.text = getString(R.string.ok)
		linearLayoutOrderSuccessTexts?.contentDescription =
				textViewOrderSuccessful1?.text.toString() + " " + textViewOrderSuccessful2?.text
	}

	override fun onResume() {
		super.onResume()
		loadFareMediaCategories()
	}

	private fun loadFareMediaCategories() {
		if (BytemarkSDK.isLoggedIn()) {
			viewModel.getFareMediaCategories()
		} else {
			viewModel.showSignInEmptyLayout()
		}
	}

	private fun onCreateVirtualCardButtonClick() {
		viewModel.createVirtualCard(fareCategoryId, editTextCardNickname?.text.toString())
	}

	private fun showCreateVirtualCardSuccess() {
		activity?.setResult(Activity.RESULT_OK)
		linearLayoutMainContent?.visibility = View.INVISIBLE
		linearLayoutOrderSuccessful?.visibility = View.VISIBLE
		linearLayoutOrderSuccessTexts?.importantForAccessibility =
				View.IMPORTANT_FOR_ACCESSIBILITY_YES
		linearLayoutOrderSuccessTexts?.announceForAccessibility(textViewOrderSuccessful1?.text)
		linearLayoutOrderSuccessTexts?.post {
			linearLayoutOrderSuccessTexts?.sendAccessibilityEvent(
					AccessibilityEvent.TYPE_VIEW_FOCUSED
			)
		}
		buttonMyTickets?.setOnClickListener {
			activity?.setResult(Activity.RESULT_OK)
			activity?.finish()
		}
	}

	private fun setAccessibilityOnTextInputField() {
		context?.setCursorForEditTextInAccessibilityMode(mutableListOf(editTextCardNickname))
	}

  override fun showAboutScreenDialog() {
    val title = getString(R.string.fare_medium_create_virtual_card)
    showDialog(
            getString(R.string.info_alert_title, title),
            getString(R.string.info_message_create_virtual_card_screen),
            getString(R.string.ok),
    )
  }

	override fun getScreenName() = AppConstants.SCREEN_CREATE_VIRTUAL_CARD
}