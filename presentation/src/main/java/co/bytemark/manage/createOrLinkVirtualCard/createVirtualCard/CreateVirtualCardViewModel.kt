package co.bytemark.manage.createOrLinkVirtualCard.createVirtualCard

import androidx.lifecycle.MutableLiveData
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.interactor.manage.CreateVirtualCardUseCase
import co.bytemark.domain.interactor.manage.FareMediaCategoriesUseCase
import co.bytemark.domain.interactor.manage.VirtualCardUseCaseValue
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.manage.FareCategory
import kotlinx.coroutines.launch
import javax.inject.Inject

class CreateVirtualCardViewModel
@Inject constructor(private val fareMediaCategoriesUseCase: FareMediaCategoriesUseCase,
                    private val createVirtualCardUseCase: CreateVirtualCardUseCase) : BaseViewModel() {

    init {
        CustomerMobileApp.component.inject(this)
    }

    val displayLiveData by lazy { MutableLiveData<Display>() }

    val errorVirtualLiveData by lazy { MutableLiveData<Pair<BMError, Int>>() }

    val fareMediaCategoriesLiveData by lazy { MutableLiveData<MutableList<FareCategory>>() }

    val createVirtualCartLiveData by lazy { MutableLiveData<Data>() }

    val networkErrorLiveData by lazy { MutableLiveData<Pair<Display, Int>>() }

    fun getFareMediaCategories() = uiScope.launch {
        displayLiveData.value = Display.EmptyState.Loading(R.drawable.tickets_material, R.string.loading)
        when (val result = fareMediaCategoriesUseCase(UseCase.EmptyRequestValues())) {
            is Result.Success -> {
                hideLoading()
                fareMediaCategoriesLiveData.value = result.data?.fareCategories?.toMutableList()
            }
            is Result.Failure -> {
                errorVirtualLiveData.value = Pair(result.bmError.first(), 1)
            }
            is Result.Loading -> {
                displayLiveData.value = Display.EmptyState.Loading(R.drawable.tickets_material, R.string.loading)
            }
        }
    }

    fun createVirtualCard(id: String?, nickname: String?) = uiScope.launch {
        displayLiveData.value = Display.EmptyState.Loading(R.drawable.tickets_material, R.string.loading)
        when (val result = createVirtualCardUseCase(VirtualCardUseCaseValue(id, nickname))) {
            is Result.Success -> {
                hideLoading()
                createVirtualCartLiveData.value = result.data
            }
            is Result.Failure -> {
                errorVirtualLiveData.value = Pair(result.bmError.first(), 2)
            }
            is Result.Loading -> {
                displayLiveData.value = Display.EmptyState.Loading(R.drawable.tickets_material, R.string.loading)
            }
        }
    }

    // Here networkRequestType is to handle retry action on offline
    // 1 -> Retry to get FareMediaCategories
    // 2 -> Retry to createVirtualCard
    fun showOfflineEmptyLayout(networkRequestType: Int) {
        networkErrorLiveData.value = Pair(Display.EmptyState.Error(R.drawable.error_material, R.string.network_connectivity_error,
                R.string.network_connectivity_error_message), networkRequestType)
    }

    fun hideLoading() {
        displayLiveData.value = Display.EmptyState.ShowContent()
    }

    fun showSignInEmptyLayout() {
        displayLiveData.value = Display.EmptyState.Error(R.drawable.error_material, R.string.sign_in_required, R.string.sign_in_required_message)
    }

}