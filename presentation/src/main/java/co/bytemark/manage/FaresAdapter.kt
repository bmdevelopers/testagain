package co.bytemark.manage

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.data.util.Utils
import co.bytemark.domain.model.fare_medium.fares.Fare
import co.bytemark.helpers.ConfHelper
import co.bytemark.notification_settings.NotificationSettingsGroupAdapter
import co.bytemark.use_tickets.UseTicketsAccessibility
import kotlinx.android.synthetic.main.fare_item.view.*
import java.text.ParseException
import java.util.*

class FaresAdapter(private val confHelper: ConfHelper, private val fares: MutableList<Fare>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        NotificationSettingsGroupAdapter.ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.fare_item, parent, false)
        )

    override fun getItemCount() = fares.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.apply {
            with(fares[position]) {
                fareTitle.text = name
                try {
                    if (allowedUses != null) {
                        remainingUsesLL.visibility = View.VISIBLE
                        remainingUsesText.text =
                            String.format(rootView.context.getString(R.string.use_tickets_remaining_uses) + " : ")
                        remainingUsesTextValue.text = if (allowedUses == 0) {
                            rootView.context.getString(R.string.use_tickets_unlimited)
                        } else {
                            (usesCount?.let { allowedUses?.minus(it) })?.toString()
                        }
                    } else {
                        remainingUsesLL.visibility = View.GONE
                    }
                    val calendar: Calendar? = Utils.convertToCalendarFormServer(expiration)
                    if (calendar != null) {
                        expirationLayout.visibility = View.VISIBLE
                        if (isActive) {
                            fareExpiration.text = String.format(
                                fareExpiration.context
                                    .getString(R.string.available_passes_fares_expiration),
                                confHelper.dateDisplayFormat.format(calendar.time) + " " +
                                        confHelper.timeDisplayFormat.format(calendar.time)
                            )
                            fareActive.visibility = View.VISIBLE
                        } else {
                            fareActive.visibility = View.GONE
                            fareExpiration.text = String.format(
                                fareExpiration.context
                                    .getString(R.string.available_passes_fares_active_until),
                                confHelper.dateDisplayFormat.format(calendar.time) + " " +
                                        confHelper.timeDisplayFormat.format(calendar.time)
                            )
                        }
                    } else {
                        expirationLayout.visibility = View.GONE
                    }
                } catch (e: ParseException) {
                    e.printStackTrace()
                    expirationLayout.visibility = View.GONE
                }
                UseTicketsAccessibility.announceAccessibilityWithoutDoubleTapHint(fareItemLinearLayout)
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}