package co.bytemark.manage


import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import co.bytemark.sdk.model.menu.MenuGroup

class ManageCardOptionAdapter(fm: FragmentManager, private val menuGroups : List<MenuGroup>) : FragmentPagerAdapter(fm){

    override fun getItem(p0: Int)
            = ManageCardActionFragment.newInstance(ArrayList(menuGroups[p0].menuItems))

    override fun getCount()
            = menuGroups.size

    override fun getPageTitle(position: Int): String
            = menuGroups[position].header

}