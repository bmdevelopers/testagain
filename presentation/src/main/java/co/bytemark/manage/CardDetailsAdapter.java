package co.bytemark.manage;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import co.bytemark.CustomerMobileApp;
import co.bytemark.R;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.model.menu.MenuGroup;
import co.bytemark.sdk.model.menu.MenuItem;
import co.bytemark.widgets.dynamicmenu.AbstractMenuGroupAdapter;
import co.bytemark.widgets.dynamicmenu.AbstractMenuItemAdapter;
import co.bytemark.widgets.dynamicmenu.MenuClickManager;

public class CardDetailsAdapter extends AbstractMenuGroupAdapter<CardDetailsAdapter.MoreInfoMenuGroupHolder, CardDetailsAdapter.CaredDetailsItemHolder> {

    @Inject
    ConfHelper confHelper;


    @Inject
    AnalyticsPlatformAdapter analyticsPlatformAdapter;


    CardDetailsAdapter(@NonNull Context context, @NonNull MenuClickManager menuClickManager) {
        super(context, menuClickManager);
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    @Override
    public MoreInfoMenuGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_info_menu_group_item_template, parent, false);
        return new MoreInfoMenuGroupHolder(view);
    }

    @Override
    protected void onBindViewHolder(MoreInfoMenuGroupHolder holder, int position, MenuGroup menuGroup) {
        if (!TextUtils.isEmpty(menuGroup.getHeader())) {
            holder.header.setText(menuGroup.getHeader());
            holder.header.setTextColor(confHelper.getDataThemePrimaryTextColor());
        } else
            holder.header.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(menuGroup.getFooter())) {
            holder.footer.setText(menuGroup.getFooter());
            holder.footer.setTextColor(confHelper.getDataThemePrimaryTextColor());
        } else
            holder.footer.setVisibility(View.GONE);
        holder.itemView.setContentDescription(menuGroup.getHeader());
    }

    @Override
    protected AbstractMenuItemAdapter<CaredDetailsItemHolder> createMenuItemAdapter(final Context context, MenuClickManager menuClickManager, List<MenuItem> menuItems) {
        return new AbstractMenuItemAdapter<CaredDetailsItemHolder>(confHelper, context, menuItems, menuClickManager, analyticsPlatformAdapter) {
            @Override
            public CaredDetailsItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_details_item_template, parent, false);
                return new CaredDetailsItemHolder(view);
            }

            @Override
            public void onBindViewHolder(CaredDetailsItemHolder holder, int position, MenuItem menuItem) {
                holder.itemView.setContentDescription(menuItem.getTitle());
                holder.llMenuItemRoot.setBackgroundColor(confHelper.getDataThemeBackgroundColor());

                holder.singleText.setVisibility(View.VISIBLE);
                holder.singleText.setText(menuItem.getTitle());
                holder.singleText.setTextColor(confHelper.getDataThemePrimaryTextColor());
            }
        };
    }

    static class MoreInfoMenuGroupHolder extends AbstractMenuGroupAdapter.AbstractMenuGroupHolder {

        @BindView(R.id.menuGroupHeaderTextView)
        TextView header;
        @BindView(R.id.menuGroupFooterTextView)
        TextView footer;

        MoreInfoMenuGroupHolder(View itemView) {
            super(itemView);
        }
    }

    static class CaredDetailsItemHolder extends AbstractMenuItemAdapter.AbstractMenuItemHolder {

        @BindView(R.id.ll_menu_item_root)
        ConstraintLayout llMenuItemRoot;
        @BindView(R.id.textViewTitle)
        TextView singleText;

        CaredDetailsItemHolder(View itemView) {
            super(itemView);
        }
    }
}
