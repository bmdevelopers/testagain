package co.bytemark.manage;


import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import co.bytemark.domain.error.BytemarkException;
import co.bytemark.domain.interactor.fare_medium.FareMediumRequestValues;
import co.bytemark.domain.interactor.fare_medium.GetFareMedia;
import co.bytemark.domain.interactor.fare_medium.GetFareMediumContents;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.helpers.RxUtils;
import co.bytemark.mvp.MvpBasePresenter;
import co.bytemark.mvp.MvpView;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.model.menu.MenuGroup;
import co.bytemark.securityquestion.UserSecurityQuestionChecker;
import rx.Observable;
import rx.SingleSubscriber;
import timber.log.Timber;

import static co.bytemark.sdk.BytemarkSDK.ResponseCode.UNEXPECTED_ERROR;

public interface ManageCards {

    interface View extends MvpView {

        void showLoading();

        void hideLoading();

        void showNoFaresView();

        void setFareMedia(List<FareMedium> fareMedia);

        void updateFareMedium(FareMedium fareMedium);

        void showError(String userFacingMessage);

        void showLoadFareMediaNetworkError();

        void setMenuGroups(List<MenuGroup> menuGroups);

        void enforceSecurityQuestions();
    }

    class Presenter extends MvpBasePresenter<View> {

        private final RxUtils rxUtils;
        private final ConfHelper confHelper;
        private final GetFareMedia getFareMedia;
        private final GetFareMediumContents getFareMediumContents;
        private final UserSecurityQuestionChecker userSecurityQuestionChecker;
        private boolean isSecurityQuestionsDisplayed;

        @Inject
        Presenter(RxUtils rxUtils,
                  ConfHelper confHelper,
                  GetFareMedia getFareMedia,
                  GetFareMediumContents getFareMediumContents,
                  UserSecurityQuestionChecker userSecurityQuestionChecker) {
            this.rxUtils = rxUtils;
            this.confHelper = confHelper;
            this.getFareMedia = getFareMedia;
            this.getFareMediumContents = getFareMediumContents;
            this.userSecurityQuestionChecker = userSecurityQuestionChecker;
        }

        void loadFareMediumCardDetails() {
            Observable.fromCallable(confHelper::getFareMediumCardDetails)
                    .compose(rxUtils.applySchedulers())
                    .map(menuList -> {
                        if (BytemarkSDK.isLoggedIn() && menuList.getSignedInMenuGroups() != null && !menuList.getSignedInMenuGroups().isEmpty()) {
                            return menuList.getSignedInMenuGroups();
                        } else if (menuList.getSignedOutMenuGroups() != null && !menuList.getSignedOutMenuGroups().isEmpty())
                            return menuList.getSignedOutMenuGroups();
                        else
                            return menuList.getMenuGroups();
                    })
                    .toSingle()
                    .subscribe(new SingleSubscriber<List<MenuGroup>>() {
                        @Override
                        public void onSuccess(List<MenuGroup> menuGroups) {
                            if (isViewAttached()) {
                                getView().setMenuGroups(menuGroups);
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            Timber.e(error.getMessage());
                        }
                    });
        }

        void loadFareMedia() {
            getFareMedia.singleExecute(null, new SingleSubscriber<List<FareMedium>>() {
                @Override
                public void onSuccess(List<FareMedium> fareMedia) {
                    if (isViewAttached()) {
                        if (!fareMedia.isEmpty()) {
                            getView().hideLoading();
                            getView().setFareMedia(fareMedia);
                        } else {
                            getView().hideLoading();
                            getView().showNoFaresView();
                        }
                    }
                }

                @Override
                public void onError(Throwable error) {
                    if (isViewAttached()) {
                        getView().hideLoading();
                        if (error instanceof BytemarkException) {
                            getView().showError(((BytemarkException) error).getUserFacingMessage());
                        } else if (error instanceof IOException) {
                            getView().showLoadFareMediaNetworkError();
                        } else {
                            getView().showError(BytemarkException.fromStatusCode(UNEXPECTED_ERROR).getUserFacingMessage());
                        }
                    }
                }
            });
        }

        public void checkIfSecurityQuestionsAreAnswered() {
            userSecurityQuestionChecker.checkIfEmpty(() -> {
                if (getView() != null && !isSecurityQuestionsDisplayed) {
                    isSecurityQuestionsDisplayed = true;
                    getView().enforceSecurityQuestions();
                }
            });
        }

        private void loadFareMediaContents(List<FareMedium> fareMedia) {
            for (FareMedium fareMedium : fareMedia) {
                getFareMediumContents.singleExecute(new FareMediumRequestValues(fareMedium.getUuid()), new SingleSubscriber<FareMediumContents>() {
                    @Override
                    public void onSuccess(FareMediumContents fareMediumContents) {
                        fareMedium.setFareMediumContents(fareMediumContents);
                        if (isViewAttached()) {
                            getView().updateFareMedium(fareMedium);
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        // no ops
                    }
                });
            }
        }

        void cancelRequests() {
            getFareMedia.unsubscribe();
            getFareMediumContents.unsubscribe();
        }
    }
}
