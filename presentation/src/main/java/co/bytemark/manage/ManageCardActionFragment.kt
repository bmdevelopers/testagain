package co.bytemark.manage

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.autoload.AutoloadActivity
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.card_history.CardHistoryActivity
import co.bytemark.domain.model.fare_medium.FARE_MEDIUM_BLOCK_STATE
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.fare_medium.TYPE_VIRTUAL_CARD
import co.bytemark.domain.model.fare_medium.UNLIMITED
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.AppConstants.BLOCK_UNBLOCK_CARD_REQUEST_CODE
import co.bytemark.helpers.AppConstants.FARE_MEDIA
import co.bytemark.helpers.AppConstants.REMOVE_PHYSICAL_CARD_REQUEST_CODE
import co.bytemark.helpers.AppConstants.TITLE
import co.bytemark.helpers.AppConstants.TRANSFER_BALANCE_REQUEST_CODE
import co.bytemark.helpers.AppConstants.UPASS_VALIDATION_REQUEST_CODE
import co.bytemark.init_fare_capping.InitFareCappingActivity
import co.bytemark.manage.FareMediumCardDetailsAdapter.ItemClickCallback
import co.bytemark.manage.available_passes.AvailablePassesActivity
import co.bytemark.manage.block_unblock_card.BlockUnblockCardActivity
import co.bytemark.manage.remove_physical_card.RemovePhysicalFareMediumActivity
import co.bytemark.manage.upass.UPassValidationActivity
import co.bytemark.sdk.model.common.Action
import co.bytemark.sdk.model.menu.MenuItem
import co.bytemark.subscriptions.ListSubscriptionsActivity
import co.bytemark.transfer_balance.TransferBalanceActivity
import co.bytemark.transfer_virtual_card.TransferVirtualCardActivity
import co.bytemark.widgets.util.showMaterialDialog
import kotlinx.android.synthetic.main.fragment_manage_options.*
import rx.subscriptions.CompositeSubscription
import timber.log.Timber
import java.util.*

class ManageCardActionFragment : BaseMvvmFragment(), ItemClickCallback {

    companion object {

        private const val BUNDLE_KEY_FARE_MEDIUM_OPTIONS = "fare_medium_menu_options"

        fun newInstance(menuItems: ArrayList<MenuItem>) = ManageCardActionFragment().apply {
            arguments = Bundle().apply {
                putParcelableArrayList(BUNDLE_KEY_FARE_MEDIUM_OPTIONS, menuItems)
            }
        }
    }

    private lateinit var menuItems: List<MenuItem>

    private var fareMedium: FareMedium? = null

    private var fareMediumCount: Int = 0

    private var subscriptions: CompositeSubscription = CompositeSubscription()

    override fun onInject() {
        CustomerMobileApp.appComponent.inject(this)
    }

    init {
        subscriptions.add(
            ManageCardsFragment.fareMediumPublishSubject?.subscribe(
                {
                    this.fareMedium = it
                    updateCardActions(it)
                },
                {
                    Timber.e(it)
                }
            )
        )
    }

    private fun updateCardActions(fareMedium: FareMedium) {
        // checking UPass status to hide "Are you Student option"
        menuItems.forEach {
            it.shouldHide =
                (it.action?.screen == Action.UPASS_VALIDATION && fareMedium.institutionAccountId != null) ||
                        (it.action?.screen == Action.TRANSFER_VIRTUAL_CARD && fareMedium.type == TYPE_VIRTUAL_CARD && fareMedium.activeFareMedium == true)
        }
        (recyclerView.adapter as FareMediumCardDetailsAdapter).updateMenuItem(menuItems)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        LayoutInflater.from(context).inflate(R.layout.fragment_manage_options, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        menuItems = arguments?.getParcelableArrayList(BUNDLE_KEY_FARE_MEDIUM_OPTIONS) ?: ArrayList()
        recyclerView.adapter = FareMediumCardDetailsAdapter(confHelper, this, menuItems)

        subscriptions.add(
            ManageCardsFragment.fareMediumCountSubject?.subscribe(
                {
                    this.fareMediumCount = it
                },
                {
                    Timber.e(it)
                }
            )
        )
    }

    override fun showScreen(fareType: String?, title: String?) {
        if (activity == null || fareMedium == null) return
        when (fareType) {
            Action.AUTOLOADS -> {
                if (!isOnline()) {
                    connectionErrorDialog { }
                    return
                }
                restrictActionIfCardIsBlocked {
                    val intent = Intent(activity, AutoloadActivity::class.java)
                    intent.putExtra(AppConstants.FARE_MEDIA_ID, fareMedium!!.fareMediumId)
                    intent.putExtra(TITLE, title)
                    activity?.startActivity(intent)
                }
            }
            Action.AVAILABLE_PASSES -> {
                val intent = Intent(activity, AvailablePassesActivity::class.java)
                intent.putExtra(FARE_MEDIA, fareMedium)
                intent.putExtra(TITLE, title)
                activity?.startActivity(intent)
            }
            Action.CARD_HISTORY -> {
                val intent = Intent(activity, CardHistoryActivity::class.java)
                intent.putExtra(FARE_MEDIA, fareMedium)
                intent.putExtra(TITLE, title)
                activity?.startActivity(intent)
            }
            Action.INIT_FARE_CAPPING -> {
                val intent = Intent(activity, InitFareCappingActivity::class.java)
                intent.putExtra(FARE_MEDIA, fareMedium)
                intent.putExtra(TITLE, title)
                activity?.startActivity(intent)
            }
            Action.PASS_AUTO_RENEWALS -> {
                restrictActionIfCardIsBlocked {
                    val intent = Intent(activity, ListSubscriptionsActivity::class.java)
                    intent.putExtra(AppConstants.FARE_MEDIA_ID, fareMedium!!.fareMediumId)
                    intent.putExtra(TITLE, title)
                    activity?.startActivity(intent)
                }
            }
            Action.TRANSFER_BALANCE -> {
                restrictActionIfCardIsBlocked {
                    restrictIfSingleCard {
                        restrictIfZeroBalance {
                            val intent = Intent(activity, TransferBalanceActivity::class.java)
                            intent.putExtra(FARE_MEDIA, fareMedium)
                            intent.putExtra(TITLE, title)
                            activity?.startActivityForResult(intent, TRANSFER_BALANCE_REQUEST_CODE)
                        }
                    }
                }
            }
            Action.UPASS_VALIDATION -> {
                restrictActionIfCardIsBlocked {
                    fareMedium?.let {
                        val intent = Intent(activity, UPassValidationActivity::class.java)
                        intent.putExtra(FARE_MEDIA, it)
                        intent.putExtra(TITLE, title)
                        activity?.startActivityForResult(intent, UPASS_VALIDATION_REQUEST_CODE)
                    }
                }
            }
            Action.BLOCK_UNBLOCK_CARD -> {
                val intent = Intent(activity, BlockUnblockCardActivity::class.java)
                intent.putExtra(FARE_MEDIA, fareMedium)
                intent.putExtra(TITLE, title)
                activity?.startActivityForResult(intent, BLOCK_UNBLOCK_CARD_REQUEST_CODE)
            }
            Action.REMOVE_PHYSICAL_CARD -> {
                val intent = Intent(activity, RemovePhysicalFareMediumActivity::class.java)
                intent.putExtra(FARE_MEDIA, fareMedium)
                intent.putExtra(TITLE, title)
                activity?.startActivityForResult(intent, REMOVE_PHYSICAL_CARD_REQUEST_CODE)
            }
            Action.TRANSFER_VIRTUAL_CARD -> {
                restrictIfRemainingTransferCountZero {
                    fareMedium?.let {
                        val intent = Intent(activity, TransferVirtualCardActivity::class.java)
                        intent.putExtra(AppConstants.FARE_MEDIA_ID, it.fareMediumId)
                        activity?.startActivityForResult(intent, REMOVE_PHYSICAL_CARD_REQUEST_CODE)
                    }
                }
            }
        }
    }

    fun hideUPassOptionTemporarily() {
        // As we are passing the same object via Rx Subject setting this,
        // will work temporarily until the next refresh
        fareMedium?.let {
            it.institutionAccountId = ""
            updateCardActions(it)
        }
    }

    private fun restrictIfZeroBalance(action: () -> Unit) {
        if (fareMedium?.storedValue == 0) {
            context.showMaterialDialog(
                title = R.string.transfer_balance_fare_medium_zero_balance_title,
                body = R.string.transfer_balance_fare_medium_zero_balance_message,
                positiveButton = R.string.ok
            )
            return
        }
        action()
    }

    private fun restrictIfRemainingTransferCountZero(action: () -> Unit) {
        if (fareMedium?.remainingTransferCount == 0) {
            context?.let { context ->
                val message = StringBuffer()
                message.append(context.getString(R.string.transfer_virtual_card_transfer_restricted_message_1))
                if (fareMedium?.nextResetInDays != UNLIMITED) {
                    message.append(
                        context.getString(
                            R.string.transfer_virtual_card_transfer_restricted_message_2,
                            context.resources.getQuantityString(
                                R.plurals.transfer_virtual_card_note_transfer_period,
                                fareMedium!!.nextResetInDays,
                                fareMedium!!.nextResetInDays
                            )
                        )
                    )
                }
                showDialog(
                    title = getString(R.string.transfer_virtual_card_to_device),
                    content = message.toString(),
                    positiveText = getString(R.string.ok),
                )
            }
            return
        }
        action()
    }

    private fun restrictActionIfCardIsBlocked(action: () -> Unit) {
        if (fareMedium?.state == FARE_MEDIUM_BLOCK_STATE) {
            context.showMaterialDialog(
                title = R.string.fare_medium_your_card_is_blocked_title,
                body = R.string.fare_medium_your_card_is_blocked_body,
                positiveButton = R.string.ok
            )
            return
        }
        action()
    }

    private fun restrictIfSingleCard(action: () -> Unit) {
        if (fareMediumCount < 2) {
            context.showMaterialDialog(
                title = R.string.popup_error,
                body = R.string.transfer_balance_one_faremedium_error,
                positiveButton = R.string.ok
            )
            return
        }
        action()
    }

    override fun onDestroy() {
        super.onDestroy()
        subscriptions.unsubscribe()
    }
}
