package co.bytemark.manage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.viewpager.widget.ViewPager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import co.bytemark.CustomerMobileApp;
import co.bytemark.MainActivity;
import co.bytemark.R;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.analytics.AnalyticsPlatformsContract;
import co.bytemark.authentication.AuthenticationActivity;
import co.bytemark.base.BaseMvpFragment;
import co.bytemark.buy_tickets.BuyTicketsActivity;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.helpers.AppConstants;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.manage.createOrLinkVirtualCard.CreateOrLinkCardActivity;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.model.menu.MenuGroup;
import co.bytemark.sdk.model.menu.MenuItem;
import co.bytemark.securityquestion.SecurityQuestionActivity;
import co.bytemark.shopping_cart.AcceptPaymentActivity;
import co.bytemark.widgets.IndefinitePagerIndicator;
import co.bytemark.widgets.dynamicmenu.MenuClickManager;
import co.bytemark.widgets.emptystateview.EmptyStateLayout;
import co.bytemark.widgets.util.Util;
import kotlin.Unit;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

import static android.view.accessibility.AccessibilityEvent.TYPE_VIEW_FOCUSED;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_DRAGGING;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_SETTLING;
import static co.bytemark.base.MasterActivity.REQUEST_CODE_LOGIN;
import static co.bytemark.domain.model.fare_medium.FareMediumKt.FARE_MEDIUM_BLOCK_STATE;
import static co.bytemark.manage.ManageCardsActivity.FARE_MEDIUM_ID_TO_FOCUS;
import static co.bytemark.manage.ManageCardsActivity.IS_PHYSICAL_CARD_LINKED;

public class ManageCardsFragment extends BaseMvpFragment<ManageCards.View, ManageCards.Presenter>
        implements ManageCards.View, MenuClickManager, AccessibilityManager.AccessibilityStateChangeListener {

    @BindView(R.id.emptyStateLayout)
    EmptyStateLayout emptyStateLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.indefinitePagerIndicator)
    IndefinitePagerIndicator indefinitePagerIndicator;
    @BindView(R.id.cardViewLoadMoney)
    CardView cardViewLoadMoney;
    @BindView(R.id.cardViewAddProducts)
    CardView cardViewAddProducts;
    @BindView(R.id.manageCardBottomViewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.textViewCardDetails)
    TextView textViewCardDetails;
    @BindView(R.id.imageViewAddPasses)
    ImageView imageViewAddPasses;
    @BindView(R.id.imageViewLoadMoney)
    ImageView imageViewLoadMoney;
    @BindView(R.id.leftArrow)
    AppCompatImageView leftArrow;
    @BindView(R.id.rightArrow)
    AppCompatImageView rightArrow;


    @Inject
    ManageCards.Presenter presenter;
    @Inject
    ConfHelper confHelper;

    @Inject
    AnalyticsPlatformAdapter analyticsPlatformAdapter;

    private ManageCardsAdapter manageCardsAdapter;

    private List<FareMedium> fareMedia = new ArrayList<>();

    private FareMedium fareMedium;

    private Boolean isScrollEnabled = true;

    private int position = 0;

    //To track the position of the current visible item .for navigation with arrows
    private int currentVisibleItem = 0;

    private String fareMediumIdToFocus = null;
    private boolean focusPhysicalCard = false;

    public ManageCardsFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public ManageCards.Presenter createPresenter() {
        return presenter;
    }

    public static PublishSubject<FareMedium> fareMediumPublishSubject = PublishSubject.create();
    public static BehaviorSubject<Integer> fareMediumCountSubject = BehaviorSubject.create();

    public static ManageCardsFragment newInstance(String fareMediumIdToFocus, boolean linkedPhysicalCard) {
        Bundle args = new Bundle();
        args.putString(FARE_MEDIUM_ID_TO_FOCUS, fareMediumIdToFocus);
        args.putBoolean(IS_PHYSICAL_CARD_LINKED, linkedPhysicalCard);
        ManageCardsFragment fragment = new ManageCardsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fareMediumIdToFocus = getArguments().getString(FARE_MEDIUM_ID_TO_FOCUS, null);
            focusPhysicalCard = getArguments().getBoolean(IS_PHYSICAL_CARD_LINKED, false);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        manageCardsAdapter = new ManageCardsAdapter(confHelper);
        recyclerView.setAdapter(manageCardsAdapter);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);
        indefinitePagerIndicator.attachToRecyclerView(recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext()) {
            @Override
            public boolean canScrollHorizontally() {
                return isScrollEnabled;
            }
        };
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        talkBackListenerEnabled(true);
        setRecyclerViewOnScroll();
    }

    private void talkBackListenerEnabled(Boolean isEnabled) {
        if (getContext() != null) {
            AccessibilityManager manager = (AccessibilityManager) this.getContext().getSystemService(Context.ACCESSIBILITY_SERVICE);
            if (isEnabled) {
                manager.addAccessibilityStateChangeListener(this);
                onAccessibilityStateChanged(Util.isTalkBackAccessibilityEnabled(getContext()));
            } else {
                manager.removeAccessibilityStateChangeListener(this);
            }
        }
    }

    private void shallScrollWhenAccessibilityIsOn(boolean shallScroll) {
        //Enforce scroll enable/disable, To allow talk-back user navigate to options like add passes,load money etc
        if ((getContext() != null && Util.isTalkBackAccessibilityEnabled(getContext()))) {
            isScrollEnabled = shallScroll;
        } else {
            isScrollEnabled = true; //Talk-back is off, scroll always
        }
    }

    private void setRecyclerViewOnScroll() {
        handleWritingViewNavigationArrows(false);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case SCROLL_STATE_DRAGGING:
                    case SCROLL_STATE_IDLE:
                    case SCROLL_STATE_SETTLING:
                        //Indicated that user scrolled.
                        currentVisibleItem = position;
                        handleWritingViewNavigationArrows(false);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_manage_cards_new;
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.cancelRequests();
    }

    private void loadFareMedia() {
        if (BytemarkSDK.isLoggedIn()) {
            showLoading();
            presenter.loadFareMediumCardDetails();// Load Bottom Menu Options
            presenter.loadFareMedia();//Get All Fare Mediums
            presenter.checkIfSecurityQuestionsAreAnswered();//Check if Security Questions are answered ?
        } else {
            analyticsPlatformAdapter.manageCardsScreenOpened(AnalyticsPlatformsContract.Status.SUCCESS, getString(R.string.you_are_signed_out));
            emptyStateLayout.showError(R.drawable.ic_enter_filled, R.string.you_are_signed_out,
                    R.string.use_tickets_popup_signin_to_view_your_ticket, R.string.settings_sign_in, v -> {
                        if (getActivity() == null) return Unit.INSTANCE;
                        if (confHelper.isAuthenticationNative()) {
                            Intent intent = new Intent(getActivity(), AuthenticationActivity.class);
                            intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.MANAGE_CARD);
                            startActivityForResult(intent, REQUEST_CODE_LOGIN);
                        } else {
                            if (isOnline())
                                getActivity().startActivityForResult(new Intent(getActivity(), MainActivity.class), REQUEST_CODE_LOGIN);
                            else
                                Snackbar.make(emptyStateLayout, R.string.network_connectivity_error_message, Snackbar.LENGTH_LONG).show();
                        }
                        return Unit.INSTANCE;
                    });
        }
    }

    @Override
    protected void onInject() {
        CustomerMobileApp.Companion.getComponent().inject(this);
    }

    @Override
    protected void onOnline() {
        loadFareMedia();
    }

    @Override
    protected void onOffline() {
        if (fareMedia.isEmpty()) loadFareMedia();
        manageCardsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void setAccentThemeColors() {
        imageViewAddPasses.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
        imageViewLoadMoney.setImageTintList(ColorStateList.valueOf(confHelper.getBackgroundThemeAccentColor()));
    }

    @Override
    protected void setBackgroundThemeColors() {
    }

    @Override
    protected void setDataThemeColors() {

    }

    @Override
    public void setMenuGroups(List<MenuGroup> menuGroups) {
        if (!menuGroups.isEmpty()) {
            tabLayout.setTabMode(menuGroups.size() == 1 ? TabLayout.MODE_SCROLLABLE : TabLayout.MODE_FIXED);
            viewPager.setAdapter(new ManageCardOptionAdapter(getChildFragmentManager(), menuGroups));
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    @Override
    public void enforceSecurityQuestions() {
        startActivity(new Intent(getContext(), SecurityQuestionActivity.class));
    }

    @Override
    public void showLoading() {
        hideKeyboard();
        emptyStateLayout.showLoading(R.drawable.available_passes_empty_state, R.string.loading);
    }

    @Override
    public void hideLoading() {
        emptyStateLayout.showContent();
    }

    @Override
    public void showNoFaresView() {
        if (getActivity() == null) return;
        analyticsPlatformAdapter.manageCardsScreenOpened(AnalyticsPlatformsContract.Status.SUCCESS,
                getString(R.string.fare_medium_you_have_no_cards_title));
        emptyStateLayout.showError(R.drawable.available_passes_empty_state, getString(R.string.fare_medium_you_have_no_cards_title),
                null, getString(R.string.fare_medium_create_or_add_card), v -> {
                    startActivityForResult(new Intent(getActivity(), CreateOrLinkCardActivity.class), AppConstants.ADD_OR_LINK_CARD_REQUEST_CODE);
                    return Unit.INSTANCE;
                });
        new Handler().postDelayed(() -> {
            if (getActivity() == null) return;
            emptyStateLayout.announceForAccessibility(getString(R.string.fare_medium_you_have_no_cards_title));
            emptyStateLayout.setFocusable(true);
            emptyStateLayout.sendAccessibilityEvent(TYPE_VIEW_FOCUSED);
        }, 5000);
    }

    @Override
    public void setFareMedia(List<FareMedium> fareMedia) {
        // Current FareMedium Order from server is
        // Active Virtual card >> available physical cards >> available virtual cards
        // in recently created / linked order

        this.fareMedia = fareMedia;
        hideLoading();
        fareMediumCountSubject.onNext(fareMedia.size());
        manageCardsAdapter.addAll(fareMedia);
        indefinitePagerIndicator.setListener(this::setFares);
        analyticsPlatformAdapter.manageCardsScreenOpened(AnalyticsPlatformsContract.Status.SUCCESS, "");

        currentVisibleItem = 0;

        if (fareMediumIdToFocus != null) {
            for (int i = 0; i < fareMedia.size(); i++) {
                if (fareMedia.get(i).getFareMediumId().equals(fareMediumIdToFocus)) {
                    currentVisibleItem = i;
                    break;
                }
            }
        }

        // if new physical card is linked we need to move focus to that when there is active virtual card
        // if there is no active VC, then the newly added physical card will be in 0th position
        // here we are checking if the 1st cards is active or not. if active we need to focus the 2nd card,
        // because that is where the newly added physical card will be.
        if (focusPhysicalCard && fareMedia.size() > 0) {
            FareMedium fareMedium = fareMedia.get(0);
            if (fareMedium.getActiveFareMedium() != null && fareMedium.getActiveFareMedium()) {
                currentVisibleItem = 1;
            }
        }
        handleWritingViewNavigationArrows(true);
    }

    private void setFares(int position) {
        this.position = position;
        fareMedium = fareMedia.get(position);
        fareMediumPublishSubject.onNext(fareMedium);
    }

    @Override
    public void updateFareMedium(FareMedium fareMedium) {
        for (int i = 0; i < fareMedia.size(); i++) {
            if (fareMedia.get(i).getUuid().equalsIgnoreCase(fareMedium.getUuid())) {
                this.fareMedia.remove(i);
                this.fareMedia.add(i, fareMedium);
            }
        }
        manageCardsAdapter.addAll(fareMedia);
        setFares(position);
    }

    @Override
    public void showError(String userFacingMessage) {
        analyticsPlatformAdapter.manageCardsScreenOpened(AnalyticsPlatformsContract.Status.FAILURE,
                userFacingMessage);
        new MaterialDialog.Builder(getContext())
                .title(R.string.popup_error)
                .content(userFacingMessage)
                .positiveText(R.string.ok)
                .positiveColor(confHelper.getDataThemeAccentColor())
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void showLoadFareMediaNetworkError() {
        analyticsPlatformAdapter.manageCardsScreenOpened(AnalyticsPlatformsContract.Status.FAILURE,
                getString(R.string.network_connectivity_error));
        emptyStateLayout.showError(R.drawable.error_material, R.string.network_connectivity_error,
                R.string.network_connectivity_error_message, R.string.popup_retry, v -> {
                    loadFareMedia();
                    return Unit.INSTANCE;
                });
    }

    @OnClick(R.id.cardViewLoadMoney)
    public void onCardViewLoadMoneyClick() {
        if (getActivity() == null) return;

        if (fareMedium.getState() == FARE_MEDIUM_BLOCK_STATE) {
            showActionNotAllowedDialog();
            return;
        }

        final Intent intent = new Intent(getActivity(), AcceptPaymentActivity.class);
        intent.putExtra(AppConstants.RELOAD, true);
        intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.MANAGE_CARD);
        intent.putExtra("fareMediumId", fareMedium.getFareMediumId());
        getActivity().startActivityForResult(intent, AppConstants.FAREMEDIUM_LOAD_MONEY_REQUEST_CODE);
    }

    private void showActionNotAllowedDialog() {
        showInfoDialog(R.string.fare_medium_your_card_is_blocked_title,
                R.string.fare_medium_your_card_is_blocked_body,
                R.string.ok);
    }

    @OnClick(R.id.cardViewAddProducts)
    public void onCardViewAddProductsClick() {
        if (getActivity() == null) return;

        analyticsPlatformAdapter.addPassesToFareMediumSelected();

        if (fareMedium.getState() == FARE_MEDIUM_BLOCK_STATE) {
            showActionNotAllowedDialog();
            return;
        }

        final Intent intent = new Intent(getActivity(), BuyTicketsActivity.class);
        intent.putExtra(AppConstants.RELOAD, false);
        intent.putExtra(AppConstants.EXTRA_ORIGIN, AnalyticsPlatformsContract.Screen.MANAGE_CARD);
        intent.putExtra("fareMediumId", fareMedium.getFareMediumId());
        getActivity().startActivity(intent);
    }


    /**
     * Handles the arrows visibility based on current visible items and scrolls the
     * current visibility item based on param scroll.
     * <p>
     * Scroll - is False if User scrolls the Reycler Manually
     * - is True means User used arrows to navigate
     *
     * @param scroll
     */
    private void handleWritingViewNavigationArrows(boolean scroll) {

        if (recyclerView.getAdapter().getItemCount() > 1) {

            if (currentVisibleItem == 0) { //Oth Position
                rightArrow.setVisibility(View.VISIBLE);
                leftArrow.setVisibility(View.INVISIBLE);
            } else if (currentVisibleItem == (recyclerView.getAdapter().getItemCount() - 1)) { //Last Position
                rightArrow.setVisibility(View.INVISIBLE);
                leftArrow.setVisibility(View.VISIBLE);
            } else { //Somewhere in middle
                rightArrow.setVisibility(View.VISIBLE);
                leftArrow.setVisibility(View.VISIBLE);
            }
        } else {
            rightArrow.setVisibility(View.INVISIBLE);
            leftArrow.setVisibility(View.INVISIBLE);
        }

        if (scroll) {
            try {
                shallScrollWhenAccessibilityIsOn(true);
                recyclerView.scrollToPosition(currentVisibleItem);
                shallScrollWhenAccessibilityIsOn(false);
                recyclerView.post(() -> {
                    if (recyclerView.getLayoutManager() != null) {
                        View view = recyclerView.getLayoutManager().findViewByPosition(currentVisibleItem);
                        if (view != null) {
                            view.requestFocus();
                        }
                    }
                });
            } catch (Exception e) {
                // no ops
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        talkBackListenerEnabled(false);
    }

    @OnClick(R.id.leftArrow)
    public void leftArrowClicked() {
        //Decrement current visible item position to navigate back to previous item
        currentVisibleItem--;
        handleWritingViewNavigationArrows(true);
    }

    @OnClick(R.id.rightArrow)
    public void rightArrowClicked() {
        //Increment current visible item position to navigate next item
        currentVisibleItem++;
        handleWritingViewNavigationArrows(true);
    }

    @Nullable
    @Override
    public Activity getAttachedActivity() {
        return getActivity();
    }

    @Override
    public boolean onWebViewItemRequiresConnection() {
        return true;
    }

    @Override
    public void onLogOutInitiated() {
        // no ops
    }

    @Override
    public void onLogInInitiated() {
        // no ops
    }

    @Override
    public boolean shouldUseTaskStack() {
        return false;
    }

    @Override
    public boolean onMenuItemClicked(MenuItem item) {
        return false;
    }

    @Override
    public void onAccessibilityStateChanged(boolean enabled) {
        shallScrollWhenAccessibilityIsOn(!enabled);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == REQUEST_CODE_LOGIN || requestCode == AppConstants.ADD_OR_LINK_CARD_REQUEST_CODE)
                && resultCode == BytemarkSDK.ResponseCode.SUCCESS) {
            loadFareMedia();
        }
    }

    @Override
    public void onActionSheetClicked(@org.jetbrains.annotations.Nullable String actionSheetType) {
    }
}
