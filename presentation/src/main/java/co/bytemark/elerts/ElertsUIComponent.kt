package co.bytemark.elerts

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import androidx.fragment.app.Fragment
import co.bytemark.R
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.Api.ApiUtility.internetIsAvailable
import co.bytemark.settings.SettingsFragment
import co.bytemark.widgets.util.Util
import com.google.android.material.bottomsheet.BottomSheetDialog

class ElertsUIComponent {
    private lateinit var reportProblem: Button
    private lateinit var viewAlert: Button
    private lateinit var call: Button

    fun setupElertsBottomSheet(
        elertsBottomSheetDialog: BottomSheetDialog,
        activity: Activity,
        fragment: Fragment
    ) {
        val bottomSheetView =
            LayoutInflater.from(activity).inflate(R.layout.elerts_bottom_sheet, null)
        reportProblem = bottomSheetView.findViewById(R.id.reportBtn)
        setupReportClickListener(reportProblem, elertsBottomSheetDialog, activity, fragment)
        viewAlert = bottomSheetView.findViewById(R.id.viewAlertsBtn)
        setupViewAlertsClickListener(viewAlert, elertsBottomSheetDialog, activity, fragment)
        call = bottomSheetView.findViewById(R.id.callBtn)
        setupCallBtnClickListener(call, elertsBottomSheetDialog, activity)
        elertsBottomSheetDialog.setContentView(bottomSheetView)
    }

    private fun setupReportClickListener(
        reportBtnView: Button,
        elertsBottomSheetDialog: BottomSheetDialog,
        activity: Activity,
        fragment: Fragment
    ) {
        reportBtnView.setOnClickListener { v ->
            elertsBottomSheetDialog.hide()
            startElertsReportActivity(activity, fragment)
        }
    }

    private fun setupViewAlertsClickListener(
        reportBtnView: Button,
        elertsBottomSheetDialog: BottomSheetDialog,
        activity: Activity,
        fragment: Fragment
    ) {
        reportBtnView.setOnClickListener { v ->
            elertsBottomSheetDialog.hide()
            startAlertsActivity(activity, fragment)
        }
    }

    private fun setupCallBtnClickListener(
        callBtnView: Button,
        elertsBottomSheetDialog: BottomSheetDialog,
        activity: Activity
    ) {
        callBtnView.setOnClickListener { v ->
            elertsBottomSheetDialog.hide()
            startCallPrompt(activity)
        }
    }

    private fun startElertsReportActivity(activity: Activity, fragment: Fragment) {
        if(!internetIsAvailable(activity) &&  fragment is SettingsFragment) {
            fragment.connectionErrorDialog()
            return
        }
        Util.regUserAndOrgForECUI(activity.application)
        Util.getOrgs(activity.application, activity, true)
    }

    private fun startAlertsActivity(activity: Activity, fragment: Fragment) {
        if(!internetIsAvailable(activity) &&  fragment is SettingsFragment) {
            fragment.connectionErrorDialog()
            return
        }
        val clazz= Util.getClass(AppConstants.ELERTS_MESSAGE_CLASS_NAME)
        val bundle = Bundle()
        bundle.putString(AppConstants.TITLE, activity.getString(R.string.view_alerts))
        val intent = Intent(activity, clazz)
        intent.putExtras(bundle)
        activity.startActivity(intent)
    }

    private fun startCallPrompt(activity: Activity) {
        Util.invokeECUIUtilMethod(AppConstants.ELERTS_SHOW_CALL_PROMPT_METHOD_NAME, null, activity);
    }
}