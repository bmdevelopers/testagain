package co.bytemark;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog.Builder;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.apache.http.util.EncodingUtils;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.base.MasterActivity;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.sdk.Api.ApiUtility;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.data.identifiers.IdentifiersRepository;
import co.bytemark.sdk.data.userAccount.UserAccountRepository;
import co.bytemark.use_tickets.UseTicketsActivity;
import timber.log.Timber;

import static co.bytemark.analytics.AnalyticsPlatformsContract.AnalyticsPlatformEntry.FACEBOOK;
import static co.bytemark.analytics.AnalyticsPlatformsContract.AnalyticsPlatformEntry.GOOGLE;
import static co.bytemark.analytics.AnalyticsPlatformsContract.AnalyticsPlatformEntry.NATIVE_ACCOUNT;
import static co.bytemark.sdk.BytemarkSDK.ResponseCode.NOT_AUTHENTICATED;
import static co.bytemark.sdk.model.common.Action.AUTHENTICATION;

public class MainActivity extends MasterActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    //Google+ signin variables
    private static final int STATE_DEFAULT = 0;
    private static final int STATE_SIGN_IN = 1;
    // Request code
    private static final int RC_GOOGLE_SIGN_IN = 0;
    private static final String SAVED_PROGRESS = "sign_in_progress";
    private final List<String> permission = new ArrayList<String>() {{
        add("email");
    }};
    @Inject
    ConfHelper confHelper;
    @Inject
    AnalyticsPlatformAdapter analyticsPlatformAdapter;
    @Inject
    IdentifiersRepository repository;
    @Inject
    UserAccountRepository userAccountRepository;
    private String fullUrl;
    private WebView webView;
    private ProgressDialog dialog;
    private String signInMethod = NATIVE_ACCOUNT;
    private int signInProgress;
    private String authToken;
    private GoogleApiClient googleApiClient;
    private CallbackManager callbackManager;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomerMobileApp.Companion.getComponent().inject(this);
        setToolbarTitle(confHelper.getSettingsMenuScreenTitleFromTheConfig(false, false, AUTHENTICATION));

        setStatusBarColor();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) this)
                .build();

        if (savedInstanceState != null) {
            signInProgress = savedInstanceState.getInt(SAVED_PROGRESS, STATE_DEFAULT);
        }

        /* Check that the SDK has been initialized */
        if (!BytemarkSDK.isInitialized()) {
            finishWithResult(BytemarkSDK.ResponseCode.SDK_NOT_INITIALIZED);
            return;
        }

        /* Log the current time */
        userAccountRepository.insertOrUpdateLastAccessTime(new GregorianCalendar());

        /* Check that there is a network connection */
        if (!ApiUtility.internetIsAvailable(this)) {
            finishWithResult(BytemarkSDK.ResponseCode.NETWORK_NOT_AVAILABLE);
            return;
        }

        /* Check if oauth_token already exists */
        if (BytemarkSDK.SDKUtility.getAuthToken() != null && !BytemarkSDK.SDKUtility.getAuthToken().equals("")) {
            BytemarkSDK.updateAppInstallationId().subscribe();
            BytemarkSDK.updateUserInfoDatabase().subscribe();
            finishWithResult(BytemarkSDK.ResponseCode.SUCCESS);
            return;
        }

        //at this point, the oauth_token doesn't exist, so we need to log in

        //making full screen webview
        webView = findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);

        /* Set up loginParams url */
        final String loginURL = BytemarkSDK.SDKUtility.getLoginUrl();

        /* Get other device information */
        String appVersion = BytemarkSDK.SDKUtility.getAppVersion();
        String aii = repository.getAttribute("aii");
        String osi = BytemarkSDK.SDKUtility.getDeviceOsi();
        String deviceModel = null;
        String deviceOS = "android"; // Hardcoded android as OS
        String deviceOSVersion = Build.VERSION.RELEASE;
        String deviceTime = null;
        final String state = UUID.randomUUID().toString();

        try {
            deviceModel = URLEncoder.encode(Build.MODEL, "UTF-8");
            deviceTime = URLEncoder.encode(ApiUtility.getSFromCalendar(new GregorianCalendar()), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            finishWithResult(BytemarkSDK.ResponseCode.UNEXPECTED_ERROR);
        }

        fullUrl = loginURL +
                "&app_version=" + appVersion +
                "&aii=" + aii +
                "&osi=" + osi +
                "&device_model=" + deviceModel +
                "&device_os=" + deviceOS +
                "&device_os_version=" + deviceOSVersion +
                "&device_time=" + deviceTime +
                "&state=" + state;

        webView.setWebViewClient(new MyWebClient(state));
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.setCancelable(false);
        loadHomeUrl();
    }

    private void loadHomeUrl() {
        webView.clearHistory();
        webView.loadUrl(fullUrl);
        dialog.show();
    }


    @Override
    public void onStart() {
        super.onStart();
        analyticsPlatformAdapter.startAnalyticsPlatforms(this);
        googleApiClient.connect();
    }


    @Override
    public void onStop() {
        analyticsPlatformAdapter.stopAnalyticsPlatforms(this);
        googleApiClient.disconnect();
        super.onStop();
    }

    private void setStatusBarColor() {
        getWindow().setStatusBarColor(confHelper.getStatusBarColor());
    }

    private void getIdToken() {
        // Show an account picker to let the user choose a Google account from the device.
        // If the GoogleSignInOptions only asks for IDToken and/or profile and/or email then no
        // consent screen will be shown here.
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Timber.tag(TAG).d("handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            if (acct != null && !TextUtils.isEmpty(acct.getIdToken())) {
                String accessToken = acct.getIdToken();
                Timber.tag(TAG).d("handleSignInResult: " + fullUrl + "&service=googleplus" + "&service_access_token="
                        + accessToken + "&Allow=true");
                Timber.tag(TAG).d("handleSignInResult: " + accessToken);
                Timber.tag(TAG).d("handleSignInResult: " + acct.getId());
                dialog.show();
                webView.postUrl(fullUrl + "&service=googleplus" + "&service_access_token="
                        + accessToken + "&Allow=true", EncodingUtils.getBytes("", "BASE64"));
            }

            if (!TextUtils.isEmpty(authToken)) {
                signInMethod = GOOGLE;
                doLogin(authToken);
            }

        } else {
            Toast.makeText(this, GoogleSignInStatusCodes.getStatusCodeString(result.getStatus().getStatusCode()), Toast.LENGTH_SHORT).show();
            redirectToHomePage();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Timber.tag(TAG).d("onConnectionFailed:" + connectionResult);
    }

    private void googleSignOut() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                status -> Timber.tag(TAG).d("signOut:onResult:" + status));
    }

    private boolean isFacebookLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    private void facebookLoginClick() {
        //LoginManager.getInstance().logOut();
        if (!isFacebookLoggedIn()) {
            callbackManager = CallbackManager.Factory.create();
            //((UseTicketsActivity)getApplicationContext()).setCallbackManager(callbackManager);
            LoginManager.getInstance().logInWithReadPermissions(this, permission);
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Timber.tag("TAG").d( "SUCCESS");
                    // App code
                    GraphRequest request = GraphRequest.newMeRequest(
                            loginResult.getAccessToken(),
                            (object, response) -> {
                                Timber.tag("LoginActivity").v( response.toString());

                                // Application code
                                try {
                                    String email = object.has("email") ? object.getString("email") : null;
                                    Timber.tag("TAG").d("FACEBOOK EMAIL: " + email);

                                    // change the state of the button or do whatever you want
                                    //POST to get auth-token https://phabricator.bytemark.co/w/platform/user_accounts/social_sign_in_by_token/
                                    webView.setVisibility(View.VISIBLE);
                                    webView.postUrl(fullUrl + "&service=facebook" + "&service_access_token=" + loginResult.getAccessToken().getToken() + "&Allow=true", EncodingUtils.getBytes("", "BASE64"));

                                    //simpleFacebook.logout(onLogoutListener);
                                    dialog.show();
                                    //Timber.tag(TAG).d(fullUrl + "&service=facebook" + "&service_access_token=" + token + "&Allow=true");

                                    // Do not sent the access token, check if authToken has been received, if not then let the page load and do loginParams in onPageFinished.
                                    if (!TextUtils.isEmpty(authToken)) {
                                        signInMethod = FACEBOOK;
                                        doLogin(authToken);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,name,email,gender,birthday");
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                @Override
                public void onCancel() {
                    Timber.tag("TAG").d("CANCEL");
                    redirectToHomePage();
                }

                @Override
                public void onError(FacebookException e) {
                    Timber.tag("TAG").d("ERROR");
                    Timber.tag(TAG).e("facebook sign in exception:" + e.getLocalizedMessage());
                    redirectToHomePage();
                }
            });
        }
    }

    private void redirectToHomePage() {
        webView.setVisibility(View.VISIBLE);
        if (webView.canGoBack()) {
            loadHomeUrl();
        } else {
            finishWithResult(NOT_AUTHENTICATED);
        }
    }

    /**
     * Back press on cancels LoginActivity with ResponseCode.USER_CANCELLED (105), should be handled accordingly
     */
    @Override
    public void onBackPressed() {
        finishWithResult(BytemarkSDK.ResponseCode.USER_CANCELLED);
    }

    private void finishWithResult(int bytemarkResultCode) {
        setResult(bytemarkResultCode);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RC_GOOGLE_SIGN_IN:
                if (resultCode == RESULT_OK) {
                    // If the error resolution was successful we should continue
                    // processing errors.
                    signInProgress = STATE_SIGN_IN;
                } else {
                    // If the error resolution was not successful or the user canceled,
                    // we should stop processing errors.
                    signInProgress = STATE_DEFAULT;
                }
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);

                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVED_PROGRESS, signInProgress);
    }

    private void doLogin(@NonNull String authToken) {
        dialog.show();
        BytemarkSDK.doLogin(authToken)
                .subscribe(response -> {
                    dialog.hide();
                    dialog.cancel();
                    if (isTaskRoot()) {
                        startActivity(new Intent(this, UseTicketsActivity.class));
                    }
                    finishWithResult(BytemarkSDK.ResponseCode.SUCCESS);
                }, throwable -> {
                    Timber.tag(TAG).e(throwable.getLocalizedMessage());
                    if (throwable instanceof BytemarkSDK.SpecialException) {
                        showAppUpdateDialog();
                    }
                });
    }

    public void showAppUpdateDialog() {
        BytemarkSDK.logout(this);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.popup_update_required_title);
        alertDialog.setMessage(R.string.popup_update_required_message);
        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
        alertDialog.setPositiveButton(R.string.update, (dialog, which) -> {
            Intent goToMarket = new Intent(Intent.ACTION_VIEW)
                    .setData(Uri.parse("market://details?id=" + getPackageName()));
            startActivity(goToMarket);
            finish();
        });
        alertDialog.setNegativeButton(R.string.exit, (dialog, which) -> {
            dialog.cancel();
            finish();
        });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private class MyWebClient extends WebViewClient {
        final String mStateParameter;

        private MyWebClient(String stateParameter) {
            mStateParameter = stateParameter;
        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            final Builder builder = new Builder(getApplicationContext());
            builder.setMessage(R.string.login_popup_notification_error_ssl_cert_invalid);
            builder.setPositiveButton(R.string.continuee, (dialog12, which) -> handler.proceed());
            builder.setNegativeButton(R.string.popup_cancel, (dialog1, which) -> handler.cancel());
            final androidx.appcompat.app.AlertDialog dialog = builder.create();
            dialog.show();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (url.contains("facebook.com")) {
                //Timber.tag(TAG).d("time for facebook signin");
                webView.setVisibility(View.GONE);
                webView.loadUrl("about:blank");
                //loginToFacebook();
                facebookLoginClick();

            } else if (url.contains("google.com")) {
                webView.loadUrl("about:blank");
                Timber.tag(TAG).d("time for google+ signin");

                /* Now try singing in*/
                if (!googleApiClient.isConnecting()) {
                    getIdToken();
                }
            } else if (url.contains("access_token")) {
                dialog.show();
            }
        }

        public void onPageFinished(WebView view, String url) {
            //Timber.tag(TAG).d("URL returned onPageFinished: " + url);
            if (googleApiClient.isConnected()) {
                googleSignOut();
            }

            // Check if the activity is been destroyed before dismissing the progress dialog.
            if (!MainActivity.this.isDestroyed()) {
                dialog.dismiss();
            }

            /* Split the URL into an array */
            String[] urlSplit = url.split("=|#|&|\\?");
            authToken = "";
            String state = "";
            /* Iterate through and find values */
            for (int i = 0; i < urlSplit.length; i++) {
                if (urlSplit[i].equals("state")) {
                    if (urlSplit.length > (i + 1)) {
                        state = urlSplit[i + 1];
                    }
                }
                if (urlSplit[i].equals("access_token")) {
                    if (urlSplit.length > (i + 1)) {
                        authToken = urlSplit[i + 1];
                    }
                }
            }

            if (!state.equals(mStateParameter)) {
                //Timber.tag(TAG).d("Got state: " + state + " instead of expected: " + mStateParameter + " possible cross-site attack.");
                return; // TODO: report cross-site attack
            }
            if (!authToken.isEmpty()) {
                //Timber.tag(TAG).d("Got access token: " + authToken);

                if (!MainActivity.this.isDestroyed()) {
                    dialog.dismiss();
                }
                doLogin(authToken);
            }
        }
    }
}
