package co.bytemark.menu

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.sdk.model.common.Action

class MenuActivity : MasterActivity() {

    override fun getLayoutRes(): Int = R.layout.activity_more_info

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setNavigationViewCheckedItem(Action.MORE_INFORMATION)
    }
}