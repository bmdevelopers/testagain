package co.bytemark.menu

import android.content.Context
import android.content.res.ColorStateList
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.helpers.ConfHelper
import co.bytemark.menu.MenuAdapter.MoreInfoMenuItemHolder
import co.bytemark.sdk.model.menu.MenuGroup
import co.bytemark.sdk.model.menu.MenuItem
import co.bytemark.widgets.dynamicmenu.AbstractMenuGroupAdapter
import co.bytemark.widgets.dynamicmenu.AbstractMenuItemAdapter
import co.bytemark.widgets.dynamicmenu.AbstractMenuItemAdapter.AbstractMenuItemHolder
import co.bytemark.widgets.dynamicmenu.MenuClickManager
import co.bytemark.widgets.dynamicmenu.MenuItemDrawableLoader
import co.bytemark.widgets.util.hide
import co.bytemark.widgets.util.show
import kotlinx.android.synthetic.main.more_info_menu_group_item_template.view.*
import kotlinx.android.synthetic.main.more_info_menu_item_template.view.*
import javax.inject.Inject

class MenuAdapter(
    context: Context,
    menuClickManager: MenuClickManager
) : AbstractMenuGroupAdapter<MenuAdapter.MoreInfoMenuGroupHolder, MoreInfoMenuItemHolder>(
    context,
    menuClickManager
) {

    @Inject
    lateinit var confHelper: ConfHelper

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    override fun onBindViewHolder(
        holder: MoreInfoMenuGroupHolder,
        position: Int,
        menuGroup: MenuGroup
    ) {
        with(holder.itemView) {
            if (!TextUtils.isEmpty(menuGroup.header)) {
                menuGroupHeaderTextView.text = menuGroup.header
            } else {
                menuGroupHeaderTextView.hide()
            }
            if (!TextUtils.isEmpty(menuGroup.footer)) {
                menuGroupFooterTextView.text = menuGroup.footer
            } else {
                menuGroupFooterTextView.hide()
            }
            this.contentDescription = menuGroup.header
        }
    }

    override fun createMenuItemAdapter(
        context: Context,
        menuClickManager: MenuClickManager,
        menuItems: MutableList<MenuItem>
    ): AbstractMenuItemAdapter<MoreInfoMenuItemHolder> {
        return object : AbstractMenuItemAdapter<MoreInfoMenuItemHolder>(
            confHelper,
            context,
            menuItems,
            menuClickManager,
            analyticsPlatformAdapter
        ) {

            override fun onBindViewHolder(
                holder: MoreInfoMenuItemHolder,
                position: Int,
                menuItem: MenuItem
            ) {
                with(holder.itemView) {
                    this.contentDescription = menuItem.title
                    if (menuItem.subtitle?.isNotEmpty() == true) {
                        moreInfoMenuItemSingleTitle.hide()
                        moreInfoMenuItemDoubleTitle.show()
                        moreInfoMenuItemDoubleSubTitle.show()
                        moreInfoMenuItemDoubleTitle.text = menuItem.title
                        moreInfoMenuItemDoubleSubTitle.text = menuItem.subtitle
                    } else {
                        moreInfoMenuItemDoubleTitle.hide()
                        moreInfoMenuItemDoubleSubTitle.hide()
                        moreInfoMenuItemSingleTitle.show()
                        moreInfoMenuItemSingleTitle.text = menuItem.title
                    }
                    if (menuItem.selectedImage.isNotEmpty()) {
                        moreInfoMenuItemIcon.setImageDrawable(
                            context.getDrawable(
                                MenuItemDrawableLoader.getDrawableIdRes(menuItem.selectedImage)
                            )
                        )
                        // avoid setting tint on colored icons
                        if (!menuItem.selectedImage.contains("color")) {
                            moreInfoMenuItemIcon.imageTintList =
                                ColorStateList.valueOf(confHelper.dataThemePrimaryTextColor)
                        }
                    } else {
                        moreInfoMenuItemIcon.hide()
                    }
                }
            }

            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): MoreInfoMenuItemHolder =
                MoreInfoMenuItemHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.more_info_menu_item_template, parent, false)
                )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoreInfoMenuGroupHolder =
        MoreInfoMenuGroupHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.more_info_menu_group_item_template, parent, false)
        )

    class MoreInfoMenuGroupHolder(itemView: View) : AbstractMenuGroupHolder(itemView)

    class MoreInfoMenuItemHolder(itemView: View) : AbstractMenuItemHolder(itemView)

    init {
        component.inject(this)
    }
}
