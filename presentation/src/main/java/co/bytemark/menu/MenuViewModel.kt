package co.bytemark.menu

import androidx.lifecycle.MutableLiveData
import co.bytemark.base.BaseViewModel
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.model.menu.MenuGroup
import javax.inject.Inject

class MenuViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var configHelper: ConfHelper

    val menuListLiveData by lazy { MutableLiveData<List<MenuGroup>>() }
    private val isUserLoggedIn = BytemarkSDK.isLoggedIn()

    fun loadMenuGroups(menuGroups: List<MenuGroup>?) {
        if (menuGroups != null) {
            menuListLiveData.value = menuGroups
        } else {
            val menuList = configHelper.supportedMoreInfoList
            menuListLiveData.value = if (isUserLoggedIn && menuList?.signedInMenuGroups?.isNotEmpty() == true) {
                menuList.signedInMenuGroups
            } else if (!isUserLoggedIn && menuList?.signedOutMenuGroups?.isNotEmpty() == true) {
                menuList.signedOutMenuGroups
            } else {
                menuList.menuGroups
            }
        }
    }

}