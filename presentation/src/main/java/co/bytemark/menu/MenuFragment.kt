package co.bytemark.menu

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import co.bytemark.CustomerMobileApp
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.sdk.model.common.Action
import co.bytemark.sdk.model.menu.MenuItem
import co.bytemark.widgets.dynamicmenu.AbstractMenuItemAdapter
import co.bytemark.widgets.dynamicmenu.MenuClickManager
import co.bytemark.widgets.util.createViewModel
import co.bytemark.widgets.util.getViewModel
import kotlinx.android.synthetic.main.fragment_more_info.*
import javax.inject.Inject

class MenuFragment : BaseMvvmFragment(), MenuClickManager {

    private lateinit var menuAdapter: MenuAdapter
    private lateinit var viewModel: MenuViewModel

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_more_info, container, false)

    override fun onInject() {
        component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = createViewModel { CustomerMobileApp.appComponent.menuViewModel }
        initMenuList()
        observeLiveData()
        analyticsPlatformAdapter.moreInfoScreenDisplayed()
    }

    fun getViewModel(): MenuViewModel = getViewModel(CustomerMobileApp.appComponent.menuViewModel)

    override fun onResume() {
        super.onResume()
        val action: Action? = activity?.intent?.getParcelableExtra(AbstractMenuItemAdapter.EXTRA_KEY_ACTION)
        viewModel.loadMenuGroups(action?.menuGroups)
        val title = activity?.intent?.getStringExtra(Intent.EXTRA_TITLE)
        (activity as AppCompatActivity?)?.supportActionBar?.let { actionBar ->
            actionBar.setDisplayShowTitleEnabled(true)
            title?.let {
                actionBar.title = it
            }
        }
    }

    private fun observeLiveData() {
        viewModel.menuListLiveData.observe(this, androidx.lifecycle.Observer {
            menuAdapter.setMenuGroups(it)
        })
        viewModel.errorLiveData.observe(this, androidx.lifecycle.Observer {
            it?.let { it1 -> handleError(it1) }
        })
    }

    private fun initMenuList() {
        context?.let {
            menuAdapter = MenuAdapter(it, this)
            moreInfoMenuGroupRecyclerView.adapter = menuAdapter
        }
    }

    override fun onWebViewItemRequiresConnection(): Boolean {
        connectionErrorDialog()
        // Consume the event
        return true
    }

    override fun getAttachedActivity(): Activity? = activity

    override fun onLogOutInitiated() {}

    override fun shouldUseTaskStack(): Boolean = false

    override fun onLogInInitiated() {}

    override fun onMenuItemClicked(item: MenuItem): Boolean {
        if (!isOnline() && item.action?.screen != null &&
            item.action?.screen.equals(Action.NATIVE_APP_SUPPORT, ignoreCase = true)
        ) {
            connectionErrorDialog()
            return true
        }
        return false
    }

    override fun onActionSheetClicked(actionSheetType: String?) {}
}