package co.bytemark.add_payment_card

import android.text.TextUtils
import co.bytemark.R
import java.util.regex.Pattern

enum class PaymentCard constructor(
    internal val cardName: String,
    private val regex: Pattern,
    private val typeImage: Int
) {
    AMERICAN_EXPRESS(
        "American Express",
        Pattern.compile("^3[47][0-9]*$"),
        R.mipmap.ic_american_express
    ),
    DINERS_CLUB(
        "Diners Club",
        Pattern.compile("^3(?:0[0-5]|[68])[0-9]*$"),
        R.mipmap.ic_diners_club
    ),
    DISCOVER(
        "Discover",
        Pattern.compile("^6(?:011|5)[0-9]*$"),
        R.mipmap.ic_discover
    ),
    UNION_PAY("UnionPay",
              Pattern.compile("^(62[0-9]{14,17})\$"),
              R.drawable.unionpay_light),
    MAESTRO("Maestro",
            Pattern.compile("^(5018|5020|5038|5893|6304|6759|6761|6762|6763|676770|676774)[0-9]{8,15}\$"),
            R.drawable.ic_maestro),
    JCB(
        "JCB",
        Pattern.compile("^(?:2131|1800|35)[0-9]*$"),
        R.mipmap.ic_jcb
    ),
    MASTERCARD(
        "MasterCard",
        Pattern.compile("^5[1-5]\\d{0,14}$|^2(?:2(?:2[1-9]|[3-9]\\d)|[3-6]\\d\\d|7(?:[01]\\d|20))\\d{0,12}$"),
        R.mipmap.ic_mastercard
    ),
    VISA(
        "Visa",
        Pattern.compile("^4[0-9]*$"),
        R.mipmap.ic_visa_electron
    ),
    DEFAULT(
        "Default",
        Pattern.compile(""),
        R.mipmap.ic_default_card
    );

    fun getTypeImage() = typeImage

    companion object {
        private val PAYMENT_CARD =
            HashMap<String, PaymentCard>().apply {
                values().forEach { paymentCard ->
                    put(paymentCard.cardName, paymentCard)
                }
            }

        private fun getPaymentCard() = PAYMENT_CARD

        fun fromPaymentCardType(paymentCardType: String): PaymentCard {
            if (paymentCardType.isEmpty()) return DEFAULT

            getPaymentCard().values.forEach { value ->
                if (value.cardName.equals(paymentCardType, ignoreCase = true)) return value
            }
            return DEFAULT
        }

        fun fromPaymentCardNumber(number: String): PaymentCard {
            if (TextUtils.isEmpty(number)) return DEFAULT

            val trimmedNumber = number.replace("\\s+".toRegex(), "")

            val possiblePaymentCardTypes = HashSet<PaymentCard>()

            getPaymentCard().values.forEach { value ->
                if (doesNumberMatchesPattern(trimmedNumber, value.regex))
                    possiblePaymentCardTypes.add(value)
            }
            return if (possiblePaymentCardTypes.size == 1)
                possiblePaymentCardTypes.iterator().next()
            else
                DEFAULT
        }

        private fun doesNumberMatchesPattern(number: String, pattern: Pattern) =
            pattern.matcher(number).matches()
    }
}
