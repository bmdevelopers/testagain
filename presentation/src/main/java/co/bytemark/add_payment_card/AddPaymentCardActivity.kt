package co.bytemark.add_payment_card

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import co.bytemark.R
import co.bytemark.add_payment_card.nmi.NMIAddCardWebViewFragment
import co.bytemark.base.ToolbarActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.util.addFragment

class AddPaymentCardActivity : ToolbarActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)

        setToolbarTitle(getString(R.string.screen_title_add_payment_card))

        addFragment(
                if (confHelper.isNMIEnabled)
                    NMIAddCardWebViewFragment.newInstance()
                else
                    AddPaymentCardFragment.newInstance(),
                R.id.container
        )

        val intent = Intent()
        intent.putExtra(AppConstants.SHOPPING_CART_INTENT, getIntent().getBooleanExtra(AppConstants.SHOPPING_CART_INTENT, false))
        setResult(Activity.RESULT_CANCELED, intent)
    }

    override fun getLayoutRes() = R.layout.activity_add_payment_card

}