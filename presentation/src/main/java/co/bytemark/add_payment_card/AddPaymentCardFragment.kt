package co.bytemark.add_payment_card

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.TextUtils
import android.text.method.DigitsKeyListener
import android.view.*
import android.view.View.OnFocusChangeListener
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.TextView
import androidx.core.graphics.ColorUtils
import androidx.lifecycle.Observer
import co.bytemark.AssetParser
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.add_payment_card.Helper.TextWatcherHelper
import co.bytemark.add_payment_card.validators.*
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.Result
import co.bytemark.helpers.AppConstants
import co.bytemark.helpers.AppConstants.MAX_CARD_CVV_LENGTH
import co.bytemark.sdk.model.payment_methods.PaymentMethods
import co.bytemark.widgets.emptystateview.EmptyStateLayout
import co.bytemark.widgets.util.createViewModel
import co.bytemark.widgets.util.setCursorForEditTextInAccessibilityMode
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding.widget.RxTextView
import com.jakewharton.rxbinding.widget.TextViewAfterTextChangeEvent
import io.card.payment.CardIOActivity
import io.card.payment.CardType
import io.card.payment.CreditCard
import kotlinx.android.synthetic.main.fragment_payment_card.*
import rx.android.schedulers.AndroidSchedulers
import rx.subscriptions.CompositeSubscription
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class AddPaymentCardFragment : BaseMvvmFragment() {

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    @Inject
    lateinit var assetParser: AssetParser

    private lateinit var cardNumberValidator: TextValidator
    private lateinit var cardExpirationValidator: TextValidator
    private lateinit var cardCVVValidator: TextValidator
    private lateinit var addressValidator: TextValidator
    private lateinit var zipCodeValidator: TextValidator
    private lateinit var firstNameValidator: TextValidator
    private lateinit var lastNameValidator: TextValidator
    private lateinit var cityValidator: TextValidator
    private lateinit var emailValidator: TextValidator
    private lateinit var stateValidator: TextValidator
    private lateinit var viewModel: AddPaymentCardViewModel
    private lateinit var acceptedPaymentMethods: ArrayList<String>
    private var isCardTypeSupported = true

    private var cardNumber: String? = null
    private val cardValidationSubs = CompositeSubscription()

    var countryCode: String? = null
    var stateCodeOrName: String? = null
    private var emptyStateLayout: EmptyStateLayout? = null

    companion object {
        @JvmStatic
        fun newInstance() =
            AddPaymentCardFragment()
    }

    override fun onInject() =
        CustomerMobileApp.component.inject(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = createViewModel { CustomerMobileApp.appComponent.addPaymentCardViewModel }
        acceptedPaymentMethods =
            activity?.intent?.getStringArrayListExtra(AppConstants.ACCEPTED_PAYMENTS)
                ?: arrayListOf()
        retainInstance = true
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_payment_card, container, false)
        emptyStateLayout = view.findViewById(R.id.emptyStateLayout) as EmptyStateLayout
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initCardNumberValidator()
        setCardIcon("")
        initCardExpirationValidator()
        initCardCVVValidator()
        initZipPostalValidator()
        initAddressValidator()
        initFirstNameValidator()
        initLastNameValidator()
        initCityValidator()
        initEmailValidator()
        initCountryAndStateValidators()
        initKeyListener()
        initSaveButtonEnablerListener()
        buttonSave.setOnClickListener {
            addPaymentCard()
        }
        setCursorOnEndInAccessibilityMode()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        activity!!.menuInflater.inflate(R.menu.menu_add_payment_card, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.scan) {
            val scanIntent = Intent(context, CardIOActivity::class.java)
            startActivityForResult(scanIntent, AppConstants.MY_SCAN_REQUEST_CODE)
            analyticsPlatformAdapter.featureScanCardSelected()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        if (!TextUtils.isEmpty(cardNumber)) {
            editTextCard.setText(cardNumber)
            cardNumber = null
        }

        setVisibilityOfAdditionCCFields(
            if (confHelper.isAdditionalCCFieldsRequired) View.VISIBLE else View.GONE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AppConstants.MY_SCAN_REQUEST_CODE) {
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                val scanResult: CreditCard? =
                    data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT)
                cardNumber = scanResult?.formattedCardNumber
                scanResult?.formattedCardNumber?.let { setCardIcon(it) }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        cardValidationSubs.clear()
    }

    private fun setCardIcon(numbers: String) {
        val paymentCard = PaymentCard.fromPaymentCardNumber(numbers)
        cardNumberTypeValidator(paymentCard.cardName)
        Glide.with(activity)
            .load(paymentCard.getTypeImage())
            .crossFade()
            .skipMemoryCache(false)
            .into(imageViewCard)
    }

    private fun initCardNumberValidator() {
        cardNumberValidator = CardNumberValidator()
        editTextCard.addTextChangedListener(cardNumberValidator)
        editTextCard.addTextChangedListener(object : TextWatcherHelper() {
            override fun afterTextChanged(s: Editable?) {
                super.afterTextChanged(s)
                setCardIcon(s.toString())
                if (s === editTextCard.text) {
                    val cardType = CardType.fromCardNumber(cardNumberValidator.getTextValue())
                    cardNumberValidationListener(false)
                    val cvvValidator = cardCVVValidator as CardCvvValidator
                    cvvValidator.requiredLength = cardType.cvvLength()
                }
            }
        })

        editTextCard.setOnFocusChangeListener { _, focused ->
            if (!focused) cardNumberValidationListener(true)
        }
        editTextCard.filters = arrayOf(DigitsKeyListener(), cardNumberValidator)
    }

    private fun cardNumberTypeValidator(type: String) {
        if (acceptedPaymentMethods.isEmpty()) {
            isCardTypeSupported = true
            return
        }
        if (editTextCard.text?.isNotEmpty() == true && type != PaymentCard.DEFAULT.cardName &&
            !acceptedPaymentMethods.contains(type.toLowerCase())
        ) {
            isCardTypeSupported = false
            showCardNumberError(true, getString(R.string.add_card_unsupported_card_type_error))
        } else {
            isCardTypeSupported = true
            showCardNumberError(false, null)
        }
    }

    private fun cardNumberValidationListener(lostFocus: Boolean) {
        if (cardNumberValidator.hasFullLengthText()) {
            if (cardNumberValidator.isTextValid()) {
                showCardNumberError(false, null)
                val paymentCard =
                    PaymentCard.fromPaymentCardNumber(editTextCard.text?.toString() ?: "")
                cardNumberTypeValidator(paymentCard.cardName)
                if (!lostFocus) editTextCard.onEditorAction(EditorInfo.IME_ACTION_NEXT)
            } else {
                showCardNumberError(true, getString(R.string.add_card_card_number_is_invalid))
            }
        } else if (lostFocus) {
            showCardNumberError(true, getString(R.string.add_card_card_number_is_invalid))
        }
    }

    private fun initCardExpirationValidator() {
        cardExpirationValidator = CardExpirationValidator()
        editTextExpiration.addTextChangedListener(cardExpirationValidator)
        editTextExpiration.addTextChangedListener(object : TextWatcherHelper() {
            override fun afterTextChanged(s: Editable?) {
                super.afterTextChanged(s)
                if (editTextExpiration != null && s === editTextExpiration.text) {
                    initExpirationValidationListener(false)
                }
            }
        })
        editTextExpiration.onFocusChangeListener =
            OnFocusChangeListener { _: View?, focused: Boolean ->
                if (!focused) {
                    initExpirationValidationListener(true)
                }
            }
        editTextExpiration.filters = arrayOf(DigitsKeyListener(), cardExpirationValidator)
    }

    private fun initExpirationValidationListener(lostFocus: Boolean) {
        if (cardExpirationValidator.hasFullLengthText()) {
            if (cardExpirationValidator.isTextValid()) {
                textInputLayoutExpiration.error = null
                textInputLayoutExpiration.isErrorEnabled = false
            } else {
                textInputLayoutExpiration.isErrorEnabled = true
                textInputLayoutExpiration.error =
                    getString(R.string.payment_card_expiration_is_invalid)
            }
        } else if (lostFocus) {
            textInputLayoutExpiration.isErrorEnabled = true
            textInputLayoutExpiration.error = getString(R.string.payment_card_expiration_is_invalid)
        }
    }

    private fun showCardNumberError(isErrorEnabled: Boolean, errorMessage: String?) {
        textInputLayoutCard.isErrorEnabled = isErrorEnabled
        textInputLayoutCard.error = errorMessage
    }

    private fun initCardCVVValidator() {
        cardCVVValidator = CardCvvValidator(MAX_CARD_CVV_LENGTH)
        editTextCVV.addTextChangedListener(cardCVVValidator)
        editTextCVV.addTextChangedListener(object : TextWatcherHelper() {
            override fun afterTextChanged(s: Editable?) {
                super.afterTextChanged(s)
                if (editTextCVV != null && s === editTextCVV.text) {
                    cardCVVValidationListener(false)
                }
            }
        })
        editTextCVV.setOnFocusChangeListener { _, focused ->
            if (!focused) {
                cardCVVValidationListener(true)
            }
        }
        editTextCVV.filters = arrayOf(DigitsKeyListener(), cardCVVValidator)
    }

    private fun cardCVVValidationListener(lostFocus: Boolean) {
        if (cardCVVValidator.hasFullLengthText()) {
            if (cardCVVValidator.isTextValid()) {
                textInputLayoutCVV.error = null
                textInputLayoutCVV.isErrorEnabled = false
            } else {
                textInputLayoutCVV.isErrorEnabled = true
                textInputLayoutCVV.error = getString(R.string.payment_card_cvv_is_invalid)
            }
        } else if (lostFocus) {
            textInputLayoutCVV.isErrorEnabled = true
            textInputLayoutCVV.error = getString(R.string.payment_card_cvv_is_invalid)
        }
    }

    private fun initZipPostalValidator() {
        zipCodeValidator = ZipCodeValidator(
            AppConstants.MAX_CARD_ZIP_POSTAL_CODE_LENGTH,
            AppConstants.MINIMUM_CARD_ZIP_POSTAL_CODE_LENGTH
        )
        editTextZipCode.filters = arrayOf(
            zipCodeValidator,
            LengthFilter(AppConstants.MAX_CARD_ZIP_POSTAL_CODE_LENGTH)
        )
        editTextZipCode.addTextChangedListener(zipCodeValidator)
        editTextZipCode.addTextChangedListener(object : TextWatcherHelper() {
            override fun afterTextChanged(s: Editable?) {
                super.afterTextChanged(s)
                if (editTextZipCode != null && s === editTextZipCode.text) {
                    initZipPostalListener(false)
                }
            }
        })
        editTextZipCode.onFocusChangeListener =
            OnFocusChangeListener { _: View?, focused: Boolean ->
                if (!focused) {
                    initZipPostalListener(true)
                }
            }
    }

    private fun initZipPostalListener(lostFocus: Boolean) {
        if (zipCodeValidator.hasFullLengthText()) {
            if (zipCodeValidator.isTextValid()) {
                textInputLayoutZipPostal.error = null
                textInputLayoutZipPostal.isErrorEnabled = false
            } else {
                textInputLayoutZipPostal.isErrorEnabled = true
                textInputLayoutZipPostal.error =
                    getString(R.string.add_card_zip_or_postal_code_is_invalid)
            }
        } else if (lostFocus) {
            textInputLayoutZipPostal.isErrorEnabled = true
            textInputLayoutZipPostal.error =
                getString(R.string.add_card_zip_or_postal_code_is_invalid)
        }
    }

    private fun initAddressValidator() {
        addressValidator = EmptyValidator()
        editTextAddress.filters =
            arrayOf(addressValidator, LengthFilter(AppConstants.MAX_CARD_ADDRESS_LENGTH))
        editTextAddress.addTextChangedListener(addressValidator)
        editTextAddress.addTextChangedListener(object : TextWatcherHelper() {
            override fun afterTextChanged(s: Editable?) {
                super.afterTextChanged(s)
                if (editTextAddress != null && s === editTextAddress.text) {
                    initAddressListener(false)
                }
            }
        })
        editTextAddress.onFocusChangeListener =
            OnFocusChangeListener { _: View?, focused: Boolean ->
                if (!focused) {
                    initAddressListener(true)
                }
            }
    }

    private fun initAddressListener(lostFocus: Boolean) {
        if (addressValidator.hasFullLengthText()) {
            if (addressValidator.isTextValid()) {
                textInputLayoutAddress.error = null
                textInputLayoutAddress.isErrorEnabled = false
            } else {
                textInputLayoutAddress.isErrorEnabled = true
                textInputLayoutAddress.error =
                    getString(R.string.add_card_billing_street_address_is_invalid)
            }
        } else if (lostFocus) {
            textInputLayoutAddress.isErrorEnabled = true
            textInputLayoutAddress.error =
                getString(R.string.add_card_billing_street_address_is_invalid)
        }
    }

    private fun initFirstNameValidator() {
        firstNameValidator = EmptyValidator()
        editTextFirstName.filters = arrayOf<InputFilter>(firstNameValidator)
        editTextFirstName.addTextChangedListener(firstNameValidator)
        editTextFirstName.addTextChangedListener(object : TextWatcherHelper() {
            override fun afterTextChanged(s: Editable?) {
                super.afterTextChanged(s)
                if (editTextFirstName != null && s === editTextFirstName.text) {
                    initFirstNameListener(false)
                }
            }
        })
        editTextFirstName.onFocusChangeListener =
            OnFocusChangeListener { _: View?, focused: Boolean ->
                if (!focused) {
                    initFirstNameListener(true)
                }
            }
    }

    private fun initFirstNameListener(lostFocus: Boolean) {
        if (firstNameValidator.hasFullLengthText()) {
            if (firstNameValidator.isTextValid()) {
                textInputLayoutFirstName.error = null
                textInputLayoutFirstName.isErrorEnabled = false
            } else {
                textInputLayoutFirstName.isErrorEnabled = true
                textInputLayoutFirstName.error = getString(R.string.payment_first_name_is_invalid)
            }
        } else if (lostFocus) {
            textInputLayoutFirstName.isErrorEnabled = true
            textInputLayoutFirstName.error = getString(R.string.payment_first_name_is_invalid)
        }
    }

    private fun initLastNameValidator() {
        lastNameValidator = EmptyValidator()
        editTextLastName.filters = arrayOf<InputFilter>(lastNameValidator)
        editTextLastName.addTextChangedListener(lastNameValidator)
        editTextLastName.addTextChangedListener(object : TextWatcherHelper() {
            override fun afterTextChanged(s: Editable?) {
                super.afterTextChanged(s)
                if (editTextLastName != null && s === editTextLastName.text) {
                    initLastNameListener(false)
                }
            }
        })
        editTextLastName.onFocusChangeListener =
            OnFocusChangeListener { _: View?, focused: Boolean ->
                if (!focused) {
                    initLastNameListener(true)
                }
            }
    }

    private fun initLastNameListener(lostFocus: Boolean) {
        if (lastNameValidator.hasFullLengthText()) {
            if (lastNameValidator.isTextValid()) {
                textInputLayoutLastName.error = null
                textInputLayoutLastName.isErrorEnabled = false
            } else {
                textInputLayoutLastName.isErrorEnabled = true
                textInputLayoutLastName.error = getString(R.string.payment_last_name_is_invalid)
            }
        } else if (lostFocus) {
            textInputLayoutLastName.isErrorEnabled = true
            textInputLayoutLastName.error = getString(R.string.payment_last_name_is_invalid)
        }
    }

    private fun initCityValidator() {
        cityValidator = EmptyValidator()
        editTextCity.filters = arrayOf<InputFilter>(cityValidator)
        editTextCity.addTextChangedListener(cityValidator)
        editTextCity.addTextChangedListener(object : TextWatcherHelper() {
            override fun afterTextChanged(s: Editable?) {
                super.afterTextChanged(s)
                if (editTextCity != null && s === editTextCity.text) {
                    initCityListener(false)
                }
            }
        })
        editTextCity.onFocusChangeListener = OnFocusChangeListener { _: View?, focused: Boolean ->
            if (!focused) {
                initCityListener(true)
            }
        }
    }

    private fun initCityListener(lostFocus: Boolean) {
        if (cityValidator.hasFullLengthText()) {
            if (cityValidator.isTextValid()) {
                textInputLayoutCity.error = null
                textInputLayoutCity.isErrorEnabled = false
            } else {
                textInputLayoutCity.isErrorEnabled = true
                textInputLayoutCity.error = getString(R.string.payment_city_is_invalid)
            }
        } else if (lostFocus) {
            textInputLayoutCity.isErrorEnabled = true
            textInputLayoutCity.error = getString(R.string.payment_city_is_invalid)
        }
    }

    private fun initEmailValidator() {
        emailValidator = EmptyValidator()
        editTextEmail.filters = arrayOf<InputFilter>(emailValidator)
        editTextEmail.addTextChangedListener(emailValidator)
        editTextEmail.addTextChangedListener(object : TextWatcherHelper() {
            override fun afterTextChanged(s: Editable?) {
                super.afterTextChanged(s)
                if (editTextEmail != null && s === editTextEmail.text) {
                    initEmailListener(false)
                }
            }
        })
        editTextEmail.onFocusChangeListener = OnFocusChangeListener { _: View?, focused: Boolean ->
            if (!focused) {
                initEmailListener(true)
            }
        }
    }

    private fun initEmailListener(lostFocus: Boolean) {
        if (emailValidator.hasFullLengthText()) {
            if (emailValidator.isTextValid()) {
                textInputLayoutEmail.error = null
                textInputLayoutEmail.isErrorEnabled = false
            } else {
                textInputLayoutEmail.isErrorEnabled = true
                textInputLayoutEmail.error = getString(R.string.payment_email_is_invalid)
            }
        } else if (lostFocus) {
            textInputLayoutEmail.isErrorEnabled = true
            textInputLayoutEmail.error = getString(R.string.payment_email_is_invalid)
        }
    }

    private fun initCountryAndStateValidators() {
        val countries = assetParser.loadCountries()
        val countryNames: MutableList<String> = ArrayList(countries.size)
        countryNames.clear()
        for ((name) in countries) {
            countryNames.add(name)
        }
        val countriesAdapter = ArrayAdapter(
            context!!,
            R.layout.formly_spinner_item, countryNames
        )
        countriesAdapter.setDropDownViewResource(R.layout.formly_simple_spinner_dropdown_item)
        spinnerCountry.adapter = countriesAdapter
        spinnerCountry.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                countryCode = countries[i].code
                if (countryCode != "US") {
                    spinnerStateUS.visibility = View.GONE
                    textInputLayoutState.visibility = View.VISIBLE
                    initStatValidator()
                } else {
                    spinnerStateUS.visibility = View.VISIBLE
                    textInputLayoutState.visibility = View.GONE
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }

        val states = assetParser.loadStates()
        val stateNames: MutableList<String> = ArrayList(states.size)
        stateNames.clear()
        for ((name) in states) {
            stateNames.add(name)
        }
        val statesAdapter = ArrayAdapter(
            context!!,
            R.layout.formly_spinner_item, stateNames
        )
        statesAdapter.setDropDownViewResource(R.layout.formly_simple_spinner_dropdown_item)
        spinnerStateUS.adapter = statesAdapter
        spinnerStateUS.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                stateCodeOrName = states[i].code
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
    }

    private fun initStatValidator() {
        stateValidator = EmptyValidator()
        editTextState.filters = arrayOf<InputFilter>(stateValidator)
        editTextState.addTextChangedListener(stateValidator)
        editTextState.addTextChangedListener(object : TextWatcherHelper() {
            override fun afterTextChanged(s: Editable?) {
                super.afterTextChanged(s)
                if (editTextState != null && s === editTextState.text) {
                    initStateListener(false)
                }
            }
        })
        editTextState.onFocusChangeListener = OnFocusChangeListener { _: View?, focused: Boolean ->
            if (!focused) {
                initStateListener(true)
            }
        }
    }

    private fun initStateListener(lostFocus: Boolean) {
        if (stateValidator.hasFullLengthText()) {
            if (stateValidator.isTextValid()) {
                textInputLayoutState.error = null
                textInputLayoutState.isErrorEnabled = false
            } else {
                textInputLayoutState.isErrorEnabled = true
                textInputLayoutState.error = getString(R.string.payment_state_providence_is_invalid)
            }
        } else if (lostFocus) {
            textInputLayoutState.isErrorEnabled = true
            textInputLayoutState.error = getString(R.string.payment_state_providence_is_invalid)
        }
    }

    private fun initKeyListener() {
        editTextLabel.setOnEditorActionListener { _: TextView?, actionId: Int, event: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                addPaymentCard()
            }
            false
        }
    }

    private fun initSaveButtonEnablerListener() {
        cardValidationSubs.clear()
        subscribeForTextChangeListener(editTextCard)
        subscribeForTextChangeListener(editTextExpiration)
        subscribeForTextChangeListener(editTextCVV)
        subscribeForTextChangeListener(editTextAddress)
        subscribeForTextChangeListener(editTextZipCode)
        // Additional CC Fields
        subscribeForTextChangeListener(editTextFirstName)
        subscribeForTextChangeListener(editTextLastName)
        subscribeForTextChangeListener(editTextCity)
        subscribeForTextChangeListener(editTextEmail)
    }

    private fun subscribeForTextChangeListener(editText: EditText) {
        val subscription = RxTextView.afterTextChangeEvents(editText)
            .debounce(200, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { textViewAfterTextChangeEvent: TextViewAfterTextChangeEvent? ->
                setSaveButtonEnabled(
                    isValid()
                )
            }
        cardValidationSubs.add(subscription)
    }

    private fun setSaveButtonEnabled(isEnabled: Boolean) {
        if (!isEnabled) {
            val alphaComponent =
                ColorUtils.setAlphaComponent(confHelper.accentThemeBacgroundColor, 26)
            buttonSave.backgroundTintList = ColorStateList.valueOf(alphaComponent)
        } else {
            buttonSave.backgroundTintList =
                ColorStateList.valueOf(confHelper.accentThemeBacgroundColor)
        }
        buttonSave.isEnabled = isEnabled
    }

    private fun setVisibilityOfAdditionCCFields(visibility: Int) {
        textInputLayoutFirstName.visibility = visibility
        textInputLayoutLastName.visibility = visibility
        textInputLayoutCity.visibility = visibility
        textInputLayoutEmail.visibility = visibility

        spinnerCountry.visibility = visibility
        if (visibility == View.VISIBLE && countryCode != null && countryCode == "US")
            spinnerStateUS.visibility = View.VISIBLE
        else
            spinnerStateUS.visibility = View.GONE
    }

    private fun isValid(): Boolean {
        val areFieldsValid = (
                cardNumberValidator.isValid() &&
                        cardExpirationValidator.isValid() &&
                        cardCVVValidator.isValid() &&
                        zipCodeValidator.isValid() &&
                        addressValidator.isValid()
                )
        val areAdditionalFieldsValid = (
                firstNameValidator.isValid() &&
                        lastNameValidator.isValid() &&
                        emailValidator.isValid() &&
                        cityValidator.isValid()
                )
        return if (confHelper.isAdditionalCCFieldsRequired) {
            areFieldsValid && areAdditionalFieldsValid && isCardTypeSupported
        } else {
            areFieldsValid && isCardTypeSupported
        }
    }

    private fun addPaymentCard() {
        if (isOnline()) {
            if (isValid()) {
                var cardNumber = editTextCard.text.toString()
                cardNumber = cardNumber.replace("\\s+".toRegex(), "")
                val cardMonth = editTextExpiration.text.toString().substring(0, 2)
                val cardYear = "20" + editTextExpiration.text.toString().substring(3)
                val cardCvv = editTextCVV.text.toString()
                val cardZipCode = editTextZipCode.text.toString()
                val cardAddress = editTextAddress.text.toString().trim { it <= ' ' }
                val cardLabel = editTextLabel.text.toString().trim { it <= ' ' }
                val cardFirstName = editTextFirstName.text.toString().trim { it <= ' ' }
                val cardLastName = editTextLastName.text.toString().trim { it <= ' ' }
                val cardCity = editTextCity.text.toString().trim { it <= ' ' }
                val cardEmail = editTextEmail.text.toString().trim { it <= ' ' }
                val state: String = if (countryCode == null || countryCode != "US") {
                    editTextState.text.toString().trim { it <= ' ' }
                } else {
                    stateCodeOrName!!
                }
                hideKeyboard()
                // emptyStateLayout.showLoading(R.drawable.payment_card_material, R.string.payment_method_saving)
                viewModel.addCard(
                    cardNumber, cardMonth, cardYear, cardCvv, cardZipCode, cardAddress, cardLabel,
                    cardFirstName, cardLastName, cardCity, countryCode, state, cardEmail
                )?.observe(this, Observer { onCardAddResult(it) })
            } else {
                cardNumberValidationListener(true)
                initExpirationValidationListener(true)
                cardCVVValidationListener(true)
                initFirstNameListener(true)
                initLastNameListener(true)
                initAddressListener(true)
                initCityListener(true)
                initEmailListener(true)
            }
        } else {
            connectionErrorDialog { }
        }
    }

    private fun onCardAddResult(result: Result<PaymentMethods>?) {
        result?.let {
            when (it) {
                is Result.Loading -> {
                    emptyStateLayout?.showLoading(
                        R.drawable.ic_payment_card_filled,
                        R.string.payment_method_saving
                    )
                }
                is Result.Success -> {
                    emptyStateLayout?.showContent()
                    goBackToPreviousActivity()
                }
                is Result.Failure -> {
                    emptyStateLayout?.showContent()
                    handleError(it.bmError[0])
                    analyticsPlatformAdapter.paymentMethodAdded(
                        AnalyticsPlatformsContract.PaymentType.CARD, "",
                        AnalyticsPlatformsContract.Status.FAILURE, it.bmError.first().message
                    )
                }
                else -> {
                }
            }
        }
    }

    private fun goBackToPreviousActivity() {
        analyticsPlatformAdapter.paymentMethodAdded(
            AnalyticsPlatformsContract.PaymentType.CARD,
            "",
            AnalyticsPlatformsContract.Status.SUCCESS,
            ""
        )
        view?.announceForAccessibility(getString(R.string.payment_successfully_added_a_new_card))
        val intent = Intent()
        intent.putExtra(
            AppConstants.SHOPPING_CART_INTENT,
            activity!!.intent.getBooleanExtra(AppConstants.SHOPPING_CART_INTENT, false)
        )
        activity!!.setResult(AppConstants.ADD_CARD_SUCCESS_CODE, intent)
        activity!!.finish()
    }

    private fun setCursorOnEndInAccessibilityMode() {
        context?.setCursorForEditTextInAccessibilityMode(
            mutableListOf(
                editTextLabel,
                editTextEmail,
                editTextZipCode,
                editTextState,
                editTextCountry,
                editTextCity,
                editTextAddress,
                editTextLastName,
                editTextFirstName,
                editTextCVV,
                editTextExpiration,
                editTextCard
            )
        )
    }
}
