package co.bytemark.add_payment_card.nmi

import android.annotation.SuppressLint
import android.app.Activity
import android.net.http.SslError
import android.os.Bundle
import android.view.View
import android.webkit.*
import androidx.appcompat.app.AlertDialog
import co.bytemark.BuildConfig
import co.bytemark.R
import co.bytemark.webview.WebViewFragment
import co.bytemark.widgets.emptystateview.EmptyStateLayout
import timber.log.Timber
import java.util.*


class NMIAddCardWebViewFragment : WebViewFragment() {

    lateinit var emptyStateLayout: EmptyStateLayout

    companion object {

        private const val NMI_URL = "${BuildConfig.OVERTURE_URL}nmiform"
        const val TITLE = "Add Card"

        @JvmStatic
        fun newInstance() =
                NMIAddCardWebViewFragment().apply {
                    arguments = Bundle().apply {
                        putString(WEBVIEW_URL, NMI_URL)
                        putString(WEBVIEW_TITLE, TITLE)
                        putBoolean(WEBVIEW_ADD_DEFAULT_HEADERS, true)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        emptyStateLayout = view.findViewById(R.id.emptyStateLayout)
    }

    override fun onOnline() {
        emptyStateLayout.showContent()
    }

    override fun onOffline() {
        emptyStateLayout.showEmpty(
                R.drawable.error_material,
                getString(R.string.network_connectivity_error),
                getString(R.string.network_connectivity_error_message))
    }

    override fun getLayoutRes() = R.layout.fragment_nmi_add_card

    override fun getWebViewClient(): WebViewClient {
        return object : WebViewClient() {

            /**
             * Report an error to the host application. These errors are unrecoverable
             * (i.e. the menu_main resource is unavailable). The errorCode parameter
             * corresponds to one of the ERROR_* constants.
             *
             * @param view        The WebView that is initiating the callback.
             * @param errorCode   The error code corresponding to an ERROR_* value.
             * @param description A String describing the error.
             * @param failingUrl  The url that failed to load.
             */
            @SuppressLint("BinaryOperationInTimber")
            @Deprecated("""Use {@link #onReceivedError(WebView, WebResourceRequest, WebResourceError)
             * onReceivedError(WebView, WebResourceRequest, WebResourceError)} instead.""")
            override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                hideLoading()
                super.onReceivedError(view, errorCode, description, failingUrl)
                Timber.i("onReceivedError() called with: errorCode = [$errorCode], description = [$description], failingUrl = [$failingUrl]")
            }

            override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
                hideLoading()
                val builder = AlertDialog.Builder(activity!!)
                builder.setMessage(R.string.login_popup_notification_error_ssl_cert_invalid)
                builder.setPositiveButton(R.string.continuee) { dialog, which -> handler.proceed() }
                builder.setNegativeButton(R.string.popup_cancel) { dialog, which -> handler.cancel() }
                val dialog = builder.create()
                dialog.show()
            }

            /**
             * Report web resource loading error to the host application. These errors usually indicate
             * inability to connect to the server. Note that unlike the deprecated version of the callback,
             * the new version will be called for any resource (iframe, image, etc), not just for the menu_main
             * page. Thus, it is recommended to perform minimum required work in this callback.
             *
             * @param view    The WebView that is initiating the callback.
             * @param request The originating request.
             * @param error   Information about the error occured.
             */
            override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
                super.onReceivedError(view, request, error)
                hideLoading()
                Timber.e("onReceivedError() called with: view = [ %s ], request = [%s], error = [%s]", view, request, error)
            }

            /**
             * Notify the host application that an HTTP error has been received from the server while
             * loading a resource.  HTTP errors have status codes &gt;= 400.  This callback will be called
             * for any resource (iframe, image, etc), not just for the menu_main page. Thus, it is recommended to
             * perform minimum required work in this callback. Note that the content of the server
             * response may not be provided within the **errorResponse** parameter.
             *
             * @param view          The WebView that is initiating the callback.
             * @param request       The originating request.
             * @param errorResponse Information about the error occured.
             */
            override fun onReceivedHttpError(view: WebView, request: WebResourceRequest, errorResponse: WebResourceResponse) {
                super.onReceivedHttpError(view, request, errorResponse)
                hideLoading()
                Timber.e("onReceivedHttpError() called with: view = [%s], request = [%s], errorResponse = [%s]", view, request, errorResponse)
            }

            override fun onReceivedHttpAuthRequest(view: WebView, handler: HttpAuthHandler, host: String, realm: String) {
                handler.proceed("srvc_upuat_access", "2\$Hear\$Hole\$Note$3")
            }

            override fun onPageFinished(view: WebView, url: String) {
                Timber.i("URL: %s", url)
                hideLoading()
                if (url.contains("result")) {
                    /* Split the URL into an array */
                    val regEx = "[=#&?]".toRegex()
                    val urlSplit = url.split(regEx).toTypedArray()
                    var state = "failure"
                    urlSplit.forEachIndexed { index, str ->
                        if (str == "result") {
                            state = urlSplit[index + 1]
                            return@forEachIndexed
                        }
                    }
                    if (state.toLowerCase(Locale.getDefault()) == "success") {
                        activity?.setResult(Activity.RESULT_OK)
                        activity?.finish()
                    } else {
                        // payment add failed, keep the user in same page.
                        // Note: Need to check the UnAuthorized issue on this case.
                        return
                    }
                } else {
                    super.onPageFinished(view, url)
                }
            }
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun applyWebViewSettings(settings: WebSettings?) {
        settings?.apply {
            cacheMode = WebSettings.LOAD_DEFAULT
            loadWithOverviewMode = true
            setAppCacheEnabled(true)
            databaseEnabled = true
            useWideViewPort = true
            domStorageEnabled = true
            javaScriptEnabled = true
            builtInZoomControls = true
            displayZoomControls = true
            setGeolocationEnabled(true)
            javaScriptCanOpenWindowsAutomatically = true
        }

    }
}