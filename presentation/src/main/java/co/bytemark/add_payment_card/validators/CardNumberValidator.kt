package co.bytemark.add_payment_card.validators

import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.Spanned
import co.bytemark.add_payment_card.Helper.CreditCardHelper
import co.bytemark.widgets.util.getDigitsOnly
import io.card.payment.CardType

/** Created by Santosh on 2019-06-05.
 */

class CardNumberValidator() : TextValidator() {

    private var number: String = ""

    private var deleteSpace: Int = 0

    constructor(number: String) : this() {
        this.number = number
    }

    override fun getTextValue(): String =
            number

    override fun isTextValid(): Boolean =
            hasFullLengthText() && CreditCardHelper.passesLuhnChecksum(number)

    override fun hasFullLengthText(): Boolean {
        if (number.isEmpty()) return false

        val cardType = CardType.fromCardNumber(number)
        return number.length == cardType.numberLength()
    }

    override fun isValid(): Boolean =
            hasFullLengthText() && isTextValid()


    override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned,
                        dstart: Int, dend: Int
    ): CharSequence {
        val updatedDigits =
                SpannableStringBuilder(dest)
                        .replace(dstart, dend, source, start, end)
                        .toString()
                        .getDigitsOnly()
        val maxLength = CardType.fromCardNumber(updatedDigits).numberLength()

        if (updatedDigits.length > maxLength) {
            return ""
        }

        val result = SpannableStringBuilder(source)

        val spacers = if (maxLength == 15) AMEX_SPACE else REGULAR_SPACE

        val replen = dend - dstart

        var endValue = end
        for (spacer in spacers) {
            if (source.isEmpty() && dstart == spacer && dest.get(dstart) == ' ') {
                deleteSpace = spacer
            }
            if (dstart - replen <= spacer && dstart + endValue - replen >= spacer) {
                val loc = spacer - dstart
                if (loc == end || loc in 0 until end && result[loc] != ' ') {
                    result.insert(loc, " ")
                    endValue++
                }
            }
        }

        return result
    }

    override fun afterTextChanged(editable: Editable?) {
        if (editable.isNullOrEmpty()) return

        number = editable.toString().getDigitsOnly()
        val cardType = CardType.fromCardNumber(number)

        if (deleteSpace > 1) {
            val a = deleteSpace
            val b = deleteSpace - 1
            deleteSpace = 0

            if (a > b) {
                editable.delete(b, a)
            }
        }

        var i = 0
        while (i < editable.length) {
            val character = editable[i]
            if (cardType.numberLength() == 15 && (i == 4 || i == 11) || (cardType.numberLength() == 16 || cardType.numberLength() == 14) && (i == 4 || i == 9 || i == 14)) {
                if (character != ' ') {
                    editable.insert(i, " ")
                }
            } else if (character == ' ') {
                editable.delete(i, i + 1)
                i--
            }
            i++
        }
    }

    companion object {
        val AMEX_SPACE = arrayOf(4, 11)
        val REGULAR_SPACE = arrayOf(4, 9, 14)
    }
}