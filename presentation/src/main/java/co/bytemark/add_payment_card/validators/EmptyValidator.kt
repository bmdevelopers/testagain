package co.bytemark.add_payment_card.validators

import android.text.Editable
import android.text.Spanned

/** Created by Santosh on 2019-06-05.
 */

open class EmptyValidator : TextValidator() {

    private var value: String = ""

    override fun afterTextChanged(s: Editable?) {
        value = s.toString().trim()
    }

    override fun getTextValue(): String = value

    override fun isTextValid(): Boolean = value.isNotEmpty()

    override fun hasFullLengthText(): Boolean = this.isTextValid()

    override fun isValid(): Boolean = hasFullLengthText() && isTextValid()

    override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int): CharSequence? {
        for (index in start until end) {

            val type = source?.get(index)?.let { Character.getType(it) }

            if (type == Character.SURROGATE.toInt() || type == Character.OTHER_SYMBOL.toInt()) {
                return ""
            }
        }
        return null
    }
}