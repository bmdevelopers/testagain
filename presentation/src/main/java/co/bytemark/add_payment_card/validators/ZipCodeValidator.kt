package co.bytemark.add_payment_card.validators

import android.text.Spanned

/** Created by Santosh on 2019-06-05.
 */

class ZipCodeValidator(private val maxLength: Int, private val minLength: Int) : EmptyValidator() {

    override fun isTextValid(): Boolean =
            super.isTextValid() && getTextValue().length <= maxLength && getTextValue().length >= minLength

    override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int): CharSequence? {
        for (i in end - 1 downTo start) {
            val currentChar = source?.get(i)
            currentChar?.let {
                if (!Character.isLetterOrDigit(it) && !Character.isSpaceChar(it) && Character.toString(it) != "-") {
                    return ""
                }
            }
        }
        return null
    }
}