package co.bytemark.add_payment_card.validators

import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.Spanned
import co.bytemark.add_payment_card.Helper.CreditCardHelper
import java.util.*

/** Created by Santosh on 16/04/20.
 * Copyright (c) 2020 Bytemark Inc. All rights reserved.
 */

class CardExpirationValidator() : TextValidator() {
    private var month: Int = 0
    private var year: Int = 0
    private var fullLength: Boolean = false

    constructor(month: Int, year: Int) : this() {
        this.month = month
        this.year = year
        fullLength = (month > 0 && year > 0)

        // Accept 2-digit years.
        if (year < 2000) this.year += 2000
    }

    override fun afterTextChanged(s: Editable?) {
        fullLength = s!!.length >= 5

        val dateStr = s.toString()
        val expiry = CreditCardHelper.getDate(dateStr) ?: return

        month = expiry.month + 1 // Java months are 0-11
        year = expiry.year

        if (year < 1900) year += 1900
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        month = 0
        year = 0
        fullLength = false
    }

    override fun getTextValue(): String =
            String.format("%02d/%02d", month, year % 100)

    override fun isTextValid(): Boolean {
        if (month < 1 || 12 < month) {
            return false
        }

        val now = Date()

        return year <= 1900 + now.year + 15 && (year > 1900 + now.year || year == 1900 + now.year && month >= now.month + 1)
    }

    override fun hasFullLengthText(): Boolean =
            fullLength

    override fun isValid(): Boolean =
            hasFullLengthText() && isTextValid()

    override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int): CharSequence {
        val result = SpannableStringBuilder(source)

        var end1 = end
        if (dstart == 0 && result.isNotEmpty() && '1' < result[0] && result[0] <= '9') {
            result.insert(0, "0")
            end1++
        }

        val replen = dend - dstart
        if (dstart - replen <= 2 && dstart + end1 - replen >= 2) {
            val loc = 2 - dstart
            if (loc == end1 || loc in 0 until end1 && result[loc] != '/') {
                result.insert(loc, "/")
                end1++
            }
        }

        val updated = SpannableStringBuilder(dest).replace(dstart, dend, result, start, end)
                .toString()

        if (updated.isNotEmpty()) {
            if (updated[0] < '0' || '1' < updated[0]) {
                return ""
            }
        }

        if (updated.length >= 2) {
            if (updated[0] != '0' && updated[1] > '2') {
                return ""
            }
            if (updated[0] == '0' && updated[1] == '0') {
                return ""
            }
        }

        return if (updated.length > 5) {
            ""
        } else result

    }
}