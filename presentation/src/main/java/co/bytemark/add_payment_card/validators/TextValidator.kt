package co.bytemark.add_payment_card.validators

import android.text.InputFilter
import co.bytemark.add_payment_card.Helper.TextWatcherHelper

abstract class TextValidator : TextWatcherHelper(), InputFilter {
    abstract fun getTextValue(): String

    abstract fun isTextValid(): Boolean

    abstract fun hasFullLengthText(): Boolean

    abstract fun isValid(): Boolean
}