package co.bytemark.add_payment_card.validators

import android.text.Editable
import android.text.Spanned

class CardCvvValidator(var requiredLength: Int) : TextValidator() {

    private var value: String = ""

    override fun getTextValue() =
            value

    override fun afterTextChanged(s: Editable?) {
        value = s.toString()
    }

    override fun isTextValid() =
            value.length == requiredLength

    override fun isValid() =
            this.isTextValid()

    override fun hasFullLengthText() =
            this.isTextValid()

    override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?,
                        dstart: Int, dend: Int
    ): CharSequence? =
            if (end > 0 && dest != null && dest.length + dend - dstart + end > requiredLength) {
                ""
            } else {
                null
            }
}