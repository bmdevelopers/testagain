package co.bytemark.add_payment_card

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import co.bytemark.domain.interactor.payments.AddPaymentMethodV2UseCase
import co.bytemark.domain.interactor.payments.AddPaymentMethodV2UseCase.AddPaymentMethodUseCaseValue
import co.bytemark.domain.model.common.Result
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.BMError
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.model.payment_methods.BraintreePaypalPaymentMethod
import co.bytemark.sdk.model.payment_methods.PaymentMethods
import co.bytemark.sdk.network_impl.BMNetwork
import co.bytemark.sdk.network_impl.BaseNetworkRequestCallback
import co.bytemark.sdk.post_body.PostCard
import co.bytemark.sdk.post_body.PostPaymentMethod
import java.util.*
import javax.inject.Inject

class AddPaymentCardViewModel @Inject constructor(
        private val confHelper: ConfHelper,
        private val addPaymentMethodV2UseCase: AddPaymentMethodV2UseCase,
        private val bmNetwork: BMNetwork
) : ViewModel() {

    fun addCard(cardNumber: String,
                cardMonth: String,
                cardYear: String,
                cardCvv: String,
                cardZipCode: String,
                cardAddress: String,
                cardLabel: String,
                cardFirstName: String,
                cardLastName: String,
                cardCity: String,
                countryCode: String?,
                state: String,
                cardEmail: String
    ): LiveData<Result<PaymentMethods>>? {
        val postCard = PostCard()
        postCard.nickname = cardLabel
        postCard.cardNumber = cardNumber
        postCard.expirationMonth = cardMonth
        postCard.expirationYear = cardYear
        postCard.cvv = cardCvv
        postCard.billingPostalCode = cardZipCode
        postCard.billingAddress = cardAddress

        postCard.firstName = cardFirstName
        postCard.billingCountry = countryCode
        postCard.lastName = cardLastName
        postCard.billingCity = cardCity
        postCard.billingTerritory = state
        postCard.billingEmail = cardEmail
        postCard.billingFullName = "$cardFirstName $cardLastName"

        if (confHelper.isV2PaymentMethodsEnabled) {
            val paymentMethod = PostPaymentMethod(
                    ideal = false,
                    payNearMe = false,
                    googlePay = false,
                    card = postCard,
                    paypalPaymentMethod = BraintreePaypalPaymentMethod("")
            )
            val requestValues = AddPaymentMethodUseCaseValue(paymentMethod)
            return addPaymentMethodV2UseCase.getLiveData(requestValues)
        } else {
            val result = MediatorLiveData<Result<PaymentMethods>>()
            result.postValue(Result.Loading(null))

            bmNetwork.postPaymentMethod(postCard, object : BaseNetworkRequestCallback {
                override fun onNetworkRequestSuccessfullyCompleted() {}
                override fun <T> onNetworkRequestSuccessWithResponse(listOf: ArrayList<T>) {}
                override fun onNetworkRequestSuccessWithResponse(card: Any) {
                    result.postValue(Result.Success(PaymentMethods()))
                }

                override fun onNetworkRequestSuccessWithUnexpectedError() {
                    result.postValue(Result.Failure(arrayListOf(
                            co.bytemark.domain.model.common.BMError(0, ""))))
                }

                override fun onNetworkRequestSuccessWithError(error: BMError) {
                    result.postValue(Result.Failure(arrayListOf(
                            co.bytemark.domain.model.common.BMError(error.code, error.message))))
                }

                override fun onNetworkRequestSuccessWithDeviceTimeError() {
                    result.postValue(Result.Failure(arrayListOf(
                            co.bytemark.domain.model.common.BMError(
                                    BytemarkSDK.ResponseCode.TIME_INCONSISTENCY_DETECTED, ""))))
                }
            })
            return result
        }
    }
}