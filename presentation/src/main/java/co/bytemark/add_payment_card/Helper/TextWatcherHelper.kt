package co.bytemark.add_payment_card.Helper

import android.text.Editable
import android.text.TextWatcher

/** Created by Santosh on 11/04/19.
 * Adapter class to implement all methods from {@link android.text.TextWatcher} enabling us to
 * implement only what is needed, instead of having empty methods.
 */

open class TextWatcherHelper : TextWatcher {
    override fun afterTextChanged(s: Editable?) {}

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
}