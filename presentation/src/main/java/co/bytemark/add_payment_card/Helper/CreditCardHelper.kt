package co.bytemark.add_payment_card.Helper

import android.annotation.SuppressLint
import co.bytemark.widgets.util.getDigitsOnly
import io.card.payment.CreditCard
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/** Created by Santosh on 11/04/19.
 */

class CreditCardHelper {

    companion object CreditCardHelper {

        fun passesLuhnChecksum(number: String): Boolean {
            var even = 0
            var sum = 0

            val sums = arrayOf(intArrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9), intArrayOf(0, 2, 4, 6, 8, 1, 3, 5, 7, 9))

            for (char in number.reversed()) {
                if (!Character.isDigit(char)) {
                    return false
                }
                val cInt = char - '0'
                sum += sums[even++ and 0x1][cInt]
            }
            return sum % 10 == 0
        }

        fun getDate(dateString: String): Date? {
            val digitsOnly = dateString.getDigitsOnly()
            val simpleDateFormat = getDateFormat(digitsOnly.length)
            if (simpleDateFormat != null) {
                return try {
                    simpleDateFormat.isLenient = false
                    simpleDateFormat.parse(digitsOnly)
                } catch (pe: ParseException) {
                    null
                }
            }
            return null
        }

        @SuppressLint("SimpleDateFormat")
        fun getDateFormat(length: Int): SimpleDateFormat? {
            return when (length) {
                4 -> SimpleDateFormat("MMyy")
                6 -> SimpleDateFormat("MMyyyy")
                else -> null
            }
        }

        fun isDateValid(expiryMonth: Int, expiryYear: Int): Boolean {
            if (expiryMonth < 1 || 12 < expiryMonth) return false

            val now = Calendar.getInstance()
            val thisYear = now.get(Calendar.YEAR)
            val thisMonth = now.get(Calendar.MONTH) + 1

            return expiryYear >= thisYear && !(expiryYear == thisYear && expiryMonth < thisMonth) && expiryYear <= thisYear + CreditCard.EXPIRY_MAX_FUTURE_YEARS
        }

        fun isDateValid(dateString: String): Boolean {
            val dateFormat = getDateFormat(dateString.getDigitsOnly().length)
            dateFormat ?: return false
            return try {
                dateFormat.isLenient = false
                val calendar = Calendar.getInstance()
                calendar.time = dateFormat.parse(dateString.getDigitsOnly())
                isDateValid(calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.MONTH) + 1)
            } catch (e: ParseException) {
                false
            }
        }
    }
}