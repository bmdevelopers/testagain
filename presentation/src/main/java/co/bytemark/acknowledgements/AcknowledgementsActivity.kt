package co.bytemark.acknowledgements

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.sdk.model.common.Action
import com.mikepenz.aboutlibraries.LibsBuilder


class AcknowledgementsActivity : MasterActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setNavigationViewCheckedItem(Action.SETTINGS)
        setToolbarTitle(R.string.settings_acknowledgements)

        supportFragmentManager.beginTransaction()
                .add(R.id.fragment, getLibraryFragment())
                .commit()
    }

    private fun getLibraryFragment() =
            LibsBuilder()
                    .withFields(R.string::class.java.fields)
                    .withAboutIconShown(true)
                    .withAboutVersionShown(true)
                    .withActivityTitle(getString(R.string.settings_acknowledgements))
                    .withAboutAppName(getString(R.string.app_name))
                    .withSortEnabled(true)
                    .supportFragment()

    override fun useHamburgerMenu() = false

    override fun getLayoutRes() =
            R.layout.activity_acknowledgements
}