package co.bytemark.acknowledgements

import java.io.*

internal class Test {
    fun testing() {
        val file = File("fastlane/FastfileTemp")
        val tempFile = File("fastlane/FastfileTemp")
        val reader = BufferedReader(FileReader(file))
        val writer = BufferedWriter(FileWriter(tempFile))
        val lineToModify = "app"
        val firebaseAppId = ""
        var currentLine: String
        while (reader.readLine().also { currentLine = it } != null) {
            val trimmedLine = currentLine.trim { it <= ' ' }
            if (trimmedLine.startsWith(lineToModify)) {
                val lineToReplace = "app: \"$firebaseAppId\","
                writer.write(lineToReplace + System.getProperty("line.separator"))
                continue
            }
            writer.write(currentLine + System.getProperty("line.separator"))
        }
    }
}