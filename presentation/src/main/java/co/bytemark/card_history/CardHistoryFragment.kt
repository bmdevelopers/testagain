package co.bytemark.card_history

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.lifecycle.Observer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.CustomerMobileApp
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.base.BaseMvvmFragment
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.fare_medium.Transaction
import co.bytemark.widgets.util.getViewModel
import co.bytemark.widgets.util.withArgs
import kotlinx.android.synthetic.main.fragment_card_history.*
import javax.inject.Inject

class CardHistoryFragment : BaseMvvmFragment() {

    var fareMedia: FareMedium? = null

    companion object {
        fun newInstance(fareMedia: FareMedium?) = CardHistoryFragment().withArgs {
            putParcelable("fareMedia", fareMedia)
        }
    }

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    @Inject
    lateinit var cardHistoryAdapter: CardHistoryAdapter

    lateinit var viewModel: CardHistoryViewModel

    private val transactions: MutableList<Transaction?> = ArrayList()

    override fun onInject() = component.inject(this)

    private var pageIndex = 0
    private var isLoading = false
        private set(value) {
            field = value
            progressBar?.visibility = if (field) View.VISIBLE else View.GONE
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_card_history, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = getViewModel(CustomerMobileApp.appComponent.cardHistoryViewModel)
        fareMedia = arguments?.getParcelable("fareMedia")
        textViewFareMediaNickname?.text = fareMedia?.nickname
        textViewId?.text =
            String.format(getString(R.string.fare_medium_id), fareMedia?.printedCardNumber)
        setRecyclerViewAndPageIndex()
        initialLoadTransactions(pageIndex)
        observeData()
    }

    private fun observeData() {
        observeErrorLiveData()
        observeDisplayLiveData()
        observeTransactionLiveData()
    }

    private fun observeTransactionLiveData() {
        viewModel.transactionsLiveData.observe(this, Observer {
            loadTransactionsToAdapter(it)
        })
    }

    fun loadTransactionsToAdapter(transactionsList: MutableList<Transaction?>?) {
        transactions.addAll(transactionsList!!)
        cardHistoryAdapter.submitList(transactions)
    }

    private fun observeDisplayLiveData() {
        viewModel.displayLiveData.observe(this, Observer {
            when (it) {
                is Display.ProgressBar -> {
                    isLoading = it.visibility == View.VISIBLE
                }
                is Display.EmptyState.Loading -> {
                    emptyStateLayout.showLoading(
                        R.drawable.available_passes_empty_state,
                        R.string.loading
                    )
                }
                is Display.EmptyState.Error -> {
                    analyticsPlatformAdapter.cardHistoryScreenLoaded(
                        AnalyticsPlatformsContract.Status.FAILURE,
                        ""
                    )
                    emptyStateLayout?.showError(
                        it.errorImageDrawable,
                        getString(it.errorTextTitle),
                        it.errorTextContent?.let { it1 -> getString(it1) },
                        it.errorButtonText?.let { it1 -> getString(it1) }) { _ ->
                        if (it.errorButtonText == R.string.autoload_retry) {
                            if (pageIndex == 0) {
                                initialLoadTransactions(pageIndex)
                            } else {
                                loadTransactions(pageIndex)
                            }
                        } else if (it.errorButtonText == R.string.use_tickets_go_back) {
                            activity?.finish()
                        }
                    }
                }
                is Display.EmptyState.ShowContent -> {
                    emptyStateLayout.showContent()
                    analyticsPlatformAdapter.cardHistoryScreenLoaded(
                        AnalyticsPlatformsContract.Status.SUCCESS,
                        ""
                    )
                }
            }
        })
    }

    private fun observeErrorLiveData() {
        viewModel.errorLiveData.observe(this, Observer {
            if (isOnline()) {
                it?.let {
                    analyticsPlatformAdapter.cardHistoryScreenLoaded(
                        AnalyticsPlatformsContract.Status.FAILURE,
                        it.message
                    )
                    handleError(it)
                }
            } else {
                viewModel.showOfflineEmptyLayout()
            }
        })
    }

    private fun setRecyclerViewAndPageIndex() {
        recyclerViewTransactions?.adapter = cardHistoryAdapter
        recyclerViewTransactions?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager?
                val position = layoutManager?.findLastVisibleItemPosition()
                if (position == cardHistoryAdapter.itemCount - 1 && dy > 0) {
                    if (!isLoading) {
                        pageIndex += 1
                        loadTransactions(pageIndex)
                    }
                }
            }
        })
    }

    private fun initialLoadTransactions(pageIndex: Int) {
        fareMedia?.fareMediumId?.let {
            viewModel.getTransactionsList(it, pageIndex)
        }
    }

    private fun loadTransactions(pageIndex: Int) {
        fareMedia?.fareMediumId?.let {
            isLoading = true
            viewModel.getTransactionsList(it, pageIndex)
        }
    }

}