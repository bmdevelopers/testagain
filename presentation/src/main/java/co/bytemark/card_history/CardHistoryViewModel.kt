package co.bytemark.card_history

import androidx.lifecycle.MutableLiveData
import android.view.View
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.base.BaseViewModel
import co.bytemark.domain.interactor.manage.TransactionRequestValues
import co.bytemark.domain.interactor.manage.TransactionsUseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.fare_medium.Transaction
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import java.net.UnknownHostException
import javax.inject.Inject

class CardHistoryViewModel @Inject constructor(private val transactionsUseCase: TransactionsUseCase) : BaseViewModel() {

    init {
        CustomerMobileApp.component.inject(this)
    }

    private var hasTransactions = false

    val displayLiveData by lazy { MutableLiveData<Display>() }

    val transactionsLiveData by lazy { MutableLiveData<MutableList<Transaction?>>() }

    val handler = CoroutineExceptionHandler { _, exception ->
        if (exception is UnknownHostException)
            showOfflineEmptyLayout()
    }

    fun getTransactionsList(fareMediaId: String, pageIndex: Int) = uiScope.launch(handler) {
        if (pageIndex != 0)
            displayLiveData.value = Display.ProgressBar(View.VISIBLE)
        else
            displayLiveData.value = Display.EmptyState.Loading(R.drawable.available_passes_empty_state, R.string.loading)
        when (val result = transactionsUseCase(TransactionRequestValues(fareMediaId, pageIndex))) {
            is Result.Success -> {
                showTransaction(result.data?.transactions?.toMutableList() ?: mutableListOf())
            }
            is Result.Failure -> {
                displayLiveData.value = Display.EmptyState.Error(R.drawable.error_material, R.string.something_went_wrong, R.string.error_loading_message, R.string.autoload_retry)
                errorLiveData.value = result.bmError.first()
            }
        }
        displayLiveData.value = Display.ProgressBar(View.GONE)
    }

    private fun showTransaction(transactions: MutableList<Transaction?>?) {
        displayLiveData.value = Display.EmptyState.ShowContent()
        if (transactions?.isNotEmpty() == true) {
            hasTransactions = true
            transactionsLiveData.value = transactions
        } else if (!hasTransactions) {
            displayLiveData.value = Display.EmptyState.Error(R.drawable.error_material, R.string.autoload_no_card_history, null, R.string.use_tickets_go_back)
        }
    }

    fun showOfflineEmptyLayout() {
        displayLiveData.value = Display.EmptyState.Error(R.drawable.error_material, R.string.network_connectivity_error, R.string.network_connectivity_error_message, R.string.autoload_retry)
    }

}