package co.bytemark.card_history

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.R
import co.bytemark.card_history.CardHistoryAdapter.CardHistoryViewHolder
import co.bytemark.domain.model.fare_medium.Transaction
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.Api.ApiUtility
import kotlinx.android.synthetic.main.card_history_item.view.*
import javax.inject.Inject

class CardHistoryAdapter @Inject constructor(private val confHelper: ConfHelper) : ListAdapter<Transaction, CardHistoryViewHolder>(TransactionDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardHistoryViewHolder =
            LayoutInflater.from(parent.context).inflate(R.layout.card_history_item, parent, false).run { CardHistoryViewHolder(this) }

    override fun onBindViewHolder(holder: CardHistoryViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class CardHistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(transaction: Transaction) {
            itemView.apply {

                linearLayoutCardHistory?.setBackgroundColor(confHelper.dataThemeBackgroundColor)

                val date = ApiUtility.getCalendarFromS(transaction.time)?.time
                date?.let {
                    textViewTime?.text = String.format("%1\$s, %2\$s", confHelper.dateDisplayFormat.format(date), confHelper.timeDisplayFormat.format(date))
                }
                textViewDescription?.text = String.format(this.context.getString(R.string.card_history_transaction_description), transaction.description)
                textViewTransactionType?.text = transaction.transactionType

                if (transaction.location?.isEmpty() == true) {
                    textViewLocation?.visibility = View.GONE
                } else {
                    textViewLocation?.text = String.format(textViewLocation!!.resources.getString(R.string.card_history_location_card_history), transaction.location)
                }

                when {
                    transaction.amount == null -> {
                        textViewAmount.text = null
                    }
                    transaction.amount!!.compareTo(0) == 0 -> {
                        textViewAmount?.setTextColor(ContextCompat.getColor(this.context, R.color.orgDataPrimaryTextColor))
                        textViewAmount?.text = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(transaction.amount!!)
                    }
                    transaction.amount!!.compareTo(0) >= 1 -> {
                        textViewAmount?.setTextColor(ContextCompat.getColor(this.context, R.color.orgSuccessColor))
                        textViewAmount?.text = String.format("+%1\$s", confHelper.getConfigurationPurchaseOptionsCurrencySymbol(transaction.amount!!))
                    }
                    transaction.amount!!.compareTo(0) <= -1 -> {
                        textViewAmount?.setTextColor(ContextCompat.getColor(this.context, R.color.orgErrorColor))
                        textViewAmount?.text = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(transaction.amount!!)
                    }
                }
                if (transaction.balance == null) {
                    textViewBalance?.text = null
                } else {
                    textViewBalance?.text = confHelper.getConfigurationPurchaseOptionsCurrencySymbol(transaction.balance!!)
                }
            }
        }
    }

    class TransactionDiffCallback : DiffUtil.ItemCallback<Transaction>() {
        override fun areItemsTheSame(oldItem: Transaction, newItem: Transaction): Boolean = oldItem.transactionId == newItem.transactionId
        override fun areContentsTheSame(oldItem: Transaction, newItem: Transaction): Boolean = oldItem.transactionId == newItem.transactionId

    }
}

