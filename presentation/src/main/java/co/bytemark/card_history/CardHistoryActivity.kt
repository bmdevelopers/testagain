package co.bytemark.card_history

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.util.replaceFragment

class CardHistoryActivity : MasterActivity() {

    override fun getLayoutRes(): Int = R.layout.activity_card_history

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(intent?.extras?.getString(AppConstants.TITLE))
        replaceFragment(CardHistoryFragment.newInstance(intent?.extras?.getParcelable(AppConstants.FARE_MEDIA)), R.id.container)
    }

    override fun useHamburgerMenu(): Boolean = false
}