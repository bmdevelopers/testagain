package co.bytemark.formly.adapterdelegates

import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.formly.adapter.viewholder.InputViewHolder
import co.bytemark.helpers.ConfHelper
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.formly_input_item.view.*
import rx.subjects.PublishSubject

open class EditTextAdapterDelegate(
    confHelper: ConfHelper, private val textChanges: PublishSubject<Pair<Formly, String>>,
    private val formlyDelegatesValidator: FormlyDelegatesValidator
) : FormlyAdapterDelegate(confHelper) {

    override fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): FormlyViewHolder =
        EditTextViewHolder(layoutInflater.inflate(R.layout.formly_input_item, parent, false), textChanges, confHelper)

    override fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>): Boolean =
        formlyDelegatesValidator.editTextAdapterDelegate(formly)

    open class EditTextViewHolder(
        val view: View,
        textChanges: PublishSubject<Pair<Formly, String>>,
        confHelper: ConfHelper
    ) : InputViewHolder(view, textChanges, confHelper) {

        override fun textView(): TextView? = view.formlyInputTextView

        override fun textInputLayout(): TextInputLayout? = view.formlyInputTextInput

        override fun editText(): EditText? = view.formlyInputEditText

        override fun equalityError() {}
    }

}