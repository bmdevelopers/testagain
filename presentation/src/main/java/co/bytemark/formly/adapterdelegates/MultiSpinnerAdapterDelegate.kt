package co.bytemark.formly.adapterdelegates


import android.util.Pair
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.core.graphics.ColorUtils
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.authentication.TemplateOptions
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.formly.adapter.viewholder.InputViewHolder
import co.bytemark.helpers.ConfHelper
import co.bytemark.widgets.util.hide
import co.bytemark.widgets.util.invisible
import co.bytemark.widgets.util.show
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.formly_multi_spinner_item.view.*
import rx.subjects.PublishSubject


class MultiSpinnerAdapterDelegate(
    private val configHelper: ConfHelper, private val dropDownSelections: PublishSubject<Pair<Formly, String>>
    , private val formlyDelegatesValidator: FormlyDelegatesValidator?
) : FormlyAdapterDelegate(configHelper) {

    override fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup) =
        MultiSpinnerViewHolder(layoutInflater.inflate(R.layout.formly_multi_spinner_item, parent, false), dropDownSelections, configHelper)

    override fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>) =
        formlyDelegatesValidator?.multiSpinner(formly) ?: false

    class MultiSpinnerViewHolder(
        view: View,
        dropDownSelections: PublishSubject<Pair<Formly, String>>,
        confHelperX: ConfHelper
    ) : InputViewHolder(view, dropDownSelections, confHelperX) { //Extending InputViewHolder because it contains a child edit text

        private var childEditTextFormly: Formly? = null

        private var childSpinnerFormly: Formly? = null

        private val childSpinnerAdapter = ArrayAdapter<String>(view.context, R.layout.formly_spinner_item)

        override fun onBind(formly: Formly?, forms: List<Formly?>) {

            with(itemView) {

                childEditTextFormly = formly?.child?.firstOrNull { it.type == Formly.INPUT_TYPE }
                childSpinnerFormly = formly?.child?.firstOrNull { it.type == Formly.SELECT_TYPE }

                super.onBind(childEditTextFormly, forms) //Only For Input

                if (formly?.data != null && formly.data.allOptions != null) {

                    fun setUpSpinnerAndLabel(
                        spinner: Spinner?,
                        label: TextView?,
                        templateOptions: TemplateOptions?,
                        adapter: ArrayAdapter<String>,
                        onSpinnerItemSelected: (adapterView: AdapterView<*>?, view: View?, i: Int, l: Long) -> Unit
                    ) {
                        if (spinner == null || label == null || templateOptions == null)
                            return

                        label.text = templateOptions.label
                        label.setTextColor(ColorUtils.setAlphaComponent(confHelper.backgroundThemePrimaryTextColor, 122))

                        adapter.setDropDownViewResource(R.layout.formly_simple_spinner_dropdown_item)
                        spinner.adapter = adapter
                        spinner.isFocusableInTouchMode = true
                        spinner.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
                            if (hasFocus) {
                                label.text = getFormattedLabel(templateOptions)
                                v.performClick()
                                label.setTextColor(confHelper.accentThemeAccentColor)
                            } else {
                                label.setTextColor(ColorUtils.setAlphaComponent(confHelper.backgroundThemePrimaryTextColor, 122))
                            }
                        }


                        spinner.setOnTouchListener { v: View?, _: MotionEvent? ->
                            label.text = getFormattedLabel(templateOptions)
                            v?.performClick();
                            false
                        }

                        spinner.onItemSelectedListener = object : OnItemSelectedListener {
                            override fun onNothingSelected(parent: AdapterView<*>?) {
                                themeSelectedItem()
                            }

                            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                onSpinnerItemSelected(parent, view, position, id)
                            }
                        }
                    }

                    val parentSpinnerAdapter = ArrayAdapter(itemView.context, R.layout.formly_spinner_item,
                                                            formly.data.allOptions.map { it.name })

                    setUpSpinnerAndLabel(childSpinner, childLabel, childEditTextFormly?.templateOptions, childSpinnerAdapter) { _, _, i, _ ->
                        textChanges.onNext(Pair.create(childSpinnerFormly, formly.data.allOptions[parentSpinner!!.selectedItemPosition].items[i].id))
                    }

                    setUpSpinnerAndLabel(parentSpinner, parentLabel, formly.templateOptions, parentSpinnerAdapter) { _, _, i, _ ->
                        themeSelectedItem()
                        textChanges.onNext(Pair.create(formly, formly.data.allOptions[i].id))
                        onParentSpinnerItemSelected()
                    }

                }
            }
        }

        private fun onParentSpinnerItemSelected() {
            if (formly?.data?.allOptions?.get(itemView.parentSpinner!!.selectedItemPosition)?.items.isNullOrEmpty().not()) {
                showChildSpinner()
            } else {
                showChildEditText()
            }
        }

        private fun showChildSpinner() {

            with(itemView) {
                var positionToSelect = 0

                if (mapChildValueFromServerResponse) {
                    positionToSelect = getChildPositionFromResponseValue()
                    mapChildValueFromServerResponse = false
                }

                val childSpinnerOptions = formly?.data?.allOptions?.get(parentSpinner!!.selectedItemPosition)?.items

                if (!childSpinnerOptions.isNullOrEmpty() && positionToSelect > -1) {

                    childSpinner.show()
                    childLabel.show()
                    childInputLayout.hide()
                    itemView.childInputLayout?.editText?.text?.clear()
                    childSpinnerAdapter.clear()
                    childSpinnerAdapter.addAll(childSpinnerOptions.map { it.name })
                    childSpinnerAdapter.notifyDataSetChanged()
                    childSpinner?.setSelection(positionToSelect)

                    textChanges.onNext(
                        Pair(
                            childSpinnerFormly!!,
                            formly?.data?.allOptions?.get(parentSpinner!!.selectedItemPosition)?.items?.get(positionToSelect)?.id!!
                        )
                    )

                }
            }
        }

        private fun hideSpinnerLayout() {
            itemView.childSpinner.hide()
            itemView.childLabel.invisible()
        }

        private fun showChildEditText() {

            hideSpinnerLayout()
            itemView.childInputLayout.show()
            if (mapChildValueFromServerResponse) {
                itemView.childInputLayout?.editText?.setText(childValueResponse)
                mapChildValueFromServerResponse = false
            }

            textChanges.onNext(Pair(childEditTextFormly!!, itemView.childInputLayout?.editText?.text.toString()))

        }

        private var childValueResponse = ""

        private var mapChildValueFromServerResponse = false

        //From Server Response
        fun setParentSpinnerValue(id: String) {
            formly?.data?.allOptions?.indexOfFirst { it.id == id }?.let {
                if (it > -1) {
                    mapChildValueFromServerResponse = true

                    if (it == 0) { //For position 0, onItemSelectedListener won't be called so calling it manually
                        textChanges.onNext(Pair.create(formly, id))
                        onParentSpinnerItemSelected()
                    } else {
                        itemView.parentSpinner?.setSelection(it, true)
                    }

                }
            }
        }

        //From Server Response
        fun setChildValue(value: String) {
            childValueResponse = value
        }

        private fun getChildPositionFromResponseValue(): Int {
            formly?.data!!.allOptions[itemView.parentSpinner!!.selectedItemPosition]?.items?.let {
                return it.indexOfFirst { it.id == childValueResponse }
            }
            return -1
        }

        private fun getFormattedLabel(templateOptions: TemplateOptions): String? {
            return if (templateOptions.required != null && templateOptions.required == true) {
                templateOptions.label + "*"
            } else templateOptions.label
        }

        private fun themeSelectedItem() {
            with(itemView.parentSpinner) {
                if (selectedView != null && selectedView is TextView) {
                    (selectedView as TextView).setTextColor(confHelper.backgroundThemePrimaryTextColor)
                }
            }
        }

        override fun textInputLayout(): TextInputLayout = itemView.childInputLayout

        override fun editText(): EditText = itemView.childEditText

        override fun equalityError() {}

        override fun textView(): TextView? = itemView.textView

        override fun isValid(): Boolean {
            if (itemView.childInputLayout!!.visibility == View.VISIBLE) {
                super.isValid()
            }
            return true
        }

    }
}