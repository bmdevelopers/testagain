package co.bytemark.formly.adapterdelegates

import android.text.TextUtils
import android.text.util.Linkify
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.TextView
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.authentication.TemplateOptions
import co.bytemark.formly.FormlyUtil
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.formly.adapter.viewholder.InputViewHolder
import co.bytemark.helpers.ConfHelper
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.formly_check_box_item.view.*
import rx.subjects.PublishSubject

class CheckBoxAdapterDelegate(
    confHelper: ConfHelper,
    private val textChanges: PublishSubject<Pair<Formly, String>>,
    private val formlyDelegatesValidator: FormlyDelegatesValidator
) : FormlyAdapterDelegate(confHelper) {

    override fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): FormlyViewHolder {
        return CheckBoxViewHolder(layoutInflater.inflate(R.layout.formly_check_box_item, parent, false), textChanges, confHelper)
    }

    override fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>): Boolean {
        return formlyDelegatesValidator.checkBoxAdapterDelegate(formly)
    }

    internal class CheckBoxViewHolder(
        val view: View,
        textChanges: PublishSubject<Pair<Formly, String>>,
        confHelper: ConfHelper
    ) : InputViewHolder(view, textChanges, confHelper) {

        override fun textView(): TextView? = view.formlyCheckboxText

        override fun textInputLayout(): TextInputLayout? = TextInputLayout(view.context)

        override fun editText(): EditText? = EditText(view.context)

        override fun equalityError() {}
        override fun onBind(formly: Formly?, forms: List<Formly?>) {
            with(view) {
                super.setCheckBox(formlyCheckbox)
                super.setCheckBoxErrorHolderView(formlyCheckboxText)
                super.onBind(formly, forms)
                val templateOptions = formly?.templateOptions
                formlyCheckbox.setOnCheckedChangeListener { _: CompoundButton?, isChecked: Boolean ->
                    super.setCheckBoxState(isChecked)
                    if (isChecked) {
                        formlyCheckbox.error = null
                        formlyCheckboxText.setTextColor(confHelper.backgroundThemePrimaryTextColor)
                        formlyCheckboxText.text = templateOptions?.let { getCheckBoxDescription(it) } ?: ""
                        Linkify.addLinks(formlyCheckboxText, Linkify.ALL)
                    }
                }
                formlyCheckbox.isChecked = formly?.defaultValue ?: false
                if (templateOptions?.label != null) {
                    FormlyUtil.setTextViewHTML(formlyCheckbox, templateOptions.label!!)
                }
                val description = templateOptions?.description
                if (!TextUtils.isEmpty(description)) {
                    formlyCheckboxText.visibility = View.VISIBLE
                    formlyCheckboxText.text = description
                    Linkify.addLinks(formlyCheckboxText, Linkify.ALL)
                } else {
                    formlyCheckboxText.visibility = View.GONE
                }
            }
        }

        private fun getCheckBoxDescription(templateOptions: TemplateOptions): String? {
            return templateOptions.description
        }
    }

}