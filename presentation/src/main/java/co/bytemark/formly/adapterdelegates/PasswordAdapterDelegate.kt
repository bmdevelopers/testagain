package co.bytemark.formly.adapterdelegates

import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.formly.adapter.viewholder.InputViewHolder
import co.bytemark.helpers.ConfHelper
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.formly_password_item.view.*
import rx.subjects.PublishSubject

class PasswordAdapterDelegate(
    confHelper: ConfHelper, private val textChangesSub: PublishSubject<Pair<Formly, String>>,
    private val formlyDelegatesValidator: FormlyDelegatesValidator
) : FormlyAdapterDelegate(confHelper) {

    override fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): FormlyViewHolder =
        PasswordViewHolder(layoutInflater.inflate(R.layout.formly_password_item, parent, false), textChangesSub, confHelper)

    override fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>): Boolean =
        formlyDelegatesValidator.passwordAdapterDelegate(formly)

    inner class PasswordViewHolder internal constructor(
        val view: View,
        textChangesSub: PublishSubject<Pair<Formly, String>>,
        confHelper: ConfHelper
    ) : InputViewHolder(view, textChangesSub, confHelper) {

        override fun textView(): TextView = view.formlyPasswordTextView

        override fun textInputLayout(): TextInputLayout = view.formlyPasswordTextInput

        override fun editText(): EditText = view.formlyPasswordEditText

        override fun equalityError() {
            if (!view.formlyPasswordTextInput.isErrorEnabled) {
                view.formlyPasswordTextInput.error =
                    view.formlyPasswordTextInput.resources.getString(R.string.sign_in_password_equality_error)
            }
        }

        override fun onBind(formly: Formly?, forms: List<Formly?>) {
            super.onBind(formly, forms)
            with(view) {
                formlyPasswordTextInput.isPasswordVisibilityToggleEnabled = true
                formlyPasswordTextInput.passwordVisibilityToggleContentDescription =
                    formlyPasswordTextInput.context.getString(R.string.sign_up_show_voonly) + " " + view.formlyPasswordTextInput.hint
            }
        }
    }

}