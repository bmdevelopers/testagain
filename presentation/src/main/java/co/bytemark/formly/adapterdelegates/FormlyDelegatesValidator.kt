package co.bytemark.formly.adapterdelegates

import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.authentication.TemplateOptions
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FormlyDelegatesValidator @Inject constructor() {
    fun filterValidFormly(forms: MutableList<Formly>) {
        val iterator = forms.iterator()
        while (iterator.hasNext()) {
            val formly = iterator.next()
            if (!canProvideFormly(formly)) {
                iterator.remove()
            }
        }
    }

    /**
     * Given a [Formly] object, this method will return whether a AdapterDelegate implementation
     * can be provided so that it can be rendered in recyclerview. If it says false, then the formly
     * object should be corrected or a new adapter delegate should be added.
     *
     * @param formly Formly to check against.
     */
    private fun canProvideFormly(formly: Formly): Boolean {
        val valid = buttonAdapterDelegate(formly) || checkBoxAdapterDelegate(formly) ||
                datePickerAdapterDelegate(formly) || editTextAdapterDelegate(formly) ||
                passwordAdapterDelegate(formly) || emailAdapterDelegate(formly) ||
                phoneAdapterDelegate(formly) || spinner(formly) || numericInputAdapterDelegate(formly) ||
                switchAdapterDelegate(formly) || textAdapter(formly) || multiLineTextAdapter(formly) || multiSpinner(formly)
        if (!valid) {
            Timber.e("Invalid formly : %s", formly.toString())
        }
        return valid
    }

    fun buttonAdapterDelegate(formly: Formly?): Boolean {
        return formly?.type.equals(Formly.BUTTON_TYPE, ignoreCase = true)
    }

    fun checkBoxAdapterDelegate(formly: Formly?): Boolean {
        return formly?.type.equals(Formly.CHECKBOX_TYPE, ignoreCase = true)
    }

    fun datePickerAdapterDelegate(formly: Formly?): Boolean {
        return formly?.type.equals(Formly.DATE_PICKER_TYPE, ignoreCase = true)
    }

    fun editTextAdapterDelegate(formly: Formly?): Boolean {
        return ((formly?.type.equals(Formly.INPUT_TYPE, ignoreCase = true) || formly?.type
            .equals(Formly.MASKED_INPUT_TYPE, ignoreCase = true))
                && formly?.templateOptions?.type.equals(TemplateOptions.TEMPLATE_TYPE_INPUT, ignoreCase = true))
    }

    fun emailAdapterDelegate(formly: Formly?): Boolean {
        return (formly?.type.equals(Formly.INPUT_TYPE, ignoreCase = true)
                && formly?.templateOptions?.type.equals(TemplateOptions.TEMPLATE_TYPE_EMAIL, ignoreCase = true))
    }

    fun passwordAdapterDelegate(formly: Formly?): Boolean {
        return (formly?.type.equals(Formly.INPUT_TYPE, ignoreCase = true)
                && formly?.templateOptions?.type.equals(TemplateOptions.TEMPLATE_TYPE_PASSWORD, ignoreCase = true))
    }

    fun phoneAdapterDelegate(formly: Formly?): Boolean {
        return ((formly?.type.equals(Formly.INPUT_TYPE, ignoreCase = true) || formly?.type
            .equals(Formly.MASKED_INPUT_TYPE, ignoreCase = true))
                && formly?.templateOptions?.type.equals(TemplateOptions.TEMPLATE_TYPE_PHONE, ignoreCase = true))
    }

    fun numericInputAdapterDelegate(formly: Formly?): Boolean {
        return ((formly?.type.equals(Formly.INPUT_TYPE, ignoreCase = true) || formly?.type
            .equals(Formly.MASKED_INPUT_TYPE, ignoreCase = true))
                && formly?.templateOptions?.type.equals(TemplateOptions.TEMPLATE_TYPE_NUMERIC_INPUT, ignoreCase = true))
    }

    fun spinner(formly: Formly?): Boolean {
        return formly?.type.equals(Formly.SELECT_TYPE, ignoreCase = true)
    }

    fun multiSpinner(formly: Formly?): Boolean {
        return formly?.type.equals(Formly.MULTI_SELECT, ignoreCase = true)
    }

    fun switchAdapterDelegate(formly: Formly?): Boolean {
        return formly?.type.equals(Formly.SWITCH_TYPE, ignoreCase = true)
    }

    fun textAdapter(formly: Formly?): Boolean {
        return formly?.type.equals(Formly.TEXT_TYPE, ignoreCase = true)
    }

    fun multiLineTextAdapter(formly: Formly?): Boolean {
        return formly?.type.equals(Formly.INPUT_TYPE, ignoreCase = true) &&
                formly?.templateOptions?.type.equals(TemplateOptions.TEMPLATE_TYPE_MULTI_LINE_TEXT, ignoreCase = true)
    }
}