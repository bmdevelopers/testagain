package co.bytemark.formly.adapterdelegates

import android.content.res.ColorStateList
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.authentication.TemplateOptions
import co.bytemark.formly.FormlyUtil
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.formly.adapter.viewholder.LoadingViewHolder
import co.bytemark.helpers.ConfHelper
import co.bytemark.widgets.util.hide
import co.bytemark.widgets.util.show
import kotlinx.android.synthetic.main.formly_button_item.view.*
import rx.subjects.PublishSubject
import javax.inject.Inject

class ButtonAdapterDelegate(
    confHelper: ConfHelper,
    private val buttonClicks: PublishSubject<Formly>,
    private val formlyDelegatesValidator: FormlyDelegatesValidator
) : FormlyAdapterDelegate(confHelper) {

    override fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): FormlyViewHolder =
        ButtonViewHolder(layoutInflater.inflate(R.layout.formly_button_item, parent, false), confHelper)

    override fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>): Boolean =
        formlyDelegatesValidator.buttonAdapterDelegate(formly)

    inner class ButtonViewHolder(val view: View, private val confHelper: ConfHelper) : LoadingViewHolder(view) {

        init {
            component.inject(this)
        }

        @JvmField
        @Inject
        var analyticsPlatformAdapter: AnalyticsPlatformAdapter? = null

        override fun showLoading() {
            view.formlyButton.hide()
            view.formlyButtonProgressView.show()
        }

        override fun hideLoading() {
            view.formlyButton.show()
            view.formlyButtonProgressView.hide()
        }

        override fun onBind(formly: Formly?, forms: List<Formly?>) {
            with(view) {
                formlyButtonProgressView.setProgressColor(confHelper.backgroundThemeAccentColor, confHelper.backgroundThemeBackgroundColor)
                formlyButton.text = formly?.templateOptions?.label
                formlyButton.transitionName = formly?.key
                formlyButton.setOnClickListener {
                    buttonClicks.onNext(formly)
                }
                if (formly?.let { getmType(it).equals(TemplateOptions.TEMPLATE_TYPE_FLAT, ignoreCase = true) } == true) {
                    formlyButton.setTextAppearance(R.style.TextAppearance_AppCompat_Widget_Button_Borderless_Colored)
                    formlyButton.isAllCaps = false
                    formlyButton.setTextColor(ColorStateList.valueOf(confHelper.backgroundThemePrimaryTextColor))
                    formlyButton.background = null
                } else if (formly?.let { getmType(it).equals(TemplateOptions.TEMPLATE_TYPE_STADIUM, ignoreCase = true) } == true) {
                    FormlyUtil.themeButtonBackground(formlyButton.background, confHelper.backgroundThemeAccentColor)
                }
            }
        }

        private fun getmType(formly: Formly): String? {
            return formly.templateOptions.mType
        }
    }

}