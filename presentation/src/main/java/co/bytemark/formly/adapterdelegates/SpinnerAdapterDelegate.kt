package co.bytemark.formly.adapterdelegates

import android.annotation.SuppressLint
import android.util.Pair
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.core.graphics.ColorUtils
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.authentication.SelectOption
import co.bytemark.domain.model.authentication.TemplateOptions
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.helpers.ConfHelper
import kotlinx.android.synthetic.main.formly_filter_item.view.*
import rx.subjects.PublishSubject
import java.util.*

class SpinnerAdapterDelegate(
    confHelper: ConfHelper,
    private val dropDownSelections: PublishSubject<Pair<Formly, String>>,
    private val formlyDelegatesValidator: FormlyDelegatesValidator
) : FormlyAdapterDelegate(confHelper) {

    override fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): FormlyViewHolder =
        SpinnerViewHolder(layoutInflater.inflate(R.layout.formly_filter_item, parent, false))

    override fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>): Boolean =
        formlyDelegatesValidator.spinner(formly)

    inner class SpinnerViewHolder internal constructor(val view: View) : FormlyViewHolder(view) {

        val spinner = view.formlySpinner

        @SuppressLint("ClickableViewAccessibility")
        override fun onBind(formly: Formly?, forms: List<Formly?>) {
            with(view) {
                val templateOptions = formly?.templateOptions
                formlySpinnerTitle.text = templateOptions?.label
                formlySpinnerTitle.setTextColor(ColorUtils.setAlphaComponent(confHelper.backgroundThemePrimaryTextColor, 122))
                if (formly?.data != null && formly.data.allOptions != null) {
                    val options: MutableList<SelectOption> = ArrayList()
                    formly.data?.allOptions?.let { options.addAll(it) }
                    val displayNames: MutableList<String> = ArrayList()
                    for (option in options) {
                        displayNames.add(option.name)
                    }
                    val adapter = ArrayAdapter(formlySpinnerTitle.context, R.layout.formly_spinner_item, displayNames)
                    adapter.setDropDownViewResource(R.layout.formly_simple_spinner_dropdown_item)
                    formlySpinner.adapter = adapter
                    formlySpinner.onItemSelectedListener = object : OnItemSelectedListener {
                        override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                            themeSelectedItem(formlySpinner)
                            dropDownSelections.onNext(Pair.create(formly, formly?.data?.allOptions?.get(i)?.id))
                        }

                        override fun onNothingSelected(adapterView: AdapterView<*>?) {
                            themeSelectedItem(formlySpinner)
                        }
                    }
                    formlySpinner.setOnTouchListener { _: View?, _: MotionEvent? ->
                        formlySpinnerTitle.text = templateOptions?.let { getFormattedLabel(it) }
                        false
                    }
                }
            }
        }

        private fun getFormattedLabel(templateOptions: TemplateOptions): String? {
            return if (templateOptions.required == true) {
                templateOptions.label + "*"
            } else templateOptions.label
        }

        private fun themeSelectedItem(spinner: Spinner) {
            if (spinner.selectedView != null && spinner.selectedView is TextView) {
                (spinner.selectedView as TextView).setTextColor(confHelper.backgroundThemePrimaryTextColor)
            }
        }
    }

}