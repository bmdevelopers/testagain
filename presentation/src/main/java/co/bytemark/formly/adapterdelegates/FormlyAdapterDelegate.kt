package co.bytemark.formly.adapterdelegates

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.helpers.ConfHelper
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate

abstract class FormlyAdapterDelegate(protected val confHelper: ConfHelper) : AdapterDelegate<List<Formly>>() {

    @CallSuper
    override fun isForViewType(items: List<Formly>, position: Int): Boolean {
        return isFormlyForType(items[position], position, items)
    }

    @CallSuper
    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return onCreateFormlyViewHolder(LayoutInflater.from(parent.context), parent)
    }

    protected abstract fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): FormlyViewHolder

    protected abstract fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>): Boolean

    @CallSuper
    override fun onBindViewHolder(
        items: List<Formly>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: List<Any>
    ) {
        val formly = items[position]
        val formlyViewHolder = holder as FormlyViewHolder
        formlyViewHolder.bindFormly(formly, items)
    }

}