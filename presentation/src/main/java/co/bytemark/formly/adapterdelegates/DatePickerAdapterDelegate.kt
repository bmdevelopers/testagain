package co.bytemark.formly.adapterdelegates

import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.helpers.ConfHelper
import kotlinx.android.synthetic.main.formly_date_picker_item.view.*
import rx.subjects.PublishSubject
import java.text.SimpleDateFormat
import java.util.*

class DatePickerAdapterDelegate(
    confHelper: ConfHelper,
    private val textChanges: PublishSubject<Pair<Formly, String>>,
    private val fragmentManager: FragmentManager,
    private val formlyDelegatesValidator: FormlyDelegatesValidator
) : FormlyAdapterDelegate(confHelper) {

    override fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): FormlyViewHolder =
        DatePickerViewHolder(layoutInflater.inflate(R.layout.formly_date_picker_item, parent, false), confHelper, fragmentManager)

    override fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>): Boolean =
        formlyDelegatesValidator.datePickerAdapterDelegate(formly)

    inner class DatePickerViewHolder(
        val view: View,
        val confHelper: ConfHelper,
        val fragmentManager: FragmentManager
    ) : FormlyViewHolder(view) {

        override fun onBind(formly: Formly?, forms: List<Formly?>) {
            with(view) {
                val currentDate = Calendar.getInstance().time
                val sdf = SimpleDateFormat("yyyy-mm-dd", Locale.getDefault())
                val currentDateStr = sdf.format(currentDate)
                formlyDatePickerButton.text = currentDateStr
                formlyDatePickerButton.setOnClickListener {
                    val dialogFragment = DatePickerFragment()
                    dialogFragment.setListener(object : DatePickerFragment.OnSetDateListener {
                        override fun setDate(date: Date?) {
                            val sdf1 = SimpleDateFormat("yyyy-mm-dd", Locale.getDefault())
                            val currentDateStr1 = sdf1.format(date)
                            formlyDatePickerButton.text = currentDateStr1
                            textChanges.onNext(Pair.create(formly, currentDateStr1))
                        }
                    })
                    dialogFragment.show(fragmentManager, dialogFragment::class.java.canonicalName)
                }
            }
        }
    }
}