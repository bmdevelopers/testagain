package co.bytemark.formly.adapterdelegates

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.helpers.ConfHelper
import kotlinx.android.synthetic.main.formly_switch_item.view.*

class SwitchAdapterDelegate(confHelper: ConfHelper, private val formlyDelegatesValidator: FormlyDelegatesValidator) :
    FormlyAdapterDelegate(confHelper) {

    override fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): FormlyViewHolder =
        SwitchViewHolder(layoutInflater.inflate(R.layout.formly_switch_item, parent, false))

    override fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>): Boolean =
        formlyDelegatesValidator.switchAdapterDelegate(formly)

    internal class SwitchViewHolder(val view: View) : FormlyViewHolder(view) {
        override fun onBind(formly: Formly?, forms: List<Formly?>) {
            view.switch1.text = formly?.templateOptions?.label
        }
    }

}