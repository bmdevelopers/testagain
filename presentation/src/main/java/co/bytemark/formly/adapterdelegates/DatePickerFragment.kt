package co.bytemark.formly.adapterdelegates

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.R
import co.bytemark.helpers.ConfHelper
import java.util.*
import javax.inject.Inject

class DatePickerFragment : DialogFragment(), OnDateSetListener {

    private var onSetDateListener: OnSetDateListener? = null

    @JvmField
    @Inject
    var confHelper: ConfHelper? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        component.inject(this)
        // Use the current date as the default date in the picker
        val c = Calendar.getInstance()
        val year = c[Calendar.YEAR]
        val month = c[Calendar.MONTH]
        val day = c[Calendar.DAY_OF_MONTH]
        val datePickerDialog = DatePickerDialog(activity!!, R.style.CustomDialog, this, year, month, day)
        datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
        return datePickerDialog
    }

    override fun onDateSet(datePicker: DatePicker, selectedYear: Int, selectedMonth: Int, selectedDay: Int) {
        onSetDateListener?.setDate(getDateFromDatePicker(datePicker))
    }

    private fun getDateFromDatePicker(datePicker: DatePicker): Date {
        val day = datePicker.dayOfMonth
        val month = datePicker.month
        val year = datePicker.year
        val calendar = Calendar.getInstance()
        calendar[year, month] = day
        return calendar.time
    }

    fun setListener(onSetDateListener: OnSetDateListener?) {
        this.onSetDateListener = onSetDateListener
    }

    interface OnSetDateListener {
        fun setDate(date: Date?)
    }
}