package co.bytemark.formly.adapterdelegates

import android.text.InputFilter
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.TextView
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.formly.adapter.viewholder.InputViewHolder
import co.bytemark.helpers.ConfHelper
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.formly_email_item.view.*
import rx.subjects.PublishSubject
import java.util.*

class EmailAdapterDelegate(
    confHelper: ConfHelper,
    private val textChanges: PublishSubject<Pair<Formly, String>>,
    private val formlyDelegatesValidator: FormlyDelegatesValidator
) : FormlyAdapterDelegate(confHelper) {

    override fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): FormlyViewHolder =
        EmailViewHolder(layoutInflater.inflate(R.layout.formly_email_item, parent, false), textChanges, confHelper)

    override fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>): Boolean =
        formlyDelegatesValidator.emailAdapterDelegate(formly)

    class EmailViewHolder(
        val view: View,
        textChanges: PublishSubject<Pair<Formly, String>>,
        confHelper: ConfHelper
    ) : InputViewHolder(view, textChanges, confHelper) {

        private val emails: MutableList<String> = ArrayList()

        override fun onBind(formly: Formly?, forms: List<Formly?>) {
            super.onBind(formly, forms)
            if (emails.isNotEmpty()) {
                val adapter = ArrayAdapter(view.tv_email.context, R.layout.simple_dropdown_item_1line, emails)
                view.tv_email?.setAdapter(adapter)
            }
        }

        override fun textView(): TextView? = view.textView

        override fun textInputLayout(): TextInputLayout = view.tl_email

        override fun editText(): EditText = view.tv_email

        override fun equalityError() {
            if (!view.tl_email.isErrorEnabled) {
                view.tl_email.error = view.tl_email.resources.getString(R.string.sign_in_email_equality_error)
            }
        }

        fun setEmails(emails: List<String>) {
            this.emails.clear()
            this.emails.addAll(emails)
        }

        fun restrictEmojiInput(emojiFilter: Array<InputFilter?>?) {
            view.tv_email.filters = emojiFilter
        }
    }

}