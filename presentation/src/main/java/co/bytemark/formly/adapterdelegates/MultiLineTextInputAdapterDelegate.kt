package co.bytemark.formly.adapterdelegates

import com.google.android.material.textfield.TextInputLayout
import android.text.InputFilter
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.formly.adapter.viewholder.InputViewHolder
import co.bytemark.helpers.ConfHelper
import kotlinx.android.synthetic.main.formly_multi_line_text_input.view.*
import rx.subjects.PublishSubject

class MultiLineTextInputAdapterDelegate(var helper: ConfHelper, val textChanges: PublishSubject<Pair<Formly, String>>,
                                  val formlyDelegatesValidator: FormlyDelegatesValidator)
    : EditTextAdapterDelegate(helper, textChanges, formlyDelegatesValidator) {

    override fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): FormlyViewHolder {
        return MultiLineTextViewHolder(layoutInflater.inflate(R.layout.formly_multi_line_text_input, parent, false), textChanges, helper)
    }

    override fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>): Boolean {
        return formlyDelegatesValidator.multiLineTextAdapter(formly)
    }

    class MultiLineTextViewHolder(itemView: View, textChanges: PublishSubject<Pair<Formly, String>>, confHelper: ConfHelper)
        : InputViewHolder(itemView,textChanges,confHelper){

        override fun textInputLayout(): TextInputLayout = itemView.messageTextInputLayout

        override fun editText(): EditText = itemView.messageEditText

        override fun equalityError() {}

        override fun textView(): TextView? = itemView.messageTextView

        fun restrictEmojiInput(emojiFilter: Array<InputFilter?>?) {
            itemView.messageEditText.filters = emojiFilter
        }
    }

}