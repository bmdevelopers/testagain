package co.bytemark.formly.adapterdelegates

import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.formly.adapter.viewholder.InputViewHolder
import co.bytemark.helpers.ConfHelper
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.formly_phone_item.view.*
import rx.subjects.PublishSubject

class PhoneAdapterDelegate(
    confHelper: ConfHelper, private val textChangesSub: PublishSubject<Pair<Formly, String>>,
    private val formlyDelegatesValidator: FormlyDelegatesValidator
) : FormlyAdapterDelegate(confHelper) {

    override fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): FormlyViewHolder =
        PhoneViewHolder(layoutInflater.inflate(R.layout.formly_phone_item, parent, false), textChangesSub, confHelper)

    override fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>): Boolean =
        formlyDelegatesValidator.phoneAdapterDelegate(formly)

    inner class PhoneViewHolder internal constructor(
        val view: View,
        textChanges: PublishSubject<Pair<Formly, String>>,
        confHelper: ConfHelper
    ) : InputViewHolder(view, textChanges, confHelper) {

        override fun textView(): TextView = view.formlyPhoneTextView

        override fun textInputLayout(): TextInputLayout = view.formlyPhoneTextInput

        override fun editText(): EditText = view.formlyPhoneEditText

        override fun equalityError() {}

    }

}