package co.bytemark.formly.adapterdelegates

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.formly.FormlyUtil
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.helpers.ConfHelper
import kotlinx.android.synthetic.main.formly_text_item.view.*

class TextAdapterDelegate(confHelper: ConfHelper, private val formlyDelegatesValidator: FormlyDelegatesValidator) :
    FormlyAdapterDelegate(confHelper) {

    override fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): FormlyViewHolder =
        TextViewHolder(layoutInflater.inflate(R.layout.formly_text_item, parent, false), confHelper)

    override fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>): Boolean =
        formlyDelegatesValidator.textAdapter(formly)

    internal class TextViewHolder(val view: View, var confHelper: ConfHelper) : FormlyViewHolder(view) {

        override fun onBind(formly: Formly?, forms: List<Formly?>) {
            with(view) {
                val templateOptions = formly?.templateOptions
                if (!TextUtils.isEmpty(templateOptions?.label)) {
                    templateOptions?.label?.let { FormlyUtil.setTextViewHTML(text, it) }
                } else {
                    textView.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
                }
                val description = templateOptions?.description
                if (!TextUtils.isEmpty(description)) {
                    textView.visibility = View.VISIBLE
                    description?.let { FormlyUtil.setTextViewHTML(textView, it) }
                } else {
                    textView.visibility = View.GONE
                }
            }
        }
    }

}