package co.bytemark.formly.adapterdelegates

import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.R
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.formly.adapter.viewholder.FormlyViewHolder
import co.bytemark.helpers.ConfHelper
import rx.subjects.PublishSubject

class NumericInputAdapterDelegate(
    var helper: ConfHelper, val textChanges: PublishSubject<Pair<Formly, String>>,
    val formlyDelegatesValidator: FormlyDelegatesValidator
) : EditTextAdapterDelegate(helper, textChanges, formlyDelegatesValidator) {

    override fun onCreateFormlyViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): FormlyViewHolder {
        return NumericInputViewHolder(layoutInflater.inflate(R.layout.formly_numeric_input_item, parent, false), textChanges, helper)
    }

    override fun isFormlyForType(formly: Formly?, position: Int, items: List<Formly?>): Boolean {
        return formlyDelegatesValidator.numericInputAdapterDelegate(formly)
    }

    private inner class NumericInputViewHolder(itemView: View, textChanges: PublishSubject<Pair<Formly, String>>, confHelper: ConfHelper) :
        EditTextViewHolder(itemView, textChanges, confHelper)
}

