package co.bytemark.formly.adapter.viewholder

interface Validatable {
    /**
     * Normally, we trigger validation only when focus of the item has been lost, calling this would
     * lift that requirement. Needed when clicking sign in button without entering anything, for example.
     */
    fun enableValidation()

    fun isValid(): Boolean
}