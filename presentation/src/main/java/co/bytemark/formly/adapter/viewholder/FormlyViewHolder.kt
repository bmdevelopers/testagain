package co.bytemark.formly.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import co.bytemark.domain.model.authentication.Formly

/**
 * ViewHolder's using this store the Formly object they are rendering.
 *
 * This is needed to unify interactions with RecyclerView. We can't use adapter delegates to
 * communicate with items rendered by RecyclerView since one adapter delegate can delegate rendering
 * to multiple view holders. If adapter delegate is used to handle logic, it will trigger the change on
 * all view holders that the adapter delegate handles.
 *
 * By storing the [co.bytemark.domain.model.authentication.Formly] object associated with the
 * view holder, we can differentiate and do precise changes that only affect the concerned view holder.
 */
abstract class FormlyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    // Marked as Nullable since it could be called even before bind was called.
    var formly: Formly? = null
        protected set

    fun bindFormly(formly: Formly?, forms: List<Formly?>) {
        this.formly = formly
        onBind(formly, forms)
    }

    protected abstract fun onBind(formly: Formly?, forms: List<Formly?>)

}