package co.bytemark.formly.adapter.viewholder

import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.util.Pair
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.R
import co.bytemark.add_payment_card.Helper.TextWatcherHelper
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.authentication.TemplateOptions
import co.bytemark.formly.FormlyUtil
import co.bytemark.formly.validation.MaskInputFormatter
import co.bytemark.helpers.ConfHelper
import co.bytemark.helpers.FormValidator
import co.bytemark.widgets.util.Util
import com.google.android.material.textfield.TextInputLayout
import rx.subjects.PublishSubject
import java.util.*

abstract class InputViewHolder(
    itemView: View,
    protected val textChanges: PublishSubject<Pair<Formly, String>>,
    protected val confHelper: ConfHelper
) : ValidatableViewHolder(itemView) {

    private var validator: FormValidator? =null
    protected var enableValidation = false
    private var checkBox: CheckBox? = null
    private var formlyKey: String? = null
    private var checkBoxErrorHolderView: TextView? = null
    private var checkBoxErrorMessage: String? = null
    private var isCheckBoxRequired: Boolean? = null
    private val formatterHashMap: MutableMap<Formly?, MaskInputFormatter> = HashMap()


    abstract fun textView(): TextView?
    abstract fun textInputLayout(): TextInputLayout?
    abstract fun editText(): EditText?

    /**
     * This method can be called by adapter when other Formly item and this items are supposed to
     * have same value, but they are not. For example, "Set password" and "Confirm password" are
     * supposed to have same value.
     *
     *
     * By implementing this method, view holders can specify custom message for equality error.
     */
    abstract fun equalityError()

    override fun onBind(formly: Formly?, forms: List<Formly?>) {
        formlyKey = formly?.key
        isCheckBoxRequired = formly?.templateOptions?.required
        if (formlyKey.equals(AGREE_TO_TERMS, ignoreCase = true)) {
            checkBoxErrorMessage = formly?.templateOptions?.unCheckedFieldMessage
            val textInputLayout = textInputLayout()
            validator = formly?.let { textInputLayout?.let { it1 -> FormValidator(it, it1) } }
        } else {
            val templateOptions = formly?.templateOptions
            val textInputLayout = textInputLayout()
            textInputLayout?.let {til ->
                validator = formly?.let { FormValidator(it, til) }
                til.hint = templateOptions?.label
                til.transitionName = formly?.key
            }
            val editText = editText()
            editText?.let {
                it.setTextColor(confHelper.backgroundThemePrimaryTextColor)
                it.setHintTextColor(ColorUtils.setAlphaComponent(confHelper.backgroundThemePrimaryTextColor, 122))
                it.addTextChangedListener(validator)
                it.addTextChangedListener(object : TextWatcherHelper() {
                    override fun afterTextChanged(s: Editable?) {
                        if (enableValidation) {
                            doValidation()
                        }
                        textChanges.onNext(Pair.create(formly, s.toString()))
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
                })
            }

            val placeholder = templateOptions?.placeholder
            editText?.onFocusChangeListener = OnFocusChangeListener { _: View?, hasFocus: Boolean ->
                templateOptions?.let { addAsteriskAfterField(it) }
                if (!hasFocus) {
                    enableValidation = true
                    doValidation()
                }
                if (hasFocus && !TextUtils.isEmpty(placeholder)) {
                    editText?.hint = placeholder
                    showKeyboard(editText)
                } else {
                    editText?.hint = ""
                }
            }
            if (!TextUtils.isEmpty(templateOptions?.mask)) {
                editText?.let {
                    val inputFormatter = MaskInputFormatter(it, formly?.templateOptions?.mask ?: "")
                    it.addTextChangedListener(inputFormatter)
                    formatterHashMap[formly] = inputFormatter
                }
            }
            val description = templateOptions?.description
            if (!TextUtils.isEmpty(description)) {
                textView()?.let {
                    it.visibility = View.VISIBLE
                    if (description != null) {
                        FormlyUtil.setTextViewHTML(it, description)
                    }
                    it.setTextColor(confHelper.backgroundThemePrimaryTextColor)
                }
            } else {
                textView()?.visibility = View.GONE
            }
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION && forms.size != position && forms[position + 1]?.type == Formly.INPUT_TYPE) {
                editText?.imeOptions = EditorInfo.IME_ACTION_NEXT
            } else {
                editText?.imeOptions = EditorInfo.IME_ACTION_DONE
            }
            editText?.setOnClickListener {
                if (Util.isTalkBackAccessibilityEnabled(editText.context) && editText.text != null) {
                    editText.setSelection(editText.text.length)
                }
            }

            // enabling / disabling
            if(formly?.templateOptions?.editable == false){
                editText?.isEnabled = false
            }
        }
    }

    fun setCheckBoxState(isChecked: Boolean?) {
        checkBox?.isChecked = isChecked!!
    }

    fun setCheckBox(checkBox: CheckBox?) {
        this.checkBox = checkBox
    }

    fun setCheckBoxErrorHolderView(view: TextView?) {
        checkBoxErrorHolderView = view
    }

    private fun addAsteriskAfterField(templateOptions: TemplateOptions) {
        if (templateOptions.required == true) {
            textInputLayout()?.hint = templateOptions.label + "*"
        }
    }

    private fun doValidation() {
        validator?.validate()
    }

    override fun enableValidation() {
        enableValidation = true
    }

    fun setMaskedText(value: CharSequence) {
        formatterHashMap[formly]?.setExternalValue(value.toString())
    }

    override fun isValid(): Boolean {
        return if (formlyKey != null && formlyKey.equals(AGREE_TO_TERMS, ignoreCase = true)) {
            // Validations for checkbox
            if (isCheckBoxRequired != null) {
                doValidationsForCheckBox()
                checkBox?.isChecked == true
            } else {
                true
            }
        } else {
            // Validations for rest of the fields.
            doValidation()
            validator?.isValid == true
        }
    }

    private fun doValidationsForCheckBox() {
        if (checkBox?.isChecked == false) {
            setCheckBoxError()
        }
    }

    private fun setCheckBoxError() {
        checkBoxErrorHolderView?.visibility = View.VISIBLE
        checkBoxErrorHolderView?.setTextColor(checkBoxErrorHolderView!!.context.resources.getColor(R.color.bt_error_red))
        checkBoxErrorHolderView?.text = if (checkBoxErrorMessage != null) checkBoxErrorMessage else ""
        checkBox?.error = checkBoxErrorMessage
    }

    private fun showKeyboard(editText: EditText?) {
        val imm = editText?.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED)
    }

    companion object {
        private const val AGREE_TO_TERMS = "agree_to_terms"
    }

}