package co.bytemark.formly.adapter.viewholder

import android.view.View

abstract class LoadingViewHolder(itemView: View) : FormlyViewHolder(itemView) {
    abstract fun showLoading()
    abstract fun hideLoading()
}