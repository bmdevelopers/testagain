package co.bytemark.formly.adapter.viewholder

import android.view.View

/**
 * [co.bytemark.formly.adapter.FormlyAdapter] may query or trigger a validation anytime using the given methods.
 */

abstract class ValidatableViewHolder(itemView: View) : FormlyViewHolder(itemView), Validatable