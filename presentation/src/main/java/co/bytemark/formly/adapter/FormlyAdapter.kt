package co.bytemark.formly.adapter

import android.text.InputFilter
import android.text.Spanned
import android.util.Pair
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import co.bytemark.CustomerMobileApp.Companion.component
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.formly.adapter.viewholder.*
import co.bytemark.formly.adapterdelegates.*
import co.bytemark.formly.adapterdelegates.EmailAdapterDelegate.EmailViewHolder
import co.bytemark.formly.adapterdelegates.MultiLineTextInputAdapterDelegate.MultiLineTextViewHolder
import co.bytemark.formly.adapterdelegates.MultiSpinnerAdapterDelegate.MultiSpinnerViewHolder
import co.bytemark.formly.adapterdelegates.SpinnerAdapterDelegate.SpinnerViewHolder
import co.bytemark.formly.validation.ManualValidation
import co.bytemark.formly.validation.ManualValidation.Companion.EQUALITY
import co.bytemark.helpers.ConfHelper
import com.hannesdorfmann.adapterdelegates4.AdapterDelegatesManager
import rx.Observable
import rx.subjects.PublishSubject
import rx.subscriptions.CompositeSubscription
import java.util.*
import javax.inject.Inject

class FormlyAdapter(
    val recyclerView: RecyclerView,
    fragmentManager: FragmentManager?
) : Validatable, RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    @Inject
    lateinit var confHelper: ConfHelper

    @Inject
    lateinit var formlyDelegatesValidator: FormlyDelegatesValidator
    private val delegatesManager: AdapterDelegatesManager<List<Formly>>
    private val formlyList: MutableList<Formly> = ArrayList()

    // Observe text input changes in this adapter.
    private val textChanges =
        PublishSubject.create<Pair<Formly, String>>()

    // Observe all button type clicks in this adapter
    private val buttonClicks = PublishSubject.create<Formly>()

    // Observe all dropdown selections in this adapter
    private val dropDownSelections =
        PublishSubject.create<Pair<Formly, String>>()
    private val manualValidationResolver = ManualValidationResolver()
    private val manualValidations: MutableList<ManualValidation> = ArrayList()

    // In house subscriptions
    private val subs = CompositeSubscription()

    override fun getItemViewType(position: Int): Int {
        return delegatesManager.getItemViewType(formlyList, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegatesManager.onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegatesManager.onBindViewHolder(formlyList, position, holder)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        delegatesManager.onBindViewHolder(formlyList, position, holder, payloads)
    }

    override fun getItemId(position: Int): Long {
        return formlyList[position].key.hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return formlyList.size
    }

    fun setFormlyList(formlyList: MutableList<Formly>) {
        this.formlyList.clear()
        formlyDelegatesValidator.filterValidFormly(formlyList)
        this.formlyList.addAll(formlyList)
        notifyDataSetChanged()
    }

    private fun addManualValidationList(vararg validation: ManualValidation) {
        manualValidations.addAll(validation)
    }

    fun clearManualValidations() {
        manualValidations.clear()
    }

    fun setEmails(emails: List<String>) {
        formlyList.filter { it.key.equals(Formly.EMAIL_KEY, ignoreCase = true) }.forEach {
            (getViewHolderWithFormly(it) as? EmailViewHolder)?.setEmails(emails)
        }
    }

    fun disableEmojiInput() {
        val emojiFilter =
            InputFilter { source: CharSequence, _: Int, _: Int, _: Spanned?, _: Int, _: Int ->
                if (source.isEmpty()) return@InputFilter ""
                val type = Character.getType(source[0])
                if (type == Character.SURROGATE.toInt() || type == Character.OTHER_SYMBOL.toInt() || type == Character.MATH_SYMBOL.toInt()) {
                    return@InputFilter ""
                }
                null
            }
        formlyList.filter {
            it.key.equals(Formly.EMAIL_KEY, ignoreCase = true) || it.key
                .equals(
                    Formly.EMAIL_ADDRESS_KEY,
                    ignoreCase = true
                ) || it.key.equals(Formly.MESSAGE, ignoreCase = true)
        }.forEach {
            when (val viewHolder = getViewHolderWithFormly(it)) {
                is EmailViewHolder -> viewHolder.restrictEmojiInput(arrayOf(emojiFilter))
                is MultiLineTextViewHolder -> viewHolder.restrictEmojiInput(arrayOf(emojiFilter))
            }
        }
    }

    fun textChanges(): Observable<Pair<Formly, String>> {
        return textChanges.asObservable()
    }

    fun dropdownSelects(): Observable<Pair<Formly, String>> {
        return dropDownSelections.asObservable()
    }

    fun buttonClicks(): Observable<Formly> {
        return buttonClicks.asObservable().filter { formly: Formly? -> formly != null }
    }

    private fun getViewHolderWithFormly(formly: Formly): FormlyViewHolder? {
        return getViewHolderWithFormlyKey(formly.key)
    }

    private fun getViewHolderWithFormlyKey(key: String?): FormlyViewHolder? {
        val viewHolder = recyclerView.findViewHolderForItemId(key.hashCode().toLong())
        return viewHolder as? FormlyViewHolder
    }

    override fun enableValidation() {
        formlyList.forEach {
            (getViewHolderWithFormly(it) as? ValidatableViewHolder)?.enableValidation()
        }
    }

    override fun isValid(): Boolean {
        var valid = true
        for (formly in formlyList) {
            val viewHolder = getViewHolderWithFormly(formly)
            if (viewHolder is ValidatableViewHolder) {
                valid = valid and (viewHolder.isValid() ?: true)
            }
        }
        valid = valid and isManualValid(true)
        return valid
    }

    /**
     * Validates if the manual validation criteria specified in [.manualValidations] passes.
     *
     *
     * After validation, you can control whether an error message should be applied by passing
     * {@param applyError}
     *
     * @param applyError Whether to apply an error message based on validation result.
     * @return true if manual validation passes.
     */
    fun isManualValid(applyError: Boolean): Boolean {
        var valid = true
        for (manualValidation in manualValidations) {
            valid = valid and manualValidationResolver.resolve(manualValidation, applyError)
        }
        return valid
    }

    /**
     * Finds the view holder that contains the formly object key {@param formlyKey}) and if its a
     * instance of [LoadingViewHolder] then, calls appropriate loading methods.
     *
     * @param formlyKey Key of the formly object to trigger loading change.
     * @param loading   True if loading, false other wise.
     */
    fun setLoading(formlyKey: String, loading: Boolean) {
        for (formly in formlyList) {
            if (formly.key.equals(formlyKey, ignoreCase = true)) {
                val viewHolder = getViewHolderWithFormly(formly)
                if (viewHolder is LoadingViewHolder) {
                    if (loading) {
                        viewHolder.showLoading()
                    } else {
                        viewHolder.hideLoading()
                    }
                }
            }
        }
    }

    fun setValue(formlyKey: String, value: CharSequence?) {
        var keyPresentInChild: Boolean = false
        formlyList.filter {
            keyPresentInChild = isKeyPresentInChildFormly(it, formlyKey)
            it.key.equals(formlyKey, ignoreCase = true) || keyPresentInChild
        }.forEach {
            when (val viewHolder = getViewHolderWithFormly(it)) {
                is MultiSpinnerViewHolder -> {
                    if (isKeyPresentInChildFormly(it, formlyKey)) {
                        viewHolder.setChildValue(value.toString())
                    } else {
                        viewHolder.setParentSpinnerValue(value.toString())
                    }
                }

                is InputViewHolder -> {
                    if (it.type == Formly.MASKED_INPUT_TYPE) {
                        viewHolder.setMaskedText(value!!)
                    } else {
                        viewHolder.editText()?.setText(value)
                        viewHolder.editText()?.setSelection(viewHolder.editText()!!.text.length)
                    }
                }

                is SpinnerViewHolder -> {
                    if (it.data != null && it.data.allOptions != null) {
                        var position = 0
                        for (i in it.data.allOptions.indices) {
                            if (value.toString()
                                    .equals(it.data.allOptions[i].id, ignoreCase = true)
                            ) {
                                position = i
                                break
                            }
                            if (value.toString()
                                    .equals(it.data.allOptions[i].name, ignoreCase = true)
                            ) {
                                position = i
                                break
                            }
                        }
                        viewHolder.spinner.setSelection(position, false)
                    }
                }
            }
        }
    }

    private fun isKeyPresentInChildFormly(formly: Formly, formlyKey: String): Boolean {
        if (formly.child != null) {
            for (child in formly.child) {
                if (child.key.equals(formlyKey, ignoreCase = true)) {
                    return true
                }
            }
        }
        return false
    }

    fun cleanUp() {
        subs.clear()
    }

    /**
     * Resolver class that takes in a [ManualValidation] class and validates it against the
     * content of the [FormlyViewHolder]
     */
    private inner class ManualValidationResolver {
        /**
         * Resolver which actually performs the validation by talking to the ViewHolder. It does so
         * by finding ViewHolder for keys specified in [ManualValidation.whichKeys].
         * After finding the ViewHolder, it evaluates the [ManualValidation.criteria]
         *
         * @param manualValidation The validation class containing metadata.
         * @param applyError       Whether to apply error message based on the result of the validation.
         * @return
         */
        fun resolve(manualValidation: ManualValidation, applyError: Boolean): Boolean {
            // Store the view holders in a map with formly key as key for easier access.
            val viewHolderMap: MutableMap<String, FormlyViewHolder> =
                LinkedHashMap()
            for (key in manualValidation.whichKeys) {
                val formlyViewHolder = getViewHolderWithFormlyKey(key)
                if (formlyViewHolder != null) {
                    viewHolderMap[key] = formlyViewHolder
                } else {
                    // If we ever come across a null view holder, assume validation passed.
                    return true
                }
            }
            when (manualValidation.criteria) {
                EQUALITY -> {
                    var valid = true

                    // We will make two passes. On the first pass we will validate the criteria and
                    // in the second pass we will apply error to the view holder.
                    var value: String? = null
                    for (formlyViewHolder in viewHolderMap.values) {
                        if (formlyViewHolder is InputViewHolder) {
                            val enteredText =
                                formlyViewHolder.editText()?.text.toString().trim { it <= ' ' }
                            if (value == null) {
                                // Initial iteration, store the first value
                                value = enteredText
                            } else {
                                // Compare the value and if they are not equal return false right away
                                if (!value.equals(enteredText, ignoreCase = true)) {
                                    valid = false
                                    break
                                }
                            }
                        } else {
                            // Handle other view holders
                        }
                    }

                    // Second pass to apply result of validation i.e display error message

                    // Modified now to apply error to last field.
                    if (applyError && !valid) {
                        val size = viewHolderMap.values.size
                        var i = 0
                        for (formlyViewHolder in viewHolderMap.values) {
                            if (i == size - 1) {
                                if (formlyViewHolder is InputViewHolder) {
                                    formlyViewHolder.equalityError()
                                }
                            }
                            i++
                        }
                    }
                    return valid
                }
            }
            return false
        }
    }

    init {
        component.inject(this)
        delegatesManager = AdapterDelegatesManager()
        delegatesManager.apply {
            addDelegate(EmailAdapterDelegate(confHelper, textChanges, formlyDelegatesValidator))
            addDelegate(
                NumericInputAdapterDelegate(
                    confHelper,
                    textChanges,
                    formlyDelegatesValidator
                )
            )
            addDelegate(PasswordAdapterDelegate(confHelper, textChanges, formlyDelegatesValidator))
            addDelegate(
                SpinnerAdapterDelegate(
                    confHelper,
                    dropDownSelections,
                    formlyDelegatesValidator
                )
            )
            addDelegate(EditTextAdapterDelegate(confHelper, textChanges, formlyDelegatesValidator))
            addDelegate(PhoneAdapterDelegate(confHelper, textChanges, formlyDelegatesValidator))
            addDelegate(CheckBoxAdapterDelegate(confHelper, textChanges, formlyDelegatesValidator))
            addDelegate(
                DatePickerAdapterDelegate(
                    confHelper,
                    textChanges,
                    fragmentManager!!,
                    formlyDelegatesValidator
                )
            )
            addDelegate(TextAdapterDelegate(confHelper, formlyDelegatesValidator))
            addDelegate(SwitchAdapterDelegate(confHelper, formlyDelegatesValidator))
            addDelegate(ButtonAdapterDelegate(confHelper, buttonClicks, formlyDelegatesValidator))
            addDelegate(
                MultiLineTextInputAdapterDelegate(
                    confHelper,
                    textChanges,
                    formlyDelegatesValidator
                )
            )
            addDelegate(
                MultiSpinnerAdapterDelegate(
                    confHelper,
                    textChanges,
                    formlyDelegatesValidator
                )
            )
        }
        setHasStableIds(true)

        // Add manual validations for forms.
        addManualValidationList(
            ManualValidation(
                whichKeys = mutableListOf(
                    Formly.PASSWORD_KEY,
                    Formly.CONFIRM_PASSWORD_KEY
                )
            ),
            ManualValidation(whichKeys = mutableListOf(Formly.EMAIL_KEY, Formly.CONFIRM_EMAIL_KEY)),
            ManualValidation(
                whichKeys = mutableListOf(
                    Formly.NEW_PASSWORD_KEY,
                    Formly.CONFIRM_NEW_PASSWORD_KEY
                )
            )
        )

        // On each button click relayout recyclerview. EditText messes up layouts measurement.
        subs.add(buttonClicks.subscribe { formly: Formly? -> recyclerView.requestLayout() })
    }
}