package co.bytemark.formly

import android.app.Activity
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.net.Uri
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.URLSpan
import android.text.util.Linkify
import android.view.View
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import co.bytemark.R
import co.bytemark.customtabs.CustomTabsActivityHelper
import co.bytemark.customtabs.CustomTabsHelper
import co.bytemark.customtabs.WebViewFallback
import co.bytemark.widgets.util.Util

object FormlyUtil {

    @JvmStatic
    fun themeButtonBackground(drawableToTheme: Drawable, @ColorInt color: Int) {
        if (drawableToTheme is LayerDrawable) {
            drawableToTheme.getDrawable(0).setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
        }
    }

    /**
     * Given a TextView and text to set on the TextView, this method would convert links in the
     * {@param text} to clickable ones and redirect all the clicks to our [WebViewActivity]
     *
     * @param textView TextView to set text on.
     * @param text     The text, preferably HTML string.
     */
    fun setTextViewHTML(textView: TextView, text: String) {
        textView.autoLinkMask = Linkify.ALL
        textView.linksClickable = true
        var sequence: CharSequence = Util.fromHtml(text)
        /* First we set the parsed text spannable to TextView. This step parses out additional URLSpans.
         * If we don't do this, we will miss out spans for literal urls in the text like www.example.com */textView.text = sequence
        // Now get back the spannable string since TextView would have parsed additional spans.
        sequence = textView.text
        val strBuilder = SpannableStringBuilder(sequence)
        val urls = strBuilder.getSpans(0, sequence.length, URLSpan::class.java)
        for (span in urls) {
            makeLinkClickable(strBuilder, span)
        }
        textView.text = strBuilder
        textView.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun makeLinkClickable(strBuilder: SpannableStringBuilder, span: URLSpan) {
        val start = strBuilder.getSpanStart(span)
        val end = strBuilder.getSpanEnd(span)
        val flags = strBuilder.getSpanFlags(span)
        val title = strBuilder.subSequence(start, end).toString()
        strBuilder.setSpan(ClickToWebViewRedirectSpan(span.url, title), start, end, flags)
        strBuilder.removeSpan(span)
    }

    private class ClickToWebViewRedirectSpan internal constructor(private val url: String, private val title: String) : ClickableSpan() {
        override fun onClick(widget: View) {
            if (widget.context is Activity) {
                val customTabsIntent = CustomTabsIntent.Builder()
                    .setToolbarColor(ContextCompat.getColor(widget.context, R.color.colorPrimary))
                    .enableUrlBarHiding()
                    .setShowTitle(true)
                    .build()
                CustomTabsHelper.addKeepAliveExtra(widget.context, customTabsIntent.intent)
                CustomTabsActivityHelper
                    .openCustomTab(
                        widget.context, customTabsIntent,
                        Uri.parse(url),
                        WebViewFallback()
                    )
            }
        }

    }
}