package co.bytemark.formly.validation

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.jakewharton.rxbinding.view.RxView

/**
 * This class enforces editText to accept input in the `EXACT` format specified in @property maskformat.
 *
 * @property maskToRegexMap
 * contains maskDefinition mapping - default: { '9': /\d/, 'A': /[a-zA-Z]/, '*': /[a-zA-Z0-9]/ },
 *
 * Eg Format.
 * Format `99/99/9999` can accept only numbers(0-9) separated by 2 slashes e.g 10/02/1999, 33/99/1111
 * Format `AAA-AAA` can accept only alphabets separated by - e.g MAR-MAY
 * Format `** *** ****` can accept alphanumeric separated by spaces - e.g 13 MAR 2019
 *
 * Explained here :
 * http://angular-formly.com/#!/example/integrations/ui-mask
 */


class MaskInputFormatter(private val editText: EditText, private val maskFormat: String) : TextWatcher {

    //Track current cursor
    private var cursorPosition = 0

    //Maintains the list of editable position in the sorted order
    private val editablePositionList = mutableListOf<Int>()

    //Store previous text length to identify insertion/deletion
    private var previousTextLength = -1

    //Contains the text with the given maskFormat
    private var currentText = StringBuilder("")

    //Ignore any text changes, if modified inside text watcher.
    private var ignoreChange = false

    //tracks total character, to show the hint if empty
    private var totalCharacterCount = 0

    /*
     *@property maskToRegexMap
        9 Any number.
        A Any letter.
        * Any letter or number.
     */
    private val maskToRegexMap = mutableMapOf('9' to "\\d", 'A' to "[a-zA-Z]", '*' to "[a-zA-Z0-9]")

    // Placeholder to display editable spaces
    private val defaultPlaceHolder = '-'


    init {
        extractEditablePositionFromFormat()
        currentText.append(maskFormat.replace("[9A*]".toRegex(), defaultPlaceHolder.toString()))
        cursorPosition = getNextEditablePositionFrom()
        setUpFocusListener()
    }

    //When value is set from server
    fun setExternalValue(value: String?) {
        value?.let {
            currentText.clear()
            currentText.append(it)
            totalCharacterCount = it.replace("[^A-Za-z0-9]".toRegex(), "").length
            updateEditText()
        }
    }

    //Cant use focus change listener, it is already being used
    private fun setUpFocusListener() {
        RxView.focusChanges(editText.rootView).filter { it }.take(1).subscribe {
            editText.post { updateEditText() }
        }

    }

    //Extract position
    private fun extractEditablePositionFromFormat(format: String = maskFormat) {
        format.toCharArray().forEachIndexed { index, it ->
            if ("[9A*]".toRegex().matches(it.toString())) {
                editablePositionList.add(index)
            }
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        s?.let { previousTextLength = s.length }
    }

    override fun onTextChanged(newText: CharSequence?, start: Int, before: Int, count: Int) {

        if (ignoreChange || newText == null) {
            return
        }

        if (newText.length > previousTextLength) { //Character Added
            insert(newText[start + before], start + before)
        } else {
            delete(atPosition = start + before - 1)
        }
    }

    override fun afterTextChanged(finalText: Editable?) {

        if (ignoreChange) {
            ignoreChange = false
            return
        }

        updateEditText()
    }

    private fun updateEditText() {

        ignoreChange = true

        when (totalCharacterCount) {
            0 -> {
                editText.text.clear() //shows hint
                editText.setSelection(0)
            }
            else -> {
                editText.setText(currentText) //shows formatted text
                editText.setSelection(cursorPosition)
            }
        }
    }

    //for insertion
    private fun getNextEditablePositionFrom(position: Int = 0) =
            editablePositionList.firstOrNull { it >= position } ?: run { -1 }


    //for deletion
    private fun getPreviousEditablePositionFrom(position: Int) =
            editablePositionList.lastOrNull { it <= position } ?: run { -1 }


    //insert at given position or next available position
    private fun insert(char: Char, atPosition: Int) {

        val position = getNextEditablePositionFrom(atPosition)

        if (position < 0) { //insertion not possible,return
            cursorPosition = atPosition
            return
        }

        maskToRegexMap[maskFormat[position]]?.let {
            if (char.toString().matches(Regex(it))) { //Update if regex matches
                currentText.setCharAt(position, char)
                totalCharacterCount++
                cursorPosition = if (position + 1 < maskFormat.length) {
                    position + 1
                } else {
                    maskFormat.length
                }
            }
        }
    }

    //delete at given position or next available position
    private fun delete(atPosition: Int) {

        val position = getPreviousEditablePositionFrom(atPosition)

        if (position < 0) { //deletion not possible,return
            cursorPosition = atPosition + 1
            return
        }

        if (currentText.get(position) != defaultPlaceHolder) {
            currentText.setCharAt(position, defaultPlaceHolder)
            totalCharacterCount--
        }

        cursorPosition = position
    }

}