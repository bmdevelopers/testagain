package co.bytemark.formly.validation

import androidx.annotation.IntDef
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

class ManualValidation(@field:Criteria @param:Criteria val criteria: Int = EQUALITY, val whichKeys: List<String>) {

    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    @IntDef(EQUALITY)
    internal annotation class Criteria

    companion object {
        const val EQUALITY = 0
    }

}