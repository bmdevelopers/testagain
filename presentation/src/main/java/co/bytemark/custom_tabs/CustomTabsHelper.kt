package co.bytemark.custom_tabs

import android.app.Activity
import android.content.ComponentName
import androidx.browser.customtabs.CustomTabsClient
import androidx.browser.customtabs.CustomTabsServiceConnection
import androidx.browser.customtabs.CustomTabsSession

class CustomTabsHelper {
    private var customTabsSession: CustomTabsSession? = null
    private var client: CustomTabsClient? = null
    private var connection: CustomTabsServiceConnection? = null

    /**
     * Unbinds the Activity from the Custom Tabs Service
     *
     * @param activity the activity that is connected to the service
     */
    fun unbindCustomTabsService(activity: Activity) {
        if (connection == null) {
            return
        }
        activity.unbindService(connection!!)
        client = null
        customTabsSession = null
    }

    /**
     * Creates or retrieves an exiting CustomTabsSession
     *
     * @return a CustomTabsSession
     */
    val session: CustomTabsSession?
        get() {
            if (client == null) {
                customTabsSession = null
            } else if (customTabsSession == null) {
                customTabsSession = client!!.newSession(null)
            }
            return customTabsSession
        }

    /**
     * Binds the Activity to the Custom Tabs Service
     *
     * @param activity the activity to be bound to the service
     */
    fun bindCustomTabsService(activity: Activity?) {
        if (client != null) {
            return
        }
        val packageName = CustomTabsPackageHelper.getPackageNameToUse(activity) ?: return
        connection = object : CustomTabsServiceConnection() {
            override fun onCustomTabsServiceConnected(
                name: ComponentName,
                client: CustomTabsClient
            ) {
                this@CustomTabsHelper.client = client
                this@CustomTabsHelper.client!!.warmup(0L)
                //Initialize a session as soon as possible.
                session
            }

            override fun onServiceDisconnected(name: ComponentName) {
                client = null
            }

            override fun onBindingDied(name: ComponentName) {
                client = null
            }
        }
        if (activity != null) {
            CustomTabsClient.bindCustomTabsService(activity, packageName,
                connection as CustomTabsServiceConnection
            )
        }
    }
}