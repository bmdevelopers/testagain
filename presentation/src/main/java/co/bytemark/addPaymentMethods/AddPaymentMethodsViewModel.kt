package co.bytemark.addPaymentMethods

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.bytemark.domain.interactor.payments.AddPayPalAccountUseCase
import co.bytemark.domain.interactor.payments.AddPayPalAccountUseCaseValue
import co.bytemark.domain.interactor.payments.AddPaymentMethodV2UseCase
import co.bytemark.domain.interactor.product.GetPayPalTokenRequestValue
import co.bytemark.domain.interactor.product.order.paypal.GetPayPalTokenUseCase
import co.bytemark.helpers.ConfHelper
import co.bytemark.sdk.model.config.SupportedPaymentType
import co.bytemark.sdk.model.payment_methods.BraintreePaypalPaymentMethod
import co.bytemark.sdk.post_body.PostPaymentMethod
import com.google.gson.JsonObject
import rx.subscriptions.CompositeSubscription
import timber.log.Timber
import javax.inject.Inject

class AddPaymentMethodsViewModel @Inject constructor(private val confHelper: ConfHelper,
                                                     private val payPalTokenUseCase: GetPayPalTokenUseCase,
                                                     private val addPayPalAccountUseCase: AddPayPalAccountUseCase,
                                                     private val addPaymentMethodV2UseCase: AddPaymentMethodV2UseCase
) : ViewModel() {

    private val supportedPaymentTypes: MutableLiveData<List<SupportedPaymentType>> by lazy {
        MutableLiveData<List<SupportedPaymentType>>().apply { value = confHelper.supportedPaymentTypes }
    }

    private var payPalToken: MutableLiveData<String> = MutableLiveData()

    private var sentNounceToServer: MutableLiveData<Boolean> = MutableLiveData()

    private val subs = CompositeSubscription()

    fun getPayPalTokenLiveData() {
        MutableLiveData<String>().apply {
            payPalTokenUseCase.getObservable(GetPayPalTokenRequestValue())
                    .subscribe({ token -> payPalToken.value = token },
                            { error -> Timber.e(error) })
        }
    }

    fun getSupportedPaymentMethods() = supportedPaymentTypes

    fun getPayPalClientToken() = payPalToken

    fun getSentNonceToServerResult() = sentNounceToServer

    fun sendPayPalNonceToServer(nonce: String) {
        if (confHelper.isV2PaymentMethodsEnabled) {
            val paymentMethod = PostPaymentMethod(false, false,
                    false, null, BraintreePaypalPaymentMethod(nonce))
            val requestValues = AddPaymentMethodV2UseCase.AddPaymentMethodUseCaseValue(paymentMethod)
            subs.add(addPaymentMethodV2UseCase.getObservable(requestValues).toSingle()
                    .subscribe({ sentNounceToServer.value = true },
                            { error ->
                                Timber.e(error)
                                sentNounceToServer.value = false

                            }))
        } else {
            val jsonBody = JsonObject()
            jsonBody.addProperty("braintree_payment_method_nonce", nonce)
            val addPayPalAccountUseCaseValue = AddPayPalAccountUseCaseValue(jsonBody)
            subs.add(addPayPalAccountUseCase.getObservable(addPayPalAccountUseCaseValue).toSingle()
                    .subscribe({ sentNounceToServer.value = true },
                            { error ->
                                Timber.e(error)
                                sentNounceToServer.value = false
                            }))

        }
    }

    override fun onCleared() {
        super.onCleared()
        subs.clear()
    }
}