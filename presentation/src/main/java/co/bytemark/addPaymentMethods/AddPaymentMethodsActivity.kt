package co.bytemark.addPaymentMethods

import android.os.Bundle
import co.bytemark.R
import co.bytemark.base.MasterActivity
import co.bytemark.sdk.model.common.Action
import com.braintreepayments.api.interfaces.BraintreeCancelListener
import com.braintreepayments.api.interfaces.BraintreeErrorListener
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener
import com.braintreepayments.api.models.PaymentMethodNonce

class AddPaymentMethodsActivity : MasterActivity(), PaymentMethodNonceCreatedListener,
        BraintreeErrorListener, BraintreeCancelListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle(confHelper.getSettingsMenuScreenTitleFromTheConfig(true, true, Action.PAYMENT_METHODS))
        setNavigationViewCheckedItem(Action.SETTINGS)
    }

    override fun useHamburgerMenu() = false

    override fun getLayoutRes(): Int = R.layout.activity_add_payment_methods

    override fun onPaymentMethodNonceCreated(paymentMethodNonce: PaymentMethodNonce) {
        val fragment = supportFragmentManager.findFragmentById(R.id.fragment) as AddPaymentMethodsFragment
        fragment.onPaymentMethodNonceCreated(paymentMethodNonce)
    }

    override fun onError(error: Exception?) {
    }

    override fun onCancel(requestCode: Int) {
    }
}