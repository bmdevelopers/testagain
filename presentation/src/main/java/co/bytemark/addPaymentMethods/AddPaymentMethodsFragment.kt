package co.bytemark.addPaymentMethods

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.add_payment_card.AddPaymentCardActivity
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.analytics.AnalyticsPlatformsContract
import co.bytemark.helpers.AppConstants
import co.bytemark.widgets.emptystateview.EmptyStateLayout
import co.bytemark.widgets.util.ConnectivityLiveData
import co.bytemark.widgets.util.connectionErrorDialog
import co.bytemark.widgets.util.createViewModel
import co.bytemark.widgets.util.isOnline
import com.braintreepayments.api.BraintreeFragment
import com.braintreepayments.api.PayPal
import com.braintreepayments.api.exceptions.InvalidArgumentException
import com.braintreepayments.api.models.PaymentMethodNonce
import kotlinx.android.synthetic.main.fragment_add_payment_methods.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import kotlin.collections.LinkedHashSet

class AddPaymentMethodsFragment : Fragment() {

    private var acceptedPaymentMethods: ArrayList<String>? = null

    private lateinit var viewModel: AddPaymentMethodsViewModel

    private var lastClickTimePayPal: Long = 0

    private var brainTreeFragment: BraintreeFragment? = null

    private lateinit var connectivityLiveData: ConnectivityLiveData

    private var emptyStateLayout: EmptyStateLayout? = null

    private lateinit var addPaymentMethodsAdapter: AddPaymentMethodsAdapter

    @Inject
    lateinit var analyticsPlatformAdapter: AnalyticsPlatformAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = createViewModel { CustomerMobileApp.appComponent.addPaymentMethodsViewModel }

        CustomerMobileApp.appComponent.inject(this)

        acceptedPaymentMethods =
            activity?.intent?.getStringArrayListExtra(AppConstants.ACCEPTED_PAYMENTS)

        connectivityLiveData =
            ConnectivityLiveData(activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
        connectivityLiveData.observe(
            this,
            Observer<Boolean> { isOnline ->
                acceptedPaymentMethods?.let {
                    if (isOnline!! && brainTreeFragment == null && hasPayPal(acceptedPaymentMethods!!)) {
                        viewModel.getPayPalTokenLiveData()
                    }
                }
            }
        )
    }

    private fun hasPayPal(acceptedPaymentMethods: ArrayList<String>): Boolean {
        acceptedPaymentMethods.forEach {
            if (it.toLowerCase(Locale.getDefault())
                    .equals(AppConstants.PAYMENT_TYPE_PAYPAL, ignoreCase = true)
            ) {
                return true
            }
        }
        return false
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_add_payment_methods, container)
        emptyStateLayout = view.findViewById(R.id.emptyStateLayout1)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerViewPaymentMethods.layoutManager = GridLayoutManager(activity, 2)
        addPaymentMethodsAdapter = AddPaymentMethodsAdapter { onClick(it) }
        recyclerViewPaymentMethods.adapter = addPaymentMethodsAdapter
        showSupportedPayments(acceptedPaymentMethods)

        if (acceptedPaymentMethods != null && acceptedPaymentMethods!!.contains(AppConstants.PAYMENT_TYPE_PAYPAL)) {
            viewModel.getPayPalClientToken().observe(
                this,
                Observer<String> {
                    if (it != null) {
                        try {
                            brainTreeFragment = BraintreeFragment.newInstance(activity, it)
                        } catch (e: InvalidArgumentException) {
                            Timber.e(e)
                        }
                    }
                }
            )
            viewModel.getSentNonceToServerResult().observe(
                this,
                Observer { success ->
                    if (success!!) {
                        analyticsPlatformAdapter.paymentMethodAdded(
                            AnalyticsPlatformsContract.PaymentType.PAYPAL,
                            "",
                            AnalyticsPlatformsContract.Status.SUCCESS,
                            ""
                        )
                        emptyStateLayout?.showContent()
                        activity?.setResult(AppConstants.ADD_PAYMENT_METHOD_REQUEST_CODE, Intent())
                        activity?.finish()
                    } else {
                        analyticsPlatformAdapter.paymentMethodAdded(
                            AnalyticsPlatformsContract.PaymentType.PAYPAL,
                            "",
                            AnalyticsPlatformsContract.Status.FAILURE,
                            ""
                        )
                    }
                }
            )
        }
    }

    private fun showSupportedPayments(paymentTypes: ArrayList<String>?) {
        paymentTypes?.let {
            val paymentMethods = LinkedHashSet<String>()
            paymentTypes.forEach {
                val paymentType = it.toLowerCase(Locale.getDefault())
                if (isPaymentMethodCardType(paymentType)) {
                    paymentMethods.add(AppConstants.PAYMENT_TYPE_CARD)
                } else if (
                    paymentType == AppConstants.PAYMENT_TYPE_PAYPAL
                // TODO v3232 Uncomment this if add wallet functionality is required
                // || paymentType == AppConstants.PAYMENT_TYPE_WALLET
                ) {
                    paymentMethods.add(paymentType)
                }
            }
            addPaymentMethodsAdapter.submitList(paymentMethods.toMutableList())
            addPaymentMethodsAdapter.notifyDataSetChanged()
        }
    }

    private fun isPaymentMethodCardType(paymentMethod: String): Boolean {
        return paymentMethod == AppConstants.CARD_TYPE_AMERICAN_EXPRESS ||
                paymentMethod == AppConstants.CARD_TYPE_CIRRUS_DEBIT ||
                paymentMethod == AppConstants.CARD_TYPE_DINERS_CLUB ||
                paymentMethod == AppConstants.CARD_TYPE_DISCOVER ||
                paymentMethod == AppConstants.CARD_TYPE_JCB ||
                paymentMethod == AppConstants.CARD_TYPE_MASTERCARD ||
                paymentMethod == AppConstants.CARD_TYPE_SOLO_DEBIT ||
                paymentMethod == AppConstants.CARD_TYPE_UNION_PAY ||
                paymentMethod == AppConstants.CARD_TYPE_VISA
    }

    private fun onClick(paymentType: String) {
        when (paymentType) {
            AppConstants.PAYMENT_TYPE_CARD -> {
                val intent = Intent(activity, AddPaymentCardActivity::class.java)
                intent.putStringArrayListExtra(
                    AppConstants.ACCEPTED_PAYMENTS,
                    acceptedPaymentMethods
                )
                intent.putExtra(
                    AppConstants.SHOPPING_CART_INTENT,
                    activity!!.intent.getBooleanExtra(AppConstants.SHOPPING_CART_INTENT, false)
                )
                this.startActivityForResult(intent, AppConstants.ADD_CARD_REQUEST_CODE)
            }

            AppConstants.PAYMENT_TYPE_PAYPAL -> {
                if (context?.isOnline()!!) {
                    if (SystemClock.elapsedRealtime() - lastClickTimePayPal < 4000) return
                    if (brainTreeFragment != null) {
                        PayPal.authorizeAccount(brainTreeFragment)
                    } else {
                        GlobalScope.launch {
                            delay(400)
                            if (brainTreeFragment != null) PayPal.authorizeAccount(brainTreeFragment)
                        }
                    }
                    lastClickTimePayPal = SystemClock.elapsedRealtime()
                } else {
                    context?.connectionErrorDialog()
                }
            }
// TODO v3232 Uncomment this if add wallet functionality is required
            // AppConstants.PAYMENT_TYPE_WALLET -> {
            //     val intent = Intent(activity, AddWalletActivity::class.java)
            //     intent.putExtra(
            //         AppConstants.SHOPPING_CART_INTENT,
            //         activity!!.intent.getBooleanExtra(
            //             AppConstants.SHOPPING_CART_INTENT,
            //             false
            //         )
            //     )
            //     this.startActivityForResult(intent, AppConstants.ADD_CARD_REQUEST_CODE)
            // }
        }
    }

    fun onPaymentMethodNonceCreated(paymentMethodNonce: PaymentMethodNonce) {
        emptyStateLayout?.showLoading(
            R.drawable.ic_payment_card_filled,
            R.string.payment_adding_payment_method
        )
        viewModel.sendPayPalNonceToServer(paymentMethodNonce.nonce)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppConstants.ADD_CARD_REQUEST_CODE) {
            if (resultCode == AppConstants.ADD_CARD_SUCCESS_CODE) {
                activity?.setResult(AppConstants.ADD_CARD_SUCCESS_CODE, Intent())
                activity?.finish()
            } else if (resultCode == AppConstants.ADD_WALLET_SUCCESS_CODE) {
                activity?.finish()
            }
        }
    }
}
