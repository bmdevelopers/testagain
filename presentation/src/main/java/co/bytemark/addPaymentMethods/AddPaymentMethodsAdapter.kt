package co.bytemark.addPaymentMethods

import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.bytemark.R
import co.bytemark.helpers.AppConstants
import kotlinx.android.synthetic.main.add_payment_method_item.view.*
import java.util.*

/** Created by Santosh on 2019-11-08.
 * Copyright (c) 2019 Bytemark Inc. All rights reserved.
 */

class AddPaymentMethodsAdapter(
        private val onClickListener: (String) -> Unit
) : ListAdapter<String, AddPaymentMethodsAdapter.AddPaymentMethodsViewHolder>(PaymentMethodsDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddPaymentMethodsViewHolder =
            LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.add_payment_method_item, parent, false)
                    .run { AddPaymentMethodsViewHolder(this) }

    override fun onBindViewHolder(holder: AddPaymentMethodsViewHolder, position: Int) =
            holder.bind(getItem(position))

    class PaymentMethodsDiffCallback : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(firstPaymentMethod: String, secondPaymentMethod: String)
                : Boolean =
                firstPaymentMethod == secondPaymentMethod

        override fun areContentsTheSame(firstPaymentMethod: String, secondPaymentMethod: String)
                : Boolean =
                firstPaymentMethod == secondPaymentMethod
    }

    inner class AddPaymentMethodsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(paymentMethod: String) {
            itemView.apply {
                val title = getTitle(paymentMethod).first
                if (title != null) {
                    textViewTitle.text = title
                } else {
                    textViewTitle.visibility = View.GONE
                }
                imageView.setImageResource(getImageForPaymentMethod(paymentMethod))
               contentDescription = getTitle(paymentMethod).second
            }
            itemView.setOnClickListener { onClickListener(paymentMethod) }
        }

        private fun getTitle(paymentMethod: String): Pair<String?,String> {
            return when (paymentMethod) {
                AppConstants.PAYMENT_TYPE_CARD ->
                   Pair(itemView.context.getString(R.string.payment_credit_card), itemView.context.getString(R.string.payment_add_credit_card_voonly))
                AppConstants.PAYMENT_TYPE_PAYPAL ->
                    Pair(null, itemView.context.getString(R.string.payment_add_paypal_voonly))
               // TODO v3232 Uncomment this if add wallet functionality is required
                // AppConstants.PAYMENT_TYPE_WALLET ->
                //     itemView.context.getString(R.string.payment_stored_value_wallet)
                else ->
                    Pair(null, "")
            }
        }

        private fun getImageForPaymentMethod(paymentMethod: String): Int =
                when (paymentMethod) {
                    AppConstants.PAYMENT_TYPE_PAYPAL.toLowerCase(Locale.getDefault()) ->
                        R.drawable.ic_paypal
                    AppConstants.PAYMENT_TYPE_CARD.toLowerCase(Locale.getDefault()) ->
                        R.drawable.cards
                    // AppConstants.PAYMENT_TYPE_WALLET.toLowerCase(Locale.getDefault()) ->
                    //     R.drawable.ic_wallet_colored
                    else ->
                        R.drawable.cards
                }
    }
}
