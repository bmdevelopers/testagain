package co.bytemark.fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import co.bytemark.sdk.BytemarkSDK;
import de.hafas.notification.registration.PushRegistrationHandler;

public class BMFirebaseInstanceIdService extends FirebaseInstanceIdService
{
    @Override
    public void onTokenRefresh()
    {
        // Obtain Firebase token.
        String token = FirebaseInstanceId.getInstance().getToken();

        // Register with your own push server.
        // ...
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        BytemarkSDK.sendRegistrationToServer(token);

        // Register with HAFAS push server.
        if (token != null)
            PushRegistrationHandler.getInstance().setRegistrationId(getApplicationContext(), token);
    }
}