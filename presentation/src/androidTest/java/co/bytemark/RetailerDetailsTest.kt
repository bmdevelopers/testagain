package co.bytemark

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isEnabled
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.rule.ActivityTestRule
import co.bytemark.domain.model.incomm.*
import co.bytemark.helpers.AppConstants
import co.bytemark.incomm.retailer_details.RetailerDetailsActivity
import co.bytemark.incomm.retailer_details.RetailerDetailsFragment
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@MediumTest
class RetailerDetailsTest {

    lateinit var fragment: RetailerDetailsFragment

    @get:Rule
    var activityTestRule: ActivityTestRule<RetailerDetailsActivity> =
        ActivityTestRule(RetailerDetailsActivity::class.java, false, false)

    @Test
    fun test_completeScreen() {
        val incomm = Incomm(
            id = "",
            hours = null,
            phones = mutableListOf(""),
            merchant = Merchant("", "", "", ""),
            distance = 0.0,
            coordinates = Coordinates(0.0, 0.0),
            address = Address(
                address1 = "",
                city = "",
                country = "",
                state = "",
                zipCode = "", formattedAddress = "1192 MYRTLE AVE, BROOKLYN, " +
                        "KINGS, NY 11221-2613, USA",
                county = ""
            )
        )
        val barcodeDetail = IncommBarcodeDetail(
            "799366947520006371683018605486",
            "https://media.cashtie.com/images/barcode_locator.png"
        )
        val intent = Intent()
        intent.putExtra(
            AppConstants.INTENT_INCOMM_BARCODE_DETAIL, barcodeDetail
        )
        intent.putExtra(AppConstants.INTENT_INCOMM_STORE, incomm)
        intent.putExtra(AppConstants.INTENT_INCOMM_ORDER_AMOUNT, 330)

        activityTestRule.launchActivity(intent)

        fragment =
            activityTestRule.activity.supportFragmentManager.fragments[0] as RetailerDetailsFragment

        onView(ViewMatchers.withId(R.id.barcodeNumberTextView))
            .check(matches(ViewMatchers.withText(barcodeDetail.accountNumber)))

        onView(ViewMatchers.withId(R.id.retailerDetailAddressTextView))
            .check(matches(ViewMatchers.withText(incomm.address?.formattedAddress)))

        onView(ViewMatchers.withId(R.id.paymentAmountTextView))
            .check(matches(ViewMatchers.withText("$3.30")))

        onView(ViewMatchers.withId(R.id.goToTicketButton)).check(
            matches((isEnabled()))
        )
    }

}