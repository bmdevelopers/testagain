package co.bytemark.testsuit

import co.bytemark.RetailerDetailsTest
import co.bytemark.SelectRetailerTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(SelectRetailerTest::class, RetailerDetailsTest::class)
class IncommTestSuit {
}