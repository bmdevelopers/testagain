package co.bytemark.sdk;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import co.bytemark.sdk.Api.JsonResponse;

import static androidx.test.InstrumentationRegistry.getTargetContext;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class UICPayloadTest {

    private List<Pass> passes;
    private String jsonStringPasses;

    public UICPayloadTest() {
    }

    @Before
    public void setUp() throws Exception {
        jsonStringPasses = loadJSONFromAsset("passes_uicpayload_test.json");
        passes = Pass.createListFromJson(new JsonResponse(jsonStringPasses));
    }

    @After
    public void finish() {
        passCacheDatabaseManager().deleteDb();
    }

    public PassCacheDatabaseManager passCacheDatabaseManager() {
        return PassCacheDatabaseManager.getInstance(getTargetContext());
    }

    public String loadJSONFromAsset(String fileName) {
        String jsonContent = null;
        try {
            InputStream is = getTargetContext().getResources().getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            jsonContent = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return jsonContent;
    }

    @Test
    public void parseJsonAndTestValuesFilledCorrectly() throws Exception {
        assertNotNull(passes);
        assertTrue(passes.size() == 1);

        final Pass pass = passes.get(0);
        assertUicData(pass);
    }


    @Test
    public void testDatabaseOperationSavePass() throws Exception {
        passCacheDatabaseManager().parseJsonResponseAndSaveWithDBClear(new JsonResponse(jsonStringPasses));
        assertTrue(passCacheDatabaseManager().getAllCachedPasses(new GregorianCalendar()).size() == 1);
    }

    @Test
    public void testDatabaseOperationUicPayloadSaved() throws Exception {
        final ArrayList<Pass> allCachedPasses = passCacheDatabaseManager().getAllCachedPasses(new GregorianCalendar());
        assertTrue(allCachedPasses.size() == 1);

        assertUicData(allCachedPasses.get(0));
    }

    private void assertUicData(Pass pass) {
        assertNotNull(pass.getKapschPayload());

        final ActivationTimestampsData activationTimestampsData = pass.getKapschPayload().getActivationTimestampsData();
        assertNotNull(activationTimestampsData);
        assertEquals("3392TS", activationTimestampsData.getRecordId());
        assertEquals("01", activationTimestampsData.getRecordVersion());


        final FareData fareData = pass.getKapschPayload().getFareData();
        assertNotNull(fareData);
        assertEquals("3392FD", fareData.getRecordId());
        assertEquals("01", fareData.getRecordVersion());
        assertEquals("<hex value of the length of the fare record>", fareData.getRecordLength());
        //assertEquals("<product data from list contracts>", fareData.getData());

        final QrCodeData qrCodeData = pass.getKapschPayload().getQrCodeData();
        assertNotNull(qrCodeData);
        assertEquals("#UT", qrCodeData.getUniqueMessageId());
        assertEquals("01", qrCodeData.getMessageTypeVersion());
        assertEquals("3392", qrCodeData.getRicsCode());
        assertEquals("01234", qrCodeData.getSignatureId());
    }
}