package co.bytemark.sdk

import android.content.Context
import android.view.View
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import co.bytemark.CustomerMobileApp
import co.bytemark.R
import co.bytemark.ticket_storage.TicketStorageActivity
import co.bytemark.ticket_storage.TicketStorageFragment
import co.bytemark.ticket_storage.TicketStorageViewModel
import co.bytemark.widgets.util.getViewModel
import org.hamcrest.CoreMatchers
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException


@RunWith(AndroidJUnit4::class)
class TicketStorageUITest {

    private lateinit var viewModel: TicketStorageViewModel
    private lateinit var fragment: TicketStorageFragment
    private var response: List<Pass>? = null

    @Rule
    @JvmField
    var activityTestRule: ActivityTestRule<TicketStorageActivity> = ActivityTestRule(
        TicketStorageActivity::class.java)

    @Before
    fun setUp() {
        fragment = activityTestRule.activity.supportFragmentManager.fragments[0] as TicketStorageFragment
        viewModel = fragment.getViewModel(CustomerMobileApp.appComponent.ticketStorageViewModel)
        response = getPassList()
    }

    @Test
    fun testUnLockedPasses() {
        activityTestRule.runOnUiThread {
            val devicePassList = ArrayList<Pass>()
            response?.get(0)?.let { devicePassList.add(it) }
            viewModel.devicePassesLiveData.value = devicePassList

            onView(withId(R.id.devicePassesRecyclerView))
                .check(matches(isDisplayed()))

            onView(withId(R.id.devicePassesRecyclerView)).check(matches(atPosition(0, hasDescendant(allOf(withText("One Way"), withId(R.id.ticketTitle))))))
            onView(withId(R.id.devicePassesRecyclerView)).check(matches(atPosition(0, hasDescendant(allOf(withText("One Way"), withId(R.id.ticketDescription))))))
            onView(withId(R.id.devicePassesRecyclerView)).check(matches(atPosition(0, hasDescendant(withText("Use By: 11/12/20")))))
            onView(withId(R.id.devicePassesRecyclerView)).check(matches(atPosition(0, hasDescendant(
                withTagValue(
                    CoreMatchers.equalTo(
                    R.drawable.to_cloud_material))))))
        }
    }

    @Test
    fun testLockedPasses() {
        activityTestRule.runOnUiThread {
            val devicePassList = ArrayList<Pass>()
            response?.get(1)?.let { devicePassList.add(it) }
            viewModel.devicePassesLiveData.value = devicePassList

            onView(withId(R.id.devicePassesRecyclerView)).check(matches(isDisplayed()))
            onView(withId(R.id.devicePassesRecyclerView)).check(matches(atPosition(0, hasDescendant(allOf(withText("One Way"), withId(R.id.ticketTitle))))))
            onView(withId(R.id.devicePassesRecyclerView)).check(matches(atPosition(0, hasDescendant(allOf(withText("One Way"), withId(R.id.ticketDescription))))))
            onView(withId(R.id.devicePassesRecyclerView)).check(matches(atPosition(0, hasDescendant(withText("Use By: 11/13/20")))))
            onView(withId(R.id.devicePassesRecyclerView)).check(matches(atPosition(0, hasDescendant(
                withTagValue(CoreMatchers.equalTo(
                    R.drawable.to_cloud_material))))))
        }
    }

    private fun atPosition(position: Int, @NonNull itemMatcher: Matcher<View?>): Matcher<View?>? {
        checkNotNull(itemMatcher)
        return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                itemMatcher.describeTo(description)
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                val viewHolder: RecyclerView.ViewHolder =
                    view.findViewHolderForAdapterPosition(position)
                        ?: // has no item on such position
                        return false
                return itemMatcher.matches(viewHolder.itemView)
            }
        }
    }

    private fun getPassList() : List<Pass>{
        var list = ArrayList<Pass>()
        try {
            val json = JSONObject(getJsonDataFromAsset("mock_pass_response.json"))
            val array: JSONArray = json.getJSONArray("passes")
            list = Pass.createListFromJson(array)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return list
    }

    private fun getJsonDataFromAsset(fileName: String): String? {
        val jsonString: String
        try {
            jsonString = InstrumentationRegistry.getInstrumentation().context.assets.open(fileName)
                .bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }
}