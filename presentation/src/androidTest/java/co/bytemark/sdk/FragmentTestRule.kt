package co.bytemark.sdk

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.test.rule.ActivityTestRule


class FragmentTestRule<F : Fragment>(private val mFragmentClass: Class<F>, private val bundle: Bundle?) : ActivityTestRule<V3_Activity>(V3_Activity::class.java, true, true) {

    override fun afterActivityLaunched() {
        super.afterActivityLaunched()


        activity.runOnUiThread {
            try {
                //Instantiate and insert the fragment into the container layout
                val manager = activity.supportFragmentManager
                val transaction = manager.beginTransaction()
                val fragment = mFragmentClass.newInstance()
                fragment.arguments = bundle
                transaction.replace(R.id.frameLayout, fragment)
                transaction.commit()
            } catch (e: InstantiationException) {

            } catch (e: IllegalAccessException) {
            }
        }
    }

    override fun beforeActivityLaunched() {
        super.beforeActivityLaunched()
        V3_Activity.setPasses(bundle?.getParcelableArrayList("PASSES"))
    }

}