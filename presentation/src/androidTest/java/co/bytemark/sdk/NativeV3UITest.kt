@file:Suppress("DEPRECATION")

package co.bytemark.sdk

import android.graphics.Color
import android.os.Bundle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.json.JSONObject
import org.junit.*
import org.junit.runner.RunWith
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import androidx.test.espresso.matcher.BoundedMatcher
import android.view.View
import org.hamcrest.Matcher
import android.widget.TextView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.intent.Checks
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Description


@RunWith(AndroidJUnit4::class)
class NativeV3UITest {


    companion object {
        var bundle : Bundle? = null
        @BeforeClass
        @JvmStatic
        fun setPasses() {
            val response = getJson("MockPassesArray")
            val jsonObject = JSONObject(response)
            val passListObj = Passes(jsonObject)
            val passes = passListObj.getPasses()
            bundle = Bundle()
            bundle?.putParcelableArrayList("PASSES",passes)

        }
        private fun getJson(fileName: String): String {
            val resource = javaClass.classLoader?.getResourceAsStream(fileName)
            val br = BufferedReader(InputStreamReader(resource))

            var line: String?
            var sb = StringBuilder()

            try {
                do {
                    line = br.readLine()
                    if(line == null)
                        break
                    sb.append(line)
                } while (true)
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                br.close()
            }

            return sb.toString()
        }
    }


    @Rule @JvmField
    var fragmentTestRule: FragmentTestRule<NativeV3Fragment> = FragmentTestRule(NativeV3Fragment::class.java,bundle)

    @Test
    fun checkViewVisibility() {
        onView(withId(R.id.headerSurfaceView)).check(matches(isDisplayed()))
        onView(withId(R.id.footerSurfaceView)).check(matches(isDisplayed()))
        onView(withId(R.id.bodySurfaceView)).check(matches(isDisplayed()))
    }

    @Test
    fun verifyTextBgColor(){
        onView(withId(R.id.textViewFooterBottom)).check(matches(isDisplayed()))
        onView(withId(R.id.textViewFooterBottom)).check(matches(withBgColor(Color.parseColor("#FFFFFF"))))

        onView(withId(R.id.textViewFooterTop)).check(matches(isDisplayed()))
        onView(withId(R.id.textViewFooterTop)).check(matches(withBgColor(Color.parseColor("#FFFFFF"))))

        onView(withId(R.id.textViewLabel)).check(matches(isDisplayed()))
        onView(withId(R.id.textViewLabel)).check(matches(withBgColor(Color.parseColor("#8B9E2B"))))
    }

    @Test
    fun showHideQr(){
        onView(allOf(withId(R.id.imageButtonQrCode),hasContentDescription())).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.imageButtonQrCode),hasContentDescription())).perform(click()).check(matches(not(isDisplayed())))
        onView(withId(R.id.textViewShowCode)).check(matches(not(isDisplayed())))
    }


    private fun withBgColor(color: Int): Matcher<View> {
        Checks.checkNotNull(color)
        return object : BoundedMatcher<View, TextView>(TextView::class.java) {
            public override fun matchesSafely(textView: TextView): Boolean {
                return color == textView.currentTextColor
            }

            override fun describeTo(description: Description) {
            }
        }
    }

}