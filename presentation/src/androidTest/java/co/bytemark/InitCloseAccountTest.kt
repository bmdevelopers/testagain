package co.bytemark

import android.app.Activity
import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.ActivityResultMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import co.bytemark.authentication.AuthenticationActivity
import co.bytemark.authentication.delete_account.InitDeleteAccountFragment
import co.bytemark.extensions.*
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
open class InitCloseAccountTest {

    @get:Rule
    var activityTestRule =
        object  : ActivityTestRule<AuthenticationActivity>(AuthenticationActivity::class.java) {
            override fun getActivityIntent() = Intent().apply {
                putExtra(AuthenticationActivity.EXTRA_KEY_DEFAULT_SCREEN,
                         InitDeleteAccountFragment.EXTRA_TAG)
            }
        }

    @Test
    fun test_toolbarTitle(){
        onView(withId(R.id.toolbar)).check(matches(hasDescendant(
            withText(getResourceString(R.string.close_account)))))
    }

    @Test
    fun test_ui_visibility() {
        R.id.deleteMessageTextView.checkIsDisplayed()
        R.id.deleteButton.checkIsDisplayed()
        R.id.cancelButton.checkIsDisplayed()
        R.id.textViewMessageSelectPaymentMethod.checkIsGone()
        R.id.groupDeleteAccount.checkIsInvisible()
    }

    @Test
    fun test_view_colors() {
        R.id.deleteMessageTextView.checkHasTextColor(R.color.orgCollectionPrimaryTextColor)
        R.id.deleteButton.checkHasTextColor(R.color.orgAccentPrimaryTextColor)
        R.id.deleteButton.checkHasBackgroundColor(R.color.errorIndicatorColor)
        R.id.cancelButton.checkHasTextColor(R.color.orgCollectionPrimaryTextColor)
    }

    @Test
    fun test_buttonText(){
        R.id.deleteButton.checkHasText(R.string.close_account)
        R.id.cancelButton.checkHasText(R.string.popup_cancel)
    }

    @Test
    fun test_message() {
        R.id.deleteMessageTextView.checkHasText(R.string.close_account_message)
    }


    @Test
    fun test_delete_button_action(){
        R.id.deleteButton.performClick()
        with(UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())) {
            checkTextExists(R.string.close_account_dialog_title)
            checkTextExists(R.string.close_account_message)
            checkTextExists(getResourceString(R.string.close_account)!!.toUpperCase())
            checkTextExists(getResourceString(R.string.popup_cancel)!!.toUpperCase())
        }
    }

    @Test
    fun test_cancel_action(){
        R.id.cancelButton.performClick()
        assertThat(activityTestRule.activityResult,
                   ActivityResultMatchers.hasResultCode(Activity.RESULT_CANCELED)
        );
    }

    @Test
    fun test_dialog_cancel_action(){
        R.id.deleteButton.performClick()
        with(UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())) {
            checkTextExists(R.string.close_account_dialog_title)
            checkTextExists(R.string.close_account_message)
            checkTextExists(getResourceString(R.string.close_account)!!.toUpperCase())
            checkTextExists(getResourceString(R.string.popup_cancel)!!.toUpperCase())
            clickOnText(getResourceString(R.string.popup_cancel)!!.toUpperCase())
            Assert.assertFalse(textExists(R.string.close_account_dialog_title))
        }
    }

}