package co.bytemark

import androidx.test.annotation.UiThreadTest
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import co.bytemark.schedules.SchedulesActivityFragment
import co.bytemark.sdk.FragmentTestRule
import org.hamcrest.Matchers.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@MediumTest
class SchedulesTest {

    @get:Rule
    var fragmentTestRule: FragmentTestRule<SchedulesActivityFragment> = FragmentTestRule(SchedulesActivityFragment::class.java, null)

    @Test
    @UiThreadTest
    fun test_selectSchedulesAndBuy() {
        onView(withId(R.id.spinnerOrigin)).perform(click())
        onData(allOf(`is`(instanceOf(String::class.java)))).atPosition(0).perform(click())
        onView(withId(R.id.spinnerDestination)).perform(click())
        onData(allOf(`is`(instanceOf(String::class.java)))).atPosition(0).perform(click())
        onView(withId(R.id.buttonBuy)).perform(click())
    }

    @Test
    @UiThreadTest
    fun test1_swapOriginAndDestination() {
        onView(withId(R.id.spinnerOrigin)).perform(click())
        onData(allOf(`is`(instanceOf(String::class.java)))).atPosition(1).perform(click())
        onView(withId(R.id.spinnerDestination)).perform(click())
        onData(allOf(`is`(instanceOf(String::class.java)))).atPosition(0).perform(click())
        onView(withId(R.id.imageViewSwap)).perform(click())
        onView(withId(R.id.buttonBuy)).perform(click())
    }

    @Test
    @UiThreadTest
    fun test1_selectDifferentDate() {
        onView(withId(R.id.spinnerOrigin)).perform(click())
        onData(allOf(`is`(instanceOf(String::class.java)))).atPosition(1).perform(click())
        onView(withId(R.id.spinnerDestination)).perform(click())
        onData(allOf(`is`(instanceOf(String::class.java)))).atPosition(0).perform(click())
       // onView(withId(R.id.buttonSchedulesDate)).perform(PickerActions.setDate(2020, 7, 14));
        onView(withId(R.id.buttonBuy)).perform(click())
    }

}