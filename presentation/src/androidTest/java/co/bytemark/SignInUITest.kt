package co.bytemark

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.rule.ActivityTestRule
import co.bytemark.authentication.AuthenticationActivity
import co.bytemark.authentication.signin.SignInFragment
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@RunWith(AndroidJUnit4::class)
@MediumTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class SignInUITest {

    lateinit var fragment: SignInFragment

    @get:Rule
    var activityTestRule: ActivityTestRule<AuthenticationActivity> =
        ActivityTestRule(AuthenticationActivity::class.java)

    @Before
    fun initUI() {
        fragment =
            activityTestRule.activity.supportFragmentManager.fragments[0] as SignInFragment
    }

    @Test
    fun test0_checkSignInFields() {
        onView(withId(R.id.forgotPasswordTextView)).check(matches(isDisplayed()))
        onView(withId(R.id.dontHaveAccountPlaceHolderTextView)).check(matches(isDisplayed()))
        onView(withId(R.id.signUpTextView)).check(matches(isDisplayed()))
        onView(withId(R.id.emailSignInTextInputLayout)).check(matches(hasDescendant(withText(""))))
        onView(withId(R.id.emailSignInTextInputEditText)).perform(
            clearText(),
            typeText("foo@bytemark.co"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.emailSignInTextInputLayout)).check(matches(hasDescendant(withText(""))))
        onView(withId(R.id.signInButton)).check(matches(not(isEnabled())))

        onView(withId(R.id.emailSignInTextInputEditText)).perform(
            clearText(),
            typeText("foo@bytemark"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.emailSignInTextInputLayout)).check(matches(hasDescendant(withText("Invalid email"))))
        onView(withId(R.id.signInButton)).check(matches(not(isEnabled())))

        onView(withId(R.id.passwordSignInTextInputEditText)).perform(
            clearText(),
            typeText("foo"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.passwordSignInTextInputLayout)).check(matches(hasDescendant(withText(""))))
        onView(withId(R.id.signInButton)).check(matches(not(isEnabled())))

        onView(withId(R.id.passwordSignInTextInputEditText)).perform(
            clearText(),
            typeText(""),
            closeSoftKeyboard()
        )
        onView(withId(R.id.passwordSignInTextInputLayout)).check(matches(hasDescendant(withText("Password cannot be empty."))))
        onView(withId(R.id.signInButton)).check(matches(not(isEnabled())))

        onView(withId(R.id.emailSignInTextInputEditText)).perform(
            clearText(),
            typeText("foo@bytemark.co"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.emailSignInTextInputLayout)).check(matches(hasDescendant(withText(""))))
        onView(withId(R.id.signInButton)).check(matches(not(isEnabled())))

        onView(withId(R.id.passwordSignInTextInputEditText)).perform(
            clearText(),
            typeText("foo"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.passwordSignInTextInputLayout)).check(matches(hasDescendant(withText(""))))
        onView(withId(R.id.signInButton)).check(matches(isEnabled()))

        onView(withId(R.id.signInButton)).perform(click())
        onView(withId(R.id.progressViewSignIn)).check(matches(isDisplayed()))

        val bindingConfig = fragment.getSignInBindingConfig()

        if (bindingConfig.isAppIconVisible) {
            onView(withId(R.id.appLogoSignInImageView)).check(matches(isDisplayed()))
        } else {
            onView(withId(R.id.appLogoSignInImageView)).check(matches(not(isDisplayed())))
        }

        if (bindingConfig.socialSignInList?.contains("facebook") == true) {
            onView(withId(R.id.facebookSignInButton)).check(matches(isDisplayed()))
        } else {
            onView(withId(R.id.facebookSignInButton)).check(matches(not(isDisplayed())))
        }
        if (bindingConfig.socialSignInList?.contains("google") == true) {
            onView(withId(R.id.googleSignInButton)).check(matches(isDisplayed()))
        } else {
            onView(withId(R.id.googleSignInButton)).check(matches(not(isDisplayed())))
        }
        if (bindingConfig.socialSignInList?.contains("apple") == true) {
            onView(withId(R.id.appleSignInButton)).check(matches(isDisplayed()))
        } else {
            onView(withId(R.id.appleSignInButton)).check(matches(not(isDisplayed())))
        }

    }

}