package co.bytemark


import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import co.bytemark.use_tickets.UseTicketsActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class ManageScreenUITest {

    @get:Rule
    var activityTestRule = ActivityTestRule(UseTicketsActivity::class.java, true, true)

    @Test
    fun test_openManage() {
        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        device.findObject(UiSelector().description("Open navigation drawer")).click()
        device.findObject(UiSelector().text("Manage")).click()
    }

}