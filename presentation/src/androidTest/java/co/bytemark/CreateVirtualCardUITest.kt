package co.bytemark

import android.view.View
import androidx.test.annotation.UiThreadTest
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.rule.ActivityTestRule
import androidx.viewpager.widget.ViewPager
import co.bytemark.domain.model.common.BMError
import co.bytemark.manage.createOrLinkVirtualCard.CreateOrLinkCardActivity
import co.bytemark.manage.createOrLinkVirtualCard.createVirtualCard.CreateVirtualCardFragment
import co.bytemark.manage.createOrLinkVirtualCard.createVirtualCard.CreateVirtualCardViewModel
import co.bytemark.widgets.util.getViewModel
import kotlinx.android.synthetic.main.activity_tabs.*
import kotlinx.android.synthetic.main.fragment_create_virtual_card.*
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@RunWith(AndroidJUnit4::class)
@MediumTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class CreateVirtualCardUITest {

    private var createVirtualCardFragment: CreateVirtualCardFragment? = null

    private var createVirtualCardViewModel: CreateVirtualCardViewModel? = null

    private lateinit var viewPager: ViewPager

    @get:Rule
    var activityRule: ActivityTestRule<CreateOrLinkCardActivity> = ActivityTestRule(CreateOrLinkCardActivity::class.java)

    @Before
    fun initUIAndData() {
        viewPager = activityRule.activity.swipeToggleViewPager
        createVirtualCardFragment = (activityRule.activity.tabPagerAdapter?.instantiateItem(viewPager, 0) as? CreateVirtualCardFragment)
        createVirtualCardViewModel = createVirtualCardFragment?.getViewModel(CustomerMobileApp.appComponent.createVirtualCardViewModel)
    }

    @Test
    @UiThreadTest
    fun test1_CreateVirtualCard() {
        createVirtualCardFragment?.editTextCardNickname?.setText("MyCard")
        createVirtualCardFragment?.spinnerCategory?.setSelection(0)
        createVirtualCardFragment?.buttonCreateVirtualCard?.performClick()
    }

    @Test
    @UiThreadTest
    fun test2_onError() {
        createVirtualCardViewModel?.errorVirtualLiveData?.value = Pair(BMError(102, "Unexpected Error"), 1)
    }

    @Test
    @UiThreadTest
    fun test3_ScrollRightToLeftViewPager() {
        activityRule.activity.swipeToggleViewPager.arrowScroll(View.FOCUS_RIGHT)
    }

}