package co.bytemark

import android.content.Context
import android.content.Intent
import android.view.View
import android.view.accessibility.AccessibilityEvent
import androidx.test.espresso.Espresso
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.AccessibilityChecks
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import co.bytemark.appnotification.NotificationFragment
import co.bytemark.appnotification.NotificationViewModel
import co.bytemark.appnotification.NotificationsActivity
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Navigate
import co.bytemark.domain.model.notification.Notification
import co.bytemark.domain.model.notification.NotificationResponse
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.webview.WebViewActivity
import co.bytemark.widgets.util.getCurrentFragment
import co.bytemark.widgets.util.getViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import junit.framework.Assert.assertEquals
import kotlinx.android.synthetic.main.activity_user_photo.*
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.notification_not_logged_in.*
import kotlinx.android.synthetic.main.use_tickets_not_logged_in.*
import kotlinx.android.synthetic.main.use_tickets_not_logged_in.loginParentLinearLayout
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class NotificationUITest{

    companion object{
        //private var response : NotificationResponse? = null
        @BeforeClass
        @JvmStatic
        fun enableAccessibilityChecks() {
            AccessibilityChecks.enable().setRunChecksFromRootView(true)
        }
    }

    @get:Rule
    var activityTestRule: ActivityTestRule<NotificationsActivity> = ActivityTestRule(NotificationsActivity::class.java)

    private lateinit var viewModel: NotificationViewModel
    private lateinit var fragment: NotificationFragment
    private lateinit var response: NotificationResponse
    private lateinit var notification: Notification

    //change view model
    //observe state of the fragment

    @Before
    fun setUp() {
        notification = Notification(
                "id",
                "Note",
                "title",
                "teaser",
                "https://www.google.com",
                "body",
                true,
                "senderUuid",
                "recipientUuid",
                "timeCreated",
                "timeModified",
                "startDate",
                "endDate",
                true
        )
        fragment = activityTestRule.activity.supportFragmentManager.fragments[0] as NotificationFragment
        viewModel = fragment.getViewModel(CustomerMobileApp.appComponent.notificationViewModel)
        //Might need to download the notifications first to get a correct match
        response = Gson().fromJson(getJsonDataFromAsset(activityTestRule.activity.applicationContext,"mock_notification_alert.json"), object : TypeToken<NotificationResponse>() {}.type)

    }

    @Test
    fun test_load_data() {
        //This test will fail if account has no notifications
        activityTestRule.runOnUiThread{
            viewModel.loadNotifications()
            viewModel.notificationLiveData.getOrAwaitValue(5)
            assertEquals(viewModel.notificationLiveData.value!![0]::class, response?.notifications?.toMutableList()!![0]::class)
        }
    }

    @Test
    fun test_navigate_activity() {
        activityTestRule.runOnUiThread {
            fragment.observeNavigationLiveData()
            viewModel.navigationLiveData.value = Navigate.FinishActivity()
            //This might need a different indicator
            assertEquals(fragment.activity?.isFinishing, true)
        }
        //Need to reset the fragment
        //setUp()
    }

    /*@Test
    fun test_navigate_away_activity() {
        val testIntent = Intent(viewModel.application, WebViewActivity::class.java).apply {
            putExtra(
                    WebViewActivity.EXTRA_TITLE,
                    viewModel.application.getString(R.string.screen_title_notification)
            )
            putExtra(WebViewActivity.EXTRA_URL, notification.link)
            putExtra(WebViewActivity.EXTRA_PDF_SUPPORT, true)
            putExtra(WebViewActivity.EXTRA_NOTIFICATION, notification)
        }
        activityTestRule.runOnUiThread {
            fragment.observeNavigationLiveData()
            viewModel.navigationLiveData.value = Navigate.StartActivity(testIntent)
            viewModel.navigationLiveData.getOrAwaitValue(5)
        }
        //There's no way to get the fragment intent from the old fragment
        assertEquals(fragment.fragmentManager?.fragments!![0].activity?.intent.toString(), testIntent.toString())
    }*/

    @Test
    fun test_change_login_layout_visibility(){
        activityTestRule.runOnUiThread{
            fragment.observeVisibilityLiveData()
            viewModel.visibilityStateLiveData.value = NotificationViewModel.VisibilityState.LoginLayout(View.INVISIBLE)
            viewModel.visibilityStateLiveData.getOrAwaitValue(5)
            assertEquals(fragment.loginParentLinearLayout.visibility, View.INVISIBLE)
            viewModel.visibilityStateLiveData.value = NotificationViewModel.VisibilityState.SignInButton(View.INVISIBLE)
            viewModel.visibilityStateLiveData.getOrAwaitValue(5)
            assertEquals(fragment.signInButton.visibility, View.INVISIBLE)
            viewModel.visibilityStateLiveData.value = NotificationViewModel.VisibilityState.SubscribedLayout(View.INVISIBLE)
            viewModel.visibilityStateLiveData.getOrAwaitValue(5)
            assertEquals(fragment.subscribedLayout.visibility, View.INVISIBLE)
        }
    }

    @Test
    fun test_accessibility_live_data(){
        activityTestRule.runOnUiThread{
            fragment.observeAccessibilityLiveData()
            viewModel.visibilityStateLiveData.value = NotificationViewModel.VisibilityState.LoginLayout(View.VISIBLE)
            viewModel.visibilityStateLiveData.getOrAwaitValue(5)
            viewModel.accessibilityLiveData.value = NotificationViewModel.TalkBack.LoginLayout(R.string.you_are_signed_out)
            viewModel.accessibilityLiveData.getOrAwaitValue(5)
        }
        Espresso.onView(withId(R.id.loginParentLinearLayout)).perform(ViewActions.click())
        /*viewModel.accessibilityLiveData.observe(this, Observer {
            if (it is NotificationViewModel.TalkBack.LoginLayout) {
                loginParentLinearLayout.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
                loginParentLinearLayout.announceForAccessibility(getString(it.contentDescription))
            }
        })*/
    }

    @Test
    fun test_error_handler(){
        activityTestRule.runOnUiThread{
            fragment.observeErrorLiveData()
            viewModel.errorLiveData.value = BMError(BytemarkSDK.ResponseCode.DEVICE_LOST_OR_STOLEN)
            viewModel.errorLiveData.getOrAwaitValue(5)
        }
        Espresso.onView(withText(co.bytemark.sdk.R.string.device_lost_stolen_title)).perform(ViewActions.click())
    }

    private fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            /*jsonString = InstrumentationRegistry.getInstrumentation().context.assets.open(fileName)
                    .bufferedReader().use { it.readText() }*/
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

}