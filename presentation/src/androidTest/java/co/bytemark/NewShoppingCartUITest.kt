package co.bytemark

import android.content.Context
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.doubleClick
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import co.bytemark.sdk.model.product_search.entity.EntityResult
import co.bytemark.shopping_cart_new.NewShoppingCartActivity
import co.bytemark.shopping_cart_new.NewShoppingCartActivityFragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import java.io.IOException


@RunWith(AndroidJUnit4::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class NewShoppingCartUITest {

    companion object {
        @JvmStatic
        private var entityResult: List<EntityResult> = ArrayList()

        @JvmStatic
        private lateinit var fragment: NewShoppingCartActivityFragment
    }

    @Rule
    @JvmField
    var activityTestRule: ActivityTestRule<NewShoppingCartActivity> = ActivityTestRule(NewShoppingCartActivity::class.java)

    @Before
    fun setupEntity() {
        fragment = activityTestRule.activity.supportFragmentManager.fragments[0] as NewShoppingCartActivityFragment
        entityResult = Gson().fromJson(getJsonDataFromAsset(activityTestRule.activity,"MockEntityResults.json"), object : TypeToken<List<EntityResult>>() {}.type)
    }

    @Test
    fun enableSubscription() {
        entityResult[0].subscription.subscription_allowed = true
        activityTestRule.runOnUiThread {
            fragment.updateEntityResultList(entityResult)
        }
    }

    @Test
    fun disableSubscription() {
        entityResult[0].subscription.subscription_allowed = false
        activityTestRule.runOnUiThread {
            fragment.updateEntityResultList(entityResult)
        }
    }


    @Test
    fun onButtonQuantityClick() {
        onView(withId(R.id.buttonPlus))
                .perform(click(), doubleClick())
        onView(withId(R.id.buttonMinus))
                .perform(click())
    }

    @Test
    fun onCheckoutButtonClick() {
        onView(withId(R.id.checkoutButton))
                .perform(click())
    }

    @Test
    fun onDeleteButtonClick() {
        onView(withId(R.id.btn_delete))
                .perform(click())
    }

    private fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }
}