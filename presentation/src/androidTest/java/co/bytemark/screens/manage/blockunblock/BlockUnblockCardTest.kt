package co.bytemark.screens.manage.blockunblock

import android.app.Activity
import android.content.Intent
import android.text.SpannableStringBuilder
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.ActivityResultMatchers.hasResultCode
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import co.bytemark.BuildConfig
import co.bytemark.R
import co.bytemark.domain.model.fare_medium.FARE_MEDIUM_BLOCK_STATE
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.extensions.*
import co.bytemark.helpers.AppConstants
import co.bytemark.manage.block_unblock_card.BlockUnblockCardActivity
import co.bytemark.manage.block_unblock_card.BlockUnblockCardFragment
import co.bytemark.manage.block_unblock_card.BlockUnblockCardViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
open class BlockUnblockCardTest {

    lateinit var blockUnblockCardFragment: BlockUnblockCardFragment
    lateinit var blockUnblockCardViewModel: BlockUnblockCardViewModel
    lateinit var fareMedium: FareMedium

    //Send Data In Intent
    @get:Rule
    var activityTestRule =
        object : ActivityTestRule<BlockUnblockCardActivity>(BlockUnblockCardActivity::class.java) {
            override fun getActivityIntent(): Intent {
                val intent = Intent()
                intent.putExtra(AppConstants.FARE_MEDIA, getTestFareMedium())
                intent.putExtra(AppConstants.TITLE, "Block/Unblock")
                return intent
            }
        }

    @Before
    fun setUp() {
        activityTestRule.runOnUiThread {
            blockUnblockCardFragment =
                activityTestRule.activity.supportFragmentManager.fragments[0] as BlockUnblockCardFragment
            blockUnblockCardViewModel = blockUnblockCardFragment.getViewModel()
            fareMedium = getTestFareMedium()
        }
    }


    @Test
    fun test_toolbarTitle() {
        onView(withId(R.id.toolbar)).check(
            matches(
                hasDescendant(
                    withText("Block/Unblock")
                )
            )
        )
    }

    @Test
    fun test_buttonText() {

        R.id.positiveActionButton.checkHasText(
            if (fareMedium.state == FARE_MEDIUM_BLOCK_STATE) {
                R.string.fare_medium_unblock_card
            } else {
                R.string.fare_medium_block_card
            }
        )

        R.id.negativeActionButton.checkHasText(R.string.popup_cancel)

    }

    @Test
    fun test_image() {
        R.id.imageView.checkIsImageViewDrawable(
            if (fareMedium.state == FARE_MEDIUM_BLOCK_STATE) {
                R.drawable.ic_unblock
            } else {
                R.drawable.ic_alert
            }
        )

    }

    @Test
    fun test_message() {
        R.id.textViewContent.checkHasText(
            if (fareMedium.state == FARE_MEDIUM_BLOCK_STATE) expectedStringContentWhenBlocked().toString()
            else expectedStringContentWhenUnBlocked().toString()
        )
    }

    @Test
    fun test_cancel_button_action() {
        R.id.negativeActionButton.performClick()
        assertThat(activityTestRule.activityResult, hasResultCode(Activity.RESULT_CANCELED));
    }

    @Test
    fun test_positive_button_action() {
        R.id.positiveActionButton.performClick()
        R.id.loadingText.checkIsVisible()
        R.id.loadingText.checkHasText(R.string.loading)
        R.id.doneButton.performIdleAction(3000).checkIsVisible()
        R.id.doneButton.checkHasText(R.string.fare_medium_card_action_done)
    }

    @Test
    fun test_no_internet_action(){
        with(UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())){

            if(isAirplaneModeOFF()){
                openQuickSettings()
                clickAbove(UiSelector().textContains("plane"))
                pressRecentApps()
                clickOnTextContains(BuildConfig.DISPLAY_NAME)
            }

            assert(isAirplaneModeOFF())
            clickOnText("UNBLOCK CARD")
            textExists("Connection Error")
            clickOnText("DISMISS")
        }
    }



    @Test
    fun test_update_error() {
        activityTestRule.runOnUiThread {
            blockUnblockCardViewModel.fareMedium = getTestFareMedium().apply {
                uuid = "Something Invalid"
            }
        }
        R.id.positiveActionButton.performClick()
        //R.id.loadingText.checkIsVisible()
        //R.id.loadingText.checkHasText(R.string.loading)
        R.string.change_password_popup_conErrorTitle.checkIfStringIsDisplayed()
    }


    fun expectedStringContentWhenUnBlocked(): SpannableStringBuilder {
        return SpannableStringBuilder().apply {
            appendln(getResourceString(R.string.fare_medium_block_card_message))
            appendln()
            appendln(getResourceString(R.string.fare_medium_block_card_confirm))
            appendln()
            append(blockUnblockCardViewModel.cardDetailFormattedText)
        }
    }

    fun expectedStringContentWhenBlocked(): SpannableStringBuilder {
        return SpannableStringBuilder().apply {
            appendln(getResourceString(R.string.fare_medium_unblock_card_message))
            appendln()
            appendln(getResourceString(R.string.fare_medium_unblock_card_confirm))
            appendln()
            append(blockUnblockCardViewModel.cardDetailFormattedText)
        }
    }


    fun getTestFareMedium() = FareMedium(
        uuid = "106863", name = "MobileAppCard", nickname = "", category = "Adult", type = 220,
        fareMediumId = "00000012423910683321",
        barcodePayload = "abwAAwABAAAAAHd1lDVhaV6QUKRgctWkAAD18cs6EdCtzLs8",
        activeFareMedium = false, printedCardNumber = " 00000000777594356169", state = 2
    )

}