package co.bytemark

import android.content.Intent
import android.text.SpannableStringBuilder
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.extensions.getResourceString
import co.bytemark.extensions.withDrawable
import co.bytemark.helpers.AppConstants
import co.bytemark.manage.remove_physical_card.RemovePhysicalFareMediumActivity
import co.bytemark.manage.remove_physical_card.RemovePhysicalFareMediumFragment
import co.bytemark.manage.remove_physical_card.RemovePhysicalFareMediumViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
open class ManageScreenTest {

    lateinit var removeCardFragment : RemovePhysicalFareMediumFragment
    lateinit var removeViewModel : RemovePhysicalFareMediumViewModel

    //Send Data In Intent
    @get:Rule
    var activityTestRule =
        object  : ActivityTestRule<RemovePhysicalFareMediumActivity>(RemovePhysicalFareMediumActivity::class.java) {
            override fun getActivityIntent(): Intent {
                val intent = Intent()
                intent.putExtra(AppConstants.FARE_MEDIA, getTestFareMedium())
                intent.putExtra(AppConstants.TITLE, "Remove Card")
                return intent
            }
        }

    @Before
    fun setUp() {
        removeCardFragment = activityTestRule.activity.supportFragmentManager.fragments[0] as RemovePhysicalFareMediumFragment
        removeViewModel = removeCardFragment.getViewModel() as RemovePhysicalFareMediumViewModel
    }


    @Test
    fun test_toolbarTitle(){
        onView(withId(R.id.toolbar)).check(matches(hasDescendant(
            withText(getResourceString(R.string.fare_medium_remove_card)))))
    }

    @Test
    fun test_buttonText(){
        onView(withId(R.id.positiveActionButton))
                .check(matches(withText(getResourceString(R.string.fare_medium_remove_card))))

        onView(withId(R.id.negativeActionButton))
                .check(matches(withText(getResourceString(R.string.popup_cancel))))
    }

    @Test
    fun test_image(){
        onView(withId(R.id.imageView)).check(matches(withDrawable(R.drawable.ic_alert)))
    }

    @Test
    fun test_message() {
        onView(withId(R.id.textViewContent)).check(matches(withText(expectedStringContent().toString())))
    }


    fun expectedStringContent() : SpannableStringBuilder {
        return  SpannableStringBuilder().apply {
            appendln(getResourceString(R.string.fare_medium_remove_card_message))
            appendln()
            appendln(getResourceString(R.string.fare_medium_remove_card_confirm))
            appendln()
            append(removeViewModel.cardDetailFormattedText)
        }
    }


    fun getTestFareMedium() = FareMedium(
        uuid = "106863",
        name = "MobileAppCard",
        nickname = "",
        category = "Adult",
        type = 220,
        fareMediumId = "00000000777594356169",
        barcodePayload = "abwAAwABAAAAAHd1lDVhaV6QUKRgctWkAAD18cs6EdCtzLs8",
        activeFareMedium = false,
        printedCardNumber = " 00000000777594356169",
        state = 1
    )
}