package co.bytemark.extensions

import android.graphics.drawable.Drawable
import android.provider.Settings
import android.view.View
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.core.graphics.drawable.toBitmap
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.common.Result
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.Utils
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher


fun <T> Response<T>.toResult(skipServerTimeCheck: Boolean = true): Result<T> {

    fun getErrorIfAny(skipServerTimeCheck: Boolean, response: Response<T>): BMError? {
        if (response.errors.isNotEmpty()) {
            val code: Int = errors.first().code
            val msg: String = errors.first().message ?: ""
            return BMError(code, msg)
        } else if (!skipServerTimeCheck && (response.serverTime == null || !Utils.isServerTimeWithin5MinuteRange(response.serverTime!!))) {
            return BMError(BytemarkSDK.ResponseCode.TIME_INCONSISTENCY_DETECTED, "")
        } else if (data == null) {
            return BMError(BytemarkSDK.ResponseCode.UNEXPECTED_ERROR)
        }
        return null
    }

    return try {
        val result = getErrorIfAny(skipServerTimeCheck, this)?.let {
            Result.Failure<T>(listOf(it))
        } ?: Result.Success(this.data)
        result
    } catch (throwable: Throwable) {
        Result.Failure(listOf(BmErrorHandler().fromThrowable(throwable)))
    }
}

inline fun <reified T> String.toBean() = GSON.fromJson<T>(this)



inline fun <reified T> JsonElement.toBean() = GSON.fromJson<T>(this)

fun Any.toJson() = GSON.toJson(this)

fun JsonElement.toJson() = GSON.toJson(this)

fun getResourceString(id: Int): String? {
    return InstrumentationRegistry.getInstrumentation().targetContext.getString(id)
}

fun getDrawableFromResource(@DrawableRes id: Int): Drawable? {
    return InstrumentationRegistry.getInstrumentation().targetContext.getDrawable(id)
}

object GSON {
    var gson = GsonBuilder().setLenient().create()

    inline fun <reified T> fromJson(json: String): T {
        val type = object : TypeToken<T>() {}.type
        return gson.fromJson(json, type)
    }

    inline fun <reified T> fromJson(jsonElement: JsonElement): T {
        val type = object : TypeToken<T>() {}.type
        return gson.fromJson(jsonElement, type)
    }

    fun toJson(any: Any) = gson.toJson(any)


    fun toJson(jsonElement: JsonElement) = gson.toJson(jsonElement)
}

fun withDrawable(@DrawableRes id: Int) = object : TypeSafeMatcher<View>() {
    override fun describeTo(description: Description) {
        description.appendText("ImageView with drawable same as drawable with id $id")
    }

    override fun matchesSafely(view: View): Boolean {
        val context = view.context
        val expectedBitmap = context.getDrawable(id)!!.toBitmap()

        return view is ImageView && view.drawable.toBitmap().sameAs(expectedBitmap)
    }
}

fun isGone()
        = ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.GONE))

fun isVisible()
        = ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE))

fun isInvisible()
        = ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE))

fun isAirplaneModeOFF(): Boolean {
    return Settings.System.getInt(
        InstrumentationRegistry.getInstrumentation().context.contentResolver, Settings.Global.AIRPLANE_MODE_ON, 0
    ) == 0
}

/* Extension Usage

val object = GSON.fromJson<Student>(objectJson)

val list = GSON.fromJson<List<Student>>(listJson)

val map1 = GSON.fromJson<Map<String, Student>>(mapJson)

val map2 = GSON.fromJson<Map<String, List<Student>>>(objectListMapJson)

*/
