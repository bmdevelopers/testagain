package co.bytemark.extensions

import androidx.annotation.StringRes
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import org.junit.Assert


infix fun UiDevice.clickOnText(text : String){
    findObject(UiSelector().text(text)).click()
}

infix fun UiDevice.clickOnTextStartsWith(text : String){
    findObject(UiSelector().textStartsWith(text)).click()
}

infix fun UiDevice.clickOnTextContains(text : String){
    findObject(UiSelector().textContains(text)).click()
}

infix fun UiDevice.clickAbove(uiSelector: UiSelector){
    val bounds = findObject(uiSelector).bounds
    click(bounds.centerX(), (bounds.centerY()-100))
}

infix fun UiDevice.textExists(text : String) : Boolean{
    return findObject(UiSelector().text(text)).exists()
}

infix fun UiDevice.textExists(@StringRes res : Int) : Boolean{
    return findObject(UiSelector().text(getResourceString(res))).exists()
}

infix fun UiDevice.checkTextExists(@StringRes res : Int) {
    Assert.assertTrue(findObject(UiSelector().text(getResourceString(res))).exists())
}

infix fun UiDevice.checkTextExists(string : String) {
    Assert.assertTrue(findObject(UiSelector().text(string)).exists())
}
