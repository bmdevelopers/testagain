package co.bytemark

import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import androidx.test.uiautomator.Until
import co.bytemark.authentication.AuthenticationActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class SignInUiAnimatorTest {

    val emailId = "saurabh+riptauat20@bytemark.co"
    val password = "Byte@123"

    @get:Rule
    var activityTestRule = ActivityTestRule(AuthenticationActivity::class.java, true, true)

    @Test
    fun test_login() {
        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        device.findObject(UiSelector().textContains("Email")).setText(emailId)
        device.findObject(UiSelector().textContains("Password")).setText(password)
        device.findObject(UiSelector().text("SIGN IN")).click()
        device.wait(Until.hasObject(By.textContains("Welcome")), 10000);
    }
}