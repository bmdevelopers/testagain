package co.bytemark

import android.view.View
import androidx.test.annotation.UiThreadTest
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.rule.ActivityTestRule
import androidx.viewpager.widget.ViewPager
import co.bytemark.domain.model.common.BMError
import co.bytemark.manage.createOrLinkVirtualCard.CreateOrLinkCardActivity
import co.bytemark.manage.createOrLinkVirtualCard.linkExistingCard.LinkExistingCardFragment
import co.bytemark.manage.createOrLinkVirtualCard.linkExistingCard.LinkExistingCardViewModel
import co.bytemark.widgets.util.getViewModel
import kotlinx.android.synthetic.main.activity_tabs.*
import kotlinx.android.synthetic.main.fragment_link_existing_card.*
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@RunWith(AndroidJUnit4::class)
@MediumTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class LinkExistingCardUITest {

    private var linkExistingCardFragment: LinkExistingCardFragment? = null

    private var linkExistingCardViewModel: LinkExistingCardViewModel? = null

    private lateinit var viewPager: ViewPager

    @get:Rule
    var activityRule: ActivityTestRule<CreateOrLinkCardActivity> = ActivityTestRule(CreateOrLinkCardActivity::class.java)

    @Before
    fun initUIAndData() {
        viewPager = activityRule.activity.swipeToggleViewPager
        linkExistingCardFragment = (activityRule.activity.tabPagerAdapter?.instantiateItem(viewPager, 1) as? LinkExistingCardFragment)
        linkExistingCardViewModel = linkExistingCardFragment?.getViewModel(CustomerMobileApp.appComponent.linkExistingCardViewModel)
    }

    @Test
    @UiThreadTest
    fun test1_ScrollRightToLeftViewPager() {
        activityRule.activity.swipeToggleViewPager.arrowScroll(View.FOCUS_RIGHT)
    }


    @Test
    @UiThreadTest
    fun test2_linkExistingCardWithValidData() {
        linkExistingCardFragment?.editTextCardNumber?.setText("123456789")
        linkExistingCardFragment?.editTextCardNickname?.setText("Dummy")
        linkExistingCardFragment?.editTextSecurityCode?.setText("123")
        linkExistingCardFragment?.buttonLinkExistingCard?.performClick()
    }

    @Test
    @UiThreadTest
    fun test3_linkExistingCardWithInvalidData() {
        linkExistingCardFragment?.editTextCardNumber?.setText("123456")
        linkExistingCardFragment?.editTextCardNickname?.setText("Dummy")
        linkExistingCardFragment?.editTextSecurityCode?.setText("1234")
        linkExistingCardFragment?.buttonLinkExistingCard?.performClick()
    }

    @Test
    @UiThreadTest
    fun test4_onError() {
        linkExistingCardViewModel?.errorLiveData?.value = BMError(102, "Unexpected Error")
    }

}