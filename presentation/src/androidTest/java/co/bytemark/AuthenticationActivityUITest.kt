package co.bytemark

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.test.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import co.bytemark.authentication.AuthenticationActivity
import co.bytemark.authentication.account_management.AccountManagementFragment
import co.bytemark.authentication.change_password.ChangePasswordFragment
import co.bytemark.authentication.voucher_code.VoucherCodeFragment
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.model.common.Action
import co.bytemark.sdk.model.menu.MenuItem
import co.bytemark.sdk.model.settings.SettingsList
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
@LargeTest
class AuthenticationActivityUITest {

    private var settingsList: SettingsList? = null

    private var signedInMenuItem: MutableList<MenuItem>? = null

    @get:Rule
    var activityRule = ActivityTestRule(AuthenticationActivity::class.java, false, false)

    @Before
    fun createMenuList() {
        settingsList = Gson().fromJson(getJsonDataFromAsset(InstrumentationRegistry.getTargetContext()), object : TypeToken<SettingsList>() {}.type)
        signedInMenuItem = settingsList?.signedInMenuGroups?.first()?.menuItems
    }

    @Test
    fun test1_openYourProfileScreen() {
        if (signedInMenuItem?.any { menuItem -> menuItem.action.screen == Action.ACCOUNT } == true) {
            val bundle = Bundle()
            bundle.putString(AppConstants.TITLE, "Account")
            bundle.putString(AuthenticationActivity.EXTRA_KEY_DEFAULT_SCREEN, AccountManagementFragment.EXTRA_TAG)
            val intent = Intent()
            intent.putExtras(bundle)
            intent.putExtra(AppConstants.ACTION, Action.ACCOUNT)
            activityRule.launchActivity(intent)
        }
    }

    @Test
    fun test2_openChangePassword() {
        if (signedInMenuItem?.any { menuItem -> menuItem.action.screen == Action.PASSWORD } == true) {
            val bundle = Bundle()
            bundle.putString(AppConstants.TITLE, "Change Password")
            bundle.putString(AuthenticationActivity.EXTRA_KEY_DEFAULT_SCREEN, ChangePasswordFragment.EXTRA_TAG)
            val intent = Intent()
            intent.putExtras(bundle)
            intent.putExtra(AppConstants.ACTION, Action.PASSWORD)
            activityRule.launchActivity(intent)
        }
    }

    @Test
    fun test3_openVoucherCode() {
        if (signedInMenuItem?.any { menuItem -> menuItem.action.screen == Action.VOUCHER_CODE } == true) {
            val bundle = Bundle()
            bundle.putString(AppConstants.TITLE, "Voucher Code")
            bundle.putString(AuthenticationActivity.EXTRA_KEY_DEFAULT_SCREEN, VoucherCodeFragment.EXTRA_TAG)
            val intent = Intent()
            intent.putExtras(bundle)
            intent.putExtra(AppConstants.ACTION, Action.VOUCHER_CODE)
            activityRule.launchActivity(intent)
        }
    }

    @Test
    fun test4_openSignIn() {
        if (signedInMenuItem?.any { menuItem -> menuItem.action.screen != Action.VOUCHER_CODE || menuItem.action.screen != Action.PASSWORD || menuItem.action.screen != Action.ACCOUNT } == true) {
            val bundle = Bundle()
            bundle.putString(AppConstants.TITLE, "Sign In")
            val intent = Intent()
            intent.putExtras(bundle)
            activityRule.launchActivity(intent)
        }
    }

    private fun getJsonDataFromAsset(context: Context, fileName: String = "settings_en.json"): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

}