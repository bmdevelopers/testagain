package co.bytemark;




import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import co.bytemark.add_payment_card.validators.CardNumberValidator;

/**
 * Created by omkaramberkar on 8/31/16.
 */
@RunWith(AndroidJUnit4.class)
public class CardNumberValidatorTest {

    private CardNumberValidator cardNumberValidator;
    private String[] testCards;

    @Before
    public void createTestCards() {

        testCards =  new String[18];

        testCards[0] = "378282246310005";
        testCards[1] = "371449635398431";
        testCards[2] = "378734493671000";
        testCards[3] = "5610591081018250";
        testCards[4] = "30569309025904";
        testCards[5] = "38520000023237";
        testCards[6] = "6011111111111117";
        testCards[7] = "6011000990139424";
        testCards[8] = "3530111333300000";
        testCards[9] = "3566002020360505";
        testCards[10] = "5555555555554444";
        testCards[11] = "5105105105105100";
        testCards[12] = "4111111111111111";
        testCards[13] = "4012888888881881";
        testCards[14] = "4222222222222";
        testCards[15] = "76009244561";
        testCards[16] = "5019717010103742";
        testCards[17] = "6331101999990016";

    }

    @Test
    public void test_card_amex_1() {
        cardNumberValidator = new CardNumberValidator(testCards[0]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[0]));
    }

    @Test
    public void test_card_amex_2() {
        cardNumberValidator = new CardNumberValidator(testCards[1]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[1]));
    }

    @Test
    public void test_card_amex_corporate() {
        cardNumberValidator = new CardNumberValidator(testCards[2]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[2]));
    }

    @Test
    public void test_card_australian_bank() {
        cardNumberValidator = new CardNumberValidator(testCards[3]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[3]));
    }

    @Test
    public void test_card_diners_club_1() {
        cardNumberValidator = new CardNumberValidator(testCards[4]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[4]));
    }

    @Test
    public void test_card_diners_club_2() {
        cardNumberValidator = new CardNumberValidator(testCards[5]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[5]));
    }

    @Test
    public void test_card_discover_1() {
        cardNumberValidator = new CardNumberValidator(testCards[6]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[6]));
    }

    @Test
    public void test_card_discover_2() {
        cardNumberValidator = new CardNumberValidator(testCards[7]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[7]));
    }

    @Test
    public void test_card_jcb_1() {
        cardNumberValidator = new CardNumberValidator(testCards[8]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[8]));
    }

    @Test
    public void test_card_jcb_2() {
        cardNumberValidator = new CardNumberValidator(testCards[9]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[9]));
    }

    @Test
    public void test_card_master_1() {
        cardNumberValidator = new CardNumberValidator(testCards[10]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[10]));
    }

    @Test
    public void test_card_master_2() {
        cardNumberValidator = new CardNumberValidator(testCards[11]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[11]));
    }

    @Test
    public void test_card_visa_1() {
        cardNumberValidator = new CardNumberValidator(testCards[12]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[12]));
    }

    @Test
    public void test_card_visa_2() {
        cardNumberValidator = new CardNumberValidator(testCards[13]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[13]));
    }

    // This is a failing test if true
    @Test
    public void test_card_visa_3() {
        cardNumberValidator = new CardNumberValidator(testCards[14]);
        assertThat(cardNumberValidator.isTextValid(), is(false));
        assertThat(cardNumberValidator.hasFullLengthText(), is(false));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[14]));
    }

    @Test
    public void test_card_dankort_1() {
        cardNumberValidator = new CardNumberValidator(testCards[15]);
        assertThat(cardNumberValidator.isTextValid(), is(false));
        assertThat(cardNumberValidator.hasFullLengthText(), is(false));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[15]));
    }

    @Test
    public void test_card_dankort_2() {
        cardNumberValidator = new CardNumberValidator(testCards[16]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[16]));
    }

    @Test
    public void test_card_solo() {
        cardNumberValidator = new CardNumberValidator(testCards[17]);
        assertThat(cardNumberValidator.isTextValid(), is(true));
        assertThat(cardNumberValidator.hasFullLengthText(), is(true));
        assertThat(cardNumberValidator.getTextValue(), Matchers.is(testCards[17]));
    }
}