package co.bytemark

import androidx.test.annotation.UiThreadTest
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import co.bytemark.domain.interactor.fare_capping.FareCappingResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.extensions.GSON
import co.bytemark.extensions.toResult
import co.bytemark.fare_capping.FareCappingActivity
import co.bytemark.fare_capping.FareCappingFragment
import co.bytemark.fare_capping.FareCappingViewModel
import co.bytemark.widgets.util.getViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class FareCappingTest {

    private lateinit var viewModel: FareCappingViewModel
    private lateinit var fareCappingFragment: FareCappingFragment

    val SUCCESS_EMPTY_RESPONSE_FILE_NAME = "mock_fare_capping_empty_response.json"
    val SUCCESS_NON_EMPTY_RESPONSE_FILE_NAME = "mock_fare_capping_response.json"
    val ERROR_SERVER_ERROR_RESPONSE_FILE_NAME = "mock_fare_capping_error_response.json"

    @get:Rule
    var activityTestRule = ActivityTestRule(FareCappingActivity::class.java)

    @Before
    fun init() {
        fareCappingFragment =
            activityTestRule.activity.supportFragmentManager.fragments.first() as FareCappingFragment
        viewModel =
            fareCappingFragment.getViewModel(CustomerMobileApp.appComponent.fareCappingViewModel)
    }


    @Test
    @UiThreadTest
    fun test_emptyResponse() {
        val emptyResponse = getFareCappingEmptyResponse()
        viewModel.handleResult(emptyResponse.toResult())
    }


    @Test
    @UiThreadTest
    fun test_errorResponse() {
        val errorResponse = getFareCappingErrorResponse()
        viewModel.handleResult(errorResponse.toResult())
    }

    @Test
    @UiThreadTest
    fun test_successResponse() {
        val successResponse = getFareCappingNonEmptyResponse()
        viewModel.handleResult(successResponse.toResult())
    }

    private fun getFareCappingErrorResponse(): Response<FareCappingResponse> {
        return GSON.fromJson(getJsonDataFromAsset(ERROR_SERVER_ERROR_RESPONSE_FILE_NAME)!!)
    }

    private fun getFareCappingNonEmptyResponse(): Response<FareCappingResponse> {
        return GSON.fromJson(getJsonDataFromAsset(SUCCESS_NON_EMPTY_RESPONSE_FILE_NAME)!!)
    }

    private fun getFareCappingEmptyResponse(): Response<FareCappingResponse> {
        return GSON.fromJson(getJsonDataFromAsset(SUCCESS_EMPTY_RESPONSE_FILE_NAME)!!)
    }

    private fun getJsonDataFromAsset(fileName: String = "mock_fare_capping_response.json"): String? {
        val jsonString: String
        try {
            jsonString = InstrumentationRegistry.getInstrumentation().context.assets.open(fileName)
                    .bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

}