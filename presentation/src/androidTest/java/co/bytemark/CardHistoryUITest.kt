package co.bytemark

import android.content.Context
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import co.bytemark.card_history.CardHistoryActivity
import co.bytemark.card_history.CardHistoryFragment
import co.bytemark.card_history.CardHistoryViewModel
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.fare_medium.TransactionsResponse
import co.bytemark.widgets.util.getViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_card_history.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class CardHistoryUITest {

    private lateinit var fragment: CardHistoryFragment

    private var response: TransactionsResponse? = null

    private lateinit var viewModel: CardHistoryViewModel

    @Rule
    var activityRule: ActivityTestRule<CardHistoryActivity> = ActivityTestRule(CardHistoryActivity::class.java)

    @Before
    fun initUIAndData() {
        fragment = activityRule.activity.supportFragmentManager.fragments[0] as CardHistoryFragment
        viewModel = fragment.getViewModel(CustomerMobileApp.appComponent.cardHistoryViewModel)
        response = Gson().fromJson(getJsonDataFromAsset(activityRule.activity), object : TypeToken<TransactionsResponse>() {}.type)
    }

    @Test
    fun testLoadData() {
        activityRule.runOnUiThread {
            viewModel.displayLiveData.value = Display.EmptyState.Loading(R.drawable.available_passes_empty_state, R.string.loading)
        }
    }

    @Test
    fun testSubmitDataToRecyclerView() {
        activityRule.runOnUiThread {
            fragment.loadTransactionsToAdapter(response?.transactions?.toMutableList())
        }
    }

    @Test
    fun testScrollTransactionListToEnd() {
        activityRule.runOnUiThread {
            fragment.recyclerViewTransactions.smoothScrollToPosition(fragment.cardHistoryAdapter.itemCount - 1)
        }
    }

    private fun getJsonDataFromAsset(context: Context, fileName: String = "mock_transactions_response.json"): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

}