package co.bytemark

import android.content.Context
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.notificationSettings.NotificationSettingsResponse
import co.bytemark.notification_settings.NotificationSettingsActivity
import co.bytemark.notification_settings.NotificationSettingsFragment
import co.bytemark.notification_settings.NotificationSettingsViewModel
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.widgets.util.getViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class NotificationSettingsUITest {
    companion object {
        @JvmStatic
        private var response: NotificationSettingsResponse? = null

        @JvmStatic
        private lateinit var fragment: NotificationSettingsFragment
    }

    @Rule
    @JvmField
    var activityTestRule: ActivityTestRule<NotificationSettingsActivity> = ActivityTestRule(NotificationSettingsActivity::class.java)

    private lateinit var viewModel: NotificationSettingsViewModel

    @Before
    fun setUp() {
        fragment = activityTestRule.activity.supportFragmentManager.fragments[0] as NotificationSettingsFragment
        viewModel = fragment.getViewModel(CustomerMobileApp.appComponent.notificationSettingsViewModel)
        response = Gson().fromJson(getJsonDataFromAsset(activityTestRule.activity,"MockNotificationSettings.json"), object : TypeToken<NotificationSettingsResponse>() {}.type)
    }

    @Test
    fun loadData() {
        activityTestRule.runOnUiThread {
            viewModel.notificationSettingsLiveData.value = response?.notificationSettings?.toMutableList()
        }
    }

    @Test
    fun checkOfflineFunctionality() {
        activityTestRule.runOnUiThread {
            fragment.onOffline()
        }
    }

    @Test
    fun showErrorView() {
        activityTestRule.runOnUiThread {
            viewModel.errorLiveData.value = BMError(BytemarkSDK.ResponseCode.NETWORK_NOT_AVAILABLE)
        }
    }

    private fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }
}
