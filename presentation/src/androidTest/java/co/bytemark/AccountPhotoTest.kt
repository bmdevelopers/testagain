package co.bytemark

import android.Manifest
import android.content.Context
import androidx.test.annotation.UiThreadTest
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import co.bytemark.domain.interactor.userphoto.UserPhotoResponse
import co.bytemark.domain.model.userphoto.UserPhoto
import co.bytemark.userphoto.AccountPhotoActivity
import co.bytemark.userphoto.AccountPhotoFragment
import co.bytemark.userphoto.AccountPhotoUploadFragment
import co.bytemark.userphoto.AccountPhotoViewModel
import co.bytemark.widgets.util.getViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class AccountPhotoTest {

    private lateinit var viewModel: AccountPhotoViewModel
    private lateinit var accountPhotoFragment: AccountPhotoFragment
    private lateinit var uploadFragment: AccountPhotoUploadFragment
    private var response: UserPhotoResponse? = null

    @get:Rule
    var activityTestRule = ActivityTestRule(AccountPhotoActivity::class.java)

    @Rule
    var permissionRule: GrantPermissionRule = GrantPermissionRule.grant(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE)

    @Before
    fun setupViewModel() {
        accountPhotoFragment = activityTestRule.activity.supportFragmentManager.fragments[0] as AccountPhotoFragment
        uploadFragment = activityTestRule.activity.supportFragmentManager.fragments[2] as AccountPhotoUploadFragment
        viewModel = accountPhotoFragment.getViewModel(CustomerMobileApp.appComponent.accountPhotoViewModel)
        response = Gson().fromJson(getJsonDataFromAsset(activityTestRule.activity), object : TypeToken<UserPhotoResponse>() {}.type)
    }


    @Test
    @UiThreadTest
    fun test_accountPhotoStatusMissing() {
        viewModel.setUserPhotoLiveData.value = UserPhoto(status = UserPhoto.MISSING)
        onView(withId(R.id.textViewStatus)).check(matches(withText("MISSING")))
        onView(withId(R.id.buttonUploadPhoto)).check(matches(isDisplayed()))
    }

    @Test
    @UiThreadTest
    fun test_accountPhotoStatusPending() {
        response?.userPhoto?.status = UserPhoto.PENDING
        viewModel.setUserPhotoLiveData.value = response?.userPhoto
        onView(withId(R.id.textViewStatus)).check(matches(isDisplayed()))
        onView(withId(R.id.buttonUploadPhoto)).check(matches(not(isDisplayed())))
    }

    @Test
    @UiThreadTest
    fun test_accountPhotoStatusRejected() {
        response?.userPhoto?.status = UserPhoto.REJECTED
        viewModel.setUserPhotoLiveData.value = response?.userPhoto
        onView(withId(R.id.textViewStatus)).check(matches(isDisplayed()))
        onView(withId(R.id.buttonUploadPhoto)).check(matches(not(isDisplayed())))
    }

    @Test
    @UiThreadTest
    fun test_accountPhotoStatusAccepted() {
        response?.userPhoto?.status = UserPhoto.ACCEPTED
        viewModel.setUserPhotoLiveData.value = response?.userPhoto
        onView(withId(R.id.textViewStatus)).check(matches(isDisplayed()))
        onView(withId(R.id.buttonUploadPhoto)).check(matches(not(isDisplayed())))
    }

    private fun getJsonDataFromAsset(context: Context, fileName: String = "mock_user_account.json"): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }


}