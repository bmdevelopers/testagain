package co.bytemark

import android.content.Intent
import android.content.Intent.EXTRA_TITLE
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import co.bytemark.extensions.RecyclerViewMatcher
import co.bytemark.menu.MenuActivity
import co.bytemark.menu.MenuFragment
import co.bytemark.menu.MenuViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MenusUITest {

    private lateinit var menuViewModel: MenuViewModel
    private lateinit var menusFragment: MenuFragment

    @get:Rule
    var activityTestRule =
        object : ActivityTestRule<MenuActivity>(MenuActivity::class.java) {
            override fun getActivityIntent(): Intent {
                val intent = Intent()
                intent.putExtra(EXTRA_TITLE, "More Menu")
                return intent
            }
        }

    @Before
    fun setUp() {
        activityTestRule.runOnUiThread {
            menusFragment =
                activityTestRule.activity.supportFragmentManager.fragments[0] as MenuFragment
            menuViewModel = menusFragment.getViewModel()
        }
    }

    @Test
    fun test_toolbarTitle() {
        onView(withId(R.id.toolbar)).check(
            matches(
                ViewMatchers.hasDescendant(
                    withText("More Menu")
                )
            )
        )
    }

    @Test
    fun test_checkMenusItemsWhenLogIn() {
        menuViewModel.menuListLiveData.value?.forEachIndexed { index, menuGroup ->
            onView(
                RecyclerViewMatcher(R.id.dynamicMenuItemRecyclerView)
                    .atPositionOnView(index, R.id.moreInfoMenuItemDoubleTitle)
            ).check(matches(withText(menuGroup.header)))
            menuGroup.menuItems?.forEachIndexed { indexSubMenu, submenu ->
                onView(
                    RecyclerViewMatcher(R.id.dynamicMenuItemRecyclerView)
                        .atPositionOnView(indexSubMenu, R.id.moreInfoMenuItemDoubleTitle)
                ).check(matches(withText(submenu.title)))
            }
        }
    }

}