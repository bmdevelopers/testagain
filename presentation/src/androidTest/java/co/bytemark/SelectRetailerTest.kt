package co.bytemark

import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import co.bytemark.Utils.FindViewOnWait
import co.bytemark.incomm.SelectRetailerActivity
import co.bytemark.incomm.SelectRetailerFragment
import com.google.android.material.textfield.TextInputLayout
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@RunWith(AndroidJUnit4::class)
@MediumTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class SelectRetailerTest {

    lateinit var fragment: SelectRetailerFragment

    @get:Rule
    var activityTestRule: ActivityTestRule<SelectRetailerActivity> =
        ActivityTestRule(SelectRetailerActivity::class.java)

    @get:Rule
    var permissionRule: GrantPermissionRule =
        GrantPermissionRule.grant(android.Manifest.permission.ACCESS_FINE_LOCATION)

    @Before
    fun initUI() {
        fragment =
            activityTestRule.activity.supportFragmentManager.fragments[0] as SelectRetailerFragment
    }

    @Test
    fun test0_clickRecyclerView() {

        onView(withId(R.id.zipCodeEditTextLayout)).perform(
            clearText(),
            typeText("12345"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasDescendant(withText(""))))
        onView(withId(R.id.searchRetailerButton)).check(matches(isEnabled()))
        onView(withId(R.id.distanceSpinner)).perform(click())
        onData(anything()).atPosition(2).perform(click())
        onView(withId(R.id.distanceSpinner)).check(matches(withSpinnerText(containsString("0.75 mi"))))
        onView(withId(R.id.searchRetailerButton)).perform(click())

        FindViewOnWait().assertOnView(
            withId(R.id.title_empty),
            matches(TextViewValueMatcher("No result found"))
        )

        onView(withId(R.id.zipCodeEditTextLayout)).perform(
            clearText(),
            typeText("12345"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasDescendant(withText(""))))
        onView(withId(R.id.searchRetailerButton)).check(matches(isEnabled()))
        onView(withId(R.id.distanceSpinner)).perform(click())
        onData(anything()).atPosition(8).perform(click())
        onView(withId(R.id.distanceSpinner)).check(matches(withSpinnerText(containsString("7 mi"))))
        onView(withId(R.id.searchRetailerButton)).check(matches(isEnabled()))
        onView(withId(R.id.searchRetailerButton)).perform(click())
        FindViewOnWait().assertOnView(
            withId(R.id.title_loading), matches
                (TextViewValueMatcher("Loading Stores"))
        )
    }

    @Test
    fun test1_checkTextInputError() {
        onView(withId(R.id.zipCodeEditTextLayout)).perform(
            clearText(),
            typeText("1234"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasDescendant(withText("Invalid ZIP code"))))
        onView(withId(R.id.searchRetailerButton)).check(matches(not(isEnabled())))
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasTextInputLayoutHintText("City,State or ZIP Code")))

        onView(withId(R.id.zipCodeEditTextLayout)).perform(
            clearText(), typeText("1234 Ny"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.zipCodeTextInputLayout)).check(
            matches(
                hasDescendant(
                    withText(
                        "ZIP " +
                                "code should be only 5 digits number"
                    )
                )
            )
        )
        onView(withId(R.id.searchRetailerButton)).check(matches(not(isEnabled())))
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasTextInputLayoutHintText("City,State or ZIP Code")))

        onView(withId(R.id.zipCodeEditTextLayout)).perform(
            clearText(), typeText("Ny Ny"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasDescendant(withText("Please enter as City, State"))))
        onView(withId(R.id.searchRetailerButton)).check(matches(not(isEnabled())))
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasTextInputLayoutHintText("City,State or ZIP Code")))

        onView(withId(R.id.zipCodeEditTextLayout)).perform(
            clearText(), typeText(""),
            closeSoftKeyboard()
        )
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasDescendant(withText("Location cannot be empty"))))
        onView(withId(R.id.searchRetailerButton)).check(matches(not(isEnabled())))
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasTextInputLayoutHintText("City,State or ZIP Code")))

        onView(withId(R.id.zipCodeEditTextLayout)).perform(
            clearText(), typeText("12345"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasDescendant(withText(""))))
        onView(withId(R.id.searchRetailerButton)).check(matches(isEnabled()))
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasTextInputLayoutHintText("City,State or ZIP Code")))

        onView(withId(R.id.zipCodeEditTextLayout)).perform(
            clearText(), typeText("Ny,Ny"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasDescendant(withText(""))))
        onView(withId(R.id.searchRetailerButton)).check(matches(isEnabled()))
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasTextInputLayoutHintText("City,State or ZIP Code")))
    }

    @Test
    fun test2_checkLocation() {
        onView(withId(R.id.zipCodeEditTextLayout)).perform(
            clearText(), typeText("1234"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.searchRetailerButton)).check(matches(not(isEnabled())))
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasDescendant(withText("Invalid ZIP code"))))

        onView(withId(R.id.locationImageView)).perform(click())
        onView(withId(R.id.cancelImageView)).isVisible()
        onView(withId(R.id.cancelImageView)).perform(click())
        onView(withId(R.id.locationImageView)).isVisible()
        onView(withId(R.id.zipCodeTextInputLayout)).check(matches(hasDescendant(withText("Invalid ZIP code"))))
        onView(withId(R.id.zipCodeEditTextLayout)).check(matches(withText("1234")))
        onView(withId(R.id.zipCodeEditTextLayout)).perform(
            clearText(), typeText("12345"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.searchRetailerButton)).perform(click())
    }

    private fun ViewInteraction.isGone() = getViewAssertion(Visibility.GONE)

    private fun ViewInteraction.isVisible() = getViewAssertion(Visibility.VISIBLE)

    private fun ViewInteraction.isInvisible() = getViewAssertion(Visibility.INVISIBLE)

    private fun getViewAssertion(visibility: Visibility): ViewAssertion? {
        return matches(withEffectiveVisibility(visibility))
    }

    // It is required if running on real device
//    private fun hasTextInputLayoutErrorText(expectedErrorText: String): Matcher<View?>? {
//        return object : TypeSafeMatcher<View?>() {
//            override fun matchesSafely(view: View?): Boolean {
//                if (view !is TextInputLayout) {
//                    return false
//                }
//                val error = view.error ?: return false
//                val hint = error.toString()
//                return expectedErrorText == hint
//            }
//
//            override fun describeTo(description: Description) {}
//        }
//    }


    fun withChildText(string: String): Matcher<View?>? {
        return object : BoundedMatcher<View?, FrameLayout>(FrameLayout::class.java) {
            override fun matchesSafely(view: FrameLayout): Boolean {
                val child = view.getChildAt(0)
                return if (child != null && child is TextView) {
                    child.text.toString() == string
                } else false
            }

            override fun describeTo(description: Description) {
                description.appendText("with child text: ")
            }
        }
    }

    private fun hasTextInputLayoutHintText(expectedHintText: String): Matcher<View?>? {
        return object : TypeSafeMatcher<View?>() {
            override fun matchesSafely(view: View?): Boolean {
                if (view !is TextInputLayout) {
                    return false
                }
                val hints = view.hint ?: return false
                val hint = hints.toString()
                return expectedHintText == hint
            }

            override fun describeTo(description: Description) {}
        }
    }
}

class TextViewValueMatcher(val expectedText: String) : TypeSafeMatcher<View>() {

    override fun describeTo(description: Description?) {}

    override fun matchesSafely(item: View?): Boolean {
        val textView = item as TextView
        val value = textView.text.toString()
        return expectedText == value
    }
}

