package co.bytemark

import android.content.Context
import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents
import co.bytemark.helpers.AppConstants
import co.bytemark.manage.available_passes.AvailablePassesActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException


@RunWith(AndroidJUnit4::class)
class AvailablePassesUITest {

    @Rule
    @JvmField
    var activityTestRule: ActivityTestRule<AvailablePassesActivity> = object : ActivityTestRule<AvailablePassesActivity>(AvailablePassesActivity::class.java) {
        override fun getActivityIntent(): Intent {
            val targetContext: Context = InstrumentationRegistry.getInstrumentation().targetContext
            val result = Intent(targetContext, AvailablePassesActivity::class.java)
            result.putExtra(AppConstants.FARE_MEDIA, fareMedium)
            return result
        }
    }

    companion object {
        private var fareMedium: FareMedium? = null
        @BeforeClass
        @JvmStatic
        fun setupClass() {
            fareMedium = Gson().fromJson(getJsonDataFromAsset(), object : TypeToken<FareMedium>() {}.type)
            fareMedium?.fareMediumContents = fareMedium?.fares?.let { FareMediumContents(fareMedium?.storedValue.toString(), it, "") }
        }

        private fun getJsonDataFromAsset(): String? {
            val jsonString: String
            try {
                jsonString = InstrumentationRegistry.getInstrumentation().targetContext.assets.open("mock_fare_medium.json").bufferedReader().use { it.readText() }
            } catch (ioException: IOException) {
                ioException.printStackTrace()
                return null
            }
            return jsonString
        }
    }

    @Test
    fun checkTextViewValues() {
        onView(withId(R.id.textViewFareMediaNickname)).check(matches(withText("VC1")))
        onView(withId(R.id.fareTitle)).check(matches(withText("10 Min Pass")))
    }

    @Test
    fun checkViewsVisibility() {
        onView(withId(R.id.recyclerViewFares)).check(matches(isDisplayed()))
        onView(withId(R.id.fareExpiration)).check(matches(isDisplayed()))
    }
}