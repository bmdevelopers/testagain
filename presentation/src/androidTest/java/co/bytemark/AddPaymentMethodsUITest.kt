package co.bytemark

import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import co.bytemark.addPaymentMethods.AddPaymentMethodsActivity
import org.junit.Rule
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class AddPaymentMethodsUITest {

    @get:Rule
    val activityRule = ActivityTestRule(AddPaymentMethodsActivity::class.java)

    @get:Rule
    val intentsTestRule = IntentsTestRule(AddPaymentMethodsActivity::class.java)
/*
    @Test
    fun onClickAddCardCardView_goToAddCardScreen() {
        onView(withId(R.id.cardViewCreditCard)).perform(click())
        Intents.init()
        intended(IntentMatchers.hasComponent(AddPaymentCardActivity::class.qualifiedName))
    }*/

    /* @Test
     fun onClickPayPalCardView_launchPaypalScreen() {
         onView(withId(R.id.cardViewPaypal)).perform(click())
         //Assert.assertEquals("com.braintreepayments.api.BraintreeBrowserSwitchActivity", activityRule.activity.componentName)
         Intents.init()
         intended(IntentMatchers.toPackage("com.braintreepayments.api.BraintreeBrowserSwitchActivity"))
     }*/
}