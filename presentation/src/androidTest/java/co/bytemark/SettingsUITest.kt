package co.bytemark

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import co.bytemark.Utils.FindViewOnWait
import co.bytemark.extensions.RecyclerViewMatcher
import co.bytemark.extensions.getResourceString
import co.bytemark.extensions.performScrollRecyclerToEnd
import co.bytemark.helpers.AppConstants
import co.bytemark.sdk.model.common.Action
import co.bytemark.sdk.model.config.Domain
import co.bytemark.settings.SettingsActivity
import co.bytemark.settings.SettingsFragment
import co.bytemark.settings.SettingsViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SettingsUITest {

    private lateinit var settingViewModel: SettingsViewModel
    private lateinit var settingsFragment: SettingsFragment

    @get:Rule
    var activityTestRule =
        object : ActivityTestRule<SettingsActivity>(SettingsActivity::class.java) {
            override fun getActivityIntent(): Intent {
                val intent = Intent()
                intent.putExtra(AppConstants.TITLE, "Settings")
                return intent
            }
        }

    @Before
    fun setUp() {
        activityTestRule.runOnUiThread {
            settingsFragment =
                activityTestRule.activity.supportFragmentManager.fragments[0] as SettingsFragment
            settingViewModel = settingsFragment.getViewModel()
        }
    }

    @Test
    fun test_toolbarTitle() {
        onView(withId(R.id.toolbar)).check(
            matches(
                ViewMatchers.hasDescendant(
                    withText(settingsFragment.confHelper.getNavigationMenuScreenTitleFromTheConfig(Action.SETTINGS))
                )
            )
        )
    }

    @Test
    fun test_checkSettingsItemsWhenLogIn() {
        settingViewModel.settingListLiveData.value!!.forEachIndexed { index, menuGroup ->
            onView(
                RecyclerViewMatcher(R.id.settingsMenuGroupRecyclerView)
                    .atPositionOnView(index, R.id.settingsMenuItemSingleTitle)
            ).check(matches(withText(menuGroup.header)))
            menuGroup.menuItems?.forEachIndexed { indexSubMenu, submenu ->
                onView(
                    RecyclerViewMatcher(R.id.settingsMenuGroupRecyclerView)
                        .atPositionOnView(indexSubMenu, R.id.settingsMenuItemSingleTitle)
                ).check(matches(withText(submenu.title)))
            }
        }
    }

    @Test
    fun test_checkAppVersionAndName() {
        onView(withId(R.id.settingsMenuGroupRecyclerView)).performScrollRecyclerToEnd()

        val domain: Domain = settingsFragment.confHelper.domain
        val brandingName = if (domain.region != null && domain.region?.contentEquals("EU") == true) {
            getResourceString(R.string.settings_bytetoken)
        } else {
            getResourceString(R.string.settings_bytemark)
        }
        FindViewOnWait().assertOnView(
            withId(R.id.brandingNameSettingsMenu),
            matches(withText(brandingName!!))
        )
        // We can not use string resource here directly because app name is generated at runtime
        FindViewOnWait().assertOnView(
            withId(R.id.appNameSettingsMenu),
            matches(withText("Fargo Team uat Mobile App"))
        )
        FindViewOnWait().assertOnView(
            withId(R.id.appVersionSettingsMenu),
            matches(
                withText(
                    String.format(
                        "V %s",
                        BuildConfig.VERSION_NAME
                    )
                )
            )
        )
    }

}