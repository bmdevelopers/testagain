package co.bytemark

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import co.bytemark.authentication.AuthenticationActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class SignUpUITest {

    @get:Rule
    var activityTestRule = ActivityTestRule(AuthenticationActivity::class.java, true, true)

    @Test
    fun test_validateFormlyOnEmptyValues() {
        onView(withText("Sign up")).check(matches(isDisplayed()))
        onView(withText("Sign up")).perform(click())

        onView(withId(R.id.button)).perform(click())
    }
}