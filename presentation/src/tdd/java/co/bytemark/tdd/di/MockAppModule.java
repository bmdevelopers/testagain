package co.bytemark.tdd.di;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import co.bytemark.helpers.AppConstants;
import dagger.Module;
import dagger.Provides;

/**
 * Created by omkaramberkar on 10/10/16.
 */
@Module
@Singleton
public class MockAppModule {
    private Application application;

    public MockAppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return application;
    }

    @Provides
    @Singleton
    SharedPreferences providesSharedPreferences(Application application) {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    AppConstants providesAppConstants() {
        return AppConstants.getInstance();
    }
}
