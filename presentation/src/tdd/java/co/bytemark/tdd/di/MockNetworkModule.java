package co.bytemark.tdd.di;

import android.app.Application;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import co.bytemark.BuildConfig;
import co.bytemark.CustomerMobileApp;
import co.bytemark.analytics.AnalyticsPlatform;
import co.bytemark.analytics.AnalyticsPlatformAdapter;
import co.bytemark.analytics.AnalyticsPlatformsFactory;
import co.bytemark.helpers.ConfHelper;
import co.bytemark.helpers.GsonFactory;
import co.bytemark.helpers.RxUtils;
import co.bytemark.sdk.network_impl.API;
import co.bytemark.sdk.network_impl.BMNetwork;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Omkar on 8/25/16.
 */
@Module
@Singleton
public class MockNetworkModule {

    private final String baseUrl;

    public MockNetworkModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    @Singleton
    Cache providesOkHttpCache(Application application) {
        int cacheSize = 12 * 1024 * 1024;
        return new Cache(application.getCacheDir(), cacheSize);
    }

    @Provides
    @Singleton
    Gson providesGson() {
        return GsonFactory.getGson();
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient(Cache cache) {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();

        /***
         * ATTENTION: Do not remove this check ever. If been removed, remove the interceptor too.
         *
         * The interceptor should NEVER be enabled while in release mode.
         * It must only be enabled for debugging purpose.
         * Enabling it in release mode might result in logging secure credentials.
         */
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);
        }

        builder.addNetworkInterceptor(new StethoInterceptor())
                .connectTimeout(60 * 1000, TimeUnit.MILLISECONDS)
                .readTimeout(60 * 1000, TimeUnit.MILLISECONDS)
                .cache(cache);

        return builder.build();
    }

    @Provides
        @Singleton
        Retrofit provideRetrofit(OkHttpClient okHttpClient, Gson gson) {
            return new Retrofit.Builder().client(okHttpClient)
                    .baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
    }

    @Provides
    @Singleton
    BMNetwork providesBmNetwork(Application context, API api) {
        return new BMNetwork(context, api);
    }

    @Provides
    @Singleton
    ConfHelper providesConf() {
        return ConfHelper.getInstance();
    }

    @Provides
    @Singleton
    RxUtils providesRxUtils() {
        return RxUtils.getInstance();
    }

    @Provides
    @Singleton
    AnalyticsPlatformAdapter providesAnalyticsHandler(Application application) {
        final List<BuildIdentifier> buildIdentifiers = CustomerMobileApp
                .getConf()
                .getClients()
                .getCustomerMobile()
                .getAndroid()
                .getBuildIdentifiers();

        final List<AnalyticsPlatform> analyticsPlatformList = AnalyticsPlatformsFactory.getAnalyticsPlatforms(buildIdentifiers);
        return new AnalyticsPlatformAdapter(application.getApplicationContext(), analyticsPlatformList);
    }
}
