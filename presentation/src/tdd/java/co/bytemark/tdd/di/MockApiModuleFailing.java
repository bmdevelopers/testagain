package co.bytemark.tdd.di;

import com.google.gson.JsonObject;

import java.util.Map;

import javax.inject.Singleton;

import co.bytemark.sdk.AddPaypalAccountResponse;
import co.bytemark.sdk.DeletePayPalResponse;
import co.bytemark.sdk.network_impl.API;
import co.bytemark.sdk.post_body.PostOrder;
import co.bytemark.sdk.post_body.PostPassActivation;
import co.bytemark.sdk.post_body.PostPassUseBatches;
import co.bytemark.sdk.post_body.PostSearch;
import co.bytemark.sdk.post_body.PutDeviceAppInstallations;
import dagger.Module;
import dagger.Provides;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by omkaramberkar on 8/29/16.
 */
@Module
public class MockApiModuleFailing {

    @Provides
    @Singleton
    API providesAPI() {
        return new API() {
            @Override
            public Observable<Response> postOrder(@Body PostOrder postOrder) {
                return null;
            }

            @Override
            public Observable<Response> postProductSearch(@Body PostSearch postSearch) {
                return null;
            }

            @Override
            public Observable<Response> getUser(@QueryMap Map<String, String> queryParameters) {
                return null;
            }

            @Override
            public Observable<Response> getCards(@QueryMap Map<String, String> queryParameters) {
                return Observable.just(null);
            }

            @Override
            public Observable<Response> deleteCard(@Path("cardUUID") String cardUUID, @QueryMap Map<String, String> queryParameters) {
                return null;
            }

            @Override
            public Observable<Response> postCard(@Body PostCard card) {
                return null;
            }

            @Override
            public Observable<Response> getReceipt(@QueryMap Map<String, String> queryParameters) {
                return null;
            }

            @Override
            public Observable<Response> postReOrder(@Path("receiptUuid") String receiptUuid, @QueryMap Map<String, String> queryParameters) {
                return null;
            }

            @Override
            public Observable<Response> getVisualPassTemplates(@Path("template_uuid") String templateUuid, @Query("oauth_token") String oauth_token) {
                return null;
            }

            @Override
            public Observable<ResponseBody> getAllPasses(@Query("oauth_token") String authToken) {
                return null;
            }

            @Override
            public Observable<Response> postSinglePassActivation(@Path("pass_uuid") String passUuid, @Body PostPassActivation passActivation) {
                return null;
            }

            @Override
            public Observable<Response> postMultiplePassActivation(@Body PostPassActivation passActivation) {
                return null;
            }

            @Override
            public Observable<Response> putDeviceAppInstallation(@Body PutDeviceAppInstallations deviceAppInstallations) {
                return null;
            }

            @Override
            public Observable<PayPalTokenData> getPayPalClientToken(@Query("oauth_token") String oAuthToken) {
                return null;
            }

            @Override
            public Observable<AddPaypalAccountResponse> postPayPalAccount(@Body JsonObject body) {
                return null;
            }

            @Override
            public Observable<DeletePayPalResponse> deletePayPalAccount(@Path("braintree_paypal_payment_method_token") String token, @Query("oauth_token") String oAuthToken) {
                return null;
            }

            @Override
            public Observable<Response> postPassUseBatches(@Body PostPassUseBatches passUseBatches) {
                return null;
            }
        };
    }
}
