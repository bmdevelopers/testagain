package co.bytemark.tdd.di;

import javax.inject.Singleton;

import co.bytemark.di.components.AppComponent;
import dagger.Component;

/**
 * Created by Omkar on 8/25/16.
 */
@Singleton
@Component(modules = {MockAppModule.class, MockNetworkModule.class, MockApiModule.class, MockUiModule.class})
public interface MockAppComponent extends AppComponent {
}
