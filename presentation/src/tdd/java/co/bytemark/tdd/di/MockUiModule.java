package co.bytemark.tdd.di;

import android.app.Application;

import javax.inject.Singleton;

import co.bytemark.base.NavigationDrawerImpl;
import co.bytemark.use_tickets.passview.PassViewFactory;
import dagger.Module;
import dagger.Provides;

/**
 * Created by omkaramberkar on 10/10/16.
 */
@Module
@Singleton
public class MockUiModule {
    @Provides
    @Singleton
    NavigationDrawerImpl providesNavigationDrawer() {
        return new NavigationDrawerImpl();
    }

    @Provides
    @Singleton
    PassViewFactory providePassViewFactory(Application application) {
        return new PassViewFactory(application);
    }
}
