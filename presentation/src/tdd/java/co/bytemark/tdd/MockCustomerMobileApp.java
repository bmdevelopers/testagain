package co.bytemark.tdd;

import com.facebook.stetho.Stetho;
import com.google.gson.Gson;
import com.squareup.leakcanary.LeakCanary;

import co.bytemark.ConfProvider;
import co.bytemark.CustomerMobileApp;
import co.bytemark.helpers.GsonFactory;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

/**
 * Created by Omkar on 8/25/16.
 */
public class MockCustomerMobileApp extends CustomerMobileApp {

    private static final String TAG = MockCustomerMobileApp.class.getSimpleName();
    private Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();
        initDebugTools();

        gson = GsonFactory.getGson();

        Observable.defer(new Func0<Observable<Conf>>() {
            @Override
            public Observable<Conf> call() {
                try {
                    return Observable.just(new ConfProvider(getApplicationContext(), "conf.json").loadDefaultConf(gson));
                } catch (Throwable e) {
                    return Observable.error(e);
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Conf>() {
                    @Override
                    public void onCompleted() {
                        Timber.tag(TAG).e("onCompleted: " );
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.tag(TAG).e("onError: ");
                    }

                    @Override
                    public void onNext(Conf conf) {
                        CustomerMobileApp.setConf(conf);
                    }
                });
    }

    @Override
    protected void initDebugTools() {
        super.initDebugTools();
        LeakCanary.install(this);
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                        .build());
    }
}
