package co.bytemark;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by virajgaikwad on 8/29/16.
 */
public class AnalyticsPlatformAdapterFactoryTest {

    /*@Test
    public void test_flurry_platform_creation() {
        List<BuildIdentifier> buildIdentifiers = createBuildIdentifierList("flurry", "");
        List<AnalyticsPlatform> analyticses = createAnalyticsInterfaces(buildIdentifiers);

        assertTrue("Lists are not same size", analyticses.size() == buildIdentifiers.size());
        String message = "Object is not an instance of " + FlurryAnalyticsPlatform.class.getSimpleName();
        assertTrue(message, analyticses.get(0) instanceof FlurryAnalyticsPlatform);

    }

    @Test
    public void test_fabric_platform_creation() {
        List<BuildIdentifier> buildIdentifiers = createBuildIdentifierList("fabric", "");
        List<AnalyticsPlatform> analyticses = createAnalyticsInterfaces(buildIdentifiers);

        assertTrue("Lists are not same size", analyticses.size() == buildIdentifiers.size());
        String message = "Object is not an instance of " + FabricAnalyticsPlatform.class.getSimpleName();
        assertTrue(message, analyticses.get(0) instanceof FabricAnalyticsPlatform);

    }

    @Test
    public void test_google_analytics_platform_creation() {
        List<BuildIdentifier> buildIdentifiers = createBuildIdentifierList("google-analytics", "");
        List<AnalyticsPlatform> analyticses = createAnalyticsInterfaces(buildIdentifiers);

        assertTrue("Lists are not same size", analyticses.size() == buildIdentifiers.size());
        String message = "Object is not an instance of " + GoogleAnalyticsPlatform.class.getSimpleName();
        assertTrue(message, analyticses.get(0) instanceof GoogleAnalyticsPlatform);

    }*/

    private List<BuildIdentifier> createBuildIdentifierList(String platform, String key) {
        BuildIdentifier buildIdentifier = makeBuildItentifierWithValues(platform, key);

        List<BuildIdentifier> buildIdentifiers = new ArrayList<>();
        buildIdentifiers.add(buildIdentifier);
        return buildIdentifiers;
    }
//
//    private List<AnalyticsPlatform> createAnalyticsInterfaces(List<BuildIdentifier> buildIdentifiers) {
//        List<AnalyticsPlatform> analyticses = AnalyticsPlatformsFactory.getAnalyticsPlatforms(buildIdentifiers);
//        return analyticses;
//    }

    private BuildIdentifier makeBuildItentifierWithValues(String flurry, String key) {
        BuildIdentifier buildIdentifier = new BuildIdentifier();
        buildIdentifier.setService(flurry);
        buildIdentifier.setKey(key);
        return buildIdentifier;
    }
}
