package co.bytemark

import android.util.Log
import co.bytemark.add_payment_card.Helper.CreditCardHelper
import co.bytemark.widgets.util.getDigitsOnly
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test


/** Created by Santosh on 12/04/19.
 */

class CardNumberTest {

    private lateinit var testCards: Array<String>

    @Before
    fun addTestCards() {
        testCards = arrayOf("378282246310005",
                "371449635398431",
                "378734493671000",
                "5610591081018250",
                "30569309025904",
                "38520000023237",
                "6011111111111117",
                "6011000990139424",
                "3530111333300000",
                "3566002020360505",
                "5555555555554444",
                "5105105105105100",
                "4111111111111111",
                "4012888888881881",
                "4222222222222",
                "76009244561",
                "5019717010103742",
                "6331101999990016")
    }

    @Test
    fun test_card_amex_1_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[0]), `is`(true))
    }

    @Test
    fun test_card_amex_2_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[1]), `is`(true))
    }

    @Test
    fun test_card_amex_corporate_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[2]), `is`(true))
    }

    @Test
    fun test_card_australian_bank_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[3]), `is`(true))
    }

    @Test
    fun test_card_diners_club_1_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[4]), `is`(true))
    }

    @Test
    fun test_card_diners_club_2_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[5]), `is`(true))
    }

    @Test
    fun test_card_discover_1_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[6]), `is`(true))
    }

    @Test
    fun test_card_discover_2_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[7]), `is`(true))
    }

    @Test
    fun test_card_jcb_1_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[8]), `is`(true))
    }

    @Test
    fun test_card_jcb_2_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[9]), `is`(true))
    }

    @Test
    fun test_card_master_1_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[10]), `is`(true))
    }

    @Test
    fun test_card_master_2_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[11]), `is`(true))
    }

    @Test
    fun test_card_visa_1_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[12]), `is`(true))
    }

    @Test
    fun test_card_visa_2_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[13]), `is`(true))
    }

    @Test
    fun test_card_visa_3_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[14]), `is`(true))
    }

    @Test
    fun test_card_dankort_1_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[15]), `is`(false))
    }

    @Test
    fun test_card_dankort_2_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[16]), `is`(true))
    }

    @Test
    fun test_card_solo_against_luhn_checksum() {
        assertThat(CreditCardHelper.passesLuhnChecksum(testCards[17]), `is`(true))
    }

    @Test
    fun test_get_date_format_for_length_5() {
        assertNull(CreditCardHelper.getDateFormat(5))
    }

    @Test
    fun test_get_date_format_for_length_4() {
        assertNotNull(CreditCardHelper.getDateFormat(4))
    }

    @Test
    fun test_get_date_format_for_length_6() {
        assertNotNull(CreditCardHelper.getDateFormat(6))
    }

    @Test
    fun test_get_digits_only_from_string_1() {
        assertEquals("41111111111S".getDigitsOnly(), "41111111111")
    }

    @Test
    fun test_get_digits_only_from_string_2() {
        assertEquals("41111%@_111<> 111S".getDigitsOnly(), "41111111111")
    }

    fun test_is_date_valid_for_string(){
        assertEquals(CreditCardHelper, true)
    }
}