package co.bytemark

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.data.FakeTransferPassRepository
import co.bytemark.domain.interactor.passes.TransferPassUseCase
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.sdk.Pass
import co.bytemark.sdk.network_impl.BMNetwork
import co.bytemark.ticket_storage.TicketStorageViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class TicketStorageUnitTest {

    lateinit var viewModel: TicketStorageViewModel

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var repository: FakeTransferPassRepository


    @Before
    fun setupViewModel() {
        val bmNetwork = Mockito.mock(BMNetwork::class.java)
        val application: Application =  Mockito.mock(Application::class.java)
        val analyticsPlatformAdapter = Mockito.mock(AnalyticsPlatformAdapter::class.java)
        val useCase = TransferPassUseCase(FakeTransferPassRepository(), BmErrorHandler(), application)

        viewModel = TicketStorageViewModel(useCase, bmNetwork, analyticsPlatformAdapter)
    }

    @Test
    fun test_loadPassSuccess() {
        viewModel.transferPass(Pass(),0)
        val cloudPasses = viewModel.cloudPassesLiveData.getOrAwaitValue(5)
        val devicePasses = viewModel.devicePassesLiveData.getOrAwaitValue(5)

        MatcherAssert.assertThat(cloudPasses, CoreMatchers.not(CoreMatchers.nullValue()))
        MatcherAssert.assertThat(devicePasses, CoreMatchers.not(CoreMatchers.nullValue()))

    }

    @Test
    fun test_loadPassFailure() {
        repository.shouldReturnError = false
        repository.shouldThrowException = true
        viewModel.transferPass(Pass(),0)

        val bmError = viewModel.errorLiveData.getOrAwaitValue()
        MatcherAssert.assertThat(bmError, CoreMatchers.not(CoreMatchers.nullValue()))
    }
}