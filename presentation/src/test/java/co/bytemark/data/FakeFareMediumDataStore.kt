package co.bytemark.data

import co.bytemark.data.fare_medium.local.FareMediumLocalEntityStore
import co.bytemark.domain.interactor.fare_medium.TransferBalanceRequestData
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents
import kotlinx.coroutines.Deferred
import rx.Observable

class FakeFareMediumDataStore() : FareMediumLocalEntityStore {

    override suspend fun getAutoLoadConfig(): Response<LoadConfig> {
        TODO("Not yet implemented")
    }

    override suspend fun updateFareMediumState(fareMediumUuid: String, newState: Int): Response<Any> {
        TODO("Not yet implemented")
    }

    override fun getFareMediumContents(fareMediumUuid: String): Observable<FareMediumContents> {
        TODO("Not yet implemented")
    }

    override fun transferVirtualCardAsync(fareMediumId: String): Deferred<BMResponse> {
        TODO("Not yet implemented")
    }

    override suspend fun removeFareMedium(fareMediumUuid: String): Response<Any> {
        TODO("Not yet implemented")
    }

    override fun saveFareMediumContents(fareMediumUuid: String, fareMediumContents: FareMediumContents): Observable<FareMediumContents> {
        TODO("Not yet implemented")
    }

    override fun getAllVirtualCardsAsync(): Deferred<BMResponse> {
        TODO("Not yet implemented")
    }

    override suspend fun transferBalance(transferBalanceRequestData: TransferBalanceRequestData): Response<Unit> {
        TODO("Not yet implemented")
    }

    override fun getFareMediums(): Observable<List<FareMedium>> {
        TODO("Not yet implemented")
    }

    override fun saveFareMediums(fareMediumList: MutableList<FareMedium>): Observable<MutableList<FareMedium>> {
        TODO("Not yet implemented")
    }

    override fun deleteFareMedia(fareMediumUuid: String): Observable<Boolean> {
        TODO("Not yet implemented")
    }

    override fun deleteFareMediumContents(fareMediumUuid: String): Observable<Boolean> {
        TODO("Not yet implemented")
    }
}