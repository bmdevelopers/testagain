package co.bytemark.data

import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.repository.Repository
import com.google.gson.Gson

open class FakeRepositoryBase : Repository {

	var shouldReturnError = false
	var shouldThrowException = false
	val gson = Gson()


	override fun register(useCase: UseCase<*, *, *>?) {
		TODO("Not yet implemented")
	}

	override fun unregister(useCase: UseCase<*, *, *>?) {
		TODO("Not yet implemented")
	}

}