package co.bytemark.data

import com.google.gson.Gson

abstract class BaseTestRepository {
    var shouldReturnError = false
    var shouldThrowException = false
    val gson = Gson()
}