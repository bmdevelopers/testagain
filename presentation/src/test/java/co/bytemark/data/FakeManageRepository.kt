package co.bytemark.data

import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.autoload.PostAutoload
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.fare_medium.TransactionsResponse
import co.bytemark.domain.model.init_fare_capping.InitFareCappingData
import co.bytemark.domain.model.manage.*
import co.bytemark.domain.repository.ManageRepository
import rx.Observable

class FakeManageRepository : BaseTestRepository(), ManageRepository {


    override suspend fun linkExistingCardsAsync(linkExistingCard: LinkExistingCard?): Response<AddSmartCard> {
        TODO("Not yet implemented")
    }

    override suspend fun getFareMediaCategoriesAsync(): Response<FareCategoryResponse> {
        TODO("Not yet implemented")
    }

    override suspend fun createVirtualCardAsync(virtualCard: VirtualCard?): Response<Data> {
        TODO("Not yet implemented")
    }

    override suspend fun getTransactionsAsync(fareMediaId: String?, pageIndex: Int?): Response<TransactionsResponse> {
        TODO("Not yet implemented")
    }

    override fun saveAutoload(postAutoload: PostAutoload): Observable<BMResponse> {
        TODO("Not yet implemented")
    }

    override fun updateAutoload(postAutoload: PostAutoload): Observable<BMResponse> {
        TODO("Not yet implemented")
    }

    override fun getAutoload(fareMediaId: String): Observable<BMResponse> {
        TODO("Not yet implemented")
    }

    override fun deleteAutoload(fareMediaId: String): Observable<BMResponse> {
        TODO("Not yet implemented")
    }

    override suspend fun getInitFareCappings(fareMediumId: String): Response<InitFareCappingData> {
        TODO("Not yet implemented")
    }

    override suspend fun getInstitutionList(): Response<InstitutionListData> {
        return when {
            shouldReturnError -> Response(errors = listOf(BMError(1, "Unknown Error")),
                    data = InstitutionListData(listOf()))
            shouldThrowException -> throw Exception("Unknown Exception")
            else -> {
                val data = InstitutionListData(listOf(
                        Institution("1", "Institute 1"),
                        Institution("1", "Institute 1")))
                Response(data = data)

            }
        }
    }

    override suspend fun checkUPassEligibility(fareMediumUuid: String,
                                               uPassEligibilityRequestData: UPassEligibilityRequestData)
            : Response<UPassEligibilityResponseData> {
        return when {
            shouldReturnError -> Response(errors = listOf(BMError(1, "Unknown Error")),
                    data = UPassEligibilityResponseData(""))
            shouldThrowException -> throw Exception("Unknown Exception")
            else -> {
                val data = UPassEligibilityResponseData("Validation success..")
                Response(data = data)
            }
        }
    }

    override fun register(useCase: UseCase<*, *, *>?) {
        TODO("Not yet implemented")
    }

    override fun unregister(useCase: UseCase<*, *, *>?) {
        TODO("Not yet implemented")
    }

}