package co.bytemark.data

import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.store.filter.SearchResponseData
import co.bytemark.domain.repository.StoreFiltersRepository
import co.bytemark.genericType
import co.bytemark.sdk.post_body.PostSearch
import org.intellij.lang.annotations.Language


class FakeStoreFiltersRepository(
		private val filterType: Int
) : FakeRepositoryBase(), StoreFiltersRepository {

	@Language("JSON")
	private val filtersResponse: String =
			if (filterType == 1)
				""
			else
				"{\"results\":[{\"uuid\":\"f5ac32cc-6684-11e9-8042-0a2f32a45c90\",\"type\":\"facet\",\"values\":[{\"uuid\":\"4214d417-6681-11e9-8042-0a2f32a45c90\",\"value\":\"Alabama Southeastern Region\",\"image_url\":null,\"short_description\":null},{\"uuid\":\"6cc3e733-6681-11e9-8042-0a2f32a45c90\",\"value\":\"California\",\"image_url\":null,\"short_description\":null}],\"filter_type\":\"dropdown\",\"display_name\":\"Origin\",\"placeholder_text\":\"Select origin\"},{\"uuid\":\"3f46bb91-6685-11e9-8042-0a2f32a45c90\",\"type\":\"facet\",\"values\":[{\"uuid\":\"4214d417-6681-11e9-8042-0a2f32a45c90\",\"value\":\"Alabama Southeastern Region\",\"image_url\":null,\"short_description\":null},{\"uuid\":\"6cc3e733-6681-11e9-8042-0a2f32a45c90\",\"value\":\"California\",\"image_url\":null,\"short_description\":null},{\"uuid\":\"96f39fab-6681-11e9-8042-0a2f32a45c90\",\"value\":\"Georgia\",\"image_url\":null,\"short_description\":null}],\"filter_type\":\"dropdown\",\"display_name\":\"Destination\",\"placeholder_text\":\"Select destination\"}]}"

	override suspend fun getStoreFilters(postSearch: PostSearch): Response<SearchResponseData> {
		return when {
			shouldReturnError -> Response(errors = listOf(BMError(1, "Unknown Error")),
					data = SearchResponseData(listOf()))
			shouldThrowException -> throw Exception("Unknown Exception")
			else -> {
				val filtersList =
						gson.fromJson<SearchResponseData>(filtersResponse, genericType<SearchResponseData>())
				return Response(data = filtersList)
			}
		}
	}

}
