package co.bytemark.data

import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.interactor.fare_medium.TransferBalanceRequestData
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents
import co.bytemark.domain.repository.FareMediumRepository
import co.bytemark.genericType
import com.google.gson.Gson
import kotlinx.coroutines.Deferred
import rx.Observable

class FakeFareMediumRepository : FareMediumRepository {

    private val cachedFareMediumResponse = "[{\"uuid\":\"19721\",\"name\":\"MobileAppCard\",\"type\":220,\"is_active_faremedium\":false,\"faremedia_id\":\"00000015734512524021\",\"barcode_payload\":\"abwAAwABAAAAFXNFElJAIV7a5nlgvWt5AADaIe3KLk2U2n0W\",\"nickname\":\"My Card\",\"category\":\"Adult\",\"printed_card_number\":\"00000015734512524021\",\"state\":2},{\"uuid\":\"19841\",\"name\":\"MobileAppCard\",\"type\":220,\"is_active_faremedium\":false,\"faremedia_id\":\"00000015546904894021\",\"barcode_payload\":\"abwAAwABAAAAFVRpBIlAIV7rPNZgzcHWAAAtPOWHC40hRI54\",\"nickname\":\"Test Card 1\",\"category\":\"Adult\",\"printed_card_number\":\"00000015546904894021\",\"state\":1}]\n"
    private val loadConfigResponse = "{\"load_config\":{\"load_values\":[500,1000,1500,2000,2500],\"min_wallet_load_value\":500,\"max_wallet_load_value\":25000},\"autoload_config\":{\"autoload_values\":[2000,3000,4000,5000],\"min_wallet_autoload_value\":2000,\"max_wallet_autoload_value\":25000},\"autoload_threshold_config\":{\"autoload_threshold_values\":[500,1000,1500,2000],\"min_autoload_threshold_value\":0,\"max_autoload_threshold_value\":23000},\"currency_code\":\"USD\"}"

    var shouldReturnError = false
    var shouldThrowException = false
    val gson = Gson()


    override fun getFareMediums(): Observable<List<FareMedium>> {
        TODO("Not yet implemented")
    }

    override fun getFareMediumContents(fareMediumUuid: String): Observable<FareMediumContents> {
        TODO("Not yet implemented")
    }

    override fun getAllVirtualCardsAsync(): Deferred<BMResponse> {
        TODO("Not yet implemented")
    }

    override fun transferVirtualCardAsync(fareMediumId: String): Deferred<BMResponse> {
        TODO("Not yet implemented")
    }

    override suspend fun updateFareMediumState(fareMediumUuid: String, newState: Int): Response<Any> {
        TODO("Not yet implemented")
    }

    override suspend fun getAutoLoadConfig(): Response<LoadConfig> {
        return when {
            shouldReturnError -> Response(errors = listOf(BMError(1, "Unknown Error")), data = LoadConfig())
            shouldThrowException -> throw Exception("Unknown Exception")
            else -> {
                val config = gson.fromJson<LoadConfig>(loadConfigResponse, genericType<LoadConfig>())
                return Response(data = config)
            }
        }
    }

    override suspend fun transferBalance(transferBalanceRequestData: TransferBalanceRequestData): Response<Unit> {
        return when {
            shouldReturnError -> Response(errors = listOf(BMError(1, "Unknown Error")), data = Unit)
            shouldThrowException -> throw Exception("Unknown Exception")
            else -> Response(data = Unit)
        }
    }

    override fun register(useCase: UseCase<*, *, *>?) {
        TODO("Not yet implemented")
    }

    override fun unregister(useCase: UseCase<*, *, *>?) {
        TODO("Not yet implemented")
    }

}

