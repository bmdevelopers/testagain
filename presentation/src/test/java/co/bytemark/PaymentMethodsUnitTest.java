package co.bytemark;

/**
 * Created by omkaramberkar on 8/30/16.
 */
public class PaymentMethodsUnitTest {

//    @Rule
//    public MockWebServer mockWebServer = new MockWebServer();
//
//    Gson gson;
//    Retrofit retrofit;
//    API api;
//    TestSubscriber<ResponseEntity> responseTestSubscriber;
//
//    String responseBody = "{\n" +
//            "    \"data\": {\n" +
//            "        \"cards\": [\n" +
//            "            {\n" +
//            "                \"uuid\": \"3101e2f6-56ab-46df-8398-31e3072c61f1\",\n" +
//            "                \"nickname\": \"John\",\n" +
//            "                \"type_id\": 1,\n" +
//            "                \"type_name\": \"Visa\",\n" +
//            "                \"last_four\": \"1111\",\n" +
//            "                \"expiration_date\": \"11/2019\",\n" +
//            "                \"cvv_required\": false\n" +
//            "            }\n" +
//            "        ]\n" +
//            "    },\n" +
//            "    \"server_time\": \"mock_response\"\n" +
//            "}";
//
//    @Before
//    public void setUp() {
//
//        gson = new GsonBuilder()
//                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
//                .create();
//
//        retrofit = new Retrofit.Builder()
//                .baseUrl(mockWebServer.url("").toString())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build();
//
//        api = retrofit.create(API.class);
//
//        responseTestSubscriber = new TestSubscriber<>();
//    }
//
//    @Test
//    public void test_payment_methods_get_request() throws InterruptedException {
//
//        mockWebServer.enqueue(new MockResponse().setBody(responseBody));
//
//        Map<String, String> queryParameters = new HashMap<>();
//        queryParameters.put("oauth_token", "BgmjFBfqnOVmwelgt7zt44XCdTNbwRfu");
//
//        Observable<ResponseEntity> responseObservable = api.getPaymentMethods(queryParameters);
//
//        responseObservable
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(Schedulers.newThread())
//                .subscribe(responseTestSubscriber);
//
//        Thread.sleep(1000);
//
//        responseTestSubscriber.assertNoErrors();
//        responseTestSubscriber.assertCompleted();
//
//        assertThat(responseTestSubscriber.getOnNextEvents().get(0).getServerTime()).isEqualTo("mock_response");
//        assertThat(responseTestSubscriber.getOnNextEvents().get(0).getData().getPaymentMethods().get(0).getUuid()).isEqualTo("3101e2f6-56ab-46df-8398-31e3072c61f1");
//        assertThat(responseTestSubscriber.getOnNextEvents().get(0).getData().getPaymentMethods().get(0).getNickname()).isEqualTo("John");
//        assertThat(responseTestSubscriber.getOnNextEvents().get(0).getData().getPaymentMethods().get(0).getTypeId()).isEqualTo(1);
//        assertThat(responseTestSubscriber.getOnNextEvents().get(0).getData().getPaymentMethods().get(0).getTypeName()).isEqualTo("Visa");
//        assertThat(responseTestSubscriber.getOnNextEvents().get(0).getData().getPaymentMethods().get(0).getLastFour()).isEqualTo("1111");
//        assertThat(responseTestSubscriber.getOnNextEvents().get(0).getData().getPaymentMethods().get(0).getExpirationDate()).isEqualTo("11/2019");
//        assertThat(responseTestSubscriber.getOnNextEvents().get(0).getData().getPaymentMethods().get(0).getCvvRequired()).isEqualTo(false);
//    }
//
//    @After
//    public void after() throws IOException {
//        mockWebServer.shutdown();
//    }
}
