package co.bytemark.store

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import co.bytemark.MainCoroutineRule
import co.bytemark.buy_tickets.filters.StoreFiltersViewModel
import co.bytemark.data.FakeStoreFiltersRepository
import co.bytemark.domain.interactor.newStoreFilters.StoreFilterUseCase
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.getOrAwaitValue
import co.bytemark.sdk.post_body.AppliedFilter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class StoreFilterViewModelTest {

	@ExperimentalCoroutinesApi
	@get:Rule
	var mainCoroutineRule = MainCoroutineRule()

	@get:Rule
	var instantExecutorRule = InstantTaskExecutorRule()

	lateinit var viewModel: StoreFiltersViewModel

	private lateinit var repository: FakeStoreFiltersRepository

	@Before
	fun setupViewModel() {
		repository = FakeStoreFiltersRepository(0)
		val error = BmErrorHandler()

		viewModel = StoreFiltersViewModel(StoreFilterUseCase(repository, error))
	}

	@Test
	fun loadFilters_Success_setFiltersData() {

		viewModel.loadFilters(listOf(), "")

		val data = viewModel.filtersLiveData.getOrAwaitValue(5)

		MatcherAssert.assertThat(data, CoreMatchers.not(CoreMatchers.nullValue()))
//		MatcherAssert.assertThat(data.size, IsEqual(2))

		val loading = viewModel.displayStatus.value
		MatcherAssert.assertThat(loading, IsEqual(StoreFiltersViewModel.DisplayState.NONE))
	}

	@Test
	fun loadFilters_Failure_setErrorData() {
		repository.shouldReturnError = true
		viewModel.loadFilters(listOf(), "")

		val bmError = viewModel.errorLiveData.getOrAwaitValue()
		MatcherAssert.assertThat(bmError, CoreMatchers.not(CoreMatchers.nullValue()))

		val loading = viewModel.displayStatus.value
		MatcherAssert.assertThat(loading, IsEqual(StoreFiltersViewModel.DisplayState.NONE))
	}

	@Test
	fun loadFilters_Failure_with_Exception_setErrorData() {
		repository.shouldReturnError = false
		repository.shouldThrowException = true
		viewModel.loadFilters(listOf(), "")

		val bmError = viewModel.errorLiveData.getOrAwaitValue()
		MatcherAssert.assertThat(bmError, CoreMatchers.not(CoreMatchers.nullValue()))

		val loading = viewModel.displayStatus.value
		MatcherAssert.assertThat(loading, IsEqual(StoreFiltersViewModel.DisplayState.NONE))
	}

	@Test
	fun applyFilter_Success_setFiltersData() {

		viewModel.loadFilters(listOf(AppliedFilter("123456", "7654321")), "")

		val data = viewModel.filtersLiveData.getOrAwaitValue(5)

		MatcherAssert.assertThat(data, CoreMatchers.not(CoreMatchers.nullValue()))
//		MatcherAssert.assertThat(data.size, IsEqual(2))

		val loading = viewModel.displayStatus.value
		MatcherAssert.assertThat(loading, IsEqual(StoreFiltersViewModel.DisplayState.NONE))
	}


}