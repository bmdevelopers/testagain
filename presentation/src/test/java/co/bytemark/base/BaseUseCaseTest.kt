package co.bytemark.base

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.common.Result
import co.bytemark.sdk.BytemarkSDK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsInstanceOf
import org.junit.Test


class FaKeUseCase(private val response: Response<Unit?>) : UseCaseV2<Unit, Unit?>(BmErrorHandler()) {

    override suspend fun execute(requestValues: Unit): Response<Unit?> {
        return response
    }
}

@ExperimentalCoroutinesApi
class BaseUseCaseTest {

    @Test
    fun invoke_withSuccessResponse() = runBlockingTest {
        val result = FaKeUseCase(Response(data = Unit)).invoke(Unit)
        assertThat(result, IsInstanceOf(Result.Success::class.java))
    }


    @Test
    fun invoke_withFailureResponse() = runBlockingTest {
        val result = FaKeUseCase(Response(errors = listOf(
                BMError(1, "Unknown error")), data = null)).invoke(Unit)
        assertThat(result, IsInstanceOf(Result.Failure::class.java))
    }

    @Test
    fun invoke_with_NullData_And_EmptyError_Response() = runBlockingTest {
        val result = FaKeUseCase(Response(data = null)).invoke(Unit)
        assertThat(result, IsInstanceOf(Result.Failure::class.java))

        assertThat((result as Result.Failure).bmError.first().code,
                `is`(BytemarkSDK.ResponseCode.UNEXPECTED_ERROR))
    }

    @Test
    fun invoke_with_Data_And_Error_Response() = runBlockingTest {
        val result = FaKeUseCase(Response(errors = listOf(
                BMError(1, "Unknown error")), data = Unit)).invoke(Unit)
        assertThat(result, IsInstanceOf(Result.Failure::class.java))
    }

}