package co.bytemark;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import co.bytemark.sdk.ActivationRestrictionException;
import co.bytemark.sdk.ActivationRestrictionV2;
import co.bytemark.sdk.PassActivationRestrictionCalculator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PassActivationRestrictionCalculatorTest {

    ArrayList<ActivationRestrictionException> getExceptions() {
        ArrayList<ActivationRestrictionException> exception = new ArrayList<>();
        ActivationRestrictionException activationRestrictionException = new ActivationRestrictionException(
                "0", // uuid
                -1, // start_cutoff_year,
                -1, //start_cutoff_month,
                -1, //start_cutoff_day,
                -1, //start_cutoff_day_of_week,
                11, // start_cutoff_hour,
                0, // start_cutoff_minute,
                -1, // start_cutoff_second,
                -1, // stop_cutoff_year,
                -1, // stop_cutoff_month,
                -1, // stop_cutoff_day,
                -1, // stop_cutoff_day_of_week,
                13, // stop_cutoff_hour,
                0, // stop_cutoff_minute,
                -1, // stop_cutoff_second,
                TimeZone.getTimeZone("America/New_York"), // cutoff_timezone,
                false, // first_activation_only,
                null // ArrayList<ActivationRestrictionException > activationRestrictionExceptions) {
        );
        exception.add(activationRestrictionException);
        return exception;
    }


    final ActivationRestrictionV2 activationRestriction8_16 = new ActivationRestrictionV2(
            "0", // uuid
            -1, // start_cutoff_year,
            -1, //start_cutoff_month,
            -1, //start_cutoff_day,
            -1, //start_cutoff_day_of_week,
            8, // start_cutoff_hour,
            0, // start_cutoff_minute,
            -1, // start_cutoff_second,
            -1, // stop_cutoff_year,
            -1, // stop_cutoff_month,
            -1, // stop_cutoff_day,
            -1, // stop_cutoff_day_of_week,
            16, // stop_cutoff_hour,
            0, // stop_cutoff_minute,
            -1, // stop_cutoff_second,
            TimeZone.getTimeZone("America/New_York"), // cutoff_timezone,
            false, // first_activation_only,
            getExceptions() // ArrayList<ActivationRestrictionException > activationRestrictionExceptions) {
    );


    final ActivationRestrictionV2 activationRestriction8 = new ActivationRestrictionV2(
            "0", // uuid
            -1, // start_cutoff_year,
            -1, //start_cutoff_month,
            -1, //start_cutoff_day,
            -1, //start_cutoff_day_of_week,
            8, // start_cutoff_hour,
            0, // start_cutoff_minute,
            -1, // start_cutoff_second,
            -1, // stop_cutoff_year,
            -1, // stop_cutoff_month,
            -1, // stop_cutoff_day,
            -1, // stop_cutoff_day_of_week,
            -1, // stop_cutoff_hour,
            -1, // stop_cutoff_minute,
            -1, // stop_cutoff_second,
            TimeZone.getTimeZone("America/New_York"), // cutoff_timezone,
            false, // first_activation_only,
            getExceptions() // ArrayList<ActivationRestrictionException > activationRestrictionExceptions) {
    );


    final ActivationRestrictionV2 activationRestriction16 = new ActivationRestrictionV2(
            "0", // uuid
            -1, // start_cutoff_year,
            -1, //start_cutoff_month,
            -1, //start_cutoff_day,
            -1, //start_cutoff_day_of_week,
            -1, // start_cutoff_hour,
            -1, // start_cutoff_minute,
            -1, // start_cutoff_second,
            -1, // stop_cutoff_year,
            -1, // stop_cutoff_month,
            -1, // stop_cutoff_day,
            -1, // stop_cutoff_day_of_week,
            16, // stop_cutoff_hour,
            0, // stop_cutoff_minute,
            -1, // stop_cutoff_second,
            TimeZone.getTimeZone("America/New_York"), // cutoff_timezone,
            false, // first_activation_only,
            getExceptions()
    );

    final Calendar _7amCalendar = new GregorianCalendar(2017, 1, 18, 7, 0);
    final Calendar _9amCalendar = new GregorianCalendar(2017, 1, 18, 9, 0);
    final Calendar _12pmCalendar = new GregorianCalendar(2017, 1, 18, 12, 0);
    final Calendar _20pmCalendar = new GregorianCalendar(2017, 1, 18, 20, 0);


    @Test
    public void test_is_before_start_of_restriction_range() {
        assertFalse(PassActivationRestrictionCalculator.isActivationRestricted(_7amCalendar, activationRestriction8_16));
    }

    @Test
    public void test_is_before_start_of_restriction_start() {
        assertFalse(PassActivationRestrictionCalculator.isActivationRestricted(_7amCalendar, activationRestriction8));
    }

    @Test
    public void test_is_before_start_of_restriction_end() {
        assertTrue(PassActivationRestrictionCalculator.isActivationRestricted(_7amCalendar, activationRestriction16));
    }

    @Test
    public void test_is_during_restriction_range() {
        assertTrue(PassActivationRestrictionCalculator.isActivationRestricted(_9amCalendar, activationRestriction8_16));
    }

    @Test
    public void test_is_during_restriction_start() {
        assertTrue(PassActivationRestrictionCalculator.isActivationRestricted(_9amCalendar, activationRestriction8));
    }

    @Test
    public void test_is_during_restriction_end() {
        assertTrue(PassActivationRestrictionCalculator.isActivationRestricted(_9amCalendar, activationRestriction16));
    }

    @Test
    public void test_is_during_restriction_range_exception() {
        assertFalse(PassActivationRestrictionCalculator.isActivationRestricted(_12pmCalendar, activationRestriction8_16));
    }

    @Test
    public void test_is_during_restriction_start_exception() {
        assertFalse(PassActivationRestrictionCalculator.isActivationRestricted(_12pmCalendar, activationRestriction8));
    }

    @Test
    public void test_is_during_restriction_end_exception() {
        assertFalse(PassActivationRestrictionCalculator.isActivationRestricted(_12pmCalendar, activationRestriction16));
    }

    @Test
    public void test_is_after_restriction_range() {
        assertFalse(PassActivationRestrictionCalculator.isActivationRestricted(_20pmCalendar, activationRestriction8_16));
    }

    @Test
    public void test_is_after_restriction_start() {
        assertTrue(PassActivationRestrictionCalculator.isActivationRestricted(_20pmCalendar, activationRestriction8));
    }

    @Test
    public void test_is_after_restriction_end() {
        assertFalse(PassActivationRestrictionCalculator.isActivationRestricted(_20pmCalendar, activationRestriction16));
    }
}

