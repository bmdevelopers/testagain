package co.bytemark

import co.bytemark.data.util.Utils
import co.bytemark.domain.model.fare_medium.fares.Fare
import org.junit.Test
import org.junit.Assert.*
import org.junit.Before
import java.text.SimpleDateFormat

class AvailablePassesTest {

    private lateinit var fare: Fare
    
    @Before
    fun setupEntity() {
        fare =  Fare("62884","19742","10 Min Pass", "", "1", true, "2020-06-09T03:15:05.000Z")
    }

    @Test
    fun validate_expiration_time() {
        var displayDateFormat = SimpleDateFormat("MM/dd/yyyy")
        assertEquals(displayDateFormat.format(Utils.convertToCalendarFormServer(fare.expiration).time), "06/09/2020")
    }

    @Test
    fun validate_active_flag() {
        assertEquals(fare.isActive, true)
    }
}