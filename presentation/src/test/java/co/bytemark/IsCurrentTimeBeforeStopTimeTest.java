package co.bytemark;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import co.bytemark.independenttestcases.IsCurrentTimeBeforeStopTime;
import co.bytemark.sdk.ActivationRestrictionException;
import co.bytemark.sdk.ActivationRestrictionV2;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IsCurrentTimeBeforeStopTimeTest {

    ArrayList<ActivationRestrictionException> getExceptions() {
        ArrayList<ActivationRestrictionException> exception = new ArrayList<>();
        ActivationRestrictionException activationRestrictionException = new ActivationRestrictionException(
                "0", // uuid
                -1, // start_cutoff_year,
                -1, //start_cutoff_month,
                -1, //start_cutoff_day,
                -1, //start_cutoff_day_of_week,
                11, // start_cutoff_hour,
                0, // start_cutoff_minute,
                -1, // start_cutoff_second,
                -1, // stop_cutoff_year,
                -1, // stop_cutoff_month,
                -1, // stop_cutoff_day,
                -1, // stop_cutoff_day_of_week,
                13, // stop_cutoff_hour,
                0, // stop_cutoff_minute,
                -1, // stop_cutoff_second,
                TimeZone.getTimeZone("America/New_York"), // cutoff_timezone,
                false, // first_activation_only,
                null // ArrayList<ActivationRestrictionException > activationRestrictionExceptions) {
        );
        exception.add(activationRestrictionException);
        return exception;
    }

    ActivationRestrictionV2 activationRestriction16 = new ActivationRestrictionV2(
            "0", // uuid
            -1, // start_cutoff_year,
            -1, //start_cutoff_month,
            -1, //start_cutoff_day,
            -1, //start_cutoff_day_of_week,
            -1, // start_cutoff_hour,
            -1, // start_cutoff_minute,
            -1, // start_cutoff_second,
            -1, // stop_cutoff_year,
            -1, // stop_cutoff_month,
            -1, // stop_cutoff_day,
            -1, // stop_cutoff_day_of_week,
            16, // stop_cutoff_hour,
            0, // stop_cutoff_minute,
            -1, // stop_cutoff_second,
            TimeZone.getTimeZone("America/New_York"), // cutoff_timezone,
            false, // first_activation_only,
            getExceptions()
    );

    Calendar _7amCalendar = new GregorianCalendar(2017, 1, 18, 7, 0);
    Calendar _9amCalendar = new GregorianCalendar(2017, 1, 18, 9, 0);
    Calendar _12pmCalendar = new GregorianCalendar(2017, 1, 18, 12, 0);
    Calendar _20pmCalendar = new GregorianCalendar(2017, 1, 18, 20, 0);


    @Test
    public void test_is_current_time_before_stop_cutoff_7() {
        assertTrue(IsCurrentTimeBeforeStopTime.isCurrentTimeBeforeStopCutoff(_7amCalendar, activationRestriction16));
    }

    @Test
    public void test_is_current_time_before_stop_cutoff_9() {
        assertTrue(IsCurrentTimeBeforeStopTime.isCurrentTimeBeforeStopCutoff(_9amCalendar, activationRestriction16));
    }

    @Test
    public void test_is_current_time_before_stop_cutoff_12() {
        assertTrue(IsCurrentTimeBeforeStopTime.isCurrentTimeBeforeStopCutoff(_12pmCalendar, activationRestriction16));
    }

    @Test
    public void test_is_current_time_before_stop_cutoff_20() {
        assertFalse(IsCurrentTimeBeforeStopTime.isCurrentTimeBeforeStopCutoff(_20pmCalendar, activationRestriction16));
    }

}
