package co.bytemark;


import org.junit.Test;

import java.util.ArrayList;
import java.util.TimeZone;

import co.bytemark.independenttestcases.CheckIfStartAndStopCutoffNotEmpty;
import co.bytemark.sdk.ActivationRestrictionException;
import co.bytemark.sdk.ActivationRestrictionV2;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CheckIfStartAndStopCutoffNotEmptyTest {

    ArrayList<ActivationRestrictionException> getExceptions() {
        ArrayList<ActivationRestrictionException> exception = new ArrayList<>();
        ActivationRestrictionException activationRestrictionException = new ActivationRestrictionException(
                "0", // uuid
                -1, // start_cutoff_year,
                -1, //start_cutoff_month,
                -1, //start_cutoff_day,
                -1, //start_cutoff_day_of_week,
                11, // start_cutoff_hour,
                0, // start_cutoff_minute,
                -1, // start_cutoff_second,
                -1, // stop_cutoff_year,
                -1, // stop_cutoff_month,
                -1, // stop_cutoff_day,
                -1, // stop_cutoff_day_of_week,
                13, // stop_cutoff_hour,
                0, // stop_cutoff_minute,
                -1, // stop_cutoff_second,
                TimeZone.getTimeZone("America/New_York"), // cutoff_timezone,
                false, // first_activation_only,
                null // ArrayList<ActivationRestrictionException > activationRestrictionExceptions) {
        );
        exception.add(activationRestrictionException);
        return exception;
    }


    ActivationRestrictionV2 activationRestriction8_16 = new ActivationRestrictionV2(
            "0", // uuid
            -1, // start_cutoff_year,
            -1, //start_cutoff_month,
            -1, //start_cutoff_day,
            -1, //start_cutoff_day_of_week,
            8, // start_cutoff_hour,
            0, // start_cutoff_minute,
            -1, // start_cutoff_second,
            -1, // stop_cutoff_year,
            -1, // stop_cutoff_month,
            -1, // stop_cutoff_day,
            -1, // stop_cutoff_day_of_week,
            16, // stop_cutoff_hour,
            0, // stop_cutoff_minute,
            -1, // stop_cutoff_second,
            TimeZone.getTimeZone("America/New_York"), // cutoff_timezone,
            false, // first_activation_only,
            getExceptions() // ArrayList<ActivationRestrictionException > activationRestrictionExceptions) {
    );


    ActivationRestrictionV2 activationRestriction8 = new ActivationRestrictionV2(
            "0", // uuid
            -1, // start_cutoff_year,
            -1, //start_cutoff_month,
            -1, //start_cutoff_day,
            -1, //start_cutoff_day_of_week,
            8, // start_cutoff_hour,
            0, // start_cutoff_minute,
            -1, // start_cutoff_second,
            -1, // stop_cutoff_year,
            -1, // stop_cutoff_month,
            -1, // stop_cutoff_day,
            -1, // stop_cutoff_day_of_week,
            -1, // stop_cutoff_hour,
            -1, // stop_cutoff_minute,
            -1, // stop_cutoff_second,
            TimeZone.getTimeZone("America/New_York"), // cutoff_timezone,
            false, // first_activation_only,
            getExceptions() // ArrayList<ActivationRestrictionException > activationRestrictionExceptions) {
    );


    ActivationRestrictionV2 activationRestriction16 = new ActivationRestrictionV2(
            "0", // uuid
            -1, // start_cutoff_year,
            -1, //start_cutoff_month,
            -1, //start_cutoff_day,
            -1, //start_cutoff_day_of_week,
            -1, // start_cutoff_hour,
            -1, // start_cutoff_minute,
            -1, // start_cutoff_second,
            -1, // stop_cutoff_year,
            -1, // stop_cutoff_month,
            -1, // stop_cutoff_day,
            -1, // stop_cutoff_day_of_week,
            16, // stop_cutoff_hour,
            0, // stop_cutoff_minute,
            -1, // stop_cutoff_second,
            TimeZone.getTimeZone("America/New_York"), // cutoff_timezone,
            false, // first_activation_only,
            getExceptions()
    );

    @Test
    public void test_is_start_cutoff_not_empty8_16() {
        assertTrue(CheckIfStartAndStopCutoffNotEmpty.checkIfStartCutoffNotEmpty(activationRestriction8_16));
    }

    @Test
    public void test_is_start_cutoff_not_empty8() {
        assertTrue(CheckIfStartAndStopCutoffNotEmpty.checkIfStartCutoffNotEmpty(activationRestriction8));
    }

    @Test
    public void test_is_start_cutoff_not_empty16() {
        assertFalse(CheckIfStartAndStopCutoffNotEmpty.checkIfStartCutoffNotEmpty(activationRestriction16));
    }

    @Test
    public void test_is_stop_cutoff_not_empty8_16() {
        assertTrue(CheckIfStartAndStopCutoffNotEmpty.checkIfStopCutoffNotEmpty(activationRestriction8_16));
    }

    @Test
    public void test_is_stop_cutoff_not_empty8() {
        assertFalse(CheckIfStartAndStopCutoffNotEmpty.checkIfStopCutoffNotEmpty(activationRestriction8));
    }

    @Test
    public void test_is_stop_cutoff_not_empty16() {
        assertTrue(CheckIfStartAndStopCutoffNotEmpty.checkIfStopCutoffNotEmpty(activationRestriction16));
    }
}
