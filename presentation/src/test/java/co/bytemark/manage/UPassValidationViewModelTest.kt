package co.bytemark.manage

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import co.bytemark.MainCoroutineRule
import co.bytemark.data.FakeManageRepository
import co.bytemark.domain.interactor.manage.GetInstitutionListUseCase
import co.bytemark.domain.interactor.manage.VerifyUPassEligibilityUseCase
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.getOrAwaitValue
import co.bytemark.manage.upass.UPassValidationViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class UPassValidationViewModelTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    lateinit var viewModel: UPassValidationViewModel
    lateinit var repository: FakeManageRepository

    @Before
    fun setup() {
        repository = FakeManageRepository()
        val errorHandler = BmErrorHandler()
        val getInstitutionListUseCase = GetInstitutionListUseCase(repository, errorHandler)
        val verifyUPassEligibilityUseCase = VerifyUPassEligibilityUseCase(repository, errorHandler)

        viewModel = UPassValidationViewModel(
                getInstitutionListUseCase,
                verifyUPassEligibilityUseCase
        )

    }

    @Test
    fun getInstitutionList_Success_Sets_Institution_LiveData() {
        viewModel.getInstitutionList()
        val data = viewModel.institutionList.getOrAwaitValue(5)

        assertThat(data, not(nullValue()))
        assertThat(data.size, IsEqual(2))

        val loading = viewModel.displayState.value
        assertThat(loading, IsEqual(UPassValidationViewModel.DisplayState.NONE))
    }

    @Test
    fun getInstitutionList_Failure_Sets_Error_LiveData() {
        repository.shouldReturnError = true
        viewModel.getInstitutionList()

        val error = viewModel.errorLiveData.getOrAwaitValue(5)

        assertThat(error, not(nullValue()))

        val loading = viewModel.displayState.value
        assertThat(loading, IsEqual(UPassValidationViewModel.DisplayState.NONE))

    }

    @Test
    fun getInstitutionList_ThrowsException_Sets_Error_LiveData() {
        repository.shouldReturnError = false
        repository.shouldThrowException = true
        viewModel.getInstitutionList()

        val error = viewModel.errorLiveData.getOrAwaitValue(5)

        assertThat(error, not(nullValue()))

        val loading = viewModel.displayState.value
        assertThat(loading, IsEqual(UPassValidationViewModel.DisplayState.NONE))

    }

    @Test
    fun verifyEligibility_Success_Sets_VerificationStatus_LiveData() {
        viewModel.verifyEligibility("12345", "123124")

        val data = viewModel.verificationStatus.getOrAwaitValue(5)

        assertThat(data, not(nullValue()))

        val loading = viewModel.displayState.value
        assertThat(loading, IsEqual(UPassValidationViewModel.DisplayState.NONE))
    }

    @Test
    fun verifyEligibility_Failure_Sets_Error_LiveData() {
        repository.shouldReturnError = true
        viewModel.verifyEligibility("12345", "123124")

        val error = viewModel.errorLiveData.getOrAwaitValue(5)

        assertThat(error, not(nullValue()))

        val loading = viewModel.displayState.value
        assertThat(loading, IsEqual(UPassValidationViewModel.DisplayState.NONE))
    }


    @Test
    fun verifyEligibility_ThrowsException_Sets_Error_LiveData() {
        repository.shouldReturnError = false
        repository.shouldThrowException = true
        viewModel.verifyEligibility("12345", "123124")

        val error = viewModel.errorLiveData.getOrAwaitValue(5)

        assertThat(error, not(nullValue()))

        val loading = viewModel.displayState.value
        assertThat(loading, IsEqual(UPassValidationViewModel.DisplayState.NONE))
    }


}