package co.bytemark.faremedium.transfer_balance

import co.bytemark.data.FakeFareMediumRepository
import co.bytemark.domain.interactor.fare_medium.TransferBalanceRequestData
import co.bytemark.domain.interactor.fare_medium.TransferBalanceUseCase
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.domain.model.common.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsInstanceOf
import org.junit.Before
import org.junit.Test

val DUMMY_DATA = TransferBalanceRequestData("12345", "54321", 200)

@ExperimentalCoroutinesApi
class TransferBalanceUseCaseTest {

    lateinit var repository: FakeFareMediumRepository

    @Before
    fun setupRepository() {
        repository = FakeFareMediumRepository()
    }

    @Test
    fun invoke_With_Success_Response() = runBlockingTest {
        val result = TransferBalanceUseCase(repository, BmErrorHandler()).invoke(DUMMY_DATA)
        assertThat(result, IsInstanceOf(Result.Success::class.java))
    }

    @Test
    fun invoke_With_Error_Response() = runBlockingTest {
        repository.shouldReturnError = true
        val result = TransferBalanceUseCase(repository, BmErrorHandler()).invoke(DUMMY_DATA)
        assertThat(result, IsInstanceOf(Result.Failure::class.java))
    }


}