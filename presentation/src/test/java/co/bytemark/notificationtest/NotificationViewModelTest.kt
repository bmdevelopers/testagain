package co.bytemark.notificationtest

import android.app.Application
import android.view.View
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import co.bytemark.MainCoroutineRule
import co.bytemark.R
import co.bytemark.analytics.AnalyticsPlatformAdapter
import co.bytemark.appnotification.NotificationViewModel
import co.bytemark.domain.interactor.notification.GetNotificationsUseCase
import co.bytemark.domain.interactor.notification.MarkNotificationAsReadUseCase
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.domain.model.common.Display
import co.bytemark.domain.model.common.Navigate
import co.bytemark.domain.model.notification.Notification
import co.bytemark.getOrAwaitValue
import co.bytemark.helpers.ConfHelper
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class NotificationViewModelTest {
    lateinit var notificationViewModel: NotificationViewModel

    private lateinit var repository: FakeNotificationRepository

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setupViewModel() {
        repository = FakeNotificationRepository()
        val errorHandler = BmErrorHandler()

        val confHelper = Mockito.mock(ConfHelper::class.java)
        val application = Mockito.mock(Application::class.java)
        Mockito.`when`<Boolean>(confHelper.isAuthenticationNative).thenReturn(true)

        val getNotificationsUseCase = GetNotificationsUseCase(repository, errorHandler)
        val markNotificationAsRead = MarkNotificationAsReadUseCase(repository, errorHandler)

        val analyticsPlatformAdapter = Mockito.mock(AnalyticsPlatformAdapter::class.java)

        notificationViewModel = NotificationViewModel(confHelper, application, getNotificationsUseCase, markNotificationAsRead, analyticsPlatformAdapter)

    }

    @Test
    fun test_mark_notification_success() {
        repository.shouldReturnError = false
        repository.shouldThrowException = true
        notificationViewModel.markNotificationAsRead("fbe121d4-c914-4a08-8ad1-69fcb4f5c287")

        val bmError = notificationViewModel.markNotificationReadLiveData.getOrAwaitValue(5)
        assertThat(bmError, not(nullValue()))
    }

    @Test
    fun test_load_notification() {
        repository.shouldReturnError = false
        repository.shouldThrowException = false
        notificationViewModel.loadNotifications()

        assertEquals(notificationViewModel.displayLiveData.value!!::class, Display.EmptyState.ShowContent()::class)
        assertEquals(notificationViewModel.visibilityStateLiveData.value!!::class, NotificationViewModel.VisibilityState.LoginLayout(View.INVISIBLE)::class)
        assertNotNull(notificationViewModel.notificationLiveData.value)
    }

    @Test
    fun test_load_notification_fail() {
        repository.shouldReturnError = true
        repository.shouldThrowException = false
        notificationViewModel.loadNotifications()

        assertEquals(notificationViewModel.displayLiveData.value!!::class, Display.EmptyState.Error()::class)
    }

    @Test
    fun test_on_swipe_refresh() {
        repository.shouldReturnError = false
        repository.shouldThrowException = false
        notificationViewModel.onSwipeRefresh()

        val data = notificationViewModel.displayLiveData.getOrAwaitValue(5)

        assertEquals(data::class, Display.EmptyState.ShowContent::class)
    }

    @Test
    fun test_on_connection_error() {
        notificationViewModel.onConnectionError()

        val data = Display.EmptyState.Error(
                R.drawable.error_material,
                R.string.network_connectivity_error,
                R.string.network_connectivity_error_message
        )

        val liveData = notificationViewModel.displayLiveData.getOrAwaitValue(5)

        assertEquals(liveData::class, data::class)
    }

    @Test
    fun test_on_sign_in_click() {
        notificationViewModel.onSignInClick()

        val data = notificationViewModel.navigationLiveData.getOrAwaitValue(5)
        assertEquals(data::class, Navigate.FinishActivity::class)
    }

    @Test
    fun test_on_handle_notifications() {
        val notificationList: MutableList<Notification> = mutableListOf()
        notificationList.add(Notification("id",
                "type",
                "title",
                "teaser",
                "link",
                "body",
                true,
                "senderUuid",
                "recipientUuid",
                "timeCreated",
                "timeModified",
                "startDate",
                "endDate",
                true))
        notificationViewModel.handleNotifications(notificationList)
        assertEquals(notificationViewModel.visibilityStateLiveData.value!!::class, NotificationViewModel.VisibilityState.SubscribedLayout::class)
    }

    @Test
    fun test_on_notification_click() {
        val notification: Notification = Notification("id",
                "type",
                "title",
                "teaser",
                "link",
                "",
                true,
                "senderUuid",
                "recipientUuid",
                "timeCreated",
                "timeModified",
                "startDate",
                "endDate",
                true)
        notificationViewModel.onNotificationClick(notification)
        assertNotNull(notificationViewModel.navigationLiveData.value)
    }
}
