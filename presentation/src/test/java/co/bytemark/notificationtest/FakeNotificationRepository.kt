package co.bytemark.notificationtest

import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.notification.Notification
import co.bytemark.domain.model.notification.NotificationResponse
import co.bytemark.domain.repository.NotificationRepository
import co.bytemark.genericType
import com.google.gson.Gson

class FakeNotificationRepository : NotificationRepository {

    val responseString = "{\"errors\":[],\"server_time\":\"2020-09-10T12:59:05.227Z\",\"data\"" +
            ":{\"notifications\":[{\"id\":\"fbe121d4-c914-4a08-8ad1-69fcb4f5c287\"," +
            "\"type\":\"SYSTEM_NOTIFICATION\",\"title\":\"Test notification\",\"teaser\":" +
            "\"Delay information\",\"link\":\"https://www.google.com/\",\"body\":\"\",\"show_in_tiles\"" +
            ":false,\"sender_uuid\":\"2a4fde05-1436-11e9-8042-0a2f32a45c90\",\"recipient_uuid\":\"\"," +
            "\"time_created\":\"2020-09-10T12:56:49.000Z\",\"time_modified\":\"2020-09-10T12:58:32.000Z\"," +
            "\"start_date\":\"2020-09-09T18:30:00.000Z\",\"end_date\":\"2020-09-16T18:30:00.000Z\"}]}}"

    var shouldReturnError = false
    var shouldThrowException = false
    val gson = Gson()

    override suspend fun saveNotifications(notifications: MutableList<Notification>): MutableList<Notification> {
        return when {
            shouldThrowException -> throw Exception("Unknown Exception")
            else -> {
                return notifications
            }
        }
    }

    override suspend fun getNotifications(): Response<NotificationResponse> {
        return when {
            shouldReturnError -> Response(errors = listOf(BMError(1, "Unknown Error")), data = NotificationResponse())
            shouldThrowException -> throw Exception("Unknown Exception")
            else -> {
                val fareMediumList = gson.fromJson<NotificationResponse>(responseString, genericType<NotificationResponse>())
                return Response(data = fareMediumList)
            }
        }
    }

    override suspend fun markNotificationAsRead(notificationId: String): Response<Boolean> {
        return when {
            shouldReturnError -> Response(errors = listOf(BMError(1, "Unknown Error")), data = false)
            shouldThrowException -> throw Exception("Unknown Exception")
            else -> {
                return Response(data = true)
            }
        }
    }

    override fun register(useCase: UseCase<*, *, *>?) {
        TODO("Not yet implemented")
    }

    override fun unregister(useCase: UseCase<*, *, *>?) {
        TODO("Not yet implemented")
    }
}