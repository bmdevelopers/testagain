package co.bytemark

import androidx.test.ext.junit.runners.AndroidJUnit4
import co.bytemark.authentication.signin.binding.SignInBindingAdapter
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SignInUnitTest {

    @Test
    fun test0_testEmailPasswordValidation() {
        val signInBindingAdapter = SignInBindingAdapter()
        signInBindingAdapter.onEmailTextChanged("foo@bytemark.csa")
        signInBindingAdapter.onPasswordTextChanged("foo")
        assertTrue("Login Fields are valid", signInBindingAdapter.isValid())
        assertTrue("Alpha is high", signInBindingAdapter.getAlphaValue() == 1.0f)
        signInBindingAdapter.onEmailTextChanged("foo@bytemark")
        signInBindingAdapter.onPasswordTextChanged("foo")
        assertFalse("Login Fields are not valid", signInBindingAdapter.isValid())
        assertTrue("Alpha is low", signInBindingAdapter.getAlphaValue() == 0.3f)
        signInBindingAdapter.onEmailTextChanged("foo@bytemark.co")
        signInBindingAdapter.onPasswordTextChanged("")
        assertFalse("Login Fields are not valid", signInBindingAdapter.isValid())
        assertTrue("Alpha is low", signInBindingAdapter.getAlphaValue() == 0.3f)
    }

    @Test
    fun test1_testEmailValidation() {
        val signInBindingAdapter = SignInBindingAdapter()
        assertFalse("Email is not valid", signInBindingAdapter.isEmailValid("foo@bytemark"))
        assertTrue("Email is valid", signInBindingAdapter.isEmailValid("foo@bytemark.co"))
    }

    @Test
    fun test2_testPasswordValidation() {
        val signInBindingAdapter = SignInBindingAdapter()
        assertFalse("Form is not valid", signInBindingAdapter.isPasswordValid(""))
        assertTrue("Form is not valid", signInBindingAdapter.isPasswordValid("foo"))
    }
}