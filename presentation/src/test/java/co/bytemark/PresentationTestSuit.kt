package co.bytemark

import co.bytemark.base.BaseUseCaseTest
import co.bytemark.faremedium.transfer_balance.TransferBalanceViewModelTest
import co.bytemark.store.StoreFilterViewModelTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.runner.RunWith
import org.junit.runners.Suite


@ExperimentalCoroutinesApi
@RunWith(Suite::class)
@Suite.SuiteClasses(
		BaseUseCaseTest::class,
		TransferBalanceViewModelTest::class,
		StoreFilterViewModelTest::class
)
class PresentationUnitTestSuite