import domain.models.ClientConfiguration
import script.BuildScript

val client = "CMTACONNECT"
val environment = "uat"
val downloadFromRemote = true

tasks.register("updatePermission") {
    project.exec { commandLine("chmod", "755", "../fastlane/Fastfile") }
}

tasks.register("initProject") {
    BuildScript(ClientConfiguration(client, environment, downloadFromRemote), project)
}

/*DO NOT REMOVE ↓: OTHERWISE THIS SCRIPT WILL NOT EXECUTE*/
tasks.whenTaskAdded {}

