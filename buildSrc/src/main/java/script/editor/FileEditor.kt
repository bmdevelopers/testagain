package script.editor

import androidGradleVersion
import di.Dependency.getGson
import domain.models.Conf
import domain.models.constant.PathConstant
import domain.models.google.GoogleServices
import org.gradle.api.Project
import org.w3c.dom.Document
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import org.xml.sax.SAXException
import utils.replaceLine
import java.io.*
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.parsers.ParserConfigurationException
import javax.xml.transform.OutputKeys
import javax.xml.transform.TransformerException
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult


class FileEditor(project: Project) {

    val manifestPath = "${project.projectDir}/src/main/AndroidManifest.xml"

    fun updateFastLaneFile(appId: String) {
        val googleServiceFile = File(PathConstant.googleServicesDestPath)

        val googleServicesKt = getGson().fromJson(
                googleServiceFile.bufferedReader().use { it.readText() }, GoogleServices::class.java
        )

        val firebaseAppID = googleServicesKt.client.first {
            it.clientInfo?.androidClientInfo?.packageName == appId
        }.clientInfo?.mobilesdkAppId

        File("fastlane/Fastfile").replaceLine(
                lineToModifyStartsWith = "app",
                lineToReplace = "app: \"$firebaseAppID\",",
                tempFile = File("fastlane/FastfileTemp")
        )
    }

    private fun updateGradlePluginVersion(conf: Conf) {

        val gradleVersion =
                conf.clients.customerMobile.android.gradleVersion ?: androidGradleVersion

        File("buildSrc/src/main/java/Dependencies.kt").replaceLine(
                lineToModifyStartsWith = "const val androidGradleVersion",
                lineToReplace = "const val androidGradleVersion = \"$gradleVersion\"",
                tempFile = File("buildSrc/src/main/java/DependenciesTemp.kt")
        )
    }

    private fun updateManifestLocationPermission(conf: Conf) {
        val linesToReplace =
                if (conf.clients.customerMobile.locationEnabled == null || conf.clients.customerMobile.locationEnabled!!) {
                    "<uses-permission android:name=\"android.permission.ACCESS_FINE_LOCATION\" />"
                } else {
                    "<uses-permission android:name=\"android.permission.ACCESS_FINE_LOCATION\" tools:node=\"remove\"/>"
                }

        File(manifestPath).replaceLine(
                lineToModifyContains = "<uses-permission android:name=\"android.permission.ACCESS_FINE_LOCATION\"",
                lineToReplace = linesToReplace,
                tempFile = File("presentation/src/main/AndroidManifestTmp.xml")
        )
    }

    private fun updateManifestSchema(conf: Conf) {
        conf.clients.customerMobile.schemes.let {
            if (!it.isNullOrEmpty()) {
                File(manifestPath).replaceLine(
                        lineToModifyStartsWith = "android:scheme=\"",
                        lineToReplace = "android:scheme=\"" + it.first() + "\" />",
                        tempFile = File("presentation/src/main/AndroidManifestTmp.xml")
                )
            }
        }
    }

    private fun parseManifestFileForUpdate(conf: Conf) {
        try {
            getManifestDocument()?.let { doc ->
                val listOfNodes: NodeList = doc.getElementsByTagName("activity")
                for (i in 0 until listOfNodes.length) {
                    val activityNode: Node = listOfNodes.item(i)
                    if (activityNode.nodeType == Node.ELEMENT_NODE) {
                        val id: String = activityNode.attributes.getNamedItem("android:name").textContent
                        when (id.trim { it <= ' ' }) {
                            ".buy_tickets.BuyTicketsActivity" -> {
                                var node = getMatchingChildNode(activityNode, "intent-filter")
                                if (node == null && conf.organization.isConnectApp.not()) {
                                    // if intent filter is null and the app is not connect app, we want the intent filter
                                    node = createIntentFilterNode(doc)
                                    activityNode.appendChild(node)
                                }
                                if (node != null) {
                                    if (conf.organization.isConnectApp) {
                                        activityNode.removeChild(node)
                                    } else {
                                        // hacon action
                                        removeHaconFilters(node)
                                        if (conf.organization.isConnectApp.not()) {
                                            addHaconFilters(doc, node)
                                        }
                                    }
                                }
                            }
                            ".deeplink.DeeplinkActivity" -> {
                                var node = getMatchingChildNode(activityNode, "intent-filter")
                                if (node == null && conf.organization.isConnectApp.not()) {
                                    // if intent filter is null and the app is not connect app, we want the intent filter
                                    node = createIntentFilterNode(doc)
                                    activityNode.appendChild(node)
                                }
                                if (node != null) {
                                    // Deeplink data
                                    removeDeeplinkConfig(node)
                                    if (conf.organization.deeplinkConfiguration != null && conf.organization.deeplinkConfiguration?.host != null) {
                                        addDeepLinkConfig(doc, node, conf)
                                    }
                                }
                            }
                            ".manage.ManageCardsActivity" -> {
                                if (conf.organization.isConnectApp) {
                                    var node = getMatchingChildNode(activityNode, "intent-filter")
                                    if (node == null) {
                                        // if intent filter not available already, create new and add to activity
                                        node = createIntentFilterNode(doc)
                                        activityNode.appendChild(node)
                                    }
                                    // remove if any actions added already from previous build,
                                    // instead of checking if action already present, removing and adding will be easy
                                    removeHaconFilters(node)
                                    addHaconFilters(doc, node)
                                } else if (activityNode.childNodes.length >= 0) {
                                    // remove the intent Filter, because we don't want the intent filter if it is not connect app
                                    val node = getMatchingChildNode(activityNode, "intent-filter")
                                    if (node != null)
                                        activityNode.removeChild(node)
                                }
                            }
                        }
                    }
                }

                val transformerFactory = TransformerFactory.newInstance()
                transformerFactory.setAttribute("indent-number", 2)
                val transformer = transformerFactory.newTransformer()
                transformer.setOutputProperty(OutputKeys.INDENT, "yes")
                val dSource = DOMSource(doc)
                val result: StreamResult = StreamResult(
                        File(manifestPath)
                )
                transformer.transform(dSource, result)
            }
        } catch (e: ParserConfigurationException) {
            e.printStackTrace()
        } catch (e: SAXException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: TransformerException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getMatchingChildNode(parentNode: Node, childNodeName: String): Node? {
        for (i in 0..parentNode.childNodes.length) {
            val node = parentNode.childNodes.item(i)
            if (node != null && node.nodeType == Node.ELEMENT_NODE
                    && childNodeName.equals(node.nodeName, true))
                return node
        }
        return null
    }

    private fun createIntentFilterNode(doc: Document): Node {
        return doc.createElement("intent-filter")
    }

    private fun addHaconFilters(doc: Document, item: Node) {
        val haconAction = doc.createElement("action")
        haconAction.setAttribute(
                "android:name",
                "de.hafas.android.ACTION_BUY_TICKET"
        )
        item.appendChild(haconAction)

        val viewAction = doc.createElement("action")
        viewAction.setAttribute(
                "android:name",
                "android.intent.action.VIEW"
        )
        item.appendChild(viewAction)

        val category = doc.createElement("category")
        category.setAttribute(
                "android:name",
                "android.intent.category.DEFAULT"
        )
        item.appendChild(category)
    }

    private fun removeHaconFilters(item: Node) {
        for (i in 0 until item.childNodes.length) {
            val childItem: Node? = item.childNodes.item(i)
            if (("action".equals(childItem?.nodeName, ignoreCase = true))
                    && ("de.hafas.android.ACTION_BUY_TICKET".equals
                    (childItem?.attributes?.item(0)?.nodeValue, ignoreCase = true))) {
                item.removeChild(childItem)
            } else if (("action".equals(childItem?.nodeName, ignoreCase = true))
                    && ("android.intent.action.VIEW".equals
                    (childItem?.attributes?.item(0)?.nodeValue, ignoreCase = true))) {
                item.removeChild(childItem)
            } else if (("category".equals(childItem?.nodeName, ignoreCase = true))
                    && ("android.intent.category.DEFAULT".equals
                    (childItem?.attributes?.item(0)?.nodeValue, ignoreCase = true))) {
                item.removeChild(childItem)
            }
        }
    }

    private fun addDeepLinkConfig(doc: Document, item: Node, conf: Conf) {
        val category = doc.createElement("category")
        category.setAttribute(
                "android:name",
                "android.intent.category.BROWSABLE"
        )
        item.appendChild(category)
        val dataHttps = doc.createElement("data")
        dataHttps.setAttribute(
                "android:host",
                conf.organization.deeplinkConfiguration?.host
        )
        dataHttps.setAttribute("android:scheme", "https")
        item.appendChild(dataHttps)

        val dataHttp = doc.createElement("data")
        dataHttp.setAttribute(
                "android:host",
                conf.organization.deeplinkConfiguration?.host
        )
        dataHttp.setAttribute("android:scheme", "http")
        item.appendChild(dataHttp)
    }

    private fun removeDeeplinkConfig(item: Node) {
        for (c in 0 until item.childNodes.length) {
            val childItem: Node? = item.childNodes.item(c)
            if ("category".equals(
                            childItem?.nodeName,
                            ignoreCase = true
                    )
                    && "android.intent.category.BROWSABLE".equals(
                            childItem?.attributes?.item(0)?.nodeValue,
                            ignoreCase = true
                    )
            ) {
                println("remove node ${childItem?.attributes?.item(0)?.nodeValue}")
                item.removeChild(childItem);
            }
            if ("data".equals(
                            childItem?.nodeName,
                            ignoreCase = true
                    )
            ) {
                item.removeChild(childItem);
                item.childNodes
            }
        }
    }

    private fun getManifestDocument(): Document? {
        return DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse(File(manifestPath)
                )?.apply {
                    normalize()
                }
    }


    fun modifyRequiredFiles(conf: Conf) {
        updateManifestSchema(conf)
        parseManifestFileForUpdate(conf)
        updateManifestLocationPermission(conf)
        updateGradlePluginVersion(conf)
    }
}
