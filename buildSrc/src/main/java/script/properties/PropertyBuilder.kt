package script.properties

import domain.models.Conf
import domain.models.local.AppConfiguration
import domain.models.local.ServerConfiguration
import domain.models.organisation.Branding
import domain.models.organisation.Organization
import java.lang.Math.max
import java.lang.Math.min
import java.util.*
import kotlin.collections.ArrayList

class PropertyBuilder {

    lateinit var branding: Branding

    lateinit var serverConfig: ServerConfiguration

    lateinit var appConfiguration: AppConfiguration

    lateinit var organizationConfig: Organization
    lateinit var statusBarColor: String
    lateinit var iconTintColorWithAlpha: String
    var elertEnabled: Boolean = false

    fun build(conf: Conf): PropertyBuilder {
        with(conf) {

            organizationConfig = organization
            branding = organization.branding!!
            serverConfig = ServerConfiguration(
                accountUrl = domain.accountsUrl,
                overtureUrl = domain.overtureUrl,
                redirectUrl = domain.accountsUrl + "oauth/client/callback"

            )
            appConfiguration = AppConfiguration().apply {
                reverseDomain = conf.clients.customerMobile.android.reverseDomain ?: ""
                domainType = conf.domain.type.substring(conf.domain.type.lastIndexOf("-") + 1)
                        .toLowerCase(Locale.ENGLISH)

                if(clients.customerMobile.android.wasOldApp && clients.customerMobile.android.reverseDomain!=null){
                    reverseDomain = clients.customerMobile.android.reverseDomain!!
                    domainType = domain.type.substring(conf.domain.type.lastIndexOf("-") + 1,
                                                       conf.domain.type.lastIndexOf("-") + 2).toUpperCase() +
                            (conf.domain.type.substring(conf.domain.type.lastIndexOf("-") + 2)).toLowerCase()
                } else if(clients.customerMobile.android.reverseDomain!=null){
                    reverseDomain = clients.customerMobile.android.reverseDomain!!
                    domainType = domain.type.substring(conf.domain.type.lastIndexOf("-") + 1,
                                                       conf.domain.type.lastIndexOf("-") + 2).toLowerCase() +
                            (conf.domain.type.substring(conf.domain.type.lastIndexOf("-") + 2)).toLowerCase()
                } else {
                    reverseDomain = clients.customerMobile.reverseDomain!!
                }

                if(clients.customerMobile.android.identifier!=null){
                    identifier = clients.customerMobile.android.identifier!!.toLowerCase().replace("-", ".")
                } else {
                    identifier = clients.customerMobile.identifier?.toLowerCase()!!.replace("-", ".")
                }

                if (domainType.matches(Regex("alpha|uat|staging|Alpha|Uat|Staging"))){
                    appId = "${reverseDomain}.${identifier}.${domainType}"
                } else {
                    appId = "${reverseDomain}.${identifier}"
                }

                clientId = clients.customerMobile.client.clientId
                displayName = organization.displayName

                elertEnabled = organization.isElertSdkEnabled ?: false

                clients.customerMobile.android.buildIdentifiers.forEach {

                    when {
                        it.service!!.contains("google-maps") -> {
                            googleMapsApiKey = it.key ?: ""
                        }

                        it.service!!.contains("google-sign-in") -> {
                            googleMapsApiKey = it.key ?: googleMapsApiKey
                        }
                    }
                }

                supportedLocales = organization.supportedLocales.map {
                    when (it.languageCode) {
                        "en" -> it.languageCode
                        else -> {
                            "${it.languageCode}-r${it.countryCode}"
                        }
                    }
                } as ArrayList<String>

                if(elertEnabled) {
                    supportedLocales.add("es")
                }

                appName = clients.customerMobile.displayName

                if (domainType.matches(Regex("alpha|uat|staging|Alpha|Uat|Staging"))) {
                    appName += " $domainType"
                }

                appName = appName.trim()

                fileProviderAuthority =
                    if (domainType.matches(Regex("alpha|uat|staging|Alpha|Uat|Staging"))) "${reverseDomain}.${identifier}.${domainType}"
                    else "${reverseDomain}.${identifier}"
                if(elertEnabled)
                    fileProviderAuthority += ".provider"

                serverClientId = conf.clients.customerMobile.android.buildIdentifiers.firstOrNull{
                    it.service == "google-sign-in"
                }?.webClientId ?: "667480664410-hesip7svv8rpc2935kk3utplm958bhie.apps.googleusercontent.com"

                setStatusBarColor()
                setIconTintColorWithAlpha()
            }
        }

        return this
    }
    private fun setStatusBarColor() {
        val factor = 0.7
        val color: Int =
            branding.themes?.backgroundTheme?.accentColor?.replaceFirst("#", "")?.toInt(16)!!
        val statusbar = color and -0x1000000 or
                (crimp(((color shr 16 and 0xFF) * factor).toInt()) shl 16) or
                (crimp(((color shr 8 and 0xFF) * factor).toInt()) shl 8) or
                crimp(((color and 0xFF) * factor).toInt())
        statusBarColor = String.format("#%06X", 0xFFFFFF and statusbar)
    }

    private fun setIconTintColorWithAlpha() {
        var stingColor = branding.themes?.headerTheme?.backgroundColor?.replace("#", "")
        iconTintColorWithAlpha = lightenIconColor(stingColor!!, 0.4)
    }

    private fun crimp(c: Int): Int {
        return min(max(c, 0), 255)
    }

    private fun lightenIconColor(hex: String, num: Double): String {
        var rgb = "#"
        var temp: String
        var c: Double
        var cd: Double
        var i = 0
        while (i < 3) {
            c = hex.substring(i * 2, i * 2 + 2).toInt(16).toDouble()
            c = min(max(0.0, c + 255 * num), 255.0)
            cd = c - c.toInt()
            if (cd > 0) {
                c = c.toInt() + 1.toDouble()
            }
            temp = Integer.toHexString(c.toInt())
            if (temp.length < 2) temp += temp
            rgb += temp
            i++
        }
        return rgb
    }
}


