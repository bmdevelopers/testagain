package script.manager

import domain.models.Conf
import domain.models.client.IconsKt
import domain.models.client.SplashScreensKt
import domain.models.constant.PathConstant
import utils.deleteAndLog
import java.io.File

class MipMapManager : ResourceManager {

    private var downloadMap = mutableMapOf<String?, String?>()

    override fun deleteResources() {
        PathConstant.mipMapFiles.map { File(it) }.forEach { it.deleteAndLog() }
    }

    override fun enqueueDownload(conf: Conf): MutableMap<String?, String?> {
        downloadMap.clear()
        downloadSplashScreens(conf.clients.customerMobile.android.splashScreens)
        downloadLauncherIcons(conf.clients.customerMobile.android.icons)
        downloadNotificationIcons(conf.clients.customerMobile.android.notificationIcons)
        return downloadMap
    }

    private fun downloadLauncherIcons(icons: IconsKt) {
        downloadMap[icons.mdpi] = PathConstant.destMdPI
        downloadMap[icons.hdpi] = PathConstant.destHDPI
        downloadMap[icons.xhdpi] = PathConstant.destHDPI
        downloadMap[icons.xxhdpi] = PathConstant.destXXHDPI
        downloadMap[icons.xxxhdpi] = PathConstant.destXXXHDPI
    }

    private fun downloadSplashScreens(splash: SplashScreensKt) {
        downloadMap[splash.mdpi] = PathConstant.splash_mdpi
        downloadMap[splash.hdpi] = PathConstant.splash_hdpi
        downloadMap[splash.xhdpi] = PathConstant.splash_xhdpi
        downloadMap[splash.xxhdpi] = PathConstant.splash_xxhdpi
        downloadMap[splash.xxxhdpi] = PathConstant.splash_xxxhdpi
    }

    private fun downloadNotificationIcons(notificationIcon: IconsKt?) {
        notificationIcon?.let {
            downloadMap[it.mdpi] = PathConstant.notification_mdpi
            downloadMap[it.hdpi] = PathConstant.notification_hdpi
            downloadMap[it.xhdpi] = PathConstant.notification_xhdpi
            downloadMap[it.xxhdpi] = PathConstant.notification_xxhdpi
            downloadMap[it.xxxhdpi] = PathConstant.notification_xxxhdpi
        }
    }

}