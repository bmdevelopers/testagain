package script.manager

import domain.models.Conf
import domain.models.constant.PathConstant
import domain.models.constant.UrlConstant
import utils.deleteFilesFromDirectory
import java.io.File

class ElertsManager : ResourceManager {

    lateinit var urlConstant: UrlConstant

    private var downloadMap = mutableMapOf<String?, String?>()

    override fun deleteResources() {
        deleteFiles()
    }

    override fun enqueueDownload(conf: Conf): MutableMap<String?, String?> {
        downloadMap.clear()
        urlConstant = UrlConstant(conf.clientConfiguration)
        if (conf.organization.isElertSdkEnabled == true) {
            File(PathConstant.elertPackageDestPath).mkdir()
            downloadElertSrcFiles()
        }
        return downloadMap
    }

    private fun downloadElertSrcFiles() {
        PathConstant.elertClassList.forEach {
            downloadMap[urlConstant.elertURL + it] = PathConstant.elertPackageDestPath + it
        }
    }

    private fun deleteFiles() {
        PathConstant.elertClassList.deleteFilesFromDirectory(
            "presentation/src/main/java/co/bytemark/elerts/"
        )
    }
}