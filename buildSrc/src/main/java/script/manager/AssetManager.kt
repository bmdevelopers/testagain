package script.manager

import di.Dependency
import domain.models.Conf
import domain.models.constant.PathConstant
import domain.models.constant.UrlConstant
import domain.models.menu.MenuGroup
import domain.models.menu.MenuItem
import domain.models.menu.Settings
import domain.repository.DownloadTask
import utils.deleteAndLog
import java.io.File
import java.util.*

class AssetManager : ResourceManager {

    lateinit var urlConstant: UrlConstant

    private var downloadMap = mutableMapOf<String?, String?>()

    private var downloadTask = DownloadTask()

    override fun deleteResources() {
        File(PathConstant.assetFolderPath).listFiles()?.filter { f ->
            PathConstant.localizedFilesList.any { f.name.contains(it) } || (f.name.endsWith(".pdf") || (f.name.endsWith(".html")))
        }?.forEach {
            it.deleteAndLog()
        }
    }

    //Setting & Menu Files are required before all other files
    override suspend fun downloadPriorityFiles(conf: Conf) {
        urlConstant = UrlConstant(conf.clientConfiguration)

        downloadTask.downloadAndSave(
            urlConstant.settingsJsonFileUrl,
            PathConstant.assetFileSettingPath()
        )

        downloadTask.downloadAndSave(
            urlConstant.settingsJsonFileUrlUs,
            PathConstant.assetFileSettingPathES()
        )

        downloadTask.downloadAndSave(
            urlConstant.moreMenuJsonFileUrl,
            PathConstant.assetFileMoreMenuPath()
        )

        downloadTask.downloadAndSave(
            urlConstant.moreMenuJsonFileUrlUs,
            PathConstant.assetFileMoreMenuPathES()
        )
    }

    override fun enqueueDownload(conf: Conf): MutableMap<String?, String?> {
        urlConstant = UrlConstant(conf.clientConfiguration)
        conf.organization.supportedLocales.forEach { lang ->
            with(lang) {
                PathConstant.localizedFilesList.forEach {

                    downloadMap[urlConstant.fileDownloadUrlFromLanguageCode(it, languageCode)] =
                        PathConstant.assetFileNameFromLanguageCode(it, languageCode)


                    downloadMap[urlConstant.fileDownloadUrlWithLanguageCountryCode(it, languageCode, countryCode!!)] =
                        PathConstant.assetFileNameFromLanguageCountryCode(it, languageCode, countryCode!!)

                    if (it == PathConstant.baseSettingsListFileName || it == PathConstant.baseMoreInformationFileName) {
                        accumulateMenuResource(it, languageCode, countryCode!!)
                    }

                }
            }
        }
        return downloadMap
    }


    private fun accumulateMenuResource(fileName: String, languageCode: String, countryCode: String) {

        File(PathConstant.assetFileNameFromLanguageCode(fileName, languageCode)).run {

            if (exists()) {

                val settings = Dependency.getGson()
                        .fromJson(bufferedReader().use { it.readText() }, Settings::class.java)

                val group = arrayListOf<MenuGroup>()

                if (!settings.signedOutMenuGroups.isNullOrEmpty()) {
                    group.addAll(settings.signedInMenuGroups)
                }

                if (!settings.signedOutMenuGroups.isNullOrEmpty()) {
                    group.addAll(settings.signedOutMenuGroups)
                }

                if (!settings.menuGroups.isNullOrEmpty()) {
                    group.addAll(settings.menuGroups)
                }
                recursiveResourceSearch(group, languageCode, countryCode)
            }
        }
    }

    private fun recursiveResourceSearch(group: List<MenuGroup>, languageCode: String, countryCode: String) {
        group.forEach { main ->
            main.menuItems.forEach {
                when (it.action.type) {
                    "static-resource" -> {
                        downloadStaticResources(it, languageCode, countryCode)
                    }

                    "display_group" -> {
                        recursiveResourceSearch(it.action.menuGroups, languageCode, countryCode)
                    }
                }

            }
        }
    }

    private fun downloadStaticResources(menu: MenuItem, languageCode: String, countryCode: String) {
        menu.action.link?.let {
            val resourceName =
                it.substringAfterLast("/").toLowerCase(Locale.ENGLISH).substringBeforeLast(".")

            when (val extension = it.substringAfterLast(".")) {
                "pdf", "html" -> {
                    downloadMap[it] =
                        PathConstant.assetFileNameFromLanguageCode(resourceName, languageCode, extension = extension)
                }

                "png", "jpeg", "jpg" -> {
                    downloadMap[it] =
                        PathConstant.drawableFileNameFromLanguageCode(resourceName, languageCode, extension = extension)
                }

                else -> {
                }
            }
        }
    }

}