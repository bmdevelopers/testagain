package script.manager

import domain.models.Conf

interface ResourceManager {
    suspend fun downloadPriorityFiles(conf: Conf){}
    fun deleteResources()
    fun enqueueDownload(conf: Conf): MutableMap<String?, String?>
}