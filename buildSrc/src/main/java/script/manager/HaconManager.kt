package script.manager

import domain.models.Conf
import domain.models.client.IAndroidKt
import domain.models.constant.PathConstant
import domain.models.constant.UrlConstant
import utils.deleteFilesFromDirectory
import java.io.File


class HaconManager : ResourceManager {

    lateinit var urlConstant: UrlConstant

    lateinit var environment: String

    private var downloadMap = mutableMapOf<String?, String?>()

    private fun deleteLibs() {
        PathConstant.hasfasLibList.deleteFilesFromDirectory(PathConstant.libraryPath)
    }

    private fun deleteFiles() {
        PathConstant.hasfasClassList.deleteFilesFromDirectory(
            "presentation/src/main/java/co/bytemark/hacon/"
        )
    }

    override fun deleteResources() {
        deleteLibs()
        deleteFiles()
    }

    override fun enqueueDownload(conf: Conf): MutableMap<String?, String?> {
        downloadMap.clear()
        environment = conf.clientConfiguration.environment
        urlConstant = UrlConstant(conf.clientConfiguration)
        if (conf.clients.customerMobile.haconSdkEnabled) {
            File(PathConstant.haconPackageDestPath).mkdir()
            downloadLibs(conf.clients.customerMobile.android)
            downloadHaconSrcFiles()
        }
        return downloadMap;
    }

    private fun downloadLibs(android: IAndroidKt) {
        downloadMap[android.haconPiwikLibLink] =
            PathConstant.libraryPath + PathConstant.piwikLibPath
        downloadMap[android.hacon_slidinguppanel_lib_link] =
            PathConstant.libraryPath + PathConstant.slidingPanelLibPath
        downloadMap[android.haconLibLink] = getHafasDestPath()
        downloadMap[urlConstant.haconGSONLibUrl] =
            PathConstant.libraryPath + PathConstant.hafasGsonPath
    }

    private fun downloadHaconSrcFiles() {
        PathConstant.hasfasClassList.forEach {
            downloadMap[urlConstant.baseHaconURL + it] = PathConstant.haconPackageDestPath + it
        }
    }

    private fun getHafasDestPath() = if (environment == "store") {
        PathConstant.libraryPath + PathConstant.hafasLibPath
    } else {
        PathConstant.libraryPath + PathConstant.hafasLibDemoPath
    }

}