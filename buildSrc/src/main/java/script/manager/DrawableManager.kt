package script.manager

import domain.models.Conf
import domain.models.constant.ConsoleColors
import domain.models.constant.PathConstant
import domain.models.organisation.ChildOrganizationKt
import domain.models.organisation.LogoImages
import domain.models.organisation.Organization
import java.io.File

class DrawableManager : ResourceManager {

    private var downloadMap = mutableMapOf<String?, String?>()

    override fun deleteResources() {
        File(PathConstant.drawableFolderPath).listFiles()?.filter {
            (it.name.endsWith(".jpg") || it.name.endsWith(".png")) && !it.name.contains("account_photo_sample") && !it.name.contains("route_picker_swap_arrows")
        }?.forEach {

            System.out.println("")
            System.out.println(ConsoleColors.RESET + "Deleting    :::  " + ConsoleColors.RED + it.name + ConsoleColors.RESET);

            if (it.delete()) {
                System.out.println(ConsoleColors.RESET + "Status      :::" + ConsoleColors.BLUE + "  Successful \uD83D\uDE00" + ConsoleColors.RESET);
            } else {
                System.out.println(ConsoleColors.RESET + "Status      :::" + ConsoleColors.RED + "  Skipped \uD83D\uDE10" + ConsoleColors.RESET);
            }
        }
    }

    override fun enqueueDownload(conf: Conf): MutableMap<String?, String?> {
        return downloadDrawableFiles(conf)
    }

    private fun downloadChildOrganisationLogoImages(childOrg: List<ChildOrganizationKt>?) {
        childOrg?.forEach {
            downloadLogoImages(it.branding?.logoImages, it.uuid)
        }
    }

    private fun downloadOrganisationLogoImages(organization: Organization) {
        downloadLogoImages(organization.branding?.logoImages, organization.uuid)
    }

    private fun downloadLogoImages(logoImages: LogoImages?, orgId: String) {
        val orgUUID = orgId.replace("-", "_")
        logoImages?.let {
            downloadMap[it.logoMonochromeMask] = PathConstant.monochromeDrawableFilePath(orgUUID)
            downloadMap[it.logoSquare] = PathConstant.squareDrawableFilePath(orgUUID)
            downloadMap[it.logoWide] = PathConstant.wideDrawableFilePath(orgUUID)
            downloadMap[it.logoWideUseScreen] = PathConstant.wideUseScreenDrawableFilePath(orgUUID)
            downloadMap[it.fareMediumLogo] = PathConstant.fareMediumLogoPath
        }
    }

    private fun downloadDrawableFiles(conf: Conf): MutableMap<String?, String?> {
        downloadOrganisationLogoImages(conf.organization)
        downloadChildOrganisationLogoImages(conf.organization.childOrganizations)
        return downloadMap
    }

}