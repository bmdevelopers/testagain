package script.config

import di.Dependency.getGson
import domain.models.ClientConfiguration
import domain.models.Conf
import domain.models.constant.PathConstant.configPath
import domain.models.constant.UrlConstant
import domain.repository.DownloadTask
import java.io.File

class Configuration {

    private fun parseConfiguration(): Conf {
        val inputFile = File(configPath)
        return getGson().fromJson(
            inputFile.bufferedReader().use { it.readText() }, Conf::class.java
        )
    }

    suspend fun downloadAndGetConfiguration(clientConf: ClientConfiguration): Conf? {
        val urlSet = UrlConstant(clientConf)
        val status = DownloadTask().downloadAndSave(urlSet.confUrl, configPath, true)
        return if (status) {
            parseConfiguration().apply {
                clientConfiguration = clientConf
            }
        } else {
            null
        }
    }

}