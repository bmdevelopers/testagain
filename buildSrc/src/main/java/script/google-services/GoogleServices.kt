package script.`google-services`

import domain.models.ClientConfiguration
import domain.models.constant.PathConstant
import domain.models.constant.UrlConstant
import domain.repository.DownloadTask

class GoogleServices {

    suspend fun downloadFile(client: ClientConfiguration) {
        val urlSet = UrlConstant(client)
        DownloadTask().downloadAndSave(urlSet.googleServicesJsonFileUrl, PathConstant.googleServicesDestPath, true)
    }
}