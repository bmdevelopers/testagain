package script

import domain.models.ClientConfiguration
import domain.models.Conf
import domain.repository.DownloadTask
import kotlinx.coroutines.runBlocking
import org.gradle.api.Project
import script.`google-services`.GoogleServices
import script.config.Configuration
import script.editor.FileEditor
import script.manager.*


class BuildScript(private val clientConf: ClientConfiguration, val project: Project) {

    private var downloadMap = mutableMapOf<String?, String?>()

    private var resourceManager: MutableList<ResourceManager> =
        mutableListOf(DrawableManager(), MipMapManager(), HaconManager(), AssetManager(), ElertsManager())

    init {
        execute()
    }

    private fun execute() {
        if (clientConf.downloadFromRemote) {
            println("ONLINE MODE")
            runBlocking {
                Configuration().downloadAndGetConfiguration(clientConf)?.let {
                    GoogleServices().downloadFile(clientConf)
                    cleanUpTemporaryFiles()
                    downloadPriorityFiles(it)
                    modifySourceFiles(it)
                    enqueueProjectFilesForDownload(it)
                }
            }
            DownloadTask(downloadMap).execute()
        } else {
            println("OFFLINE MODE")
        }
    }

    private suspend fun downloadPriorityFiles(conf: Conf) {
        resourceManager.forEach {
            it.downloadPriorityFiles(conf)
        }
    }

    private fun enqueueProjectFilesForDownload(conf: Conf) {
        resourceManager.forEach {
            downloadMap.putAll(it.enqueueDownload(conf))
        }
    }

    private fun cleanUpTemporaryFiles() {
        resourceManager.forEach {
            it.deleteResources()
        }
    }

    private fun modifySourceFiles(conf: Conf) {
        FileEditor(project).modifyRequiredFiles(conf)
    }

}

