package di

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

object Dependency {

    fun getRetrofit(): Retrofit {
        val baseUrl = "https://s3.amazonaws.com/config.bytemark.co/develop/01"

        return Retrofit.Builder().client(provideOKHttpClient()).baseUrl("${baseUrl}/")
                .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke()).build()

    }

    private fun provideOKHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        builder.readTimeout(20 * 1000, TimeUnit.MILLISECONDS)
        builder.writeTimeout(20 * 1000, TimeUnit.MILLISECONDS)
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        builder.addInterceptor(logging)
        return builder.build()
    }

    fun getGson(): Gson {
        return GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()

    }
}