package domain.repository

import data.AwsApi
import di.Dependency
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import utils.save
import java.io.InterruptedIOException

class DownloadTask(private val downloadMap: MutableMap<String?, String?> = mutableMapOf()) {

    private var retrofit = Dependency.getRetrofit().create(AwsApi::class.java)

    fun execute() {
        GlobalScope.launch {
            val list = mutableListOf<Deferred<Boolean>>()

            downloadMap.forEach { key, value ->
                list.add(async { downloadAndSave(key, value) })
            }

            list.forEach { it.await() }
        }
    }

    suspend fun downloadAndSave(url: String?, destPath: String?, abortIfFails: Boolean = false): Boolean {

        if (url.isNullOrEmpty() || destPath.isNullOrEmpty()) {
            return false
        }

        val response = retrofit.downloadFile(url)
        val status = response.save(destPath, url)

        if (!status && abortIfFails) {
            throw InterruptedIOException("$url Download Failed! Operation Aborted.")
        }

        return status
    }


}