package domain.models.google

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class GoogleServicesClient(
    @SerializedName("client_info") @Expose var clientInfo: ClientInfo? = null
)