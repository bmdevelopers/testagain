package domain.models.google

data class GoogleServices(
    val client: ArrayList<GoogleServicesClient>
)