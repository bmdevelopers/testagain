package domain.models.google

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class ClientInfo(
    @SerializedName("mobilesdk_app_id") @Expose var mobilesdkAppId: String? = null,

    @SerializedName("android_client_info") @Expose var androidClientInfo: AndroidClientInfo? = null
)