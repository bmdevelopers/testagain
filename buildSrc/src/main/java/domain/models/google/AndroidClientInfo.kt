package domain.models.google

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class AndroidClientInfo(
    @SerializedName("package_name") @Expose var packageName: String? = null
)