package domain.models

import domain.models.client.ClientsKt
import domain.models.domain.Domain
import domain.models.organisation.Organization
import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class Conf(
    @SerializedName("domain") @Expose var domain: Domain,

    @SerializedName("organization") @Expose var organization: Organization,

    @SerializedName("clients") @Expose var clients: ClientsKt,

    @Expose(serialize = false) var clientConfiguration: ClientConfiguration
)

