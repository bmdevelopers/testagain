package domain.models

data class ClientConfiguration(
    val client: String, val environment: String, val downloadFromRemote: Boolean
)