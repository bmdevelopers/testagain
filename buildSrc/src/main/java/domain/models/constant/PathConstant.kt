package domain.models.constant

object PathConstant {

    const val project = "presentation"

    //File Name
    const val configFileName = "conf.json"
    const val clientConfigFileName = "clientconf.txt"
    const val googleServicesJsonFile = "google-services.json"

    //logos names
    const val mono_chrome_image = "monochrome"
    const val square_image = "square"
    const val wide_image = "wide"
    const val wide_image_use_screen = "wide_use_screen"

    //Resource Path
    const val mipmapFolder = "${project}/src/main/res/mipmap"
    const val assetFolderPath = "${project}/src/main/assets"
    const val drawableFolderPath = "${project}/src/main/res/drawable"
    const val libraryPath = "${project}/libs/"

    //Hacon Dest paths
    const val googleServicesDestPath = "${project}/$googleServicesJsonFile"
    const val haconPackageDestPath = "${project}/src/main/java/co/bytemark/hacon/"

    //Elert Dest paths
    const val elertPackageDestPath = "${project}/src/main/java/co/bytemark/elerts/"

    //Localised Files
    const val baseNavItemsFileName = "navigation"
    const val baseSettingsListFileName = "settings"
    const val baseMoreInformationFileName = "more_menu"
    const val baseFormlySignInItemsFileName = "formly_sign_in"
    const val baseFormlyForgotPasswordFileName = "formly_forgot_password"
    const val baseFormlyVoucherCodeFileName = "formly_voucher_code"
    const val baseFormlySignUpFileName = "formly_sign_up"
    const val baseFormlyChangePasswordFileName = "formly_change_password"
    const val baseFormlyAccountManagementFileName = "formly_account_management"
    const val baseFareMediumCardDetails = "fare_medium_card_details"
    const val baseScheduleListFileName = "schedules"
    const val baseFormlyAppSupportFileName = "app_support"

    val localizedFilesList = mutableListOf(
        baseNavItemsFileName, baseSettingsListFileName, baseMoreInformationFileName,
        baseFormlySignInItemsFileName, baseFormlyForgotPasswordFileName,
        baseFormlyVoucherCodeFileName, baseFormlySignUpFileName, baseFormlyChangePasswordFileName,
        baseFormlyAccountManagementFileName, baseFareMediumCardDetails, baseScheduleListFileName,
        baseFormlyAppSupportFileName
    )


    //App Icons Path
    const val destMdPI = "${mipmapFolder}-mdpi/app_icon.png"
    const val destHDPI = "${mipmapFolder}-hdpi/app_icon.png"
    const val destXHDPI = "${mipmapFolder}-xhdpi/app_icon.png"
    const val destXXHDPI = "${mipmapFolder}-xxhdpi/app_icon.png"
    const val destXXXHDPI = "${mipmapFolder}-xxxhdpi/app_icon.png"

    //Splash Path
    const val splash_mdpi = "${mipmapFolder}-mdpi/splash_screen.png"
    const val splash_hdpi = "${mipmapFolder}-hdpi/splash_screen.png"
    const val splash_xhdpi = "${mipmapFolder}-xhdpi/splash_screen.png"
    const val splash_xxhdpi = "${mipmapFolder}-xxhdpi/splash_screen.png"
    const val splash_xxxhdpi = "${mipmapFolder}-xxxhdpi/splash_screen.png"

    //Notification Icon Path
    const val notification_mdpi = "${mipmapFolder}-mdpi/notification_icon.png"
    const val notification_hdpi = "${mipmapFolder}-hdpi/notification_icon.png"
    const val notification_xhdpi = "${mipmapFolder}-xhdpi/notification_icon.png"
    const val notification_xxhdpi = "${mipmapFolder}-xxhdpi/notification_icon.png"
    const val notification_xxxhdpi = "${mipmapFolder}-xxxhdpi/notification_icon.png"

    val mipMapFiles = arrayOf(
        destMdPI, destHDPI, destXHDPI, destXXHDPI, destXXXHDPI, splash_mdpi, splash_hdpi,
        splash_xhdpi, splash_xxhdpi, splash_xxxhdpi
    )

    //Config Path
    const val configPath = "$assetFolderPath/$configFileName"

    //Hacon File Names
    const val hafasClassActivity = "HaconActivity.java"
    const val hafasClassMenu = "Menu.java"
    const val hafasClassMenuProvider = "MenuProvider.java"

    const val hafasLibPath = "hafas-library.aar"
    const val hafasLibDemoPath = "hafas-library-demo.aar"
    const val piwikLibPath = "piwik.aar"
    const val hafasGsonPath = "google-gson-2.1.0.jar"
    const val slidingPanelLibPath = "hafas-slidinguppanel.aar"

    //Elert File Names
    const val elertClassActivity = "ElertsReportActivity.kt"
    const val elertClassUtil = "ElertsUtil.kt"

    val hasfasClassList = arrayOf(
        hafasClassActivity, hafasClassMenu, hafasClassMenuProvider
    )

    val hasfasLibList = arrayOf(
        hafasLibPath, hafasLibDemoPath, piwikLibPath, slidingPanelLibPath, hafasGsonPath
    )

    val elertClassList = arrayOf(
        elertClassActivity, elertClassUtil
    )

    //Asset Paths
    fun assetFileNameFromLanguageCode(
        fileName: String, languageCode: String, extension: String = "json"
    ) = "${assetFolderPath}/${fileName}_${languageCode}.${extension}"

    fun assetFileNameFromLanguageCountryCode(
        fileName: String, languageCode: String, countryCode: String, extension: String = "json"
    ): String {
        return "${assetFolderPath}/${fileName}_${languageCode}-${countryCode}.${extension}"
    }

    fun assetFileMoreMenuPath(): String {
        return "${assetFolderPath}/${baseMoreInformationFileName}_en.json"
    }

    fun assetFileMoreMenuPathES(): String {
        return "${assetFolderPath}/${baseMoreInformationFileName}_es.json"
    }

    fun assetFileSettingPath(): String {
        return "${assetFolderPath}/${baseSettingsListFileName}_en.json"
    }

    fun assetFileSettingPathES(): String {
        return "${assetFolderPath}/${baseSettingsListFileName}_es.json"
    }

    //Drawable
    fun drawableFileNameFromLanguageCode(
        fileName: String, languageCode: String, extension: String = "json"
    ) = "${drawableFolderPath}/${fileName}_${languageCode}.${extension}"


    //Drawable - Monochrome Image Destination Path
    fun monochromeDrawableFilePath(orgUUID: String, extension: String = "png"): String {
        return "${drawableFolderPath}/${mono_chrome_image}_${orgUUID}.${extension}"
    }

    fun squareDrawableFilePath(orgUUID: String, extension: String = "png"): String {
        return "${drawableFolderPath}/${square_image}_${orgUUID}.${extension}"
    }

    fun wideDrawableFilePath(orgUUID: String, extension: String = "png"): String {
        return "${drawableFolderPath}/${wide_image}_${orgUUID}.${extension}"
    }

    fun wideUseScreenDrawableFilePath(orgUUID: String, extension: String = "png"): String {
        return "${drawableFolderPath}/${wide_image_use_screen}_${orgUUID}.${extension}"
    }

    const val fareMediumLogoPath = "$drawableFolderPath/fare_medium_logo.png"

}