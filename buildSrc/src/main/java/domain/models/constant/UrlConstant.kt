package domain.models.constant

import domain.models.ClientConfiguration
import java.util.*

class UrlConstant(clientConf: ClientConfiguration) {

    val client = clientConf.client
    val environment = clientConf.environment

    val baseUrl = "https://s3.amazonaws.com/config.bytemark.co/develop/01"

    val confUrl = "$baseUrl/${client}/${environment}/${PathConstant.configFileName}"

    val googleServicesJsonFileUrl =
        "$baseUrl/${client}/${environment}/${PathConstant.googleServicesJsonFile}"

    val settingsJsonFileUrl =
        "$baseUrl/${client}/${environment}/${PathConstant.baseSettingsListFileName}.en"

    val settingsJsonFileUrlUs =
        "$baseUrl/${client}/${environment}/${PathConstant.baseSettingsListFileName}.en-US"

    val moreMenuJsonFileUrl =
        "$baseUrl/${client}/${environment}/${PathConstant.baseMoreInformationFileName}.en"

    val moreMenuJsonFileUrlUs =
        "$baseUrl/${client}/${environment}/${PathConstant.baseMoreInformationFileName}.en-US"

    val baseHaconURL =
        "https://s3.amazonaws.com/hacon-sdk-integration/android/" + client.toLowerCase(Locale.ENGLISH) + "/"

    val haconGSONLibUrl =
        "https://s3.amazonaws.com/hacon-sdk-integration/android/" + client.toLowerCase(Locale.ENGLISH) + "/google-gson-2.1.0.jar"

    val elertURL =
        "https://s3.amazonaws.com/elert-sdk-integration/android/" + client.toLowerCase(Locale.ENGLISH) + "/"


    fun fileDownloadUrlFromLanguageCode(fileName: String, languageCode: String) =
        "$baseUrl/${client}/${environment}/${fileName}.${languageCode}"


    fun fileDownloadUrlWithLanguageCountryCode(fileName: String, languageCode: String, countryCode: String) =
        "$baseUrl/${client}/${environment}/${fileName}.${languageCode}-${countryCode}"


}