package domain.models.menu

import domain.models.gtfs.Schedules
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class Settings(
    @SerializedName("signed_in_menu_groups") var signedInMenuGroups: MutableList<MenuGroup> = ArrayList(),
    @SerializedName("signed_out_menu_groups") var signedOutMenuGroups: MutableList<MenuGroup> = ArrayList(),
    @SerializedName("menu_groups") var menuGroups: MutableList<MenuGroup> = ArrayList(),
    var schedules: Schedules
)