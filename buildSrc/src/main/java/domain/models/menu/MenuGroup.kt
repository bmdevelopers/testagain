package domain.models.menu

import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class MenuGroup(@SerializedName("menu_items") var menuItems: MutableList<MenuItem> = ArrayList())

