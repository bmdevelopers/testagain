package domain.models.menu

import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class Action(
    var type: String,
    val link: String? = null,
    @SerializedName("menu_groups") var menuGroups: MutableList<MenuGroup> = ArrayList()
)
