package domain.models.domain

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class Domain(
    @SerializedName("type") @Expose var type: String,

    @SerializedName("overture_url") @Expose var overtureUrl: String,

    @SerializedName("accounts_url") @Expose var accountsUrl: String
)