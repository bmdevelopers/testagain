package domain.models.local

data class AppConfiguration(
    var appId : String = "",
    var appName : String = "",
    var domainType : String = "",
    var reverseDomain : String = "",
    var identifier : String = "",
    var clientId : String = "",
    var displayName : String = "",
    var googleMapsApiKey : String = "",
    var serverClientId : String = "",
    var fileProviderAuthority : String = "",
    var supportedLocales : ArrayList<String> = ArrayList<String>()
)