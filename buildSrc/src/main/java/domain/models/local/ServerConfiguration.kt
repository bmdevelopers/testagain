package domain.models.local

data class ServerConfiguration(
        val accountUrl : String = "",
        val overtureUrl : String = "",
        val redirectUrl : String = ""
)
