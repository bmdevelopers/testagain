package domain.models.organisation

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class SupportedLocale(
    @SerializedName("display_name") @Expose var displayName: String? = null,

    @SerializedName("language_code") @Expose var languageCode: String,

    @SerializedName("country_code") @Expose var countryCode: String? = null
)