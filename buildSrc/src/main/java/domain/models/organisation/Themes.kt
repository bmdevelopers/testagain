package domain.models.organisation

import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class Themes(
    @SerializedName("data_theme") var dataTheme: Theme, @SerializedName("accent_theme") var accentTheme: Theme, @SerializedName("background_theme") var backgroundTheme: Theme, @SerializedName("collection_theme") var collectionTheme: Theme, @SerializedName("header_theme") var headerTheme: Theme, @SerializedName("navigation_theme") var navigationTheme: Theme
)