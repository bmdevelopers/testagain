package domain.models.organisation

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class Branding(
    @SerializedName("logo_images") @Expose var logoImages: LogoImages? = null,

    @SerializedName("themes") @Expose var themes: Themes? = null,

    @SerializedName("indicators") @Expose var indicators: Indicators? = null
)