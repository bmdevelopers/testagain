package domain.models.organisation

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class ChildOrganizationKt(
    @SerializedName("uuid") @Expose var uuid: String,

    @SerializedName("abbreviation") var abbreviation: String? = null,

    @SerializedName("branding") @Expose var branding: Branding? = null
)