package domain.models.organisation

import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class Theme(
    @SerializedName("background_color") var backgroundColor: String,
    @SerializedName("primary_text_color") var primaryTextColor: String,
    @SerializedName("secondary_text_color") var secondaryTextColor: String,
    @SerializedName("accent_color") var accentColor: String
)
