package domain.models.organisation

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class Organization(
    @SerializedName("uuid") @Expose var uuid: String,

    @SerializedName("display_name") @Expose var displayName: String,

    @SerializedName("is_elert_sdk_enabled") @Expose var isElertSdkEnabled: Boolean?,

    @SerializedName("deeplink_configuration") @Expose var deeplinkConfiguration: DeepLinkConfiguration? = null,

    @SerializedName("supported_locales") @Expose var supportedLocales: List<SupportedLocale> = ArrayList(),

    @SerializedName("branding") @Expose var branding: Branding? = null,

    @SerializedName("child_organizations") @Expose var childOrganizations: List<ChildOrganizationKt> = ArrayList(),

    @SerializedName("fare_medium_card_details") @Expose var fareMediumCardDetailsList: List<String> = ArrayList(),

    @SerializedName("is_connect_app") @Expose var isConnectApp: Boolean = true

)
