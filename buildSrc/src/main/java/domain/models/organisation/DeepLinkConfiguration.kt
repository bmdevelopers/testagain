package domain.models.organisation

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class DeepLinkConfiguration (
    @SerializedName("host") @Expose var host: String? = null
)