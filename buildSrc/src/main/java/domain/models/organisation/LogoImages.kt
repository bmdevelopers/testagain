package domain.models.organisation

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class LogoImages(
    @SerializedName("logo_monochrome_mask") @Expose var logoMonochromeMask: String? = null,

    @SerializedName("logo_square") @Expose var logoSquare: String? = null,

    @SerializedName("logo_wide") @Expose var logoWide: String? = null,

    @SerializedName("logo_wide_use_screen") @Expose var logoWideUseScreen: String? = null,

    @SerializedName("fare_medium_logo") @Expose var fareMediumLogo: String? = null
)
