package domain.models.organisation

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class Indicators(
    @SerializedName("success") @Expose var success: String? = null,

    @SerializedName("error") @Expose var error: String? = null,

    @SerializedName("warning") @Expose var warning: String? = null
)