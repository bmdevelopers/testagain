package domain.models.client

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class ClientsKt(
    @SerializedName("customer_mobile") @Expose var customerMobile: CustomerMobileKt
)
