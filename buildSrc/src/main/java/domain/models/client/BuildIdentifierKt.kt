package domain.models.client

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class BuildIdentifierKt(
    @SerializedName("service") @Expose var service: String? = null,

    @SerializedName("key") @Expose var key: String? = null,

    @SerializedName("url") @Expose var url: String? = null,

    @SerializedName("web_client_id") @Expose var webClientId: String? = null
)
