package domain.models.client

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class IconsKt(
    @SerializedName("mdpi") @Expose var mdpi: String? = null,

    @SerializedName("hdpi") @Expose var hdpi: String? = null,

    @SerializedName("xhdpi") @Expose var xhdpi: String? = null,

    @SerializedName("xxhdpi") @Expose var xxhdpi: String? = null,

    @SerializedName("xxxhdpi") @Expose var xxxhdpi: String? = null,

    @SerializedName("google_play") @Expose private val googlePlay: String? = null
)