package domain.models.client

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class CustomerMobileKt(
    @SerializedName("display_name") @Expose var displayName: String,

    @SerializedName("reverse_domain") @Expose var reverseDomain: String? = null,

    @SerializedName("identifier") @Expose var identifier: String? = null,

    @SerializedName("location_enabled") @Expose var locationEnabled: Boolean? = true,

    @SerializedName("hacon_sdk_enabled") @Expose var haconSdkEnabled: Boolean = false,

    @SerializedName("remote_notification_enabled") @Expose var notificationEnabled: Boolean? = false,

    @SerializedName("authentication_method") var authenticationMethod: String? = null,

    @SerializedName("schemes") @Expose var schemes: List<String> = ArrayList(),

    @SerializedName("client") @Expose var client: Client,

    @SerializedName("android") @Expose var android: IAndroidKt
)