package domain.models.client

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class Client(
    @SerializedName("client_id") @Expose var clientId: String
)

