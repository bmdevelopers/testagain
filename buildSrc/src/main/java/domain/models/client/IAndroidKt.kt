package domain.models.client

import org.gradle.internal.impldep.com.google.gson.annotations.Expose
import org.gradle.internal.impldep.com.google.gson.annotations.SerializedName

data class IAndroidKt(
    @SerializedName("icons") @Expose var icons: IconsKt,

    @SerializedName("notification_icons") @Expose var notificationIcons: IconsKt? = null,

    @SerializedName("splash_screens") @Expose var splashScreens: SplashScreensKt,

    @SerializedName("reverse_domain") @Expose var reverseDomain: String? = null,

    @SerializedName("identifier") @Expose var identifier: String? = null,

    @SerializedName("gradle_version") @Expose var gradleVersion: String? = null,

    @SerializedName("build_identifiers") @Expose var buildIdentifiers: List<BuildIdentifierKt> = ArrayList(),

    @SerializedName("hacon_lib_link") @Expose var haconLibLink: String? = null,

    //Keeping the name same : hacon_slidinguppanel_lib_link otherwise it is not working (cause : Gson naming policy)
    @SerializedName("hacon_slidinguppanel_lib_link") @Expose val hacon_slidinguppanel_lib_link: String,

    @SerializedName("hacon_piwik_lib_link") @Expose var haconPiwikLibLink: String? = null,

    @SerializedName("was_old_app") @Expose var wasOldApp :Boolean = false
)