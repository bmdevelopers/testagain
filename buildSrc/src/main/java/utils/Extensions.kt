package utils

import domain.models.constant.ConsoleColors
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.*

fun Array<String>.deleteFilesFromDirectory(dirPath: String) {
    forEach {
        File("${dirPath}${it}").run {
            deleteAndLog()
        }
    }
}

fun Response<ResponseBody>.save(path: String, sourceUrl: String = "", abortIfFails: Boolean = false): Boolean {
    val status = saveWithStatus(path)
    printDetail(status.first, sourceUrl, path, status.second, abortIfFails)
    return status.first
}

fun printDetail(status: Boolean, url: String, destination: String, errorMessage: String?, abortIfFails: Boolean = false) {
    if (!status) {
        println(
            "\nURL            ::: $url" + "\nDestination    ::: $destination" + ConsoleColors.RESET +
            "\nStatus         :::" + ConsoleColors.RED + " Failed \uD83D\uDE10" + ConsoleColors.RESET +
            "\nCause          ::: $errorMessage"
        )

        if (abortIfFails) {
            throw InterruptedIOException("$url Download Failed! Operation Aborted.")
        }

    } else {
        println(
            "\nURL            ::: $url" +
            "\nDestination    ::: $destination" + ConsoleColors.RESET +
            "\nStatus         :::" + ConsoleColors.BLUE + " Successful \uD83D\uDE00" + ConsoleColors.RESET
        )
    }
}

fun Response<ResponseBody>.saveWithStatus(path: String): Pair<Boolean, String?> {
    if (isSuccessful && body() != null) {
        return try {
            val destinationFile = File(path)
            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null
            try {
                val fileReader = ByteArray(4096)
                val fileSize: Long = body()!!.contentLength()
                var fileSizeDownloaded: Long = 0
                inputStream = body()!!.byteStream()
                outputStream = FileOutputStream(destinationFile)
                while (true) {
                    val read = inputStream.read(fileReader)
                    if (read == -1) {
                        break
                    }
                    outputStream.write(fileReader, 0, read)
                    fileSizeDownloaded += read.toLong()
                }
                outputStream.flush()
                return Pair(true, "Successful")
            } catch (e: IOException) {
                Pair(false, e.message)
            } finally {
                inputStream?.close()
                outputStream?.close()
            }
        } catch (e: IOException) {
            Pair(false, e.message)
        }
    } else {
        return Pair(false, "Error Downloading")
    }
}

fun File.replaceLine(
    lineToModifyStartsWith: String? = null,
    lineToModifyContains: String? = null,
    lineToReplace: String, tempFile: File,
    deepDebugging: Boolean = false
) {

    if (deepDebugging) {
        println("ZZZ Inside Update current line ${tempFile.name}")
    }

    try {

        if (deepDebugging) {
            println("ZZZ Inside Update current line ${tempFile.name}")
        }

        val reader = java.io.BufferedReader(FileReader(this))
        val writer = java.io.BufferedWriter(FileWriter(tempFile))
        var currentLine = ""

        while (reader.readLine().also { currentLine = it ?: "" } != null) {
            val trimmedLine = currentLine.trim { it <= ' ' }

            if (deepDebugging) {
                println("ZZZ current line $currentLine")
            }

            if (!lineToModifyStartsWith.isNullOrEmpty()) {
                if (trimmedLine.startsWith(lineToModifyStartsWith)) {
                    if (deepDebugging) {
                        println("ZZZ write line $lineToReplace")
                    }
                    writer.write(lineToReplace + System.getProperty("line.separator"))
                    continue
                }
            } else if (!lineToModifyContains.isNullOrEmpty() && trimmedLine.contains(lineToModifyContains)) {
                if (deepDebugging) {
                    println("ZZZ modify write line $lineToReplace")
                }
                writer.write(lineToReplace + System.getProperty("line.separator"))
                continue
            }

            writer.write(currentLine + System.getProperty("line.separator"))
        }
        writer.close()
        reader.close()
        tempFile.renameTo(this)
    } catch (e: Exception) {
        e.printStackTrace()
        if (deepDebugging) {
            println("Error : error ${e.toString()}")
            println("Error : stackTrace ${e.printStackTrace()}")
            println("Error : localizedMessage ${e.localizedMessage}")
            println("Error : Could write to ${this.name}")
        }
    }
}

fun File.deleteAndLog() {
    println("")
    println(ConsoleColors.RESET + "Deleting       :::  " + ConsoleColors.RED + path + ConsoleColors.RESET);
    if (exists() && delete()) {
        println(ConsoleColors.RESET + "Status         :::" + ConsoleColors.BLUE + "  Successful \uD83D\uDE00" + ConsoleColors.RESET);
    } else {
        println(ConsoleColors.RESET + "Status         :::" + ConsoleColors.RED + "  Skipped \uD83D\uDE10" + ConsoleColors.RESET);
    }
}