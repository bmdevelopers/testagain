package co.bytemark.data.product.local;

import androidx.annotation.NonNull;

import co.bytemark.data.data_store.local.LocalEntityStore;
import co.bytemark.domain.model.common.BMResponse;
import rx.Observable;

/**
 * Created by Omkar on 9/7/17.
 */
public interface ProductsLocalEntityStore extends LocalEntityStore {

    @NonNull
    Observable<BMResponse> purchaseProducts();
}
