package co.bytemark.data.product;

import androidx.annotation.NonNull;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import co.bytemark.data.RepositoryImpl;
import co.bytemark.data.annotation.Local;
import co.bytemark.data.annotation.Remote;
import co.bytemark.data.net.manager.NetworkManager;
import co.bytemark.data.product.local.ProductsLocalEntityStore;
import co.bytemark.data.product.remote.ProductsRemoteEntityStore;
import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.domain.model.order.ApplyPromoCode;
import co.bytemark.domain.repository.ProductRepository;
import co.bytemark.sdk.post_body.CreateOrder;
import co.bytemark.sdk.post_body.MakePayment;
import retrofit2.http.Tag;
import rx.Observable;

@Singleton
public class ProductRepositoryImpl extends RepositoryImpl<ProductsRemoteEntityStore, ProductsLocalEntityStore>
        implements ProductRepository {

    @Inject
    public ProductRepositoryImpl(@NonNull NetworkManager networkManager,
                                 @NonNull @Remote ProductsRemoteEntityStore remoteStore,
                                 @NonNull @Local ProductsLocalEntityStore localStore) {
        super(networkManager, remoteStore, localStore);
    }


    @NonNull
    @Override
    public Observable<BMResponse> purchaseProducts(@NonNull CreateOrder createOrder, @NonNull Map<String, String> queryParameters) {
        return remoteStore.purchaseProducts(createOrder, queryParameters);
    }

    @Override
    public Observable<BMResponse> getPayPalToken() {
        return remoteStore.getPayPalToken();
    }

    @NonNull
    @Override
    public Observable<BMResponse> getAcceptedPayments() {
        return remoteStore.getAcceptedPayments();
    }

    @NonNull
    @Override
    public Observable<BMResponse> getAcceptedPaymentMethods(String organizationUuid) {
        return remoteStore.getAcceptedPaymentMethods(organizationUuid);
    }

    @NonNull
    @Override
    public Observable<BMResponse> purchaseProductsForIdeal(CreateOrder createOrder, Map<String, String> queryParameters) {
        return remoteStore.purchaseProductsForIdeal(createOrder, queryParameters);
    }

    @NonNull
    @Override
    public Observable<BMResponse> makePayment(String orderUUID, MakePayment makePayment, String deepLinkJwtToken) {
        return remoteStore.makePayment(orderUUID, makePayment, deepLinkJwtToken);
    }

    @NonNull
    @Override
    public Observable<BMResponse> createOrder(CreateOrder createOrder, String deepLinkJwtToken) {
        return remoteStore.createOrder(createOrder, deepLinkJwtToken);
    }

    @Override
    public Observable<BMResponse> applyPromoCode(ApplyPromoCode applyPromoCode) {
        return remoteStore.applyPromoCode(applyPromoCode);
    }


}
