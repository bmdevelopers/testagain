package co.bytemark.data.product.remote;

import android.app.Application;
import androidx.annotation.NonNull;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import co.bytemark.data.net.OvertureRestApi;
import co.bytemark.data.net.store.OvertureRestApiStore;
import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.domain.model.order.ApplyPromoCode;
import co.bytemark.sdk.post_body.CreateOrder;
import co.bytemark.sdk.post_body.MakePayment;
import rx.Observable;

/**
 * Created by Arunkumar on 10/10/17.
 */
@Singleton
public class ProductsRemoteStoreImpl extends OvertureRestApiStore implements ProductsRemoteEntityStore {

    @Inject
    ProductsRemoteStoreImpl(@NonNull Application application, @NonNull OvertureRestApi overtureRestApi) {
        super(application, overtureRestApi);
    }

    @NonNull
    @Override
    public Observable<BMResponse> purchaseProducts(@NonNull CreateOrder createOrder, @NonNull Map<String, String> queryParameters) {
        return overtureRestApi.postPurchaseOrder(createOrder, queryParameters)
                .compose(errorHandlingTransformer);

    }

    @Override
    public Observable getPayPalToken() {
        return overtureRestApi.getPayPalClientToken().compose(errorHandlingTransformer);
    }

    @Override
    public Observable<BMResponse> getAcceptedPayments() {
        return overtureRestApi.getAcceptedPayments();
    }

    @Override
    public Observable<BMResponse> getAcceptedPaymentMethods(String organizationUuid) {
        return overtureRestApi.getAcceptedPaymentMethods(organizationUuid).compose(errorHandlingTransformer);
    }

    @Override
    public Observable<BMResponse> purchaseProductsForIdeal(CreateOrder createOrder, Map<String, String> queryParameters) {
        return overtureRestApi.postPurchaseOrderForIdeal(createOrder, queryParameters)
                .compose(errorHandlingTransformer);
    }

    @Override
    public Observable<BMResponse> makePayment(String orderUUID, MakePayment makePayment, String deepLinkJwtToken) {
        return overtureRestApi.makePayment(orderUUID, makePayment, deepLinkJwtToken)
                .compose(errorHandlingTransformer);
    }

    @Override
    public Observable<BMResponse> createOrder(CreateOrder createOrder, String deepLinkJwtToken) {
        return overtureRestApi.createOrder(createOrder, deepLinkJwtToken).compose(errorHandlingTransformer)
                .compose(errorHandlingTransformer);
    }

    @Override
    public Observable<BMResponse> applyPromoCode(ApplyPromoCode applyPromoCode) {
        return overtureRestApi.applyPromoCode(applyPromoCode).compose(errorHandlingTransformer);
    }


}
