package co.bytemark.data.product.local;

import androidx.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;

import co.bytemark.domain.model.common.BMResponse;
import rx.Observable;

/**
 * Created by Arunkumar on 10/10/17.
 */
@Singleton
public class ProductsLocalStoreImpl implements ProductsLocalEntityStore {

    @Inject
    public ProductsLocalStoreImpl() {
    }

    @NonNull
    @Override
    public Observable<BMResponse> purchaseProducts() {
        return null;
    }
}
