package co.bytemark.data.product.remote;

import androidx.annotation.NonNull;

import java.util.Map;

import co.bytemark.data.data_store.remote.RemoteEntityStore;
import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.domain.model.order.ApplyPromoCode;
import co.bytemark.sdk.post_body.CreateOrder;
import co.bytemark.sdk.post_body.MakePayment;
import rx.Observable;

/**
 * Created by Omkar on 9/7/17.
 */
public interface ProductsRemoteEntityStore extends RemoteEntityStore {

    Observable getPayPalToken();

    Observable<BMResponse> getAcceptedPayments();

    Observable<BMResponse> getAcceptedPaymentMethods(String organizationUuid);

    Observable<BMResponse> purchaseProducts(CreateOrder createOrder, Map<String, String> queryParameters);

    Observable<BMResponse> purchaseProductsForIdeal(CreateOrder createOrder, Map<String, String> queryParameters);

    Observable<BMResponse> makePayment(String orderUUID, MakePayment makePayment, String deepLinkJwtToken);

    Observable<BMResponse> createOrder(CreateOrder createOrder, String deepLinkJwtToken);

    Observable<BMResponse> applyPromoCode(ApplyPromoCode applyPromoCode);
}
