package co.bytemark.data.discount

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.discount.local.DiscountLocalEntityStore
import co.bytemark.data.discount.remote.DiscountRemoteEntityStore
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.discount.DiscountCalculationRequest
import co.bytemark.domain.repository.DiscountRepository
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class DiscountRepositoryImpl @Inject constructor(
    networkManager: NetworkManager,
    @Remote remoteEntityStore: DiscountRemoteEntityStore,
    @Local localEntityStore: DiscountLocalEntityStore
) : RepositoryImpl<DiscountRemoteEntityStore, DiscountLocalEntityStore>(
    networkManager,
    remoteEntityStore,
    localEntityStore
), DiscountRepository {
    override fun discountCalculation(
        discountCalculationRequest: DiscountCalculationRequest,
        deeplinkJWTToken: String
    ): Deferred<BMResponse> =
        remoteStore.discountCalculation(discountCalculationRequest, deeplinkJWTToken)
}