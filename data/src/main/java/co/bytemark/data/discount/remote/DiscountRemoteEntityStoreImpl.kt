package co.bytemark.data.discount.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.discount.DiscountCalculationRequest
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class DiscountRemoteEntityStoreImpl @Inject constructor(
    application: Application,
    overtureRestApi: OvertureRestApi,
    coroutineOvertureApi: CoroutineOvertureApi
) : OvertureRestApiStore(
    application,
    overtureRestApi,
    coroutineOvertureApi
), DiscountRemoteEntityStore {
    override fun discountCalculation(
        discountCalculationRequest: DiscountCalculationRequest,
        deeplinkJWTToken: String
    ): Deferred<BMResponse> =
        coroutineOvertureApi.discountCalculation(
            discountCalculationRequest,
            deeplinkJWTToken
        )
}