package co.bytemark.data.discount.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.discount.DiscountCalculationRequest
import kotlinx.coroutines.Deferred

interface DiscountRemoteEntityStore : RemoteEntityStore {
    fun discountCalculation(discountCalculationRequest: DiscountCalculationRequest, deeplinkJWTToken: String): Deferred<BMResponse>
}