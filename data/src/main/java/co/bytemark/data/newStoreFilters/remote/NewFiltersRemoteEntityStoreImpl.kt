package co.bytemark.data.newStoreFilters.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.store.filter.SearchResponseData
import co.bytemark.sdk.post_body.PostSearch
import javax.inject.Inject

/**
 * Created by vally on 05/12/19.
 */
class NewFiltersRemoteEntityStoreImpl @Inject constructor(application: Application, overtureRestApi: OvertureRestApi,
                                                          coroutineOvertureApi: CoroutineOvertureApi) :
        OvertureRestApiStore(application, overtureRestApi, coroutineOvertureApi), NewFiltersRemoteEntityStore {

    override suspend fun getStoreFilters(postSearch: PostSearch): Response<SearchResponseData> =
            coroutineOvertureApi.postSearch(postSearch)

}