package co.bytemark.data.newStoreFilters

import androidx.annotation.NonNull
import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.newStoreFilters.local.NewFiltersLocalEntityStore
import co.bytemark.data.newStoreFilters.remote.NewFiltersRemoteEntityStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.store.filter.SearchResponseData
import co.bytemark.domain.repository.StoreFiltersRepository
import co.bytemark.sdk.post_body.PostSearch
import javax.inject.Inject


/**
 * Created by vally on 05/12/19.
 */
class NewFiltersRepositoryImpl @Inject constructor(@NonNull networkManager: NetworkManager,
                                                   @NonNull @Remote remoteEntityStore: NewFiltersRemoteEntityStore,
                                                   @NonNull @Local localEntityStore: NewFiltersLocalEntityStore)
    : RepositoryImpl<NewFiltersRemoteEntityStore, NewFiltersLocalEntityStore>(networkManager, remoteEntityStore, localEntityStore), StoreFiltersRepository {

    override suspend fun getStoreFilters(postSearch: PostSearch): Response<SearchResponseData> =
            remoteStore.getStoreFilters(postSearch)
}