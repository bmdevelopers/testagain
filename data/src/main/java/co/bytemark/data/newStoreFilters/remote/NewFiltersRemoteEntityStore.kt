package co.bytemark.data.newStoreFilters.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.store.filter.SearchResponseData
import co.bytemark.sdk.post_body.PostSearch

/**
 * Created by vally on 05/12/19.
 */
interface NewFiltersRemoteEntityStore : RemoteEntityStore {

    suspend fun getStoreFilters(postSearch: PostSearch): Response<SearchResponseData>

}