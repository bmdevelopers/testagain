package co.bytemark.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timber.log.Timber;

public class DeviceInfo {

    private static final String TAG = "DeviceInfo";
    private static final String DEFAULT_APP_VERSION = "0.0";

    /**
     * Returns the display name of the current operating system.
     */
    public static String getOS() {
        return "Android";
    }

    /**
     * Returns the current operating system version as a displayable string.
     */
    public static String getOSVersion() {
        return android.os.Build.VERSION.RELEASE;
    }

    /**
     * Returns the current device model.
     */
    public static String getDevice() {
        return android.os.Build.MODEL;
    }

    /**
     * Returns the consumer friendly device model.
     */
    public static String getDeviceModel() {
        String manufacturer = android.os.Build.MANUFACTURER;
        String model = android.os.Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    /**
     * Capitalizes the model number passed from method
     * {@link DeviceInfo#getDeviceModel()}
     */
    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    /**
     * Returns the current locale (ex. "en_US").
     */
    public static String getLocale() {
        final Locale locale = Locale.getDefault();
        return locale.getLanguage() + "_" + locale.getCountry();
    }

    /**
     * Returns the application version string stored in the specified
     * context's package info versionName field.
     */
    public static String getCurrentAppVersion(final Context context) {
        String appVersion = DEFAULT_APP_VERSION;
        try {
            String versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;

            Pattern pattern = Pattern.compile("[0-9]+\\.[0-9]");
            Matcher matches = pattern.matcher(versionName);

            if (matches.find())
                appVersion = matches.group();
        } catch (PackageManager.NameNotFoundException e) {
            Timber.tag(TAG).d("No app version found.");
        }
        return appVersion;
    }

    /**
     * Returns the app version saved inside {@link SharedPreferences}
     * after updating the local application installation identifier.
     */
    public static String getDeviceAppVersion(final Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        String appVersionKey = context.getPackageName() + ".version";
        return sharedPrefs.getString(appVersionKey, null);
    }

    /**
     * Saves the app version inside {@link SharedPreferences} after
     * sign in.
     */
    public static void setDeviceAppVersion(final Context context, final String deviceAppVersion) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        String appVersionKey = context.getPackageName() + ".version";
        sharedPrefs.edit().putString(appVersionKey, deviceAppVersion).apply();
    }
}
