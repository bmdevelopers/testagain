package co.bytemark.data.passes.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import javax.inject.Inject

/** Created by Santosh on 2019-11-21.
 * Copyright (c) 2019 Bytemark Inc. All rights reserved.
 */

class PassesRemoteEntityStoreImpl @Inject constructor(
        application: Application,
        overtureRestApi: OvertureRestApi,
        coroutineOvertureApi: CoroutineOvertureApi
) : OvertureRestApiStore(application,
        overtureRestApi,
        coroutineOvertureApi
), PassesRemoteEntityStore {

    override fun transferPassAsync(passUuid: String, destination: String): Deferred<JsonObject> =
            coroutineOvertureApi.transferPassAsync(passUuid,
                    destination)
}