package co.bytemark.data.passes.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import org.json.JSONObject

/** Created by Santosh on 2019-11-21.
 * Copyright (c) 2019 Bytemark Inc. All rights reserved.
 */

interface PassesRemoteEntityStore : RemoteEntityStore {

    fun transferPassAsync(passUuid: String, destination: String): Deferred<JsonObject>
}