package co.bytemark.data.passes

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.passes.local.PassesLocalEntityStore
import co.bytemark.data.passes.remote.PassesRemoteEntityStore
import co.bytemark.domain.repository.PassesRepository
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import org.json.JSONObject
import javax.inject.Inject

/** Created by Santosh on 2019-11-21.
 * Copyright (c) 2019 Bytemark Inc. All rights reserved.
 */

class PassesRepositoryImpl @Inject constructor(
        networkManager: NetworkManager,
        @Remote remoteEntityStore: PassesRemoteEntityStore,
        @Local localEntityStore: PassesLocalEntityStore
) : RepositoryImpl<PassesRemoteEntityStore, PassesLocalEntityStore>(
        networkManager,
        remoteEntityStore,
        localEntityStore
), PassesRepository {

    override fun transferPassAsync(passUuid: String, destination: String): Deferred<JsonObject> =
            remoteStore.transferPassAsync(passUuid, destination)
}

