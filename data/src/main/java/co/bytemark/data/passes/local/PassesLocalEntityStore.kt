package co.bytemark.data.passes.local

import co.bytemark.data.data_store.local.LocalEntityStore

/** Created by Santosh on 2019-11-21.
 * Copyright (c) 2019 Bytemark Inc. All rights reserved.
 */

interface PassesLocalEntityStore : LocalEntityStore