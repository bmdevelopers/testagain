package co.bytemark.data.voucher_redeem.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.voucher_code.VoucherCodeData
import co.bytemark.sdk.model.voucher_code.TVMVoucherRedeemRequest

interface VoucherRedeemRemoteEntityStore : RemoteEntityStore {

    suspend fun redeemVoucherCode(tvmVoucherRedeemRequest: TVMVoucherRedeemRequest): Response<VoucherCodeData>

}