package co.bytemark.data.voucher_redeem

import androidx.annotation.NonNull
import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.voucher_redeem.local.VoucherRedeemLocalEntityStore
import co.bytemark.data.voucher_redeem.remote.VoucherRedeemRemoteEntityStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.voucher_code.VoucherCodeData
import co.bytemark.domain.repository.VoucherRedeemRepository
import co.bytemark.sdk.model.voucher_code.TVMVoucherRedeemRequest
import javax.inject.Inject

class VoucherRedeemRepositoryImpl @Inject constructor(
        @NonNull networkManager: NetworkManager,
        @NonNull @Remote remoteEntityStore: VoucherRedeemRemoteEntityStore,
        @NonNull @Local localEntityStore: VoucherRedeemLocalEntityStore
) : RepositoryImpl<VoucherRedeemRemoteEntityStore, VoucherRedeemLocalEntityStore>(
        networkManager,
        remoteEntityStore,
        localEntityStore
), VoucherRedeemRepository {

    override suspend fun redeemVoucherCode(
            orderType: String, voucherCode: String, saveToDevice: Boolean
    ): Response<VoucherCodeData> {
        return remoteStore.redeemVoucherCode(TVMVoucherRedeemRequest(orderType, voucherCode, saveToDevice))
    }

}