package co.bytemark.data.voucher_redeem.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.voucher_code.VoucherCodeData
import co.bytemark.sdk.model.voucher_code.TVMVoucherRedeemRequest
import javax.inject.Inject

class VoucherRedeemRemoteEntityStoreImpl @Inject constructor(
        application: Application,
        overtureRestApi: OvertureRestApi,
        coroutineOvertureApi: CoroutineOvertureApi
) : OvertureRestApiStore(
        application,
        overtureRestApi,
        coroutineOvertureApi
), VoucherRedeemRemoteEntityStore {


    override suspend fun redeemVoucherCode(tvmVoucherRedeemRequest: TVMVoucherRedeemRequest): Response<VoucherCodeData> {
        return coroutineOvertureApi.redeemVoucherCodeAsync(tvmVoucherRedeemRequest)
    }

}