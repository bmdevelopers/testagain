package co.bytemark.data.fare_capping.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.interactor.fare_capping.FareCappingResponse
import co.bytemark.domain.model.common.Response

interface FareCappingRemoteEntityStore : RemoteEntityStore {
    suspend fun getFareCapping(): Response<FareCappingResponse>
}