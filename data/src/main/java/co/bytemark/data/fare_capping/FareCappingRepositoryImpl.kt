package co.bytemark.data.fare_capping

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.fare_capping.local.FareCappingLocalEntityStore
import co.bytemark.data.fare_capping.remote.FareCappingRemoteEntityStore
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.domain.interactor.fare_capping.FareCappingResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.FareCappingRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FareCappingRepositoryImpl @Inject internal constructor(
    networkManager: NetworkManager, @Remote remoteStore: FareCappingRemoteEntityStore, @Local localStore: FareCappingLocalEntityStore
) : RepositoryImpl<FareCappingRemoteEntityStore, FareCappingLocalEntityStore>(
    networkManager, remoteStore, localStore
), FareCappingRepository {

    override suspend fun getFareCapping(): Response<FareCappingResponse> {
        return remoteStore.getFareCapping()
    }
}