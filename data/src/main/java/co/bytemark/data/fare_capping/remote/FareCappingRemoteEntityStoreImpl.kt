package co.bytemark.data.fare_capping.remote

import android.app.Application

import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import javax.inject.Inject

class FareCappingRemoteEntityStoreImpl @Inject internal constructor(
    application: Application, overtureRestApi: OvertureRestApi, coroutineOvertureApi: CoroutineOvertureApi
) : OvertureRestApiStore(
    application, overtureRestApi, coroutineOvertureApi
), FareCappingRemoteEntityStore {

    override suspend fun getFareCapping() = coroutineOvertureApi.getFareCapping()

}