package co.bytemark.data.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.text.TextUtils;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;

/**
 * Created by Omkar on 6/1/17.
 * <p>
 * {@inheritDoc https://medium.com/@ericfu/securely-storing-secrets-in-an-android-application-501f030ae5a3,
 * https://stackoverflow.com/questions/42560868/storing-credentials-in-the-android-app?answertab=votes#tab-top}
 */

class BytemarkKeyStore {

    private static final String RSA_MODE = "RSA/ECB/PKCS1Padding";
    private static final String AES_MODE = "AES/GCM/NoPadding";
    private static final String KEY_ALIAS = "Bytemark, Inc.";
    private static final String KEY_ALIAS_NEW = "Bytemark";
    private static final String ANDROID_KEY_STORE = "AndroidKeyStore";
    private static final String SHARED_PREFERENCE_NAME = "BytemarkKeyStore";
    private static final String ENCRYPTED_KEY = "ENCRYPTED_AES_KEY";
    private static final String PUBLIC_IV = "PUBLIC_IV";

    private KeyStore keyStore;
    private static BytemarkKeyStore INSTANCE;

    private BytemarkKeyStore(Context context) throws NoSuchPaddingException, NoSuchProviderException,
            NoSuchAlgorithmException, InvalidAlgorithmParameterException, KeyStoreException,
            CertificateException, IOException {
        this.generateEncryptKey(context);
        this.generateRandomIV(context);
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
            try {
                this.generateAESKey(context);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized BytemarkKeyStore getInstance(Context context)
            throws NoSuchPaddingException, NoSuchProviderException, NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, KeyStoreException, CertificateException, IOException {
        if (INSTANCE == null) {
            INSTANCE = new BytemarkKeyStore(context);
        }
        return INSTANCE;
    }

    @SuppressLint("InlinedApi")
    @SuppressWarnings("deprecation")
    private void generateEncryptKey(Context context) throws NoSuchProviderException,
            NoSuchAlgorithmException, InvalidAlgorithmParameterException, KeyStoreException,
            CertificateException, IOException {

        keyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
        keyStore.load(null);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (!keyStore.containsAlias(KEY_ALIAS)) {
                KeyGenerator keyGenerator = KeyGenerator
                        .getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEY_STORE);
                keyGenerator.init(
                        new KeyGenParameterSpec.Builder(KEY_ALIAS,
                                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                                .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                                .setRandomizedEncryptionRequired(false)
                                .build());
                keyGenerator.generateKey();
            }
        } else {
            if (!keyStore.containsAlias(KEY_ALIAS)) {
                try {
                    generateSpecBuilder(context, KEY_ALIAS);
                }catch (Exception e){
                    generateSpecBuilder(context, KEY_ALIAS_NEW);
                }
            }
        }
    }

    private void generateSpecBuilder(Context context,String KEY_ALIAS) throws NoSuchProviderException,
            NoSuchAlgorithmException, InvalidAlgorithmParameterException{
        // Generate a key pair for encryption
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        end.add(Calendar.YEAR, 30);
        KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
                .setAlias(KEY_ALIAS)
                .setSubject(new X500Principal("CN=" + KEY_ALIAS))
                .setSerialNumber(BigInteger.TEN)
                .setStartDate(start.getTime())
                .setEndDate(end.getTime())
                .build();
        KeyPairGenerator kpg = KeyPairGenerator
                .getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEY_STORE);
        kpg.initialize(spec);
        kpg.generateKeyPair();
    }

    private void generateRandomIV(Context context) {
        SharedPreferences pref = context
                .getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        String publicIV = pref.getString(PUBLIC_IV, null);

        if (publicIV == null) {
            SecureRandom random = new SecureRandom();
            byte[] generated = random.generateSeed(12);
            String generatedIVstr = Base64.encodeToString(generated, Base64.DEFAULT);
            SharedPreferences.Editor edit = pref.edit();
            edit.putString(PUBLIC_IV, generatedIVstr);
            edit.apply();
        }
    }

    private byte[] rsaEncrypt(byte[] secret) throws Exception {
        PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry(KEY_ALIAS, null);
        // Encrypt the text
        Cipher inputCipher = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        inputCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inputCipher);
        cipherOutputStream.write(secret);
        cipherOutputStream.close();

        return outputStream.toByteArray();
    }

    @SuppressWarnings("UnnecessaryUnboxing")
    private byte[] rsaDecrypt(byte[] encrypted) throws Exception {
        PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry(KEY_ALIAS, null);
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
        CipherInputStream cipherInputStream = new CipherInputStream(
                new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i).byteValue();
        }
        return bytes;
    }

    private void generateAESKey(Context context) throws Exception {
        SharedPreferences pref = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        String enryptedKeyB64 = pref.getString(ENCRYPTED_KEY, null);
        if (enryptedKeyB64 == null) {
            byte[] key = new byte[16];
            SecureRandom secureRandom = new SecureRandom();
            secureRandom.nextBytes(key);
            byte[] encryptedKey = rsaEncrypt(key);
            enryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT);
            SharedPreferences.Editor edit = pref.edit();
            edit.putString(ENCRYPTED_KEY, enryptedKeyB64);
            edit.apply();
        }
    }


    @SuppressWarnings("UnnecessaryLocalVariable")
    private Key getAESKeyFromKS() throws NoSuchProviderException, NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, KeyStoreException, CertificateException,
            IOException, UnrecoverableKeyException {
        keyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
        keyStore.load(null);
        SecretKey key = (SecretKey) keyStore.getKey(KEY_ALIAS, null);
        return key;
    }


    private Key getSecretKey(Context context) throws Exception {
        SharedPreferences pref = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        String encryptedKeyB64 = pref.getString(ENCRYPTED_KEY, null);

        byte[] encryptedKey = Base64.decode(encryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaDecrypt(encryptedKey);
        return new SecretKeySpec(key, "AES");
    }

    void encrypt(Context context, String key, String input) throws NoSuchAlgorithmException,
            NoSuchPaddingException, NoSuchProviderException, BadPaddingException,
            IllegalBlockSizeException, UnsupportedEncodingException {
        Cipher c;
        SharedPreferences pref = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        String publicIV = pref.getString(PUBLIC_IV, null);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            c = Cipher.getInstance(AES_MODE);
            try {
                c.init(Cipher.ENCRYPT_MODE, getAESKeyFromKS(),
                        new GCMParameterSpec(128, Base64.decode(publicIV, Base64.DEFAULT)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            c = Cipher.getInstance(AES_MODE);
            try {
                c.init(Cipher.ENCRYPT_MODE, getSecretKey(context),
                        new GCMParameterSpec(128, Base64.decode(publicIV, Base64.DEFAULT)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        byte[] encodedBytes = c.doFinal(input.getBytes("UTF-8"));
        String encodedString = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
        setStringToSharedPrefs(context, key, encodedString);
    }

    String decrypt(Context context, String key) throws NoSuchAlgorithmException,
            NoSuchPaddingException, NoSuchProviderException, BadPaddingException,
            IllegalBlockSizeException, UnsupportedEncodingException {
        Cipher c;
        SharedPreferences pref = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        String publicIV = pref.getString(PUBLIC_IV, null);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            c = Cipher.getInstance(AES_MODE);
            try {
                c.init(Cipher.DECRYPT_MODE, getAESKeyFromKS(),
                        new GCMParameterSpec(128, Base64.decode(publicIV, Base64.DEFAULT)));

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            c = Cipher.getInstance(AES_MODE);
            try {
                c.init(Cipher.DECRYPT_MODE, getSecretKey(context),
                        new GCMParameterSpec(128, Base64.decode(publicIV, Base64.DEFAULT)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String encrypted = getStringFromSharedPrefs(context, key);
        if (TextUtils.isEmpty(encrypted)) return null;
        byte[] decodedValue = Base64.decode(encrypted.getBytes("UTF-8"), Base64.DEFAULT);
        byte[] decryptedVal = c.doFinal(decodedValue);
        return new String(decryptedVal);
    }

    private String getStringFromSharedPrefs(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        return prefs.getString(key, null);
    }

    private void setStringToSharedPrefs(Context context, String key, String value) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(key, value);
        edit.apply();
    }
}
