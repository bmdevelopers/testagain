package co.bytemark.data.sdk;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import co.bytemark.data.DeviceId;
import co.bytemark.data.DeviceInfo;
import co.bytemark.data.entity.model.config.Conf;
import co.bytemark.data.util.Serializer;

public class Bytemark {
    public static final String LOGOUT = "LOGOUT";
    public static final String LOGIN = "LOGIN";
    public static final String ACTION_UPDATE_DEVICE_APP_INSTALLATIONS = "ACTION_UPDATE_DEVICE_APP_INSTALLATIONS";

    private static final String OPERATING_SYSTEM_ID = "OperatingSystemId";

    private DeviceId deviceId;
    private Serializer serializer;
    private BytemarkStore bytemarkStore;

    private Bytemark() {}

    /**
     * Returns the Bytemark singleton.
     */
    public static Bytemark sharedInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        static final Bytemark INSTANCE = new Bytemark();
    }

    public synchronized Bytemark init(final Context context) {
        if (context == null)
            throw new IllegalArgumentException("valid context is required.");

        serializer = new Serializer();
        bytemarkStore = new BytemarkStore(context);
        deviceId = new DeviceId(context, bytemarkStore);

        String operatingSystemId, tempOperatingSystemId;

        if (BytemarkUDID1.getInstance(context).isDefaultUDIDSource()) {
            operatingSystemId = BytemarkUDID1.getInstance(context).getUDID();
        } else {
            operatingSystemId = deviceId.getId();

            if (operatingSystemId == null) {
                operatingSystemId = BytemarkUDID1.getInstance(context).getUDID();
                BytemarkUDID1.getInstance(context).setDefaultUDIDSource(context, true);
            }
        }

        try {
            BytemarkKeyStore keyStore = BytemarkKeyStore.getInstance(context);
            tempOperatingSystemId = keyStore.decrypt(context, OPERATING_SYSTEM_ID);

            if (TextUtils.isEmpty(tempOperatingSystemId)) {
                keyStore.encrypt(context, OPERATING_SYSTEM_ID, operatingSystemId);
            }

            operatingSystemId = keyStore.decrypt(context, OPERATING_SYSTEM_ID);

        } catch (Exception e) {
            e.printStackTrace();
        }

        String appVersionKey = context.getPackageName() + ".version";

        String savedAppVersion = bytemarkStore.getPreference(appVersionKey);

        String currentAppVersion = DeviceInfo.getCurrentAppVersion(context);

        if (savedAppVersion == null || (!currentAppVersion.equals(savedAppVersion))) {
            if (SDK.isUserSignedIn()) {
                final Intent intent = new Intent(ACTION_UPDATE_DEVICE_APP_INSTALLATIONS);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        }

        return this;
    }

    public void setFeaturesConfig(String featuresFileName) {
        if (featuresFileName == null) {
            throw new IllegalArgumentException("valid features file name is required.");
        }
        final String fileContent = bytemarkStore.readFileContent(featuresFileName);
        final Conf conf = serializer.deserialize(fileContent, Conf.class);
        if (conf == null) {
            throw new IllegalArgumentException("valid conf is required.");
        } else {
            SDK.setConf(conf);
        }

        final String displayName = conf.getOrganization().getDisplayName();
        if (displayName == null) {
            throw new IllegalArgumentException("valid displayName is required.");
        } else {
            SDK.setOrganizationName(displayName);
        }

        final String clientId = conf.getClients().getCustomerMobile().getClient().getClientId();
        if (clientId == null) {
            throw new IllegalArgumentException("valid clientId is required.");
        } else {
            SDK.setClientId(clientId);
        }

        final String overtureUrl = conf.getDomain().getOvertureUrl();
        if (overtureUrl == null) {
            throw new IllegalArgumentException("valid overtureUrl is required.");
        } else {
            SDK.setOvertureUrl(overtureUrl.endsWith("/") ? overtureUrl : overtureUrl + "/");
        }

        final String accountsUrl = conf.getDomain().getAccountsUrl();
        if (accountsUrl == null) {
            throw new IllegalArgumentException("valid accountsUrl is required.");
        } else {
            SDK.setAccountUrl(accountsUrl.endsWith("/") ? accountsUrl : accountsUrl + "/");
        }
        final String redirectUrl = SDK.getAccountUrl() + "oauth/client/callback/";
        SDK.setRedirectUrl(redirectUrl);
    }
}
