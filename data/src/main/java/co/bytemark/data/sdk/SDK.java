package co.bytemark.data.sdk;

import java.util.Calendar;

import co.bytemark.data.entity.model.config.Conf;

public class SDK {
    private static Conf conf;
    private static String organizationName;
    private static String clientId;
    private static String overtureUrl;
    private static String accountUrl;
    private static String redirectUrl;
    private static Calendar lastAccessTime;
    private static String OAuthToken;
    private static String appInstallationId;

    public static synchronized boolean isUserSignedIn() {
        return OAuthToken != null;
    }

    public static synchronized void setConf(Conf varConf) {
        conf = varConf;
    }

    public static synchronized Conf getConf() {
        return conf;
    }

    public static synchronized void setOrganizationName(String varOrganizationName) {
        organizationName = varOrganizationName;
    }

    public static synchronized String getOrganizationName() {
        return organizationName;
    }

    public static synchronized void setClientId(String varClientId) {
        clientId = varClientId;
    }

    public static synchronized String getClientId() {
        return clientId;
    }

    public static synchronized void setOvertureUrl(String varOvertureUrl) {
        overtureUrl = varOvertureUrl.endsWith("/") ? varOvertureUrl : varOvertureUrl + "/";
    }

    public static synchronized String getOvertureUrl() {
        return overtureUrl;
    }

    public static synchronized void setAccountUrl(String varAccountUrl) {
        accountUrl = varAccountUrl.endsWith("/") ? varAccountUrl : varAccountUrl + "/";
    }

    public static synchronized String getAccountUrl() {
        return accountUrl;
    }

    public static synchronized void setRedirectUrl(String varRedirectUrl) {
        redirectUrl = varRedirectUrl.endsWith("/") ? varRedirectUrl : varRedirectUrl + "/";
    }

    public static synchronized String getRedirectUrl() {
        return redirectUrl;
    }

    public static synchronized void setLastAccessTime(Calendar varLastAccessTime) {
        lastAccessTime = varLastAccessTime;
    }

    public static synchronized Calendar getLastAccessTime() {
        return lastAccessTime;
    }

    public static synchronized void setOAuthToken(String varOAuthToken) {
        OAuthToken = varOAuthToken;
    }

    public static synchronized String getOAuthToken() {
        return OAuthToken;
    }

    public static synchronized void setAppInstallationId(String varAppInstallationId) {
        appInstallationId = varAppInstallationId;
    }

    public static synchronized String getAppInstallationId() {
        return appInstallationId;
    }
}
