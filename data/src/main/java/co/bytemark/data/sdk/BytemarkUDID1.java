package co.bytemark.data.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.provider.Settings.Secure;

import java.math.BigInteger;
import java.security.SecureRandom;

class BytemarkUDID1 {

    private static final String PREFS_NAME = "BytemarkUDID";
    private static final String UDID_PREF_KEY = "udid";
    private static final String DEFAULT_UDID_SOURCE_KEY = "default_udid_source";

    private static BytemarkUDID1 mBytemarkUDID1;

    private String mUdid;
    private boolean mDefaultUDIDSource;

    public synchronized static BytemarkUDID1 getInstance(Context context) {
        if (mBytemarkUDID1 == null) {
            mBytemarkUDID1 = new BytemarkUDID1(context);
        }
        return mBytemarkUDID1;
    }

    private BytemarkUDID1(Context context) {
        // Attempt to read the UDID from SharedPreferences
        SharedPreferences sharedPrefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String udid = sharedPrefs.getString(UDID_PREF_KEY, null);
        mDefaultUDIDSource = sharedPrefs.getBoolean(DEFAULT_UDID_SOURCE_KEY, false);
        // If the UDID is null we should generate one and store it
        if (udid == null) {
            udid = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
            if (udid == null || udid.equals("9774d56d682e549c") || udid.length() < 15) {
                // Generate a new ID if the ANDROID_ID is null, was incorrectly set to "9774d56d682e549c" by the mfg, or is too short
                final SecureRandom random = new SecureRandom();
                udid = new BigInteger(64, random).toString(16);
            }
            final Editor e = sharedPrefs.edit();
            e.putString(UDID_PREF_KEY, udid);
            e.commit();
        }
        mUdid = udid;
        setDefaultUDIDSource(context, true);
    }

    public String getUDID() {
        return mBytemarkUDID1.mUdid;
    }

    protected boolean isDefaultUDIDSource() {
        return mBytemarkUDID1.mDefaultUDIDSource;
    }

    protected void setDefaultUDIDSource(Context context, boolean val) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        final Editor e = sharedPrefs.edit();
        e.putBoolean(DEFAULT_UDID_SOURCE_KEY, val);
        e.commit();
    }

}

