package co.bytemark.data.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class BytemarkStore {

    private final Context context;
    private final SharedPreferences sharedPreferences;

    public BytemarkStore(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("must provide valid context");
        } else {
            this.context = context;
        }
        sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }

    /**
     * Retrieves a preference from local store.
     *
     * @param key the preference key
     */
    public synchronized String getPreference(final String key) {
        return sharedPreferences.getString(key, null);
    }

    /**
     * Adds a preference to local store.
     *
     * @param key   the preference key
     * @param value the preference value, supply null value to remove preference
     */
    public synchronized void setPreference(final String key, final String value) {
        if (value == null) {
            sharedPreferences.edit().remove(key).apply();
        } else {
            sharedPreferences.edit().putString(key, value).apply();
        }
    }

    /**
     * Reads a content from a file.
     * This is an I/O operation and this method executes in the main thread, so it is recommended to
     * perform the operation using another thread.
     *
     * @param fileName The file name to read from.
     * @return A string with the content of the file.
     */
    public synchronized String readFileContent(String fileName) {
        StringBuilder fileContentBuilder = new StringBuilder();
        String stringLine;
        try {
            AssetManager assetManager = context.getAssets();
            InputStream inputStream = assetManager.open(fileName);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            while ((stringLine = bufferedReader.readLine()) != null)
                fileContentBuilder.append(stringLine);
            bufferedReader.close();
            inputStreamReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileContentBuilder.toString();
    }
}
