package co.bytemark.data.payments

import androidx.annotation.NonNull
import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.payments.local.PaymentsLocalEntityStore
import co.bytemark.data.payments.remote.PaymentsRemoteEntityStore
import co.bytemark.domain.model.autoload.Autoload
import co.bytemark.domain.model.autoload.DeleteAutoload
import co.bytemark.domain.model.autoload.WalletCalendarAutoload
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.incomm.IncommBarcodeDetail
import co.bytemark.domain.model.incomm.IncommStoreListResponse
import co.bytemark.domain.model.payments.CreateWalletAutoload
import co.bytemark.domain.repository.PaymentsRepository
import co.bytemark.sdk.model.payment_methods.Wallet
import co.bytemark.sdk.post_body.PostPaymentMethod
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import rx.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PaymentsRepositoryImpl @Inject constructor(
    networkManager: NetworkManager,
    @NonNull @Remote remoteStore: PaymentsRemoteEntityStore,
    @NonNull @Local localStore: PaymentsLocalEntityStore
) : RepositoryImpl<PaymentsRemoteEntityStore, PaymentsLocalEntityStore>(
    networkManager,
    remoteStore,
    localStore
), PaymentsRepository {

    override fun getPayments(): Observable<Data> =
        remoteStore.getPayments()

    override fun getPaymentMethods(organizationUUID: String?): Observable<Data> =
        remoteStore.getPaymentMethods(organizationUUID)

    override fun getPaymentMethodsAsync(): Deferred<BMResponse> =
        remoteStore.getPaymentMethodsAsync()

    override fun addV2PaymentMethodAsync(postPaymentMethod: PostPaymentMethod): Deferred<BMResponse> =
        remoteStore.addV2PaymentMethodAsync(postPaymentMethod)

    override fun addPaymentMethod(postPaymentMethod: PostPaymentMethod): Observable<Data> =
        remoteStore.addPaymentMethod(postPaymentMethod)

    override fun deletePaymentMethod(paymentMethodUuid: String): Observable<BMResponse> =
        remoteStore.deletePaymentMethod(paymentMethodUuid)

    override fun addPayPalAccount(jsonObject: JsonObject): Observable<Data> =
        remoteStore.addPayPalAccount(jsonObject)

    override fun deletePayPalAccount(payPalToken: String): Observable<Data> =
        remoteStore.deletePayPalAccount(payPalToken)

    override fun getWalletLoadMoneyConfiguration(): Observable<Data> =
        remoteStore.getLoadMoneyToWalletConfig()

    override suspend fun getAutoLoadConfiguration(): Response<LoadConfig> =
        remoteStore.getAutoLoadConfiguration()

    override suspend fun getAutoloadForWallet(walletUuid: String): Response<WalletCalendarAutoload> =
        remoteStore.getAutoloadForWallet(walletUuid)

    override suspend fun setAutoloadForWallet(createWalletAutoload: CreateWalletAutoload): Response<Autoload> =
        remoteStore.setAutoloadForWallet(createWalletAutoload)

    override suspend fun updateAutoloadForWallet(createWalletAutoload: CreateWalletAutoload): Response<Autoload> =
        remoteStore.updateAutoloadForWallet(createWalletAutoload)

    override suspend fun deleteAutoloadForWallet(walletUuid: String): Response<DeleteAutoload> =
        remoteStore.deleteAutoloadForWallet(walletUuid)

    override fun createWalletAsync(wallet: Wallet): Deferred<BMResponse> =
        remoteStore.createWalletAsync(wallet)

    override suspend fun getIncommRetailerWithLocation(
        latitude: Double,
        longitude: Double,
        radius: Double,
        orderUuid: String
    ): Response<IncommStoreListResponse> =
        remoteStore.getIncommRetailerWithLocation(latitude, longitude, radius, orderUuid)

    override suspend fun getIncommRetailerWithAddress(
        address: String,
        radius: Double,
        orderUuid: String
    ): Response<IncommStoreListResponse> =
        remoteStore.getIncommRetailerWithAddress(address, radius, orderUuid)

    override suspend fun getIncommBarcodeDetails(orderUuid: String): Response<IncommBarcodeDetail> =
        remoteStore.getIncommBarcodeDetails(orderUuid)
}