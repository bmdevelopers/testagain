package co.bytemark.data.payments.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.autoload.Autoload
import co.bytemark.domain.model.autoload.DeleteAutoload
import co.bytemark.domain.model.autoload.WalletCalendarAutoload
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.incomm.IncommBarcodeDetail
import co.bytemark.domain.model.incomm.IncommStoreListResponse
import co.bytemark.domain.model.payments.CreateWalletAutoload
import co.bytemark.sdk.model.payment_methods.Wallet
import co.bytemark.sdk.post_body.PostPaymentMethod
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import rx.Observable

/**
 * Created by Santosh on 18/11/19.
 */
interface PaymentsRemoteEntityStore : RemoteEntityStore {

    fun getPayments(): Observable<Data>

    fun getPaymentMethods(organizationUUID: String?): Observable<Data>

    fun getPaymentMethodsAsync(): Deferred<BMResponse>

    fun addV2PaymentMethodAsync(postPaymentMethod: PostPaymentMethod): Deferred<BMResponse>

    fun addPayPalAccount(jsonObject: JsonObject): Observable<Data>

    fun deletePaymentMethod(paymentMethodUuid: String): Observable<BMResponse>

    fun addPaymentMethod(postPaymentMethod: PostPaymentMethod): Observable<Data>

    fun deletePayPalAccount(payPalToken: String): Observable<Data>

    fun getLoadMoneyToWalletConfig(): Observable<Data>

    suspend fun getAutoLoadConfiguration(): Response<LoadConfig>

    suspend fun getAutoloadForWallet(walletUuid: String): Response<WalletCalendarAutoload>

    suspend fun setAutoloadForWallet(createWalletAutoload: CreateWalletAutoload): Response<Autoload>

    suspend fun updateAutoloadForWallet(createWalletAutoload: CreateWalletAutoload): Response<Autoload>

    suspend fun deleteAutoloadForWallet(walletUuid: String): Response<DeleteAutoload>

    fun createWalletAsync(wallet: Wallet): Deferred<BMResponse>

    suspend fun getIncommRetailerWithLocation(
        latitude: Double,
        longitude: Double,
        radius: Double,
        orderUuid: String
    ): Response<IncommStoreListResponse>

    suspend fun getIncommRetailerWithAddress(
        address: String,
        radius: Double,
        orderUuid: String
    ): Response<IncommStoreListResponse>

    suspend fun getIncommBarcodeDetails(orderUuid: String): Response<IncommBarcodeDetail>
}