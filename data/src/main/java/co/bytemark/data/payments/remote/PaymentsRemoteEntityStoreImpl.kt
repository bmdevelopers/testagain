package co.bytemark.data.payments.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.autoload.Autoload
import co.bytemark.domain.model.autoload.DeleteAutoload
import co.bytemark.domain.model.autoload.WalletCalendarAutoload
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.incomm.IncommBarcodeDetail
import co.bytemark.domain.model.incomm.IncommStoreListResponse
import co.bytemark.domain.model.payments.CreateWalletAutoload
import co.bytemark.sdk.model.payment_methods.Wallet
import co.bytemark.sdk.post_body.PostPaymentMethod
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import rx.Observable
import javax.inject.Inject

/**
 * Created by Santosh on 18/11/19.
 */

class PaymentsRemoteEntityStoreImpl @Inject constructor(
    application: Application,
    overtureRestApi: OvertureRestApi,
    coroutineOvertureApi: CoroutineOvertureApi
) : OvertureRestApiStore(
    application,
    overtureRestApi,
    coroutineOvertureApi
), PaymentsRemoteEntityStore {

    override fun getPayments(): Observable<Data> =
        overtureRestApi.paymentMethods
            .compose(errorHandlingTransformer)
            .map { it.data }

    override fun getPaymentMethods(organizationUUID: String?): Observable<Data> =
        overtureRestApi.getPaymentMethodsv2(organizationUUID)
            .compose(errorHandlingTransformer)
            .map { it.data }

    override fun getPaymentMethodsAsync(): Deferred<BMResponse> =
        coroutineOvertureApi.getPaymentMethodsAsync()

    override fun addV2PaymentMethodAsync(postPaymentMethod: PostPaymentMethod): Deferred<BMResponse> =
        coroutineOvertureApi.addV2PaymentMethodAsync(postPaymentMethod)

    override fun addPayPalAccount(jsonObject: JsonObject): Observable<Data> =
        overtureRestApi.addPayPalAccount(jsonObject)
            .compose(errorHandlingTransformer)
            .map { it.data }

    override fun deletePaymentMethod(
        paymentMethodUuid: String
    ): Observable<BMResponse> =
        overtureRestApi.deletePaymentMethod(paymentMethodUuid)
            .compose(errorHandlingTransformer)

    override fun addPaymentMethod(
        postPaymentMethod: PostPaymentMethod
    ): Observable<Data> =
        overtureRestApi.addPaymentMethod(postPaymentMethod)

    override fun deletePayPalAccount(payPalToken: String): Observable<Data> =
        overtureRestApi.deletePayPalAccount(payPalToken)

    override fun getLoadMoneyToWalletConfig(): Observable<Data> =
        overtureRestApi.loadMoneyToWalletConfig
            .compose(errorHandlingTransformer)
            .map { it.data }

    override suspend fun getAutoLoadConfiguration(): Response<LoadConfig> =
        coroutineOvertureApi.getAutoLoadConfig()

    override suspend fun getAutoloadForWallet(walletUuid: String): Response<WalletCalendarAutoload> =
        coroutineOvertureApi.getAutoloadForWallet(walletUuid)

    override suspend fun setAutoloadForWallet(createWalletAutoload: CreateWalletAutoload): Response<Autoload> =
        coroutineOvertureApi.setAutoloadForWallet(createWalletAutoload)

    override suspend fun updateAutoloadForWallet(createWalletAutoload: CreateWalletAutoload): Response<Autoload> =
        coroutineOvertureApi.updateAutoloadForWallet(createWalletAutoload)

    override suspend fun deleteAutoloadForWallet(walletUuid: String): Response<DeleteAutoload> =
        coroutineOvertureApi.deleteAutoloadForWallet(walletUuid)

    override fun createWalletAsync(wallet: Wallet): Deferred<BMResponse> =
        coroutineOvertureApi.createWalletAsync(wallet)

    override suspend fun getIncommRetailerWithLocation(
        latitude: Double,
        longitude: Double,
        radius: Double,
        orderUuid: String
    ): Response<IncommStoreListResponse> =
        coroutineOvertureApi.getRetailerWithLocation(latitude, longitude, radius, orderUuid)

    override suspend fun getIncommRetailerWithAddress(
        address: String,
        radius: Double,
        orderUuid: String
    ): Response<IncommStoreListResponse> =
        coroutineOvertureApi.getRetailerWithAddress(address, radius, orderUuid)

    override suspend fun getIncommBarcodeDetails(orderUuid: String): Response<IncommBarcodeDetail> =
        coroutineOvertureApi.getIncommBarcodeDetails(orderUuid)
}