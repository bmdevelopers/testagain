package co.bytemark.data.payments.local;

import co.bytemark.data.data_store.local.LocalEntityStore;

/**
 * Created by Omkar on 12/1/17.
 */

public interface PaymentsLocalEntityStore extends LocalEntityStore {
}
