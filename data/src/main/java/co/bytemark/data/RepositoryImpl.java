package co.bytemark.data;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import co.bytemark.data.annotation.Local;
import co.bytemark.data.annotation.Remote;
import co.bytemark.data.data_store.local.LocalEntityStore;
import co.bytemark.data.data_store.remote.RemoteEntityStore;
import co.bytemark.data.net.manager.NetworkManager;
import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.repository.Repository;

public abstract class RepositoryImpl
        <REMOTE_ENTITY_STORE extends RemoteEntityStore,
                LOCAL_ENTITY_STORE extends LocalEntityStore> implements Repository {

    protected NetworkManager networkManager;

    @Remote
    protected REMOTE_ENTITY_STORE remoteStore;

    @Local
    protected LOCAL_ENTITY_STORE localStore;

    protected final Map<String, UseCase> useCasesMap = new HashMap<>();

    public RepositoryImpl(@NonNull NetworkManager networkManager,
                          @NonNull @Remote REMOTE_ENTITY_STORE remoteStore,
                          @NonNull @Local LOCAL_ENTITY_STORE localStore) {
        this.networkManager = networkManager;
        this.remoteStore = remoteStore;
        this.localStore = localStore;
    }

    @Override
    public void register(UseCase useCase) {
        if (useCase != null) {
            useCasesMap.put(useCase.toString(), useCase);
        }
    }

    @Override
    public void unregister(UseCase useCase) {
        if (useCase != null) {
            useCasesMap.remove(useCase.toString());
        }
    }
}
