package co.bytemark.data.resend_receipt.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.BMResponse
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class ResendReceiptRemoteRemoteEntityStoreImpl @Inject constructor(application: Application, overtureRestApi: OvertureRestApi,
                                                                   coroutineOvertureApi: CoroutineOvertureApi) :
        OvertureRestApiStore(application, overtureRestApi, coroutineOvertureApi), ResendReceiptRemoteEntityStore {

    override fun resendReceiptAsync(orderUUID: String): Deferred<BMResponse> {
        return coroutineOvertureApi.resendReceiptAsync(orderUUID)
    }
}