package co.bytemark.data.resend_receipt.local

import co.bytemark.data.ticket_history.local.TicketHistoryLocalEntityStore
import javax.inject.Inject

class ResendReceiptLocalEntityStoreImpl @Inject constructor() : ResendReceiptLocalEntityStore