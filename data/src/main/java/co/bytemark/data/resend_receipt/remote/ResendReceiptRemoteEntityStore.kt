package co.bytemark.data.resend_receipt.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.common.BMResponse
import kotlinx.coroutines.Deferred

interface ResendReceiptRemoteEntityStore : RemoteEntityStore {
    fun resendReceiptAsync(orderUUID: String): Deferred<BMResponse>
}