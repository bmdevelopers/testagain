package co.bytemark.data.resend_receipt

import androidx.annotation.NonNull
import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.resend_receipt.local.ResendReceiptLocalEntityStore
import co.bytemark.data.resend_receipt.remote.ResendReceiptRemoteEntityStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.repository.ResendReceiptRepository
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class ResendReceiptRepositoryImpl @Inject constructor(@NonNull networkManager: NetworkManager,
                                                      @NonNull @Remote remoteEntityStore: ResendReceiptRemoteEntityStore,
                                                      @NonNull @Local localEntityStore: ResendReceiptLocalEntityStore)
    : RepositoryImpl<ResendReceiptRemoteEntityStore, ResendReceiptLocalEntityStore>(networkManager, remoteEntityStore, localEntityStore), ResendReceiptRepository {

    override fun resendReceiptAsync(orderUUID: String): Deferred<BMResponse> {
        return remoteStore.resendReceiptAsync(orderUUID)
    }
}