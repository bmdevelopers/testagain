package co.bytemark.data.purchase_history

import androidx.annotation.NonNull
import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.purchase_history.local.OrderHistoryLocalEntityStore
import co.bytemark.data.purchase_history.remote.OrderHistoryRemoteEntityStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.repository.OrderHistoryRepository
import kotlinx.coroutines.Deferred
import javax.inject.Inject

/** Created by Santosh on 16/04/19.
 */

class OrderHistoryRepositoryImpl @Inject constructor(@NonNull networkManager: NetworkManager,
                                                     @NonNull @Remote remoteEntityStore: OrderHistoryRemoteEntityStore,
                                                     @NonNull @Local localEntityStore: OrderHistoryLocalEntityStore)
    : RepositoryImpl<OrderHistoryRemoteEntityStore, OrderHistoryLocalEntityStore>(networkManager, remoteEntityStore, localEntityStore), OrderHistoryRepository {

    override fun getReceiptsAsync(perPage: Int, pageNo: Int, sortBy: String, order: String): Deferred<BMResponse> {
        return remoteStore.getReceiptsAsync(perPage, pageNo, sortBy, order)
    }
}