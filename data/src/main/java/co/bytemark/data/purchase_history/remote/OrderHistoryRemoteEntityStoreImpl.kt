package co.bytemark.data.purchase_history.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.sdk.BytemarkSDK
import kotlinx.coroutines.Deferred
import javax.inject.Inject

/** Created by Santosh on 16/04/19.
 */

class OrderHistoryRemoteEntityStoreImpl @Inject constructor(application: Application, overtureRestApi: OvertureRestApi,
                                                            coroutineOvertureApi: CoroutineOvertureApi) :
        OvertureRestApiStore(application, overtureRestApi, coroutineOvertureApi), OrderHistoryRemoteEntityStore {

    override fun getReceiptsAsync(perPage: Int, pageNo: Int, sortBy: String, order: String): Deferred<BMResponse> {
        return coroutineOvertureApi.getReceiptsAsync(perPage,
                pageNo, sortBy, order)
    }
}