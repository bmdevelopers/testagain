package co.bytemark.data.purchase_history.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.common.BMResponse
import kotlinx.coroutines.Deferred

/** Created by Santosh on 16/04/19.
 */

interface OrderHistoryRemoteEntityStore : RemoteEntityStore {

    fun getReceiptsAsync(perPage: Int, pageNo: Int, sortBy: String, order: String): Deferred<BMResponse>
}