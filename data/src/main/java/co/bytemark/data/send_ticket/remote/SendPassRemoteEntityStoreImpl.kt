package co.bytemark.data.send_ticket.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.sdk.model.send_pass.SendPassRequest
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class SendPassRemoteEntityStoreImpl @Inject constructor(
        application: Application,
        overtureRestApi: OvertureRestApi,
        coroutineOvertureApi: CoroutineOvertureApi
) : OvertureRestApiStore(application, overtureRestApi, coroutineOvertureApi),
        SendPassRemoteEntityStore {
    override fun sendPassAsync(passUuid: String, sendPassRequest: SendPassRequest): Deferred<BMResponse> {
        return coroutineOvertureApi.sendPassAsync(passUuid, sendPassRequest)
    }
}