package co.bytemark.data.send_ticket.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.sdk.model.send_pass.SendPassRequest
import kotlinx.coroutines.Deferred

interface SendPassRemoteEntityStore : RemoteEntityStore {
    fun sendPassAsync(passUuid: String, sendPassRequest: SendPassRequest): Deferred<BMResponse>
}