package co.bytemark.data.send_ticket

import androidx.annotation.NonNull
import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.send_ticket.local.SendPassLocalEntityStore
import co.bytemark.data.send_ticket.remote.SendPassRemoteEntityStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.repository.SendPassRepository
import co.bytemark.sdk.model.send_pass.SendPassRequest
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class SendPassRepositoryImpl @Inject constructor(
        @NonNull networkManager: NetworkManager,
        @NonNull @Remote remoteEntityStore: SendPassRemoteEntityStore,
        @NonNull @Local localEntityStore: SendPassLocalEntityStore
) : RepositoryImpl<SendPassRemoteEntityStore, SendPassLocalEntityStore>(
        networkManager,
        remoteEntityStore,
        localEntityStore
), SendPassRepository {

    override suspend fun sendPassAsync(passUuid: String, receiverEmail: String): Deferred<BMResponse> {
        return remoteStore.sendPassAsync(passUuid, SendPassRequest(receiverEmail))
    }

}