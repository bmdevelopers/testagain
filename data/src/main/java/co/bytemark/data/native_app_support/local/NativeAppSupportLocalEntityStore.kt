package co.bytemark.data.native_app_support.local

import co.bytemark.data.data_store.local.LocalEntityStore

interface NativeAppSupportLocalEntityStore : LocalEntityStore