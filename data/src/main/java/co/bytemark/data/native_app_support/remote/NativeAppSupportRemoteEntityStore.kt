package co.bytemark.data.native_app_support.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Response

interface NativeAppSupportRemoteEntityStore : RemoteEntityStore {
    suspend fun sendAppSupportEmailAsync(params: Map<String, String>) : Response<BMResponse>
}