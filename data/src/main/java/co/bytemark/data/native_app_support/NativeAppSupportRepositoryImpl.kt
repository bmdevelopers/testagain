package co.bytemark.data.native_app_support

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.native_app_support.local.NativeAppSupportLocalEntityStore
import co.bytemark.data.native_app_support.remote.NativeAppSupportRemoteEntityStore
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.NativeAppSupportRepository
import javax.inject.Inject

class NativeAppSupportRepositoryImpl @Inject internal constructor(
        networkManager: NetworkManager,
        @Remote cloudStore: NativeAppSupportRemoteEntityStore,
        @Local localStore: NativeAppSupportLocalEntityStore
) : RepositoryImpl<NativeAppSupportRemoteEntityStore, NativeAppSupportLocalEntityStore>(
        networkManager,
        cloudStore,
        localStore
), NativeAppSupportRepository {

    override suspend fun sendAppSupportEmailAsync(params: Map<String, String>): Response<BMResponse> {
        return remoteStore.sendAppSupportEmailAsync(params)
    }
}