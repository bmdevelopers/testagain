package co.bytemark.data.native_app_support.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Response
import javax.inject.Inject

class NativeAppSupportRemoteEntityStoreImpl @Inject internal constructor(
        application: Application,
        overtureRestApi: OvertureRestApi,
        api: CoroutineOvertureApi
) : OvertureRestApiStore(
        application,
        overtureRestApi,
        api
), NativeAppSupportRemoteEntityStore {

    override suspend fun sendAppSupportEmailAsync(params: Map<String, String>): Response<BMResponse> =
            coroutineOvertureApi.sendAppSupportEmailAsync(params)
}