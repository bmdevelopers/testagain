package co.bytemark.data.fare_medium.remote

import android.app.Application
import android.content.Context
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.interactor.fare_medium.TransferBalanceRequestData
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents
import kotlinx.coroutines.Deferred
import rx.Observable
import javax.inject.Inject

class FareMediumRemoteEntityStoreImpl @Inject internal constructor(
        application: Application,
        overtureRestApi: OvertureRestApi,
        coroutineOvertureApi: CoroutineOvertureApi
) : OvertureRestApiStore(
        application,
        overtureRestApi,
        coroutineOvertureApi
), FareMediumRemoteEntityStore {

    private val context: Context = application.applicationContext

    override fun getFareMediums(): Observable<List<FareMedium>> {
        return overtureRestApi.getFareMedium()
                .compose(errorHandlingTransformer)
                .map { bmResponse: BMResponse -> bmResponse.data.fareMediums }
    }

    override fun getFareMediumContents(fareMediumUuid: String): Observable<FareMediumContents> {
        return overtureRestApi.getFareMediumContents(fareMediumUuid)
                .compose(errorHandlingTransformer)
                .map { obj: BMResponse -> obj.data }
                .map { data -> FareMediumContents(data.storedValue, data.fares!!, data.transferTime) }
    }

    override fun getAllVirtualCardsAsync(): Deferred<BMResponse> {
        return coroutineOvertureApi.getAllVirtualCardsAsync()
    }

    override fun transferVirtualCardAsync(fareMediumId: String): Deferred<BMResponse> {
        return coroutineOvertureApi.transferVirtualCardAsync(fareMediumId, Unit)
    }

    override suspend fun updateFareMediumState(fareMediumUuid: String, newState: Int): Response<Any> {
        return coroutineOvertureApi.updateFareMediaState(mapOf("uuid" to fareMediumUuid, "state" to newState.toString()))
    }

    override suspend fun removeFareMedium(fareMediumUuid: String): Response<Any> {
        return coroutineOvertureApi.removeFareMedia(fareMediumUuid)
    }



    override suspend fun getAutoLoadConfig(): Response<LoadConfig> =
            coroutineOvertureApi.getAutoLoadConfig()

    override suspend fun transferBalance(transferBalanceRequestData: TransferBalanceRequestData): Response<Unit> =
            coroutineOvertureApi.transferBalance(transferBalanceRequestData)
}