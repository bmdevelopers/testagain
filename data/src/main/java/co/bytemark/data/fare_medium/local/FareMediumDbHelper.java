package co.bytemark.data.fare_medium.local;

import android.app.Application;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.BARCODE_2D_TYPE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.BARCODE_PAYLOAD_TYPE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.CACHED_BARCODE_VALIDATION_V2_TABLE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.MEDIA_SCHEME;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.MEDIA_TYPE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.PAYLOAD_DATA;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.REFRESH_RATE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.UUID;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumContentsEntry.CACHED_FARES_TABLE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumContentsEntry.CACHED_STORED_VALUE_TABLE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumContentsEntry.FARE_EXPIRATION;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumContentsEntry.FARE_IS_ACTIVE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumContentsEntry.FARE_NAME;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumContentsEntry.FARE_ORG_ID;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumContentsEntry.FARE_SHORT_DESCRIPTION;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumContentsEntry.FARE_UUID;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumContentsEntry.STORED_VALUE_PRICE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.CACHED_FARE_MEDIUM_TABLE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_BARCODE_PAYLOAD;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_CATEGORY;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_DOWNLOAD_TIME;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_ID;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_IMAGE_PATH;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_INSTITUTION_ACCOUNT_ID;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_IS_ACTIVE_FARE_MEDIUM;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_NAME;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_NICKNAME;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_POST_TAX_BALANCE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_PRE_TAX_BALANCE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_PRINTED_CARD_NUMBER;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_STATE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_STORED_VALUE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_TYPE;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry.FARE_MEDIUM_UUID;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumThemeEntry.TABLE_NAME;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumThemeEntry.THEME_ACCENT_COLOR;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumThemeEntry.THEME_BACKGROUND_COLOR;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumThemeEntry.THEME_PRIMARY_TEXT_COLOR;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumThemeEntry.THEME_SECONDARY_TEXT_COLOR;
import static co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumThemeEntry.THEME_UUID;

@Singleton
class FareMediumDbHelper extends SQLiteOpenHelper {
    // LOG
    // 312 - Default Fare Medium with Contents (Stored Value and Fares)
    // 313 - Altered Fare Medium Table to add downloaded time.
    // 314 - Altered Fare medium table to add state
    // 315 - Altered Fare medium table to add institution_account_id
    // 316 - Altered Fare medium table to add image_path and created a new table for theme
    // 319 - added preTax and postTax value fields
    private static final int DATABASE_VERSION = 319;
    public static final String DATABASE_NAME = ".i";
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER = " INTEGER";
    private static final String NOT_NULL = " NOT NULL";
    private static final String COMMA_SEP = ",";

    static class FareMediumTable {
        static final String[] FARE_MEDIUM_ALL_ROW_PROJECTION = {
                FARE_MEDIUM_UUID,
                FARE_MEDIUM_NAME,
                FARE_MEDIUM_NICKNAME,
                FARE_MEDIUM_CATEGORY,
                FARE_MEDIUM_TYPE,
                FARE_MEDIUM_ID,
                FARE_MEDIUM_PRINTED_CARD_NUMBER,
                FARE_MEDIUM_BARCODE_PAYLOAD,
                FARE_MEDIUM_IS_ACTIVE_FARE_MEDIUM,
                FARE_MEDIUM_DOWNLOAD_TIME,
                FARE_MEDIUM_STATE,
                FARE_MEDIUM_INSTITUTION_ACCOUNT_ID,
                FARE_MEDIUM_IMAGE_PATH,
                FARE_MEDIUM_STORED_VALUE,
                FARE_MEDIUM_PRE_TAX_BALANCE,
                FARE_MEDIUM_POST_TAX_BALANCE
        };

        static final String CREATE_CACHED_FARE_MEDIUM_TABLE =
                "CREATE TABLE IF NOT EXISTS " + CACHED_FARE_MEDIUM_TABLE + " (" +
                        FARE_MEDIUM_UUID + TEXT_TYPE + " PRIMARY KEY" + NOT_NULL + COMMA_SEP +
                        FARE_MEDIUM_NAME + TEXT_TYPE + COMMA_SEP +
                        FARE_MEDIUM_NICKNAME + TEXT_TYPE + COMMA_SEP +
                        FARE_MEDIUM_CATEGORY + TEXT_TYPE + COMMA_SEP +
                        FARE_MEDIUM_TYPE + INTEGER + COMMA_SEP +
                        FARE_MEDIUM_ID + TEXT_TYPE + COMMA_SEP +
                        FARE_MEDIUM_PRINTED_CARD_NUMBER + TEXT_TYPE + COMMA_SEP +
                        FARE_MEDIUM_BARCODE_PAYLOAD + TEXT_TYPE + COMMA_SEP +
                        FARE_MEDIUM_IS_ACTIVE_FARE_MEDIUM + INTEGER + COMMA_SEP +
                        FARE_MEDIUM_DOWNLOAD_TIME + " INTEGER DEFAULT 0" + COMMA_SEP +
                        FARE_MEDIUM_STATE + " INTEGER DEFAULT 0 " + COMMA_SEP +
                        FARE_MEDIUM_INSTITUTION_ACCOUNT_ID + TEXT_TYPE + COMMA_SEP +
                        FARE_MEDIUM_IMAGE_PATH + TEXT_TYPE + COMMA_SEP +
                        FARE_MEDIUM_STORED_VALUE + INTEGER+ COMMA_SEP +
                        FARE_MEDIUM_PRE_TAX_BALANCE + INTEGER+ COMMA_SEP +
                        FARE_MEDIUM_POST_TAX_BALANCE + INTEGER+
                        ");";

        static final String CREATE_CACHED_THEME_TABLE = "create table if not exists " + TABLE_NAME + " ("
                + FARE_MEDIUM_UUID + " TEXT,"
                + THEME_UUID + " TEXT,"
                + THEME_BACKGROUND_COLOR + " TEXT,"
                + THEME_ACCENT_COLOR + " TEXT,"
                + THEME_PRIMARY_TEXT_COLOR + " TEXT,"
                + THEME_SECONDARY_TEXT_COLOR + " TEXT "
                + ");";

    }

    static class FareTable {
        private static final String CREATE_CACHED_FARES_TABLE =
                "CREATE TABLE IF NOT EXISTS " + CACHED_FARES_TABLE + " (" +
                        FARE_UUID + TEXT_TYPE + " PRIMARY KEY" + NOT_NULL + COMMA_SEP +
                        FARE_MEDIUM_UUID + TEXT_TYPE + COMMA_SEP +
                        FARE_NAME + TEXT_TYPE + COMMA_SEP +
                        FARE_SHORT_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                        FARE_ORG_ID + TEXT_TYPE + COMMA_SEP +
                        FARE_IS_ACTIVE + INTEGER + COMMA_SEP +
                        FARE_EXPIRATION + TEXT_TYPE +
                        ");";

        static final String[] FARE_ALL_ROW_PROJECTION = {
                FARE_UUID,
                FARE_MEDIUM_UUID,
                FARE_NAME,
                FARE_SHORT_DESCRIPTION,
                FARE_ORG_ID,
                FARE_IS_ACTIVE,
                FARE_EXPIRATION
        };
    }

    static class BarcodeValidationV2Table {
        private static final String CREATE_CACHED_BARCODE_VALIDATION_V2_TABLE =
                "CREATE TABLE IF NOT EXISTS " + CACHED_BARCODE_VALIDATION_V2_TABLE + " (" +
                        UUID + TEXT_TYPE + " PRIMARY KEY" + NOT_NULL + COMMA_SEP +
                        BARCODE_2D_TYPE + TEXT_TYPE + COMMA_SEP +
                        BARCODE_PAYLOAD_TYPE + TEXT_TYPE + COMMA_SEP +
                        REFRESH_RATE + INTEGER + COMMA_SEP +
                        PAYLOAD_DATA + TEXT_TYPE + COMMA_SEP +
                        MEDIA_SCHEME + TEXT_TYPE + COMMA_SEP +
                        MEDIA_TYPE + TEXT_TYPE +
                        ");";
    }

    static class StoredValueTable {
        private static final String CREATE_CACHED_STORED_VALUE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + CACHED_STORED_VALUE_TABLE + " (" +
                        FARE_MEDIUM_UUID + TEXT_TYPE
                        + " PRIMARY KEY" + NOT_NULL + COMMA_SEP +
                        STORED_VALUE_PRICE + TEXT_TYPE +
                        ");";

        static final String[] STORED_VALUE_PROJECTION = {
                FARE_MEDIUM_UUID,
                STORED_VALUE_PRICE
        };
    }

    @Inject
    FareMediumDbHelper(Application application) {
        super(application.getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(FareMediumTable.CREATE_CACHED_FARE_MEDIUM_TABLE);
        sqLiteDatabase.execSQL(FareMediumTable.CREATE_CACHED_THEME_TABLE);
        sqLiteDatabase.execSQL(FareTable.CREATE_CACHED_FARES_TABLE);
        sqLiteDatabase.execSQL(BarcodeValidationV2Table.CREATE_CACHED_BARCODE_VALIDATION_V2_TABLE);
        sqLiteDatabase.execSQL(StoredValueTable.CREATE_CACHED_STORED_VALUE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            if (oldVersion < 314) {
                db.execSQL("ALTER TABLE " + CACHED_FARE_MEDIUM_TABLE + " ADD COLUMN " + FARE_MEDIUM_STATE + " INTEGER");
            }
            if (oldVersion < 315) {
                db.execSQL("ALTER TABLE " + CACHED_FARE_MEDIUM_TABLE + " ADD COLUMN " + FARE_MEDIUM_INSTITUTION_ACCOUNT_ID + " INTEGER");
            }
            if (oldVersion < 316) {
                db.execSQL("ALTER TABLE " + CACHED_FARE_MEDIUM_TABLE + " ADD COLUMN " + FARE_MEDIUM_IMAGE_PATH + " TEXT");
                db.execSQL(FareMediumTable.CREATE_CACHED_THEME_TABLE);
            }
            if (oldVersion < 317) {
                db.execSQL("ALTER TABLE " + CACHED_FARE_MEDIUM_TABLE + " ADD COLUMN " + FARE_MEDIUM_STORED_VALUE + " INTEGER");
            }
            if (oldVersion < 318) {
                db.execSQL(BarcodeValidationV2Table.CREATE_CACHED_BARCODE_VALIDATION_V2_TABLE);
            }
            if(oldVersion < 319){
                db.execSQL("ALTER TABLE " + CACHED_FARE_MEDIUM_TABLE + " ADD COLUMN " + FARE_MEDIUM_PRE_TAX_BALANCE + " INTEGER");
                db.execSQL("ALTER TABLE " + CACHED_FARE_MEDIUM_TABLE + " ADD COLUMN " + FARE_MEDIUM_POST_TAX_BALANCE + " INTEGER");
            }
        }
    }
}
