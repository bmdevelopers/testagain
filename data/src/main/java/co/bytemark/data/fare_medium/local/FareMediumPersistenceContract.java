package co.bytemark.data.fare_medium.local;

import android.provider.BaseColumns;

class FareMediumPersistenceContract {
    static abstract class FareMediumEntry implements BaseColumns {
        static final String CACHED_FARE_MEDIUM_TABLE = "CachedFareMedium";
        static final String FARE_MEDIUM_UUID = "fare_medium_uuid";
        static final String FARE_MEDIUM_NAME = "name";
        static final String FARE_MEDIUM_NICKNAME = "nickname";
        static final String FARE_MEDIUM_CATEGORY = "category";
        static final String FARE_MEDIUM_BARCODE_PAYLOAD = "barcode_payload";
        static final String FARE_MEDIUM_IS_ACTIVE_FARE_MEDIUM = "is_active_faremedium";
        static final String FARE_MEDIUM_DOWNLOAD_TIME = "download_time";
        static final String FARE_MEDIUM_TYPE = "type";
        static final String FARE_MEDIUM_ID = "fare_medium_id";
        static final String FARE_MEDIUM_PRINTED_CARD_NUMBER = "printed_card_number";
        static final String FARE_MEDIUM_STATE = "state";
        static final String FARE_MEDIUM_INSTITUTION_ACCOUNT_ID = "institution_account_id";
        static final String FARE_MEDIUM_IMAGE_PATH = "image_path";
        static final String FARE_MEDIUM_STORED_VALUE = "fare_medium_stored_value";
        static final String FARE_MEDIUM_PRE_TAX_BALANCE = "fare_medium_pre_tax_balance";
        static final String FARE_MEDIUM_POST_TAX_BALANCE = "fare_medium_post_tax_balance";
    }

    static abstract class FareMediumContentsEntry implements BaseColumns {
        static final String CACHED_FARES_TABLE = "CachedFares";
        static final String FARE_UUID = "uuid";
        static final String FARE_NAME = "name";
        static final String FARE_SHORT_DESCRIPTION = "short_description";
        static final String FARE_ORG_ID = "org_id";
        static final String FARE_IS_ACTIVE = "is_active";
        static final String FARE_EXPIRATION = "expiration";

        static final String CACHED_STORED_VALUE_TABLE = "CachedStoredValue";
        static final String STORED_VALUE_PRICE = "stored_value";
    }

    static abstract class FareMediumBarcodeValidationV2Entry implements BaseColumns {
        static final String CACHED_BARCODE_VALIDATION_V2_TABLE = "CachedBarcodeValidationV2";
        static final String UUID = "fare_uuid";
        static final String BARCODE_2D_TYPE = "barcode_type";
        static final String BARCODE_PAYLOAD_TYPE = "payload_type";
        static final String REFRESH_RATE = "refresh_rate";
        static final String PAYLOAD_DATA = "payload_data";
        static final String MEDIA_SCHEME = "media_scheme";
        static final String MEDIA_TYPE = "media_type";
    }

    static abstract class FareMediumThemeEntry implements BaseColumns {
        static final String TABLE_NAME = "CachedThemeTable";
        static final String FARE_MEDIUM_UUID = "fare_medium_uuid";
        static final String THEME_UUID = "theme_uuid";
        static final String THEME_ACCENT_COLOR = "accent_color";
        static final String THEME_BACKGROUND_COLOR = "background_color";
        static final String THEME_PRIMARY_TEXT_COLOR = "primary_text_color";
        static final String THEME_SECONDARY_TEXT_COLOR = "secondary_text_color";
    }
}
