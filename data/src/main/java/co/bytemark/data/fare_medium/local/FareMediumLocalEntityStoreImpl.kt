package co.bytemark.data.fare_medium.local

import android.app.Application
import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase.CONFLICT_REPLACE
import android.text.TextUtils
import androidx.core.util.Pair
import co.bytemark.data.data_store.local.LocalEntityStore
import co.bytemark.data.data_store.local.SQLCipherLocalStore
import co.bytemark.data.fare_medium.local.FareMediumDbHelper.StoredValueTable
import co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumContentsEntry
import co.bytemark.data.fare_medium.local.FareMediumPersistenceContract.FareMediumEntry
import co.bytemark.domain.interactor.fare_medium.TransferBalanceRequestData
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents
import co.bytemark.domain.model.fare_medium.fares.Fare
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.Theme
import co.bytemark.sdk.model.common.BarcodeValidationV2
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import net.sqlcipher.Cursor
import rx.Observable
import rx.functions.Func0
import rx.functions.Func1
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FareMediumLocalEntityStoreImpl @Inject internal constructor(application: Application, private val dbHelper: FareMediumDbHelper) : SQLCipherLocalStore(application), FareMediumLocalEntityStore {

    override fun onOpenRequested() {
        database = try {
            dbHelper.getWritableDatabase(BytemarkSDK.SDKUtility.getAuthToken())
        } catch (e: Exception) {
            Timber.d("Db corrupted, deleted %b and recreating", context.deleteDatabase(FareMediumDbHelper.DATABASE_NAME))
            dbHelper.getWritableDatabase(BytemarkSDK.SDKUtility.getAuthToken())
        }
    }

    override fun getFareMediums(): Observable<List<FareMedium>> {
        return Observable.fromCallable(
                Func0 {
                    open()
                    val sql = String.format("SELECT %s FROM %s", TextUtils.join(LocalEntityStore.COMMA, FareMediumDbHelper.FareMediumTable.FARE_MEDIUM_ALL_ROW_PROJECTION), FareMediumEntry.CACHED_FARE_MEDIUM_TABLE)
                    val cursor = database.rawQuery(sql, null)
                    val fareMediumList = mapToList(cursor, CURSOR_TO_FARE_MEDIUM)
                    (fareMediumList as List<FareMedium>).forEach {
                        val theme = getCachedFareMediumTheme(it.uuid)
                        it.theme = theme
                        it.barcodePayloadV2 = getCachedBarcodeValidationV2(it.uuid)
                    }
                    fareMediumList
                } as Func0<List<FareMedium>>
        )
    }

    override fun saveFareMediums(newFareMediumList: List<FareMedium>): Observable<List<FareMedium>> {
        return Observable.fromCallable { Pair(newFareMediumList, fareMediumsSync) }
                .map { fareMediumPair: Pair<List<FareMedium>, List<FareMedium>> ->
                    val latestData = fareMediumPair.first
                    val staleData = fareMediumPair.second
                    // Clean up data not present in cloud response
                    if (staleData != null) {
                        for (fareMedium in staleData) {
                            if (latestData?.contains(fareMedium) == false) {
                                // Not present, so delete the local one.
                                val id = fareMedium.uuid
                                val success = deleteFareMediumSync(id)
                                Timber.d("Deleted Fare Medium id :%s : result : %b", id, success)
                            }
                        }
                    }
                    if (latestData != null) {
                        // clearing the theme data table, as it will be updated by new data
                        deleteFareMediumThemeData()
                        for (fareMedium in latestData) {
                            saveFareMediumSync(fareMedium)
                        }
                    }
                    fareMediumsSync
                }
    }

    override fun deleteFareMedia(fareMediumUuid: String): Observable<Boolean> {
        open()
        val selection = FareMediumEntry.FARE_MEDIUM_UUID + " LIKE ?"
        val selectionArgs = arrayOf(fareMediumUuid)
        val fareMedia = Observable.fromCallable { database.delete(FareMediumEntry.CACHED_FARE_MEDIUM_TABLE, selection, selectionArgs) != -1 }
        return Observable.zip(
                fareMedia, deleteFareMediumContents(fareMediumUuid)
        ) { fareMedia1: Boolean, fareContents: Boolean ->
            Timber.d("fareMedia delete - %s : %b", fareMediumUuid, fareMedia1)
            Timber.d("fareContents delete - %s : %b", fareMediumUuid, fareContents)
            fareMedia1 && fareContents
        }
    }

    override fun getFareMediumContents(fareMediumUuid: String): Observable<FareMediumContents> {
        return Observable.zip(
                getFares(fareMediumUuid),
                getStoredValue(fareMediumUuid)
        ) { fares: List<Fare>, storedValue: String? -> FareMediumContents(storedValue, fares, null) }
    }

    override fun saveFareMediumContents(fareMediumUuid: String, fareMediumContents: FareMediumContents): Observable<FareMediumContents> {
        return Observable.zip(
                saveFares(fareMediumUuid, fareMediumContents.fares),
                saveStoredValue(fareMediumUuid, fareMediumContents.storedValue!!)
        ) { fares: List<Fare>?, storedValue: String? -> FareMediumContents(storedValue, fares!!, fareMediumContents.transferTime) }
    }

    override fun deleteFareMediumContents(fareMediumUuid: String): Observable<Boolean> {
        open()
        val selection = FareMediumEntry.FARE_MEDIUM_UUID + " LIKE ?"
        val selectionArgs = arrayOf(fareMediumUuid)
        val fares = Observable.fromCallable { database.delete(FareMediumContentsEntry.CACHED_FARES_TABLE, selection, selectionArgs) != -1 }
        val storedValue = Observable.fromCallable { database.delete(FareMediumContentsEntry.CACHED_STORED_VALUE_TABLE, selection, selectionArgs) != -1 }
        return Observable.zip(fares, storedValue) { faresResult: Boolean, storedValueResult: Boolean -> faresResult && storedValueResult }
    }

    private fun saveFares(fareMediumUUID: String, fares: List<Fare>): Observable<List<Fare>> {
        open()
        for (fare in fares) {
            val values = ContentValues()
            values.put(FareMediumContentsEntry.FARE_UUID, fare.uuid)
            values.put(FareMediumEntry.FARE_MEDIUM_UUID, fareMediumUUID)
            values.put(FareMediumContentsEntry.FARE_NAME, fare.name)
            values.put(FareMediumContentsEntry.FARE_SHORT_DESCRIPTION, fare.shortDescription)
            values.put(FareMediumContentsEntry.FARE_ORG_ID, fare.orgId)
            values.put(FareMediumContentsEntry.FARE_IS_ACTIVE, fare.isActive)
            values.put(FareMediumContentsEntry.FARE_EXPIRATION, fare.expiration)
            database.insertWithOnConflict(FareMediumContentsEntry.CACHED_FARES_TABLE, null, values, CONFLICT_REPLACE)
        }
        return Observable.just(fares)
    }

    private fun getFares(fareMediumUUID: String): Observable<List<Fare>> {
        return Observable.fromCallable(
                Func0 {
                    open()
                    val cursor = database.rawQuery(
                            "SELECT * FROM " + FareMediumContentsEntry.CACHED_FARES_TABLE + " WHERE " + FareMediumEntry.FARE_MEDIUM_UUID + "=?",
                            arrayOf(fareMediumUUID)
                    )
                    mapToList(cursor, CURSOR_TO_FARE)
                } as Func0<List<Fare>>
        )
    }

    private fun saveStoredValue(fareMediumUUID: String, storedValue: String): Observable<String> {
        return Observable.fromCallable(
                Func0 {
                    val values = ContentValues()
                    values.put(FareMediumEntry.FARE_MEDIUM_UUID, fareMediumUUID)
                    values.put(FareMediumContentsEntry.STORED_VALUE_PRICE, storedValue)
                    open()
                    database.insertWithOnConflict(FareMediumContentsEntry.CACHED_STORED_VALUE_TABLE, null, values, CONFLICT_REPLACE)
                    storedValue
                } as Func0<String>
        )
    }

    private fun getStoredValue(fareMediumUUID: String): Observable<String> {
        val sql = String.format("SELECT %s FROM %s WHERE %s LIKE ?", TextUtils.join(",", StoredValueTable.STORED_VALUE_PROJECTION), FareMediumContentsEntry.CACHED_STORED_VALUE_TABLE, FareMediumEntry.FARE_MEDIUM_UUID)
        return Observable.fromCallable(
                Func0 {
                    open()
                    val cursor = database.rawQuery(sql, arrayOf(fareMediumUUID))
                    mapToOne(cursor) { c: Cursor? -> cursor.getString(cursor.getColumnIndex(FareMediumContentsEntry.STORED_VALUE_PRICE)) }
                } as Func0<String>
        )
    }

    private fun deleteFareMediumThemeData() {
        try {
            open()
            database.delete(
                    FareMediumPersistenceContract.FareMediumThemeEntry.TABLE_NAME,
                    null,
                    null
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun deleteAll() {
        try {
            open()
            database.delete(FareMediumEntry.CACHED_FARE_MEDIUM_TABLE, null, null)
            database.delete(FareMediumContentsEntry.CACHED_FARES_TABLE, null, null)
            database.delete(FareMediumContentsEntry.CACHED_STORED_VALUE_TABLE, null, null)
            database.delete(FareMediumPersistenceContract.FareMediumThemeEntry.TABLE_NAME, null, null)
            context.deleteDatabase(FareMediumDbHelper.DATABASE_NAME)
            close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getCachedBarcodeValidationV2(uuid: String): BarcodeValidationV2? {
        val query = """SELECT 
                |${FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.UUID},
                |${FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.BARCODE_2D_TYPE},
                |${FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.BARCODE_PAYLOAD_TYPE},
                |${FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.REFRESH_RATE},
                |${FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.PAYLOAD_DATA},
                |${FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.MEDIA_SCHEME},
                |${FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.MEDIA_TYPE}
                | FROM
                | ${FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.CACHED_BARCODE_VALIDATION_V2_TABLE} 
                | where ${FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.UUID} =?""".trimMargin()

        open()
        val cursor = database.rawQuery(query, arrayOf(uuid))
        if (cursor.count > 0) {
            return mapToOne(cursor, CURSOR_TO_BARCODE_VALIDATION_V2)
        } else {
            cursor.close()
        }
        return null
    }

    private fun getCachedFareMediumTheme(uuid: String): Theme? {
        val query = """SELECT 
                |${FareMediumPersistenceContract.FareMediumThemeEntry.FARE_MEDIUM_UUID},
                |${FareMediumPersistenceContract.FareMediumThemeEntry.THEME_UUID},
                |${FareMediumPersistenceContract.FareMediumThemeEntry.THEME_ACCENT_COLOR},
                |${FareMediumPersistenceContract.FareMediumThemeEntry.THEME_BACKGROUND_COLOR},
                |${FareMediumPersistenceContract.FareMediumThemeEntry.THEME_PRIMARY_TEXT_COLOR},
                |${FareMediumPersistenceContract.FareMediumThemeEntry.THEME_SECONDARY_TEXT_COLOR}
                | FROM
                | ${FareMediumPersistenceContract.FareMediumThemeEntry.TABLE_NAME} 
                | where ${FareMediumPersistenceContract.FareMediumThemeEntry.FARE_MEDIUM_UUID} =?""".trimMargin()

        open()
        val cursor = database.rawQuery(query, arrayOf(uuid))
        if (cursor.count > 0) {
            return mapToOne<Theme>(cursor, CURSOR_TO_THEME)
        } else {
            cursor.close()
        }
        return null
    }

    private fun saveFareMediumSync(fareMedium: FareMedium) {
        open()
        val values = ContentValues()
        with(values) {
            put(FareMediumEntry.FARE_MEDIUM_UUID, fareMedium.uuid)
            put(FareMediumEntry.FARE_MEDIUM_NAME, fareMedium.name)
            put(FareMediumEntry.FARE_MEDIUM_NICKNAME, fareMedium.nickname)
            put(FareMediumEntry.FARE_MEDIUM_CATEGORY, fareMedium.category)
            put(FareMediumEntry.FARE_MEDIUM_TYPE, fareMedium.type)
            put(FareMediumEntry.FARE_MEDIUM_ID, fareMedium.fareMediumId)
            put(FareMediumEntry.FARE_MEDIUM_PRINTED_CARD_NUMBER, fareMedium.printedCardNumber)
            put(FareMediumEntry.FARE_MEDIUM_BARCODE_PAYLOAD, fareMedium.barcodePayload)
            put(FareMediumEntry.FARE_MEDIUM_IS_ACTIVE_FARE_MEDIUM, fareMedium.activeFareMedium)
            put(FareMediumEntry.FARE_MEDIUM_DOWNLOAD_TIME, System.currentTimeMillis())
            put(FareMediumEntry.FARE_MEDIUM_STATE, fareMedium.state)
            put(FareMediumEntry.FARE_MEDIUM_INSTITUTION_ACCOUNT_ID, fareMedium.institutionAccountId)
            put(FareMediumEntry.FARE_MEDIUM_IMAGE_PATH, fareMedium.imagePath)
            put(FareMediumEntry.FARE_MEDIUM_STORED_VALUE, fareMedium.storedValue)
            put(FareMediumEntry.FARE_MEDIUM_PRE_TAX_BALANCE, fareMedium.preTaxBalance)
            put(FareMediumEntry.FARE_MEDIUM_POST_TAX_BALANCE, fareMedium.postTaxBalance)
        }
        database.insertWithOnConflict(FareMediumEntry.CACHED_FARE_MEDIUM_TABLE, null, values, CONFLICT_REPLACE)

        fareMedium.theme?.let { theme ->
            val themeValues = ContentValues()
            with(themeValues) {
                put(FareMediumPersistenceContract.FareMediumThemeEntry.FARE_MEDIUM_UUID, fareMedium.uuid)
                put(FareMediumPersistenceContract.FareMediumThemeEntry.THEME_UUID, theme.uuid)
                put(FareMediumPersistenceContract.FareMediumThemeEntry.THEME_ACCENT_COLOR, theme.accentColor)
                put(FareMediumPersistenceContract.FareMediumThemeEntry.THEME_BACKGROUND_COLOR, theme.backgroundColor)
                put(FareMediumPersistenceContract.FareMediumThemeEntry.THEME_PRIMARY_TEXT_COLOR, theme.primaryTextColor)
                put(FareMediumPersistenceContract.FareMediumThemeEntry.THEME_SECONDARY_TEXT_COLOR, theme.secondaryTextColor)
            }
            database.insertWithOnConflict(
                    FareMediumPersistenceContract.FareMediumThemeEntry.TABLE_NAME,
                    null, themeValues, CONFLICT_REPLACE
            )
        }

        fareMedium.barcodePayloadV2?.let { payloadV2 ->
            val payloadValue = ContentValues()
            with(payloadValue) {
                put(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.UUID, fareMedium.uuid)
                put(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.BARCODE_2D_TYPE, payloadV2.barcodeType)
                put(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.BARCODE_PAYLOAD_TYPE, payloadV2.payloadType)
                put(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.PAYLOAD_DATA, payloadV2.payloadData)
                put(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.REFRESH_RATE, payloadV2.refreshRate)
                put(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.MEDIA_SCHEME, payloadV2.mediaScheme)
                put(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.MEDIA_TYPE, payloadV2.mediaType)
            }

            database.insertWithOnConflict(
                    FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.CACHED_BARCODE_VALIDATION_V2_TABLE,
                    null, payloadValue, CONFLICT_REPLACE
            )
        }
    }

    private val fareMediumsSync: List<FareMedium>
        get() = getFareMediums().toBlocking().first()

    private fun deleteFareMediumSync(fareMediumId: String): Boolean {
        return deleteFareMedia(fareMediumId).toBlocking().first()
    }

    override fun getAllVirtualCardsAsync(): Deferred<BMResponse> {
        return GlobalScope.async { BMResponse() }
    }

    override fun transferVirtualCardAsync(fareMediumId: String): Deferred<BMResponse> {
        return GlobalScope.async { BMResponse() }
    }

    override suspend fun updateFareMediumState(fareMediumUuid: String, newState: Int): Response<Any> {
        return Response(data = Any())
    }

    override suspend fun removeFareMedium(fareMediumUuid: String): Response<Any> {
        return Response(data = Any())
    }

    override suspend fun getAutoLoadConfig(): Response<LoadConfig> {
        return Response(data = LoadConfig())
    }

    override suspend fun transferBalance(transferBalanceRequestData: TransferBalanceRequestData): Response<Unit> {
        return Response(data = Unit)
    }

    companion object {
        private val CURSOR_TO_FARE_MEDIUM = Func1 { cursor: Cursor ->
            val uuid = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_UUID))
            val name = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_NAME))
            val nickname = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_NICKNAME))
            val category = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_CATEGORY))
            val type = cursor.getInt(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_TYPE))
            val fareMediumId = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_ID))
            val printedCardNumber = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_PRINTED_CARD_NUMBER))
            val barcodePayload = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_BARCODE_PAYLOAD))
            val isActiveFareMedium = cursor.getInt(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_IS_ACTIVE_FARE_MEDIUM)) == 1
            val lastDownloadTime = cursor.getLong(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_DOWNLOAD_TIME))
            val state = cursor.getLong(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_STATE))
            val institutionId = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_INSTITUTION_ACCOUNT_ID))
            val imagePath = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_IMAGE_PATH))
            val storedValue = cursor.getInt(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_STORED_VALUE))
            val preTaxValue = cursor.getInt(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_PRE_TAX_BALANCE))
            val postTaxValue = cursor.getInt(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_POST_TAX_BALANCE))

            FareMedium(
                    uuid, name, nickname, category, type, fareMediumId, barcodePayload, null,
                    isActiveFareMedium, lastDownloadTime, null, storedValue, null,
                    null, printedCardNumber, state.toInt(), institutionId, imagePath, preTaxBalance = preTaxValue, postTaxBalance = postTaxValue
            )
        }

        private val CURSOR_TO_FARE = Func1 { cursor: Cursor ->
            val uuid = cursor.getString(cursor.getColumnIndex(FareMediumContentsEntry.FARE_UUID))
            val fareMediumUUID = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_UUID))
            val name = cursor.getString(cursor.getColumnIndex(FareMediumContentsEntry.FARE_NAME))
            val shortDescription = cursor.getString(cursor.getColumnIndex(FareMediumContentsEntry.FARE_SHORT_DESCRIPTION))
            val orgId = cursor.getString(cursor.getColumnIndex(FareMediumContentsEntry.FARE_ORG_ID))
            val isActive = cursor.getInt(cursor.getColumnIndex(FareMediumContentsEntry.FARE_IS_ACTIVE)) == 1
            val expiration = cursor.getString(cursor.getColumnIndex(FareMediumContentsEntry.FARE_EXPIRATION))
            Fare(
                uuid,
                fareMediumUUID,
                name,
                shortDescription,
                orgId,
                isActive,
                expiration,
                null,
                null
            )
        }

        private val CURSOR_TO_FARE_MEDIUM_WITH_VALUE = Func1 { cursor: Cursor ->
            val uuid = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_UUID))
            val name = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_NAME))
            val nickname = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_NICKNAME))
            val category = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_CATEGORY))
            val type = cursor.getInt(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_TYPE))
            val fareMediumId = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_ID))
            val printedCardNumber = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_PRINTED_CARD_NUMBER))
            val barcodePayload = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_BARCODE_PAYLOAD))
            val isActiveFareMedium = cursor.getInt(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_IS_ACTIVE_FARE_MEDIUM)) == 1
            val lastDownloadTime = cursor.getLong(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_DOWNLOAD_TIME))
            val state = cursor.getLong(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_STATE))
            val institutionId = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_INSTITUTION_ACCOUNT_ID))
            val storedValue = cursor.getString(cursor.getColumnIndex(FareMediumContentsEntry.STORED_VALUE_PRICE)).toInt()
            val imagePath = cursor.getString(cursor.getColumnIndex(FareMediumEntry.FARE_MEDIUM_IMAGE_PATH))

            FareMedium(
                    uuid, name, nickname, category, type, fareMediumId, barcodePayload,
                    null, isActiveFareMedium, lastDownloadTime, null,
                    storedValue, null, null, printedCardNumber, state.toInt(), institutionId,
                    imagePath
            )
        }

        private val CURSOR_TO_THEME = Func1 { cursor: Cursor ->
            val themeUuid = cursor.getString(cursor.getColumnIndex(FareMediumPersistenceContract.FareMediumThemeEntry.THEME_UUID))
            val accentColor = cursor.getString(cursor.getColumnIndex(FareMediumPersistenceContract.FareMediumThemeEntry.THEME_ACCENT_COLOR))
            val backgroundColor = cursor.getString(cursor.getColumnIndex(FareMediumPersistenceContract.FareMediumThemeEntry.THEME_BACKGROUND_COLOR))
            val primaryTextColor = cursor.getString(cursor.getColumnIndex(FareMediumPersistenceContract.FareMediumThemeEntry.THEME_PRIMARY_TEXT_COLOR))
            val secondaryTextColor = cursor.getString(cursor.getColumnIndex(FareMediumPersistenceContract.FareMediumThemeEntry.THEME_SECONDARY_TEXT_COLOR))
            Theme(themeUuid, accentColor, backgroundColor, primaryTextColor, secondaryTextColor)
        }

        private val CURSOR_TO_BARCODE_VALIDATION_V2 = Func1 { cursor: Cursor ->
            val uuid = cursor.getString(cursor.getColumnIndex(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.UUID))
            val barcodeType2d = cursor.getString(cursor.getColumnIndex(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.BARCODE_2D_TYPE))
            val payloadType = cursor.getString(cursor.getColumnIndex(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.BARCODE_PAYLOAD_TYPE))
            val refreshRate = cursor.getString(cursor.getColumnIndex(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.REFRESH_RATE))
            val payloadData = cursor.getString(cursor.getColumnIndex(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.PAYLOAD_DATA))
            val mediaScheme = cursor.getString(cursor.getColumnIndex(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.MEDIA_SCHEME))
            val mediaType = cursor.getString(cursor.getColumnIndex(FareMediumPersistenceContract.FareMediumBarcodeValidationV2Entry.MEDIA_TYPE))
            BarcodeValidationV2(mediaScheme, mediaType).apply {
                this.barcodeType = barcodeType2d
                this.payloadData = payloadData
                this.refreshRate = refreshRate
            }
        }
    }
}
