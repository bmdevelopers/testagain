package co.bytemark.data.fare_medium

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.fare_medium.local.FareMediumLocalEntityStore
import co.bytemark.data.fare_medium.remote.FareMediumRemoteEntityStore
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.domain.interactor.fare_medium.TransferBalanceRequestData
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents
import co.bytemark.domain.repository.FareMediumRepository
import kotlinx.coroutines.Deferred
import rx.Observable
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FareMediumRepositoryImpl @Inject constructor(
        networkManager: NetworkManager,
        @Remote remoteStore: FareMediumRemoteEntityStore,
        @Local localStore: FareMediumLocalEntityStore
) : RepositoryImpl<FareMediumRemoteEntityStore, FareMediumLocalEntityStore>(
        networkManager,
        remoteStore,
        localStore
), FareMediumRepository {

    private fun saveFareMediums(fareMediums: List<FareMedium>): Observable<List<FareMedium>> {
        return localStore.saveFareMediums(fareMediums)
    }

    override fun getFareMediums(loadFromLocalStore: Boolean): Observable<List<FareMedium>> {
        return if (networkManager.isNetworkAvailable && loadFromLocalStore.not()) {
            remoteStore
                    .getFareMediums()
                    .doOnNext { fareMediums: List<FareMedium?> -> Timber.d("FareMedium from API: %d", fareMediums.size) }
                    .flatMap { fareMediums: List<FareMedium> -> saveFareMediums(fareMediums) }
        } else {
            localStore.getFareMediums()
                    .doOnNext { fareMediums: List<FareMedium?> -> Timber.d("FareMedium from Local: %d", fareMediums.size) }
        }
    }

    override fun getFareMediumContents(fareMediumUuid: String, loadFromLocalStore: Boolean): Observable<FareMediumContents> {
        return if (networkManager.isNetworkAvailable && loadFromLocalStore.not()) {
            remoteStore.getFareMediumContents(fareMediumUuid)
                    .flatMap { fareMediumContents ->
                        localStore
                                .saveFareMediumContents(fareMediumUuid, fareMediumContents)
                                .doOnNext { Timber.d("FareMediumContents after saving from API: %s", fareMediumContents.toString()) }
                    }
        } else {
            localStore.getFareMediumContents(fareMediumUuid)
                    .doOnNext { fareMediumContentsSaved: FareMediumContents -> Timber.d("FareMediumContents after saving from storage: %s", fareMediumContentsSaved.toString()) }
        }
    }

    override fun getAllVirtualCardsAsync(): Deferred<BMResponse> {
        return remoteStore.getAllVirtualCardsAsync()
    }

    override fun transferVirtualCardAsync(fareMediumId: String): Deferred<BMResponse> {
        return remoteStore.transferVirtualCardAsync(fareMediumId)
    }

    override suspend fun updateFareMediumState(fareMediumUuid: String, newState: Int): Response<Any> {
        return remoteStore.updateFareMediumState(fareMediumUuid, newState)
    }

    override suspend fun removeFareMedium(fareMediumUuid: String): Response<Any> {
        return remoteStore.removeFareMedium(fareMediumUuid)
    }

    override suspend fun getAutoLoadConfig(): Response<LoadConfig> =
            remoteStore.getAutoLoadConfig()

    override suspend fun transferBalance(transferBalanceRequestData: TransferBalanceRequestData): Response<Unit> =
            remoteStore.transferBalance(transferBalanceRequestData)

}