package co.bytemark.data.fare_medium.local;

import androidx.annotation.NonNull;

import java.util.List;

import co.bytemark.data.data_store.local.LocalEntityStore;
import co.bytemark.data.fare_medium.remote.FareMediumRemoteEntityStore;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents;
import rx.Observable;

public interface FareMediumLocalEntityStore extends FareMediumRemoteEntityStore, LocalEntityStore {
    @NonNull
    Observable<List<FareMedium>> saveFareMediums(@NonNull List<FareMedium> fareMediumList);

    @NonNull
    Observable<Boolean> deleteFareMedia(@NonNull String fareMediumUuid);

    @NonNull
    Observable<FareMediumContents> saveFareMediumContents(@NonNull String fareMediumUuid, @NonNull FareMediumContents fareMediumContents);

    @NonNull
    Observable<Boolean> deleteFareMediumContents(@NonNull String fareMediumUuid);

}
