package co.bytemark.data.notification.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.notification.NotificationResponse

interface NotificationRemoteEntityStore : RemoteEntityStore {
    suspend fun getNotifications() : Response<NotificationResponse>
}