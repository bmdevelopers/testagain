package co.bytemark.data.notification.local

import android.app.Application
import android.content.ContentValues
import android.text.TextUtils
import co.bytemark.data.data_store.local.SQLCipherLocalStore
import co.bytemark.data.notification.local.NotificationPersistenceContract.NotificationEntry
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.notification.Notification
import co.bytemark.domain.model.notification.NotificationResponse
import net.sqlcipher.Cursor
import rx.functions.Func1
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NotificationLocalEntityStoreImpl @Inject internal constructor(
        application: Application,
        private val dbHelper: NotificationDbHelper
) : SQLCipherLocalStore(application.applicationContext), NotificationLocalEntityStore {

    private val notificationMapperFunction: Func1<Cursor, Notification>
    private val projection = arrayOf(
            NotificationEntry.NOTIFICATION_ID,
            NotificationEntry.NOTIFICATION_TYPE,
            NotificationEntry.NOTIFICATION_TITLE,
            NotificationEntry.NOTIFICATION_TEASER,
            NotificationEntry.NOTIFICATION_LINK,
            NotificationEntry.NOTIFICATION_BODY,
            NotificationEntry.NOTIFICATION_SHOW_IN_TILES,
            NotificationEntry.NOTIFICATION_SENDER_UUID,
            NotificationEntry.NOTIFICATION_RECIPIENT_UUID,
            NotificationEntry.NOTIFICATION_TIME_CREATED,
            NotificationEntry.NOTIFICATION_TIME_MODIFIED,
            NotificationEntry.NOTIFICATION_START_DATE,
            NotificationEntry.NOTIFICATION_END_DATE,
            NotificationEntry.NOTIFICATION_READ
    )

    private fun mapCursorToNotification(cursor: Cursor): Notification {
        val id = cursor.getString(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_ID))
        val type = cursor.getString(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_TYPE))
        val title = cursor.getString(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_TITLE))
        val teaser = cursor.getString(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_TEASER))
        val link = cursor.getString(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_LINK))
        val body = cursor.getString(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_BODY))
        val showInTiles =
                cursor.getInt(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_SHOW_IN_TILES)) == 1
        val senderUuid =
                cursor.getString(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_SENDER_UUID))
        val recipientUuid =
                cursor.getString(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_RECIPIENT_UUID))
        val timeCreated =
                cursor.getString(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_TIME_CREATED))
        val timeModified =
                cursor.getString(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_TIME_MODIFIED))
        val startDate =
                cursor.getString(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_START_DATE))
        val endDate =
                cursor.getString(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_END_DATE))
        val read = cursor.getInt(cursor.getColumnIndex(NotificationEntry.NOTIFICATION_READ)) == 1
        return Notification(
                id, type, title, teaser, link, body, showInTiles, senderUuid,
                recipientUuid, timeCreated, timeModified, startDate, endDate, read
        )
    }

    @Synchronized
    override suspend fun saveNotifications(notifications: MutableList<Notification>): MutableList<Notification> {
        synchronized(this) {
            val cachedNotifications = getNotificationSync()
            deleteAll()
            notifications.forEach { remote ->
                cachedNotifications.forEach { local ->
                    if (remote.id == local.id) {
                        remote.read = local.read
                    }
                }
                saveNotificationSync(remote)
            }
            return notifications
        }
    }

    @Synchronized
    private fun saveNotificationSync(notification: Notification): Boolean {
        open()
        val values = ContentValues().apply {
            put(NotificationEntry.NOTIFICATION_ID, notification.id)
            put(NotificationEntry.NOTIFICATION_TYPE, notification.type)
            put(NotificationEntry.NOTIFICATION_TITLE, notification.title)
            put(NotificationEntry.NOTIFICATION_TEASER, notification.teaser)
            put(NotificationEntry.NOTIFICATION_LINK, notification.link)
            put(NotificationEntry.NOTIFICATION_BODY, notification.body)
            put(NotificationEntry.NOTIFICATION_SHOW_IN_TILES, notification.showInTiles)
            put(NotificationEntry.NOTIFICATION_SENDER_UUID, notification.senderUuid)
            put(NotificationEntry.NOTIFICATION_RECIPIENT_UUID, notification.recipientUuid)
            put(NotificationEntry.NOTIFICATION_TIME_CREATED, notification.timeCreated)
            put(NotificationEntry.NOTIFICATION_TIME_MODIFIED, notification.timeModified)
            put(NotificationEntry.NOTIFICATION_START_DATE, notification.startDate)
            put(NotificationEntry.NOTIFICATION_END_DATE, notification.endDate)
            put(NotificationEntry.NOTIFICATION_READ, if (notification.read != null && notification.read) 1 else 0)
        }
        val success = database.insert(NotificationEntry.CACHED_NOTIFICATION_TABLE, null, values)
        return success != -1L
    }

    private fun getNotificationSync(): List<Notification> {
        val sql = String.format(
                "SELECT %s FROM %s",
                TextUtils.join(",", projection),
                NotificationEntry.CACHED_NOTIFICATION_TABLE
        )
        open()
        val cursor = database.rawQuery(sql, null)
        Timber.e("getNotification() " + cursor.count)
        close()
        if (cursor != null)
            return mapToList(cursor, notificationMapperFunction)
        return listOf()
    }

    @Synchronized
    override suspend fun getNotifications(): Response<NotificationResponse> {
        return Response(
                data = NotificationResponse(getNotificationSync())
        )
    }

    override suspend fun markNotificationAsRead(notificationId: String): Response<Boolean> {
        val values = ContentValues().apply {
            put(NotificationEntry.NOTIFICATION_READ, 1)
        }
        val selection = NotificationEntry.NOTIFICATION_ID + " LIKE ?"
        open()
        val updated =
                database.update(
                        NotificationEntry.CACHED_NOTIFICATION_TABLE,
                        values,
                        selection,
                        arrayOf(notificationId)
                ) != -1
        close()
        return Response(
                data = updated
        )
    }

    public override fun deleteAll() {
        try {
            open()
            database.delete(NotificationEntry.CACHED_NOTIFICATION_TABLE, null, null)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            close()
        }
    }

    /**
     * Queries and returns notification immediately on the same thread.
     *
     * @param notificationId Id of notification to be retrieved
     * @return notification
     */
    private fun getNotificationSync(notificationId: String): Notification? {
        return getNotification(notificationId)
    }

    @Synchronized
    private fun getNotification(notificationId: String): Notification? {
        val sql =
                "SELECT * FROM " + NotificationEntry.CACHED_NOTIFICATION_TABLE + " WHERE " +
                        NotificationEntry.NOTIFICATION_ID + "='" + notificationId + "'"
        open()
        val cursor = database.rawQuery(sql, null)
        close()
        return mapToOne(cursor, notificationMapperFunction)
    }

    override fun onOpenRequested() {
        database = dbHelper.getSafeDb(isWritableDatabase = true)
    }

    init {
        notificationMapperFunction = Func1 { cursor: Cursor -> mapCursorToNotification(cursor) }
    }
}