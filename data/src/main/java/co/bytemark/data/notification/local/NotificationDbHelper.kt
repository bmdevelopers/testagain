package co.bytemark.data.notification.local

import android.app.Application
import co.bytemark.data.notification.local.NotificationPersistenceContract.NotificationEntry
import co.bytemark.data.sdk.SDK
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SQLiteException
import net.sqlcipher.database.SQLiteOpenHelper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class NotificationDbHelper @Inject internal constructor(val application: Application) :
        SQLiteOpenHelper(application.applicationContext, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_CACHED_NOTIFICATION_TABLE)
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {}

    fun getSafeDb(isWritableDatabase: Boolean): SQLiteDatabase? {
        val db: SQLiteDatabase?
        db = try {
            if (isWritableDatabase) getWritableDatabase(SDK.getClientId()) else getReadableDatabase(SDK.getClientId())
        } catch (e: SQLiteException) {
            val dbFile = application.getDatabasePath(".c")
            dbFile.delete()
            if (isWritableDatabase) getWritableDatabase(SDK.getClientId()) else getReadableDatabase(SDK.getClientId())
        }
        return db
    }

    companion object {
        private const val DATABASE_VERSION = 312
        private const val DATABASE_NAME = ".c"
        private const val TEXT_TYPE = " TEXT"
        private const val INTEGER = " INTEGER"
        private const val NOT_NULL = " NOT NULL"
        private const val COMMA_SEP = ","
        private const val CREATE_CACHED_NOTIFICATION_TABLE =
                "CREATE TABLE IF NOT EXISTS " + NotificationEntry.CACHED_NOTIFICATION_TABLE + " (" +
                        NotificationEntry.NOTIFICATION_ID + TEXT_TYPE + " PRIMARY KEY" + NOT_NULL + COMMA_SEP +
                    NotificationEntry.NOTIFICATION_TYPE + TEXT_TYPE + COMMA_SEP +
                    NotificationEntry.NOTIFICATION_TITLE + TEXT_TYPE + COMMA_SEP +
                    NotificationEntry.NOTIFICATION_TEASER + TEXT_TYPE + COMMA_SEP +
                    NotificationEntry.NOTIFICATION_LINK + TEXT_TYPE + COMMA_SEP +
                    NotificationEntry.NOTIFICATION_BODY + TEXT_TYPE + COMMA_SEP +
                    NotificationEntry.NOTIFICATION_SHOW_IN_TILES + INTEGER + COMMA_SEP +
                    NotificationEntry.NOTIFICATION_SENDER_UUID + TEXT_TYPE + COMMA_SEP +
                    NotificationEntry.NOTIFICATION_RECIPIENT_UUID + TEXT_TYPE + COMMA_SEP +
                    NotificationEntry.NOTIFICATION_TIME_CREATED + TEXT_TYPE + COMMA_SEP +
                    NotificationEntry.NOTIFICATION_TIME_MODIFIED + TEXT_TYPE + COMMA_SEP +
                    NotificationEntry.NOTIFICATION_START_DATE + TEXT_TYPE + COMMA_SEP +
                    NotificationEntry.NOTIFICATION_END_DATE + TEXT_TYPE + COMMA_SEP +
                    NotificationEntry.NOTIFICATION_READ + TEXT_TYPE +
                    ");"
    }

}