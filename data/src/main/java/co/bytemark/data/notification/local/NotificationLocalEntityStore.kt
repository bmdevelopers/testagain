package co.bytemark.data.notification.local

import co.bytemark.data.data_store.local.LocalEntityStore
import co.bytemark.data.notification.remote.NotificationRemoteEntityStore
import co.bytemark.domain.model.notification.Notification

interface NotificationLocalEntityStore : NotificationRemoteEntityStore, LocalEntityStore {
    suspend fun saveNotifications(notifications: MutableList<Notification>): MutableList<Notification>
    suspend infix fun markNotificationAsRead(notificationId: String) : co.bytemark.domain.model.common.Response<Boolean>
}