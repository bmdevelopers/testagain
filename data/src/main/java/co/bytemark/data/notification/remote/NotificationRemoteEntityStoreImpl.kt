package co.bytemark.data.notification.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.notification.NotificationResponse
import javax.inject.Inject

class NotificationRemoteEntityStoreImpl @Inject internal constructor(application: Application, overtureRestApi: OvertureRestApi, api: CoroutineOvertureApi) : OvertureRestApiStore(application, overtureRestApi, api), NotificationRemoteEntityStore {

    override suspend fun getNotifications() : Response<NotificationResponse> {
        return coroutineOvertureApi.getNotifications()
    }
}