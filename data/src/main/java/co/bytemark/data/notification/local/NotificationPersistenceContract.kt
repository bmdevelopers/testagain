package co.bytemark.data.notification.local

import android.provider.BaseColumns

/**
 * The contract used for the db to save the notification locally.
 */
internal class NotificationPersistenceContract  // To prevent someone from accidentally instantiating the contract class,
// give it an empty constructor.
{
    /* Inner class that defines the table contents */
    internal object NotificationEntry : BaseColumns {
        const val CACHED_NOTIFICATION_TABLE = "CachedNotifications"
        const val NOTIFICATION_ID = "id"
        const val NOTIFICATION_TYPE = "type"
        const val NOTIFICATION_TITLE = "title"
        const val NOTIFICATION_TEASER = "teaser"
        const val NOTIFICATION_LINK = "link"
        const val NOTIFICATION_BODY = "body"
        const val NOTIFICATION_SHOW_IN_TILES = "show_in_tiles"
        const val NOTIFICATION_SENDER_UUID = "sender_uuid"
        const val NOTIFICATION_RECIPIENT_UUID = "recipient_uuid"
        const val NOTIFICATION_TIME_CREATED = "time_created"
        const val NOTIFICATION_TIME_MODIFIED = "time_modified"
        const val NOTIFICATION_START_DATE = "start_date"
        const val NOTIFICATION_END_DATE = "end_date"
        const val NOTIFICATION_READ = "read"
    }
}