package co.bytemark.data.notification

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.notification.local.NotificationLocalEntityStore
import co.bytemark.data.notification.remote.NotificationRemoteEntityStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.notification.Notification
import co.bytemark.domain.model.notification.NotificationResponse
import co.bytemark.domain.repository.NotificationRepository
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class NotificationRepositoryImpl @Inject internal constructor(
    networkManager: NetworkManager,
    @Remote cloudStore: NotificationRemoteEntityStore,
    @Local localStore: NotificationLocalEntityStore
) : RepositoryImpl<NotificationRemoteEntityStore, NotificationLocalEntityStore>(
    networkManager,
    cloudStore,
    localStore
), NotificationRepository {

    override suspend fun saveNotifications(notifications: MutableList<Notification>): MutableList<Notification> =
        localStore.saveNotifications(notifications)


    override suspend fun getNotifications(): Response<NotificationResponse> =
        if (networkManager.isNetworkAvailable) {
            saveNotifications(
                remoteStore.getNotifications().data?.notifications?.toMutableList()
                    ?: mutableListOf()
            ).let {
                Response(data = NotificationResponse(it))
            }
        } else {
            localStore.getNotifications()
        }

    override suspend fun markNotificationAsRead(notificationId: String) =
        localStore markNotificationAsRead notificationId
}