package co.bytemark.data.data_store.local;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Func1;
import timber.log.Timber;

/**
 * Created by Arunkumar on 04/05/17.
 */
public abstract class SQLCipherLocalStore extends LocalEntityStoreImpl {

    protected SQLiteDatabase database;

    public SQLCipherLocalStore(@NonNull Context context) {
        super(context.getApplicationContext());
    }

    protected static <T> List<T> mapToList(@Nullable Cursor cursor, @NonNull Func1<Cursor, T> mapper) {
        final List<T> items = new ArrayList<>();
        if (cursor == null) return items;
        if (cursor.getCount() == 0) {
            cursor.close();
            return items;
        }
        try {
            while (cursor.moveToNext()) {
                items.add(mapper.call(cursor));
            }
        } finally {
            cursor.close();
        }
        return items;
    }

    protected static <T> T mapToOne(@Nullable Cursor cursor, @NonNull Func1<Cursor, T> mapper) {
        if (cursor == null) {
            Timber.d("User photo cursor is null");
            return null;
        }
        if (cursor.getCount() == 0) {
            cursor.close();
            return null;
        }
        cursor.moveToFirst();
        try {
            cursor.moveToFirst();
            return mapper.call(cursor);
        } catch (Exception e) {
            Timber.e(e);
            return null;
        } finally {
            cursor.close();
        }
    }

    private synchronized boolean isOpen() {
        return database != null && database.isOpen();
    }

    public synchronized void open() {
        if (!isOpen()) {
            onOpenRequested();
        }
    }

    protected abstract void onOpenRequested();

    public synchronized void close() {
        if (isOpen()) {
            database.close();
        }
    }
}
