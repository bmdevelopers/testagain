package co.bytemark.data.data_store.local;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import co.bytemark.data.sdk.SDK;

import static co.bytemark.data.sdk.Bytemark.LOGIN;
import static co.bytemark.data.sdk.Bytemark.LOGOUT;

public abstract class LocalEntityStoreImpl {

    protected final Context context;

    public LocalEntityStoreImpl(@NonNull Context context) {
        this.context = context;
        final IntentFilter logoutIntentFilter = new IntentFilter(LOGOUT);
        LocalBroadcastManager.getInstance(context).registerReceiver(logoutReceiver, logoutIntentFilter);

        final IntentFilter loginIntentFilter = new IntentFilter(LOGIN);
        LocalBroadcastManager.getInstance(context).registerReceiver(loginReceiver, loginIntentFilter);
    }

    @SuppressWarnings("FieldCanBeLocal")
    private final BroadcastReceiver logoutReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(LOGOUT)) {
                SDK.setOAuthToken(null);
                deleteAll();
//                .subscribeOn(Schedulers.computation())
//                        .toSingle()
//                        .doOnSuccess(result -> Timber.d("Deletion status: " + result))
//                        .doOnError(throwable -> Timber.e("Error deleting database: ", throwable))
//                        .subscribe();
            }
        }
    };

    @SuppressWarnings("FieldCanBeLocal")
    private final BroadcastReceiver loginReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(LOGIN)) {
                final String oAuthToken = intent.getStringExtra("OAUTH_TOKEN");
                SDK.setOAuthToken(oAuthToken);
//                deleteAll().subscribeOn(Schedulers.computation())
//                        .toSingle()
//                        .doOnSuccess(result -> Timber.d("Deletion status: " + result))
//                        .doOnError(throwable -> Timber.e("Error deleting database: ", throwable))
//                        .subscribe();
            }
        }
    };

    protected String getAuthToken() {
        return SDK.getOAuthToken();
    }

    protected abstract void deleteAll();
}
