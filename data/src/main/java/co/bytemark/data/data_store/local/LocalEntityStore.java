package co.bytemark.data.data_store.local;

import co.bytemark.data.data_store.remote.RemoteEntityStore;

public interface LocalEntityStore extends RemoteEntityStore {
    String COMMA = ",";
}
