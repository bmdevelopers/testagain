package co.bytemark.data.private_key;

import androidx.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import co.bytemark.data.RepositoryImpl;
import co.bytemark.data.annotation.Local;
import co.bytemark.data.annotation.Remote;
import co.bytemark.data.net.manager.NetworkManager;
import co.bytemark.data.private_key.local.PrivateKeyLocalEntityStore;
import co.bytemark.data.private_key.remote.PrivateKeyRemoteEntityStore;
import co.bytemark.domain.model.private_key.PrivateKey;
import co.bytemark.domain.repository.PrivateKeyRepository;
import rx.Observable;

/**
 * Created by Omkar on 9/7/17.
 */
@Singleton
public class PrivateKeyRepositoryImpl
        extends RepositoryImpl<PrivateKeyRemoteEntityStore, PrivateKeyLocalEntityStore>
        implements PrivateKeyRepository {

    @Inject
    public PrivateKeyRepositoryImpl(@NonNull NetworkManager networkManager,
                                    @NonNull @Remote PrivateKeyRemoteEntityStore remoteStore,
                                    @NonNull @Local PrivateKeyLocalEntityStore localStore) {
        super(networkManager, remoteStore, localStore);
    }

    @NonNull
    @Override
    public Observable<List<PrivateKey>> getPrivateKeys() {
        Observable<List<PrivateKey>> privateKeysObservable;
        if (networkManager.isNetworkAvailable()) {
            privateKeysObservable = localStore.getPrivateKeys()
                    .flatMap(privateKeys -> remoteStore.getPrivateKeys()
                            .flatMap(this::savePrivateKeys));
        } else {
            privateKeysObservable = localStore.getPrivateKeys();
        }
        return privateKeysObservable;
    }

    @NonNull
    @Override
    public Observable<List<PrivateKey>> savePrivateKeys(@NonNull List<PrivateKey> privateKeys) {
        return localStore.savePrivateKeys(privateKeys);
    }
}
