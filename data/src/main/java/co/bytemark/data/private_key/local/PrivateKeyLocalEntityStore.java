package co.bytemark.data.private_key.local;

import androidx.annotation.NonNull;

import java.util.List;

import co.bytemark.data.data_store.local.LocalEntityStore;
import co.bytemark.domain.model.private_key.PrivateKey;
import rx.Observable;

/**
 * Created by Omkar on 9/7/17.
 */
public interface PrivateKeyLocalEntityStore extends LocalEntityStore {

    @NonNull
    Observable<List<PrivateKey>> getPrivateKeys();

    @NonNull
    Observable<List<PrivateKey>> savePrivateKeys(@NonNull List<PrivateKey> privateKeys);
}
