package co.bytemark.data.private_key.local;

import android.app.Application;
import android.content.ContentValues;
import androidx.annotation.NonNull;

import net.sqlcipher.Cursor;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import co.bytemark.data.data_store.local.SQLCipherLocalStore;
import co.bytemark.domain.model.private_key.PrivateKey;
import co.bytemark.sdk.BytemarkSDK;
import rx.Observable;
import rx.functions.Func0;
import rx.functions.Func1;

import static android.text.TextUtils.join;
import static co.bytemark.data.private_key.local.PrivateKeyPersistenceContract.PrivateKeyEntry.CACHED_PRIVATE_KEY_TABLE;
import static co.bytemark.data.private_key.local.PrivateKeyPersistenceContract.PrivateKeyEntry.ENCRYPTED_PRIVATE_KEY;
import static co.bytemark.data.private_key.local.PrivateKeyPersistenceContract.PrivateKeyEntry.ID;
import static co.bytemark.data.private_key.local.PrivateKeyPersistenceContract.PrivateKeyEntry.IV;
import static co.bytemark.data.private_key.local.PrivateKeyPersistenceContract.PrivateKeyEntry.VALID_FROM;
import static co.bytemark.data.private_key.local.PrivateKeyPersistenceContract.PrivateKeyEntry.VALID_TO;
import static java.lang.String.format;

/**
 * Created by Omkar on 9/7/17.
 */
@Singleton
public class PrivateKeyLocalEntityStoreImpl extends SQLCipherLocalStore
        implements PrivateKeyLocalEntityStore {

    @NonNull
    private final PrivateKeyDbHelper dbHelper;

    @NonNull
    private Func1<Cursor, PrivateKey> privateKeyMapperFunction;

    private final String[] projection = {
            ID,
            VALID_FROM,
            VALID_TO,
            ENCRYPTED_PRIVATE_KEY,
            IV
    };

    @Inject
    PrivateKeyLocalEntityStoreImpl(@NonNull Application application, @NonNull PrivateKeyDbHelper dbHelper) {
        super(application.getApplicationContext());
        this.dbHelper = dbHelper;
        privateKeyMapperFunction = this::mapCursorToPrivateKey;
    }

    private PrivateKey mapCursorToPrivateKey(Cursor cursor) {
        String id = cursor.getString(cursor.getColumnIndex(ID));
        String validFrom = cursor.getString(cursor.getColumnIndex(VALID_FROM));
        String validTo = cursor.getString(cursor.getColumnIndex(VALID_TO));
        String encryptedPrivateKey = cursor.getString(cursor.getColumnIndex(ENCRYPTED_PRIVATE_KEY));
        String iv = cursor.getString(cursor.getColumnIndex(IV));
        return new PrivateKey(id, validFrom, validTo, encryptedPrivateKey, iv);
    }

    @NonNull
    @Override
    public Observable<List<PrivateKey>> getPrivateKeys() {
        final String sql = format("SELECT %s FROM %s", join(",", projection), CACHED_PRIVATE_KEY_TABLE);
        return Observable.fromCallable((Func0<List<PrivateKey>>) () -> {
            open();
            final Cursor cursor = database.rawQuery(sql, null);
            return mapToList(cursor, privateKeyMapperFunction);
        });
    }

    @NonNull
    @Override
    public Observable<List<PrivateKey>> savePrivateKeys(@NonNull List<PrivateKey> privateKeys) {
        open();
        for (final PrivateKey privateKey : privateKeys) {
            final ContentValues values = new ContentValues();
            values.put(ID, privateKey.getId());
            values.put(VALID_FROM, privateKey.getValidFrom());
            values.put(VALID_TO, privateKey.getValidTo());
            values.put(ENCRYPTED_PRIVATE_KEY, privateKey.getEncryptedPrivateKey());
            values.put(IV, privateKey.getIv());
            database.insert(CACHED_PRIVATE_KEY_TABLE, null, values);
        }
        return Observable.just(privateKeys);
    }

    @Override
    protected void onOpenRequested() {
        database = dbHelper.getWritableDatabase(BytemarkSDK.SDKUtility.getAuthToken());
    }

    @Override
    protected void deleteAll() {
        try {
            open();
            String dbPath = database.getPath();
            File dbFile = new File(dbPath);
            database.close();
            dbFile.delete();
//            database.delete(CACHED_PRIVATE_KEY_TABLE, null, null);
//            context.deleteDatabase(DATABASE_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
