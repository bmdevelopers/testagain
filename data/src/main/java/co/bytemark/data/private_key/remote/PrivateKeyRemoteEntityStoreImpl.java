package co.bytemark.data.private_key.remote;

import android.app.Application;
import androidx.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import co.bytemark.data.net.OvertureRestApi;
import co.bytemark.data.net.store.OvertureRestApiStore;
import co.bytemark.data.sdk.SDK;
import co.bytemark.domain.model.private_key.PrivateKey;
import rx.Observable;

/**
 * Created by Omkar on 9/7/17.
 */
public class PrivateKeyRemoteEntityStoreImpl extends OvertureRestApiStore
        implements PrivateKeyRemoteEntityStore {

    @Inject
    protected PrivateKeyRemoteEntityStoreImpl(@NonNull Application application, @NonNull OvertureRestApi overtureRestApi) {
        super(application, overtureRestApi);
    }

    @NonNull
    @Override
    public Observable<List<PrivateKey>> getPrivateKeys() {
        return overtureRestApi.getPrivateKey()
                .compose(errorHandlingTransformer)
                .map(bmResponse -> bmResponse.getData().getPrivateKeys());
    }
}
