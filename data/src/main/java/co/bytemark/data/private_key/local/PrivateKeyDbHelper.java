package co.bytemark.data.private_key.local;

import android.app.Application;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

import static co.bytemark.data.private_key.local.PrivateKeyPersistenceContract.PrivateKeyEntry.CACHED_PRIVATE_KEY_TABLE;
import static co.bytemark.data.private_key.local.PrivateKeyPersistenceContract.PrivateKeyEntry.ENCRYPTED_PRIVATE_KEY;
import static co.bytemark.data.private_key.local.PrivateKeyPersistenceContract.PrivateKeyEntry.ID;
import static co.bytemark.data.private_key.local.PrivateKeyPersistenceContract.PrivateKeyEntry.IV;
import static co.bytemark.data.private_key.local.PrivateKeyPersistenceContract.PrivateKeyEntry.VALID_FROM;
import static co.bytemark.data.private_key.local.PrivateKeyPersistenceContract.PrivateKeyEntry.VALID_TO;

/**
 * Created by Omkar on 9/7/17.
 */
@Singleton
class PrivateKeyDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 312;

    static final String DATABASE_NAME = ".p";

    private static final String TEXT_TYPE = " TEXT";

    private static final String NOT_NULL = " NOT NULL";

    private static final String COMMA_SEP = ",";

    private static final String CREATE_PRIVATE_KEY_TABLE =
            "CREATE TABLE IF NOT EXISTS " + CACHED_PRIVATE_KEY_TABLE + " (" +
                    ID + TEXT_TYPE + " PRIMARY KEY" + NOT_NULL + COMMA_SEP +
                    VALID_FROM + TEXT_TYPE + COMMA_SEP +
                    VALID_TO + TEXT_TYPE + COMMA_SEP +
                    ENCRYPTED_PRIVATE_KEY + TEXT_TYPE + COMMA_SEP +
                    IV + TEXT_TYPE + COMMA_SEP +
                    "UNIQUE (" + ID + ") ON CONFLICT REPLACE" +
                    ");";

    @Inject
    PrivateKeyDbHelper(Application context) {
        super(context.getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_PRIVATE_KEY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
