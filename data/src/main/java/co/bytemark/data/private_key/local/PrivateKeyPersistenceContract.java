package co.bytemark.data.private_key.local;

import android.provider.BaseColumns;

/**
 * The contract used for the db to save the user photo locally.
 */
class PrivateKeyPersistenceContract {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    private PrivateKeyPersistenceContract() {
    }

    static abstract class PrivateKeyEntry implements BaseColumns {
        static final String CACHED_PRIVATE_KEY_TABLE = "CachedPrivateKey";
        static final String ID = "id";
        static final String VALID_FROM = "valid_from";
        static final String VALID_TO = "valid_to";
        static final String ENCRYPTED_PRIVATE_KEY = "encrypted_private_key";
        static final String IV = "iv";
    }
}
