package co.bytemark.data.private_key.remote;

import androidx.annotation.NonNull;

import java.util.List;

import co.bytemark.data.data_store.remote.RemoteEntityStore;
import co.bytemark.domain.model.private_key.PrivateKey;
import rx.Observable;

/**
 * Created by Omkar on 9/7/17.
 */
public interface PrivateKeyRemoteEntityStore extends RemoteEntityStore {

    @NonNull
    Observable<List<PrivateKey>> getPrivateKeys();
}
