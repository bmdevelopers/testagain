package co.bytemark.data;

import android.content.Context;

import co.bytemark.data.sdk.BytemarkStore;

public class DeviceId {

    private static final String PREFERENCE_KEY_ID_ID = "co.bytemark.DeviceId.id";

    private String id;

    public DeviceId(final Context context, final BytemarkStore store) {
        retrieveId(store);
        init(context);
    }

    private void retrieveId(BytemarkStore store) {
        String storedId = store.getPreference(PREFERENCE_KEY_ID_ID);
        if (storedId != null) {
            this.id = storedId;
        }
    }

    public void init(Context context) {
        if (OpenUDIDAdapter.isOpenUDIDAvailable()) {
            int attemptCount = 0;
            while (!OpenUDIDAdapter.isInitialized() && attemptCount < 3) {
                OpenUDIDAdapter.sync(context);
                android.os.SystemClock.sleep(1000);
                attemptCount++;
            }

        } else {
            throw new IllegalStateException("OpenUDID is not available, please make sure that you have it in your classpath");
        }
    }

    public String getId() {
        if (id == null) {
            id = OpenUDIDAdapter.getOpenUDID();
        }
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

