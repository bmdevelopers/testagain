package co.bytemark.data.filters.remote;

import androidx.annotation.NonNull;

import java.util.List;

import co.bytemark.data.data_store.remote.RemoteEntityStore;
import co.bytemark.domain.model.store.filter.Filter;
import co.bytemark.domain.model.store.filter.FilterResult;
import rx.Observable;

public interface FiltersRemoteEntityStore extends RemoteEntityStore {
    @NonNull
    Observable<List<FilterResult>> getResults(@NonNull Filter filter);
}
