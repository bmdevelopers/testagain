package co.bytemark.data.filters.remote;

import android.app.Application;

import androidx.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import co.bytemark.data.net.OvertureRestApi;
import co.bytemark.data.net.store.OvertureRestApiStore;
import co.bytemark.data.sdk.SDK;
import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.domain.model.store.filter.Filter;
import co.bytemark.domain.model.store.filter.FilterResult;
import retrofit2.Response;
import rx.Observable;

public class FiltersRemoteEntityStoreImpl extends OvertureRestApiStore implements FiltersRemoteEntityStore {

    @Inject
    FiltersRemoteEntityStoreImpl(@NonNull Application application, @NonNull OvertureRestApi overtureRestApi) {
        super(application, overtureRestApi);
    }

    @NonNull
    @Override
    public Observable<List<FilterResult>> getResults(@NonNull Filter filter) {
        Observable<Response<BMResponse>> resultsObservable;
        if (SDK.isUserSignedIn()) {
            resultsObservable = overtureRestApi.getSignedInResults(filter);
        } else {
            resultsObservable = overtureRestApi.getSignedOutResults(filter);
        }

//        resultsObservable = accountRestApi.getResults(filter);

        // TODO: 4/5/17 add error handling later.
        return resultsObservable
                .map(bmResponse -> bmResponse.body().getData().getFilterResults());
    }
}
