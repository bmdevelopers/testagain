package co.bytemark.data.filters.local;

import co.bytemark.data.data_store.local.LocalEntityStore;

public interface FiltersLocalEntityStore extends LocalEntityStore {
}
