package co.bytemark.data.filters;

import androidx.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import co.bytemark.data.RepositoryImpl;
import co.bytemark.data.annotation.Local;
import co.bytemark.data.annotation.Remote;
import co.bytemark.data.filters.local.FiltersLocalEntityStore;
import co.bytemark.data.filters.remote.FiltersRemoteEntityStore;
import co.bytemark.data.net.manager.NetworkManager;
import co.bytemark.domain.model.store.filter.Filter;
import co.bytemark.domain.model.store.filter.FilterResult;
import co.bytemark.domain.repository.FiltersRepository;
import rx.Observable;

@Singleton
public class FiltersRespositoryImpl
        extends RepositoryImpl<FiltersRemoteEntityStore, FiltersLocalEntityStore>
        implements FiltersRepository {

    @Inject
    public FiltersRespositoryImpl(@NonNull NetworkManager networkManager,
                                  @NonNull @Remote FiltersRemoteEntityStore remoteStore,
                                  @NonNull @Local FiltersLocalEntityStore localStore) {
        super(networkManager, remoteStore, localStore);
    }

    @NonNull
    @Override
    public Observable<List<FilterResult>> getResults(@NonNull Filter filter) {
        return remoteStore.getResults(filter);
    }
}
