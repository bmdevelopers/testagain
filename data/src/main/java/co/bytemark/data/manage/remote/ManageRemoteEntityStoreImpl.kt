package co.bytemark.data.manage.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.autoload.Autoload
import co.bytemark.domain.model.autoload.DeleteAutoload
import co.bytemark.domain.model.autoload.FareMediumAutoload
import co.bytemark.domain.model.autoload.PostAutoload
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.fare_medium.TransactionsResponse
import co.bytemark.domain.model.init_fare_capping.InitFareCappingData
import co.bytemark.domain.model.manage.*
import javax.inject.Inject

class ManageRemoteEntityStoreImpl @Inject constructor(
    application: Application,
    overtureRestApi: OvertureRestApi,
    coroutineOvertureApi: CoroutineOvertureApi
) : OvertureRestApiStore(
    application,
    overtureRestApi,
    coroutineOvertureApi
), ManageRemoteEntityStore {

    override suspend fun linkExistingCardsAsync(linkExistingCard: LinkExistingCard?): Response<AddSmartCard> {
        return coroutineOvertureApi.linkExistingCardsAsync(linkExistingCard)
    }

    override suspend fun getFareMediaCategoriesAsync(): Response<FareCategoryResponse> {
        return coroutineOvertureApi.getFareMediaCategoriesAsync()
    }

    override suspend fun createVirtualCardAsync(virtualCard: VirtualCard?): Response<Data> {
        return coroutineOvertureApi.createVirtualCardAsync(virtualCard)
    }

    override suspend fun getTransactionsAsync(
        fareMediaId: String?,
        pageIndex: Int?
    ): Response<TransactionsResponse> =
        coroutineOvertureApi.getTransactionsAsync(fareMediaId, pageIndex)

    override suspend fun saveAutoLoad(postAutoload: PostAutoload): Response<Autoload> =
        coroutineOvertureApi.saveAutoLoad(postAutoload)

    override suspend fun updateAutoLoad(postAutoload: PostAutoload): Response<Autoload> =
        coroutineOvertureApi.updateAutoLoad(postAutoload)

    override suspend fun getAutoLoad(fareMediumId: String): Response<FareMediumAutoload> =
        coroutineOvertureApi.getAutoLoad(fareMediumId)

    override suspend fun deleteAutoLoad(fareMediumId: String): Response<DeleteAutoload> =
        coroutineOvertureApi.deleteAutoLoad(fareMediumId)

    override suspend fun getInitFareCappings(fareMediumId: String): Response<InitFareCappingData> =
        coroutineOvertureApi.getInitFareCappings(fareMediumId)

    override suspend fun getInstitutionList(pageIndex: Int): Response<InstitutionListData> =
            coroutineOvertureApi.getInstitutionList(pageIndex)

    override suspend fun checkUPassEligibility(
        institutionId: String,
        uPassEligibilityRequestData: UPassEligibilityRequestData
    )
            : Response<UPassEligibilityResponseData> =
        coroutineOvertureApi.checkUPassEligibility(institutionId, uPassEligibilityRequestData)
}