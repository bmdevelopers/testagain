package co.bytemark.data.manage

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.manage.local.ManageLocalEntityStore
import co.bytemark.data.manage.remote.ManageRemoteEntityStore
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.domain.model.autoload.Autoload
import co.bytemark.domain.model.autoload.DeleteAutoload
import co.bytemark.domain.model.autoload.FareMediumAutoload
import co.bytemark.domain.model.autoload.PostAutoload
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.fare_medium.TransactionsResponse
import co.bytemark.domain.model.init_fare_capping.InitFareCappingData
import co.bytemark.domain.model.manage.*
import co.bytemark.domain.repository.ManageRepository
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ManageRepositoryImpl @Inject constructor(
    networkManager: NetworkManager,
    @Remote remoteStore: ManageRemoteEntityStore,
    @Local localStore: ManageLocalEntityStore
) : RepositoryImpl<ManageRemoteEntityStore, ManageLocalEntityStore>(
    networkManager,
    remoteStore,
    localStore
), ManageRepository {

    override suspend fun linkExistingCardsAsync(linkExistingCard: LinkExistingCard?): Response<AddSmartCard> {
        return remoteStore.linkExistingCardsAsync(linkExistingCard)
    }

    override suspend fun getFareMediaCategoriesAsync(): Response<FareCategoryResponse> {
        return remoteStore.getFareMediaCategoriesAsync()
    }

    override suspend fun createVirtualCardAsync(virtualCard: VirtualCard?): Response<Data> {
        return remoteStore.createVirtualCardAsync(virtualCard)
    }

    override suspend fun getTransactionsAsync(
        fareMediaId: String?,
        pageIndex: Int?
    ): Response<TransactionsResponse> =
        remoteStore!!.getTransactionsAsync(fareMediaId, pageIndex)

    override suspend fun saveAutoload(postAutoload: PostAutoload): Response<Autoload> =
        remoteStore.saveAutoLoad(postAutoload)

    override suspend fun updateAutoload(postAutoload: PostAutoload): Response<Autoload> =
        remoteStore.updateAutoLoad(postAutoload)

    override suspend fun getAutoload(fareMediumId: String): Response<FareMediumAutoload> =
        remoteStore.getAutoLoad(fareMediumId)

    override suspend fun deleteAutoload(fareMediumId: String): Response<DeleteAutoload> =
        remoteStore.deleteAutoLoad(fareMediumId)

    override suspend fun getInitFareCappings(fareMediumId: String): Response<InitFareCappingData> =
        remoteStore.getInitFareCappings(fareMediumId)

    override suspend fun getInstitutionList(pageIndex: Int): Response<InstitutionListData> =
            remoteStore.getInstitutionList(pageIndex)

    override suspend fun checkUPassEligibility(
        institutionId: String,
        uPassEligibilityRequestData: UPassEligibilityRequestData
    )
            : Response<UPassEligibilityResponseData> =
        remoteStore.checkUPassEligibility(institutionId, uPassEligibilityRequestData)
}