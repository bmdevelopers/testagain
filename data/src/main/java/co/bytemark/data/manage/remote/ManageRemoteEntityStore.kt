package co.bytemark.data.manage.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.autoload.Autoload
import co.bytemark.domain.model.autoload.DeleteAutoload
import co.bytemark.domain.model.autoload.FareMediumAutoload
import co.bytemark.domain.model.autoload.PostAutoload
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.fare_medium.TransactionsResponse
import co.bytemark.domain.model.init_fare_capping.InitFareCappingData
import co.bytemark.domain.model.manage.*

interface ManageRemoteEntityStore : RemoteEntityStore {

    suspend fun linkExistingCardsAsync(linkExistingCard: LinkExistingCard?): Response<AddSmartCard>

    suspend fun getFareMediaCategoriesAsync(): Response<FareCategoryResponse>

    suspend fun createVirtualCardAsync(virtualCard: VirtualCard?): Response<Data>

    suspend fun getTransactionsAsync(fareMediaId: String?, pageIndex: Int?): Response<TransactionsResponse>

    suspend fun saveAutoLoad(postAutoload: PostAutoload): Response<Autoload>

    suspend fun updateAutoLoad(postAutoload: PostAutoload): Response<Autoload>

    suspend fun getAutoLoad(fareMediumId: String): Response<FareMediumAutoload>

    suspend fun deleteAutoLoad(fareMediumId: String): Response<DeleteAutoload>

    suspend fun getInitFareCappings(fareMediumId: String): Response<InitFareCappingData>

    suspend fun getInstitutionList(pageIndex: Int): Response<InstitutionListData>

    suspend fun checkUPassEligibility(institutionId: String, uPassEligibilityRequestData: UPassEligibilityRequestData)
            : Response<UPassEligibilityResponseData>
}