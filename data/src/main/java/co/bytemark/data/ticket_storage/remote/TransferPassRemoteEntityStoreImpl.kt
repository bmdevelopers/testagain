package co.bytemark.data.ticket_storage.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.ticket_storage.TransferPassResponse
import javax.inject.Inject

class TransferPassRemoteEntityStoreImpl @Inject internal constructor(
    application: Application,
    overtureRestApi: OvertureRestApi,
    api: CoroutineOvertureApi
) : OvertureRestApiStore(
    application,
    overtureRestApi,
    api
), TransferPassRemoteEntityStore {
    override suspend fun transferPass(
        passUuid: String,
        destination: String
    ): Response<TransferPassResponse> =
        coroutineOvertureApi.transferPass(passUuid, destination)
}
