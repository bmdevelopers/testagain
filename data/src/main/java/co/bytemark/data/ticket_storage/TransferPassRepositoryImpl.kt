package co.bytemark.data.ticket_storage

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.ticket_storage.local.TransferPassLocalEntityStore
import co.bytemark.data.ticket_storage.remote.TransferPassRemoteEntityStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.ticket_storage.TransferPassResponse
import co.bytemark.domain.repository.TransferPassRepository
import javax.inject.Inject

class TransferPassRepositoryImpl @Inject constructor(
    networkManager: NetworkManager,
    @Remote remoteStore: TransferPassRemoteEntityStore,
    @Local localStore: TransferPassLocalEntityStore
) : RepositoryImpl<TransferPassRemoteEntityStore, TransferPassLocalEntityStore>(
    networkManager,
    remoteStore,
    localStore
), TransferPassRepository {
    override suspend fun transferPass(
        passUuid: String,
        destination: String
    ): Response<TransferPassResponse> =
        remoteStore.transferPass(passUuid, destination)
}