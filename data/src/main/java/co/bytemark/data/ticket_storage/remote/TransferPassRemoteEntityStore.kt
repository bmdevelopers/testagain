package co.bytemark.data.ticket_storage.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.ticket_storage.TransferPassResponse

interface TransferPassRemoteEntityStore : RemoteEntityStore {
    suspend fun transferPass(
        passUuid: String,
        destination: String
    ): Response<TransferPassResponse>
}