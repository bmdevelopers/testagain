package co.bytemark.data.net;

import com.google.gson.JsonObject;

import java.util.Map;

import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.domain.model.common.Data;
import co.bytemark.domain.model.manage.LinkExistingCard;
import co.bytemark.domain.model.manage.VirtualCard;
import co.bytemark.domain.model.order.ApplyPromoCode;
import co.bytemark.domain.model.store.filter.Filter;
import co.bytemark.domain.model.userphoto.UserPhoto;
import co.bytemark.sdk.post_body.CreateOrder;
import co.bytemark.sdk.post_body.MakePayment;
import co.bytemark.sdk.post_body.PostPaymentMethod;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Tag;
import retrofit2.http.Url;
import rx.Observable;

public interface OvertureRestApi {
    /**
     * List of all Notifications for an organization
     *
     * @return {@link Observable}
     * @apiNote https://overture-staging.bytemark.co/notifications?client_id=CLIENT_ID_HERE
     */
    @GET("/notifications")
    Observable<BMResponse> getNotificationsForClient();

    /**
     * List of all Notifications for an organization using oAuthToken when user has Signed In
     *
     * @return {@link Observable}
     * @apiNote https://overture-staging.bytemark.co/notifications?oauth_token=OAUTH_TOKEN_HERE
     */
    @GET("/notifications")
    Observable<BMResponse> getNotificationsForOauth();

    /**
     * List of all Fare Mediums for an organization using oAuthToken when user has Signed In
     *
     * @return {@link Observable}
     * @apiNote https://overture-staging.bytemark.co/faremedia?oauth_token=OAUTH_TOKEN_HERE
     */
    @GET("/faremedia")
    Observable<BMResponse> getFareMedium();

    /**
     * List of all Fare Medium Contents for an organization using oAuthToken when user has Signed In
     *
     * @return {@link Observable}
     * @apiNote https://overture-staging.bytemark.co/fare_medium?oauth_token=OAUTH_TOKEN_HERE
     */
    @GET("/faremedia/{fm_uuid}/fares")
    Observable<BMResponse> getFareMediumContents(@Path("fm_uuid") String fareMediumUUID);


    /**
     * Results for product search
     *
     * @param filter {@link Filter} with all applied filters for product search
     * @return {@link Observable}
     */
    @POST("/v3.0/product/search")
    Observable<Response<BMResponse>> getSignedInResults(@Body Filter filter);

    /**
     * Results for product search
     *
     * @param filter   {@link Filter} with all applied filters for product search
     * @param clientId Client Id of organization
     * @return {@link Observable}
     */
    @POST("/v3.0/product/search")
    Observable<Response<BMResponse>> getSignedOutResults(@Body Filter filter);

    @POST("/user_photos")
    Observable<BMResponse> setUserPhoto(@Body UserPhoto userPhoto);

    @GET("/user_photos")
    Observable<BMResponse> getUserPhoto();

    @GET("/private_keys")
    Observable<BMResponse> getPrivateKey();

    @POST("/offer_code")
    Observable<BMResponse> postVoucherCode(@Body Map<String, String> params);

    @POST("/orders")
    Observable<BMResponse> postPurchaseOrder(@Body CreateOrder createOrder, @QueryMap Map<String, String> queryParameters);

    @POST("/v3.0/orders")
    Observable<BMResponse> postPurchaseOrderForIdeal(@Body CreateOrder createOrder, @QueryMap Map<String, String> queryParameters);

    @PUT("/v3.0/orders/{order_uuid}")
    Observable<BMResponse> makePayment(@Path("order_uuid") String orderUUID, @Body MakePayment makePayment, @Tag String deepLinkJwtToken);

    @GET("/payment_methods")
    Observable<BMResponse> getPaymentMethods();

    @POST("/v3.0/orders")
    Observable<BMResponse> createOrder(@Body CreateOrder createOrder, @Tag String deepLinkJwtToken);

    @GET("paypal_client_token")
    Observable<BMResponse> getPayPalClientToken();

    @GET("accepted_card_types")
    Observable<BMResponse> getAcceptedPayments();

    @GET("accepted_payment_methods")
    Observable<BMResponse> getAcceptedPaymentMethods(@Query("organization_uuid") String organizationUuid);

    @POST("paypal_accounts")
    Observable<BMResponse> addPayPalAccount(@Body JsonObject body);

    @POST("v2.0/payment_methods")
    Observable<Data> addPaymentMethod(@Body PostPaymentMethod postPaymentMethod);

    @DELETE("v2.0/payment_methods/{uuid}")
    Observable<BMResponse> deletePaymentMethod(@Path("uuid") String uuid);

    @DELETE("paypal_accounts/{braintree_paypal_payment_method_token}")
    Observable<Data> deletePayPalAccount(@Path("braintree_paypal_payment_method_token") String token);


    @POST("/faremedia/add_faremedia_card")
    Observable<BMResponse> linkExistingCards(@Body LinkExistingCard body);

    @GET("/faremedia/farecategories")
    Observable<BMResponse> getFareMediaCategories();

    @POST("/faremedia/category_faremedia")
    Observable<BMResponse> createVirtualCard(@Body VirtualCard virtualCard);

    @GET("/orders/transactions")
    Observable<BMResponse> getTransactions(@Query("faremedia_id") String fareMediaId, @Query("page_index") Integer pageIndex);

    @GET
    Observable<Void> getConf(@Url String fileUrl);

    @POST("/v2.0/carts")
    Observable<BMResponse> applyPromoCode(@Body ApplyPromoCode applyPromoCode);

    @GET("/v2.0/payment_methods")
    Observable<BMResponse> getPaymentMethodsv2(@Query("organization_uuid") String organizationUUID);

    @GET("/faremedia/{fareMediumId}/fare_cappingpots")
    Observable<BMResponse> getFareCappings(@Path("fareMediumId") String fareMediumId);

    @GET("wallet/merchant/load_config")
    Observable<BMResponse> getLoadMoneyToWalletConfig();

}