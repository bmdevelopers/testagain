package co.bytemark.data.net

import co.bytemark.domain.interactor.fare_capping.FareCappingResponse
import co.bytemark.domain.interactor.fare_medium.TransferBalanceRequestData
import co.bytemark.domain.interactor.userphoto.UserPhotoResponse
import co.bytemark.domain.model.autoload.*
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.credit_pass.CreditPassRequest
import co.bytemark.domain.model.discount.DiscountCalculationRequest
import co.bytemark.domain.model.fare_medium.TransactionsResponse
import co.bytemark.domain.model.incomm.IncommBarcodeDetail
import co.bytemark.domain.model.incomm.IncommStoreListResponse
import co.bytemark.domain.model.init_fare_capping.InitFareCappingData
import co.bytemark.domain.model.manage.*
import co.bytemark.domain.model.notification.NotificationResponse
import co.bytemark.domain.model.notificationSettings.NotificationSettingTypes
import co.bytemark.domain.model.notificationSettings.NotificationSettingsResponse
import co.bytemark.domain.model.payments.CreateWalletAutoload
import co.bytemark.domain.model.security_questions.SecurityQuestionsResponse
import co.bytemark.domain.model.store.filter.SearchResponseData
import co.bytemark.domain.model.ticket_storage.TransferPassResponse
import co.bytemark.domain.model.userphoto.UserPhoto
import co.bytemark.domain.model.voucher_code.VoucherCodeData
import co.bytemark.sdk.model.payment_methods.Wallet
import co.bytemark.sdk.model.securityquestions.UpdateSecurityQuestion
import co.bytemark.sdk.model.send_pass.SendPassRequest
import co.bytemark.sdk.model.voucher_code.TVMVoucherRedeemRequest
import co.bytemark.sdk.post_body.PostPaymentMethod
import co.bytemark.sdk.post_body.PostSearch
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import retrofit2.http.*

interface CoroutineOvertureApi {

    /**
     * Payments APIs
     */
    @GET("/payment_methods")
    fun getPaymentMethodsAsync(): Deferred<BMResponse>

    @POST("v2.0/payment_methods")
    fun addV2PaymentMethodAsync(@Body postPaymentMethod: PostPaymentMethod): Deferred<BMResponse>

    @GET("/v4.0/orders")
    fun getReceiptsAsync(
        @Query("per_page") perPage: Int,
        @Query("page") pageNo: Int,
        @Query("sort_by") sortBy: String,
        @Query("order") order: String
    ): Deferred<BMResponse>

    @POST("/v4.0/orders/{order_uuid}/receipt_emails")
    fun resendReceiptAsync(@Path("order_uuid") orderUUID: String): Deferred<BMResponse>

    @POST("/wallet")
    fun createWalletAsync(@Body wallet: Wallet): Deferred<BMResponse>

    /**
     * Pass APIs
     */
    @PUT("/passes/{passUuid}/locations/{destination}")
    fun transferPassAsync(
        @Path("passUuid") passUuid: String,
        @Path("destination") destination: String
    ): Deferred<JsonObject>

    @PUT("/passes/{passUuid}/locations/{destination}")
    suspend fun transferPass(
        @Path("passUuid") passUuid: String,
        @Path("destination") destination: String
    ): Response<TransferPassResponse>

    @POST("/v3.0/passes/{pass_uuid}/sendPass")
    fun sendPassAsync(
        @Path("pass_uuid") passUuid: String,
        @Body sendPassRequest: SendPassRequest
    ): Deferred<BMResponse>

    @GET("/v4.0/passes")
    fun getTicketHistoryAsync(
        @Query("per_page") perPage: Int,
        @Query("status") status: String
    ): Deferred<BMResponse>

    @GET
    fun getTicketHistoryPaginatedDataAsync(@Url nextPageURL: String): Deferred<BMResponse>

    /**
     * Subscriptions APIs
     */
    @GET("/subscriptions")
    fun getSubscriptionsAsync(@Query("faremedia_id") fareMediaId: String?): Deferred<BMResponse>

    @PUT("/subscriptions/{subscription_uuid}")
    fun cancelSubscriptionsAsync(
        @Path("subscription_uuid") subscriptionUUID: String,
        @Body body: JsonObject
    ): Deferred<BMResponse>

    /**
     * Notification APIs
     */
    @GET("/notifications")
    suspend fun getNotifications(): Response<NotificationResponse>

    @GET("/notification_settings")
    suspend fun getNotificationSettings(): Response<NotificationSettingsResponse>

    @PUT("/notification_permissions")
    suspend fun updateNotificationPermissions(
        @Body notificationSettingTypes: NotificationSettingTypes
    ): Response<BMResponse>

    /**
     * Faremedia APIs
     */
    @GET("/faremedia/virtualcards")
    fun getAllVirtualCardsAsync(): Deferred<BMResponse>

    @POST("/faremedia/{fareMediumId}/transfer")
    fun transferVirtualCardAsync(
        @Path("fareMediumId") fareMediumId: String,
        @Body data: Unit
    ): Deferred<BMResponse>

    @GET("/faremedia/{fareMediumId}/fare_cappingpots")
    suspend fun getInitFareCappings(@Path("fareMediumId") fareMediumId: String): Response<InitFareCappingData>

    @PUT("/faremedia/update_state")
    suspend fun updateFareMediaState(@Body params: Map<String, String>): Response<Any>

    @DELETE("/faremedia/delete_physical_card/{fareMediumId}")
    suspend fun removeFareMedia(@Path("fareMediumId") fareMediumId: String): Response<Any>

    @GET("/faremedia/farecategories")
    suspend fun getFareMediaCategoriesAsync(): Response<FareCategoryResponse>

    @POST("/faremedia/category_faremedia")
    suspend fun createVirtualCardAsync(@Body virtualCard: VirtualCard?): Response<Data>

    @POST("/faremedia/add_faremedia_card")
    suspend fun linkExistingCardsAsync(@Body body: LinkExistingCard?): Response<AddSmartCard>

    /**
     * Security Questions
     **/
    @GET("security_questions")
    suspend fun getSecurityQuestions(): Response<SecurityQuestionsResponse>

    @GET("users/security_questions")
    suspend fun getSecurityQuestionsOfUser(): Response<SecurityQuestionsResponse>

    @PUT("users/security_questions")
    suspend fun updateSecurityQuestions(
        @Body securityQuestion: UpdateSecurityQuestion
    ): Response<SecurityQuestionsResponse>

    @POST("/v3.0/product/search")
    suspend fun postSearch(@Body postSearch: PostSearch): Response<SearchResponseData>

    @POST("/v4.0/customer_support_emails")
    suspend fun sendAppSupportEmailAsync(@Body params: Map<String, String>): Response<BMResponse>

    /**
     * Account Photo
     **/
    @POST("/user_photos")
    suspend fun setUserPhoto(@Body userPhoto: UserPhoto?): Response<UserPhotoResponse?>

    @GET("/user_photos")
    suspend fun getUserPhoto(): Response<UserPhotoResponse?>

    /**
     * Manage
     */
    @GET("/orders/transactions")
    suspend fun getTransactionsAsync(
        @Query("faremedia_id") fareMediaId: String?,
        @Query("page_index") pageIndex: Int?
    ): Response<TransactionsResponse>

    @PUT("/faremedia/balance_transfer")
    suspend fun transferBalance(@Body transferBalanceRequestData: TransferBalanceRequestData): Response<Unit>

    /**
     * Incomm
     */
    @GET("/v4.0/incomm_stores?")
    suspend fun getRetailerWithLocation(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("radius") radius: Double,
        @Query("order_uuid") orderUuid: String
    ): Response<IncommStoreListResponse>

    @GET("/v4.0/incomm_stores?")
    suspend fun getRetailerWithAddress(
        @Query("address") address: String,
        @Query("radius") radius: Double,
        @Query("order_uuid") orderUuid: String
    ): Response<IncommStoreListResponse>

    @GET("/v4.0/incomm_barcode/{orderUuid}")
    suspend fun getIncommBarcodeDetails(@Path("orderUuid") orderUuid: String): Response<IncommBarcodeDetail>

    /**
     *  Voucher Code
     */
    @PUT("/v3.0/voucher_orders")
    suspend fun redeemVoucherCodeAsync(@Body tvmVoucherRedeemRequest: TVMVoucherRedeemRequest): Response<VoucherCodeData>

    /**
     * Fare Capping
     */
    @GET("/v4.0/customer/farecappings")
    suspend fun getFareCapping(): Response<FareCappingResponse>

    /**
     *  UPass Validation
     */
    @GET("/institution_accounts")
    suspend fun getInstitutionList(@Query("page_index") pageIndex: Int): Response<InstitutionListData>

    @PUT("/institution_accounts/{institution_account_id}/validate")
    suspend fun checkUPassEligibility(
        @Path("institution_account_id") institutionId: String,
        @Body uPassEligibilityRequestData: UPassEligibilityRequestData
    ): Response<UPassEligibilityResponseData>

    /**
     *  Auto Load
     */

    @GET("/wallet/merchant/load_config")
    suspend fun getAutoLoadConfig(): Response<LoadConfig>

    @GET("wallet_autoload")
    suspend fun getAutoloadForWallet(@Query("wallet_uuid") walletUuid: String?): Response<WalletCalendarAutoload>

    @POST("wallet_autoload")
    suspend fun setAutoloadForWallet(@Body createWalletAutoload: CreateWalletAutoload?): Response<Autoload>

    @PUT("wallet_autoload")
    suspend fun updateAutoloadForWallet(@Body createWalletAutoload: CreateWalletAutoload?): Response<Autoload>

    @DELETE("wallet_autoload")
    suspend fun deleteAutoloadForWallet(@Query("wallet_uuid") walletUuid: String?): Response<DeleteAutoload>

    @POST("/orders/autoloads")
    suspend fun saveAutoLoad(@Body postAutoload: PostAutoload?): Response<Autoload>

    @PUT("/orders/autoloads")
    suspend fun updateAutoLoad(@Body postAutoload: PostAutoload?): Response<Autoload>

    @GET("/orders/autoloads")
    suspend fun getAutoLoad(@Query("faremedia_id") fareMediaId: String?): Response<FareMediumAutoload>

    @DELETE("/orders/autoloads")
    suspend fun deleteAutoLoad(@Query("faremedia_id") fareMediaId: String?): Response<DeleteAutoload>

    @POST("/v4.0/thirdparty/deep_link/passes/discount_calculation")
    fun discountCalculation(
        @Body discountCalculationRequest: DiscountCalculationRequest,
        @Tag deeplinkJwtToken: String
    ): Deferred<BMResponse>

    @POST("/v4.0/passes/credit")
    fun creditPassAsync(
        @Body creditPassRequest: CreditPassRequest,
        @Tag deeplinkJwtToken: String
    ): Deferred<BMResponse>
}