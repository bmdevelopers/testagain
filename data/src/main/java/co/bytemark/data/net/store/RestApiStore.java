package co.bytemark.data.net.store;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Intent;
import android.net.Uri;

import java.util.List;

import co.bytemark.domain.error.BytemarkException;
import co.bytemark.domain.model.common.BMError;
import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.sdk.BytemarkSDK;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.Exceptions;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static co.bytemark.data.util.Utils.isServerTimeWithin5MinuteRange;

public abstract class RestApiStore {

    private Application application;

    RestApiStore(Application application) {
        this.application = application;
    }

    /**
     * An observable transformer whose only purpose is to handle errors from response and
     * consolidate things that are common to all API calls like time fraud detection.
     */
    protected final Observable.Transformer<BMResponse, BMResponse> errorHandlingTransformer =
            responseObservable -> responseObservable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError(throwable -> Timber.e("Transformer: " + throwable.getMessage()))
                    .map(response -> {
                        if (!isServerTimeWithin5MinuteRange(response.getServerTime())) {
                            throw Exceptions.propagate(BytemarkException.fromStatusCode(BytemarkSDK.ResponseCode.TIME_INCONSISTENCY_DETECTED));
                        }
                        final List<BMError> errors = response.getErrors();
                        if (errors != null && !errors.isEmpty()) {
                            //handleSpecialErrors(errors);
                            throw Exceptions.propagate(BytemarkException.fromError(errors.get(0)));
                        }
                        return response;
                    })
                    .observeOn(Schedulers.computation()); // Go to computation and continue stream


    private void handleSpecialErrors(List<BMError> errors) {
        for (BMError error : errors) {
            if (error.getCode() == BytemarkSDK.ResponseCode.SDK_VERSION_OUT_OF_DATE) {
                BytemarkSDK.logout(application);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(application);
                alertDialog.setTitle("Update Required"); // TODO strings.xml
                alertDialog.setMessage("A new version of this app is available.\n\nYour user data will still be available after update."); // TODO: config/strings.xml
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("Update", (dialog, which) -> {
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW)
                            .setData(Uri.parse("market://details?id=" + application.getPackageName()));
                    application.startActivity(goToMarket);
                });
                alertDialog.setNegativeButton("Exit", (dialog, which) -> dialog.cancel());
            }
        }
    }

}
