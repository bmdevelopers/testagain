package co.bytemark.data.net.store;

import android.app.Application;
import androidx.annotation.NonNull;

import co.bytemark.data.net.CoroutineOvertureApi;
import co.bytemark.data.net.OvertureRestApi;

public abstract class OvertureRestApiStore extends RestApiStore {

    protected final OvertureRestApi overtureRestApi;

    protected final CoroutineOvertureApi coroutineOvertureApi;

    protected OvertureRestApiStore(
            @NonNull Application application,
            @NonNull OvertureRestApi overtureRestApi,
            @NonNull CoroutineOvertureApi coroutineOvertureApi) {
        super(application);
        this.overtureRestApi = overtureRestApi;
        this.coroutineOvertureApi = coroutineOvertureApi;
    }

    protected OvertureRestApiStore(@NonNull Application application,
                                   @NonNull OvertureRestApi overtureRestApi) {
        super(application);
        this.overtureRestApi = overtureRestApi;
        this.coroutineOvertureApi = null;
    }

}
