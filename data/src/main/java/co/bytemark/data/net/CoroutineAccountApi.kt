package co.bytemark.data.net

import co.bytemark.domain.interactor.authentication.ChangePinRequest
import co.bytemark.domain.interactor.authentication.VelociaLoginRequest
import co.bytemark.domain.model.authentication.ChangePinData
import co.bytemark.domain.model.authentication.SignInResponse
import co.bytemark.domain.model.authentication.VelociaLoginData
import co.bytemark.domain.model.common.ApiResponse
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.common.api_response_data.UserProfileData
import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT

/** Created by Santosh on 13/12/19.
 */

interface CoroutineAccountApi {

    /**
     * Account APIs
     */

    @POST("/users/login")
    suspend fun getLoginOAuthToken(@Body login: Map<String, String?>): Response<SignInResponse>

    @POST("/users/deactivate")
    fun deleteAccountAsync(@Body request: Map<String, String?>): Deferred<BMResponse>

    @POST("/transfer_wallet_funds/")
    fun transferWalletFundsToCcAsync(@Body request: Map<String, String>): Deferred<BMResponse>

    @GET("/users/self")
    fun getUserAsync(): Deferred<ApiResponse<UserProfileData>>

    @PUT("/users")
    fun updateUserAsync(@Body request: Map<String, String?>): Deferred<BMResponse>

    @PUT("/users/change_password")
    fun changePasswordAsync(@Body params: Map<String, String?>?): Deferred<BMResponse>

    @PUT("/users/update_phone_pin")
    suspend fun changePin(@Body changePinRequest: ChangePinRequest): Response<ChangePinData>

    @POST("/users/reset_password_request")
    suspend fun resetPassword(@Body params: Map<String, String>): Response<Any>

    @POST("/users")
    suspend fun register(@Body params: Map<String, @JvmSuppressWildcards Any?>): Response<Any>

    @POST("/users/resend_account_verification")
    suspend fun resendVerificationEmail(): Response<Any>

    @POST("/third_party/velocia/users/sign_in")
    suspend fun loginToVelocia(@Body velociaLoginRequest: VelociaLoginRequest): Response<VelociaLoginData>

}