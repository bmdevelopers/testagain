package co.bytemark.data.net.store

import android.app.Application
import co.bytemark.data.net.AccountRestApi
import co.bytemark.data.net.CoroutineAccountApi
import co.bytemark.data.net.OvertureRestApi

/** Created by Santosh Kulkarni on 16/12/19.
 */

abstract class AccountRestApiStore protected constructor(
        application: Application,
        protected var accountRestApi: AccountRestApi,
        protected var overtureRestApi: OvertureRestApi,
        protected var coroutineAccountApi: CoroutineAccountApi
) : RestApiStore(application)