package co.bytemark.data.net;

import java.util.Map;

import co.bytemark.domain.model.common.BMResponse;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.QueryMap;
import rx.Observable;

public interface AccountRestApi {
    /**
     * OAuth token for a user's signed in session
     *
     * @param login {@link Login} with validated username and password
     * @return {@link Observable}
     */
    @POST("/users/login")
    Observable<BMResponse> getOAuthToken(@Body Map<String, String> login);

    /**
     * Attempts to register new user.
     * <p>
     * Note: {@param params} should contain key-value pairs specified by Formly. Good luck!
     *
     * @param params
     * @return
     */
    @POST("/users")
    Observable<BMResponse> register(@Body Map<String, Object> params);

    /**
     * Change password request with
     *
     * @param params
     * @return
     */
    @PUT("/users/change_password")
    Observable<BMResponse> changePassword(@Body Map<String, String> params);

    /**
     * Reset password request.
     *
     * @param params
     * @return
     */
    @POST("/users/reset_password_request")
    Observable<BMResponse> resetPassword(@Body Map<String, String> params);


    /**
     * Updates already registered user.
     *
     * @param params
     * @return
     */
    @PUT("/users")
    Observable<BMResponse> updateUser(@Body Map<String, String> params);

    /**
     * Deletes the user from bytemark database
     *
     * @param params contains oauthToken
     * @return returns BMResponse
     */
    @DELETE("/users/deactivate")
    Observable<BMResponse> deleteAccount(@QueryMap Map<String, String> params);

    @POST("/users/resend_account_verification")
    Observable<BMResponse> resendVerificationEmail();
}
