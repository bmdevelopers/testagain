package co.bytemark.data.entity.model.config

import com.google.gson.annotations.SerializedName

class Organization {
    @SerializedName("display_name")
    val displayName: String? = null
}