package co.bytemark.data.entity.model.config

import com.google.gson.annotations.SerializedName

data class Domain(
    @SerializedName("overture_url")
    val overtureUrl: String? = null,

    @SerializedName("accounts_url")
    val accountsUrl: String? = null
)