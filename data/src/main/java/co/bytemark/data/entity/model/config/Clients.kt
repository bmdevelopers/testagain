package co.bytemark.data.entity.model.config

import com.google.gson.annotations.SerializedName

data class Clients (
    @SerializedName("customer_mobile")
    val customerMobile: CustomerMobile
)

data class CustomerMobile(
    @SerializedName("display_name")
    val displayName: String,
    @SerializedName("client")
    var client: Client
)

data class Client (
    @SerializedName("client_id")
    val clientId: String? = null
)