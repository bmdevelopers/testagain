package co.bytemark.data.entity.model.config

import com.google.gson.annotations.SerializedName

data class Conf (
    @SerializedName("domain")
    val domain: Domain,

    @SerializedName("organization")
    val organization: Organization,

    @SerializedName("clients")
    val clients: Clients
)