package co.bytemark.data.schedules.local

import co.bytemark.data.data_store.local.LocalEntityStore
import co.bytemark.domain.model.common.Response
import co.bytemark.gtfs.Agency
import co.bytemark.gtfs.DataObjects.Schedule
import co.bytemark.gtfs.Stop
import java.util.*

interface GTFSLocalEntityStore : LocalEntityStore {

    val originStops: Response<List<Stop>>

    fun getDestinations(origin: String?): Response<List<Stop>>

    fun getSchedules(origin: String?, destination: String?, date: Date?): Response<MutableList<Schedule>>

    val agencies: Response<List<Agency>>

}