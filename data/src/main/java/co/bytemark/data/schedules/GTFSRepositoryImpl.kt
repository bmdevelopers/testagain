package co.bytemark.data.schedules

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.schedules.local.GTFSLocalEntityStore
import co.bytemark.data.schedules.remote.GTFSRemoteEntityStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.GTFSRepository
import co.bytemark.gtfs.Agency
import co.bytemark.gtfs.DataObjects.Schedule
import co.bytemark.gtfs.Stop
import java.util.*
import javax.inject.Inject

class GTFSRepositoryImpl @Inject constructor(networkManager: NetworkManager,
                                             @Remote remoteStore: GTFSRemoteEntityStore,
                                             @Local localStore: GTFSLocalEntityStore)
    : RepositoryImpl<GTFSRemoteEntityStore, GTFSLocalEntityStore>(networkManager, remoteStore, localStore), GTFSRepository {

    override val originStops: Response<List<Stop>>
        get() = localStore.originStops


    override fun getDestinations(originId: String?): Response<List<Stop>> {
        return localStore.getDestinations(originId)
    }

    override fun getSchedules(origin: String?, destination: String?, date: Date?): Response<MutableList<Schedule>> {
        return localStore.getSchedules(origin, destination, date)
    }

    override val agencies: Response<List<Agency>>
        get() = localStore.agencies

}