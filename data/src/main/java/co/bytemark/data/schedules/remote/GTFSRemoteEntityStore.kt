package co.bytemark.data.schedules.remote

import android.database.Observable
import co.bytemark.data.data_store.remote.RemoteEntityStore


interface GTFSRemoteEntityStore : RemoteEntityStore {

    fun downloadGTFS(url: String?): Observable<Void?>?

}