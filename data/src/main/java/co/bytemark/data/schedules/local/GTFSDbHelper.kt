package co.bytemark.data.schedules.local

import android.content.Context
import co.bytemark.gtfs.GtfsDb

class GTFSDbHelper(context: Context?) : GtfsDb(context)