package co.bytemark.data.schedules.local

import android.content.Context
import co.bytemark.data.data_store.local.SQLCipherLocalStore
import co.bytemark.domain.model.common.Response
import co.bytemark.gtfs.Agency
import co.bytemark.gtfs.DataObjects.Schedule
import co.bytemark.gtfs.Stop
import java.util.*
import javax.inject.Inject

class GTFSLocalEntityStoreImpl @Inject constructor(context: Context) : SQLCipherLocalStore(context), GTFSLocalEntityStore {

    private val dbHelper: GTFSDbHelper = GTFSDbHelper(context)

    override fun deleteAll() {}

    override fun onOpenRequested() {
        dbHelper.writableDatabase
    }

    override val originStops: Response<List<Stop>>
        get() = Response(data = dbHelper.stops)

    override fun getDestinations(origin: String?): Response<List<Stop>> {
        return Response(data = dbHelper.getDestinations(origin))
    }

    override fun getSchedules(origin: String?, destination: String?, date: Date?): Response<MutableList<Schedule>> {
        val schedules: MutableList<Schedule> = dbHelper.getStopTimesWithDuration(origin, destination, date)
        return Response(data = schedules)
    }

    override val agencies: Response<List<Agency>>
        get() = Response(data = dbHelper.agencies)

}