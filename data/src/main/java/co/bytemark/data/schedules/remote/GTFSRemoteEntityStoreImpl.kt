package co.bytemark.data.schedules.remote

import android.app.Application
import android.database.Observable
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import javax.inject.Inject


class GTFSRemoteEntityStoreImpl @Inject constructor(application: Application, overtureRestApi: OvertureRestApi)
                                                : OvertureRestApiStore(application, overtureRestApi),
                                                 GTFSRemoteEntityStore {

    override fun downloadGTFS(url: String?): Observable<Void?>? {
        return null
    }
}