package co.bytemark.data.userphoto.local

import android.app.Application
import co.bytemark.data.userphoto.local.UserPhotoPersistenceContract.UserPhotoEntry
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SQLiteException
import net.sqlcipher.database.SQLiteOpenHelper
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserPhotoDbHelper @Inject internal constructor(val context: Application)
    : SQLiteOpenHelper(
        context.applicationContext,
        DATABASE_NAME,
        null,
        DATABASE_VERSION) {

    companion object {
        private const val DATABASE_VERSION = 312
        private const val DATABASE_NAME = ".h"
        private const val TEXT_TYPE = " TEXT"
        private const val NOT_NULL = " NOT NULL"
        private const val COMMA_SEP = ","
        private const val CREATE_USER_PHOTO_TABLE = "CREATE TABLE IF NOT EXISTS " + UserPhotoEntry.CACHED_USER_PHOTO_TABLE + " (" +
                UserPhotoEntry.PHOTO_UUID + TEXT_TYPE + " PRIMARY KEY" + NOT_NULL + COMMA_SEP +
                UserPhotoEntry.PHOTO_BASE64 + TEXT_TYPE + COMMA_SEP +
                UserPhotoEntry.PHOTO_TIME_CREATED + TEXT_TYPE + COMMA_SEP +
                UserPhotoEntry.PHOTO_STATUS + TEXT_TYPE + COMMA_SEP +
                UserPhotoEntry.PHOTO_REVIEWER + TEXT_TYPE + COMMA_SEP +
                UserPhotoEntry.PHOTO_TIME_REVIEWED + TEXT_TYPE + COMMA_SEP +
                "UNIQUE (" + UserPhotoEntry.PHOTO_UUID + ") ON CONFLICT REPLACE" +
                ");"
    }

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) = sqLiteDatabase.execSQL(CREATE_USER_PHOTO_TABLE)

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {}

    fun getSafeDB(isWritableDatabase: Boolean, oAuthToken: String?): SQLiteDatabase? {
        var db: SQLiteDatabase? = null
        db = try {
            if (isWritableDatabase)
                this.getWritableDatabase(oAuthToken)
            else
                this.getReadableDatabase(oAuthToken)
        } catch (e: SQLiteException) {
            val dbFile: File = context.getDatabasePath(".h")
            dbFile.delete()
            if (isWritableDatabase) this.getWritableDatabase(oAuthToken) else this.getReadableDatabase(oAuthToken)
        }
        return db
    }

}