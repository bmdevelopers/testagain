package co.bytemark.data.userphoto

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.userphoto.local.UserPhotoLocalEntityStore
import co.bytemark.data.userphoto.remote.UserPhotoRemoteEntityStore
import co.bytemark.domain.interactor.userphoto.UserPhotoResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.userphoto.UserPhoto
import co.bytemark.domain.repository.UserPhotoRepository
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserPhotoRepositoryImpl @Inject internal constructor(
        networkManager: NetworkManager,
        @Remote remoteStore: UserPhotoRemoteEntityStore,
        @Local localStore: UserPhotoLocalEntityStore
) : RepositoryImpl<UserPhotoRemoteEntityStore, UserPhotoLocalEntityStore>(
        networkManager,
        remoteStore,
        localStore
), UserPhotoRepository {

    override suspend fun getUserPhoto(): Response<UserPhotoResponse?> {
        val userPhotoResponse: Response<UserPhotoResponse?>
        if (networkManager.isNetworkAvailable) {
            userPhotoResponse = remoteStore.getUserPhoto()
            if (userPhotoResponse.errors.isNotEmpty()) {
                return userPhotoResponse
            }
            if (userPhotoResponse.data != null) {
                saveUserPhoto(userPhotoResponse.data?.userPhoto!!)
            }
        } else {
            userPhotoResponse = localStore.getUserPhoto()
            Timber.d("Fetching Photo only locally");
        }
        return userPhotoResponse
    }

    override suspend fun saveUserPhoto(userPhoto: UserPhoto): Response<UserPhotoResponse?> =
            localStore.saveUserPhoto(userPhoto)

    override suspend fun setUserPhoto(userPhoto: UserPhoto): Response<UserPhotoResponse?> =
            saveUserPhoto(remoteStore.setUserPhoto(userPhoto).data?.userPhoto!!)

}