package co.bytemark.data.userphoto.local

import android.provider.BaseColumns

/**
 * The contract used for the db to save the user photo locally.
 */

internal class UserPhotoPersistenceContract

// To prevent someone from accidentally instantiating the contract class,
// give it an empty constructor.
private constructor() {
    /* Inner class that defines the table contents */
    internal object UserPhotoEntry : BaseColumns {
        const val CACHED_USER_PHOTO_TABLE = "CachedUserPhoto"
        const val PHOTO_UUID = "uuid"
        const val PHOTO_BASE64 = "data"
        const val PHOTO_TIME_CREATED = "time_created"
        const val PHOTO_STATUS = "status"
        const val PHOTO_REVIEWER = "reviewer" // // TODO: 2/17/17 Is reviewer required?
        const val PHOTO_TIME_REVIEWED = "time_reviewed"
    }
}