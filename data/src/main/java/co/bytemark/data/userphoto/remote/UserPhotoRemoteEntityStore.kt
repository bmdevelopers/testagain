package co.bytemark.data.userphoto.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.interactor.userphoto.UserPhotoResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.userphoto.UserPhoto

interface UserPhotoRemoteEntityStore : RemoteEntityStore {

    suspend fun setUserPhoto(userPhoto: UserPhoto): Response<UserPhotoResponse?>

    suspend fun getUserPhoto(): Response<UserPhotoResponse?>

}