package co.bytemark.data.userphoto.local

import android.app.Application
import android.content.ContentValues
import android.text.TextUtils
import co.bytemark.data.data_store.local.SQLCipherLocalStore
import co.bytemark.data.userphoto.local.UserPhotoPersistenceContract.UserPhotoEntry
import co.bytemark.domain.interactor.userphoto.UserPhotoResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.userphoto.UserPhoto
import co.bytemark.sdk.BytemarkSDK
import net.sqlcipher.Cursor
import rx.Observable
import rx.functions.Func1
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserPhotoLocalEntityStoreImpl @Inject internal constructor(
        application: Application,
        private val dbHelper: UserPhotoDbHelper)
    : SQLCipherLocalStore(application.applicationContext), UserPhotoLocalEntityStore {

    private val userPhotoMapperFunction: Func1<Cursor?, UserPhoto?>

    init {
        userPhotoMapperFunction = Func1 { cursor: Cursor? -> mapCursorToUserPhoto(cursor) }
    }

    private val projection = arrayOf(
            UserPhotoEntry.PHOTO_UUID,
            UserPhotoEntry.PHOTO_BASE64,
            UserPhotoEntry.PHOTO_TIME_CREATED,
            UserPhotoEntry.PHOTO_STATUS,
            UserPhotoEntry.PHOTO_TIME_REVIEWED
    )

    private fun mapCursorToUserPhoto(cursor: Cursor?): UserPhoto {
        val uuid = cursor?.getString(cursor.getColumnIndex(UserPhotoEntry.PHOTO_UUID))
        val base64 = cursor?.getString(cursor.getColumnIndex(UserPhotoEntry.PHOTO_BASE64))
        val timeCreated = cursor?.getString(cursor.getColumnIndex(UserPhotoEntry.PHOTO_TIME_CREATED))
        val status = cursor?.getString(cursor.getColumnIndex(UserPhotoEntry.PHOTO_STATUS))
        val timeReviewed = cursor?.getString(cursor.getColumnIndex(UserPhotoEntry.PHOTO_TIME_REVIEWED))
        return UserPhoto(uuid, base64, timeCreated, status, timeReviewed = timeReviewed)
    }

    override suspend fun saveUserPhoto(userPhoto: UserPhoto): Response<UserPhotoResponse?> {
        open()
        database.delete(UserPhotoEntry.CACHED_USER_PHOTO_TABLE, null, null) != -1
        close()
        saveUserPhotoSyc(userPhoto)
        return this.getUserPhoto()
    }

    private fun saveUserPhotoSyc(userPhoto: UserPhoto): Boolean {
        open()
        val values = ContentValues()
        values.put(UserPhotoEntry.PHOTO_UUID, userPhoto.uuid)
        values.put(UserPhotoEntry.PHOTO_BASE64, userPhoto.data)
        values.put(UserPhotoEntry.PHOTO_TIME_CREATED, userPhoto.timeCreated)
        values.put(UserPhotoEntry.PHOTO_STATUS, userPhoto.status)
        values.put(UserPhotoEntry.PHOTO_TIME_REVIEWED, userPhoto.timeReviewed)
        val savedUserPhoto = userPhoto.uuid?.let { getUserPhotoSync(it) }
        val savedUuid = savedUserPhoto?.uuid
        val success: Long
        success = if (savedUuid != null && savedUuid == userPhoto.uuid) {
            val selection = UserPhotoEntry.PHOTO_UUID + " LIKE ?"
            val selectionArgs = arrayOf(userPhoto.uuid)
            database.update(UserPhotoEntry.CACHED_USER_PHOTO_TABLE, values, selection, selectionArgs).toLong()
        } else {
            database.insert(UserPhotoEntry.CACHED_USER_PHOTO_TABLE, null, values)
        }
        Timber.d("Saved %d", success)
        return success != -1L
    }

    private fun getUserPhotoSync(userPhotoUuid: String): UserPhoto? {
        return getUserPhoto(userPhotoUuid).toBlocking().first()
    }

    private fun getUserPhoto(userPhotoUuid: String): Observable<UserPhoto> {
        val sql = String.format("SELECT %s FROM %s WHERE %s LIKE ?", TextUtils.join(",", projection), UserPhotoEntry.CACHED_USER_PHOTO_TABLE, UserPhotoEntry.PHOTO_UUID)
        return Observable.fromCallable {
            open()
            val cursor = database.rawQuery(sql, arrayOf(userPhotoUuid))
            mapToOne(cursor, userPhotoMapperFunction)
        }
    }

    override suspend fun getUserPhoto(): Response<UserPhotoResponse?> {
        val sql = String.format("SELECT %s FROM %s", TextUtils.join(",", projection), UserPhotoEntry.CACHED_USER_PHOTO_TABLE)
        open()
        val cursor = database.rawQuery(sql, null)
        if (cursor != null) Timber.d("User Photo cursor count" + cursor.count)
        close()
        return Response(data = UserPhotoResponse(mapToOne(cursor, userPhotoMapperFunction)))
    }

    override fun deleteAll() {
        try {
            open()
            database.delete(UserPhotoEntry.CACHED_USER_PHOTO_TABLE, null, null)
        } catch (e: Exception) {
            e.printStackTrace()
        }finally {
            close()
        }
    }

    override fun onOpenRequested() {
        database = dbHelper.getSafeDB(true, BytemarkSDK.SDKUtility.getAuthToken())
    }

}