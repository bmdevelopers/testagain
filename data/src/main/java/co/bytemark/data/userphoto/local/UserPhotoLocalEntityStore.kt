package co.bytemark.data.userphoto.local

import co.bytemark.data.data_store.local.LocalEntityStore
import co.bytemark.domain.interactor.userphoto.UserPhotoResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.userphoto.UserPhoto

interface UserPhotoLocalEntityStore : LocalEntityStore {

    suspend fun getUserPhoto(): Response<UserPhotoResponse?>

    suspend fun saveUserPhoto(userPhoto: UserPhoto): Response<UserPhotoResponse?>

}