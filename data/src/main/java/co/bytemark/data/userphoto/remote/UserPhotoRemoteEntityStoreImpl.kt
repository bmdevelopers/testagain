package co.bytemark.data.userphoto.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.interactor.userphoto.UserPhotoResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.userphoto.UserPhoto
import javax.inject.Inject

class UserPhotoRemoteEntityStoreImpl @Inject internal constructor(
        application: Application,
        overtureRestApi: OvertureRestApi,
        coroutineOvertureApi: CoroutineOvertureApi) :
        OvertureRestApiStore(application, overtureRestApi, coroutineOvertureApi), UserPhotoRemoteEntityStore {


    override suspend fun setUserPhoto(userPhoto: UserPhoto): Response<UserPhotoResponse?> {
        return coroutineOvertureApi.setUserPhoto(userPhoto)
    }

    override suspend fun getUserPhoto(): Response<UserPhotoResponse?> {
        return coroutineOvertureApi.getUserPhoto()
    }

}