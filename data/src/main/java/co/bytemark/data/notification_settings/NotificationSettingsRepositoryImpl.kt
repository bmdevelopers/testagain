package co.bytemark.data.notification_settings

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.notification_settings.local.NotificationSettingsLocalEntityStore
import co.bytemark.data.notification_settings.remote.NotificationSettingsRemoteEntityStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.notificationSettings.NotificationSettingTypes
import co.bytemark.domain.model.notificationSettings.NotificationSettingsResponse
import co.bytemark.domain.repository.NotificationSettingsRepository
import javax.inject.Inject

class NotificationSettingsRepositoryImpl @Inject internal constructor(networkManager: NetworkManager,
                                                                      @Remote cloudStore: NotificationSettingsRemoteEntityStore,
                                                                      @Local localStore: NotificationSettingsLocalEntityStore) : RepositoryImpl<NotificationSettingsRemoteEntityStore, NotificationSettingsLocalEntityStore>(networkManager, cloudStore, localStore), NotificationSettingsRepository {
    override suspend fun getNotificationsSettings(): Response<NotificationSettingsResponse> {
        return remoteStore.getNotificationSettings()
    }

    override suspend fun updateNotificationPermissions(notificationSettingTypes: NotificationSettingTypes): Response<BMResponse> {
        return remoteStore.updateNotificationPermissions(notificationSettingTypes)
    }
}