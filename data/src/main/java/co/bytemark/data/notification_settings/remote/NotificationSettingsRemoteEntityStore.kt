package co.bytemark.data.notification_settings.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.notificationSettings.NotificationSettingTypes
import co.bytemark.domain.model.notificationSettings.NotificationSettingsResponse

interface NotificationSettingsRemoteEntityStore : RemoteEntityStore {
    suspend fun getNotificationSettings() : Response<NotificationSettingsResponse>
    suspend fun updateNotificationPermissions(notificationSettingTypes: NotificationSettingTypes) : Response<BMResponse>
}