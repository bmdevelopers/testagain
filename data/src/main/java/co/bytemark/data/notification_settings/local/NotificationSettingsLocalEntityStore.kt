package co.bytemark.data.notification_settings.local

import co.bytemark.data.data_store.local.LocalEntityStore

interface NotificationSettingsLocalEntityStore : LocalEntityStore