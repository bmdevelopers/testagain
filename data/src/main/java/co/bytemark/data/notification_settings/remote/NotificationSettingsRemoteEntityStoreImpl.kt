package co.bytemark.data.notification_settings.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.notificationSettings.NotificationSettingTypes
import co.bytemark.domain.model.notificationSettings.NotificationSettingsResponse
import javax.inject.Inject

class NotificationSettingsRemoteEntityStoreImpl @Inject internal constructor(application: Application, overtureRestApi: OvertureRestApi, api: CoroutineOvertureApi) : OvertureRestApiStore(application, overtureRestApi, api), NotificationSettingsRemoteEntityStore {
    override suspend fun getNotificationSettings(): Response<NotificationSettingsResponse> {
        return coroutineOvertureApi.getNotificationSettings()
    }

    override suspend fun updateNotificationPermissions(notificationSettingTypes: NotificationSettingTypes): Response<BMResponse> {
        return coroutineOvertureApi.updateNotificationPermissions(notificationSettingTypes)
    }
}