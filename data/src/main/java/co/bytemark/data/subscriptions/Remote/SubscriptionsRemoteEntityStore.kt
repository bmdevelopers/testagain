package co.bytemark.data.subscriptions.Remote


import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.common.BMResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred

/**
 * Created by vally on 09/06/19.
 */
interface SubscriptionsRemoteEntityStore : RemoteEntityStore {

    fun getSubscriptions(fareMediaId: String?): Deferred<BMResponse>

    fun cancelSubscriptions(uuid: String,jsonObject: JsonObject): Deferred<BMResponse>
}