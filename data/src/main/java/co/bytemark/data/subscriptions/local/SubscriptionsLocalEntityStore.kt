package co.bytemark.data.subscriptions.local

import co.bytemark.data.data_store.local.LocalEntityStore

/**
 * Created by vally on 09/06/19.
 */

interface SubscriptionsLocalEntityStore : LocalEntityStore {

}