package co.bytemark.data.subscriptions

import androidx.annotation.NonNull
import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.subscriptions.Remote.SubscriptionsRemoteEntityStore
import co.bytemark.data.subscriptions.local.SubscriptionsLocalEntityStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.repository.SubscriptionsRepository
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import javax.inject.Inject

/**
 * Created by vally on 09/06/19.
 */
class SubscriptionsRepositoryImpl @Inject constructor(@NonNull networkManager: NetworkManager,
                                                      @NonNull @Remote remoteEntityStore: SubscriptionsRemoteEntityStore,
                                                      @NonNull @Local localEntityStore: SubscriptionsLocalEntityStore)
    : RepositoryImpl<SubscriptionsRemoteEntityStore, SubscriptionsLocalEntityStore>(networkManager, remoteEntityStore, localEntityStore), SubscriptionsRepository {



    override fun getSubscriptions(fareMediaId: String?): Deferred<BMResponse> {
        return remoteStore.getSubscriptions(fareMediaId)
    }

    override fun cancelSubscriptions(uuid: String,jsonObject: JsonObject): Deferred<BMResponse> {
        return remoteStore.cancelSubscriptions(uuid,jsonObject)
    }
}