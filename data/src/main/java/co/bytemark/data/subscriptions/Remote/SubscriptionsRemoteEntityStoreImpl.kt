package co.bytemark.data.subscriptions.Remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.sdk.BytemarkSDK
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import javax.inject.Inject

/**
 * Created by vally on 09/06/19.
 */
class SubscriptionsRemoteEntityStoreImpl @Inject constructor(application: Application, overtureRestApi: OvertureRestApi,
                                                             coroutineOvertureApi: CoroutineOvertureApi) :
        OvertureRestApiStore(application, overtureRestApi, coroutineOvertureApi), SubscriptionsRemoteEntityStore {


    override fun getSubscriptions(fareMediaId: String?): Deferred<BMResponse> {
        return coroutineOvertureApi.getSubscriptionsAsync(fareMediaId)
    }

    override fun cancelSubscriptions(uuid: String, jsonObject: JsonObject): Deferred<BMResponse> {

        return coroutineOvertureApi.cancelSubscriptionsAsync(uuid, jsonObject)
    }
}