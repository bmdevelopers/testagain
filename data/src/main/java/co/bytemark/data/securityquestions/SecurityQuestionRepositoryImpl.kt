package co.bytemark.data.securityquestions

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.data.securityquestions.local.SecurityQuestionLocalEntityStore
import co.bytemark.data.securityquestions.remote.SecurityQuestionRemoteEntityStore
import co.bytemark.domain.repository.SecurityQuestionRepository
import co.bytemark.sdk.model.securityquestions.UpdateSecurityQuestion
import javax.inject.Inject

class SecurityQuestionRepositoryImpl @Inject constructor(
        networkManager: NetworkManager,
        @Remote remoteStore: SecurityQuestionRemoteEntityStore,
        @Local localStore: SecurityQuestionLocalEntityStore
) : RepositoryImpl<SecurityQuestionRemoteEntityStore, SecurityQuestionLocalEntityStore>(
        networkManager,
        remoteStore,
        localStore
), SecurityQuestionRepository {

    override suspend fun getSecurityQuestions() =
            remoteStore.getSecurityQuestions()

    override suspend fun getSecurityQuestionOfUser() =
            remoteStore.getSecurityQuestionOfUser()

    override suspend fun updateSecurityQuestions(updateSecurityQuestion: UpdateSecurityQuestion) =
            remoteStore.updateSecurityQuestions(updateSecurityQuestion)
}