package co.bytemark.data.securityquestions.local

import co.bytemark.data.data_store.local.LocalEntityStore
import javax.inject.Inject

interface SecurityQuestionLocalEntityStore : LocalEntityStore

class SecurityQuestionLocalEntityStoreImpl @Inject constructor() : SecurityQuestionLocalEntityStore