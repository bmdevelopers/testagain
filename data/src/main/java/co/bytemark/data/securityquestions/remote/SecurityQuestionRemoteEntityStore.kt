package co.bytemark.data.securityquestions.remote

import android.app.Application
import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.security_questions.SecurityQuestionsResponse
import co.bytemark.sdk.model.securityquestions.UpdateSecurityQuestion
import javax.inject.Inject

interface SecurityQuestionRemoteEntityStore : RemoteEntityStore {
    suspend fun getSecurityQuestions(): Response<SecurityQuestionsResponse>
    suspend fun getSecurityQuestionOfUser(): Response<SecurityQuestionsResponse>
    suspend fun updateSecurityQuestions(updateSecurityQuestion: UpdateSecurityQuestion): Response<SecurityQuestionsResponse>
}

class SecurityQuestionRemoteEntityStoreImpl @Inject constructor(
        application: Application,
        overtureApi: OvertureRestApi,
        coroutineApi: CoroutineOvertureApi
) : OvertureRestApiStore(
        application,
        overtureApi,
        coroutineApi
), SecurityQuestionRemoteEntityStore {

    override suspend fun getSecurityQuestions() =
            coroutineOvertureApi.getSecurityQuestions()

    override suspend fun getSecurityQuestionOfUser() =
            coroutineOvertureApi.getSecurityQuestionsOfUser()

    override suspend fun updateSecurityQuestions(updateSecurityQuestion: UpdateSecurityQuestion) =
            coroutineOvertureApi.updateSecurityQuestions(updateSecurityQuestion)
}