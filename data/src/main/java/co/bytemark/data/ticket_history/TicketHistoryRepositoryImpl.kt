package co.bytemark.data.ticket_history

import androidx.annotation.NonNull
import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.ticket_history.local.TicketHistoryLocalEntityStore
import co.bytemark.data.ticket_history.remote.TicketHistoryRemoteEntityStore
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.repository.TicketHistoryRepository
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class TicketHistoryRepositoryImpl  @Inject constructor(@NonNull networkManager: NetworkManager,
                                                       @NonNull @Remote remoteEntityStore: TicketHistoryRemoteEntityStore,
                                                       @NonNull @Local localEntityStore: TicketHistoryLocalEntityStore)
    : RepositoryImpl<TicketHistoryRemoteEntityStore, TicketHistoryLocalEntityStore>(networkManager, remoteEntityStore, localEntityStore), TicketHistoryRepository {

    override fun getTicketHistoryAsync(perPage: Int, status: String, nextPageURL: String?): Deferred<BMResponse> {
        return if(nextPageURL != null) // For pagination, we are loading the URL from "next" value in the history response.
            remoteStore.getTicketHistoryPaginatedDataAsync(nextPageURL)
        else
            remoteStore.getTicketHistoryAsync(perPage, status)
    }
}