package co.bytemark.data.ticket_history.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.BMResponse
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class TicketHistoryRemoteEntityStoreImpl @Inject constructor(application: Application, overtureRestApi: OvertureRestApi,
                                                             coroutineOvertureApi: CoroutineOvertureApi) :
        OvertureRestApiStore(application, overtureRestApi, coroutineOvertureApi), TicketHistoryRemoteEntityStore {


    override fun getTicketHistoryAsync(perPage: Int, status: String): Deferred<BMResponse> =
            coroutineOvertureApi.getTicketHistoryAsync(perPage, status)

    override fun getTicketHistoryPaginatedDataAsync(nextPageURL: String): Deferred<BMResponse> =
            coroutineOvertureApi.getTicketHistoryPaginatedDataAsync(nextPageURL)

}