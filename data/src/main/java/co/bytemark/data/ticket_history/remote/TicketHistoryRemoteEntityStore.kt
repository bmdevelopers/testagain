package co.bytemark.data.ticket_history.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.common.BMResponse
import kotlinx.coroutines.Deferred

interface TicketHistoryRemoteEntityStore : RemoteEntityStore {

    fun getTicketHistoryAsync(perPage: Int, status: String): Deferred<BMResponse>

    fun getTicketHistoryPaginatedDataAsync(nextPageURL: String): Deferred<BMResponse>
}