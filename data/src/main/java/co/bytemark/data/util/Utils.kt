package co.bytemark.data.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.jvm.Throws

object Utils {
    @Throws(ParseException::class)
    fun convertToCalendarFormServer(value: String?): Calendar? {
        if (value == null || value == "null" || value == "") return null
        val calendar = Calendar.getInstance()
        val formatter = SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            Locale.getDefault()
        )
        formatter.timeZone = TimeZone.getTimeZone("UTC")
        val date: Date
        date = formatter.parse(value)
        calendar.time = date
        return calendar
    }

    @JvmStatic
    @Throws(ParseException::class)
    fun convertToCalendar(value: String?): Calendar? {
        if (value == null || value == "null" || value == "") return null
        val calendar = Calendar.getInstance()
        val formatter = SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            Locale.getDefault()
        )
        val date: Date
        date = formatter.parse(value)
        calendar.time = date
        return calendar
    }

    @JvmStatic
    fun convertToString(calendar: Calendar?): String? {
        if (calendar == null) return null
        val formatter = SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            Locale.getDefault()
        )
        return formatter.format(calendar.time)
    }

    @JvmStatic
    fun isServerTimeWithin5MinuteRange(serverTime: String): Boolean {
        return try {
            val dateToValidate =
                convertToCalendarFormServer(serverTime)

            // current date before 5 minute
            val currentDateBefore5Minute = Calendar.getInstance()
            currentDateBefore5Minute.add(Calendar.MINUTE, -5)

            // current date after 5 minute
            val currentDateAfter5Minute = Calendar.getInstance()
            currentDateAfter5Minute.add(Calendar.MINUTE, 5)
            (dateToValidate!!.before(currentDateAfter5Minute)
                    && dateToValidate.after(currentDateBefore5Minute))
        } catch (e: ParseException) {
            false
        }
    }
}