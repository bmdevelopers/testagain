package co.bytemark.data.util

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Json Serializer/Deserializer.
 */
@Singleton
class Serializer @Inject constructor() {
    private val gson = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        .create()

    fun <T> deserialize(string: String?, clazz: Class<T>?): T {
        return gson.fromJson(string, clazz)
    }
}