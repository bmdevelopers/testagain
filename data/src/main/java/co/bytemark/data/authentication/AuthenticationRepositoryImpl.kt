package co.bytemark.data.authentication

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.authentication.local.AuthenticationLocalEntityStore
import co.bytemark.data.authentication.remote.AuthenticationRemoteEntityStore
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.domain.interactor.authentication.ChangePinRequest
import co.bytemark.domain.interactor.authentication.VelociaLoginRequest
import co.bytemark.domain.model.authentication.ChangePinData
import co.bytemark.domain.model.authentication.SignInResponse
import co.bytemark.domain.model.authentication.VelociaLoginData
import co.bytemark.domain.model.common.ApiResponse
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.common.api_response_data.UserProfileData
import co.bytemark.domain.repository.AuthenticationRepository
import kotlinx.coroutines.Deferred
import rx.Observable
import javax.inject.Inject
import javax.inject.Singleton

/** Created by Santosh Kulkarni on 16/12/19.
 */

@Singleton
class AuthenticationRepositoryImpl @Inject constructor(
        networkManager: NetworkManager,
        @Remote remoteStore: AuthenticationRemoteEntityStore,
        @Local localStore: AuthenticationLocalEntityStore
) : RepositoryImpl<AuthenticationRemoteEntityStore?, AuthenticationLocalEntityStore?>(
        networkManager,
        remoteStore,
        localStore
), AuthenticationRepository {

    override suspend fun getOauthToken(loginParams: MutableMap<String, String>): Response<SignInResponse> =
            remoteStore!!.getOauthToken(loginParams)


    override suspend fun resetPassword(params: MutableMap<String, String>): Response<Any> =
            remoteStore!!.resetPassword(params)


    override fun submitVoucherCode(params: MutableMap<String, String?>): Observable<BMResponse> {
        return remoteStore!!.submitVoucherCode(params)
    }

    override suspend fun registerNewUser(params: MutableMap<String, Any?>): Response<Any> =
            remoteStore!!.registerNewUser(params)


    override fun changePassword(params: MutableMap<String, String>?): Deferred<BMResponse> {
        return remoteStore!!.changePassword(params)
    }

    override fun deleteAccountAsync(cardUuid: String?): Deferred<BMResponse> {
        return remoteStore!!.deleteAccountAsync(cardUuid)
    }

    override suspend fun resentVerificationEmail(): Response<Any> =
            remoteStore!!.resendVerificationEmail()

    override fun getUserAsync(): Deferred<ApiResponse<UserProfileData>> =
            remoteStore!!.getUserAsync()

    override fun updateUserAsync(params: MutableMap<String, String?>): Deferred<BMResponse> {
        return remoteStore!!.updateUserAsync(params)
    }

    override suspend fun changePin(changePinRequest: ChangePinRequest): Response<ChangePinData> =
            remoteStore!!.changePin(changePinRequest)

    override suspend fun loginToVelocia(velociaLoginRequest: VelociaLoginRequest) =
            remoteStore!!.loginToVelocia(velociaLoginRequest)

}