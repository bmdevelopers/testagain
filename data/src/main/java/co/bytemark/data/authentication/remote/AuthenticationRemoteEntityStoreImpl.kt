package co.bytemark.data.authentication.remote

import android.app.Application
import android.os.Build
import android.text.TextUtils
import co.bytemark.data.net.AccountRestApi
import co.bytemark.data.net.CoroutineAccountApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.AccountRestApiStore
import co.bytemark.domain.interactor.authentication.ChangePinRequest
import co.bytemark.domain.interactor.authentication.VelociaLoginRequest
import co.bytemark.domain.model.authentication.ChangePinData
import co.bytemark.domain.model.authentication.SignInResponse
import co.bytemark.domain.model.authentication.VelociaLoginData
import co.bytemark.domain.model.common.ApiResponse
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.common.api_response_data.UserProfileData
import co.bytemark.sdk.Api.ApiUtility
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.data.identifiers.IdentifiersRepository
import kotlinx.coroutines.Deferred
import rx.Observable
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.HashMap

/** Created by Santosh on 13/12/19.
 */

@Singleton
class AuthenticationRemoteEntityStoreImpl @Inject constructor(
        accountRestApi: AccountRestApi,
        overtureRestApi: OvertureRestApi,
        private val application: Application,
        coroutineAccountApi: CoroutineAccountApi,
        private val identifiersRepository: IdentifiersRepository
) : AccountRestApiStore(
        application,
        accountRestApi,
        overtureRestApi,
        coroutineAccountApi
), AuthenticationRemoteEntityStore {

    override suspend fun getOauthToken(loginParams: MutableMap<String, String>): Response<SignInResponse> {
        prepareQueryMap(loginParams)
        return coroutineAccountApi.getLoginOAuthToken(loginParams)
    }

    override suspend fun registerNewUser(params: MutableMap<String, Any?>): Response<Any> {
        for (entry in params.entries) {
            if (entry.value is String && TextUtils.isEmpty(entry.value as String?)) {
                entry.setValue(null)
            }
        }
        return coroutineAccountApi.register(params)
    }

    private fun prepareQueryMap(queryMap: MutableMap<String, String>): Map<String, String?> {
        var deviceModel = ""
        try {
            deviceModel = URLEncoder.encode(Build.MODEL, "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        return queryMap.apply {
            put("device_os_version", Build.VERSION.RELEASE)
            put("device_os", "android")
            put("osi", BytemarkSDK.SDKUtility.getDeviceOsi())
            put("device_model", deviceModel)
            put("device_nickname", deviceModel)
            identifiersRepository.getAttribute("aii")?.let { put("aii", it) }
            put("app_version", BytemarkSDK.SDKUtility.getAppVersion())
            put("client_id", BytemarkSDK.SDKUtility.getClientId())
            ApiUtility.getSFromCalendar(Calendar.getInstance())?.let { put("device_time", it) }
        }
    }

    override suspend fun resetPassword(params: MutableMap<String, String>): Response<Any> =
            coroutineAccountApi.resetPassword(params)


    override fun changePassword(params: MutableMap<String, String>?): Deferred<BMResponse> {
        return coroutineAccountApi.changePasswordAsync(params)
    }

    override fun submitVoucherCode(params: MutableMap<String, String?>): Observable<BMResponse> {
        return overtureRestApi.postVoucherCode(params).compose(errorHandlingTransformer)
    }

    override fun deleteAccountAsync(cardUuid: String?): Deferred<BMResponse> {
        return coroutineAccountApi.deleteAccountAsync(HashMap<String, String?>().apply { put("credit_card_uuid", cardUuid) })
    }

    override suspend fun resendVerificationEmail(): Response<Any> =
            coroutineAccountApi.resendVerificationEmail()

    override fun getUserAsync(): Deferred<ApiResponse<UserProfileData>> =
            coroutineAccountApi.getUserAsync()

    override fun updateUserAsync(params: MutableMap<String, String?>): Deferred<BMResponse> {
        for (entry in params.entries) {
            if (TextUtils.isEmpty(entry.value)) {
                entry.setValue(null)
            }
        }
        return coroutineAccountApi.updateUserAsync(params)
    }

    override suspend fun changePin(changePinRequest: ChangePinRequest): Response<ChangePinData> =
            coroutineAccountApi.changePin(changePinRequest)

    override suspend fun loginToVelocia(velociaLoginRequest: VelociaLoginRequest): Response<VelociaLoginData> =
            coroutineAccountApi.loginToVelocia(velociaLoginRequest)

}