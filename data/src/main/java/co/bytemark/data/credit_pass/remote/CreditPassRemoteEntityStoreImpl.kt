package co.bytemark.data.credit_pass.remote

import android.app.Application
import co.bytemark.data.net.CoroutineOvertureApi
import co.bytemark.data.net.OvertureRestApi
import co.bytemark.data.net.store.OvertureRestApiStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.credit_pass.CreditPassRequest
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class CreditPassRemoteEntityStoreImpl @Inject constructor(
    application: Application,
    overtureRestApi: OvertureRestApi,
    coroutineOvertureApi: CoroutineOvertureApi
) : OvertureRestApiStore(
    application,
    overtureRestApi,
    coroutineOvertureApi
), CreditPassRemoteEntityStore {
    override fun creditPassAsync(
        creditPassRequest: CreditPassRequest,
        deeplinkJWTToken: String
    ): Deferred<BMResponse> =
        coroutineOvertureApi.creditPassAsync(
            creditPassRequest,
            deeplinkJWTToken
        )
}