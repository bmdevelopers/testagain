package co.bytemark.data.credit_pass.remote

import co.bytemark.data.data_store.remote.RemoteEntityStore
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.credit_pass.CreditPassRequest
import kotlinx.coroutines.Deferred

interface CreditPassRemoteEntityStore : RemoteEntityStore {
    fun creditPassAsync(
        creditPassRequest: CreditPassRequest,
        deeplinkJWTToken: String
    ): Deferred<BMResponse>
}