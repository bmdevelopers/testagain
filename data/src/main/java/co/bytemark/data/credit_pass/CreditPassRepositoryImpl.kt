package co.bytemark.data.credit_pass

import co.bytemark.data.RepositoryImpl
import co.bytemark.data.annotation.Local
import co.bytemark.data.annotation.Remote
import co.bytemark.data.credit_pass.local.CreditPassLocalEntityStore
import co.bytemark.data.credit_pass.remote.CreditPassRemoteEntityStore
import co.bytemark.data.net.manager.NetworkManager
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.credit_pass.CreditPassRequest
import co.bytemark.domain.repository.CreditPassRepository
import kotlinx.coroutines.Deferred
import javax.inject.Inject


class CreditPassRepositoryImpl @Inject constructor(
    networkManager: NetworkManager,
    @Remote remoteEntityStore: CreditPassRemoteEntityStore,
    @Local localEntityStore: CreditPassLocalEntityStore
) : RepositoryImpl<CreditPassRemoteEntityStore, CreditPassLocalEntityStore>(
    networkManager,
    remoteEntityStore,
    localEntityStore
), CreditPassRepository {
    override fun creditPassAsync(
        creditPassRequest: CreditPassRequest,
        deeplinkJWTToken: String
    ): Deferred<BMResponse> =
        remoteStore.creditPassAsync(creditPassRequest, deeplinkJWTToken)
}