package co.bytemark.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import org.junit.Before;
import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DeviceInfoTest {

    Context mockContext;
    PackageInfo packageInfo;
    PackageManager mockpackageManager;
    SharedPreferences mockSharedPreferences;
    SharedPreferences.Editor mockSharedPreferencesEditor;

    String fakePkgName;
    String fakeDeviceAppVersion;

    @Before
    public void before() throws PackageManager.NameNotFoundException {
        packageInfo = new PackageInfo();
        packageInfo.versionName = "42.0";
        fakePkgName = "i.like.cheese";
        fakeDeviceAppVersion = "foobar";
        mockContext = mock(Context.class);
        mockpackageManager = mock(PackageManager.class);
        mockSharedPreferences = mock(SharedPreferences.class);
        mockSharedPreferencesEditor = mock(SharedPreferences.Editor.class);

        when(mockContext.getPackageName()).thenReturn(fakePkgName);
        when(mockContext.getPackageManager()).thenReturn(mockpackageManager);
        when(mockpackageManager.getPackageInfo(fakePkgName, 0)).thenReturn(packageInfo);
        when(mockContext.getSharedPreferences(anyString(), anyInt())).thenReturn(mockSharedPreferences);
        when(mockSharedPreferences.getString(anyString(), anyString())).thenReturn("foobar");
        when(mockSharedPreferences.edit()).thenReturn(mockSharedPreferencesEditor);
        when(mockSharedPreferences.edit().putString(anyString(), anyString())).thenReturn(mockSharedPreferencesEditor);
    }

    @Test
    public void testGetOS() {
        assertEquals("Android", DeviceInfo.getOS());
    }

    @Test
    public void testGetOSVersion() {
        assertEquals(android.os.Build.VERSION.RELEASE, DeviceInfo.getOSVersion());
    }

    @Test
    public void testGetDevice() {
        assertEquals(android.os.Build.MODEL, DeviceInfo.getDevice());
    }

    @Test
    public void testGetLocale() {
        final Locale defaultLocale = Locale.getDefault();
        try {
            Locale.setDefault(new Locale("ab", "CD"));
            assertEquals("ab_CD", DeviceInfo.getLocale());
        } finally {
            Locale.setDefault(defaultLocale);
        }
    }

    @Test
    public void testGetDeviceAppVersion() {
        assertEquals(fakeDeviceAppVersion, DeviceInfo.getDeviceAppVersion(mockContext));
    }

    @Test
    public void testSetDeviceAppVersion() {
        DeviceInfo.setDeviceAppVersion(mockContext, fakeDeviceAppVersion);
        assertEquals(fakeDeviceAppVersion, DeviceInfo.getDeviceAppVersion(mockContext));
    }
}
