package co.bytemark.domain.repository

import co.bytemark.domain.model.common.Response
import co.bytemark.gtfs.Agency
import co.bytemark.gtfs.DataObjects.Schedule
import co.bytemark.gtfs.Stop
import java.util.*

interface GTFSRepository : Repository {

    val originStops: Response<List<Stop>>

    fun getDestinations(originId: String?): Response<List<Stop>>

    fun getSchedules(origin: String?, destination: String?, date: Date?): Response<MutableList<Schedule>>

    val agencies: Response<List<Agency>>

}