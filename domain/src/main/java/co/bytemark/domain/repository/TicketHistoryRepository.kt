package co.bytemark.domain.repository

import co.bytemark.domain.model.common.BMResponse
import kotlinx.coroutines.Deferred

interface TicketHistoryRepository : Repository {

    fun getTicketHistoryAsync(perPage: Int, status: String, nextPageURL: String?): Deferred<BMResponse>

}