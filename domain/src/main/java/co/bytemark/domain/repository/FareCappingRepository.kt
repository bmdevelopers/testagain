package co.bytemark.domain.repository

import co.bytemark.domain.interactor.fare_capping.FareCappingResponse
import co.bytemark.domain.model.common.Response

interface FareCappingRepository : Repository {
    suspend fun getFareCapping(): Response<FareCappingResponse>
}