package co.bytemark.domain.repository;

import androidx.annotation.NonNull;

import java.util.List;

import co.bytemark.domain.model.store.filter.Filter;
import co.bytemark.domain.model.store.filter.FilterResult;
import rx.Observable;

public interface FiltersRepository extends Repository {
    @NonNull
    Observable<List<FilterResult>> getResults(@NonNull Filter filter);
}
