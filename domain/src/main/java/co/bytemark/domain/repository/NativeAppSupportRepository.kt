package co.bytemark.domain.repository

import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Response

interface NativeAppSupportRepository : Repository {
    suspend fun sendAppSupportEmailAsync(params: Map<String, String>) : Response<BMResponse>
}