package co.bytemark.domain.repository

import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.store.filter.SearchResponseData
import co.bytemark.sdk.post_body.PostSearch

/**
 * Created by vally on 05/12/19.
 */
interface StoreFiltersRepository : Repository {

    suspend fun getStoreFilters(postSearch: PostSearch): Response<SearchResponseData>

}