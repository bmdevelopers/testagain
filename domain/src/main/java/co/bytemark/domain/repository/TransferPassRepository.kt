package co.bytemark.domain.repository

import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.ticket_storage.TransferPassResponse

interface TransferPassRepository : Repository {
    suspend fun transferPass(
        passUuid: String,
        destination: String
    ): Response<TransferPassResponse>
}