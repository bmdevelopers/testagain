package co.bytemark.domain.repository

import co.bytemark.domain.model.autoload.Autoload
import co.bytemark.domain.model.autoload.DeleteAutoload
import co.bytemark.domain.model.autoload.FareMediumAutoload
import co.bytemark.domain.model.autoload.PostAutoload
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.fare_medium.TransactionsResponse
import co.bytemark.domain.model.init_fare_capping.InitFareCappingData
import co.bytemark.domain.model.manage.*


interface ManageRepository : Repository {

    suspend fun linkExistingCardsAsync(linkExistingCard: LinkExistingCard?): Response<AddSmartCard>

    suspend fun getFareMediaCategoriesAsync(): Response<FareCategoryResponse>

    suspend fun createVirtualCardAsync(virtualCard: VirtualCard?): Response<Data>

    suspend fun getTransactionsAsync(fareMediaId: String?, pageIndex: Int?): Response<TransactionsResponse>

    suspend fun saveAutoload(postAutoload: PostAutoload): Response<Autoload>

    suspend fun updateAutoload(postAutoload: PostAutoload): Response<Autoload>

    suspend fun getAutoload(fareMediumId: String): Response<FareMediumAutoload>

    suspend fun deleteAutoload(fareMediumId: String): Response<DeleteAutoload>

    suspend fun getInitFareCappings(fareMediumId: String): Response<InitFareCappingData>

    suspend fun getInstitutionList(pageIndex: Int): Response<InstitutionListData>

    suspend fun checkUPassEligibility(fareMediumUuid: String, uPassEligibilityRequestData: UPassEligibilityRequestData)
            : Response<UPassEligibilityResponseData>
}