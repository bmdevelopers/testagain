package co.bytemark.domain.repository;

import androidx.annotation.NonNull;

import java.util.Map;

import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.domain.model.order.ApplyPromoCode;
import co.bytemark.sdk.post_body.CreateOrder;
import co.bytemark.sdk.post_body.MakePayment;
import retrofit2.http.Tag;
import rx.Observable;

/**
 * Created by Arunkumar on 10/10/17.
 */
public interface ProductRepository extends Repository {
    @NonNull
    Observable<BMResponse> getPayPalToken();

    @NonNull
    Observable<BMResponse> getAcceptedPayments();

    @NonNull
    Observable<BMResponse> getAcceptedPaymentMethods( String organizationId);

    @NonNull
    Observable<BMResponse> purchaseProducts(CreateOrder createOrder, Map<String, String> queryParameters);

    @NonNull
    Observable<BMResponse> purchaseProductsForIdeal(CreateOrder createOrder, Map<String, String> queryParameters);

    @NonNull
    Observable<BMResponse> makePayment(String orderUUID, MakePayment makePayment, String deepLinkJwtToken);

    @NonNull
    Observable<BMResponse> createOrder(CreateOrder createOrder, String deepLinkJwtToken);

    Observable<BMResponse> applyPromoCode(ApplyPromoCode applyPromoCode);

}
