package co.bytemark.domain.repository

import co.bytemark.domain.interactor.userphoto.UserPhotoResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.userphoto.UserPhoto

interface UserPhotoRepository : Repository {

    suspend fun getUserPhoto(): Response<UserPhotoResponse?>

    suspend fun setUserPhoto(userPhoto: UserPhoto): Response<UserPhotoResponse?>

    suspend fun saveUserPhoto(userPhoto: UserPhoto): Response<UserPhotoResponse?>

}