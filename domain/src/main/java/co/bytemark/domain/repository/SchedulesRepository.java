package co.bytemark.domain.repository;

import java.util.List;
import rx.Observable;

/**
 * Created by vally on 05/10/18.
 */

public interface SchedulesRepository extends Repository {

    Observable<List<String>> getSchedules();

}
