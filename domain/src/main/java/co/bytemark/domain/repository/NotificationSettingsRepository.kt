package co.bytemark.domain.repository

import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.notificationSettings.NotificationSettingTypes
import co.bytemark.domain.model.notificationSettings.NotificationSettingsResponse

interface NotificationSettingsRepository : Repository {
    suspend fun getNotificationsSettings() : Response<NotificationSettingsResponse>
    suspend fun updateNotificationPermissions(notificationSettingTypes: NotificationSettingTypes) : Response<BMResponse>
}