package co.bytemark.domain.repository

import androidx.annotation.Nullable
import co.bytemark.domain.interactor.fare_medium.TransferBalanceRequestData
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents
import kotlinx.coroutines.Deferred
import rx.Observable

interface FareMediumRepository : Repository {

    fun getFareMediums(loadFromLocalStore: Boolean): Observable<List<FareMedium>>

    fun getFareMediumContents(fareMediumUuid: String, loadFromLocalStore: Boolean): Observable<FareMediumContents>

    @Nullable
    fun getAllVirtualCardsAsync(): Deferred<BMResponse>

    @Nullable
    fun transferVirtualCardAsync(fareMediumId: String): Deferred<BMResponse>

    suspend fun updateFareMediumState(fareMediumUuid: String, newState: Int): Response<Any>

    suspend fun removeFareMedium(fareMediumUuid: String): Response<Any>

    suspend fun getAutoLoadConfig(): Response<LoadConfig>

    suspend fun transferBalance(transferBalanceRequestData: TransferBalanceRequestData): Response<Unit>

}