package co.bytemark.domain.repository;

import androidx.annotation.NonNull;

import java.util.List;

import co.bytemark.domain.model.private_key.PrivateKey;
import rx.Observable;

/**
 * Created by Omkar on 9/7/17.
 */
public interface PrivateKeyRepository extends Repository {

    @NonNull
    Observable<List<PrivateKey>> getPrivateKeys();

    @NonNull
    Observable<List<PrivateKey>> savePrivateKeys(@NonNull List<PrivateKey> privateKeys);
}
