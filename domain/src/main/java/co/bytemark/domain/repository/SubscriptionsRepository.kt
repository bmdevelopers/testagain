package co.bytemark.domain.repository

import co.bytemark.domain.model.common.BMResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred

/**
 * Created by vally on 07/06/19.
 */
interface SubscriptionsRepository : Repository {

    fun getSubscriptions(fareMediaId: String?): Deferred<BMResponse>

    fun cancelSubscriptions(uuid : String, jsonObject: JsonObject): Deferred<BMResponse>
}