package co.bytemark.domain.repository

import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.notification.Notification
import co.bytemark.domain.model.notification.NotificationResponse

interface NotificationRepository : Repository {
    suspend fun saveNotifications(notifications : MutableList<Notification>) : MutableList<Notification>
    suspend fun getNotifications() : Response<NotificationResponse>
    suspend infix fun markNotificationAsRead(notificationId : String) : Response<Boolean>
}