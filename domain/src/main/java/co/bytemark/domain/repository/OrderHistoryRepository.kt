package co.bytemark.domain.repository

import co.bytemark.domain.model.common.BMResponse
import kotlinx.coroutines.Deferred

/** Created by Santosh on 16/04/19.
 */

interface OrderHistoryRepository : Repository {

    fun getReceiptsAsync(perPage: Int, pageNo: Int, sortBy: String, order: String): Deferred<BMResponse>
}