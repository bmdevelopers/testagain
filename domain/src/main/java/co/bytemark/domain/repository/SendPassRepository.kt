package co.bytemark.domain.repository

import co.bytemark.domain.model.common.BMResponse
import kotlinx.coroutines.Deferred

interface SendPassRepository : Repository {
    suspend fun sendPassAsync(passUUID: String, receiverEmail: String): Deferred<BMResponse>
}