package co.bytemark.domain.repository

import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.discount.DiscountCalculationRequest
import kotlinx.coroutines.Deferred

interface DiscountRepository : Repository {
    fun discountCalculation(discountCalculationRequest: DiscountCalculationRequest, deeplinkJWTToken: String): Deferred<BMResponse>
}