package co.bytemark.domain.repository

import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.credit_pass.CreditPassRequest
import kotlinx.coroutines.Deferred

interface CreditPassRepository: Repository {
    fun creditPassAsync(
        creditPassRequest: CreditPassRequest,
        deeplinkJWTToken: String
    ): Deferred<BMResponse>
}