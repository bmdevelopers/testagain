package co.bytemark.domain.repository

import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import org.json.JSONObject

/** Created by Santosh on 2019-11-21.
 * Copyright (c) 2019 Bytemark Inc. All rights reserved.
 */

interface PassesRepository : Repository {

    fun transferPassAsync(passUuid: String, destination: String): Deferred<JsonObject>
}