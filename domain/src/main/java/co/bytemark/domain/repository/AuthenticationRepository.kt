package co.bytemark.domain.repository

import co.bytemark.domain.interactor.authentication.ChangePinRequest
import co.bytemark.domain.interactor.authentication.VelociaLoginRequest
import co.bytemark.domain.model.authentication.ChangePinData
import co.bytemark.domain.model.authentication.SignInResponse
import co.bytemark.domain.model.authentication.VelociaLoginData
import co.bytemark.domain.model.common.ApiResponse
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.common.api_response_data.UserProfileData
import kotlinx.coroutines.Deferred
import rx.Observable

/** Created by Santosh Kulkarni on 16/12/19.
 */

interface AuthenticationRepository : Repository {

    suspend fun getOauthToken(loginParams: MutableMap<String, String>): Response<SignInResponse>

    suspend fun registerNewUser(params: MutableMap<String, Any?>): Response<Any>

    fun changePassword(params: MutableMap<String, String>?): Deferred<BMResponse>

    suspend fun resetPassword(params: MutableMap<String, String>): Response<Any>

    fun submitVoucherCode(params: MutableMap<String, String?>): Observable<BMResponse>

    fun deleteAccountAsync(cardUuid: String?): Deferred<BMResponse>

    suspend fun resentVerificationEmail(): Response<Any>

    fun getUserAsync(): Deferred<ApiResponse<UserProfileData>>

    fun updateUserAsync(params: MutableMap<String, String?>): Deferred<BMResponse>

    suspend fun changePin(changePinRequest: ChangePinRequest): Response<ChangePinData>

    suspend fun loginToVelocia(velociaLoginRequest: VelociaLoginRequest): Response<VelociaLoginData>

}