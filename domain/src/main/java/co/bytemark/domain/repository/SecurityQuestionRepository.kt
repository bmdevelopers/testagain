package co.bytemark.domain.repository

import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.security_questions.SecurityQuestionsResponse
import co.bytemark.sdk.model.securityquestions.UpdateSecurityQuestion

interface SecurityQuestionRepository : Repository {

    suspend fun getSecurityQuestions(): Response<SecurityQuestionsResponse>

    suspend fun getSecurityQuestionOfUser(): Response<SecurityQuestionsResponse>

    suspend fun updateSecurityQuestions(
            updateSecurityQuestion: UpdateSecurityQuestion
    ): Response<SecurityQuestionsResponse>
}