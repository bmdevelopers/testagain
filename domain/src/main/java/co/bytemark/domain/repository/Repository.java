package co.bytemark.domain.repository;

import co.bytemark.domain.interactor.UseCase;

public interface Repository {

    void register(UseCase useCase);

    void unregister(UseCase useCase);
}
