package co.bytemark.domain.repository

import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.voucher_code.VoucherCodeData

interface VoucherRedeemRepository : Repository {

    suspend fun redeemVoucherCode(orderType: String, voucherCode:String, saveToDevice:Boolean): Response<VoucherCodeData>
}