package co.bytemark.domain.repository

import co.bytemark.domain.model.common.BMResponse
import kotlinx.coroutines.Deferred

interface ResendReceiptRepository : Repository {

    fun resendReceiptAsync(orderUUID: String): Deferred<BMResponse>

}