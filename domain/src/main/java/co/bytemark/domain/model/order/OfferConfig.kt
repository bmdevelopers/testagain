package co.bytemark.domain.model.order

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OfferConfig(

        @field:SerializedName("time_modified")
        val timeModified: String? = null,

        @field:SerializedName("offer_codes")
        val offerCodes: List<OfferCode?>? = null,

        @field:SerializedName("usage_type")
        val usageType: String? = null,

        @field:SerializedName("price_adjustment")
        val priceAdjustment: PriceAdjustment? = null,

        @field:SerializedName("price_adjustments")
        val priceAdjustments: List<PriceAdjustment>? = null,

        @field:SerializedName("time_created")
        val timeCreated: String? = null,

        @field:SerializedName("discount_type")
        val discountType: String? = null,

        @field:SerializedName("uuid")
        val uuid: String? = null,

        @field:SerializedName("status")
        val status: String? = null
) : Parcelable