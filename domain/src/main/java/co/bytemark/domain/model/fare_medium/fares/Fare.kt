package co.bytemark.domain.model.fare_medium.fares

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Fare(
    @field:SerializedName("uuid")
    val uuid: String,
    @Transient
    val fareMediumUuid: String? = null,
    @field:SerializedName("name")
    val name: String,
    @field:SerializedName("short_description")
    val shortDescription: String,
    @field:SerializedName("org_id")
    val orgId: String,
    @field:SerializedName("is_active")
    val isActive: Boolean,
    @field:SerializedName("expiration")
    val expiration: String,
    @field:SerializedName("uses_count")
    val usesCount: Int? = null,
    @field:SerializedName("allowed_uses")
    val allowedUses: Int? = null
) : Parcelable {

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val (uuid1, fareMediumUuid1, name1, shortDescription1, orgId1, isActive1, expiration1, usesCount1, allowedUses1) = o as Fare
        if (isActive != isActive1) return false
        if (uuid != uuid1) return false
        if (name != name1) return false
        if (shortDescription != shortDescription1) return false
        if (orgId != orgId1) return false
        if (expiration != expiration1) return false
        if (usesCount != usesCount1) return false
        if (allowedUses != allowedUses1) return false
        return if (fareMediumUuid != null) fareMediumUuid == fareMediumUuid1 else fareMediumUuid1 == null
    }

    override fun hashCode(): Int {
        return uuid.hashCode()
    }
}