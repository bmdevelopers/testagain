package co.bytemark.domain.model.authentication

import android.os.Parcelable
import co.bytemark.sdk.model.common.OrganizationDetails
import co.bytemark.sdk.model.common.User
import co.bytemark.sdk.model.user.DeviceAppInstallation
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SignInResponse(
    @SerializedName("user")
    val user: User? = null,

    @SerializedName("device_app_installation")
    val deviceAppInstallation: DeviceAppInstallation? = null,

    @field:SerializedName("oauth_token")
    val oauthToken: String? = null,

    @SerializedName("organization_details")
    val organizationDetails: OrganizationDetails? = null
) : Parcelable