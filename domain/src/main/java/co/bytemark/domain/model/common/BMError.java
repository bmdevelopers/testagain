package co.bytemark.domain.model.common;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.net.ConnectException;
import java.net.UnknownHostException;

import co.bytemark.domain.error.BytemarkException;
import co.bytemark.sdk.BytemarkSDK;
import okhttp3.internal.http2.StreamResetException;
import retrofit2.HttpException;

import static co.bytemark.sdk.BytemarkSDK.ResponseCode.NETWORK_NOT_AVAILABLE;
import static co.bytemark.sdk.BytemarkSDK.ResponseCode.SDK_VERSION_OUT_OF_DATE;
import static co.bytemark.sdk.BytemarkSDK.ResponseCode.UNEXPECTED_ERROR;

public class BMError implements Parcelable {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("description")
    @Expose
    private String description;

    public BMError(Integer code) {
        this.code = code;
        this.message = "";
    }

    public BMError(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static BMError fromError(co.bytemark.sdk.BMError error) {
        return new BMError(error.getCode(), error.getMessage());
    }

    protected BMError(Parcel in) {
        code = in.readInt();
        message = in.readString();
        description = in.readString();
    }

    public static final Creator<BMError> CREATOR = new Creator<BMError>() {
        @Override
        public BMError createFromParcel(Parcel in) {
            return new BMError(in);
        }

        @Override
        public BMError[] newArray(int size) {
            return new BMError[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(code);
        parcel.writeString(message);
        parcel.writeString(description);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static BMError fromThrowable(Throwable throwable) {
        if (throwable instanceof BytemarkSDK.SpecialException) {
            return new BMError(SDK_VERSION_OUT_OF_DATE, "");
        } else if (throwable instanceof HttpException) {
            return new BMError(((HttpException) throwable).code(), "");
        } else if (throwable instanceof BytemarkException) {
            return new BMError(((BytemarkException) throwable).getStatusCode(), "");
        } else if (throwable instanceof UnknownHostException || throwable instanceof StreamResetException || throwable instanceof ConnectException) {
            return new BMError(NETWORK_NOT_AVAILABLE, "");
        } else {
            return new BMError(UNEXPECTED_ERROR, "");
        }
    }
}