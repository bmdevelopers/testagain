package co.bytemark.domain.model.common;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BMResponse implements Parcelable {

    @SerializedName("errors")
    @Expose
    private List<BMError> errors = new ArrayList<>();
    @SerializedName("server_time")
    @Expose
    private String serverTime;
    @SerializedName("data")
    @Expose
    private Data data;

    public BMResponse(){}

    protected BMResponse(Parcel in) {
        errors = in.createTypedArrayList(BMError.CREATOR);
        serverTime = in.readString();
        data = in.readParcelable(Data.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(errors);
        dest.writeString(serverTime);
        dest.writeParcelable(data, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BMResponse> CREATOR = new Creator<BMResponse>() {
        @Override
        public BMResponse createFromParcel(Parcel in) {
            return new BMResponse(in);
        }

        @Override
        public BMResponse[] newArray(int size) {
            return new BMResponse[size];
        }
    };

    public List<BMError> getErrors() {
        return errors;
    }

    public void setErrors(List<BMError> errors) {
        this.errors = errors;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BMResponse that = (BMResponse) o;

        if (errors != null ? !errors.equals(that.errors) : that.errors != null) return false;
        if (serverTime != null ? !serverTime.equals(that.serverTime) : that.serverTime != null)
            return false;
        return data != null ? data.equals(that.data) : that.data == null;

    }

    @Override
    public int hashCode() {
        int result = errors != null ? errors.hashCode() : 0;
        result = 31 * result + (serverTime != null ? serverTime.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BMResponse{" +
                "errors=" + errors +
                ", serverTime='" + serverTime + '\'' +
                ", data=" + data +
                '}';
    }

}