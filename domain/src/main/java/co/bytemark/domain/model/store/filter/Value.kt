package co.bytemark.domain.model.store.filter

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Value(
    @SerializedName("uuid") val uuid: String? = null,
    @SerializedName("value") val value: String? = null,
    @SerializedName("image_url") val imageUrl: String? = null,
    @SerializedName("short_description") val shortDescription: String? = null,
    @SerializedName("width") val width: String? = null,
    @SerializedName("has_next") val isHasNext: Boolean = false

) : Parcelable {

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val value1 = o as Value?

        if (if (uuid != null) uuid != value1!!.uuid else value1!!.uuid != null) return false
        return if (value != null) value == value1.value else value1.value == null

    }

    override fun hashCode(): Int {
        var result = uuid?.hashCode() ?: 0
        result = 31 * result + (value?.hashCode() ?: 0)
        return result
    }
}

