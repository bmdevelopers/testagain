package co.bytemark.domain.model.order

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class Order(
    @field:SerializedName("uuid")
    val uuid: String,
    @field:SerializedName("reference_number")
    val referenceNumber: String,
    @field:SerializedName("time_purchased")
    val timePurchased: String,
    @field:SerializedName("time_paid")
    val timePaid: String,
    @field:SerializedName("time_fulfilled")
    val timeFulfilled: String,
    @field:SerializedName("time_created")
    val timeCreated: String,
    @field:SerializedName("time_modified")
    val timeModified: String,
    @field:SerializedName("notes")
    val notes: String,
    @field:SerializedName("subtotal")
    val subtotal: Int,
    @field:SerializedName("tax")
    val tax: Int,
    @field:SerializedName("total")
    val total: Int,
    @field:SerializedName("original_total")
    val originalTotal: Int,
    @field:SerializedName("total_overridden")
    val totalOverridden: Boolean,
    @SerializedName("total_override_description")
    var totalOverrideDescription: @RawValue Any? = null,
    @field:SerializedName("customer")
    val customer: Customer,
    @field:SerializedName("client")
    val client: Client,
    @field:SerializedName("merchant_uuid")
    val merchantUuid: String,
    @field:SerializedName("merchant_name")
    val merchantName: String,
    @field:SerializedName("merchant_currency")
    val merchantCurrency: String,
    @field:SerializedName("status")
    val status: String,
    @field:SerializedName("billing_address")
    val billingAddress: @RawValue Any? = null,
    @field:SerializedName("shipping_address")
    val shippingAddress: @RawValue Any? = null,
    @field:SerializedName("shipping_method")
    val shippingMethod: @RawValue Any? = null,
    @field:SerializedName("delivery_method")
    val deliveryMethod: String,
    @field:SerializedName("order_items_count")
    val orderItemsCount: Int,
    @field:SerializedName("payment_total")
    val paymentTotal: Int,
    @field:SerializedName("payments")
    var payments: MutableList<Payment> = ArrayList()
) : Parcelable