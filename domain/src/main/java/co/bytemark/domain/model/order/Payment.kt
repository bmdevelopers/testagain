package co.bytemark.domain.model.order

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Payment(
    @field:SerializedName("uuid")
    val uuid: String,
    @field:SerializedName("gateway_activity_uuid")
    val gatewayActivityUuid: String,
    @field:SerializedName("amount")
    val amount: Int,
    @field:SerializedName("card_type")
    val cardType: String,
    @field:SerializedName("card_last_four")
    val cardLastFour: String,
    @field:SerializedName("status_name")
    val statusName: String,
    @field:SerializedName("authorization_code")
    val authorizationCode: String?,
    @field:SerializedName("sender_uuid")
    val senderUuid: String,
    @field:SerializedName("receiver_uuid")
    val receiverUuid: String,
    @field:SerializedName("time_created")
    val timeCreated: String,
    @field:SerializedName("time_modified")
    val timeModified: String
) : Parcelable