package co.bytemark.domain.model.store.filter

import android.os.Parcelable
import co.bytemark.sdk.model.subscriptions.InputFields
import co.bytemark.sdk.model.subscriptions.ProductSubscription
import co.bytemark.sdk.model.common.Attributes
import co.bytemark.sdk.model.product_search.entity.BaseProduct
import co.bytemark.sdk.model.product_search.entity.LegacyProduct
import co.bytemark.sdk.model.product_search.entity.Organization
import co.bytemark.sdk.model.product_search.entity.Theme
import co.bytemark.sdk.model.product_search.filter.FieldValue
import co.bytemark.sdk.model.product_search.filter.Format
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class FilterResult(

    @SerializedName("uuid")
    @Expose
    var uuid: String?,

    @SerializedName("organization")
    @Expose
    var organization: Organization?,

    @SerializedName("cost_price")
    @Expose
    var costPrice: Int,

    @SerializedName("list_price")
    @Expose
    var listPrice: Int,

    @SerializedName("alert_message")
    @Expose
    var alertMessage: String?,

    @SerializedName("sale_price")
    @Expose
    var salePrice: Int,

    @SerializedName("name")
    @Expose
    var name: String?,

    @SerializedName("short_description")
    @Expose
    var shortDescription: String?,

    @SerializedName("long_description")
    @Expose
    var longDescription: String?,

    @SerializedName("sku")
    @Expose
    var sku: String?,

    @SerializedName("status")
    @Expose
    var status: Int,

    @SerializedName("time_created")
    @Expose
    var timeCreated: String?,

    @SerializedName("time_modified")
    @Expose
    var timeModified: String?,

    @SerializedName("base_product")
    @Expose
    var baseProduct: BaseProduct?,

    @SerializedName("theme")
    @Expose
    var theme: Theme?,

    @SerializedName("attributes")
    @Expose
    var attributes: List<Attributes>? = ArrayList(),

    @SerializedName("legacy_product")
    @Expose
    var legacyProduct: LegacyProduct?,

    @SerializedName("type")
    @Expose
    var type: String?,

    @SerializedName("data_type")
    @Expose
    var dataType: String?,

    @SerializedName("display_name")
    @Expose
    var displayName: String?,

    @SerializedName("placeholder_text")
    @Expose
    var placeholderText: String?,

    @SerializedName("target_entity")
    @Expose
    var targetEntity: String?,

    @SerializedName("required")
    @Expose
    var required: Boolean,

    @SerializedName("format")
    @Expose
    var format: Format?,

    @SerializedName("field_keyname")
    @Expose
    var fieldKeyname: String?,

    @SerializedName("field_values")
    @Expose
    var fieldValues: List<FieldValue>? = ArrayList(),

    @SerializedName("label_name")
    @Expose
    var labelName: String?,

    @SerializedName("filter_type")
    @Expose
    var filterType: String?,

    @SerializedName("values")
    @Expose
    var values: List<Value>? = ArrayList(),

    @SerializedName("applied")
    @Expose
    var appliedFilters: List<AppliedFilter>? = ArrayList(),

    @SerializedName("product_image_path")
    @Expose
    var productImagePath: String? = null,

    @SerializedName("subscription")
    @Expose
    var subscription: ProductSubscription?,

    @SerializedName("create_subscription")
    @Expose
    var isCreateSubscription: Boolean = false,

    @SerializedName("input_field_required")
    @Expose
    var inputFieldRequired: List<String>?,

    var inputFields: InputFields = InputFields(),

    var quantity: Int
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as FilterResult
        return uuid == that.uuid
    }

    override fun hashCode(): Int {
        return uuid.hashCode()
    }

    companion object {
        const val DROPDOWN = "dropdown"
        const val CATEGORY = "category"
        const val LIST = "list"
	}
}