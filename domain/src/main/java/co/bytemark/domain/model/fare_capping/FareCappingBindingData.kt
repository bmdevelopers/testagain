package co.bytemark.domain.model.fare_capping

import android.text.SpannableStringBuilder

data class FareCappingBindingData(
        val type: SpannableStringBuilder,
        val currentQuantity: Int,
        val targetQuantity: Int,
        val statusMessage: SpannableStringBuilder,
        val description: String,
        val formattedCycleStartTime: String,
        val formattedCycleEndTime: String,
        val formattedMoreInfo: String
)
