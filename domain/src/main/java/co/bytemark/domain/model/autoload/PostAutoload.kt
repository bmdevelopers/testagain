package co.bytemark.domain.model.autoload

import android.os.Parcelable
import co.bytemark.sdk.post_body.Payment
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/** Created by Santosh on 2019-08-26.
 */

@Parcelize
data class PostAutoload(

    @field:SerializedName("payments")
    var payments: List<Payment>? = null,

    @field:SerializedName("faremedia_id")
    var faremediaId: String? = null,

    @field:SerializedName("trace_number")
    var traceNumber: String? = null,

    @field:SerializedName("lat")
    var lat: Double? = null,

    @field:SerializedName("lon")
    var lon: Double? = null,

    @field:SerializedName("fulfill_digital_passes")
    var fulfillDigitalPasses: Boolean? = null,

    @field:SerializedName("process")
    var process: Boolean? = null,

    @field:SerializedName("save_to_device")
    var saveToDevice: Boolean? = null,

    @field:SerializedName("autoload_threshold_value")
    var autoloadThresholdValue: Int? = null,

    @field:SerializedName("autoload_value")
    var autoloadValue: Int? = null,

    @field:SerializedName("auto_load_value_id")
    var autoloadValueId: Int? = null

) : Parcelable