package co.bytemark.domain.model.discount

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Discount(
    @field:SerializedName("discount_type") val discountType: String?,
    @field:SerializedName("discount_percentage") val discountPercentage: String?,
) : Parcelable