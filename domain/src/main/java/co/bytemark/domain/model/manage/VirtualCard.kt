package co.bytemark.domain.model.manage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class VirtualCard(
        @SerializedName("oauth_token")
        @Expose
        var oauthToken: String? = null,

        @SerializedName("faremedia_id")
        @Expose
        var faremediaId: String? = null,

        @SerializedName("faremedia_nickname")
        @Expose
        var faremediaNickname: String? = null
)