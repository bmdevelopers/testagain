package co.bytemark.domain.model.manage

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FareCategory(

        @field:SerializedName("farecategory_id")
        val fareMediaId: String? = null,

        @field:SerializedName("farecategory_name")
        val fareCategoryName: String? = null

) : Parcelable

data class FareCategoryResponse(@field:SerializedName("farecategories") val fareCategories: List<FareCategory> = mutableListOf())