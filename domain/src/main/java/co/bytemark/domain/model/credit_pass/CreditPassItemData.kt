package co.bytemark.domain.model.credit_pass

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CreditPassItemData(
    @field:SerializedName("product_uuid") val productUuid: String?,
    @field:SerializedName("qty") val qty: Int?,
) : Parcelable