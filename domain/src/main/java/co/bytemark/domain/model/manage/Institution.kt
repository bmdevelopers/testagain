package co.bytemark.domain.model.manage

import com.google.gson.annotations.SerializedName

data class Institution(

        @SerializedName("institution_id")
        val institutionId: String,

        @SerializedName("name")
        val name: String
)

data class InstitutionListData(

        @SerializedName("institutions")
        val institutions: List<Institution>

)