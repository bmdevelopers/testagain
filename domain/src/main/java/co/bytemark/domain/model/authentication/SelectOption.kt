package co.bytemark.domain.model.authentication

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SelectOption(
    @field:SerializedName("id")
    val id: String,
    @field:SerializedName("name")
    var name: String,
    var type: String,
    var items: List<SelectOption>
) : Parcelable