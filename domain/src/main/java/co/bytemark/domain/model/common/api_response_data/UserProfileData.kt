package co.bytemark.domain.model.common.api_response_data

import co.bytemark.sdk.model.common.OrganizationDetails
import co.bytemark.sdk.model.common.User
import co.bytemark.sdk.model.user.DeviceAppInstallation
import com.google.gson.annotations.SerializedName

/**
 * Created by Ranjith on 24/04/20.
 */
data class UserProfileData(
    @field:SerializedName("organization_details")
    val organizationDetails: OrganizationDetails?,
    @field:SerializedName("user")
    val user: User,
    @field:SerializedName("device_app_installation")
    val deviceAppInstallation: DeviceAppInstallation
)