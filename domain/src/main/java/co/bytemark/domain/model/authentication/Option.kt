package co.bytemark.domain.model.authentication

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Option(
    @SerializedName("name")
    @Expose
    var name: String?,
    @SerializedName("value")
    @Expose
    var value: String?
) : Parcelable