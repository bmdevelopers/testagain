package co.bytemark.domain.model.order

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OfferCode(
        @field:SerializedName("time_modified")
        val timeModified: String? = null,

        @field:SerializedName("time_created")
        val timeCreated: String? = null,

        @field:SerializedName("text")
        val text: String? = null,

        @field:SerializedName("uuid")
        val uuid: String? = null,

        @field:SerializedName("status")
        val status: String? = null
) : Parcelable