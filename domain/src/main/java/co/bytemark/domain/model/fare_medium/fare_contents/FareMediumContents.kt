package co.bytemark.domain.model.fare_medium.fare_contents

import android.os.Parcelable
import co.bytemark.domain.model.fare_medium.fares.Fare
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/** Created by Santosh on 30/04/20.
 * Copyright (c) 2020 Bytemark Inc. All rights reserved.
 */

@Parcelize
data class FareMediumContents(
        @field:SerializedName("stored_value")
        var storedValue: String?,

        @field:SerializedName("fares")
        var fares: List<Fare> = mutableListOf(),

        @field:SerializedName("transfer_time")
        var transferTime: String?
) : Parcelable
