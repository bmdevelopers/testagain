package co.bytemark.domain.model.common

import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.Utils
import com.google.gson.annotations.SerializedName
import java.util.*

data class Response<T>(
    @SerializedName("errors")
    val errors: List<BMError> = mutableListOf(),

    @SerializedName("server_time")
    val serverTime: String? = Utils.convertCalenderToServerTimeString(Calendar.getInstance()),

    @SerializedName("data")
    val data: T
) {
    fun getErrorIfAny(): BMError? {
        if (hasErrors()) {
            val code: Int = errors.first().code
            val msg: String = errors.first().message ?: ""
            return BMError(code, msg)
        } else if (serverTime == null || !Utils.isServerTimeWithin5MinuteRange(serverTime)) {
            return BMError(BytemarkSDK.ResponseCode.TIME_INCONSISTENCY_DETECTED, "")
        } else if (data == null) {
            return BMError(BytemarkSDK.ResponseCode.UNEXPECTED_ERROR)
        }
        return null
    }

    private fun hasErrors(): Boolean {
        return errors.isNotEmpty()
    }
}