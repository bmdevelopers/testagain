package co.bytemark.domain.model.incomm

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Incomm(
    @SerializedName("id")
    @Expose
    val id: String,

    @SerializedName("hours")
    @Expose
    val hours: List<String>? = null,

    @SerializedName("phones")
    @Expose
    val phones: List<String>?,

    @SerializedName("merchant")
    @Expose
    val merchant: Merchant,

    @SerializedName("distance")
    @Expose
    val distance: Double?,

    @SerializedName("address")
    @Expose
    val address: Address?,

    @SerializedName("coordinates")
    @Expose
    val coordinates: Coordinates?
) : Parcelable

@Parcelize
data class Merchant(
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("logo_url")
    @Expose
    val logoUrl: String?,
    @SerializedName("icon_url")
    @Expose
    val iconUrl: String?
) : Parcelable

@Parcelize
data class Coordinates(
    @SerializedName("latitude")
    @Expose
    val latitude: Double?,
    @SerializedName("longitude")
    @Expose
    val longitude: Double?
) : Parcelable

@Parcelize
data class Address(
    @SerializedName("address1")
    @Expose
    val address1: String?,
    @SerializedName("city")
    @Expose
    val city: String?,
    @SerializedName("county")
    @Expose
    val county: String?,
    @SerializedName("state")
    @Expose
    val state: String?,
    @SerializedName("zip_code")
    @Expose
    val zipCode: String?,
    @SerializedName("country")
    @Expose
    val country: String?,
    @SerializedName("formatted_address")
    @Expose
    val formattedAddress: String?
) : Parcelable