package co.bytemark.domain.model.notificationSettings

data class NotificationSettingsResponse(val notificationSettings: List<NotificationSettings>? = mutableListOf())