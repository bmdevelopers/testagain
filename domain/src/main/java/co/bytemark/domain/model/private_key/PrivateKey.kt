package co.bytemark.domain.model.private_key

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PrivateKey(
    @field:SerializedName("id")
    val id: String,
    @field:SerializedName("valid_from")
    val validFrom: String,
    @field:SerializedName("valid_to")
    val validTo: String,
    @field:SerializedName("encrypted_private_key")
    val encryptedPrivateKey: String,
    @field:SerializedName("iv")
    val iv: String
) : Parcelable