package co.bytemark.domain.model.common

import com.google.gson.annotations.SerializedName

/**
 * Created by Ranjith on 23/04/20.
 */
data class ApiResponse<T> constructor(

        @SerializedName("errors")
        val errors: List<BMError>,

        @SerializedName("server_time")
        val serverTime: String,

        @SerializedName("data")
        val data: T
) {
    fun hasErrors(): Boolean {
        //TODO 29/04/20 ranjith: Add server timing mismatch related check here
        return errors.isNotEmpty()
    }
}