package co.bytemark.domain.model.discount

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DiscountCalculationRequest(
    @field:SerializedName("products") val products: List<DiscountProduct>? = ArrayList(),
    @field:SerializedName("discount") val discount: Discount?
) : Parcelable