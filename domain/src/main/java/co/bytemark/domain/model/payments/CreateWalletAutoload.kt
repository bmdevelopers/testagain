package co.bytemark.domain.model.payments

import co.bytemark.sdk.post_body.Payment
import com.google.gson.annotations.SerializedName

data class CreateWalletAutoload(

    @field:SerializedName("autoload_value")
    var autoloadValue: Int,

    @field:SerializedName("autoload_threshold_value")
    var autoloadThresholdValue: Int,

    @field:SerializedName("is_enable_auto_load")
    var isEnableAutoLoad: Boolean,

    @field:SerializedName("payments")
    var payments: MutableList<Payment>,

    @field:SerializedName("wallet_uuid")
    var walletUuid: String,

    @field:SerializedName("auto_load_value_id")
    var autoloadValueId: Int?
)