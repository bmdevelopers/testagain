package co.bytemark.domain.model.manage

import com.google.gson.annotations.SerializedName

data class UPassEligibilityRequestData(
        @SerializedName("faremedia_uuid")
        val fareMediaUuid: String
)

data class UPassEligibilityResponseData(
        @SerializedName("message")
        val message: String
)