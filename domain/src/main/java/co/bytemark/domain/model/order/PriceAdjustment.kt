package co.bytemark.domain.model.order

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PriceAdjustment(

        @field:SerializedName("full_label")
        val fullLabel: String? = null,

        @field:SerializedName("adjustment_description")
        val adjustmentDescription: String? = null,

        @field:SerializedName("statement_trigger")
        val statementTrigger: String? = null,

        @field:SerializedName("reselling_organization_name")
        val resellingOrganizationName: String? = null,

        @field:SerializedName("percentage_adjustment")
        val percentageAdjustment: Boolean? = null,

        @field:SerializedName("list_price")
        val listPrice: Int? = null,

        @field:SerializedName("contract_price")
        val contractPrice: Int? = null,

        @field:SerializedName("adjusted_price")
        val adjustedPrice: Int? = null,

        @field:SerializedName("uuid")
        val uuid: String? = null,

        @field:SerializedName("product_name")
        val productName: String? = null,

        @field:SerializedName("reselling_organization_uuid")
        val resellingOrganizationUuid: String? = null,

        @field:SerializedName("product_sub_type")
        val productSubType: String? = null,

        @field:SerializedName("product_uuid")
        val productUuid: String? = null,

        @field:SerializedName("product_type")
        val productType: String? = null,

        @field:SerializedName("adjusted_percentage")
        val adjustedPercentage: Double? = null,

        @field:SerializedName("generated_label")
        val generatedLabel: String? = null,

        @field:SerializedName("status")
        val status: String? = null

) : Parcelable