package co.bytemark.domain.model.autoload

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/** Created by Santosh on 2019-08-26.
 */

@Parcelize
data class Payment(
    @field:SerializedName("card_uuid")
    var cardUuid: String? = null,
    @SerializedName("amount")
    var amount: Int? = null,
    @field:SerializedName("cvv2")
    var cvv: String? = null
) : Parcelable