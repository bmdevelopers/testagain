package co.bytemark.domain.model.init_fare_capping

import com.google.gson.annotations.SerializedName

/**
 * Created by ranjith on 27/05/20
 */
data class InitFareCappingData(
        @SerializedName("result")
        val fareCappingList: List<InitFareCapping>
)