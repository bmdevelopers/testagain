package co.bytemark.domain.model.store.filter

import com.google.gson.annotations.SerializedName

data class SearchResponseData(
        @SerializedName("results")
        val filterResults: List<FilterResult>
)