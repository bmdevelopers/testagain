package co.bytemark.domain.model.store.filter

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Sort(
    @field:SerializedName("key_name")
    val keyName: String,
    @field:SerializedName("order")
    val order: String
) : Parcelable