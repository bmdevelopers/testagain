package co.bytemark.domain.model.common

import android.app.Activity
import android.content.Intent
import androidx.fragment.app.Fragment


/** Created by Santosh on 17/04/19.
 */

sealed class Result<out T> {

    data class Loading<out T>(val data: T?) : Result<T>()
    data class Success<out T>(val data: T?) : Result<T>()
    data class Failure<out T>(val bmError: List<BMError>) : Result<T>()
    data class Error<out T>(val bmError: BMError) : Result<T>()
}


sealed class Navigate {
    class StartActivity(val intent: Intent) : Navigate()
    class StartActivityForResult(val intent: Intent, val requestCode: Int) : Navigate()
    class FinishActivity(val result : Int = Activity.RESULT_CANCELED) : Navigate()
    class ReplaceFragment(val fragment: Fragment, val containerId: Int, val addToBackStack: Boolean = false) : Navigate()
    class AddFragment(val fragment: Fragment, val containerId: Int, val addToBackStack: Boolean = false) : Navigate()
}

sealed class Display {
    class Dialog(val title: Int, val message: Int) : Display()
    class SnackBar(val message: Int, val length: Int) : Display()
    class Toast(val message: Int, val length: Int) : Display()
    class SwipeRefreshLayout(val isRefreshing: Boolean) : Display()
    class ProgressBar(val visibility: Int) : Display()

    sealed class EmptyState {
        class Loading(val drawable: Int = -1, val title: Int = -1, val emptyText: Int? =
                null) :
                Display()

        class Error(val errorImageDrawable: Int = -1, val errorTextTitle: Int = -1, val
        errorTextContent: Int? = null, val errorButtonText: Int? = null) : Display()

        class ShowContent : Display()
        class HideContent : Display()
        class ShowNoData(val drawable: Int = -1, val descriptionText: Int = -1) : Display()
    }

}