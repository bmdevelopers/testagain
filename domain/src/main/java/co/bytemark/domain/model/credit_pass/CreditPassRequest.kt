package co.bytemark.domain.model.credit_pass

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CreditPassRequest(
    @field:SerializedName("items") val items: List<CreditPassItemData>? = ArrayList(),
    @field:SerializedName("save_to_device") val saveToDevice: Boolean?,
    @field:SerializedName("deep_link_credit_enabled") val deeplinkCreditEnabled: Boolean?
) : Parcelable