package co.bytemark.domain.model.init_fare_capping

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/** Created by Santosh on 2019-08-26.
 */

@Parcelize
data class InitFareCapping(

        @field:SerializedName("pot_id")
        val potId: Int? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("valid_from")
        val validFrom: String? = null,

        @field:SerializedName("valid_to")
        val validTo: String? = null,

        @field:SerializedName("amount")
        val amount: Int? = null,

        @field:SerializedName("current_value")
        val currentValue: Int? = null,

        @field:SerializedName("missing_value")
        val missingValue: Int? = null,

        @field:SerializedName("description")
        val description: String? = null,

        @SerializedName("percentage_value")
        val percentageValue: Float? = null

) : Parcelable