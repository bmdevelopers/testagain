package co.bytemark.domain.model.fare_medium

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/** Created by Santosh on 2019-08-26.
 */

@Parcelize
class Transaction(

        @field:SerializedName("transaction_id")
        val transactionId: String? = null,

        @field:SerializedName("time")
        val time: String? = null,

        @field:SerializedName("description")
        val description: String? = null,

        @field:SerializedName("location")
        val location: String? = null,

        @field:SerializedName("transaction_type")
        val transactionType: String? = null,

        @field:SerializedName("amount")
        val amount: Int? = null,

        @field:SerializedName("balance")
        val balance: Int? = null

) : Parcelable