package co.bytemark.domain.model.notification

data class NotificationResponse(val notifications: List<Notification>? = mutableListOf())