package co.bytemark.domain.model.store.filter

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AppliedFilter(
    @field:SerializedName("filter_uuid")
    val filterUuid: String,
    @field:SerializedName("filter_value")
    val filterValue: String,
    @field:SerializedName("uuid")
    val uuid: String,
    @field:SerializedName("value")
    val value: String,
    @field:SerializedName("type")
    val type: String,
    @field:SerializedName("short_name")
    val shortName: String,
    @field:SerializedName("long_name")
    val longName: String,
    @field:SerializedName("key")
    val key: String
) : Parcelable