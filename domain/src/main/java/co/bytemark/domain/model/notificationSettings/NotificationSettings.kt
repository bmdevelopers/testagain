package co.bytemark.domain.model.notificationSettings

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationSettings (@field:SerializedName("name")
                                 var name: String?,
                                 @field:SerializedName("children")
                                 var notificationSettingTypes: List<NotificationSettingTypes>? = mutableListOf()) : Parcelable
