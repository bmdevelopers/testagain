package co.bytemark.domain.model.store.filter

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Filter(
    @field:SerializedName("return")
    val _return: String,
    @field:SerializedName("applied_filters")
    val appliedFilters: MutableList<AppliedFilter> = ArrayList(),
    @field:SerializedName("client_id")
    val clientId: String,
    @field:SerializedName("sort")
    private var sort: MutableList<Sort> = ArrayList()
) : Parcelable