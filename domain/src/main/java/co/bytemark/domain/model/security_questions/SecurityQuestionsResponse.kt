package co.bytemark.domain.model.security_questions

import co.bytemark.sdk.model.securityquestions.SecurityQuestionInfo
import com.google.gson.annotations.SerializedName

/** Created by Santosh on 06/07/20.
 * Copyright (c) 2020 Bytemark Inc. All rights reserved.
 */

data class SecurityQuestionsResponse(
        @field:SerializedName("display_limit")
        val securityQuestionDisplayLimit: Int = 0,

        @SerializedName("security_questions")
        val securityQuestionInfoList: MutableList<SecurityQuestionInfo>
)