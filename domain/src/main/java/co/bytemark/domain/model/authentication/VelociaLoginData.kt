package co.bytemark.domain.model.authentication

import com.google.gson.annotations.SerializedName

data class VelociaLoginData(
        @SerializedName("redirectUrl")
        val redirectUrl: String
)
