package co.bytemark.domain.model.order

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Client(
    @field:SerializedName("client_id")
    val clientId: String,
    @field:SerializedName("name")
    val name: String
) : Parcelable