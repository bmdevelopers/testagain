package co.bytemark.domain.model.fare_capping

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class FareCapping(
    val uuid: String,
    val type: String,
    @SerializedName("cycle_start_time") val cycleStartTime: String,
    @SerializedName("cycle_end_time") val cycleEndTime: String,
    @SerializedName("current_quantity") val currentQuantity: Int,
    @SerializedName("target_quantity") val targetQuantity: Int,
    val description: String,
    @SerializedName("more_info") val moreInfo: List<String>,
    @SerializedName("org_name") val organizationName: String
)

