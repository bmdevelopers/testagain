package co.bytemark.domain.model.autoload

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WalletCalendarAutoload(
    @field:SerializedName("threshold_amount")
    var autoloadThresholdValue: Int,
    @field:SerializedName("amount")
    var autoloadValue: Int,
    @field:SerializedName("card_uuid")
    val cardUuid: String?,
    @field:SerializedName("autoload_config_name")
    val autoloadConfigName: String?,
    @field:SerializedName("autoload_config_value_id")
    val autoloadConfigValueId: Int?,
    @field:SerializedName("autoload_config_value")
    val autoloadConfigValue: String?,
    @field:SerializedName("paypal_uuid")
    val paypalUuid: String?
) : Parcelable