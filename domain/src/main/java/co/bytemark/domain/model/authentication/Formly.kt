package co.bytemark.domain.model.authentication

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Keep
@Parcelize
data class Formly(
    @field:SerializedName("key")
    var key: String,
    @field:SerializedName("type")
    var type: String,
    @field:SerializedName("templateOptions")
    var templateOptions: TemplateOptions,
    @field:SerializedName("optionsTypes")
    var optionsTypes: List<String> = ArrayList(),
    @field:SerializedName("defaultValue")
    var defaultValue: Boolean? = false,
    @field:SerializedName("data")
    var data: SelectData,
    var child: List<Formly> = ArrayList()
) : Parcelable {

    companion object {
        const val USERNAME_KEY = "username"
        const val PHONE_KEY = "phone"
        const val MOBILE_KEY = "mobile"
        const val VOUCHER_CODE = "voucher_code" // TODO: 9/7/17 This key is not finalized.
        const val RESET_PASSWORD_KEY = "reset_password"
        const val CHANGE_PASSWORD_KEY = "change_password"
        const val UPDATE_KEY = "update"
        const val DELETE_KEY = "delete_account"
        const val INIT_DELETE_KEY = "init_delete_account"
        const val NEW_PASSWORD_KEY = "new_password"
        const val CONFIRM_NEW_PASSWORD_KEY = "confirm_new_password"
        const val PASSWORD_KEY = "password"
        const val EMAIL_KEY = "email"
        const val CONFIRM_PASSWORD_KEY = "confirm_password"
        const val CONFIRM_EMAIL_KEY = "confirm_email"
        const val SIGN_UP_KEY = "sign_up"
        const val FIRST_NAME_KEY = "first_name"
        const val LAST_NAME_KEY = "last_name"
        const val SHIPPING = "Shipping"
        const val ADDRESS_LINE_1 = "address_line1"
        const val ADDRESS_LINE_2 = "address_line2"
        const val POSTAL_CODE = "postal_code"
        const val CITY = "city"
        const val STATE = "state"
        const val COUNTRY = "country"
        const val INPUT_TYPE = "input"
        const val MASKED_INPUT_TYPE = "maskedInput"
        const val CHECKBOX_TYPE = "checkbox"
        const val BUTTON_TYPE = "button"
        const val TEXT_TYPE = "text"
        const val DATE_PICKER_TYPE = "datepicker"
        const val SELECT_TYPE = "select"
        const val SWITCH_TYPE = "switch"
        const val NATIVE_APP_SUPPORT = "app_support_submit"
        const val EMAIL_ADDRESS_KEY = "email_address"
        const val MESSAGE = "message"
        const val MULTI_SELECT = "multi_select"
    }
}