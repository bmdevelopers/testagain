package co.bytemark.domain.model.userphoto

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserPhoto(
    @SerializedName("uuid")
    @Expose
    var uuid: String? = null,

    @SerializedName("data")
    @Expose
    var data: String? = null,

    @SerializedName("time_created")
    @Expose
    var timeCreated: String? = null,

    @SerializedName("status")
    @Expose
    var status: String? = null,

    @SerializedName("reviewer")
    @Expose
    var reviewer: Reviewer? = null,

    @SerializedName("time_reviewed")
    @Expose
    var timeReviewed: String? = null
) : Parcelable {

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val that = o as UserPhoto
        return uuid == that.uuid
    }

    override fun hashCode(): Int {
        return uuid.hashCode()
    }

    companion object {
        const val MISSING = "MISSING"
        const val REJECTED = "REJECTED"
        const val PENDING = "PENDING"
        const val ACCEPTED = "ACCEPTED"
    }
}