package co.bytemark.domain.model.ticket_storage

import co.bytemark.sdk.Pass
import com.google.gson.annotations.SerializedName

data class TransferPassResponse(
    @SerializedName("passes") var passes: List<Pass>?
)