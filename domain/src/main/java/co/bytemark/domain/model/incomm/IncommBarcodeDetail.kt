package co.bytemark.domain.model.incomm

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class IncommBarcodeDetail(
    @SerializedName("account_number")
    val accountNumber: String,
    @SerializedName("icon_url")
    val iconUrl: String,
    @SerializedName("org_name")
    val orgName: String
) : Parcelable