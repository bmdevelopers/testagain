package co.bytemark.domain.model.payments

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Card(
    @field:SerializedName("uuid")
    val uuid: String,
    @field:SerializedName("nickname")
    val nickname: String,
    @field:SerializedName("type_id")
    val typeId: Int,
    @field:SerializedName("type_name")
    val typeName: String,
    @field:SerializedName("last_four")
    val lastFour: String,
    @field:SerializedName("expiration_date")
    val expirationDate: String,
    @field:SerializedName("cvv_required")
    val cvvRequired: Boolean = false,
    @field:SerializedName("is_default")
    val isDefault: Boolean
) : Parcelable