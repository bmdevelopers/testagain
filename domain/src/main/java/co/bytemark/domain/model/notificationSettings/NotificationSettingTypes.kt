package co.bytemark.domain.model.notificationSettings

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationSettingTypes (@field:SerializedName("name")
                                     var name: String?,
                                     @field:SerializedName("uuid")
                                     var uuid: String?,
                                     @field:SerializedName("notification_permissions")
                                     var notificationPermissions: List<NotificationPermission>? = mutableListOf()) : Parcelable