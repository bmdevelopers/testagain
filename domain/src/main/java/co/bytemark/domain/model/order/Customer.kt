package co.bytemark.domain.model.order

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Customer(
    @field:SerializedName("user_uuid")
    val userUuid: String,
    @field:SerializedName("full_name")
    val fullName: String,
    @field:SerializedName("email")
    val email: String,
    @field:SerializedName("phone")
    val phone: String
) : Parcelable