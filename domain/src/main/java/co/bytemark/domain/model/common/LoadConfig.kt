package co.bytemark.domain.model.common

import co.bytemark.sdk.model.payment_methods.WalletAutoLoadConfig
import co.bytemark.sdk.model.payment_methods.WalletAutoLoadThresholdConfig
import co.bytemark.sdk.model.payment_methods.WalletLoadMoneyConfig
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoadConfig(

    @SerializedName("load_config")
    @Expose
    val walletLoadMoneyConfig: WalletLoadMoneyConfig? = null,

    @SerializedName("autoload_config")
    @Expose
    val walletAutoLoadConfig: WalletAutoLoadConfig? = null,

    @SerializedName("autoload_threshold_config")
    @Expose
    val walletAutoLoadThresholdConfig: WalletAutoLoadThresholdConfig? = null,

    @SerializedName("autoload_calendar_config")
    @Expose
    val calendarAutoloadConfig: MutableList<CalendarAutoloadConfigItem>? = null
)