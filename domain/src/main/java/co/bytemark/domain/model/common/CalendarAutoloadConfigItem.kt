package co.bytemark.domain.model.common

import com.google.gson.annotations.SerializedName

data class CalendarAutoloadConfigItem(
    @SerializedName("autoload_config_name")
    val configName: String,
    @SerializedName("autoload_config_values")
    val configValues: MutableList<CalendarAutoloadConfigItemValues>
) {
    override fun toString(): String {
        return configName
    }
}

data class CalendarAutoloadConfigItemValues(val id: Int, val value: String) {
    override fun toString(): String {
        return value
    }
}