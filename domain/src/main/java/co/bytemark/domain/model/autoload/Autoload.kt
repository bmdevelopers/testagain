package co.bytemark.domain.model.autoload

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Autoload(
    @field:SerializedName("id")
    var id: String?,
    @field:SerializedName("autoload_thresholdvalue")
    var autoloadThresholdvalue: Int,
    @field:SerializedName("autoload_value")
    var autoloadValue: Int,
    @field:SerializedName("autoload_config_name")
    val autoloadConfigName: String?,
    @field:SerializedName("autoload_config_value_id")
    val autoloadConfigValueId: Int?,
    @field:SerializedName("autoload_config_value")
    val autoloadConfigValue: String?
) : Parcelable