package co.bytemark.domain.model.notificationSettings

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationPermission (@field:SerializedName("method")
                                   var method: String?,
                                   @field:SerializedName("allow")
                                   var allow: Boolean? = false) : Parcelable