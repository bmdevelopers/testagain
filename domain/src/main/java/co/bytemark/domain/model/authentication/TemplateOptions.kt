package co.bytemark.domain.model.authentication

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
class TemplateOptions  : Parcelable {
    @SerializedName("label")
    @Expose
    var label: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("required")
    @Expose
    var required: Boolean? = null

    @SerializedName("uncheckedFieldMessage")
    @Expose
    var unCheckedFieldMessage: String? = null

    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName("mType")
    @Expose
    var mType: String? = null

    @SerializedName("maxlength")
    @Expose
    var maxLength: Int? = null

    @SerializedName("minlength")
    @Expose
    var minLength: Int? = null

    @SerializedName("placeholder")
    @Expose
    var placeholder: String? = null
    var mask: String? = null

    @SerializedName("pattern")
    @Expose
    var pattern: String? = null

    @SerializedName("datepickerPopup")
    @Expose
    var datePickerPopup: String? = null

    @SerializedName("options")
    @Expose
    var options: List<Option>? =
        ArrayList()

    @SerializedName("editable")
    var editable: Boolean? = true

    companion object {
        @Transient
        val TEMPLATE_TYPE_EMAIL = "email"

        @Transient
        val TEMPLATE_TYPE_PASSWORD = "password"

        @Transient
        val TEMPLATE_TYPE_INPUT = "input"

        @Transient
        val TEMPLATE_TYPE_NUMERIC_INPUT = "numeric"

        @Transient
        val TEMPLATE_TYPE_PHONE = "phone"

        @Transient
        val TEMPLATE_TYPE_FLAT = "flat"

        @Transient
        val TEMPLATE_TYPE_STADIUM = "stadium"

        @Transient
        val TEMPLATE_TYPE_MULTI_LINE_TEXT = "multi_line_text"
    }
}