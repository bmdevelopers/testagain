package co.bytemark.domain.model.order

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OfferServe(

        @field:SerializedName("offer_config")
        val offerConfig: OfferConfig? = null,

        @field:SerializedName("title")
        val title: String? = null,

        @field:SerializedName("uuid")
        val uuid: String? = null

) : Parcelable