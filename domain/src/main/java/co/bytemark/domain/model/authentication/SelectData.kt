package co.bytemark.domain.model.authentication

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SelectData(
    @field:SerializedName("allOptions")
    var allOptions: List<SelectOption> = ArrayList()
) : Parcelable