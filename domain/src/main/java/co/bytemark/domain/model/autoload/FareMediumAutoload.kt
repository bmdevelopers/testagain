package co.bytemark.domain.model.autoload

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FareMediumAutoload(@field:SerializedName("autoload") val autoload: Autoload?) :
    Parcelable