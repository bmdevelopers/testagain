package co.bytemark.domain.model.incomm

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class IncommStoreListResponse(
    @SerializedName("stores")
    val incommStoreList: List<Incomm>
) : Parcelable
