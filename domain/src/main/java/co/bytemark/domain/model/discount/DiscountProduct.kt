package co.bytemark.domain.model.discount

import android.os.Parcelable
import co.bytemark.sdk.model.common.OrganizationDetails
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DiscountProduct (
    @field:SerializedName("original_price")
    var originalPrice: Int? = 0,
    @field:SerializedName("sale_price")
    var salePrice: Int? = 0,
    @field:SerializedName("discounted_price")
    var discountedPrice: Int? = 0,
    @field:SerializedName("product_uuid")
    var productUuid: String?,
    @field:SerializedName("qty")
    var qty: Int? = 0,
    @field:SerializedName("discount_percentage")
    var discountPercentage: Double? = 0.0,
) : Parcelable