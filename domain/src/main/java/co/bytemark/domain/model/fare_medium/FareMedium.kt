package co.bytemark.domain.model.fare_medium

import android.os.Parcelable
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents
import co.bytemark.domain.model.fare_medium.fares.Fare
import co.bytemark.sdk.Theme
import co.bytemark.sdk.model.common.BarcodeValidationV2
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

const val FARE_MEDIUM_UNBLOCK_STATE = 1
const val FARE_MEDIUM_BLOCK_STATE = 2

const val TYPE_VIRTUAL_CARD = 220

const val UNLIMITED = -1

@Parcelize
data class FareMedium(
        @SerializedName("uuid")
        var uuid: String,

        @SerializedName("name")
        var name: String?,

        @SerializedName("nickname")
        var nickname: String?,

        @SerializedName("category")
        var category: String?,

        @SerializedName("type")
        var type: Int,

        @SerializedName("faremedia_id")
        var fareMediumId: String,

        @SerializedName("barcode_payload")
        var barcodePayload: String? = null,

        @SerializedName("barcode_payload_v2")
        var barcodePayloadV2: BarcodeValidationV2? = null,

        @SerializedName("is_active_faremedium")
        var activeFareMedium: Boolean?,

        var lastDownloadTime: Long? = null,

        var fareMediumContents: FareMediumContents? = null,

        @SerializedName("stored_value")
        var storedValue: Int = 0,

        @field:SerializedName("transfer_time")
        var transferTime: String? = null,

        var fares: List<Fare>? = null,

        @SerializedName("printed_card_number")
        var printedCardNumber: String? = null,

        @SerializedName("state")
        var state: Int,

        @SerializedName("institution_account_id")
        var institutionAccountId: String? = null,

        @SerializedName("image_path")
        var imagePath: String? = null,

        @SerializedName("theme")
        var theme: Theme? = null,

        @SerializedName("pre_tax_balance")
        var preTaxBalance : Int = 0,

        @SerializedName("post_tax_balance")
        var postTaxBalance : Int = 0,

        @SerializedName("remaining_transfer_count")
        var remainingTransferCount: Int = -1,  // initializing with -1, so it will not affect the INIT apps

        @SerializedName("reset_transfer_in_days")
        var nextResetInDays : Int = -1 // initializing with -1, so it will not affect the INIT apps

) : Parcelable {
        fun hasValidBarcodeValidationV2() = (
                barcodePayloadV2?.mediaType.isNullOrEmpty() ||
                        barcodePayloadV2?.mediaScheme.isNullOrEmpty() ||
                        barcodePayloadV2?.payloadData.isNullOrEmpty() ||
                        barcodePayloadV2?.barcodeType.isNullOrEmpty()
                ).not()

        // User can't transfer preTax amount to other card, So if preTax value is 0,
        // we can use either storedValue or postTax ( they will hold the same value),
        // But if preTax is set, we need to consider postTax balance for transfer
        fun getTransferableBalance(): Int {
                return if(preTaxBalance == 0)
                        storedValue
                else
                        postTaxBalance
        }

        fun isActive() = this.state == FARE_MEDIUM_UNBLOCK_STATE

}
