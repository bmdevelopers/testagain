package co.bytemark.domain.model.notification

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Notification(
    @field:SerializedName("id")
    val id: String,
    @field:SerializedName("type")
    val type: String,
    @field:SerializedName("title")
    val title: String,
    @field:SerializedName("teaser")
    val teaser: String,
    @field:SerializedName("link")
    val link: String,
    @field:SerializedName("body")
    val body: String,
    @field:SerializedName("show_in_tiles")
    val showInTiles: Boolean,
    @field:SerializedName("sender_uuid")
    val senderUuid: String,
    @field:SerializedName("recipient_uuid")
    val recipientUuid: String,
    @field:SerializedName("time_created")
    val timeCreated: String,
    @field:SerializedName("time_modified")
    val timeModified: String,
    @field:SerializedName("start_date")
    val startDate: String,
    @field:SerializedName("end_date")
    val endDate: String,
    @Transient
    var read: Boolean = false
) : Parcelable