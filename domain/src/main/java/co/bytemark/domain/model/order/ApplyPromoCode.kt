package co.bytemark.domain.model.order

import co.bytemark.sdk.post_body.Item
import com.google.gson.annotations.SerializedName

data class ApplyPromoCode(

        @field:SerializedName("total")
        var total: String? = null,

        @field:SerializedName("offer_code_text")
        var offerCodeText: String? = null,

        @field:SerializedName("items")
        var items: List<Item?>? = null
)