package co.bytemark.domain.model.manage

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/** Created by Santosh on 2019-08-26.
 */

@Parcelize
data class AddSmartCard(

        @field:SerializedName("Id")
        val id: String? = null,

        @field:SerializedName("PrintedNumber")
        val printedNumber: String? = null,

        @field:SerializedName("CustomerAccountId")
        val customerAccountId: String? = null

) : Parcelable