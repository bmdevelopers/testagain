package co.bytemark.domain.model.common

import co.bytemark.domain.error.BytemarkException
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.BytemarkSDK.ResponseCode
import co.bytemark.sdk.BytemarkSDK.SpecialException
import okhttp3.internal.http2.StreamResetException
import retrofit2.HttpException
import java.net.ConnectException
import java.net.UnknownHostException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BmErrorHandler @Inject constructor() : ErrorHandler {

    override infix fun fromThrowable(throwable: Throwable): BMError {
        return if (throwable is SpecialException) {
            BMError(ResponseCode.SDK_VERSION_OUT_OF_DATE)
        } else if (throwable is HttpException) {
            if (throwable.code() == 401) {
                val response = BytemarkSDK.getGson().fromJson(
                    throwable.response()?.errorBody()?.string(),
                    Response::class.java
                )
                if (response.getErrorIfAny() != null) {
                    response.getErrorIfAny()!!
                } else {
                    BMError(throwable.code())
                }
            } else {
                BMError(throwable.code())
            }
        } else if (throwable is BytemarkException) {
            BMError(throwable.statusCode)
        } else if (throwable is UnknownHostException || throwable is StreamResetException || throwable is ConnectException) {
            BMError(ResponseCode.NETWORK_NOT_AVAILABLE)
        } else {
            BMError(ResponseCode.UNEXPECTED_ERROR)
        }
    }

}

interface ErrorHandler {
    infix fun fromThrowable(throwable: Throwable): BMError
}