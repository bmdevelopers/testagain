package co.bytemark.domain.model.discount

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DiscountData (
    @field:SerializedName("discount_serve_uuid")
    var discountServeUuid: String?,
    @field:SerializedName("discount_type")
    var discountType: String,
    @field:SerializedName("original_price")
    var originalPrice: Int = 0,
    @field:SerializedName("discount_price")
    var discountPrice: Int = 0,
    @field:SerializedName("products")
    var products: List<DiscountProduct>? = ArrayList()
) : Parcelable