package co.bytemark.domain.model.manage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LinkExistingCard(
        @SerializedName("card_number")
        @Expose
        var cardNumber: String? = null,

        @SerializedName("card_nickname")
        @Expose
        var cardNickname: String? = null,

        @SerializedName("security_code")
        @Expose
        var securityCode: String? = null
)