package co.bytemark.domain.model.autoload

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class DeleteAutoload(
    @field:SerializedName("success")
    var isAutoloadDeleted: Boolean,
) : Parcelable