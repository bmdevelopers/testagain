package co.bytemark.domain.model.userphoto

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Reviewer(
    @SerializedName("uuid")
    @Expose
    private var uuid: String? = null,

    @SerializedName("username")
    @Expose
    private var username: String? = null,

    @SerializedName("email")
    @Expose
    private var email: String? = null,

    @SerializedName("full_name")
    @Expose
    private var fullName: String? = null,

    @SerializedName("last_login_time")
    @Expose
    private var lastLoginTime: String? = null,

    @SerializedName("mobile")
    @Expose
    private var mobile: String? = null,

    @SerializedName("total_logins")
    @Expose
    private var totalLogins: Int? = null,

    @SerializedName("timezone")
    @Expose
    private var timezone: String? = null,

    @SerializedName("url")
    @Expose
    private var url: String? = null,

    @SerializedName("class_type")
    @Expose
    private var classType: String? = null,

    @SerializedName("status")
    @Expose
    private var status: String? = null,

    @SerializedName("type")
    @Expose
    private var type: String? = null,

    @SerializedName("time_created")
    @Expose
    private var timeCreated: String? = null,

    @SerializedName("time_modified")
    @Expose
    private var timeModified: String? = null
) : Parcelable {

    override fun hashCode(): Int {
        return uuid.hashCode()
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val that = o as Reviewer
        return uuid == that.uuid
    }
}