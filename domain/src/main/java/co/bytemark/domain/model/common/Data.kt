package co.bytemark.domain.model.common

import android.os.Parcelable
import co.bytemark.domain.model.authentication.Formly
import co.bytemark.domain.model.autoload.Autoload
import co.bytemark.domain.model.autoload.PostAutoload
import co.bytemark.domain.model.discount.DiscountData
import co.bytemark.domain.model.fare_medium.FareMedium
import co.bytemark.domain.model.fare_medium.Transaction
import co.bytemark.domain.model.fare_medium.fares.Fare
import co.bytemark.domain.model.init_fare_capping.InitFareCapping
import co.bytemark.domain.model.manage.AddSmartCard
import co.bytemark.domain.model.manage.FareCategory
import co.bytemark.domain.model.notification.Notification
import co.bytemark.domain.model.notificationSettings.NotificationSettings
import co.bytemark.domain.model.order.OfferServe
import co.bytemark.domain.model.order.Order
import co.bytemark.domain.model.payments.Card
import co.bytemark.domain.model.private_key.PrivateKey
import co.bytemark.domain.model.store.filter.FilterResult
import co.bytemark.domain.model.userphoto.UserPhoto
import co.bytemark.sdk.Pass
import co.bytemark.sdk.model.payment_methods.*
import co.bytemark.sdk.model.purchase_history.Pagination
import co.bytemark.sdk.model.subscriptions.Subscriptions
import co.bytemark.sdk.pojo.PayPalAccount
import co.bytemark.sdk.post_body.Ideal
import co.bytemark.sdk.post_body.PayNearMe
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

/** Created by Santosh on 2019-08-26.
 */
@Parcelize
data class Data(

    @field:SerializedName("cards")
    val cards: List<Card>? = ArrayList(),

    @field:SerializedName("oauth_token")
    val oauthToken: String? = null,

    @field:SerializedName("success")
    val success: Boolean = false,

    @field:SerializedName("notifications")
    val notifications: List<Notification>? = ArrayList(),

    @field:SerializedName("user_photo")
    val userPhoto: UserPhoto? = null,

    @field:SerializedName("formly")
    val formly: List<Formly>? = ArrayList(),

    @field:SerializedName("fare_mediums")
    val fareMediums: List<FareMedium>? = ArrayList(),

    @field:SerializedName("stored_value")
    val storedValue: String? = null,

    @field:SerializedName("transfer_time")
    val transferTime: String? = null,

    @field:SerializedName("fares")
    val fares: List<Fare>? = ArrayList(),

    @field:SerializedName("results")
    val filterResults: List<FilterResult>? = ArrayList(),

    @field:SerializedName("private_keys")
    val privateKeys: List<PrivateKey>? = ArrayList(),

    @field:SerializedName("order")
    val order: Order? = null,

    @field:SerializedName("braintree_client_token")
    val brainTreeClientToken: String? = null,

    @field:SerializedName("order_uuid")
    val orderUuid: String?,

    @field:SerializedName("issuers")
    val issuers: ArrayList<Issuer> = ArrayList(),

    @field:SerializedName("payment")
    val payment: Map<String, String>? = null,

    @field:SerializedName("ideal")
    val ideal: Ideal? = null,

    @field:SerializedName("pay_near_me")
    val payNearMe: PayNearMe? = null,

    @field:SerializedName("braintree_paypal_payment_methods")
    val braintreePaypalPaymentMethods: List<PayPalAccount> = ArrayList(),

    @field:SerializedName("accepted_card_types")
    val acceptedPayments: List<String> = ArrayList(),

    @SerializedName("accepted_payment_methods")
    val acceptedPaymentMethod: AcceptedPaymentMethod? = null,

    @SerializedName("addSmartCard")
    val addSmartCard: AddSmartCard? = null,

    @field:SerializedName("farecategories")
    val fareCategories: List<FareCategory> = ArrayList(),

    @field:SerializedName("notification_settings")
    val notification_settings: List<NotificationSettings>? = ArrayList(),

    @field:SerializedName("offer_serve")
    val offerServe: OfferServe? = null,

    @field:SerializedName("payment_methods")
    val paymentMethods: PaymentMethods? = null,

    @field:SerializedName("google_wallet")
    val googlePay: GooglePay? = null,

    @field:SerializedName("transactions")
    val transactions: List<Transaction>? = null,

    @field:SerializedName("postAutoload")
    val postAutoload: PostAutoload? = null,

    @field:SerializedName("dotpay")
    val dotPay: DotPay? = null,

    @field:SerializedName("autoload")
    val autoload: Autoload? = null,

    @field:SerializedName("result")
    val initFareCappings: List<InitFareCapping>? = null,

    @field:SerializedName("orders")
    val orders: List<co.bytemark.sdk.model.common.Order> = ArrayList(),

    @field:SerializedName("page_count")
    val pageCount: Int = 0,

    @field:SerializedName("total_count")
    val totalCount: Int = 0,

    @field:SerializedName("pagination")
    val pagination: Pagination? = null,

    @field:SerializedName("subscribed_pass_list")
    val subscriptions: MutableList<Subscriptions> = ArrayList(),

    @field:SerializedName("load_config")
    val walletLoadMoneyConfig: WalletLoadMoneyConfig? = null,

    @field:SerializedName("autoload_config")
    val walletAutoLoadConfig: WalletAutoLoadConfig? = null,

    @field:SerializedName("autoload_threshold_config")
    val walletAutoLoadThresholdConfig: WalletAutoLoadThresholdConfig? = null,

    @SerializedName("uuid")
    val uuid: String,

    @SerializedName("amount")
    val amount: Int,

    @SerializedName("threshold_amount")
    val thresholdAmount: Int,

    @SerializedName("stored_wallets")
    val storedWallets: StoredWallets? = null,

    @SerializedName("payment_uuid")
    val paymentUuid: String? = null,

    @SerializedName("status")
    val status: String? = null,

    @SerializedName("passes")
    val passes: List<Pass>? = ArrayList(),

    @SerializedName("card_uuid")
    val cardUuid: String? = null,

    @field:SerializedName("payment_type")
    var paymentType: String,

    @field:SerializedName("discount_data")
    val discountData: DiscountData? = null
) : Parcelable