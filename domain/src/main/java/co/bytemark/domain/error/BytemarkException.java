package co.bytemark.domain.error;

import androidx.annotation.NonNull;
import android.text.TextUtils;

import java.util.List;

import co.bytemark.domain.model.common.BMError;
import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.helpers.GsonFactory;
import co.bytemark.sdk.BytemarkSDK;
import retrofit2.HttpException;

/**
 * Created by Arunkumar on 06/04/17.
 */
public class BytemarkException extends RuntimeException {

    private int statusCode;

    /**
     * This is a human readable message that can be shown to the user directly. The purpose here is to
     * reduce overhead of switch casing everywhere using {@link #statusCode} code and setting a
     * {@link #userFacingMessage} manually.
     */
    private String userFacingMessage;

    public BytemarkException(String detailMessage, int statusCode) {
        super(detailMessage);
        this.statusCode = statusCode;
        setUserFacingMessage(getResultMessage(statusCode));
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getUserFacingMessage() {
        if (TextUtils.isEmpty(userFacingMessage)) {
            return getMessage();
        }
        return userFacingMessage;
    }

    public void setUserFacingMessage(String userFacingMessage) {
        this.userFacingMessage = userFacingMessage;
    }

    public static BytemarkException fromStatusCode(int statusCode) {
        return new BytemarkException(getResultMessage(statusCode), statusCode);
    }

    public static BytemarkException fromError(@NonNull BMError error) {
        return new BytemarkException(error.getMessage() == null ? error.getDescription() : error.getMessage(), error.getCode());
    }

    public static BytemarkException fromHttpException(HttpException throwable) {
        try {
            final String errorBodyString = throwable.response().errorBody().string();
            final BMResponse response = GsonFactory.getGson().fromJson(errorBodyString, BMResponse.class);
            final List<BMError> bmErrors = response.getErrors();
            return fromError(bmErrors.get(0));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        return null;
    }

    /**
     * Convert result code into string, for debugging
     *
     * @param statusCode result code returned by SDK
     * @return string result code in english
     */

    public static String getResultMessage(int statusCode) {
        return BytemarkSDK.ResponseCode.getResultMessage(statusCode);
    }


}
