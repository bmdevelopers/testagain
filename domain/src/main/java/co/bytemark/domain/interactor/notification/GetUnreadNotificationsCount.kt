package co.bytemark.domain.interactor.notification

import android.app.Application
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.interactor.UseCase.EmptyRequestValues
import co.bytemark.domain.repository.NotificationRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Scheduler
import rx.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named


class GetUnreadNotificationsCount @Inject constructor(repository: NotificationRepository,
                                                      @Named("Thread") threadScheduler: Scheduler?,
                                                      @Named("PostExecution") postExecutionScheduler: Scheduler?,
                                                      application: Application?) : UseCase<EmptyRequestValues, Int, NotificationRepository>(repository, threadScheduler, postExecutionScheduler, application) {

    override fun buildObservable(requestValues : EmptyRequestValues?) = PublishSubject.create<Int>().apply {
        GlobalScope.launch {
            try {
                onNext(getNotificationCount())
                onCompleted()
            } catch (t: Throwable) {
                Timber.e(t.message)
                onError(t)
            }
        }
    }

    private suspend fun getNotificationCount() =
            repository.getNotifications().data?.notifications?.count { !it.read } ?: 0

}