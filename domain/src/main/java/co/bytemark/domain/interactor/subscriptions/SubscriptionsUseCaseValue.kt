package co.bytemark.domain.interactor.subscriptions

import co.bytemark.domain.interactor.UseCase

/**
 * Created by vally on 07/06/19.
 */
data class SubscriptionsUseCaseValue(val fareMediumId: String?) : UseCase.RequestValues