package co.bytemark.domain.interactor.payments

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.incomm.IncommStoreListResponse
import co.bytemark.domain.repository.PaymentsRepository
import javax.inject.Inject

class GetRetailersWithAddressUseCase @Inject constructor(
    private val repository: PaymentsRepository,
    errorHandler: ErrorHandler
) : UseCaseV2<GetRetailersWithAddressUseCaseValue, IncommStoreListResponse>(errorHandler) {

    override suspend fun execute(requestValues: GetRetailersWithAddressUseCaseValue): Response<IncommStoreListResponse> =
        repository.getIncommRetailerWithAddress(requestValues.address, requestValues.radius, requestValues.orderUuid)
}