package co.bytemark.domain.interactor.product

import co.bytemark.domain.interactor.UseCase

/**
 * Created by santosh on 28/06/18.
 */

class GetAcceptedPaymentMethodsUseCaseValue(var organizationUuid: String?) : UseCase.RequestValues
