package co.bytemark.domain.interactor.authentication

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.AuthenticationRepository
import javax.inject.Inject

class ResendVerificationEmailUseCase @Inject internal constructor(
        val repository : AuthenticationRepository,
        val handler: ErrorHandler
) : UseCaseV2<Unit, Any>(handler) {

    override suspend fun execute(requestValues: Unit): Response<Any> {
        return repository.resentVerificationEmail()
    }
}