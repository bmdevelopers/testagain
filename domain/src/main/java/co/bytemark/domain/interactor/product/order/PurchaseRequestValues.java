package co.bytemark.domain.interactor.product.order;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.model.order.OfferServe;
import co.bytemark.sdk.model.payment_methods.BraintreePaypalPaymentMethod;
import co.bytemark.sdk.model.payment_methods.Card;
import co.bytemark.sdk.model.payment_methods.DotPay;
import co.bytemark.sdk.model.payment_methods.GooglePay;
import co.bytemark.sdk.model.payment_methods.Incomm;
import co.bytemark.sdk.model.payment_methods.Wallet;
import co.bytemark.sdk.model.product_search.entity.EntityResult;
import co.bytemark.sdk.post_body.CreateOrder;
import co.bytemark.sdk.post_body.Ideal;
import co.bytemark.sdk.post_body.Item;
import co.bytemark.sdk.post_body.MakePayment;
import co.bytemark.sdk.post_body.PayNearMe;
import co.bytemark.sdk.post_body.Payment;
import co.bytemark.sdk.post_body.PaymentTypes;

/**
 * Created by yashwant on 19/12/17.
 */
public class PurchaseRequestValues implements UseCase.RequestValues {
    public final CreateOrder createOrder;
    public final MakePayment makePayment;
    public final String deepLinkJwtToken;

    public PurchaseRequestValues(List<EntityResult> savedEntityResults, Card card, Card secondCard,
                                 Ideal ideal, DotPay dotPay, PayNearMe payNearMe, Incomm incomm, int total, String traceNumber, int defaultDeviceStorage,
                                 double lat, double lon, BraintreePaypalPaymentMethod payPal,
                                 GooglePay googlePay, Wallet wallet, OfferServe offerServe,
                                 String reloadingWalletUuid, String discountServeUuid, boolean isDeeplinkDiscountEnabled, String deeplinkJwtToken, boolean isPaymentMandatory) {
        this.deepLinkJwtToken = deeplinkJwtToken;
        createOrder = new CreateOrder();
        final List<Payment> payments = new ArrayList<>();
        final Payment payment = new Payment();
        final Payment secondPayment = new Payment();

        if (secondCard != null) {
            payment.setAmount(card.getAmount());
            payment.setCardUuid(card.getUuid());
            payment.setCvv(card.getCvv());
            payments.add(payment);
            secondPayment.setAmount(secondCard.getAmount());
            secondPayment.setCardUuid(secondCard.getUuid());
            secondPayment.setCvv(secondCard.getCvv());
            payments.add(secondPayment);
            createOrder.setPaymentType(PaymentTypes.STORED_CARD.toString());
        } else if (card != null) {
            payment.setAmount(total);
            payment.setCardUuid(card.getUuid());
            payment.setCvv(card.getCvv());
            payments.add(payment);
            createOrder.setPaymentType(PaymentTypes.STORED_CARD.toString());
        } else if (payPal != null) {
            payment.setAmount(total);
            payment.setPayPalNonce(payPal.getPayPalNonce());
            payment.setPayPalToken(payPal.getToken());
            payments.add(payment);
            createOrder.setPaymentType(PaymentTypes.PAYPAL.toString());
        } else if (ideal != null) {
            createOrder.setPaymentType(PaymentTypes.IDEAL.toString());
        } else if (dotPay != null) {
            createOrder.setPaymentType(PaymentTypes.DOTPAY.toString());
        } else if (payNearMe != null) {
            payment.setAmount(total);
            payments.add(payment);
            createOrder.setPaymentType(PaymentTypes.PAY_NEAR_ME.toString());
        } else if (incomm != null) {
            payment.setAmount(total);
            payments.add(payment);
            createOrder.setPaymentType(PaymentTypes.INCOMM.toString());
        } else if (googlePay != null) {
            payment.setAmount(total);
            payment.setGoogle3DSPaylaod(googlePay.getGoogle3DSPaylaod());
            payments.add(payment);
            createOrder.setPaymentType(PaymentTypes.GOOGLE_PAY.toString());
        } else if (wallet != null) {
            payment.setAmount(total);
            payment.setWalletUuid(wallet.getUuid());
            payments.add(payment);
            createOrder.setPaymentType(PaymentTypes.STORED_VALUE.toString());
        } else if( total == 0 && !isPaymentMandatory && TextUtils.isEmpty(createOrder.getPaymentType())) {
            createOrder.setPaymentType(PaymentTypes.NA.toString());
        }

        final List<Item> items = new ArrayList<>();
        final List<Item> itemsToPut = new ArrayList<>();

        if (reloadingWalletUuid == null) {
            for (int i = 0; i < savedEntityResults.size(); i++) {
                final Item item = new Item();
                final Item item2 = new Item();
                final String uuid;
                if (savedEntityResults.get(i).getLegacyProduct() != null) {
                    uuid = savedEntityResults.get(i).getLegacyProduct().getUuid();
                } else {
                    uuid = savedEntityResults.get(i).getUuid();
                }
                item.setProductUuid(uuid);
                item2.setProductUuid(uuid);
                item.setQty(savedEntityResults.get(i).getQuantity());
                item2.setQty(savedEntityResults.get(i).getQuantity());
                item.setPrice(savedEntityResults.get(i).getSalePrice());
                item2.setPrice(savedEntityResults.get(i).getSalePrice());
                item.setDeliveryMethod("MOBILE TICKET");
                item2.setDeliveryMethod("MOBILE TICKET");
                if (savedEntityResults.get(i).getAppliedFilters() != null) {
                    item.setAppliedFilters(savedEntityResults.get(i).getAppliedFilters());
                    item2.setAppliedFilters(savedEntityResults.get(i).getAppliedFilters());
                }
                item.setCreateSubscription(savedEntityResults.get(i).getCreateSubscription());
                item2.setCreateSubscription(savedEntityResults.get(i).getCreateSubscription());
                if (wallet != null) {
                    item2.setAction("capture");
                }
                items.add(item);
                itemsToPut.add(item2);
            }
        } else {
            final Item item = new Item();
            item.setReloadingWalletUuid(reloadingWalletUuid);
            items.add(item);
            itemsToPut.add(item);
        }
        createOrder.setTraceNumber(traceNumber);
        //createOrder.setCustomerUuid(BytemarkSDK.SDKUtility.getUserInfo().getUuid());
        createOrder.setPayments(payments);
        createOrder.setProcess(true);
        createOrder.setFulfillDigitalPasses(true);
        createOrder.setSaveToDevice(defaultDeviceStorage == 0);
        createOrder.setItems(items);
        createOrder.setLat(lat);
        createOrder.setLon(lon);
        createOrder.setDeliveryMethod("MOBILE");

        makePayment = new MakePayment();
        makePayment.setPayments(payments);
        makePayment.setItems(itemsToPut);
        makePayment.setProcess(true);
        makePayment.setDeepLinkDiscountEnabled(isDeeplinkDiscountEnabled);
        makePayment.setDiscountServeUuid(discountServeUuid);

        if (card != null) {
            makePayment.setPaymentType(PaymentTypes.STORED_CARD.toString());
        } else if (payPal != null) {
            makePayment.setPaymentType(PaymentTypes.PAYPAL.toString());
        } else if (ideal != null) {
            makePayment.setPaymentType(PaymentTypes.IDEAL.toString());
        } else if (dotPay != null) {
            makePayment.setPaymentType(PaymentTypes.DOTPAY.toString());
        } else if (payNearMe != null) {
            makePayment.setPaymentType(PaymentTypes.PAY_NEAR_ME.name());
        } else if (incomm != null) {
            makePayment.setPaymentType(PaymentTypes.INCOMM.name());
        } else if (googlePay != null) {
            makePayment.setPaymentType(PaymentTypes.GOOGLE_PAY.toString());
        } else if (wallet != null) {
            makePayment.setPaymentType(PaymentTypes.STORED_VALUE.toString());
        } else if( total == 0 && !isPaymentMandatory && TextUtils.isEmpty(makePayment.getPaymentType())) {
            makePayment.setPaymentType(PaymentTypes.NA.toString());
        }

        if (offerServe != null && offerServe.getOfferConfig().getOfferCodes().get(0) != null) {
            createOrder.setOfferCode(offerServe.getOfferConfig().getOfferCodes().get(0).getText());
            createOrder.setOfferServeUUID(offerServe.getUuid());
            createOrder.setAdjustedPrice(String.valueOf(total));
            createOrder.setTotal(String.valueOf(total));
            makePayment.setTotal(String.valueOf(total));
        }
        if(isDeeplinkDiscountEnabled) {
            createOrder.setOfferCode(null);
            createOrder.setOfferServeUUID(null);
            createOrder.setAdjustedPrice(String.valueOf(total));
            createOrder.setTotal(String.valueOf(total));
            makePayment.setTotal(String.valueOf(total));
        }
        createOrder.setDeepLinkDiscountEnabled(isDeeplinkDiscountEnabled);
        createOrder.setDiscountServeUuid(discountServeUuid);
    }
}