package co.bytemark.domain.interactor.rewards

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.interactor.authentication.VelociaLoginRequest
import co.bytemark.domain.model.authentication.VelociaLoginData
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.AuthenticationRepository
import javax.inject.Inject

class VelociaLoginUseCase @Inject constructor(
        val authenticationRepository: AuthenticationRepository,
        bmErrorHandler: BmErrorHandler
) : UseCaseV2<VelociaLoginRequest, VelociaLoginData>(
        bmErrorHandler
) {
  override suspend fun execute(requestValues: VelociaLoginRequest) =
          authenticationRepository.loginToVelocia(requestValues)
}