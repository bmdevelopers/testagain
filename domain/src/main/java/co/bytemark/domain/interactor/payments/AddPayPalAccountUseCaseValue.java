package co.bytemark.domain.interactor.payments;

import com.google.gson.JsonObject;

import co.bytemark.domain.interactor.UseCase;

/**
 * Created by yashwant on 09/02/18.
 */
public class AddPayPalAccountUseCaseValue implements UseCase.RequestValues {
    public JsonObject jsonObject;

    public AddPayPalAccountUseCaseValue(JsonObject jsonObject) {
        this.jsonObject = jsonObject;

    }

}