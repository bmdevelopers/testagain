package co.bytemark.domain.interactor.autoload

import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.PaymentsRepository
import javax.inject.Inject

class AutoLoadConfigurationUseCase @Inject constructor(
    private val repository: PaymentsRepository,
    errorHandler: ErrorHandler
) : UseCaseV2<UseCase.EmptyRequestValues, LoadConfig>(errorHandler) {

    override suspend fun execute(requestValues: UseCase.EmptyRequestValues): Response<LoadConfig> =
        repository.getAutoLoadConfiguration()
}