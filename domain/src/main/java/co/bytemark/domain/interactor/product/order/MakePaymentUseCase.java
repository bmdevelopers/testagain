package co.bytemark.domain.interactor.product.order;

import android.app.Application;

import javax.inject.Inject;
import javax.inject.Named;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.domain.model.common.Data;
import co.bytemark.domain.repository.ProductRepository;
import rx.Observable;
import rx.Scheduler;

/**
 * Created by yashwant on 07/12/17.
 */
public class MakePaymentUseCase extends UseCase<MakePaymentRequestValues, Data, ProductRepository> {

    @Inject
    public MakePaymentUseCase(ProductRepository repository,
                              @Named("Thread") Scheduler threadScheduler,
                              @Named("PostExecution") Scheduler postExecutionScheduler,
                              Application application) {
        super(repository, threadScheduler, postExecutionScheduler, application);
    }

    @Override
    public Observable<Data> buildObservable(MakePaymentRequestValues requestValues) {
        return repository.makePayment(requestValues.orderUuid, requestValues.makePayment, requestValues.deeplinkJwtToken)
                .map(BMResponse::getData);
    }
}
