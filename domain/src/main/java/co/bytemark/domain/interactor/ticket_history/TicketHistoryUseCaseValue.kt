package co.bytemark.domain.interactor.ticket_history

import co.bytemark.domain.interactor.UseCase

class TicketHistoryUseCaseValue (var perPage: Int, var status: String, var nextPageURL: String?) : UseCase.RequestValues