package co.bytemark.domain.interactor.schedules

import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.GTFSRepository
import co.bytemark.gtfs.Agency
import javax.inject.Inject

class GetAgenciesUseCase @Inject constructor(val repository: GTFSRepository, handler: ErrorHandler)
    : UseCaseV2<UseCase.EmptyRequestValues, List<Agency>>(handler) {

    override suspend fun execute(requestValues: UseCase.EmptyRequestValues): Response<List<Agency>> = repository.agencies
}