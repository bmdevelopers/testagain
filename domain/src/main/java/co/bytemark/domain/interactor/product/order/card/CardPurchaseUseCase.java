package co.bytemark.domain.interactor.product.order.card;

import android.app.Application;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.interactor.product.order.CreateOrderUseCase;
import co.bytemark.domain.interactor.product.order.MakePaymentRequestValues;
import co.bytemark.domain.interactor.product.order.MakePaymentUseCase;
import co.bytemark.domain.interactor.product.order.PurchaseRequestValues;
import co.bytemark.domain.model.common.BMError;
import co.bytemark.domain.model.common.Data;
import co.bytemark.domain.repository.ProductRepository;
import co.bytemark.sdk.BMErrors;
import co.bytemark.sdk.GetAvailablePasses;
import co.bytemark.sdk.Pass;
import co.bytemark.sdk.Passes;
import co.bytemark.sdk.network_impl.BMNetwork;
import co.bytemark.sdk.passcache.PassCacheManager;
import rx.Emitter;
import rx.Observable;
import rx.Scheduler;

import static co.bytemark.domain.error.BytemarkException.fromError;

/**
 * Created by yashwant on 19/12/17.
 */
public class CardPurchaseUseCase extends UseCase<PurchaseRequestValues, Boolean, ProductRepository> {
    private final CreateOrderUseCase createOrderUseCase;
    private final MakePaymentUseCase makePaymentUseCase;
    private final BMNetwork bmNetwork;

    @Inject
    public CardPurchaseUseCase(ProductRepository repository,
                               @Named("Thread") Scheduler threadScheduler,
                               @Named("PostExecution") Scheduler postExecutionScheduler,
                               Application application,
                               CreateOrderUseCase createOrderUseCase,
                               MakePaymentUseCase makePaymentUseCase, BMNetwork bmNetwork) {
        super(repository, threadScheduler, postExecutionScheduler, application);
        this.createOrderUseCase = createOrderUseCase;
        this.makePaymentUseCase = makePaymentUseCase;
        this.bmNetwork = bmNetwork;
    }

    @Override
    protected Observable<Boolean> buildObservable(PurchaseRequestValues requestValues) {
        return createOrderUseCase.buildObservable(requestValues)
                .map(Data::getOrderUuid)
                .flatMap(orderUuid -> makePaymentUseCase
                        .buildObservable(new MakePaymentRequestValues(orderUuid, requestValues.makePayment, requestValues.deepLinkJwtToken))
                        .map(Data::getSuccess))
                .flatMap(bmResponse -> {
                    PassCacheManager.get().evictAll();
                    return Observable.create(voidEmitter -> bmNetwork.getAvailablePasses(context, new GetAvailablePasses.GetPassesCallback() {
                        @Override
                        public void onCloudPasses(Passes cloudPasses) {
                            voidEmitter.onNext(true);
                            voidEmitter.onCompleted();
                        }

                        @Override
                        public void onLocalPasses(ArrayList<Pass> localPasses) {

                        }

                        @Override
                        public void onError(@Nullable BMErrors errors) {
                            if (errors.getErrors() != null && !errors.getErrors().isEmpty()) {
                                voidEmitter.onError(fromError(BMError.fromError(errors.getErrors().get(0))));
                            }
                        }
                    }, "available", "100", "1"), Emitter.BackpressureMode.LATEST);
                });
    }
}
