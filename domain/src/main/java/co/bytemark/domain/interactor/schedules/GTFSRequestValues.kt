package co.bytemark.domain.interactor.schedules

import co.bytemark.domain.interactor.UseCase.RequestValues
import java.util.*

class GTFSRequestValues(var originId: String? = null, var destinationId: String? = null, var date: Date? = null) : RequestValues