package co.bytemark.domain.interactor.autoload

import co.bytemark.domain.interactor.UseCase.RequestValues
import co.bytemark.domain.model.autoload.PostAutoload
import co.bytemark.sdk.post_body.Payment

class FareMediaAutoloadRequestValues(
    paymentList: List<Payment>?,
    fareMediaId: String?,
    traceNumber: String?,
    lat: Double,
    lon: Double,
    saveToDevice: Int,
    value: Int,
    thresholdValue: Int,
    autoloadValueId: Int?
) : RequestValues {
    val postAutoload: PostAutoload = PostAutoload()

    init {
        postAutoload.payments = paymentList
        postAutoload.faremediaId = fareMediaId
        postAutoload.traceNumber = traceNumber
        postAutoload.lat = lat
        postAutoload.lon = lon
        postAutoload.fulfillDigitalPasses = true
        postAutoload.process = true
        postAutoload.saveToDevice = saveToDevice == 0
        postAutoload.autoloadThresholdValue = thresholdValue
        postAutoload.autoloadValue = value
        postAutoload.autoloadValueId = autoloadValueId
    }
}