package co.bytemark.domain.interactor.authentication

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.common.api_response_data.UserProfileData
import co.bytemark.domain.repository.AuthenticationRepository
import co.bytemark.sdk.UserInfoDatabaseManager
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ranjith on 23/04/20.
 */
class GetUser @Inject internal constructor(
        repository: AuthenticationRepository,
        @Named("Thread") threadScheduler: Scheduler,
        @Named("PostExecution") postExecutionScheduler: Scheduler,
        val application: Application
) : UseCase<GetOAuthTokenUseCase.SignInRequestValues, UserProfileData, AuthenticationRepository>(
        repository,
        threadScheduler,
        postExecutionScheduler,
        application
) {

    override fun getLiveData(requestValues: GetOAuthTokenUseCase.SignInRequestValues?): LiveData<Result<UserProfileData>> {
        val result = MediatorLiveData<Result<UserProfileData>>()
        result.postValue(Result.Loading(null))
        GlobalScope.launch {
            try {
                val value = repository.getUserAsync().await()
                if (value.hasErrors()) {
                    result.postValue(Result.Failure(value.errors))
                } else {
                    // updating db
                    UserInfoDatabaseManager.getInstance(application.applicationContext).saveUser(value.data.user)
                    result.postValue(Result.Success(value.data))
                }

            } catch (e: Exception) {
                Timber.e(e.message)
                result.postValue(Result.Failure(getErrorsList(e)))
            }
        }
        return result
    }

    override fun buildObservable(requestValues: GetOAuthTokenUseCase.SignInRequestValues): Observable<UserProfileData> {
        return Observable.empty()
    }
}