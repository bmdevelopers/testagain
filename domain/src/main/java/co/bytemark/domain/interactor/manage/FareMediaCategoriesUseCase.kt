package co.bytemark.domain.interactor.manage

import co.bytemark.domain.interactor.UseCase.EmptyRequestValues
import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.manage.FareCategoryResponse
import co.bytemark.domain.repository.ManageRepository
import javax.inject.Inject

class FareMediaCategoriesUseCase
@Inject internal constructor(val repository: ManageRepository, handler: ErrorHandler) :
        UseCaseV2<EmptyRequestValues, FareCategoryResponse>
        (handler) {

    override suspend fun execute(requestValues: EmptyRequestValues): Response<FareCategoryResponse> {
        return repository.getFareMediaCategoriesAsync()
    }
}