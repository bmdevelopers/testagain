package co.bytemark.domain.interactor.purchase_history

import co.bytemark.domain.interactor.UseCase

/** Created by Santosh on 17/04/19.
 */

class OrderHistoryUseCaseValue(var perPage: Int, var pageNo: Int, var sortBy: String, var order: String) : UseCase.RequestValues