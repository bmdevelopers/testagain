package co.bytemark.domain.interactor.credit_pass

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.credit_pass.CreditPassRequest
import co.bytemark.domain.repository.CreditPassRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

class CreditPassUseCase @Inject constructor(
    repository: CreditPassRepository,
    @Named("Thread") threadScheduler: Scheduler?,
    @Named("PostExecution") postExecutionScheduler: Scheduler?,
    application: Application?
) : UseCase<CreditPassUseCase.CreditPassUseCaseValue, Data, CreditPassRepository>(
    repository, threadScheduler, postExecutionScheduler, application
) {

    override fun getLiveData(requestValues: CreditPassUseCaseValue): LiveData<Result<Data>> {
        val result = MediatorLiveData<Result<Data>>()
        result.postValue(Result.Loading(null))

        GlobalScope.launch {
            launch(Dispatchers.IO) {
                try {
                    val value = repository.creditPassAsync(
                        requestValues.creditPassRequest,
                        requestValues.deeplinkJwtToken
                    ).await()
                    if (hasErrors(value)) result.postValue(Result.Failure(getErrorList(value)))
                    else result.postValue(Result.Success(value.data))
                } catch (t: Throwable) {
                    Timber.e(t.message)
                    //result.postValue(Result.Success(null))
                }
            }
        }
        return result
    }

    class CreditPassUseCaseValue(
        val creditPassRequest: CreditPassRequest,
        val deeplinkJwtToken: String
    ) : RequestValues

    override fun buildObservable(requestValues: CreditPassUseCaseValue?): Observable<Data> =
        Observable.empty()
}