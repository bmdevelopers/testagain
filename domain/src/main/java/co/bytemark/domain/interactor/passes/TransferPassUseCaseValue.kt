package co.bytemark.domain.interactor.passes

import co.bytemark.domain.interactor.UseCase

/** Created by Santosh on 2019-11-21.
 * Copyright (c) 2019 Bytemark Inc. All rights reserved.
 */

class TransferPassUseCaseValue(val passUuid: String,
                               val destination: String) : UseCase.RequestValues