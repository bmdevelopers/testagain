package co.bytemark.domain.interactor.notification_settings

import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.repository.NotificationSettingsRepository
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.notificationSettings.NotificationSettingTypes
import javax.inject.Inject

class UpdateNotificationPermissionsUseCase @Inject internal constructor(val repository: NotificationSettingsRepository, handler: ErrorHandler) : UseCaseV2<NotificationPermissionsRequestValues, BMResponse>(handler) {

    override suspend fun execute(requestValues: NotificationPermissionsRequestValues): Response<BMResponse> =
            repository.updateNotificationPermissions(requestValues.notificationSettingTypes)
}

class NotificationPermissionsRequestValues(var notificationSettingTypes: NotificationSettingTypes) : UseCase.RequestValues