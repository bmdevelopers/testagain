package co.bytemark.domain.interactor.authentication

import co.bytemark.domain.interactor.UseCase.RequestValues

data class ChangePasswordRequestValues(var params: MutableMap<String, String>) : RequestValues