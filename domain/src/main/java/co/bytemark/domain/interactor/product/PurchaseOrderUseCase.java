package co.bytemark.domain.interactor.product;

import android.app.Application;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.model.common.BMError;
import co.bytemark.domain.model.common.Data;
import co.bytemark.domain.repository.ProductRepository;
import co.bytemark.sdk.BMErrors;
import co.bytemark.sdk.GetAvailablePasses;
import co.bytemark.sdk.Pass;
import co.bytemark.sdk.Passes;
import co.bytemark.sdk.network_impl.BMNetwork;
import co.bytemark.sdk.passcache.PassCacheManager;
import rx.Emitter;
import rx.Observable;
import rx.Scheduler;

import static co.bytemark.domain.error.BytemarkException.fromError;

/**
 * Created by Arunkumar on 10/10/17.
 */
public class PurchaseOrderUseCase extends UseCase<PurchaseOrderRequestValues, Data, ProductRepository> {
    private final BMNetwork bmNetwork;
    private final Application application;

    @Inject
    PurchaseOrderUseCase(ProductRepository repository,
                         @Named("Thread") Scheduler threadScheduler,
                         @Named("PostExecution") Scheduler postExecutionScheduler,
                         Application application, BMNetwork bmNetwork) {
        super(repository, threadScheduler, postExecutionScheduler, application);
        this.bmNetwork = bmNetwork;
        this.application = application;
    }

    @Override
    protected Observable<Data> buildObservable(PurchaseOrderRequestValues requestValues) {
        Map<String, String> queryParameters = new HashMap<>();

        // TODO Refactor this once pass repository is refactored
        return repository.purchaseProducts(requestValues.createOrder, queryParameters)
                .delay(300, TimeUnit.MILLISECONDS)
                .flatMap(bmResponse -> {
                    PassCacheManager.get().evictAll();
                    return Observable.create(voidEmitter -> bmNetwork.getAvailablePasses(application, new GetAvailablePasses.GetPassesCallback() {
                        @Override
                        public void onCloudPasses(Passes cloudPasses) {
                            voidEmitter.onNext(bmResponse.getData());
                            voidEmitter.onCompleted();
                        }

                        @Override
                        public void onLocalPasses(ArrayList<Pass> localPasses) {

                        }

                        @Override
                        public void onError(@Nullable BMErrors errors) {
                            if (errors.getErrors() != null && !errors.getErrors().isEmpty()) {
                                voidEmitter.onError(fromError(BMError.fromError(errors.getErrors().get(0))));
                            }
                        }
                    }, "usable", "10", "1"), Emitter.BackpressureMode.LATEST);
                });
    }
}
