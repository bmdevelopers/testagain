package co.bytemark.domain.interactor.store.filters;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.model.store.filter.Filter;

/**
 * Created by Arunkumar on 21/07/17.
 */
public class FilterRequestValues implements UseCase.RequestValues {
    public Filter filter;

    public FilterRequestValues(Filter filter) {
        this.filter = filter;
    }
}
