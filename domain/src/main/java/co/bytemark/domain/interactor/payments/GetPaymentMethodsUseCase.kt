package co.bytemark.domain.interactor.payments

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.repository.PaymentsRepository
import co.bytemark.sdk.model.payment_methods.PaymentMethods
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

class GetPaymentMethodsUseCase @Inject constructor(
        repository: PaymentsRepository?,
        @Named("Thread")
        threadScheduler: Scheduler?,
        @Named("PostExecution")
        postExecutionScheduler: Scheduler?,
        application: Application?
) : UseCase<GetPaymentMethodsUseCaseValue, PaymentMethods, PaymentsRepository>(
        repository,
        threadScheduler,
        postExecutionScheduler,
        application
) {
    override fun buildObservable(requestValues: GetPaymentMethodsUseCaseValue?): Observable<PaymentMethods>? {
        return repository.getPaymentMethods(requestValues!!.organizationUUID!!)
                .map(Data::paymentMethods)
    }

    override fun getLiveData(requestValues: GetPaymentMethodsUseCaseValue?): LiveData<Result<PaymentMethods>> {
        val result = MediatorLiveData<Result<PaymentMethods>>()
        result.postValue(Result.Loading(null))

        GlobalScope.launch {
            try {
                val value = repository.getPaymentMethodsAsync().await()
                if (hasErrors(value)) result.postValue(Result.Failure(getErrorList(value)))
                else {
                    val data = value.data
                    val gson = Gson()
                    val json = gson.toJson(data)
                    val paymentMethods = gson.fromJson(json, PaymentMethods::class.java)
                    result.postValue(Result.Success(paymentMethods))
                }
            } catch (e: Exception) {
                Timber.e(e.message)
                result.postValue(Result.Failure(arrayListOf(BMError(0, ""))))
            }
        }
        return result
    }
}