package co.bytemark.domain.interactor.schedules

import co.bytemark.domain.interactor.UseCase.EmptyRequestValues
import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.GTFSRepository
import co.bytemark.gtfs.Stop
import javax.inject.Inject

class GetOriginStops @Inject constructor(val repository: GTFSRepository, handler: ErrorHandler)
    : UseCaseV2<EmptyRequestValues, List<Stop>>(handler) {

    override suspend fun execute(requestValues: EmptyRequestValues): Response<List<Stop>> =
            repository.originStops
}