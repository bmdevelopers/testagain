package co.bytemark.domain.interactor.product;

import android.app.Application;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.repository.ProductRepository;
import rx.Observable;
import rx.Scheduler;

/**
 * Created by yashwant on 12/03/18.
 */
public class GetAcceptedPaymentsUseCase extends UseCase<GetAcceptedPaymentsUseCaseValue, List<String>, ProductRepository> {
    @Inject
    public GetAcceptedPaymentsUseCase(ProductRepository repository,
                                      @Named("Thread") Scheduler threadScheduler,
                                      @Named("PostExecution") Scheduler postExecutionScheduler,
                                      Application application) {
        super(repository, threadScheduler, postExecutionScheduler, application);
    }

    @Override
    protected Observable<List<String>> buildObservable(GetAcceptedPaymentsUseCaseValue requestValues) {
        return repository.getAcceptedPayments()
                .map(bmResponse -> bmResponse.getData().getAcceptedPayments());
    }

}
