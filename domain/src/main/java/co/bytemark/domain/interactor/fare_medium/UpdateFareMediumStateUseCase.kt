package co.bytemark.domain.interactor.fare_medium

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.FareMediumRepository
import javax.inject.Inject

class UpdateFareMediumStateUseCase @Inject internal constructor(val repository : FareMediumRepository,val handler: ErrorHandler) : UseCaseV2<UpdateFareMediumStateUseCase.UpdateFareMediumRequestValue, Any>(handler) {

    override suspend fun execute(requestValues: UpdateFareMediumRequestValue): Response<Any> {
        return repository.updateFareMediumState(requestValues.fareMediumId,requestValues.newState)
    }

    inner class UpdateFareMediumRequestValue(val fareMediumId : String,val newState: Int)
}



