package co.bytemark.domain.interactor.payments

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.incomm.IncommStoreListResponse
import co.bytemark.domain.repository.PaymentsRepository
import javax.inject.Inject

class GetRetailersWithLocationUseCase @Inject constructor(
    private val repository: PaymentsRepository,
    errorHandler: ErrorHandler
) : UseCaseV2<GetRetailersWithLocationUseCaseValue, IncommStoreListResponse>(errorHandler) {

    override suspend fun execute(requestValues: GetRetailersWithLocationUseCaseValue): Response<IncommStoreListResponse> =
        repository.getIncommRetailerWithLocation(requestValues.lattitude, requestValues.logitude, requestValues.radius,requestValues.orderUuid)

}