package co.bytemark.domain.interactor.userphoto

import android.graphics.Bitmap.CompressFormat
import android.graphics.BitmapFactory
import android.util.Base64
import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.userphoto.UserPhoto
import co.bytemark.domain.repository.UserPhotoRepository
import co.bytemark.sdk.passcache.PassCacheManager
import java.io.ByteArrayOutputStream
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SetUserPhoto @Inject constructor(val repository: UserPhotoRepository,
                                       handler: ErrorHandler)
    : UseCaseV2<PhotoRequestValues, UserPhotoResponse?>(handler) {

    override suspend fun execute(requestValues: PhotoRequestValues): Response<UserPhotoResponse?> {
        val file = getBase64FromFile(requestValues.filePath)
        val userPhoto = repository.setUserPhoto(UserPhoto(data = file))
        PassCacheManager.get().evictAll()
        return userPhoto
    }

    private fun getBase64FromFile(path: String): String {
        val bitmap = BitmapFactory.decodeFile(path)
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(CompressFormat.JPEG, 100, byteArrayOutputStream)
        val imageByteArray = byteArrayOutputStream.toByteArray()
        // Release from memory
        bitmap.recycle()
        return Base64.encodeToString(imageByteArray, Base64.DEFAULT)
    }

}