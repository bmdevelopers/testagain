package co.bytemark.domain.interactor.voucher_redeem

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.domain.model.voucher_code.VoucherCodeData
import co.bytemark.domain.repository.VoucherRedeemRepository
import javax.inject.Inject

class VoucherRedeemUseCase @Inject constructor(
        private val repository: VoucherRedeemRepository,
        errorHandler: BmErrorHandler
) : UseCaseV2<VoucherRedeemUseCaseValue, VoucherCodeData>(errorHandler) {

    override suspend fun execute(requestValues: VoucherRedeemUseCaseValue) =
            repository.redeemVoucherCode(
                    requestValues.orderType,
                    requestValues.voucherCode,
                    requestValues.saveToDevice
            )

}
