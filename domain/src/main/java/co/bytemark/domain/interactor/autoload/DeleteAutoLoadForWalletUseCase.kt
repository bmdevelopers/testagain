package co.bytemark.domain.interactor.autoload

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.autoload.DeleteAutoload
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.PaymentsRepository
import javax.inject.Inject

class DeleteAutoLoadForWalletUseCase @Inject constructor(
    private val repository: PaymentsRepository,
    errorHandler: ErrorHandler
) : UseCaseV2<DeleteAutoLoadForWalletUseCaseValue, DeleteAutoload>(errorHandler) {

    override suspend fun execute(requestValues: DeleteAutoLoadForWalletUseCaseValue): Response<DeleteAutoload> =
        repository.deleteAutoloadForWallet(requestValues.walletUuid)
}