package co.bytemark.domain.interactor.notification
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.repository.NotificationRepository
import co.bytemark.sdk.BytemarkSDK.ResponseCode
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MarkNotificationAsReadUseCase @Inject  constructor(val repository: NotificationRepository, handler: ErrorHandler) :
        UseCaseV2<NotificationRequestValues, Boolean>(handler) {

    override suspend fun execute(requestValues: NotificationRequestValues): Response<Boolean> {
        return repository.markNotificationAsRead(requestValues.notificationId)
    }
}

class NotificationRequestValues(var notificationId: String) : UseCase.RequestValues