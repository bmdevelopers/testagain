package co.bytemark.domain.interactor.userphoto

import co.bytemark.domain.interactor.UseCase.RequestValues

class PhotoRequestValues(var filePath: String) : RequestValues