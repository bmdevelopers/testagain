package co.bytemark.domain.interactor.authentication

import com.google.gson.annotations.SerializedName

data class VelociaLoginRequest(
        @SerializedName("location") val location: Location
)

data class Location(
        @SerializedName("latitude")val latitude: Double,
        @SerializedName("longitude") val longitude: Double
)