package co.bytemark.domain.interactor.authentication;

import java.util.Map;

import co.bytemark.domain.interactor.UseCase;

/**
 * Created by Omkar on 9/7/17.
 */
public class SubmitVoucherCodeRequestValues implements UseCase.RequestValues {
    final Map<String, String> params;

    public SubmitVoucherCodeRequestValues(Map<String, String> params) {
        this.params = params;
    }
}
