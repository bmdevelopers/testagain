package co.bytemark.domain.interactor.fare_medium

import com.google.gson.annotations.SerializedName

data class TransferBalanceRequestData(
        @SerializedName("from_uuid")
        val fromUUID: String,

        @SerializedName("to_uuid")
        val toUUID: String,

        @SerializedName("amount")
        val amount: Int
)