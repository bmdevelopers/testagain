package co.bytemark.domain.interactor.manage

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.manage.InstitutionListData
import co.bytemark.domain.repository.ManageRepository
import javax.inject.Inject

class GetInstitutionListUseCase @Inject constructor(
        private val manageRepository: ManageRepository,
        errorHandler: BmErrorHandler
) : UseCaseV2<Int, InstitutionListData>(errorHandler) {

    override suspend fun execute(pageIndex: Int): Response<InstitutionListData> =
            manageRepository.getInstitutionList(pageIndex)

}