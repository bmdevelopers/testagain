package co.bytemark.domain.interactor

import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.common.Result

abstract class UseCaseV2<Q, ResponseData>(private val handler: ErrorHandler) {

    protected abstract suspend fun execute(requestValues: Q): Response<ResponseData>

    open suspend operator fun invoke(request: Q): Result<ResponseData> {
        return try {
            val response = execute(request)
            val result = response.getErrorIfAny()?.let {
                Result.Failure<ResponseData>(listOf(it))
            } ?: Result.Success(response.data)
            result
        } catch (throwable: Throwable) {
            Result.Failure(listOf(handler.fromThrowable(throwable)))
        }
    }
}
