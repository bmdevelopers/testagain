package co.bytemark.domain.interactor.authentication

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.repository.AuthenticationRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

class ChangePasswordUseCase @Inject constructor(
        repository: AuthenticationRepository?,
        @Named("Thread") threadScheduler: Scheduler?,
        @Named("PostExecution") postExecutionScheduler: Scheduler?,
        application: Application?
) : UseCase<ChangePasswordRequestValues?, Boolean, AuthenticationRepository>(
        repository,
        threadScheduler,
        postExecutionScheduler,
        application
) {

    override fun buildObservable(requestValues: ChangePasswordRequestValues?): Observable<Boolean> =
            Observable.empty()

    override fun getLiveData(requestValues: ChangePasswordRequestValues?): LiveData<Result<Boolean>> {
        val result = MediatorLiveData<Result<Boolean>>()
        result.postValue(Result.Loading(null))

        GlobalScope.launch {
            try {
                val value = repository.changePassword(requestValues?.params).await()
                if (hasErrors(value)) result.postValue(Result.Failure(getErrorList(value)))
                else result.postValue(Result.Success(value.data.success))
            } catch (t: Throwable) {
                Timber.e(t.message)
                result.postValue(Result.Failure(arrayListOf(BMError(0, ""))))
            }
        }
        return result
    }
}