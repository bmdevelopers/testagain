package co.bytemark.domain.interactor.authentication

import co.bytemark.domain.interactor.UseCase.RequestValues

/**
 * Created by Ranjith on 24/04/20.
 */
class UpdateUserRequestValues(var params: Map<String, String?>) : RequestValues