package co.bytemark.domain.interactor.product.order.ideal;

import android.app.Application;

import java.net.URLEncoder;
import java.util.Iterator;

import javax.inject.Inject;
import javax.inject.Named;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.interactor.product.order.MakePaymentRequestValues;
import co.bytemark.domain.interactor.product.order.MakePaymentUseCase;
import co.bytemark.domain.model.common.Data;
import co.bytemark.domain.model.incomm.IncommBarcodeDetail;
import co.bytemark.domain.repository.ProductRepository;
import rx.Observable;
import rx.Scheduler;

/**
 * Created by yashwant on 07/12/17.
 */
public class GetPaymentUrlUseCase extends UseCase<MakePaymentRequestValues, GetPaymentUrlUseCase.ReturnValue, ProductRepository> {
	private final MakePaymentUseCase makePaymentUseCase;

	@Inject
	public GetPaymentUrlUseCase(ProductRepository repository,
								@Named("Thread") Scheduler threadScheduler,
								@Named("PostExecution") Scheduler postExecutionScheduler,
								Application application, MakePaymentUseCase makePaymentUseCase) {
		super(repository, threadScheduler, postExecutionScheduler, application);
		this.makePaymentUseCase = makePaymentUseCase;
	}

	@Override
	protected Observable<ReturnValue> buildObservable(MakePaymentRequestValues requestValues) {

		return makePaymentUseCase.buildObservable(requestValues)
				.observeOn(threadScheduler)
				.map(Data::getPayment)
				.map(data -> {
					String paymentUrl = "";
					String accountNumber = "";
					String iconUrl = "";
					String orgName = "";
					final StringBuilder postDataBuilder = new StringBuilder();
					if (data != null && data.size() > 0) {
						paymentUrl = data.get("url");
						accountNumber = data.get("account_number");
						iconUrl = data.get("icon_url");
						orgName = data.get("org_name");
						for (Iterator<String> iterator = data.keySet().iterator(); iterator.hasNext(); ) {
							final String key = iterator.next();
							if (!key.equals("url")) {
								postDataBuilder.append(String.format("%s=%s", key, URLEncoder.encode(data.get(key))));
								if (iterator.hasNext()) {
									postDataBuilder.append("&");
								}
							}
						}
					}

					return new ReturnValue(paymentUrl, postDataBuilder.toString(), new IncommBarcodeDetail(accountNumber, iconUrl, orgName));
				});
	}

	public class ReturnValue {
		public final String url;
		public final String postData;
		public final IncommBarcodeDetail incommBarcodeDetail;

		public ReturnValue(String url, String postData, IncommBarcodeDetail incommBarcodeDetail) {
			this.url = url;
			this.postData = postData;
			this.incommBarcodeDetail = incommBarcodeDetail;
		}
	}
}
