package co.bytemark.domain.interactor.authentication

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.authentication.ChangePinData
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.AuthenticationRepository
import javax.inject.Inject

class ChangePinUseCase @Inject constructor(
        private val authenticationRepository: AuthenticationRepository,
        errorHandler: BmErrorHandler
) : UseCaseV2<ChangePinRequest, ChangePinData>(errorHandler) {

    override suspend fun execute(requestValues: ChangePinRequest): Response<ChangePinData> =
            authenticationRepository.changePin(requestValues)
}