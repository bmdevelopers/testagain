package co.bytemark.domain.interactor.payments

import android.app.Application
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.repository.PaymentsRepository
import co.bytemark.sdk.model.payment_methods.PaymentMethods
import co.bytemark.sdk.post_body.PostPaymentMethod
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Scheduler
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

class AddPaymentMethodV2UseCase @Inject constructor(
        repository: PaymentsRepository,
        @Named("Thread") threadScheduler: Scheduler,
        @Named("PostExecution") postExecutionScheduler: Scheduler,
        application: Application
) : UseCase<AddPaymentMethodV2UseCase.AddPaymentMethodUseCaseValue, PaymentMethods, PaymentsRepository>(
        repository,
        threadScheduler,
        postExecutionScheduler,
        application
) {

    override fun getLiveData(requestValues: AddPaymentMethodUseCaseValue): MutableLiveData<Result<PaymentMethods>> {
        val result = MediatorLiveData<Result<PaymentMethods>>()
        result.postValue(Result.Loading(null))

        GlobalScope.launch {
            try {
                val value = repository.addV2PaymentMethodAsync(
                        requestValues.postPaymentMethod).await()
                if (hasErrors(value)) result.postValue(Result.Failure(getErrorList(value)))
                else result.postValue(Result.Success(value.data.paymentMethods!!))
            } catch (e: Exception) {
                Timber.e(e.message)
                result.postValue(Result.Failure(arrayListOf(BMError(0, ""))))
            }
        }
        return result
    }

    override fun buildObservable(requestValues: AddPaymentMethodUseCaseValue) =
            repository.addPaymentMethod(requestValues.postPaymentMethod)
                    .map { it.paymentMethods }

    class AddPaymentMethodUseCaseValue(
            val postPaymentMethod: PostPaymentMethod
    ) : UseCase.RequestValues
}


