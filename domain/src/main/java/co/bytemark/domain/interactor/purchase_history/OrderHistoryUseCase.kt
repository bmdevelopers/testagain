package co.bytemark.domain.interactor.purchase_history

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.repository.OrderHistoryRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

/** Created by Santosh on 17/04/19.
 */

class OrderHistoryUseCase @Inject constructor(repository: OrderHistoryRepository,
                                              @Named("Thread") threadScheduler: Scheduler?,
                                              @Named("PostExecution") postExecutionScheduler: Scheduler?,
                                              application: Application?)
    : UseCase<OrderHistoryUseCaseValue, Data, OrderHistoryRepository>(repository, threadScheduler, postExecutionScheduler, application) {

    override fun getLiveData(requestValue: OrderHistoryUseCaseValue): LiveData<Result<Data>>? {
        val result = MediatorLiveData<Result<Data>>()
        result.postValue(Result.Loading(null))

        GlobalScope.launch {
            try {
                val value = repository.getReceiptsAsync(requestValue.perPage, requestValue.pageNo, requestValue.sortBy, requestValue.order).await()
                if (hasErrors(value)) result.postValue(Result.Failure(getErrorList(value))) else result.postValue(Result.Success(value.data))
            } catch (e: Exception) {
                Timber.e(e.message)
                result.postValue(Result.Failure(arrayListOf(BMError(0, ""))))
            }
        }
        return result
    }

    override fun buildObservable(requestValues: OrderHistoryUseCaseValue?): Observable<Data>? {
        return Observable.empty()
    }
}