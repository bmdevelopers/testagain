package co.bytemark.domain.interactor.payments;

import android.app.Application;

import javax.inject.Inject;
import javax.inject.Named;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.model.common.Data;
import co.bytemark.domain.repository.PaymentsRepository;
import rx.Observable;
import rx.Scheduler;

/**
 * Created by yashwant on 13/02/18.
 */
public class DeletePayPalAccountUseCase extends UseCase<DeletePayPalAccountUseCaseValue, Data, PaymentsRepository> {

    @Inject
    public DeletePayPalAccountUseCase(PaymentsRepository repository,
                                   @Named("Thread") Scheduler threadScheduler,
                                   @Named("PostExecution") Scheduler postExecutionScheduler,
                                   Application application) {
        super(repository, threadScheduler, postExecutionScheduler, application);
    }

    @Override
    protected Observable<Data> buildObservable(DeletePayPalAccountUseCaseValue requestValues) {
        return repository.deletePayPalAccount(requestValues.payPalToken);
    }

}
