package co.bytemark.domain.interactor.fare_medium

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.repository.FareMediumRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by ranjith on 04/05/20
 */

class TransferVirtualCardUseCase @Inject constructor(
        repository: FareMediumRepository,
        @Named("Thread") threadScheduler: Scheduler,
        @Named("PostExecution") postExecutionScheduler: Scheduler,
        application: Application
) : UseCase<FareMediumRequestValues, Boolean, FareMediumRepository>(
        repository,
        threadScheduler,
        postExecutionScheduler,
        application
) {
    override fun getLiveData(fareMediumRequestValues: FareMediumRequestValues): LiveData<Result<Boolean>> {
        val result = MediatorLiveData<Result<Boolean>>()
        result.postValue(Result.Loading(null))

        GlobalScope.launch {
            try {
                val value = repository.transferVirtualCardAsync(fareMediumRequestValues.fareMediumUuid).await()
                if (hasErrors(value)) result.postValue(Result.Failure(getErrorList(value)))
                else result.postValue(Result.Success(value.data.success))
            } catch (e: Exception) {
                Timber.e(e.message)
                result.postValue(Result.Failure(arrayListOf(BMError(0, ""))))
            }
        }
        return result
    }

    override fun buildObservable(fareMediumRequestValues: FareMediumRequestValues):
            Observable<Boolean> = Observable.empty()
}