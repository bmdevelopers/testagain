package co.bytemark.domain.interactor.newStoreFilters

import co.bytemark.domain.interactor.UseCase
import co.bytemark.sdk.post_body.PostSearch

/**
 * Created by vally on 05/12/19.
 */
class StoreFilterUseCaseValue(var postSearch: PostSearch) : UseCase.RequestValues