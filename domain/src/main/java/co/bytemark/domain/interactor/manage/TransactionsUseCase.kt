package co.bytemark.domain.interactor.manage

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.fare_medium.TransactionsResponse
import co.bytemark.domain.repository.ManageRepository
import javax.inject.Inject

class TransactionsUseCase
@Inject constructor(val repository: ManageRepository, handler: ErrorHandler) : UseCaseV2<TransactionRequestValues, TransactionsResponse>(handler) {

    override suspend fun execute(requestValues: TransactionRequestValues): Response<TransactionsResponse> =
            repository.getTransactionsAsync(requestValues.fareMediaId, requestValues.pageIndex)
}