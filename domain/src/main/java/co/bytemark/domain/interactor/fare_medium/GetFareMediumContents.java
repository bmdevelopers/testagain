package co.bytemark.domain.interactor.fare_medium;

import android.app.Application;

import javax.inject.Inject;
import javax.inject.Named;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.model.fare_medium.fare_contents.FareMediumContents;
import co.bytemark.domain.repository.FareMediumRepository;
import rx.Observable;
import rx.Scheduler;

public class GetFareMediumContents extends UseCase<FareMediumRequestValues, FareMediumContents, FareMediumRepository> {
    @Inject
    GetFareMediumContents(FareMediumRepository repository,
                          @Named("Thread") Scheduler threadScheduler,
                          @Named("PostExecution") Scheduler postExecutionScheduler,
                          Application application) {
        super(repository, threadScheduler, postExecutionScheduler, application);
    }

    @Override
    protected Observable<FareMediumContents> buildObservable(FareMediumRequestValues requestValues) {
        return repository.getFareMediumContents(requestValues.fareMediumUuid, requestValues.loadFromLocalStore);
    }
}
