package co.bytemark.domain.interactor.authentication

import com.google.gson.annotations.SerializedName

data class ChangePinRequest(
        @SerializedName("new_pin")
        val newPin: String,

        @SerializedName("confirm_new_pin")
        val confirmNewPin: String
)