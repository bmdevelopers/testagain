package co.bytemark.domain.interactor.notification_settings

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.notificationSettings.NotificationSettingsResponse
import co.bytemark.domain.repository.NotificationSettingsRepository
import javax.inject.Inject

class GetNotificationSettingsUseCase @Inject internal constructor(val repository: NotificationSettingsRepository, handler: ErrorHandler) : UseCaseV2<Unit, NotificationSettingsResponse>(handler) {

    override suspend fun execute(requestValues: Unit): Response<NotificationSettingsResponse> =
            repository.getNotificationsSettings()

}

