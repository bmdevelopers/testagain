package co.bytemark.domain.interactor.authentication

import co.bytemark.domain.interactor.UseCase

class DeleteAccountUseCaseRequestValue(val creditCardUuid: String?) : UseCase.RequestValues