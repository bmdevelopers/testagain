package co.bytemark.domain.interactor.autoload

import co.bytemark.domain.interactor.UseCase

data class GetAutoLoadForWalletRequestValue(var walletUuid: String) : UseCase.RequestValues