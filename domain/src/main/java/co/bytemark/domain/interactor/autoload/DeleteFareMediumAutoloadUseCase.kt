package co.bytemark.domain.interactor.autoload

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.autoload.DeleteAutoload
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.ManageRepository
import javax.inject.Inject

class DeleteFareMediumAutoloadUseCase @Inject constructor(
    private val repository: ManageRepository,
    errorHandler: ErrorHandler
) : UseCaseV2<GetAutoLoadForFareMediumRequestValue, DeleteAutoload>(errorHandler) {

    override suspend fun execute(requestValues: GetAutoLoadForFareMediumRequestValue): Response<DeleteAutoload> =
        repository.deleteAutoload(requestValues.fareMediaId)
}