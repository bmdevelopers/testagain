package co.bytemark.domain.interactor.init_fare_capping

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.init_fare_capping.InitFareCappingData
import co.bytemark.domain.repository.ManageRepository
import javax.inject.Inject

class GetInitFareCappingUseCase @Inject constructor(
        private val manageRepository: ManageRepository,
        errorHandler: ErrorHandler
) : UseCaseV2<InitFareCappingRequestValues, InitFareCappingData>(errorHandler) {

    override suspend fun execute(requestValues: InitFareCappingRequestValues): Response<InitFareCappingData> =
            manageRepository.getInitFareCappings(requestValues.fareMediumId)

}