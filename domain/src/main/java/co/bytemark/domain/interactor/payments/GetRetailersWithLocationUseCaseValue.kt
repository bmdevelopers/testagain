package co.bytemark.domain.interactor.payments

import co.bytemark.domain.interactor.UseCase

data class GetRetailersWithLocationUseCaseValue(
    var lattitude: Double,
    var logitude: Double,
    var radius: Double,
    var orderUuid: String
) : UseCase.RequestValues