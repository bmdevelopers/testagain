package co.bytemark.domain.interactor.notification

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.notification.NotificationResponse
import co.bytemark.domain.repository.NotificationRepository
import co.bytemark.sdk.Api.ApiUtility
import javax.inject.Inject

class GetNotificationsUseCase @Inject constructor(
    val repository: NotificationRepository,
    handler: ErrorHandler
) : UseCaseV2<Unit, NotificationResponse>(handler) {

    override suspend fun execute(requestValues: Unit): Response<NotificationResponse> {
        return repository.getNotifications().also { response ->
            response.data?.notifications?.let {
                it.toMutableList().sortedByDescending { notification ->
                    ApiUtility.getCalendarFromApiString(notification.startDate)?.time
                }
            }
        }
    }
}





