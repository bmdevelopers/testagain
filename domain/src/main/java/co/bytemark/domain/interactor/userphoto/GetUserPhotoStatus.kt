package co.bytemark.domain.interactor.userphoto

import android.app.Application
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.interactor.UseCase.EmptyRequestValues
import co.bytemark.domain.repository.UserPhotoRepository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import rx.Scheduler
import rx.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class GetUserPhotoStatus @Inject constructor(repository: UserPhotoRepository,
                                             @Named("Thread") threadScheduler: Scheduler?,
                                             @Named("PostExecution") postExecutionScheduler: Scheduler?,
                                             application: Application?) : UseCase<EmptyRequestValues?, String?, UserPhotoRepository>(repository, threadScheduler, postExecutionScheduler, application) {

    override fun buildObservable(requestValues: EmptyRequestValues?) = PublishSubject.create<String>().apply {
        val exceptionHandler =
                CoroutineExceptionHandler { _, e ->
                    Timber.tag("GetUserPhotoStatusExcep").e(e.message)
                }
        GlobalScope.launch(exceptionHandler) {
            async {
                onNext(getUserPhoto())
                doOnError { throwable ->
                    onError(throwable)
                }
                onCompleted()
            }
        }
    }

    private suspend fun getUserPhoto() =
            repository.getUserPhoto().data?.userPhoto?.status
}
