package co.bytemark.domain.interactor.init_fare_capping;

import co.bytemark.domain.interactor.UseCase;

public class InitFareCappingRequestValues implements UseCase.RequestValues {

    public String fareMediumId;

    public InitFareCappingRequestValues(String fareMediumId) {
        this.fareMediumId = fareMediumId;
    }
}
