package co.bytemark.domain.interactor.payments

import co.bytemark.domain.interactor.UseCase
import co.bytemark.sdk.model.payment_methods.Wallet

/** Created by Santosh on 2019-11-18.
 * Copyright (c) 2019 Bytemark Inc. All rights reserved.
 */

class CreateWalletUseCaseValue(var wallet: Wallet) : UseCase.RequestValues