package co.bytemark.domain.interactor.autoload

import android.app.Application
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.repository.PaymentsRepository
import rx.Observable
import rx.Scheduler
import javax.inject.Inject
import javax.inject.Named

/** Created by Santosh on 2019-08-22.
 */

class GetWalletLoadMoneyConfigUseCase @Inject constructor(
    repository: PaymentsRepository?,
    @Named("Thread") threadScheduler: Scheduler?,
    @Named("PostExecution") postExecutionScheduler: Scheduler?,
    application: Application?
) : UseCase<GetWalletLoadMoneyConfigUseCaseValues, Data, PaymentsRepository>
    (
    repository,
    threadScheduler,
    postExecutionScheduler,
    application
) {

    override fun buildObservable(requestValues: GetWalletLoadMoneyConfigUseCaseValues): Observable<Data>? =
        repository.getWalletLoadMoneyConfiguration()
}