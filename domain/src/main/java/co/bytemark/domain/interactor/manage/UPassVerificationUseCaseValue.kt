package co.bytemark.domain.interactor.manage

data class UPassVerificationUseCaseValue(
        val institutionId: String,
        val fareMediumId: String
)