package co.bytemark.domain.interactor.authentication

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.common.api_response_data.UserProfileData
import co.bytemark.domain.repository.AuthenticationRepository
import co.bytemark.sdk.UserInfoDatabaseManager
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ranjith on 23/04/20.
 */
class UpdateUser @Inject internal constructor(
        repository: AuthenticationRepository,
        @Named("Thread") threadScheduler: Scheduler,
        @Named("PostExecution") postExecutionScheduler: Scheduler,
        val application: Application
) : UseCase<UpdateUserRequestValues, UserProfileData, AuthenticationRepository>(
        repository,
        threadScheduler,
        postExecutionScheduler,
        application
) {

    override fun getLiveData(requestValues: UpdateUserRequestValues): LiveData<Result<UserProfileData>> {
        val result = MutableLiveData<Result<UserProfileData>>()
        result.postValue(Result.Loading(null))
        GlobalScope.launch {
            try {
                val updateResponse = repository.updateUserAsync(requestValues.params
                        as MutableMap<String, String?>).await()
                if (hasErrors(updateResponse)) {
                    result.postValue(Result.Failure(updateResponse.errors))
                    return@launch
                }

                // get updated user details from API
                val userResponse = repository.getUserAsync().await()
                if (userResponse.errors.isNotEmpty()) {
                    result.postValue(Result.Failure(userResponse.errors))
                } else {
                    UserInfoDatabaseManager.getInstance(application.applicationContext).saveUser(userResponse.data.user)
                    result.postValue(Result.Success(userResponse.data))
                }

            } catch (e: Exception) {
                result.postValue(Result.Failure(getErrorsList(e)))
            }
        }

        return result
    }

//    override fun buildObservable(requestValues: UpdateUserRequestValues): Observable<User> {
//        requestValues.params["oauth_token"] = SDKUtility.getAuthToken()
//        return repository!!.updateUser(requestValues.params)
//    }

    override fun buildObservable(requestValues: UpdateUserRequestValues?): Observable<UserProfileData> = Observable.empty()
}