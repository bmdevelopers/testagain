package co.bytemark.domain.interactor.product

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.repository.ProductRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by santosh on 28/06/18.
 */

class GetAcceptedPaymentMethodsUseCase @Inject
constructor(
    repository: ProductRepository,
    @Named("Thread") threadScheduler: Scheduler,
    @Named("PostExecution") postExecutionScheduler: Scheduler,
    application: Application
) : UseCase<GetAcceptedPaymentMethodsUseCaseValue, MutableList<String>, ProductRepository>(
    repository,
    threadScheduler,
    postExecutionScheduler,
    application
) {

    override fun buildObservable(requestValues: GetAcceptedPaymentMethodsUseCaseValue): Observable<MutableList<String>> {
        return repository.getAcceptedPaymentMethods(requestValues.organizationUuid)
            .map { bmResponse -> bmResponse.data.acceptedPaymentMethod?.getAllPaymentMethods() }
    }

    override fun getLiveData(requestValues: GetAcceptedPaymentMethodsUseCaseValue?): LiveData<Result<MutableList<String>>> {
        val result = MediatorLiveData<Result<MutableList<String>>>()
        result.postValue(Result.Loading(null))

        GlobalScope.launch {
            try {
                val value = repository.getAcceptedPaymentMethods(requestValues?.organizationUuid)
                    .asObservable().toBlocking()
                if (hasErrors(value.first())) result.postValue(Result.Failure(getErrorList(value.first())))
                else {
                    result.postValue(Result.Success(value.first().data.acceptedPaymentMethod?.getAllPaymentMethods()))
                }
            } catch (e: Exception) {
                Timber.e(e.message)
                result.postValue(Result.Failure(arrayListOf(BMError(0, ""))))
            }
        }
        return result
    }
}
