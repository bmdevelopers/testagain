package co.bytemark.domain.interactor.product;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.model.order.OfferServe;
import co.bytemark.sdk.model.payment_methods.BraintreePaypalPaymentMethod;
import co.bytemark.sdk.model.payment_methods.Card;
import co.bytemark.sdk.model.payment_methods.GooglePay;
import co.bytemark.sdk.model.payment_methods.Incomm;
import co.bytemark.sdk.model.payment_methods.Wallet;
import co.bytemark.sdk.model.product_search.entity.EntityResult;
import co.bytemark.sdk.post_body.CreateOrder;
import co.bytemark.sdk.post_body.Ideal;
import co.bytemark.sdk.post_body.Item;
import co.bytemark.sdk.post_body.Payment;
import co.bytemark.sdk.post_body.PaymentTypes;
import co.bytemark.sdk.post_body.StoredValue;
import timber.log.Timber;

/**
 * Created by Arunkumar on 21/07/17.
 */
public final class PurchaseOrderRequestValues implements UseCase.RequestValues {
    final CreateOrder createOrder;

    public PurchaseOrderRequestValues(List<EntityResult> savedEntityResults, String fareMediaUuid, boolean isIntentForReload, Card card,
                                      Card secondCard, int subTotal, String traceNumber, int defaultDeviceStorage, double lat, double lon,
                                      BraintreePaypalPaymentMethod brainTreePaypal, GooglePay googlePay, Incomm incomm,
                                      Wallet wallet, OfferServe offerServe, Boolean processState, String reloadingWalletUuid) {
        createOrder = new CreateOrder();
        final List<Payment> payments = new ArrayList<>();
        final Payment payment = new Payment();
        final Payment secondPayment = new Payment();

        if (secondCard != null) {
            payment.setAmount(card.getAmount());
            payment.setCardUuid(card.getUuid());
            payment.setCvv(card.getCvv());
            payments.add(payment);
            secondPayment.setAmount(secondCard.getAmount());
            secondPayment.setCardUuid(secondCard.getUuid());
            secondPayment.setCvv(secondCard.getCvv());
            payments.add(secondPayment);
			createOrder.setPaymentType(PaymentTypes.STORED_CARD.toString());
        } else if (card != null) {
            payment.setAmount(subTotal);
            payment.setCardUuid(card.getUuid());
            payment.setCvv(card.getCvv());
            payments.add(payment);
			createOrder.setPaymentType(PaymentTypes.STORED_CARD.toString());
        } else if (brainTreePaypal != null) {
            payment.setAmount(subTotal);
            payment.setPayPalNonce(brainTreePaypal.getPayPalNonce());
            payment.setPayPalToken(brainTreePaypal.getToken());
            createOrder.setPaymentType(PaymentTypes.PAYPAL.toString());
            payments.add(payment);
        } else if (googlePay != null) {
            payment.setAmount(subTotal);
            payment.setGoogle3DSPaylaod(googlePay.getGoogle3DSPaylaod());
            payments.add(payment);
            createOrder.setPaymentType(PaymentTypes.GOOGLE_PAY.toString());
        } else if (incomm != null) {
            payment.setAmount(subTotal);
            payments.add(payment);
            createOrder.setPaymentType(PaymentTypes.INCOMM.toString());
        } else if (wallet != null) {
            payment.setAmount(subTotal);
            payment.setWalletUuid(wallet.getUuid());
            payments.add(payment);
            createOrder.setPaymentType(PaymentTypes.STORED_VALUE.toString());
        }

        if(subTotal == 0 && !processState && TextUtils.isEmpty(createOrder.getPaymentType())) {
            createOrder.setPaymentType(PaymentTypes.NA.toString());
        }
        final List<Item> items = new ArrayList<>();
        if (!isIntentForReload) {
            for (int i = 0; i < savedEntityResults.size(); i++) {
                final Item item = new Item();
                final String uuid;
                if (savedEntityResults.get(i).getLegacyProduct() != null) {
                    uuid = savedEntityResults.get(i).getLegacyProduct().getUuid();
                } else {
                    uuid = savedEntityResults.get(i).getUuid();
                }
                item.setProductUuid(uuid);
                item.setQty(savedEntityResults.get(i).getQuantity());
                item.setPrice(savedEntityResults.get(i).getSalePrice());
                if (savedEntityResults.get(i).getAppliedFilters() != null)
                    item.setAppliedFilters(savedEntityResults.get(i).getAppliedFilters());
                item.setCreateSubscription(savedEntityResults.get(i).getCreateSubscription());
               if( savedEntityResults.get(i).getInputFieldRequired()!=null && savedEntityResults.get(i).getInputFields()!=null) {
                   item.setInputFields(savedEntityResults.get(i).getInputFields());
               }
                items.add(item);
            }
            createOrder.setItems(items);
        } else {
            createOrder.setItems(null);
        }


        Timber.e("Faremediauuid: " + fareMediaUuid);

        if (offerServe != null && offerServe.getOfferConfig().getOfferCodes().get(0) != null) {
            createOrder.setOfferCode(offerServe.getOfferConfig().getOfferCodes().get(0).getText());
            createOrder.setOfferServeUUID(offerServe.getUuid());
            createOrder.setTotal(String.valueOf(subTotal));
            createOrder.setAdjustedPrice(String.valueOf(subTotal));
        }
        createOrder.setTraceNumber(traceNumber);
        //createOrder.setCustomerUuid(BytemarkSDK.SDKUtility.getUserInfo().getUuid());
        createOrder.setPayments(payments);
        createOrder.setProcess(processState);
        createOrder.setFulfillDigitalPasses(true);
        createOrder.setSaveToDevice(defaultDeviceStorage == 0);
        createOrder.setLat(lat);
        createOrder.setLon(lon);
        if (fareMediaUuid != null) {
            StoredValue storedValue = new StoredValue(fareMediaUuid, subTotal);
            createOrder.setStoredValue(storedValue);
            createOrder.setSaveToDevice(false);
        }
        if (isIntentForReload) {
            createOrder.setFulfillDigitalPasses(false);
        }

        if (reloadingWalletUuid != null) {
            final Item item = new Item();
            item.setReloadingWalletUuid(reloadingWalletUuid);
            items.add(item);
            createOrder.setItems(items);
        }
    }

    public PurchaseOrderRequestValues(List<EntityResult> savedEntityResults, Ideal ideal, int subTotal, String traceNumber, int defaultDeviceStorage, double lat, double lon) {
        createOrder = new CreateOrder();
        final List<Item> items = new ArrayList<>();
        for (int i = 0; i < savedEntityResults.size(); i++) {
            final Item item = new Item();
            final String uuid;
            if (savedEntityResults.get(i).getLegacyProduct() != null) {
                uuid = savedEntityResults.get(i).getLegacyProduct().getUuid();
            } else {
                uuid = savedEntityResults.get(i).getUuid();
            }
            item.setProductUuid(uuid);
            item.setQty(savedEntityResults.get(i).getQuantity());
            item.setPrice(savedEntityResults.get(i).getSalePrice());
            item.setDeliveryMethod("MOBILE TICKET");
            items.add(item);
        }
        createOrder.setTraceNumber(traceNumber);
        createOrder.setPaymentType("IDEAL");
        createOrder.setProcess(true);
        createOrder.setFulfillDigitalPasses(true);
        createOrder.setSaveToDevice(defaultDeviceStorage == 0);
        createOrder.setItems(items);
        createOrder.setLat(lat);
        createOrder.setLon(lon);
        createOrder.setDeliveryMethod("MOBILE");
    }
}