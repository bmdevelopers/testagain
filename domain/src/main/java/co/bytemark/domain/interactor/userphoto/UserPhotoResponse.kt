package co.bytemark.domain.interactor.userphoto

import co.bytemark.domain.model.userphoto.UserPhoto

data class UserPhotoResponse(val userPhoto: UserPhoto?)