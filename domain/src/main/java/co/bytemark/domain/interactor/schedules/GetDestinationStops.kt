package co.bytemark.domain.interactor.schedules

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.GTFSRepository
import co.bytemark.gtfs.Stop
import javax.inject.Inject

class GetDestinationStops @Inject constructor(val repository: GTFSRepository, handler: ErrorHandler)
    : UseCaseV2<GTFSRequestValues, List<Stop>>(handler) {

    override suspend fun execute(requestValues: GTFSRequestValues): Response<List<Stop>> =
            repository.getDestinations(requestValues.originId)
}