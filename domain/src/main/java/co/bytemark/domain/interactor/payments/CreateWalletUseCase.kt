package co.bytemark.domain.interactor.payments

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.repository.PaymentsRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

/** Created by Santosh on 2019-11-18.
 * Copyright (c) 2019 Bytemark Inc. All rights reserved.
 */

class CreateWalletUseCase @Inject constructor(repository: PaymentsRepository,
                                              @Named("Thread") threadScheduler: Scheduler?,
                                              @Named("PostExecution") postExecutionScheduler: Scheduler?,
                                              application: Application?
) : UseCase<CreateWalletUseCaseValue, Data, PaymentsRepository>(
        repository,
        threadScheduler,
        postExecutionScheduler,
        application
) {

    override fun getLiveData(requestValues: CreateWalletUseCaseValue): LiveData<Result<Data>> {
        val result = MediatorLiveData<Result<Data>>()
        result.postValue(Result.Loading(null))

        GlobalScope.launch {
            try {
                val value = repository.createWalletAsync(requestValues.wallet).await()
                if (hasErrors(value)) result.postValue(Result.Failure(getErrorList(value))) else result.postValue(Result.Success(value.data))
            } catch (e: Exception) {
                Timber.e(e.message)
                result.postValue(Result.Failure(arrayListOf(BMError(0, ""))))
            }
        }
        return result
    }

    override fun buildObservable(requestValues: CreateWalletUseCaseValue?): Observable<Data> =
            Observable.empty()
}