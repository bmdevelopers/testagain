package co.bytemark.domain.interactor.product.order

import android.app.Application
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.order.OfferServe
import co.bytemark.domain.repository.ProductRepository
import rx.Observable
import rx.Scheduler
import javax.inject.Inject
import javax.inject.Named

class ApplyPromoCodeUseCase @Inject
constructor(repository: ProductRepository,
            @Named("Thread") threadScheduler: Scheduler,
            @Named("PostExecution") postExecutionScheduler: Scheduler,
            application: Application)
    : UseCase<ApplyPromoCodeRequestValues, OfferServe, ProductRepository>(repository, threadScheduler, postExecutionScheduler, application) {

    override fun buildObservable(requestValues: ApplyPromoCodeRequestValues?): Observable<OfferServe>? {
        return repository.applyPromoCode(requestValues?.applyPromoCode)
                .map { bmResponse -> bmResponse.data.offerServe }
    }
}