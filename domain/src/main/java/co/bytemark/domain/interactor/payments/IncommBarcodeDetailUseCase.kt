package co.bytemark.domain.interactor.payments

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.incomm.IncommBarcodeDetail
import co.bytemark.domain.repository.PaymentsRepository
import javax.inject.Inject

class IncommBarcodeDetailUseCase @Inject constructor(
    private val repository: PaymentsRepository,
    errorHandler: ErrorHandler
) : UseCaseV2<String, IncommBarcodeDetail>(errorHandler) {

    override suspend fun execute(requestValues: String): Response<IncommBarcodeDetail> =
        repository.getIncommBarcodeDetails(requestValues)
}