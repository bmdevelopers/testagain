package co.bytemark.domain.interactor.product.send_pass

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.repository.SendPassRepository
import co.bytemark.domain.model.common.Data
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Named

class SendPassUseCase @Inject constructor(
        repository: SendPassRepository,
        @Named("Thread") threadScheduler: Scheduler?,
        @Named("PostExecution") postExecutionScheduler: Scheduler?,
        application: Application?
) : UseCase<SendPassUseCaseValue, Data, SendPassRepository>(
        repository,
        threadScheduler,
        postExecutionScheduler,
        application
) {

    override fun getLiveData(requestValues: SendPassUseCaseValue): LiveData<Result<Data>> {
        val result = MediatorLiveData<Result<Data>>()
        result.postValue(Result.Loading(null))
        GlobalScope.launch {
            try {
                val response = repository.sendPassAsync(
                        requestValues.passUuid,
                        requestValues.receiverEmail
                ).await()

                if (hasErrors(response)) {
                    result.postValue(Result.Failure(getErrorList(response)))
                } else {
                    result.postValue(Result.Success(response.data))
                }

            } catch (e: Exception) {
                Timber.e(e.message)
                result.postValue(Result.Failure(arrayListOf(BMError(0, ""))))
            }
        }
        return result
    }

    override fun buildObservable(requestValues: SendPassUseCaseValue?): Observable<Data> {
        return Observable.empty()
    }


}