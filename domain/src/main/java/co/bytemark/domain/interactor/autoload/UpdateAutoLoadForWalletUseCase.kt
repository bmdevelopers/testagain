package co.bytemark.domain.interactor.autoload

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.autoload.Autoload
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.PaymentsRepository
import javax.inject.Inject

class UpdateAutoLoadForWalletUseCase @Inject constructor(
    private val repository: PaymentsRepository,
    errorHandler: ErrorHandler
) : UseCaseV2<UpdateAutoLoadForWalletUseCaseValue, Autoload>(errorHandler) {

    override suspend fun execute(requestValues: UpdateAutoLoadForWalletUseCaseValue): Response<Autoload> =
        repository.updateAutoloadForWallet(requestValues.createWalletAutoload)
}