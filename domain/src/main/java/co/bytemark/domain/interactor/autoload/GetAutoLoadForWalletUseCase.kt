package co.bytemark.domain.interactor.autoload

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.autoload.WalletCalendarAutoload
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.PaymentsRepository
import javax.inject.Inject

class GetAutoLoadForWalletUseCase @Inject constructor(
    private val repository: PaymentsRepository,
    errorHandler: ErrorHandler
) : UseCaseV2<GetAutoLoadForWalletRequestValue, WalletCalendarAutoload>(errorHandler) {

    override suspend fun execute(requestValues: GetAutoLoadForWalletRequestValue): Response<WalletCalendarAutoload> =
        repository.getAutoloadForWallet(requestValues.walletUuid)
}