package co.bytemark.domain.interactor.subscriptions

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.repository.SubscriptionsRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by vally on 07/06/19.
 */
class SubscriptionsUseCase @Inject constructor(repository: SubscriptionsRepository,
                                               @Named("Thread") threadScheduler: Scheduler?,
                                               @Named("PostExecution") postExecutionScheduler: Scheduler?,
                                               application: Application?)
    : UseCase<SubscriptionsUseCaseValue, Data, SubscriptionsRepository>(repository, threadScheduler, postExecutionScheduler, application) {

    override fun getLiveData(requestValues: SubscriptionsUseCaseValue): LiveData<Result<Data>> {
        val result = MediatorLiveData<Result<Data>>()
        result.postValue(Result.Loading(null))

        GlobalScope.launch {
            try {
                val value = repository.getSubscriptions(requestValues.fareMediumId).await()
                if (hasErrors(value)) result.postValue(Result.Failure(getErrorList(value))) else result.postValue(Result.Success(value.data))
            } catch (e: Exception) {
                Timber.e(e.message)
                result.postValue(Result.Failure(arrayListOf(BMError(0, ""))))
            }
        }
        return result
    }


    override fun buildObservable(requestValues: SubscriptionsUseCaseValue?): Observable<Data>? {
        return Observable.empty()
    }
}