package co.bytemark.domain.interactor.fare_capping

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.FareCappingRepository
import javax.inject.Inject

class GetFareCappingUseCase @Inject constructor(
        private val repository: FareCappingRepository,
        errorHandler: ErrorHandler
) : UseCaseV2<Unit, FareCappingResponse>(errorHandler) {

    override suspend fun execute(requestValues: Unit): Response<FareCappingResponse> =
            repository.getFareCapping()

}