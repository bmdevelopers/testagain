package co.bytemark.domain.interactor.payments

import co.bytemark.domain.interactor.UseCase

class GetPaymentMethodsUseCaseValue(var organizationUUID: String?) : UseCase.RequestValues

