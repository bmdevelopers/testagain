package co.bytemark.domain.interactor.fare_medium

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.LoadConfig
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.FareMediumRepository
import javax.inject.Inject

class GetAutoLoadConfigUseCase @Inject constructor(
        private val fareMediumRepository: FareMediumRepository,
        errorHandler: ErrorHandler
) : UseCaseV2<Unit, LoadConfig>(errorHandler) {

    override suspend fun execute(requestValues: Unit): Response<LoadConfig> =
            fareMediumRepository.getAutoLoadConfig()

}