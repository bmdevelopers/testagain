package co.bytemark.domain.interactor.newStoreFilters


import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.store.filter.SearchResponseData
import co.bytemark.domain.repository.StoreFiltersRepository
import javax.inject.Inject

/**
 * Created by vally on 05/12/19.
 */
class StoreFilterUseCase @Inject constructor(
        private val repository: StoreFiltersRepository,
        errorHandler: BmErrorHandler
) : UseCaseV2<StoreFilterUseCaseValue, SearchResponseData>(errorHandler) {

    override suspend fun execute(requestValues: StoreFilterUseCaseValue): Response<SearchResponseData> =
            repository.getStoreFilters(requestValues.postSearch)

}