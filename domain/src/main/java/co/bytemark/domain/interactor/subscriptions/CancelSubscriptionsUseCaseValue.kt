package co.bytemark.domain.interactor.subscriptions

import co.bytemark.domain.interactor.UseCase
import com.google.gson.JsonObject

/**
 * Created by vally on 11/06/19.
 */
data class CancelSubscriptionsUseCaseValue(val subscriptionUuid: String, val jsonObject: JsonObject): UseCase.RequestValues