package co.bytemark.domain.interactor.product.send_pass

import co.bytemark.domain.interactor.UseCase

data class SendPassUseCaseValue(
        val passUuid: String,
        val receiverEmail: String
): UseCase.RequestValues