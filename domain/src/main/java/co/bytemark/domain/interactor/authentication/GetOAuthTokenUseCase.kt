package co.bytemark.domain.interactor.authentication

import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.authentication.SignInResponse
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.AuthenticationRepository
import javax.inject.Inject

class GetOAuthTokenUseCase @Inject internal constructor(
    val repository: AuthenticationRepository,
    val handler: ErrorHandler
) : UseCaseV2<GetOAuthTokenUseCase.SignInRequestValues, SignInResponse>(handler) {

    override suspend fun execute(requestValues: SignInRequestValues): Response<SignInResponse> =
        repository.getOauthToken(requestValues.params)

    class SignInRequestValues(var params: MutableMap<String, String>) : UseCase.RequestValues
}