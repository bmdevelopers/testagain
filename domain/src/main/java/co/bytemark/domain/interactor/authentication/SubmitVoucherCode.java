package co.bytemark.domain.interactor.authentication;

import android.app.Application;

import javax.inject.Inject;
import javax.inject.Named;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.domain.repository.AuthenticationRepository;
import rx.Observable;
import rx.Scheduler;

/**
 * Created by Omkar on 9/7/17.
 */
public class SubmitVoucherCode extends UseCase<SubmitVoucherCodeRequestValues, BMResponse, AuthenticationRepository> {

    @Inject
    public SubmitVoucherCode(AuthenticationRepository repository,
                             @Named("Thread") Scheduler threadScheduler,
                             @Named("PostExecution") Scheduler postExecutionScheduler,
                             Application application) {
        super(repository, threadScheduler, postExecutionScheduler, application);
    }

    @Override
    protected Observable<BMResponse> buildObservable(SubmitVoucherCodeRequestValues requestValues) {
        return repository.submitVoucherCode(requestValues.params);
    }
}
