package co.bytemark.domain.interactor.store.filters;

import android.app.Application;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.model.store.filter.FilterResult;
import co.bytemark.domain.repository.FiltersRepository;
import rx.Observable;
import rx.Scheduler;

public class GetResults extends UseCase<FilterRequestValues, List<FilterResult>, FiltersRepository> {
    @Inject
    public GetResults(FiltersRepository repository,
                      @Named("Thread") Scheduler threadScheduler,
                      @Named("PostExecution") Scheduler postExecutionScheduler,
                      Application application) {
        super(repository, threadScheduler, postExecutionScheduler, application);
    }

    @Override
    protected Observable<List<FilterResult>> buildObservable(FilterRequestValues requestValues) {
        return this.repository.getResults(requestValues.filter);
    }
}
