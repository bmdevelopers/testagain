package co.bytemark.domain.interactor.autoload

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.autoload.Autoload
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.ManageRepository
import javax.inject.Inject

class UpdateFareMediumAutoloadUseCase @Inject constructor(
    private val repository: ManageRepository,
    errorHandler: ErrorHandler
) : UseCaseV2<FareMediaAutoloadRequestValues, Autoload>(
    errorHandler
) {
    override suspend fun execute(requestValues: FareMediaAutoloadRequestValues): Response<Autoload> =
        repository.updateAutoload(requestValues.postAutoload)
}