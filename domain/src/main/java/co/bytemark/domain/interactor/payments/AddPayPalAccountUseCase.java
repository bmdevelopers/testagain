package co.bytemark.domain.interactor.payments;

import android.app.Application;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.repository.PaymentsRepository;
import co.bytemark.sdk.pojo.PayPalAccount;
import rx.Observable;
import rx.Scheduler;

/**
 * Created by yashwant on 09/02/18.
 */
public class AddPayPalAccountUseCase extends UseCase<AddPayPalAccountUseCaseValue,List<PayPalAccount>, PaymentsRepository>{

    @Inject
    public AddPayPalAccountUseCase(PaymentsRepository repository,
                                 @Named("Thread") Scheduler threadScheduler,
                                 @Named("PostExecution") Scheduler postExecutionScheduler,
                                 Application application) {
        super(repository, threadScheduler, postExecutionScheduler, application);
    }

    @Override
    protected Observable<List<PayPalAccount>> buildObservable(AddPayPalAccountUseCaseValue requestValues) {
        return repository.addPayPalAccount(requestValues.jsonObject).map(data -> data.getBraintreePaypalPaymentMethods());
    }





}
