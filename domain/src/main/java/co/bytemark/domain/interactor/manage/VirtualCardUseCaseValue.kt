package co.bytemark.domain.interactor.manage

import co.bytemark.domain.interactor.UseCase.RequestValues
import co.bytemark.domain.model.manage.VirtualCard

class VirtualCardUseCaseValue(id: String?, nickname: String?) : RequestValues {
    var virtualCard: VirtualCard = VirtualCard()

    init {
        virtualCard.faremediaId = id
        virtualCard.faremediaNickname = nickname
    }
}