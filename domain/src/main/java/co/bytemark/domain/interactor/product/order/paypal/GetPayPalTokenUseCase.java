package co.bytemark.domain.interactor.product.order.paypal;

import android.app.Application;

import javax.inject.Inject;
import javax.inject.Named;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.interactor.product.GetPayPalTokenRequestValue;
import co.bytemark.domain.repository.ProductRepository;
import rx.Observable;
import rx.Scheduler;

/**
 * Created by yashwant on 30/01/18.
 */
public class GetPayPalTokenUseCase extends UseCase<GetPayPalTokenRequestValue, String, ProductRepository> {
    @Inject
    public GetPayPalTokenUseCase(ProductRepository repository,
                                 @Named("Thread") Scheduler threadScheduler,
                                 @Named("PostExecution") Scheduler postExecutionScheduler,
                                 Application application) {
        super(repository, threadScheduler, postExecutionScheduler, application);
    }

    @Override
    protected Observable<String> buildObservable(GetPayPalTokenRequestValue requestValues) {
        return repository.getPayPalToken()
                .map(bmResponse -> bmResponse.getData().getBrainTreeClientToken());
    }

    /*public String getPayPalToken(GetPayPalTokenRequestValue requestValues) {
        final String[] token = new String[1];
        repository.getPayPalToken(requestValues.authToken)
                .subscribe(bmResponse -> token[0] = bmResponse.getData().getBrainTreeClientToken());
        return token[0];
    }*/
}
