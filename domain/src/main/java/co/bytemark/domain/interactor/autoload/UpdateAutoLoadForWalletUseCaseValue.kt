package co.bytemark.domain.interactor.autoload

import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.payments.CreateWalletAutoload

/** Created by Santosh on 2019-08-28.
 */

data class UpdateAutoLoadForWalletUseCaseValue(internal var createWalletAutoload: CreateWalletAutoload)
    : UseCase.RequestValues