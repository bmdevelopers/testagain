package co.bytemark.domain.interactor.native_app_support

import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.NativeAppSupportRepository
import javax.inject.Inject

class NativeAppSupportUseCase @Inject internal constructor(
        val repository: NativeAppSupportRepository,
        handler: ErrorHandler)
    : UseCaseV2<NativeAppSupportRequestValues, BMResponse>(handler) {

    override suspend fun execute(requestValues: NativeAppSupportRequestValues): Response<BMResponse> =
            repository.sendAppSupportEmailAsync(requestValues.params)
}

class NativeAppSupportRequestValues(var params: Map<String, String>) : UseCase.RequestValues