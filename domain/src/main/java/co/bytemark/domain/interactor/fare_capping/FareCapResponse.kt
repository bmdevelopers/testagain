package co.bytemark.domain.interactor.fare_capping

import co.bytemark.domain.model.fare_capping.FareCapping
import com.google.gson.annotations.SerializedName

class FareCappingResponse(@SerializedName("farecappings") val fareCappingList: MutableList<FareCapping>)