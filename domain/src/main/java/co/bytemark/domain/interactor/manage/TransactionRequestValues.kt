package co.bytemark.domain.interactor.manage

import co.bytemark.domain.interactor.UseCase.RequestValues

class TransactionRequestValues(val fareMediaId: String, val pageIndex: Int) : RequestValues