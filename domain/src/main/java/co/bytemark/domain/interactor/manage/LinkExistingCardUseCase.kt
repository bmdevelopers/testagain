package co.bytemark.domain.interactor.manage

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.manage.AddSmartCard
import co.bytemark.domain.repository.ManageRepository
import javax.inject.Inject

class LinkExistingCardUseCase
@Inject internal constructor(val repository: ManageRepository, handler: ErrorHandler) :
        UseCaseV2<LinkExistingCardUseCaseValue, AddSmartCard>
        (handler) {

    override suspend fun execute(requestValues: LinkExistingCardUseCaseValue): Response<AddSmartCard> {
        return repository.linkExistingCardsAsync(requestValues.linkExistingCard)
    }

}