package co.bytemark.domain.interactor.securityquestions

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.security_questions.SecurityQuestionsResponse
import co.bytemark.domain.repository.SecurityQuestionRepository
import javax.inject.Inject

class UserSecurityQuestionUseCase @Inject constructor(
        private val repository: SecurityQuestionRepository,
        errorHandler: BmErrorHandler
) : UseCaseV2<Unit, SecurityQuestionsResponse>(errorHandler) {

    override suspend fun execute(requestValues: Unit): Response<SecurityQuestionsResponse> =
            repository.getSecurityQuestionOfUser()
}
