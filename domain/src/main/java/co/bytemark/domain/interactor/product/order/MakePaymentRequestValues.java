package co.bytemark.domain.interactor.product.order;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.sdk.post_body.MakePayment;

/**
 * Created by yashwant on 07/12/17.
 */
public class MakePaymentRequestValues implements UseCase.RequestValues {
    public final String orderUuid;
    public final MakePayment makePayment;
    public final String deeplinkJwtToken;

    public MakePaymentRequestValues(String orderUuid, MakePayment makePayment, String deeplinkJwtToken) {
        this.orderUuid = orderUuid;
        this.makePayment = makePayment;
        this.deeplinkJwtToken = deeplinkJwtToken;
    }
}
