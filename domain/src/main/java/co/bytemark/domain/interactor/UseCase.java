package co.bytemark.domain.interactor;

import android.app.Application;
import androidx.lifecycle.LiveData;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;

import co.bytemark.domain.error.BytemarkException;
import co.bytemark.domain.model.common.BMError;
import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.domain.model.common.Result;
import co.bytemark.domain.repository.Repository;
import co.bytemark.sdk.BytemarkSDK;
import co.bytemark.sdk.network_impl.BaseNetworkRequest;
import retrofit2.HttpException;
import rx.Observable;
import rx.Scheduler;
import rx.SingleSubscriber;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Func1;
import rx.subscriptions.CompositeSubscription;

/**
 * Abstract class for a Use Case (Interactor in terms of Clean Architecture).
 * This interface represents a execution unit for different use cases (this means any use case
 * in the application should implement this contract).
 * <p>
 * By convention each UseCase implementation will return the result using a {@link rx.Subscriber}
 * that will execute its job in a background thread and will post the result in the UI thread.
 */
public abstract class UseCase<Q extends UseCase.RequestValues, ResponseData, RepositoryType extends Repository> implements Subscription {

    protected final RepositoryType repository;

    protected final Scheduler threadScheduler;
    protected final Scheduler postExecutionScheduler;
    protected final Context context;

    private CompositeSubscription subs = new CompositeSubscription();

    public UseCase(RepositoryType repository,
                   @Named("Thread") Scheduler threadScheduler,
                   @Named("PostExecution") Scheduler postExecutionScheduler,
                   Application application) {
        this.repository = repository;
        this.threadScheduler = threadScheduler;
        this.postExecutionScheduler = postExecutionScheduler;
        this.context = application.getApplicationContext();
    }

    protected abstract Observable<ResponseData> buildObservable(Q requestValues);

    public void execute(Subscriber<ResponseData> useCaseSubscriber) {
        execute(null, useCaseSubscriber);
    }

    public void execute(Q requestValues, Subscriber<ResponseData> useCaseSubscriber) {
        subs.add(getObservable(requestValues).subscribe(useCaseSubscriber));
    }

    public void singleExecute(Q requestValues, SingleSubscriber<ResponseData> singleSubscriber) {
        subs.add(getObservable(requestValues).toSingle().subscribe(singleSubscriber));
    }

    /**
     * Returns the observable which has schedulers applied to it. Use this when you have to chain
     * multiple use caseFs.
     *
     * @return Observable with schedulers applied.
     */
    public Observable<ResponseData> getObservable(Q requestValues) {
        return buildObservable(requestValues)
                .subscribeOn(threadScheduler)
                .observeOn(postExecutionScheduler)
                .doOnSubscribe(() -> repository.register(UseCase.this))
                .doOnUnsubscribe(() -> repository.unregister(UseCase.this))
                //.retryWhen(new RetryWithDelay(3, 1000))
                .doOnError(throwable -> {
                    if (throwable instanceof BytemarkException) {
                        BytemarkException exception = (BytemarkException) throwable;
                        switch (exception.getStatusCode()) {
                            case BytemarkSDK.ResponseCode.TIME_INCONSISTENCY_DETECTED:
                                BytemarkSDK.logout(context);
                                break;
                        }
                    } else if (throwable instanceof HttpException && ((HttpException) throwable).code() == BaseNetworkRequest.UNAUTHORISED) {
                        BytemarkSDK.logout(context);
                    }
                });
    }

    public Observable<ResponseData> getObservable() {
        return getObservable(null);
    }

    public LiveData<Result<ResponseData>> getLiveData(Q requestValues) {
        return null;
    }

    @Override
    public boolean isUnsubscribed() {
        return !subs.hasSubscriptions();
    }

    @Override
    public void unsubscribe() {
        if (!isUnsubscribed()) {
            subs.clear();
        }
    }

    protected boolean hasErrors(BMResponse bmResponse) {
        return bmResponse.getErrors() != null && bmResponse.getErrors().size() > 0;
    }

    protected List<BMError> getErrorList(BMResponse bmResponse) {
        return bmResponse.getErrors();
    }

    protected List<BMError> getErrorsList(Throwable throwable) {
        List<BMError> errorList = new ArrayList<>();
        if (throwable instanceof HttpException) {
            HttpException exception = (HttpException) throwable;
            errorList.add(new BMError(exception.code(), exception.message()));
        } else {
            errorList.add(new BMError(0, throwable.getLocalizedMessage()));
        }
        return errorList;
    }

    /**
     * Interface to provide request values which will be transformed to response values by this
     * use case.
     * <p>
     * Classed implementing this interface should be ideally immutable.
     */
    public interface RequestValues {

    }

    public static class EmptyRequestValues implements RequestValues {

    }

    private static class RetryWithDelay implements Func1<Observable<? extends Throwable>, Observable<?>> {
        private final int maxRetries;
        private final int retryDelayMillis;
        private int retryCount;

        RetryWithDelay(final int maxRetries, final int retryDelayMillis) {
            this.maxRetries = maxRetries;
            this.retryDelayMillis = retryDelayMillis;
            this.retryCount = 0;
        }

        @Override
        public Observable<?> call(Observable<? extends Throwable> attempts) {
            return attempts
                    .flatMap((Func1<Throwable, Observable<?>>) throwable -> {
                        if (++retryCount < maxRetries) {
                            return Observable.timer(retryCount * retryDelayMillis, TimeUnit.MILLISECONDS);
                        }
                        return Observable.error(throwable);
                    });
        }
    }
}
