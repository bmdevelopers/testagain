package co.bytemark.domain.interactor.product.order

import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.order.ApplyPromoCode

class ApplyPromoCodeRequestValues
constructor(val applyPromoCode: ApplyPromoCode)
    : UseCase.RequestValues {
}