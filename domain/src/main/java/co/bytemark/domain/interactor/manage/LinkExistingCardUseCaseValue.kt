package co.bytemark.domain.interactor.manage

import co.bytemark.domain.interactor.UseCase.RequestValues
import co.bytemark.domain.model.manage.LinkExistingCard

class LinkExistingCardUseCaseValue(cardNumber: String?, cardNickName: String?, securityCode: String?) : RequestValues {
    var linkExistingCard: LinkExistingCard = LinkExistingCard()

    init {
        linkExistingCard.cardNumber = cardNumber
        linkExistingCard.cardNickname = cardNickName
        linkExistingCard.securityCode = securityCode
    }
}