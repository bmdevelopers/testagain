package co.bytemark.domain.interactor.autoload

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.autoload.Autoload
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.PaymentsRepository
import javax.inject.Inject

class SaveAutoLoadForWalletUseCase @Inject constructor(
    private val repository: PaymentsRepository,
    errorHandler: ErrorHandler
) : UseCaseV2<SetAutoLoadForWalletUseCaseValue, Autoload>(errorHandler) {

    override suspend fun execute(requestValues: SetAutoLoadForWalletUseCaseValue): Response<Autoload> =
        repository.setAutoloadForWallet(requestValues.createWalletAutoload)
}