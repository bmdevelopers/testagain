package co.bytemark.domain.interactor.autoload

import co.bytemark.domain.interactor.UseCase.RequestValues

class GetAutoLoadForFareMediumRequestValue(val fareMediaId: String) : RequestValues