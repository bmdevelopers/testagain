package co.bytemark.domain.interactor.userphoto

import co.bytemark.domain.interactor.UseCase.EmptyRequestValues
import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.UserPhotoRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetUserPhoto @Inject constructor(val repository: UserPhotoRepository,
                                       handler: ErrorHandler)
    : UseCaseV2<EmptyRequestValues, UserPhotoResponse?>(handler) {


    override suspend fun execute(requestValues: EmptyRequestValues): Response<UserPhotoResponse?> =
            repository.getUserPhoto()
}