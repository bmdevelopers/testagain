package co.bytemark.domain.interactor.payments;

import co.bytemark.domain.interactor.UseCase;

/**
 * Created by yashwant on 13/02/18.
 */
public class DeletePayPalAccountUseCaseValue implements UseCase.RequestValues {
    String payPalToken;

    public DeletePayPalAccountUseCaseValue(String payPalToken) {
        this.payPalToken = payPalToken;
    }

}
