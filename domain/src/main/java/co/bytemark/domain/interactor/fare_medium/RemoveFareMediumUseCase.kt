package co.bytemark.domain.interactor.fare_medium

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.FareMediumRepository
import javax.inject.Inject

class RemoveFareMediumUseCase @Inject internal constructor(
    val repository: FareMediumRepository, val handler: ErrorHandler
) : UseCaseV2<RemoveFareMediumUseCase.RemoveFareMediumRequestValue, Any>(handler) {

    override suspend fun execute(requestValues: RemoveFareMediumRequestValue): Response<Any> {
        return repository.removeFareMedium(requestValues.fareMediumId)
    }

    class RemoveFareMediumRequestValue(val fareMediumId: String)
}