package co.bytemark.domain.interactor.passes

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.repository.PassesRepository
import co.bytemark.sdk.Api.JsonResponse
import co.bytemark.sdk.BytemarkSDK
import co.bytemark.sdk.PassCacheDatabaseManager
import co.bytemark.sdk.Passes
import co.bytemark.sdk.passcache.PassCacheManager
import com.google.gson.JsonObject
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject
import javax.inject.Named

/** Created by Santosh on 2019-11-21.
 * Copyright (c) 2019 Bytemark Inc. All rights reserved.
 */

class TransferPassUseCaseV1 @Inject constructor(
    repository: PassesRepository,
    @Named("Thread") threadScheduler: Scheduler?,
    @Named("PostExecution") postExecutionScheduler: Scheduler?,
    application: Application?
) : UseCase<TransferPassUseCaseValue, Passes, PassesRepository>(
    repository, threadScheduler, postExecutionScheduler, application
) {

    override fun getLiveData(requestValues: TransferPassUseCaseValue): LiveData<Result<Passes>> {
        val result = MediatorLiveData<Result<Passes>>()
        result.postValue(Result.Loading(null))

        val exceptionHandler =
            CoroutineExceptionHandler { _, e ->
                when (e) {
                    is IOException ->
                        result.postValue(
                            Result.Failure(
                                arrayListOf(
                                    BMError(BytemarkSDK.ResponseCode.CONNECTION_NOT_AVAILABLE, "")
                                )
                            )
                        )
                    else ->
                        result.postValue(Result.Failure(arrayListOf(BMError(0, ""))))
                }
                Timber.e(e)
            }

        GlobalScope.launch(exceptionHandler) {

            var jsonObject: JsonObject? = null
            launch(Dispatchers.IO) {
                jsonObject = repository.transferPassAsync(
                    requestValues.passUuid, requestValues.destination
                ).await()
            }.join()
            val response = JsonResponse(jsonObject.toString())
            if (response.errors.length() > 0) {
                val firstError = response.errors.getJSONObject(0)
                var errorMsg = firstError.optString("message")
                val code = firstError.optInt("code")
                if (errorMsg == null) errorMsg = ""
                result.postValue(Result.Failure(listOf(BMError(code, errorMsg))))
            } else {
                var passes: Passes? = null
                launch(Dispatchers.IO) {
                    if (requestValues.destination == CLOUD) {
                        // Clear DEVICE pass from the database to avoid duplication
                        PassCacheDatabaseManager.getInstance(context)
                            .deletePass(requestValues.passUuid)
                    }
                    PassCacheManager.get().evictAll()
                    passes = PassCacheDatabaseManager.getInstance(context)
                        .parseJsonResponseAndSaveWithDBClear(response)
                }.join()
                result.postValue(Result.Success(passes))
            }
        }
        return result
    }

    override fun buildObservable(requestValues: TransferPassUseCaseValue?): Observable<Passes> =
        Observable.empty()

    companion object {
        const val DEVICE = "device"
        const val CLOUD = "cloud"
    }
}