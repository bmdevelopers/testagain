package co.bytemark.domain.interactor.voucher_redeem

import co.bytemark.domain.interactor.UseCase
import com.google.gson.annotations.SerializedName

data class VoucherRedeemUseCaseValue(@SerializedName("order_type") val orderType: String,
                                     @SerializedName("voucher_code") val voucherCode: String,
                                     @SerializedName("save_to_device") val saveToDevice: Boolean) : UseCase.RequestValues