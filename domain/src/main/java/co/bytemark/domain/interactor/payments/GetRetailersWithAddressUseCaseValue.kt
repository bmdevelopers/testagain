package co.bytemark.domain.interactor.payments

import co.bytemark.domain.interactor.UseCase

data class GetRetailersWithAddressUseCaseValue(
    var address: String,
    var radius: Double,
    var orderUuid :String
) : UseCase.RequestValues