package co.bytemark.domain.interactor.ticket_history

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.repository.TicketHistoryRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

class TicketHistoryUseCase @Inject constructor(repository: TicketHistoryRepository,
                                               @Named("Thread") threadScheduler: Scheduler?,
                                               @Named("PostExecution") postExecutionScheduler: Scheduler?,
                                               application: Application?)
    : UseCase<TicketHistoryUseCaseValue, Data, TicketHistoryRepository>(repository, threadScheduler, postExecutionScheduler, application) {

    override fun getLiveData(requestValue: TicketHistoryUseCaseValue): LiveData<Result<Data>>? {
        val result = MediatorLiveData<Result<Data>>()
        result.postValue(Result.Loading(null))

        GlobalScope.launch {
            try {
                val value = repository.getTicketHistoryAsync(requestValue.perPage, requestValue.status, requestValue.nextPageURL).await()
                if (hasErrors(value)) result.postValue(Result.Failure(getErrorList(value))) else result.postValue(Result.Success(value.data))
            } catch (t: Throwable) {
                Timber.e(t.message)
                result.postValue(Result.Failure(arrayListOf(BMError(0, ""))))
            }
        }
        return result
    }

    override fun buildObservable(requestValues: TicketHistoryUseCaseValue?): Observable<Data>? {
        return Observable.empty()
    }
}