package co.bytemark.domain.interactor.payments

import android.app.Application
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.repository.PaymentsRepository
import rx.Scheduler
import javax.inject.Inject
import javax.inject.Named

class DeletePaymentMethodUseCase @Inject constructor(
        repository : PaymentsRepository,
        @Named("Thread") threadScheduler : Scheduler,
        @Named("PostExecution") postExecutionScheduler : Scheduler,
        application : Application) : UseCase<DeletePaymentMethodUseCase.DeletePaymentMethodUseCaseValue, BMResponse, PaymentsRepository>(
        repository, threadScheduler, postExecutionScheduler,application) {


    override fun buildObservable(requestValues : DeletePaymentMethodUseCaseValue) =
            repository.deletePaymentMethod(requestValues.paymentUUID)

    class DeletePaymentMethodUseCaseValue(val paymentUUID: String) : UseCase.RequestValues
}

