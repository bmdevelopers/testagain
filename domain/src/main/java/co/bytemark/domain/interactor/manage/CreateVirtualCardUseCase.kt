package co.bytemark.domain.interactor.manage

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.Data
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.ManageRepository
import javax.inject.Inject

class CreateVirtualCardUseCase
@Inject internal constructor(val repository: ManageRepository, handler: ErrorHandler) :
        UseCaseV2<VirtualCardUseCaseValue, Data>
        (handler) {

    override suspend fun execute(requestValues: VirtualCardUseCaseValue): Response<Data> {
        return repository.createVirtualCardAsync(requestValues.virtualCard)
    }
}