package co.bytemark.domain.interactor.product.order;

import android.app.Application;

import javax.inject.Inject;
import javax.inject.Named;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.model.common.BMResponse;
import co.bytemark.domain.model.common.Data;
import co.bytemark.domain.repository.ProductRepository;
import rx.Observable;
import rx.Scheduler;

/**
 * Created by yashwant on 19/12/17.
 */
public class CreateOrderUseCase extends UseCase<PurchaseRequestValues, Data, ProductRepository> {

    @Inject
    public CreateOrderUseCase(ProductRepository repository,
                              @Named("Thread") Scheduler threadScheduler,
                              @Named("PostExecution") Scheduler postExecutionScheduler,
                              Application application) {
        super(repository, threadScheduler, postExecutionScheduler, application);
    }

    @Override
    public Observable<Data> buildObservable(PurchaseRequestValues requestValues) {
        return repository.createOrder(requestValues.createOrder, requestValues.deepLinkJwtToken)
                .map(BMResponse::getData);
    }

}
