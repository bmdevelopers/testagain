package co.bytemark.domain.interactor.fare_medium;

import android.app.Application;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import co.bytemark.domain.interactor.UseCase;
import co.bytemark.domain.model.fare_medium.FareMedium;
import co.bytemark.domain.repository.FareMediumRepository;
import rx.Observable;
import rx.Scheduler;

public class GetFareMedia extends UseCase<FareMediumRequestValues, List<FareMedium>, FareMediumRepository> {

    @Inject
    GetFareMedia(FareMediumRepository repository,
                 @Named("Thread") Scheduler threadScheduler,
                 @Named("PostExecution") Scheduler postExecutionScheduler,
                 Application application) {
        super(repository, threadScheduler, postExecutionScheduler, application);
    }

    @Override
    protected Observable<List<FareMedium>> buildObservable(FareMediumRequestValues requestValues) {
        boolean loadFromLocalStore = requestValues!= null && requestValues.loadFromLocalStore;
        return repository.getFareMediums(loadFromLocalStore);
    }
}
