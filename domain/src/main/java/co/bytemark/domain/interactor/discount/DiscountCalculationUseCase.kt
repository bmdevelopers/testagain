package co.bytemark.domain.interactor.discount

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.bytemark.domain.interactor.UseCase
import co.bytemark.domain.model.common.BMError
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.discount.DiscountCalculationRequest
import co.bytemark.domain.model.discount.DiscountData
import co.bytemark.domain.repository.DiscountRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rx.Observable
import rx.Scheduler
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

class DiscountCalculationUseCase @Inject constructor(
    repository: DiscountRepository,
    @Named("Thread") threadScheduler: Scheduler?,
    @Named("PostExecution") postExecutionScheduler: Scheduler?,
    application: Application?
) : UseCase<DiscountCalculationUseCase.DiscountCalculationUseCaseValue, DiscountData, DiscountRepository>(
    repository, threadScheduler, postExecutionScheduler, application
) {

    override fun getLiveData(requestValues: DiscountCalculationUseCaseValue): LiveData<Result<DiscountData>> {
        val result = MediatorLiveData<Result<DiscountData>>()
        result.postValue(Result.Loading(null))

        GlobalScope.launch {
                launch(Dispatchers.IO) {
                    try {
                        val value = repository.discountCalculation(
                            requestValues.discountCalculationRequest,
                            requestValues.deeplinkJwtToken
                        ).await()
                        if (hasErrors(value)) result.postValue(Result.Failure(getErrorList(value)))
                        else result.postValue(Result.Success(value.data.discountData))
                    } catch (t: Throwable) {
                        Timber.e(t.message)
                        result.postValue(Result.Success(null))
                    }
                }
        }
        return result
    }

    class DiscountCalculationUseCaseValue(
        val discountCalculationRequest: DiscountCalculationRequest,
        val deeplinkJwtToken: String
    ) : RequestValues

    override fun buildObservable(requestValues: DiscountCalculationUseCaseValue?): Observable<DiscountData> =
        Observable.empty()
}