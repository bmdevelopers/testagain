package co.bytemark.domain.interactor.fare_medium;

import co.bytemark.domain.interactor.UseCase;

/**
 * Created by Arunkumar on 21/07/17.
 */
public class FareMediumRequestValues implements UseCase.RequestValues {
    public final String fareMediumUuid;
    public final boolean loadFromLocalStore;

    public FareMediumRequestValues(String fareMediumUuid) {
        this.fareMediumUuid = fareMediumUuid;
        this.loadFromLocalStore = false;
    }

    public FareMediumRequestValues(String fareMediumUuid, boolean loadFromLocalStore) {
        this.fareMediumUuid = fareMediumUuid;
        this.loadFromLocalStore = loadFromLocalStore;
    }
}
