package co.bytemark.domain.interactor.authentication

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.BMResponse
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.AuthenticationRepository
import javax.inject.Inject

class RegisterNewUserUseCase @Inject internal constructor(
    val repository: AuthenticationRepository,
    val handler: ErrorHandler
) : UseCaseV2<RegisterNewUserUseCase.RegisterNewUserRequestValues, Any>(handler) {

    override suspend fun execute(requestValues: RegisterNewUserRequestValues): Response<Any> {
        return repository.registerNewUser(requestValues.params)
    }

    inner class RegisterNewUserRequestValues(var params: MutableMap<String, Any?>)
}