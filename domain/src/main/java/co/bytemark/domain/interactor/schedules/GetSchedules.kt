package co.bytemark.domain.interactor.schedules

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.ErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.repository.GTFSRepository
import co.bytemark.gtfs.DataObjects.Schedule
import javax.inject.Inject

class GetSchedules @Inject constructor(val repository: GTFSRepository, handler: ErrorHandler)
    : UseCaseV2<GTFSRequestValues, MutableList<Schedule>>(handler) {

    override suspend fun execute(requestValues: GTFSRequestValues): Response<MutableList<Schedule>> =
            repository.getSchedules(requestValues.originId, requestValues.destinationId, requestValues.date)

}