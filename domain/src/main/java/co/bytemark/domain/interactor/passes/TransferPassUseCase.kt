package co.bytemark.domain.interactor.passes

import android.app.Application
import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.common.Result
import co.bytemark.domain.model.ticket_storage.TransferPassResponse
import co.bytemark.domain.repository.TransferPassRepository
import co.bytemark.sdk.Api.JsonResponse
import co.bytemark.sdk.PassCacheDatabaseManager
import co.bytemark.sdk.Passes
import co.bytemark.sdk.model.ticket_storage.TicketStorageTargetType
import co.bytemark.sdk.passcache.PassCacheManager
import com.google.gson.Gson
import javax.inject.Inject

class TransferPassUseCase @Inject constructor(
    private val transferPassRepository: TransferPassRepository,
    bmErrorHandler: BmErrorHandler,
    private val application: Application
) : UseCaseV2<TransferPassUseCase.TransferPassUseCaseValue, TransferPassResponse>(bmErrorHandler) {

    override suspend fun execute(requestValues: TransferPassUseCaseValue): Response<TransferPassResponse> =
        transferPassRepository.transferPass(requestValues.passUuid, requestValues.destination)

    override suspend fun invoke(request: TransferPassUseCaseValue): Result<TransferPassResponse> {
        val result = super.invoke(request)
        if (result is Result.Success) {
            var passes: Passes? = null
            if (request.ticketStorageTargetType == TicketStorageTargetType.TYPE_CLOUD) {
                // Clear DEVICE pass from the database to avoid duplication
                PassCacheDatabaseManager.getInstance(application.applicationContext)
                    .deletePass(request.passUuid)
            }
            PassCacheManager.get().evictAll()

            passes = PassCacheDatabaseManager.getInstance(application.applicationContext)
                .parseJsonResponseAndSaveWithDBClear(
                    JsonResponse(Gson().toJson(result))
                )
            result.data?.passes = passes.passes
        }
        return result
    }

    inner class TransferPassUseCaseValue(
        val passUuid: String,
        val destination: String,
        val ticketStorageTargetType: TicketStorageTargetType
    )
}