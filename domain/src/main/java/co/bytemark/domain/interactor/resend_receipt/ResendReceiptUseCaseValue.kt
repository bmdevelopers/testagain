package co.bytemark.domain.interactor.resend_receipt

import co.bytemark.domain.interactor.UseCase

class ResendReceiptUseCaseValue(var orderUUID: String) : UseCase.RequestValues