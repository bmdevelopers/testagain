package co.bytemark.domain.interactor.autoload

import co.bytemark.domain.interactor.UseCase

/** Created by Santosh on 2019-08-28.
 */

data class DeleteAutoLoadForWalletUseCaseValue(var walletUuid: String) : UseCase.RequestValues