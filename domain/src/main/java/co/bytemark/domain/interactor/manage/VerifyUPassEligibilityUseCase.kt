package co.bytemark.domain.interactor.manage

import co.bytemark.domain.interactor.UseCaseV2
import co.bytemark.domain.model.common.BmErrorHandler
import co.bytemark.domain.model.common.Response
import co.bytemark.domain.model.manage.UPassEligibilityRequestData
import co.bytemark.domain.model.manage.UPassEligibilityResponseData
import co.bytemark.domain.repository.ManageRepository
import javax.inject.Inject

class VerifyUPassEligibilityUseCase @Inject constructor(
        private val manageRepository: ManageRepository,
        bmErrorHandler: BmErrorHandler
) : UseCaseV2<UPassVerificationUseCaseValue, UPassEligibilityResponseData>(bmErrorHandler) {

    override suspend fun execute(requestValues: UPassVerificationUseCaseValue)
            : Response<UPassEligibilityResponseData> =
            manageRepository.checkUPassEligibility(requestValues.institutionId, UPassEligibilityRequestData(requestValues.fareMediumId))

}