package co.bytemark.helpers

import co.bytemark.sdk.serializers.CalendarSerializer
import co.bytemark.sdk.serializers.NativeV3ConfigSerializer
import co.bytemark.sdk.serializers.TimezoneSerializer
import co.bytemark.sdk.model.config.NativeV3Configuration
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.util.*

object GsonFactory {
    private var INSTANCE: Gson? = null
    @JvmStatic
    val gson: Gson
        get() {
            if (INSTANCE == null) {
                val gsonBuilder = GsonBuilder()
                gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                gsonBuilder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                gsonBuilder.registerTypeHierarchyAdapter(
                    TimeZone::class.java,
                    TimezoneSerializer()
                )
                gsonBuilder.registerTypeHierarchyAdapter(
                    Calendar::class.java,
                    CalendarSerializer()
                )
                gsonBuilder.registerTypeHierarchyAdapter(
                    NativeV3Configuration::class.java,
                    NativeV3ConfigSerializer()
                )
                INSTANCE = gsonBuilder.create()
            }
            return INSTANCE!!
        }
}