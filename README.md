# Customer Mobile Application (_Leave the campground cleaner than you found it_) #

### What is this repository for? ###

* This is the official repository for the Customer Mobile 4.0 Android Application
* Version 4.4.9
* Firebase Console : https://console.firebase.google.com/
* Google Cloud Console : https://console.cloud.google.com/apis/dashboard?project=bytemark-uat

### Version Control ###

* Create branch name from the ticket you are assigned. Branch naming format: BM/bm-ticket-number
* When a ticket is completed update the CHANGELOG.md file and then create a PR.
* After code review, you send the feature build to QA for testing from your respective branch.
* Inform your QA team members and send them the branch name.
* Your QA team members will then get the build for testing from jenkins.
* If the testing brings up issues related to the current feature, work on the same branch and create a PR again.
* If the issue raised is not related to your current feature, inform your Project Managers about it and they will
  probably create a new ticket for you or other team to work on.
* After successful QA, increment the patch number and your branch is merged to develop.

### Sending builds to store ###

* When sending the build to store, bump the minor version number.
* Do a git flow and create a release branch from develop named v-version-name.
* Afterwards the release branch is merged to master with a tag named v-version-name.

### Sending builds to client ###

* Send the build to client from jenkins. (That is his job)
* Jenkins will build the apk for you, increment the patch number and commit it to version control.
* After successful build, the apk is send to fabric and notified on slack.

### How do I get set up? ###

* Building
> You can open the project in Android Studio and press run. It should work fine. If you get any problems, try 
  contacting your fellow team members to help you set up.
* Testing
> The project uses both instrumentation tests that run on the device and local unit tests that 
run on your computer. To run both of them and generate a coverage report, you can run:
`./gradlew fullCoverageReport` (requires a connected device or an emulator)
* UI Tests
> The projects uses Espresso for UI testing. Since each fragment is limited to a ViewModel, each 
test mocks related ViewModel to run the tests.
* Database Tests
> The project creates an in memory database for each database test but still runs them on the device.
* ViewModel Tests
> Each ViewModel is tested using local unit tests with mock Repository implementations.
* Repository Tests
> Each Repository is tested using local unit tests with mock web service and mock database.
* Webservice Tests
> The project uses [MockWebServer][mockwebserver] project to test REST api interactions.

### How does the Customer Mobile builds itself? ###

* The _Customer Mobile_ builds itself by downloading a configuration file _(config.json)_ from remote 
webservice and parsing build parameters and downloading the corresponding build configurations namely:
    * "navigation.json"
    * "settings.json"
    * "more_menu.json"
    * "formly_sign_in.json"
    * "formly_forgot_password.json"
    * "formly_voucher_code.json" 
    * "formly_sign_up.json"
    * "formly_change_password.json"
    * "formly_account_management.json"
* Building Online (Android Studio -> Preferences -> Gradle -> Offline work -> unchecked)
> When online, the existing configuration files and it's downloaded corresponding files will be
deleted and download the new set of configuration files given the _Slug_ and the _environment_
and build the Customer Mobile.
* Building Offline (Android Studio -> Preferences -> Gradle -> Offline work -> checked)
> When offline, the existing configuration files and it's downloaded corresponding files will be
persisted and used for building the Customer Mobile.

### Contribution guidelines ###

#### Coding Standards ####

##### 1. Meaningful Names #####

* Use Intention-Revealing Names 
> Choosing names that reveal intent can make it much easier to understand and change code. eg. 
`elapsedTimeInDays`, `daysSinceCreation`, `daysSinceModification`.
* Avoid Disinformation 
> Avoid leaving false clues that obscure the meaning of code. e.g. Do not refer to a grouping of 
accounts as an `accountList` unless it's actually a `List`. It may lead to false conclusions. So 
`accountGroup`or `bunchOfAccounts` or just a plain `accounts` would be better.
* Make Meaningful Distinctions 
> Distinguish names in such a way that the reader know what the differences offer.
* Use Pronounceable Names 
> If you can't pronounce it, you can't discuss it without sounding like an idiot.
* Use Searchable Names 
> The length of a name should correspond to the size of its scope.
* Hungarian Notation 
> Java/Kotlin programmers don't need type encoding. HN and other forms of type encoding are simply 
_impediments_.
* Member Prefixes
> You also don't need to prefix member variables with `m_`.
* Interface and Implementation 
> Encode the implementation and not the interface. e.g. "ShapeFactoryImp" is preferable to 
encoding the interface.
* Avoid Mental Mapping
>Readers shouldn't have to mentally translate your names into other names they already know.
* Class Names 
> Classes and objects should have noun or noun phrase names like `Customer`, `WikiPage`, 
`Account`, and `AddressParser`. Avoid words like `Manager`, `Processor`, `Data`, or `Info` in 
the name of a class. A class name should not be a verb.
* Method Names 
> Methods should have verb or verb phrase names like `postPayment`, `deletePage`,or `save`. 
Accessors, mutators and predicates should be named for their value and prefix with `get`, `set` 
and `is` according to the javabean standard.
    
##### 2. Functions #####

* Small!
> First rule of functions is that they should be small. The second rule of functions is that 
they should be smaller than that.
* Blocks and Indenting 
> This implies that the block within `if`, `else`, `while` statements and so on should be one 
line long. Probably that line should be a function call. Not only does this keep the enclosing 
function small, but it also adds documentary value.
* Do One Thing
> _FUNCTIONS SHOULD DO ONE THING. THEY SHOULD DO IT WELL. THEY SHOULD DO IT ONLY._
* One Level of Abstraction per Function
> In order to make sure functions are doing "one thing", we need to make sure that the statements 
within our function are all at the same level of abstraction.
* Reading Code from Top to Bottom: _The Stepdown Rule_ 
> We want the code to read like a top-down narrative. We want every function to ebe followed by 
those at the next level of abstraction so that we can read the program, descending one level of 
abstraction at a time as we read down the list of functions.
* Use Descriptive Names 
> _"You know you are working on a clean code when each routine turns out to be pretty much what 
you expected."_ Don't be afraid to make a name long. A long descriptive name is better than a 
short enigmatic name. A long descriptive name is better than a long descriptive comment. Be 
consistent in your names. eg. `includeSetupPages`, `includeTeardownPages`.
* Function Arguments
> The ideal number of arguments for a function is zero (niladic). Next comes one(monadic), 
followed closely by two (dyadic). Three arguments (triadic) should be avoided where possible. 
More than three (polyadic) requests very special justification-and then shouldn't be used anyways.
* Common Monadic Forms 
> There are two very common reasons to pass a single argument into a function. You may be asking 
a question about that argument eg. `boolean fileExists("MyFile)`, or you may be operating on that 
argument eg. `InputStream fileOpen("MyFile)`. These two uses are what readers expect when they 
see a function.
* Flag Arguments 
> Flag arguments are ugly. Passing a boolean into a function is a truly terrible practise. It 
immediately complicate the signature of the method, loudly proclaiming that this function does 
more than one thing. Split the function into two in such case.
* Dyadic Function 
> A function with two arguments is harder to understand than a monadic function. For eg. 
`writeField(name)` is easier to understand than `writeField(output-stream, name)`. Dyads aren't 
evil, you will certainly have to write them. However, you should be aware that they come with a 
cost and should take advantage of what mechanisms may be available to you to convert them into 
monads.
* Triads 
> Functions that take three arguments are significantly harder to understand than dyads. The 
issues of ordering, pausing and ignoring are more than doubled. We suggest you to think very 
carefully before creating a triad.
* Argument Objects 
> When a function seems to need more than two or three arguments, it is likely that some of those 
arguments ought to be wrapped into a class of their own. eg. `Circle makeCircle(double x, double 
y, double radius)` `Circle makeCircle(Point center, double radius)`.
* Argument List 
> Sometimes we want to pass a variable number of arguments into a function. eg. `String.format
("%s worked %.2f hours", name, hours)`. If the variable argument are all treated identically, 
as they are in the example above, then they are equivalent to a single argument of type `List`.
* Have No Side Effects 
> Side effects are lies. Your function promises to do one thing, but it also does other hidden 
things. eg. `void changePassword()` should not perform `Session.initialize()`. In this case we 
might rename the function to `void checkPasswordAndInitializeSession()`. _Do one thing_.
* Output Argument 
> In general output arguments should be avoided. If your function must change the state of 
something, have it change the state of its own owing object.
* Command Query Separation 
> Functions should either do something or answer something, but not both. Either your function 
should change the state of an object, or it should return some information about that object. 
Doing both often leads to confusion. eg. `if(set("username", "unclebob"))...`. Separate the 
command from the query so that the ambiguity cannot occur. `if(attributeExists("username)) 
setAttribute("username", "unclebob")`.
* Prefer Exceptions to Returning Error Codes 
> Returning error codes from command function is a subtle violation of command query separation. 
When you return an error code, you create the problem that the caller must deal with the error 
immediately.
* Extract Try/Catch Blocks 
> `Try/Catch` blocks are ugly in their own. They confuse the structure of the code and mix error 
processing with normal processing. So it is better to extract the bodies of the `try` and `catch` 
blocks out into functions of their own.
* Error Handling Is One Thing 
> Functions should do one thing. Error handling is one thing. Thus a function that handles errors 
should do nothing else.
* The `Error.java` Dependency Magnet 
> Returning error codes usually implies that there is some class or enum in which all the error 
codes are defined. Classes like this are a _dependency magnet_; many other classes import and use 
them. When you use exceptions rather than error codes, then new exceptions are _derivatives_ of 
the exception class. They can be added without forcing any recompilation or redeployment.
* Don't Repeat Yourself (The DRY principle) 
> Duplication may be the root of all evil in software. 
    
##### 3. Comments #####

Truth can only be found in one place: the code. Only the code can truly tell you what it does. It is 
the only source of truly accurate information. Therefore, though comments are sometimes necessary, we
will expend significant energy to minimize them.

* Comments Do Not Make Up for Bad Code 
> Clear and expressive code with few comments is far superior to cluttered and complex code with 
lots of comments. Rather than spend your time writing the comments that explain the code, spend 
it cleaning the code.
* Explain Yourself in Code 
> It takes only a few seconds of thought to explain most of your intent in code. In many cases 
it's simply a matter of creating a function that says the same thing as the comment you want to 
write.
* Good Comments 
> Some comments are necessary or beneficial. The only truly good comment is the comment you found 
a way not to write.
* Informative Comments 
> It is sometimes useful to provide basic information with a comment. eg. A comment that explains 
the return value of an abstract method.
* Explanation of Intent 
> Sometimes a comment goes beyond just useful information about the implementation and provides 
the intent behind a decision.
* Clarification 
> Sometimes it is just helpful to translate the meaning of some obscure argument or return value 
into something that's readable. In general it is better to find a way to make that argument or 
return value clear in its own right; but when its part of the standard library, or in code that 
you cannot alter, then a helpful clarifying comment can be useful. eg. `assertTrue(a.compareTo(a) 
== 0) // a == a`.
* Warning of Consequences 
> Sometimes it is useful to warn other programmers about certain consequences.
* TODO comments 
> It is sometimes reasonable to leave "To di" notes in form of `//TODO` comments. `TODO`s are 
jobs that the programmer thinks should be done, but for some reason can't do at the moment.
* Mumbling 
> Plopping in a comment just because you feel you should or because the process requires it, is 
a hack. If you decide to write a comment, then spend the time necessary to make sure it is the 
best comment you can write.
* Redundant Comments 
> These comments serve only to clutter and obscure the code. They serve no documentary purpose at all.
* Misleading Comments 
> Sometimes, with all the best intentions, a programmer makes a statement in his comments that 
isn't precise enough to be accurate. This subtle bit of information , couched in a comment that 
is harder to read than the body of the code, could cause another programmer to blithely call this 
function.
* Mandated Comments 
> It is just plain silly to have a rule that says that every function must have a javadoc, or 
every variable must have a comment. Comment like this just clutter up the code, propagate lies, 
and lend to general confusion and disorganization.
* Journal Comments 
> Sometimes people add a comment to the start of a module every time they edit it. These comments 
accumulate as a kind of journal, or log, of every change that has ever been made.
* Noise Comments 
> Sometimes you see comments that are nothing but noise. They restate the obvious and provide no 
new information. eg. `// Default Constructor`.
* Don't Use a Comment When You Can Use a Function or a Variable
* Position Markers 
> Sometimes programmers like to mark a position in a source file. There are rare times when it 
makes sense to gather certain functions together beneath a banner. But in general they are clutter 
that should be eliminated.
* Closing Brace Comment
> Sometimes programmers will put special comments on closing braces. Although this might make 
sense for long functions with deeply nested structures, it serves only to clutter the kind of 
small and encapsulated functions that we prepare.
* Attribution and Bylines 
> `/* Added by Rick */` Source code control system are very good at remembering who added what, 
when. There is no need to pollute the code with little bylines.
* Commented-Out Code 
> Few practices are as odious as commenting-out code. Don't do this!. Just delete the code. We 
won't loose it. Promise.
* HTML Comments 
> HTML source code comments is an abomination.
* Nonlocal Information 
> If you must write a comment, then make sure it describes the code it appears near. Don't offer 
systemwide information in the context of a local comment.
* Too Much Information 
> Don't put interesting historical discussions or irrelevant description of details into your 
comments.
* Inobvious Connection  
> The connection between a comment and the code it describes should be obvious. If you are going 
to the trouble to write a comment, then at least you'd like the reader to be able to look at the 
comment and the code and understand what the comment is talking about.
* Function Header 
> Short function don't need much description. A well-chosen name for a small function that does 
one thing is usually better than a comment header.

##### 4. The Law of Demeter #####

There is a well known heuristic called _Law of Demeter_ that says a module should not know about the
innards of the _objects_ it manipulates. This means that an object should not expose its internal
structure through accessors because to do so is to expose, rather than to hide, its internal structure.

More precisely the Law of Demeter says that a method _f_ of a class _C_ should only call the methods
of these:
* _C_
* An object created by _f_
* An object passes as an argument to _f_
* An object held in an instance variable of _C_
   
The method should _not_ invoke methods on objects that are returned by any of the allowed functions.
In other words, talk to friends, not to strangers.

##### 5. Unit Tests #####

* Three Laws of TDD(Test Driven Development)
    * First Law :- You may not write production code until you have written a failing unit test.
    * Second Law :- You may not write more of a unit test than is sufficient to fail, and not 
    compiling is failing.
    * Third Law :- You many not write more production code than is sufficient to pass the currently 
    failing test.
* Keep Tests Clean :- Test code is just as important as production code It is not a second class 
citizen. It requires thought, design, and care. It must be kept as clean as production code.

F.I.R.S.T

Clean tests follow five other rules than form the above acronym:
* Fast
>Test should be fast. They should run quickly. When tests run slow, you won't want to run 
them frequently. If you don't run them frequently, you won't find problems early enough to fix them
easily. You won't feel as free to clean up the code. Eventually the code will begin to rot.
* Independent
> Tests should not depend on each other. One test should not set up the conditions for
the next test. You should be able to run each test independently and run the tests in any order you
like. When tests depend on each other, then the first one fails causes a cascade of downstream failures,
making diagnosis difficult and hiding downstream defects.
* Repeatable
> Tests should be repeatable in any environment. You should be able to run the tests in 
the production environment, in the QA environment, and on your laptop while riding home on the train
without a network. If your tests aren't repeatable in any environment, then you'll have an excuse for
why they fail. You'll also find yourself unable to run the tests when the environment isn't available.
* Self-Validating
> The tests should have a boolean output. Either they pass or fail. You should not
have to read through a log file to tell whether the tests pass. You should not have to manually compare
two different text files to see whether the tests pass. If the tests aren't self-validating, then failure
can become subjective and running the tests can require a long manual evaluation.
* Timely
> The tests need to be written in a timely fashion. Unit tests should be written _just before_
the production code that makes them pass. If you write tests after the production code, then you may
find the production code to be hard to test. You may decide that some production code is too hard to 
test. You may not design the production code to be testable.

If you let the tests rot, then your code will rot too. Keep your tests clean.

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* android-team@bytemark.co

### Libraries

* [Gradle Download Task] [gradle-download-task] for adding a download task to Gradle that displays 
progress information
* [Android Support Library][support-lib]
* [Android Architecture Components][arch]
* [Android Data Binding][data-binding]
* [Dagger 2][dagger2] for dependency injection
* [Retrofit][retrofit] for REST api communication
* [Glide][glide] for image loading
* [Timber][timber] for logging
* [espresso][espresso] for UI tests
* [mockito][mockito] for mocking in tests
* [RxJava] [rx-java]  for Reactive Extensions
* [RxAndroid] [rx-android] for RxJava bindings
* [Material Dialogs] [material-dialogs] for customizable dialogs API
* [SQLCipher] [sql-cipher] for encrypting SQLite database
* [Anko] [anko] for app development in kotlin
* [Ticket] [ticker] for scrolling text change animation
* [AboutLibraries] [about-libraries] for information of libraries
* [AdapterDelegates] [adapter-delegates] for recyclerview adapters
* [EasyPermissions] [easy-permissions] for simplifying Android M system permissions
* [uCrop] [ucrop] for image cropping
* [Sectioned RecyclerView] [sectioned-recyclerview] for multi-sectioned recyclerview
* [Apache Commons Lang] [apache-commons-lang] for utility classes

[gradle-download-task]: https://github.com/michel-kraemer/gradle-download-task
[mockwebserver]: https://github.com/square/okhttp/tree/master/mockwebserver
[support-lib]: https://developer.android.com/topic/libraries/support-library/index.html
[arch]: https://developer.android.com/arch
[data-binding]: https://developer.android.com/topic/libraries/data-binding/index.html
[espresso]: https://google.github.io/android-testing-support-library/docs/espresso/
[dagger2]: https://google.github.io/dagger
[retrofit]: http://square.github.io/retrofit
[glide]: https://github.com/bumptech/glide
[timber]: https://github.com/JakeWharton/timber
[mockito]: http://site.mockito.org 
[rx-java]: https://github.com/ReactiveX/RxJava
[rx-android]: https://github.com/ReactiveX/RxAndroid
[material-dialogs]: https://github.com/afollestad/material-dialogs
[sql-cipher]: https://github.com/sqlcipher/android-database-sqlcipher
[anko]: https://github.com/Kotlin/anko
[ticker]: https://github.com/robinhood/ticker
[about-libraries]: https://github.com/mikepenz/AboutLibraries
[adapter-delegates]: https://github.com/sockeqwe/AdapterDelegates
[easy-permissions]: https://github.com/googlesamples/easypermissions
[ucrop]: https://github.com/Yalantis/uCrop
[sectioned-recyclerview]: https://github.com/afollestad/sectioned-recyclerview
[apache-commons-lang]: https://commons.apache.org/proper/commons-lang/dependency-info.html